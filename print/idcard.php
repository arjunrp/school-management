<?php
$sid = $_REQUEST["sid"];
if (!loggedUser($sid)) { 
	redirect (HOST_URL_ADMIN ."/include/messages.php?msg=relogin");
exit();
}
?>
<html>
<link rel="stylesheet" href="<?=ADMIN_CSS_PATH?>print_id_card.css" type="text/css">
<link rel="stylesheet" href="<?=ADMIN_CSS_PATH?>id_card_style.css" type="text/css">
<body  id="printarea">
    <table class="tble">
        <tr>
            <td>
                Student Name
            </td>
            <td>
                John Sypon
            </td>
        </tr>
        <tr>
            <td>
                Student Rollnumber
            </td>
            <td>
                R001
            </td>
        </tr>
        <tr>
            <td>
                Student Address
            </td>
            <td>
                132 Kane Street Toledo OH 43612.
            </td>
        </tr>
        <tr>
            <td>
                <input type="button" value="Print" class="btn" onClick="PrintDoc()"/>
            </td>
            <td>
                <input type="button" value="Print Preview" class="btn" onClick="PrintPreview()"/>
            </td>
        </tr>
    </table>
</body>
<script type="text/javascript">

/*--This JavaScript method for Print command--*/

    function PrintDoc() {

        var toPrint = document.getElementById('printarea');

        var popupWin = window.open('', '_blank', 'width=350,height=150,location=no,left=200px');

        popupWin.document.open();

        popupWin.document.write('<html><title>::Preview::</title><link rel="stylesheet" type="text/css" href="print.css" /></head><body onload="window.print()">')

        popupWin.document.write(toPrint.innerHTML);

        popupWin.document.write('</html>');

        popupWin.document.close();

    }

/*--This JavaScript method for Print Preview command--*/

    function PrintPreview() {

        var toPrint = document.getElementById('printarea');

        var popupWin = window.open('', '_blank', 'width=350,height=150,location=no,left=200px');

        popupWin.document.open();

        popupWin.document.write('<html><title>::Print Preview::</title><link rel="stylesheet" type="text/css" href="Print.css" media="screen"/></head><body">')

        popupWin.document.write(toPrint.innerHTML);

        popupWin.document.write('</html>');

        popupWin.document.close();

    }

</script>
</html>
