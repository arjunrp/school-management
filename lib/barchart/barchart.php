<?php

/* ------------------------
| BARCHART.PHP
| Copyright, Andrew Croxall (www.mitya.co.uk)
| This script may be freely modified and distributed but this header MUST remain intact.
| For usage info and demo, see http://mitya.co.uk/scripts/GD-barchart-generator-117
------------------------ */


/* ------------------------
| INITIAL VALIDATION
------------------------ */

if (!array_key_exists("data", $_GET))
    die("Error 1 - no data passed!");
else {
    $data = explode('|', $_GET['data']);
    if ($data <= 1)
        die("Error 2 - insufficient data passed!");
}


/* ------------------------
| FONT CONFIG
------------------------ */

$f = "font.ttf";
$fs = 9;
$fs2 = 8;
$fs3 = 9;
$fAngle = @is_numeric($_GET['fontAngle']) ? $_GET['fontAngle'] : -45;


/* ------------------------
| PREP
------------------------ */

//check font path is valid
if (!file_exists($f)) die("Error3 - font file not found");

//find out which X axis label will take up most space (used for clipping on eventual image). While we're here, also find out highest Y axis label

$labelDims = array('width' => array(), 'height' => array());
$labels = array();
$highestYAxisVal = 0;
foreach($data as $key => $val) {
    $splitter = explode(':', $val);
    $thisLabelBox = imagettfbbox($fs, $fAngle, $f, $splitter[1]);
    $labelDims['heights'][] = abs($thisLabelBox[1] - $thisLabelBox[3]);
    $labelDims['widths'][] = abs($thisLabelBox[4] - $thisLabelBox[0]);
    if ($splitter[0] > $highestYAxisVal) $highestYAxisVal = $splitter[0];
}

//Y axis prep - what increments to display? Also, handle possible request to show more increments than highest data value necessitates, for visual claritys

if (!@is_numeric($_GET['increment'])) {
if ($highestYAxisVal <= 20)
    $increment = 1;
else if ($highestYAxisVal <= 200)
    $increment = 10;
else if ($highestYAxisVal <= 2000)
    $increment = 100;
else if ($highestYAxisVal <= 20000)
    $increment = 1000;
} else
    $increment = $_GET['increment'];

$numUnitsAboveMax = @is_numeric($_GET['numUnitsAboveMax']) ? $_GET['numUnitsAboveMax'] : 1;
$highestYAxisVal += $increment * $numUnitsAboveMax;

//sizing and spacing

$chartHeight = @is_numeric($_GET['chartHeight']) ? $_GET['chartHeight'] : 400;
$paddingAboveLabels = 10;
$bottomLabelsAreaHeight = max($labelDims['heights']) + $paddingAboveLabels;
$yAxisIndent = 40;
if (array_key_exists('yAxisInfo', $_GET)) {
    $infoBBox = imagettfbbox($fs3, 90, $f, $_GET['yAxisInfo']);
    $yAxisIndent += ($infoBBox[2] - $infoBBox[6]) + 10;
}
$labelTextIndent = 10;
foreach($labelDims['widths'] as &$t) $t += $labelTextIndent;
$chartBlockWidth = @is_numeric($_GET['blockWidth']) ? $_GET['blockWidth'] : 30;
$spaceBetweenBlocks = @is_numeric($_GET['spaceBetweenBlocks']) ? $_GET['spaceBetweenBlocks'] : 0;
$topBuffer = 10;
$yAxisMarkersWidth = 8;

//some shortcut vars
$xAxisYPos = $chartHeight - $bottomLabelsAreaHeight;
$xPosOfRightOfLastBlock = $yAxisIndent + ($chartBlockWidth * count($data)) + (count($data)-1) + ($spaceBetweenBlocks * (count($data)+1)) + ($spaceBetweenBlocks > 0 ? 2 : 0);

//GD
$workingImg = imagecreatetruecolor($xPosOfRightOfLastBlock, $chartHeight - $topBuffer);


/* ------------------------
| COLOURS CONFIG
------------------------ */

function decipherColour($col) {
    return array_key_exists("randomColours", $_GET) ? rand(0, 255) : $col;
}

$colours = array( //specify script-wide colours in array
    "bg" => imagecolorallocate($workingImg, 255, 255, 255),
    "axis" => imagecolorallocate($workingImg, 210, 210, 210),
    "gridBg" => imagecolorallocate($workingImg, 245, 245, 245),
    "main" => array( //i.e. the actual pieces
        imagecolorallocate($workingImg, decipherColour(236), decipherColour(0), decipherColour(140)),
        imagecolorallocate($workingImg, decipherColour(46), decipherColour(49), decipherColour(146)),
        imagecolorallocate($workingImg, decipherColour(140), decipherColour(198), decipherColour(63)),
        imagecolorallocate($workingImg, decipherColour(249), decipherColour(237), decipherColour(51)),
        imagecolorallocate($workingImg, decipherColour(246), decipherColour(104), decipherColour(0)),
        imagecolorallocate($workingImg, decipherColour(150), decipherColour(14), decipherColour(76)),
        imagecolorallocate($workingImg, decipherColour(0), decipherColour(174), decipherColour(239)),
        imagecolorallocate($workingImg, decipherColour(240), decipherColour(104), decipherColour(166)),
        imagecolorallocate($workingImg, decipherColour(1), decipherColour(105), decipherColour(152)),
        imagecolorallocate($workingImg, decipherColour(70), decipherColour(128), decipherColour(0)),
        imagecolorallocate($workingImg, decipherColour(179), decipherColour(167), decipherColour(0))
    ),
    "text" => imagecolorallocate($workingImg, 0, 0, 0),
    "text2" => imagecolorallocate($workingImg, 236, 0, 140),
    "shadow" => imagecolorallocate($workingImg, 240, 240, 240)
);

//bg
imagefilledrectangle($workingImg, 0, 0, imagesx($workingImg), imagesy($workingImg), $colours['bg']);


/* ------------------------
| BUILD FRAMEWORK
------------------------ */

//axies, grid background & grid border, Y axis info

imageline($workingImg, $yAxisIndent, 0, $yAxisIndent, imagesy($workingImg) - $bottomLabelsAreaHeight + ($yAxisMarkersWidth * 1.5), $colours['axis']);
imageline($workingImg, $yAxisIndent - ($yAxisMarkersWidth * 1.5), $xAxisYPos - $topBuffer, imagesx($workingImg), $xAxisYPos - $topBuffer, $colours['axis']);

if (array_key_exists('doGridBack', $_GET))
    imagefilledrectangle($workingImg, $yAxisIndent+1, $xAxisYPos - 1, $xPosOfRightOfLastBlock, 0, $colours['gridBg']);

if (array_key_exists('doGridBorder', $_GET)) {
    imageline($workingImg, $xPosOfRightOfLastBlock, 0, $xPosOfRightOfLastBlock, $xAxisYPos, $colours['axis']);
    imageline($workingImg, $yAxisIndent, 0, imagesx($workingImg), 0, $colours['axis']);
}

if (array_key_exists('yAxisInfo', $_GET)) imagettftext($workingImg, $fs3, 90, 15, ($xAxisYPos / 2) + (($infoBBox[7] - $infoBBox[3]) / 2), $colours['text2'], $f, $_GET['yAxisInfo']);

//Y axis markers

$availArea = imagesy($workingImg) - $bottomLabelsAreaHeight;
$onePixelEquals = $availArea / $highestYAxisVal;
$totalStages = round($highestYAxisVal / $increment);
$counter = 0;

while ($counter < $totalStages+1) {
    $thisTBox = imagettfbbox($fs2, 0, $f, $increment * $counter);
    $textMiddleCorrector = $thisTBox[3] - $thisTBox[7] / 2;
    $thisY = $availArea - (($increment * $counter) * $onePixelEquals);
    $yLabels[] = array('x' => $yAxisIndent - 7 - $yAxisMarkersWidth - ($thisTBox[2] - $thisTBox[6]), 'y' => $thisY + $textMiddleCorrector + $topBuffer, 'text' => $increment * $counter);
    if ($counter != 0) {
        $x2 = !array_key_exists('noGrid', $_GET) ? $xPosOfRightOfLastBlock : $yAxisIndent;
        imageline($workingImg, $yAxisIndent-$yAxisMarkersWidth, $thisY, $x2, $thisY, $colours['axis']);
    }
    $counter++;
}


/* ------------------------
| BUILD CHART
------------------------ */

$xAxisY = imagesy($workingImg) - $bottomLabelsAreaHeight;
foreach($data as $key => $val) {

    //prep
    $splitter = explode(':', $val);
    $nextX = $yAxisIndent + 1 + $spaceBetweenBlocks + ($key * ($chartBlockWidth+$spaceBetweenBlocks)) + $key;

    //label (store here, don't write, as they're going on the eventual image, not the working one, for clipping purposes)
    $labels[] = array('text' => $splitter[1], 'x' => $nextX + $labelTextIndent, 'y' => $xAxisY + $paddingAboveLabels + $topBuffer);

    //block
    imagefilledrectangle($workingImg, $nextX, $xAxisY - 1, $nextX + $chartBlockWidth, ($endY = $xAxisY - ($splitter[0] / $highestYAxisVal) * 100 * ($xAxisY / 100)), $colours['main'][$key]);

    //gradient?
    $validGrads = array('light' => 255, 'dark' => 0);
    if (array_key_exists('gradient', $_GET)) {
        if (@array_key_exists($_GET['gradient'], $validGrads)) {
            for($i = $xAxisY-1; $i>$endY-1; $i--) {
                $color = imagecolorallocatealpha(
                    $workingImg,
                    $validGrads[$_GET['gradient']],
                    $validGrads[$_GET['gradient']],
                    $validGrads[$_GET['gradient']],
                    (($xAxisY - 1) - $i) / ($xAxisY - 1 - $endY) * 127
                );
                imageline($workingImg, $nextX, $i, $nextX + $chartBlockWidth, $i, $color);
            }
        }
    }


}



/* ------------------------
| PREP EVENTUAL IMAGE AND ADD LABELS
------------------------ */

$lastLabelEndX = $nextX + $labelDims['widths'][count($labelDims['widths'])-1];
$eventualImg = imagecreatetruecolor(imagesx($workingImg) < $lastLabelEndX ? $lastLabelEndX : imagesx($workingImg), imagesy($workingImg) + $topBuffer);
imagefilledrectangle($eventualImg, 0, 0, imagesx($eventualImg), imagesy($eventualImg), $colours['bg']);
imagecopy($eventualImg, $workingImg, 0, $topBuffer, 0, 0, imagesx($workingImg), imagesy($workingImg));

foreach($labels as $val) imagettftext($eventualImg, $fs, $fAngle, $val['x'], $val['y'], $colours['text'], $f, $val['text']);
foreach($yLabels as $val) imagettftext($eventualImg, $fs2, 0, $val['x'], $val['y'], $colours['text'], $f, $val['text']);


/* ------------------------
| OUTPUT
------------------------ */

header("content-type: image/png");
imagepng($eventualImg);
imagedestroy($workingImg);
imagedestroy($eventualImg);

?>