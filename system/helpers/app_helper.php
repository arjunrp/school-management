<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @desc   	  	App  Helper
 *
 * @category   	Helper functions for the current project
 * @author     	Fawaad Ahmed <fawaadmcs@yahoo.com>
 * @version    	0.1
 */
	
	
    /*
	* @desc    Convert given text to format which is - separated.	hello world -> hello-world 
	*		   Remove unwanted characters from string and leave one
	*		   space between words if there are more space characters
	*
	* @param   String
	* @access  default
	*/
if ( ! function_exists('text_to_url'))
{
    function text_to_url($text_) {
		$forbidden = '/[@$%^*!\|,`~<>{}[\]()\+"#;:.]+|(?:SELECT|UPDATE|DELETE|INSERT|DROP|CONCAT)/i';
		$text_ = preg_replace($forbidden, '', $text_ );
		$text_ = str_replace("/","",$text_);
		$text_ = str_replace("'","",$text_);
		$text_ = preg_replace("/[ ]+/"," ",$text_);		//Convert more than one space into one
		$text_ = str_replace("-"," ",$text_);	//Convert - into space
		
		$text_ = str_replace(" ","-",$text_);	//Convert space into -
		$text_ = strtolower($text_);
		return $text_;
	}
}


    /*
	* @desc    Subscribe to newsletter
	*
	* @param   String
	* @access  default
	*/
if ( ! function_exists('subscribe_to_nl'))
{
    function subscribe_to_nl($nl_email) {
		$CI = & get_instance();
		$CI->load->model("model_subscribe");
		$CI->model_subscribe->save($nl_email);
		$MSG = MSG_SUBSCRIPTION_SUCCESSFULLY;
		return $MSG;
	}
}


    /*
	* @desc    Current page url
	*
	* @param   String
	* @access  default
	*/
if ( ! function_exists('get_cur_page_url'))
{
    function get_cur_page_url() {
		$cur_page_url = (!empty($_SERVER['HTTPS'])) ? "https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] : "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
		$cur_page_url = str_replace("fawaad-pc","fawaad-pc:12352",$cur_page_url);
		return $cur_page_url;
	}
}


	/**
	* @desc Limit length of string
	*
	* @param string sid
	* @access default
	*/
if ( ! function_exists('limit_length'))
{
	function limit_length($txt, $lmt) {
		if (strlen($txt)> $lmt) {
			$txt = substr($txt,0,$lmt)."...";
			return $txt;
		}
	return $txt;
	}
}
?>