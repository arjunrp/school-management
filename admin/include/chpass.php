<?php include("../header.php"); 
$User=loggedUser($sid);
if (!loggedUser($sid)) { redirect (HOST_URL ."/admin/include/messages.php?msg=relogin"); exit();}

?>
<script>
function submitForm() {
	if ( isPassword() && isNewPassword() && isPasswordSame()) {
		return true;
	} else {
		return false;
	}
}

	
function isPassword() {
	var str = document.forms[0].elements[1].value;
	if (str == "") {
		alert("\nOld Password cannot be empty!")
		document.forms[0].elements[1].value="";
		document.forms[0].elements[1].focus();
		return false;
	}
	
	if(document.forms[0].elements[1].value!="") {
	 		var str = document.forms[0].elements[1].value;
			var regExp = / /g;
			var tmp = document.forms[0].elements[1].value;
			tmp = tmp.replace(regExp,'');
			if (tmp.length <= 0) {
					alert("Enter valid Old Password!");
					document.forms[0].elements[1].value="";
					document.forms[0].elements[1].focus();
					return false;
			}	
	}	
	if (str.length<4) {
		alert("\nThe Old Password should be greater than 4 Characters")
		document.forms[0].elements[1].focus();
		return false;
	}
	return true;
}

function isNewPassword() {
	
var str = document.forms[0].elements[2].value;
	if (str == "") {
		alert("\nNew Password cannot be empty!")
		document.forms[0].elements[2].value="";
		document.forms[0].elements[2].focus();
		return false;
	}
	
	if(document.forms[0].elements[2].value!="") {
	 		var str = document.forms[0].elements[2].value;
			var regExp = / /g;
			var tmp = document.forms[0].elements[2].value;
			tmp = tmp.replace(regExp,'');
			if (tmp.length <= 0) {
					alert("Enter valid New Password!");
					document.forms[0].elements[2].value="";
					document.forms[0].elements[2].focus();
					return false;
			}	
	}	
	if (str.length<4) {
		alert("\nThe New Password should be greater than 4 Characters")
		document.forms[0].elements[2].focus();
		return false;
	}
return true;
}
function isPasswordSame() {
	var str1 = document.forms[0].elements[2].value;
	var str2 = document.forms[0].elements[3].value;
	if (str1 != str2) {
		alert("\nPassword mismatch, Please Retype same Passwords in both fields.")
		document.forms[0].elements[2].focus();
		return false;
	}
return true;
}

//-->

</script>
<link rel="stylesheet" href="<?=ADMIN_CSS_PATH?>interface.css" type="text/css">
</head>
<STYLE type=text/css>#dek {
	Z-INDEX: 200; VISIBILITY: hidden; POSITION: absolute
}
</STYLE>
<?php
if($ssubmit == "yes"){
	$Querry="SELECT password FROM ". TBL_ADMIN_USERS ." WHERE user_id='$UserID' and password='$OldPass'";	
	if (isExist($Querry) ){
		    $updateQuerry="UPDATE ". TBL_ADMIN_USERS ." SET password ='$NewPass' WHERE user_id='$UserID'";
		    update($updateQuerry);
		    $MSG = "Your password has been changed successfully." ;
	}else{
		    $MSG = "Your Old password is not correct." ;
	}	

}
?>
<p>&nbsp;</p>
<table width=60%  align="center" cellpadding="0" cellspacing="0" class="bordLightBlue">
        <tr align="left"  valign="middle"> 
    <td height="24" class="adminDetailHeading">&nbsp;&nbsp;<b>Change Password </b></td>
  </tr>
    <tr> 
      <td align="center" valign="middle" > 
        <form method="post" action="" onSubmit="return submitForm()">
	                        <table width="90%" border="0" align="center" cellpadding="4" cellspacing="4">
                                        <tr > 
                                                <td colspan="3" align=center valign=bottom> 
                                                        <br> <span class="msgColor"><?php echo $MSG;?></span> 
                                                        <br></td>
                                        </tr>
                                        <tr > 
                                                <td colspan="3"  align=left 
                        valign=middle><span class="msgColor">Fields marked * are 
                                                        mandatory!</span> </td>
                                        </tr>
                                        <tr > 
                                                <td width="167"  align=right valign="center">User Name&nbsp;</td>
                                                <td width="86"  align=left  valign="center"> <input type="text" name="UserID" size="30" value="<?=$User?>" readOnly class="flat"> 
                                                </td>
                                                <td width="86"  align=left valign="center">&nbsp;</td>
                                        </tr>
                                        <tr > 
                                                <td width="167" align="right">Old Password&nbsp;<span class="msgColor">*</span></td>
                                                <td width="86"  align="left"> 
                                                        <input type="password" name="OldPass" size="30" class="flat"> 
                                                </td>
                                                <td width="86"  align="left"><i><span onMouseOver="window.status='';do_info(26);return true" onmouseout=kill() href="javascript:void(0)";><img src="<?=HOST_URL?>/images/help_form.gif"></span></i></td>
                                        </tr>
                                        <tr > 
                                                <td width="167"  align=right>New Password&nbsp;<span class="msgColor">*</span></td>
                                                <td width="86"  align=left> <input type="password" name="NewPass" size="30" class="flat"> 
                                                </td>
                                                <td width="86"  align=left><i><span onMouseOver="window.status='';do_info(27);return true" onmouseout=kill() href="javascript:void(0)";><img src="<?=HOST_URL?>/images/help_form.gif"></span></i></td>
                                        </tr>
                                        <tr > 
                                                <td width="167" align=right>Confirm password&nbsp;<span class="msgColor">*</span></td>
                                                <td width="86" align=left> <input type="password" name="ConPass" size="30" class="flat"> 
                                                </td>
                                                <td width="86" align=left><i><span onMouseOver="window.status='';do_info(28);return true" onmouseout=kill() href="javascript:void(0)";><img src="<?=HOST_URL?>/images/help_form.gif"></span></i></td>
                                        </tr>
                                        <tr align="center" > 
                                                <td  colspan="3">&nbsp;</td>
                                        </tr>
                                        <tr align="center" > 
                                                <td  colspan="3"> <input type="hidden" name="sec" value="<?=md5("04")?>"> 
                                                        <input type="hidden" name="sid" value="<?=$sid?>"> 
                                                        <input name="submit" type=submit class="flat" value="Change Password!"> 
                                                        <input name="ssubmit" type="hidden" id="ssubmit" value="yes"> 
                                                </td>
                                        </tr>
                                </table> 
        </form> </td>
    </tr>
</table>
<DIV id=dek></DIV>
<SCRIPT type=text/javascript src="<?=SCRIPTS_FOLDER?>/popupscript.js" ></SCRIPT>
<?php include("../footer.php"); ?>
