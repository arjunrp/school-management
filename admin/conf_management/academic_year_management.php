<?php

include("../header.php"); 

$sid = $_REQUEST["sid"];

if (!loggedUser($sid)) { 

	redirect (HOST_URL_ADMIN ."/include/messages.php?msg=relogin");

exit();

}

$tbl_school_id_sess = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];

$LIB_CLASSES_FOLDER = $_SERVER['DOCUMENT_ROOT']."/aqdar/lib/classes/";

include($LIB_CLASSES_FOLDER."Paging.php");

mysql_query("SET NAMES 'utf8'");

mysql_query('SET CHARACTER SET utf8'); 

?>

<?php

	$q = stripslashes($q);	

	if ($ssubmit=="insertNews") {
		$TBL_ACADEMIC_YEAR_id = getMD5_ID();
		$qry_semester_select = "SELECT * FROM ".TBL_ACADEMIC_YEAR." WHERE tbl_school_id='$tbl_school_id_sess' AND academic_start = '$academic_start' AND is_active='Y'";
		//echo $qry_semester_select; exit;
		$existSemester = SelectMultiRecords($qry_semester_select);

		if(empty($existSemester)){
				$qry = "INSERT INTO ".TBL_ACADEMIC_YEAR." (
					`tbl_academic_year_id` ,
					`academic_start` ,
					`academic_end` ,
					`added_date`,
					`tbl_school_id`
					)
					VALUES ('$TBL_ACADEMIC_YEAR_id','$academic_start', '$academic_end', NOW(),'$tbl_school_id_sess')";

				insertInto($qry);
				$academic_start =  "";
				$academic_end 	=  "";
				$is_active 		=  "";
				$MSG = "Academic year has been saved successfully.";

		}else{
			$academic_start   =  $_POST['academic_start'];
			$academic_end 	 =  $_POST['academic_end'];
			$MSG = "Same academic year already exist.";	
		}
		$mid=1;
	}//if 


if($ssubmit=="editNews") {
	   $qry_semester_data = "SELECT * FROM ".TBL_ACADEMIC_YEAR." WHERE tbl_school_id='$tbl_school_id_sess' AND academic_start = '$academic_start' AND id<>'$id' AND is_active='Y'";
		//echo $qry_semester_data; exit;
		$existSemester = SelectMultiRecords($qry_semester_data);
		if(empty($existSemester)){
				$qry_update = "UPDATE ".TBL_ACADEMIC_YEAR." SET academic_start = '$academic_start' , academic_end='$academic_end' WHERE id=$id ";
				update($qry_update);
				$MSG .= "Changes saved successfully.";
		}else{
			$MSG = "Same academic year already exist..";	
		}
		$mid=3;
}

if($ssubmit=="newsDetail"){
		if(isset($show)){
				for ($j=0; $j<count($EditBox); $j++){ 
					$Querry = "UPDATE ". TBL_ACADEMIC_YEAR ." SET is_active='Y' WHERE id=$EditBox[$j]";			
					update($Querry);
					$mid=1;	
					$MSG = "Selected Academic Year(s) have been activated successfully!";
				}
		}

		if(isset($hide)){
				for ($j=0; $j<count($EditBox); $j++){ 
					$Querry = "UPDATE ". TBL_ACADEMIC_YEAR ." SET is_active='N' WHERE id=$EditBox[$j]";			
					update($Querry);
					$mid=1;	
					$MSG = "Selected Academic Year(s) have been deactivated successfully!";
				}
			}
		if(isset($delete)){$mid = 4;}
	}

if($ssubmit == "newsDelConfirmation"){
		for($i=0;$i<count($EditBox);$i++){
				$qry_del = "DELETE FROM ".TBL_ACADEMIC_YEAR." WHERE `id`=$EditBox[$i]";
				deleteFrom($qry_del);
				$mid = 1;
				$MSG = "Selected Academic Year(s) have been deleted successfully!";
		}//for
}	

if ($color == "") {
	$color = "e0eaf1";
}
?>



<link rel="stylesheet" href="<?=ADMIN_CSS_PATH?>interface.css" type="text/css">
<script src="<?=JS_PATH?>/jquery.min.js" type="text/javascript"></script>
<script src="<?=ADMIN_SCRIPT_PATH?>/validation.js">CopyDatarichtext1();</script>
<script language="JavaScript">
function valueCheckedForNewsManagement(){
	var ml = document.frmDirectory;
	var len = ml.elements.length;
	for (var i = 0; i < len; i++){
	   	if (document.frmDirectory.elements[i].checked){
			return true;
		}
	}
	 alert ("Select at least one academic year!");
	 return false;
}

function CheckAll(){
	var ml = document.frmDirectory;
	var len = ml.elements.length;
	if (document.frmDirectory.AC.checked==true) {
		 for (var i = 0; i < len; i++) {
			document.frmDirectory.elements[i].checked=true;
		 }
	} else {
		  for (var i = 0; i < len; i++)  {
			document.frmDirectory.elements[i].checked=false;
		  }
	}
}

function UnCheckAll() {
	var ml = document.frmDirectory;
	var len = ml.elements.length;
	var count=0; var checked=0;
	    for (var i = 0; i < len; i++) {	       
			if ((document.frmDirectory.elements[i].type=='checkbox') && (document.frmDirectory.elements[i].name != "AC")) {
				count = count + 1;
	  		 	if (document.frmDirectory.elements[i].checked == true){
					checked = checked + 1;
				}
			}
	     }
	if (checked == count) {
		 document.frmDirectory.AC.checked = true;
	} else {
		document.frmDirectory.AC.checked = false;
	}
}

</script>
<script language="JavaScript">
function validateForm() {
	if (validate_academic() == false ) {
		return false;
	}else{
		return true;
	}
return true;	
}

	 function validate_academic(){
		var startAcademic = document.frmRegister.academic_start.value;
		var endAcademic = document.frmRegister.academic_end.value;
		if (startAcademic == "") {
			alert("Please enter academic Start Year.");
			$("#academic_start").focus();
			return false;
		}

		if (endAcademic == "") {
			alert("Please enter academic End Year.");
			$("#academic_end").focus();
			return false;
		}
		return true;
	 }
</script>
<link rel="stylesheet" media="all" type="text/css" href="<?=JS_PATH?>/date_time_picker/css/jquery-ui-1.8.6.custom.css" />
<style type="text/css">
pre {
	padding: 20px;
	background-color: #ffffcc;
	border: solid 1px #fff;
}
.wrapper {
	background-color: #ffffff;
	width: 800px;
	border: solid 1px #eeeeee;
	padding: 20px;
	margin: 0 auto;
}
.example-container {
	background-color: #f4f4f4;
	border-bottom: solid 2px #777777;
	margin: 0 0 40px 0;
	padding: 20px;
}
.example-container p {
	font-weight: bold;
}

.example-container dt {
	font-weight: bold;
	height: 20px;
}
.example-container dd {
	margin: -20px 0 10px 100px;
	border-bottom: solid 1px #fff;
}
.example-container input {
	width: 150px;
}
.clear {
	clear: both;
}
#ui-datepicker-div {
}
.ui-timepicker-div .ui-widget-header {
	margin-bottom: 8px;
}
.ui-timepicker-div dl {
	text-align: left;
}
.ui-timepicker-div dl dt {
	height: 25px;
}
.ui-timepicker-div dl dd {
	margin: -25px 0 10px 65px;
}
.ui-timepicker-div td {
	font-size: 90%;
}

</style>

<script type="text/javascript" src="<?=JS_PATH?>/date_time_picker/js/jquery-ui-1.8.6.custom.min.js"></script>
<script type="text/javascript" src="<?=JS_PATH?>/date_time_picker/js/jquery-ui-timepicker-addon.js"></script>
<script language="javascript">
	$(document).ready(function(){
		$('#start_date').datetimepicker({



			dateFormat: 'yy-mm-dd', showTime:false, showMinute: false, showSecond: false, showMillisec: false, showHour: false,



			timeFormat: '',



		});



		$('#end_date').datetimepicker({



			dateFormat: 'yy-mm-dd',  showTime:false, showMinute: false, showSecond: false, showMillisec: false, showHour: false,



			timeFormat: '',



		});



	

	});



</script>



<script type="text/javascript" src="<?=JS_COLOR_PATH?>/jscolor.js"></script>
</head>


<?php if (!$offset || $offset<0)  { $offset =0;} ?>



<?php if (!$LIKE)  { $LIKE = "LIKE";} ?>



  <STYLE type=text/css>



#dek {



	Z-INDEX: 200; VISIBILITY: hidden; POSITION: absolute



}



</STYLE>



  <?php if($mid=="1"){ ?>



<table width="100%" height="570" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">



  <tr> 



    <td height="30" valign="center" class="adminDetailHeading">Academic Year Management 



    </td>



  </tr>



  <tr align="center" > 



    <td height="538" align="center" valign="top" > 



    



    <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >



        <tr>



        	<td height="22" align="center" valign="middle">



        	<?php if ($MSG != "") {	?>



   



      			<span class="msgColor"><?php echo $MSG;?></span>



           



          	<?php } ?>



        



	        </td>



        </tr>



        



        <tr >



          <td height="22" align="left" valign="middle"  class="adminDetailHeading" ><strong>&nbsp;&nbsp;Add Academic Year</strong></td>



        </tr>



        <tr >



          <td align="center" valign="top" class="bordLightBlue"><form name="frmRegister" method="post" action=""  onSubmit="return validateForm();" enctype="multipart/form-data" >



              <table width="100%" border="0" align="center" cellpadding="4" cellspacing="4" >

                <tr align="center" valign="top" >

                  <td colspan="2"></td>

                </tr>

                <tr align="left" valign="top" >

                  <td colspan="2"><span class="msgColor">Fields marked with * are mandatory!</span></td>

                </tr>

              
                  <tr valign="middle" >

                  <td height="" align="right" valign="top">Academic Year<span class="msgColor">*</span>:</td>

                  <td align="left" valign="top">

            <!-- <input id="academic_start" name="academic_start" type="text" class="flat"  maxlength="4" value="" size="10" alt="Start">-->

              <select name="academic_start" id="academic_start" class="flat">
                      <option value="" >Start Year</option>  
                      <option value="2015" <?PHP if (($academic_start == "2015")) { echo "selected";}?>>2015</option>
                      <option value="2016" <?PHP if (($academic_start == "2016")) { echo "selected";}?>>2016</option>
                      <option value="2017" <?PHP if (($academic_start == "2017")) { echo "selected";}?>>2017</option>
                      <option value="2018" <?PHP if (($academic_start == "2018")) { echo "selected";}?>>2018</option>
                      <option value="2019" <?PHP if (($academic_start == "2019")) { echo "selected";}?>>2019</option>
                      <option value="2020" <?PHP if (($academic_start == "2020")) { echo "selected";}?>>2020</option>
                      <option value="2021" <?PHP if (($academic_start == "2021")) { echo "selected";}?>>2021</option>
                      <option value="2022" <?PHP if (($academic_start == "2022")) { echo "selected";}?>>2022</option>
                      <option value="2023" <?PHP if (($academic_start == "2023")) { echo "selected";}?>>2023</option>
                      <option value="2024" <?PHP if (($academic_start == "2024")) { echo "selected";}?>>2024</option>
                      <option value="2025" <?PHP if (($academic_start == "2025")) { echo "selected";}?>>2025</option>
                      <option value="2026" <?PHP if (($academic_start == "2026")) { echo "selected";}?>>2026</option>
                      <option value="2027" <?PHP if (($academic_start == "2027")) { echo "selected";}?>>2027</option>
                      <option value="2028" <?PHP if (($academic_start == "2028")) { echo "selected";}?>>2028</option>
                      <option value="2029" <?PHP if (($academic_start == "2029")) { echo "selected";}?>>2029</option>
                      <option value="2030" <?PHP if (($academic_start == "2030")) { echo "selected";}?>>2030</option>
                      <option value="2031" <?PHP if (($academic_start == "2031")) { echo "selected";}?>>2031</option>
                      <option value="2032" <?PHP if (($academic_start == "2032")) { echo "selected";}?>>2032</option>
                      <option value="2033" <?PHP if (($academic_start == "2033")) { echo "selected";}?>>2033</option>
                      <option value="2034" <?PHP if (($academic_start == "2034")) { echo "selected";}?>>2034</option>
                      <option value="2035" <?PHP if (($academic_start == "2035")) { echo "selected";}?>>2035</option>
                      <option value="2036" <?PHP if (($academic_start == "2036")) { echo "selected";}?>>2036</option>
                      <option value="2037" <?PHP if (($academic_start == "2037")) { echo "selected";}?>>2037</option>
                      <option value="2038" <?PHP if (($academic_start == "2038")) { echo "selected";}?>>2038</option>
                      <option value="2039" <?PHP if (($academic_start == "2039")) { echo "selected";}?>>2039</option>
                      <option value="2040" <?PHP if (($academic_start == "2040")) { echo "selected";}?>>2040</option>
                      <option value="2041" <?PHP if (($academic_start == "2041")) { echo "selected";}?>>2041</option>
                      <option value="2042" <?PHP if (($academic_start == "2042")) { echo "selected";}?>>2042</option>
                      <option value="2043" <?PHP if (($academic_start == "2043")) { echo "selected";}?>>2043</option>
                      <option value="2044" <?PHP if (($academic_start == "2044")) { echo "selected";}?>>2044</option>
                      <option value="2045" <?PHP if (($academic_start == "2045")) { echo "selected";}?>>2045</option>
                      <option value="2046" <?PHP if (($academic_start == "2046")) { echo "selected";}?>>2046</option>
                      <option value="2047" <?PHP if (($academic_start == "2047")) { echo "selected";}?>>2047</option>
                      <option value="2048" <?PHP if (($academic_start == "2048")) { echo "selected";}?>>2048</option>
                      <option value="2049" <?PHP if (($academic_start == "2049")) { echo "selected";}?>>2049</option>
                      <option value="2050" <?PHP if (($academic_start == "2050")) { echo "selected";}?>>2050</option>
                    </select>

               - 


              <select name="academic_end" id="academic_end" class="flat">
                      <option value="" >End Year</option>  
                      <option value="2015" <?PHP if (($academic_end == "2015")) { echo "selected";}?>>2015</option>
                      <option value="2016" <?PHP if (($academic_end == "2016")) { echo "selected";}?>>2016</option>
                      <option value="2017" <?PHP if (($academic_end == "2017")) { echo "selected";}?>>2017</option>
                      <option value="2018" <?PHP if (($academic_end == "2018")) { echo "selected";}?>>2018</option>
                      <option value="2019" <?PHP if (($academic_end == "2019")) { echo "selected";}?>>2019</option>
                      <option value="2020" <?PHP if (($academic_end == "2020")) { echo "selected";}?>>2020</option>
                      <option value="2021" <?PHP if (($academic_end == "2021")) { echo "selected";}?>>2021</option>
                      <option value="2022" <?PHP if (($academic_end == "2022")) { echo "selected";}?>>2022</option>
                      <option value="2023" <?PHP if (($academic_end == "2023")) { echo "selected";}?>>2023</option>
                      <option value="2024" <?PHP if (($academic_end == "2024")) { echo "selected";}?>>2024</option>
                      <option value="2025" <?PHP if (($academic_end == "2025")) { echo "selected";}?>>2025</option>
                      <option value="2026" <?PHP if (($academic_end == "2026")) { echo "selected";}?>>2026</option>
                      <option value="2027" <?PHP if (($academic_end == "2027")) { echo "selected";}?>>2027</option>
                      <option value="2028" <?PHP if (($academic_end == "2028")) { echo "selected";}?>>2028</option>
                      <option value="2029" <?PHP if (($academic_end == "2029")) { echo "selected";}?>>2029</option>
                      <option value="2030" <?PHP if (($academic_end == "2030")) { echo "selected";}?>>2030</option>
                      <option value="2031" <?PHP if (($academic_end == "2031")) { echo "selected";}?>>2031</option>
                      <option value="2032" <?PHP if (($academic_end == "2032")) { echo "selected";}?>>2032</option>
                      <option value="2033" <?PHP if (($academic_end == "2033")) { echo "selected";}?>>2033</option>
                      <option value="2034" <?PHP if (($academic_end == "2034")) { echo "selected";}?>>2034</option>
                      <option value="2035" <?PHP if (($academic_end == "2035")) { echo "selected";}?>>2035</option>
                      <option value="2036" <?PHP if (($academic_end == "2036")) { echo "selected";}?>>2036</option>
                      <option value="2037" <?PHP if (($academic_end == "2037")) { echo "selected";}?>>2037</option>
                      <option value="2038" <?PHP if (($academic_end == "2038")) { echo "selected";}?>>2038</option>
                      <option value="2039" <?PHP if (($academic_end == "2039")) { echo "selected";}?>>2039</option>
                      <option value="2040" <?PHP if (($academic_end == "2040")) { echo "selected";}?>>2040</option>
                      <option value="2041" <?PHP if (($academic_end == "2041")) { echo "selected";}?>>2041</option>
                      <option value="2042" <?PHP if (($academic_end == "2042")) { echo "selected";}?>>2042</option>
                      <option value="2043" <?PHP if (($academic_end == "2043")) { echo "selected";}?>>2043</option>
                      <option value="2044" <?PHP if (($academic_end == "2044")) { echo "selected";}?>>2044</option>
                      <option value="2045" <?PHP if (($academic_end == "2045")) { echo "selected";}?>>2045</option>
                      <option value="2046" <?PHP if (($academic_end == "2046")) { echo "selected";}?>>2046</option>
                      <option value="2047" <?PHP if (($academic_end == "2047")) { echo "selected";}?>>2047</option>
                      <option value="2048" <?PHP if (($academic_end == "2048")) { echo "selected";}?>>2048</option>
                      <option value="2049" <?PHP if (($academic_end == "2049")) { echo "selected";}?>>2049</option>
                      <option value="2050" <?PHP if (($academic_end == "2050")) { echo "selected";}?>>2050</option>
                    </select>

             Eg: 2016 - 2017</td>
                </tr>
                  <td>&nbsp;</td>
                  <td align="left">
                    <input type="reset" name="Reset" value="Reset" class="flat">
                    <input name="ssubmit" type="submit" class="flat" id="ssubmit"  value="Submit">
                    <input name="ssubmit" type="hidden" id="ssubmit" value="insertNews">
                    <input name="sid" type="hidden" value="<?=$sid?>">
                    <input name="mid" type="hidden" value="1">
                    <input name="offset" type="hidden" id="offset" value="<?=$offset;?>">
                    <input name="q" type="hidden" id="q" value="<?php echo htmlspecialchars($q);?>">
                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>">
                    <input name="by" type="hidden" id="by" value="<?=$by?>">
                    <input name="field" type="hidden" id="field" value="<?=$field?>">
                    <input name="LIKE" type="hidden" value="<?=$LIKE?>">
                    <input name="tab" type="hidden" value="user">
                    <input name="subtab" type="hidden" value="academic_year">
                    </td>
                </tr>
                <tr valign="middle" >
                  <td colspan="2" align="center">
                </tr>
              </table>
          </form></td>
        </tr>
      </table>
      <br>
      <span class="msgColor"><?php echo $MSG;?></span> <br> 
      <?php 
		$q = addslashes(trim($q));

		$CountRec .= "SELECT * FROM ".TBL_ACADEMIC_YEAR." WHERE tbl_school_id='$tbl_school_id_sess' ";	

		$Query .= "SELECT * FROM ".TBL_ACADEMIC_YEAR." WHERE tbl_school_id='$tbl_school_id_sess' ";	

		if(!$by){$by="DESC";}


		 $Query .= " ORDER BY added_date $by ";



		 $Query .=" LIMIT $offset, ".TBL_CLASS_SESSIONS_PAGING;

         echo 

   	     $q = stripslashes($q);



		 



		 //echo $Query;



		 $total_record = CountRecords($CountRec);



		 $data = SelectMultiRecords($Query);



		  		 if ($total_record =="")



					   echo '<span class="msgColor">'.NO_RECORD_FOUND."</span>";



					else{	



					    echo '<span class="msgColor">';



					    echo " <b> ". $total_record ." </b> Item(s) found. Showing <b>";



						if ($total_record>$offset){



							echo $offset+1;



							echo " </b>of<b> ";



							if ($offset >= $total_record - TBL_CLASS_SESSIONS_PAGING)	{ 



								  echo $total_record; 



							}else { 



							   	echo $offset + TBL_CLASS_SESSIONS_PAGING ;



							}



						}else{ 



							echo $offset+1;



							echo "</b> - ". $total_record;



							echo " of ". $total_record ." Academic Year(s) ";		



						}



						echo "</b>.</span>";	



					}



	   ?>



      <?php  if ($total_record !=""){?>



      <table width="98%" border="0" cellspacing="0" cellpadding="0">



       <tr> 



          <td height="18"><span class="msgColor">&nbsp;&nbsp;Note: To view detail 



            click a Academic Year.</span></td>



        </tr>



        <tr> 



          <td align="center" valign="top" class="adminDetailHeading"> <table width="100%" border="0" align="center"  cellpadding="2" cellspacing="1">



              <form name="frmDirectory" onSubmit="return valueCheckedForNewsManagement()">



                <tr align="center" >


                  <td width="18%" align="center" class="adminDetailHeading">Academic Year</td>


                  <td width="11%" align="center" class="adminDetailHeading">Date</td>



                  <td width="6%" height="24" align="center" class="adminDetailHeading">Status</td>



                  <td width="6%" height="24" align="center" class="adminDetailHeading">Action</td>



                  <td width="5%" height="24" class="adminDetailHeading"><input name="AC" type="checkbox" onClick="CheckAll();"></td>



                </tr>



                <?php for($i=0;$i<count($data);$i++){
					$id = $data[$i]["id"];
					$academic_start = $data[$i]["academic_start"];
					$academic_end = $data[$i]["academic_end"];
				?>

                <tr valign="middle" bgcolor="#FFFFFF">
                 
                   <td align="center" ><?=$academic_start;?> - <?=$academic_end;?></td>
                 
                  <td align="center"><? echo date("d M, Y ",strtotime($data[$i]["added_date"]));?></td>



                  <td align="center"><?php 



				  $is_active = $data[$i]["is_active"];



				  if($is_active == 'Y'){?>



                    <img src="<?=IMG_PATH?>/yes.gif">



                    <?php } else {?>



                    <img src="<?=IMG_PATH?>/no.gif">



                    <?php }  ?></td>



                  <td align="center"><a href="?mid=3&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&by=<?=$by?>&id=<?=$id?>&tab=user&subtab=academic_year" class="detailLinkColor">Edit</a></td>



                  <td align="center" ><input name="EditBox[]" type="checkbox" onClick="UnCheckAll();" value="<?=$id?>"></td>



                </tr>



                <?php }?>



                <tr>



                  <td height="20" colspan="10" align="center" valign="middle" bgcolor="#FFFFFF"><input name="reset" type="reset" class="flat"  value="Reset">



                    <input name="show" type="submit" class="flat" id="ssubmit" value="Active">



                    <input name="hide" type="submit" class="flat"  value="Deactive">



                    <input name="delete" type="submit" class="flat"  value="Delete">



                    <input name="ssubmit" type="hidden" id="ssubmit" value="newsDetail">



                    <input name="mid" type="hidden" id="mid2" value="1">



                    <input name="sid" type="hidden" id="sid" value="<?=$sid?>">



                    <input name="offset" type="hidden" id="offset" value="<?=$offset?>">



                    <input name="q" type="hidden" id="q" value="<? echo htmlspecialchars($q);?>">



                    <input name="id" type="hidden" value="<?=$id?>">



                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>">



                    <input name="by" type="hidden" id="by" value="<?=$by?>">



                    <input name="field" type="hidden" id="field" value="<?=$field?>">



                    <input name="LIKE" type="hidden" value="<?=$LIKE?>">



                    <input name="tab" type="hidden" value="user">



                    <input name="subtab" type="hidden" value="academic_year">



                    </td>



                </tr>



                <tr align="right">



                  <td height="20" colspan="10" valign="middle" bgcolor="#FFFFFF">



         				<?php 



						   if ($total_record != "" && $total_record>TBL_CLASS_SESSIONS_PAGING) {



								$url = "academic_year_management.php?mid=1&sid=$sid&field=$field&q=$q&LIKE=$LIKE&by=$by&tab=user&subtab=academic_year";



								$Paging_object = new Paging();



								$Paging_object->paging_new_style_final($url,$offset,$clicked_link,$total_record,TBL_CLASS_SESSIONS_PAGING);



							}



						?>



         



         			</td>



                </tr>



              </form>



            </table></td>



        </tr>



      </table>



      <br> 



      <?php } ?>







      <br> <br> 



      <br></td>



  </tr>



</table>
<?php }if($mid=="3"){ 

		$data = selectFrom("SELECT * FROM ".TBL_ACADEMIC_YEAR." WHERE id=$id");

		$id 			= $data["id"];
		$academic_start = $data["academic_start"];
		$academic_end 	= $data["academic_end"];
		$is_active 		= $data["is_active"];
?>

<table width="100%"  border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">

  <tr> 
    <td height="30" valign="center" class="adminDetailHeading">&nbsp;Academic Year(s)  Management 
    [Edit] </td>
  </tr>

  <tr> 



    <td align="center" valign="top"> <br>
      <span class="msgColor"><?php echo $MSG;?></span> <br> 
      <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >
        <tr > 
          <td height="22" align="left" valign="middle"  class="adminDetailHeading" >&nbsp;&nbsp;Edit Academic Year</td>
        </tr>

        <tr> 
          <td align="left" valign="top" class="bordLightBlue"> <form name="frmRegister" method="post" action=""  onSubmit="return validateForm();"  enctype="multipart/form-data">
              <table width="100%" border="0" align="center" cellpadding="4" cellspacing="4" >
                <tr align="center" valign="top" > 
                  <td colspan="2"></td>
                </tr>
                <tr align="left" valign="top" > 
                  <td colspan="2"><span class="msgColor">Fields marked with * are mandatory!</span></td>
                </tr>
                
                 <tr valign="middle">
                  <td height="" align="right" valign="top">Academic Year<span class="msgColor">*</span>:</td>

                  <td align="left" valign="top">

            <!-- <input id="academic_start" name="academic_start" type="text" class="flat"  maxlength="4" value="" size="10" alt="Start">-->

              <select name="academic_start" id="academic_start">

                      <option value="" >Start Year</option>  

                      <option value="2015" <?PHP if (($academic_start == "2015")) { echo "selected";}?>>2015</option>

                      <option value="2016" <?PHP if (($academic_start == "2016")) { echo "selected";}?>>2016</option>

                      <option value="2017" <?PHP if (($academic_start == "2017")) { echo "selected";}?>>2017</option>

                      <option value="2018" <?PHP if (($academic_start == "2018")) { echo "selected";}?>>2018</option>

                      <option value="2019" <?PHP if (($academic_start == "2019")) { echo "selected";}?>>2019</option>

                      <option value="2020" <?PHP if (($academic_start == "2020")) { echo "selected";}?>>2020</option>

                      <option value="2021" <?PHP if (($academic_start == "2021")) { echo "selected";}?>>2021</option>

                      <option value="2022" <?PHP if (($academic_start == "2022")) { echo "selected";}?>>2022</option>

                      <option value="2023" <?PHP if (($academic_start == "2023")) { echo "selected";}?>>2023</option>

                      <option value="2024" <?PHP if (($academic_start == "2024")) { echo "selected";}?>>2024</option>

                      <option value="2025" <?PHP if (($academic_start == "2025")) { echo "selected";}?>>2025</option>

                      <option value="2026" <?PHP if (($academic_start == "2026")) { echo "selected";}?>>2026</option>

                      <option value="2027" <?PHP if (($academic_start == "2027")) { echo "selected";}?>>2027</option>

                      <option value="2028" <?PHP if (($academic_start == "2028")) { echo "selected";}?>>2028</option>

                      <option value="2029" <?PHP if (($academic_start == "2029")) { echo "selected";}?>>2029</option>

                      <option value="2030" <?PHP if (($academic_start == "2030")) { echo "selected";}?>>2030</option>

                      <option value="2031" <?PHP if (($academic_start == "2031")) { echo "selected";}?>>2031</option>

                      <option value="2032" <?PHP if (($academic_start == "2032")) { echo "selected";}?>>2032</option>

                      <option value="2033" <?PHP if (($academic_start == "2033")) { echo "selected";}?>>2033</option>

                      <option value="2034" <?PHP if (($academic_start == "2034")) { echo "selected";}?>>2034</option>

                      <option value="2035" <?PHP if (($academic_start == "2035")) { echo "selected";}?>>2035</option>

                      <option value="2036" <?PHP if (($academic_start == "2036")) { echo "selected";}?>>2036</option>

                      <option value="2037" <?PHP if (($academic_start == "2037")) { echo "selected";}?>>2037</option>

                      <option value="2038" <?PHP if (($academic_start == "2038")) { echo "selected";}?>>2038</option>

                      <option value="2039" <?PHP if (($academic_start == "2039")) { echo "selected";}?>>2039</option>

                      <option value="2040" <?PHP if (($academic_start == "2040")) { echo "selected";}?>>2040</option>

                      <option value="2041" <?PHP if (($academic_start == "2041")) { echo "selected";}?>>2041</option>

                      <option value="2042" <?PHP if (($academic_start == "2042")) { echo "selected";}?>>2042</option>

                      <option value="2043" <?PHP if (($academic_start == "2043")) { echo "selected";}?>>2043</option>

                      <option value="2044" <?PHP if (($academic_start == "2044")) { echo "selected";}?>>2044</option>

                      <option value="2045" <?PHP if (($academic_start == "2045")) { echo "selected";}?>>2045</option>

                      <option value="2046" <?PHP if (($academic_start == "2046")) { echo "selected";}?>>2046</option>

                      <option value="2047" <?PHP if (($academic_start == "2047")) { echo "selected";}?>>2047</option>

                      <option value="2048" <?PHP if (($academic_start == "2048")) { echo "selected";}?>>2048</option>

                      <option value="2049" <?PHP if (($academic_start == "2049")) { echo "selected";}?>>2049</option>

                      <option value="2050" <?PHP if (($academic_start == "2050")) { echo "selected";}?>>2050</option>

                    </select>

              

              

               - 

             <!-- <input id="academic_end" name="academic_end" type="text" class="flat"  maxlength="4" value="" size="10" alt="End">-->  

              <select name="academic_end" id="academic_end">

                      <option value="" >End Year</option>  

                      <option value="2015" <?PHP if (($academic_end == "2015")) { echo "selected";}?>>2015</option>

                      <option value="2016" <?PHP if (($academic_end == "2016")) { echo "selected";}?>>2016</option>

                      <option value="2017" <?PHP if (($academic_end == "2017")) { echo "selected";}?>>2017</option>

                      <option value="2018" <?PHP if (($academic_end == "2018")) { echo "selected";}?>>2018</option>

                      <option value="2019" <?PHP if (($academic_end == "2019")) { echo "selected";}?>>2019</option>

                      <option value="2020" <?PHP if (($academic_end == "2020")) { echo "selected";}?>>2020</option>

                      <option value="2021" <?PHP if (($academic_end == "2021")) { echo "selected";}?>>2021</option>

                      <option value="2022" <?PHP if (($academic_end == "2022")) { echo "selected";}?>>2022</option>

                      <option value="2023" <?PHP if (($academic_end == "2023")) { echo "selected";}?>>2023</option>

                      <option value="2024" <?PHP if (($academic_end == "2024")) { echo "selected";}?>>2024</option>

                      <option value="2025" <?PHP if (($academic_end == "2025")) { echo "selected";}?>>2025</option>

                      <option value="2026" <?PHP if (($academic_end == "2026")) { echo "selected";}?>>2026</option>

                      <option value="2027" <?PHP if (($academic_end == "2027")) { echo "selected";}?>>2027</option>

                      <option value="2028" <?PHP if (($academic_end == "2028")) { echo "selected";}?>>2028</option>

                      <option value="2029" <?PHP if (($academic_end == "2029")) { echo "selected";}?>>2029</option>

                      <option value="2030" <?PHP if (($academic_end == "2030")) { echo "selected";}?>>2030</option>

                      <option value="2031" <?PHP if (($academic_end == "2031")) { echo "selected";}?>>2031</option>

                      <option value="2032" <?PHP if (($academic_end == "2032")) { echo "selected";}?>>2032</option>

                      <option value="2033" <?PHP if (($academic_end == "2033")) { echo "selected";}?>>2033</option>

                      <option value="2034" <?PHP if (($academic_end == "2034")) { echo "selected";}?>>2034</option>

                      <option value="2035" <?PHP if (($academic_end == "2035")) { echo "selected";}?>>2035</option>

                      <option value="2036" <?PHP if (($academic_end == "2036")) { echo "selected";}?>>2036</option>

                      <option value="2037" <?PHP if (($academic_end == "2037")) { echo "selected";}?>>2037</option>

                      <option value="2038" <?PHP if (($academic_end == "2038")) { echo "selected";}?>>2038</option>

                      <option value="2039" <?PHP if (($academic_end == "2039")) { echo "selected";}?>>2039</option>

                      <option value="2040" <?PHP if (($academic_end == "2040")) { echo "selected";}?>>2040</option>

                      <option value="2041" <?PHP if (($academic_end == "2041")) { echo "selected";}?>>2041</option>

                      <option value="2042" <?PHP if (($academic_end == "2042")) { echo "selected";}?>>2042</option>

                      <option value="2043" <?PHP if (($academic_end == "2043")) { echo "selected";}?>>2043</option>

                      <option value="2044" <?PHP if (($academic_end == "2044")) { echo "selected";}?>>2044</option>

                      <option value="2045" <?PHP if (($academic_end == "2045")) { echo "selected";}?>>2045</option>

                      <option value="2046" <?PHP if (($academic_end == "2046")) { echo "selected";}?>>2046</option>

                      <option value="2047" <?PHP if (($academic_end == "2047")) { echo "selected";}?>>2047</option>

                      <option value="2048" <?PHP if (($academic_end == "2048")) { echo "selected";}?>>2048</option>

                      <option value="2049" <?PHP if (($academic_end == "2049")) { echo "selected";}?>>2049</option>

                      <option value="2050" <?PHP if (($academic_end == "2050")) { echo "selected";}?>>2050</option>

                    </select>

              

             

             Eg: 2016 - 2017</td>

                </tr>



                <tr valign="middle" > 
                  <td align="right" valign="top">Status:</td>
                  <td align="left" valign="top"> <input name="is_active" type="radio" value="Y" checked <?php if($is_active=='Y'){echo "checked";}?>>
                    Yes 
                    <input type="radio" name="is_active" value="N" <?php if($is_active=='N'){echo "checked";}?>>
                    No </td>
                </tr>
                <tr valign="middle" > 



                  <td colspan="2" align="center"> 



                    <a href="academic_year_management.php?mid=1&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&sort=<?=$sort?>&by=<?=$by?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&tab=user&subtab=academic_year" class="detailLinkColor"> 



                    Back</a>&nbsp; 



                    <input name="Reset" type="reset" class="flat" id="Reset" value="Reset Values"> 



                    <input name="ssubmit" type="submit" class="flat" id="ssubmit"  value="Save Changes"> 



                    <input name="ssubmit" type="hidden" id="ssubmit" value="editNews"> 



                    <input name="sid" type="hidden" id="sid" value="<?=$sid?>"> 



                    <input name="mid" type="hidden" id="mid" value="1"> <input name="offset" type="hidden" id="offset" value="<?=$offset;?>"> 



                    <input name="q" type="hidden" id="q" value="<? echo htmlspecialchars($q);?>"> 



                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>"> 



                    <input name="field" type="hidden" id="field" value="<?=$field?>"> 



                    <input name="LIKE" type="hidden" id="LIKE" value="<?=$LIKE?>">



                    <input name="tab" type="hidden" value="user">



                    <input name="subtab" type="hidden" value="academic_year">



                    </td>



                </tr>



                <tr valign="middle" > 



                  <td colspan="2" align="center"> </tr>



              </table>



          </form></td>



        </tr>



      </table>



      <br>



     </td>



  </tr>



</table>

<?php }if($mid=="4"){ ?>



<table width="100%"  border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">



  <tr> 



    <td height="30" valign="center" class="adminDetailHeading">&nbsp;Academic Year(s)  Management 



    [Del Confirmation]</td>



  </tr>



  <tr align="center" > 



    <td valign="top" > <br>



      <span class="msgColor"> Are you sure you want to delete the selected Academic Year(s)?</span> 



      <br> 



      <table width="98%" border="0" cellspacing="0" cellpadding="0">



        <tr> 



          <td align="center" valign="top" class="adminDetailHeading"> <table width="100%" border="0" align="center"  cellpadding="2" cellspacing="1" >
              <form name="frmDirectory" onSubmit="return valueCheckedForNewsManagement();">
                <tr > 
                  <td width="24%" align="center" valign="middle"  class="adminDetailHeading">Academic Year</td>
                  <td width="10%" align="center"  class="adminDetailHeading">Date</td>
                  <td width="8%" align="center"  class="adminDetailHeading">Status</td>
                  <td width="9%" height="24" align="center"  class="adminDetailHeading"> 

                    <input name="AC" type="checkbox" id="AC" onClick="CheckAll();" checked> 



                  </td>



                </tr>



			<?php for($i=0;$i<count($EditBox);$i++){
			$Query ="SELECT * FROM ".TBL_ACADEMIC_YEAR." WHERE id=$EditBox[$i]";
			$data = selectFrom($Query);

				$id = $data["id"];
				$academic_year = $data["academic_start"]." - ".$data["academic_end"] ; 
				$is_active = $data["is_active"];
			?>

                <tr valign="middle" bgcolor="#FFFFFF"> 
                  <td height="20" align="left">&nbsp;<? echo $academic_year;?></td>
                  <td align="center"><? echo date("d M, Y ",strtotime($data["added_date"]));?></td>

                  <td height="20" align="center"> 
                     <?php 



		  if($is_active == 'Y'){?>



                    <img src="<?=IMG_PATH?>/yes.gif"> 



                    <?php } else {?>



                    <img src="<?=IMG_PATH?>/no.gif"> 



                    <?php }  ?></td>



                  <td height="20" align="center"> <input name="EditBox[]" type="checkbox" id="EditBox[]" onClick="UnCheckAll();" value="<?=$id?>" checked> 



                  </td>



                </tr>



                <?php }?>



                <tr> 



                  <td height="20" colspan="8" align="center" valign="middle" bgcolor="#FFFFFF" > 



                    <input name="Button" type="button" class="flat" id="hide" value="I am not sure" onClick="javascript:history.back();"> 



                    <input name="show" type="submit" class="flat" id="show" value="Yes I am sure"> 



                    <input name="sid" type="hidden" id="sid" value="<?=$sid;?>"> 



                    <input name="offset" type="hidden" id="offset" value="<?=$offset;?>"> 



                    <input name="mid" type="hidden" id="mid" value="1"> <input name="ssubmit" type="hidden" id="show" value="newsDelConfirmation"> 



                    <input name="q" type="hidden" id="q" value="<?php echo htmlspecialchars($q);?>"> 



                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>"> 



                    <input name="by" type="hidden" id="by" value="<?=$by?>"> <input name="field" type="hidden" id="field" value="<?=$field?>"> 



                    <input name="LIKE" type="hidden" value="<?=$LIKE?>">



                    <input name="tab" type="hidden" value="user">



                    <input name="subtab" type="hidden" value="academic_year">



                    



                     </td>



                </tr>



              </form>



            </table></td>



        </tr>



      </table></tr>



</table>



<?php } ?>



<DIV id=dek></DIV>



<SCRIPT type=text/javascript src="<?=JS_PATH?>/popupscript.js" ></SCRIPT>



<?php include("../footer.php"); ?>



