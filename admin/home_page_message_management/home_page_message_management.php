<?php	
	include ($_SERVER["DOCUMENT_ROOT"]."/aqdar/includes/config.inc.php");
	$sid = $_REQUEST["sid"];
	if (!loggedUser($sid)) { 
		redirect (HOST_URL ."/admin/include/messages.php?msg=relogin");
		exit();
	}
	
	mysql_query("SET NAMES 'utf8'");
	mysql_query('SET CHARACTER SET utf8');
	
	$LIB_CLASSES_FOLDER = $_SERVER['DOCUMENT_ROOT']."/aqdar/lib/classes/";
	include($LIB_CLASSES_FOLDER."Paging.php");

	$q = stripslashes($q);	
	
	/*	INSERT RECORD	*/
	if($ssubmit=="insert_record") {
		
		$qry = "	
			INSERT INTO ".TBL_HOME_PAGE_MESSAGE." (
				`title` ,
				`message` ,
				`message_ar` ,
				`is_active` ,
				`added_date`
				)
				VALUES (
				'$title', '$message ', '$message_ar', 'Y', NOW()
				)";
	
		$MSG = "Information has been saved successfully.";
		insertInto($qry);
		//echo $qry."<br><br>";
	}
	
	/*	EDIT RECORD	*/
	if($ssubmit=="editRecord") {
		
			if (trim($message_) != "") {
				$message = addslashes($message_);
			} else {
				$message = addslashes($message);
			}
			
			if (trim($message_ar_) != "") {
				$message_ar = addslashes($message_ar_);
			} else {
				$message_ar = addslashes($message_ar);
			}

			$cond = "";
			$qry = "UPDATE ".TBL_HOME_PAGE_MESSAGE." SET
				`title` = '$title', 
				`message` = '$message',
				`message_ar` = '$message_ar',
				`is_active` = '$is_active'
				";
			
			$qry .= $cond." WHERE id='$id'";
			//echo $qry;
			update($qry);
			$MSG="Information has been updated successfully.";
		$mid = 1;
	}
	
	/*	ACTIVATE/INACTIVATE RECORD	*/
	if($ssubmit=="recordActiveInactive") {
		if(isset($show)){
				for ($j=0; $j<count($EditBox); $j++){ 
					$Querry = "UPDATE ". TBL_HOME_PAGE_MESSAGE ." SET is_active='Y' WHERE id='$EditBox[$j]'";			
					update($Querry);
					$mid=1;	
					$MSG = "Selected record(s) have been activated successfully.";
				}
		}
		if(isset($hide)){
				for ($j=0; $j<count($EditBox); $j++){ 
					$Querry = "UPDATE ". TBL_HOME_PAGE_MESSAGE ." SET is_active='N' WHERE id='$EditBox[$j]'";			
					update($Querry);
					$mid=1;	
					$MSG = "Selected record(s) have been deactivated successfully.";
				}
			}
		if(isset($delete)){$mid = 4;}
	}
	
	/* DELETE RECORD */
	if($ssubmit == "recordDel"){
		for($i=0;$i<count($EditBox);$i++){
			$id = $EditBox[$i];

			deleteFrom("DELETE FROM ".TBL_HOME_PAGE_MESSAGE." WHERE `id` = '$EditBox[$i]'");
			$mid = 1;
			$MSG = "Selected record(s) has been deleted successfully.";
		}
	}

	if ($by == "") {
		$by = "ASC";
	}
?>
<html>
<title>Admin (Home Page Message Management)</title>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<?=HOST_URL?>/admin/css/interface.css" type=text/css rel=stylesheet>
<script type="text/javascript" src="<?=JS_PATH?>/nice_edit/nicEdit.php"></script>
<script language="javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script>

<style type="text/css">
	select {
		width:200px;
	}

	.col_head {
		color:#005B90;
	}
	
	.input_text {
		border:1px solid #CCC;
		width: 220px;
		height:22px;
	}
</style>
<script  src="<?=JS_PATH?>/jquery-1.3.2.min.js"></script>
<script language="JavaScript">
	
	$(document).ready(function(){
	});
	
	/* CUSTOM JS - MODIFY AS PER NEED */
	function valueCheckedForUsersManagement(){
		var ml = document.frmDirectory;
		var len = ml.elements.length;
		for (var i = 0; i < len; i++){
			if (document.frmDirectory.elements[i].checked){
				return true;
			}
		}
		 alert ("Select at least one record.");
		 return false;
	}
	
	function CheckAll(){
		var ml = document.frmDirectory;
		var len = ml.elements.length;
		if (document.frmDirectory.AC.checked==true) {
			 for (var i = 0; i < len; i++) {
				document.frmDirectory.elements[i].checked=true;
			 }
		} else {
			  for (var i = 0; i < len; i++)  {
				document.frmDirectory.elements[i].checked=false;
			  }
		}
	}
	
	function UnCheckAll() {
		var ml = document.frmDirectory;
		var len = ml.elements.length;
		var count=0; var checked=0;
			for (var i = 0; i < len; i++) {	       
				if ((document.frmDirectory.elements[i].type=='checkbox') && (document.frmDirectory.elements[i].name != "AC")) {
					count = count + 1;
					if (document.frmDirectory.elements[i].checked == true){
						checked = checked + 1;
					}
				}
			 }
			 
		if (checked == count) {
			 document.frmDirectory.AC.checked = true;
		} else {
			document.frmDirectory.AC.checked = false;
		}
	}
	
	function validateForm() {
		
		if(isTitle()) {
				return true;
		} else {
			return false;
		}
	}
	
	function isTitle() {
		var str = document.frmRecord.title.value;
		if (str == "") {
			alert("\nThe Title field is blank. Please write your Title.");
			document.frmRecord.title.value="";
			document.frmRecord.title.focus();
			return false;
		}
		if (!isNaN(str)) {
			alert("\nPlease write your Title.");
			document.frmRecord.title.value="";
			document.frmRecord.title.select();
			document.frmRecord.title.focus();
			return false;
		}
		for (var i = 0; i < str.length; i++) {
			var ch = str.substring(i, i + 1);
			if  ((ch <"A" || ch > "z" ) && (ch !=" ")){
				alert("\n Please enter valid Title.");
				document.frmRecord.title.select();
				document.frmRecord.title.focus();
				return false;
			}
		}
		return true;
	}
	
	function isLastName() {
		var str = document.frmRecord.title.value;
		if (str == "") {
			alert("\nThe Last name field is blank. Please write your Last name.");
			document.frmRecord.title.value="";
			document.frmRecord.title.focus();
			return false;
		}
		if (!isNaN(str)) {
			alert("\nPlease write your Last name.");
			document.frmRecord.title.value="";
			document.frmRecord.title.select();
			document.frmRecord.title.focus();
			return false;
		}
		for (var i = 0; i < str.length; i++) {
			var ch = str.substring(i, i + 1);
			if  ((ch <"A" || ch > "z" ) && (ch !=" ")){
				alert("\n Please enter valid Last name.");
				document.frmRecord.title.select();
				document.frmRecord.title.focus();
				return false;
			}
		}
		return true;
	}
	
	
	function isUserType() {
		var str = document.frmRecord.user_type.value;
		if (str == "") {
			alert("\nPlease select user type.");
			document.frmRecord.user_type.focus();
			return false;
		}
		return true;
	}
	
	function isEmail() {
		var str = document.frmRecord.procedure_name_ur.value;
		if (str == "") {
			alert("\nThe Email field is blank.Please enter your valid Email.");
			document.frmRecord.procedure_name_ur.focus();
			return false;
		}
		if (!isNaN(str)) {
			alert("\nPlease write your correct Email address");
			document.frmRecord.procedure_name_ur.select();
			document.frmRecord.procedure_name_ur.focus();
			return false;
		}
		if(str.indexOf('@', 0) == -1) {
			alert("\nIt seems that your procedure_name_ur address is not valid.");
			document.frmRecord.procedure_name_ur.select();
			document.frmRecord.procedure_name_ur.focus();
			return false;
		}
		return true;
	}
	
	function isUserID() {
		var str = document.frmRecord.id.value;
		if ( str=="" ) {
			alert("\nThe User-ID is blank. Please write your User-ID.");
			document.frmRecord.id.focus();
			return false;
		}
		if (str.length < 7 ) {
			alert("\nThe User-ID should be greater than 5 Characters.");
			document.frmRecord.id.focus();
			document.frmRecord.id.select();
			return false;
		}
			
		if (!isNaN(str)) {
			alert("\nThe User-ID have only letters & digits, Please re-enter your User-ID");
			document.frmRecord.id.select();
			document.frmRecord.id.focus();
			return false;
		}
	
		for (var i = 0; i < str.length; i++) {
			var ch = str.substring(i, i + 1);
			if  ((ch < "a" || ch > "z") && (ch < "0" || "9" < ch) ) {
				alert("\nThe User-ID have only letters in lower case & digits, Please re-enter your User-ID");
				document.frmRecord.id.select();
				document.frmRecord.id.focus();
				return false;
			}
		}
		return true;
	}
	
	function isPassword() {
		var str = document.frmRecord.password.value;
		if (str == "") {
			alert("\nThe Password field is blank. Please enter Password.");
			document.frmRecord.password.focus();
			return false;
		}
			if (str.length < 7) {
			alert("\nThe Password should be greater than 6 Characters.");
			document.frmRecord.password.focus();
			document.frmRecord.password.select();
			return false;
		}
		return true;
	}
	
	function isRetypePassword() {
		var str = document.frmRecord.confirm_password.value;
		if (str == "") {
			alert("\nThe Confirm Password field is blank. Please retype password.");
			document.frmRecord.confirm_password.focus();
			return false;
		}
		return true;
	}
	
	function isPasswordSame() {
		var str1 = document.frmRecord.password.value;
		var str2 = document.frmRecord.confirm_password.value;
		if (str1 != str2) {
			alert("\nPassword mismatch, Please retype same passwords in both fields.");
			document.frmRecord.confirm_password.focus();
			return false;
		}
		return true;
	}
	
	function show_page_en() {
		if (document.getElementById("message_hidden") != null) {
			var data = document.getElementById("message_hidden").innerHTML;
			document.getElementById("message_container").innerHTML = 
			'<textarea name="message_" id="message_" style="border:1px solid #CCC" cols="100" rows="10">'+data+'</textarea>';
		} else {
			document.getElementById("message_container").innerHTML = 
			'<textarea name="message_" id="message_" style="border:1px solid #CCC" cols="100" rows="10"></textarea>';
		}
	}
	function hide_page_en() {
		document.getElementById("message_container").innerHTML = '';
	}
	
	

	function show_page_ar() {
		if (document.getElementById("message_ar_hidden") != null) {
			var data = document.getElementById("message_ar_hidden").innerHTML;
			document.getElementById("message_ar_container").innerHTML = 
			'<textarea name="message_ar_" id="message_ar_" style="border:1px solid #CCC" cols="100" rows="10">'+data+'</textarea>';
		} else {
			document.getElementById("message_ar_container").innerHTML = 
			'<textarea name="message_ar_" id="message_ar_" style="border:1px solid #CCC" cols="100" rows="10"></textarea>';
		}
	}
	function hide_page_ar() {
		document.getElementById("message_ar_container").innerHTML = '';
	}
	
	

	function show_page_ur() {
		if (document.getElementById("procedure_content_ur_hidden") != null) {
			var data = document.getElementById("procedure_content_ur_hidden").innerHTML;
			document.getElementById("procedure_content_ur_container").innerHTML = 
			'<textarea name="procedure_content_ur_" id="procedure_content_ur_" style="border:1px solid #CCC" cols="100" rows="10">'+data+'</textarea>';
		} else {
			document.getElementById("procedure_content_ur_container").innerHTML = 
			'<textarea name="procedure_content_ur_" id="procedure_content_ur_" style="border:1px solid #CCC" cols="100" rows="10"></textarea>';
		}
	}
	function hide_page_ur() {
		document.getElementById("procedure_content_ur_container").innerHTML = '';
	}

</script>

</head>
<?php if (!$offset || $offset<0)  { $offset =0;} ?>
<?php if (!$LIKE)  { $LIKE = "LIKE";} ?>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<!--	DEFAULT FIRST SCREEN	-->
<?php if($mid=="1"){ ?>
<table width="100%" height="570" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr>
    <td height="30" valign="center" class="adminDetailHeading">&nbsp;Home Page Message Management </td>
  </tr>
  <tr align="center" >
    <td height="538" align="center" valign="top" >
     <br>
    	<div style="text-align:center; font-weight:bold; font-size:18px;">Home Page Message</div>
     
      <span class="msgColor"><?php echo "<br>".$MSG;?></span><br>
      <?php 		
		$q = addslashes($q);
		$CountRec = "SELECT * FROM ".TBL_HOME_PAGE_MESSAGE." WHERE 1 ";		
		$Query = "SELECT * FROM ".TBL_HOME_PAGE_MESSAGE." WHERE 1 ";
		

		if($field && !empty($q)){	    	
			if($LIKE=="LIKE"){
					$CountRec .= " AND $field $LIKE '%$q%' ";
					$Query .= " AND  $field $LIKE '%$q%' ";
			}else{
					$CountRec .= " AND  $field $LIKE '$q' ";	
					$Query .= " AND  $field $LIKE '$q' ";
			}
		 }	
		  
		 
		 $Query .= " ORDER BY added_date $by ";
		 $Query .=" LIMIT $offset, ".TBL_HOME_PAGE_MESSAGE_PAGING;
		 //echo $Query;
		 //exit();
   	     $q = stripslashes($q);
		 $total_record = CountRecords($CountRec);
		 //echo $Query;
		 $data = SelectMultiRecords($Query);
		?>
      <?php  if ($total_record !=""){?>
      <table width="98%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="18"><span class="msgColor">&nbsp;&nbsp;Note: To view detail 
            click on Title.</span></td>
        </tr>
        <tr>
          <td align="center" valign="top" class="adminDetailHeading"><table width="100%" border="0" align="center"  cellpadding="2" cellspacing="1">
              <form name="frmDirectory" onSubmit="return valueCheckedForUsersManagement()">
                <tr align="center" >
                  <td width="17%" height="24" align="center" class="adminDetailHeading"> Title</td>
                  <td width="25%" align="center" class="adminDetailHeading">Message [en] </td>
                  <td width="25%" align="center" class="adminDetailHeading">Message [ar] </td>
                  <td width="6%" align="center" class="adminDetailHeading">Action</td>
                </tr>
                <?php
				 for($i=0;$i<count($data);$i++){
						$id = $data[$i]["id"];
						$title = $data[$i]["title"];
						$message = $data[$i]["message"];
						$message_ar = $data[$i]["message_ar"];
						$is_active = $data[$i]["is_active"];
						$added_date = $data[$i]["added_date"];
				?>
                <tr valign="middle" style="background-color:#FFF">
                  <td height="20" align="left" >
                  <div style="padding:5px">
                  	<a href="<?=HOST_URL?>/admin/home_page_message_management/home_page_message_management.php?mid=2&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&by=<?=$by?>&id=<?=$id?>" title="Click to view detail" class="detailLinkColor"><?=$title?></a>
                  </div>
                  </td>
                <td align="left" ><?=$message?></td>

                  <td align="left" ><?=$message_ar?></td>
                  <td align="center"><a href="?mid=3&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&by=<?=$by?>&id=<?=$id?>" class="detailLinkColor">Edit</a></td>
                </tr>
                <?php }?>
                <?php /*?><tr>
                  <td height="20" colspan="8" align="center" valign="middle" bgcolor="#FFFFFF"><!--<input name="reset" type="reset" class="flat"  value="Reset">
                    <input name="show" type="submit" class="flat" id="ssubmit" value="Active">
                    <input name="hide" type="submit" class="flat"  value="Deactive">
                    <input name="delete" type="submit" class="flat"  value="Delete">
                    <input name="ssubmit" type="hidden" id="ssubmit" value="recordActiveInactive">
                    <input name="mid" type="hidden" id="mid2" value="1">
                    <input name="sid" type="hidden" id="sid" value="<?=$sid?>">
                    <input name="offset" type="hidden" id="offset" value="<?=$offset?>">
                    <input name="q" type="hidden" id="q" value="<? echo htmlspecialchars($q);?>">
                    <input name="id" type="hidden" value="<?=$id?>">
                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>">
                    <input name="by" type="hidden" id="by" value="<?=$by?>">
                    <input name="field" type="hidden" id="field" value="<?=$field?>">
                    <input name="LIKE" type="hidden" value="<?=$LIKE?>">--></td>
                </tr><?php */?>
                <tr align="right">
                  <td height="20" colspan="5" valign="middle" bgcolor="#FFFFFF"><?php 
					   if ($total_record != "" && $total_record>TBL_HOME_PAGE_MESSAGE_PAGING) {
							$url = "home_page_message_management.php?mid=1&sid=$sid&field=$field&q=$q&LIKE=$LIKE&by=$by";
							$Paging_object = new Paging();
							$Paging_object->paging_new_style_final($url,$offset,$clicked_link,$total_record,TBL_HOME_PAGE_MESSAGE_PAGING);
						}
					?></td>
                </tr>
              </form>
          </table></td>
        </tr>
      </table>
      <br>
      <?php } ?>
      <br>
      <br>
      </td>
  </tr>
</table>
<!--	RECORD DETAILS SCREEN	-->
<?php } if($mid=="2") { 
		$qry = "SELECT * FROM ".TBL_HOME_PAGE_MESSAGE." WHERE id='$id'";
		$data = selectFrom($qry);
		$id = $data["id"];
		$title = $data["title"];
		$message = $data["message"];
		$message_ar = $data["message_ar"];
		$is_active = $data["is_active"];
		$added_date = $data["added_date"];
?>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="1" >
  <tr >
    <td height="22" align="center" valign="middle"  class="adminDetailHeading" > Home Page Message Detail</td>
  </tr>
  <tr >
    <td align="center" valign="top" class="bordLightBlue"><form name="frmRegister" id="frmRegister" method="post" action=""  onSubmit="return validateForm();">
        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >
          <tr >
            <td height="22" align="left" valign="middle"  class="adminDetailHeading">&nbsp;&nbsp; <strong> Home Page Message Detail</strong></td>
          </tr>
          <tr >
            <td align="center" valign="top" class="bordLightBlue"><table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" >
                <tr align="center" valign="top" >
                  <td colspan="2"></td>
                </tr>
                <tr valign="middle" >
                  <td width="20%" align="right">&nbsp;</td>
                  <td width="78%">&nbsp;</td>
                </tr>
                <tr valign="middle" >
                  <td align="right">Title:</td>
                  <td align="left" valign="top"><span >
                    <?=$title?>
                  </span></td>
                </tr>
                <tr valign="middle" >
                  <td align="right">Message [En]:</td>
                  <td align="left" valign="top"><span >
                    <?=$message?>
                  </span></td>
                </tr>
                <tr valign="middle" >
                  <td align="right">Message [Ar]:</td>
                  <td align="left" valign="top"><span >
                    <?=$message_ar?>
                  </span></td>
                </tr>
                <tr valign="middle" >
                  <td align="right">&nbsp;</td>
                  <td align="left" valign="top">&nbsp;</td>
                </tr>
                <tr valign="middle" >
                  <td colspan="2" align="center" height="10"><a href="home_page_message_management.php?mid=1&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&sort=<?=$sort?>&by=<?=$by?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>" class="detailLinkColor">Back </a>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td align="left" valign="middle">&nbsp;</td>
                </tr>
              </table></td>
          </tr>
        </table>
      </form></td>
  </tr>
</table>
<!--	RECORD EDIT SCREEN	-->
<?php }if($mid=="3"){ 
		
		$qry = "SELECT * FROM ".TBL_HOME_PAGE_MESSAGE." WHERE id='$id'";
		// echo $qry;
		$data = selectFrom($qry);
		$id = $data["id"];
		$title = $data["title"];
		$message = $data["message"];
		$message_ar = $data["message_ar"];
		$is_active = $data["is_active"];
		$added_date = $data["added_date"];
?>
<div id="message_hidden" style="display:none"><?=$message?></div>
<div id="message_ar_hidden" style="display:none"><?=$message_ar?></div>
<div id="procedure_content_ur_hidden" style="display:none"><?=$procedure_content_ur?></div>

<table width="100%" height="451" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr>
    <td height="30" valign="center" class="adminDetailHeading">&nbsp;Home Page Message Management 
    [Edit] </td>
  </tr>
  <tr>
    <td align="center" valign="top"><br>
      <span class="msgColor"><?php echo $MSG;?></span> <br>
      <table width="98%" border="0" align="center" cellpadding="0" cellspacing="1" >
        <tr >
          <td height="22" align="center" valign="middle"  class="adminDetailHeading" >&nbsp;Edit 
          Home Page Message Details</td>
        </tr>
        <tr >
          <td align="center" valign="top" class="bordLightBlue"><form name="frmRecord" id="frmRecord" method="post" action=""  onSubmit="return validateForm();" enctype="multipart/form-data">
              <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >
                <tr >
                  <td height="22" align="left" valign="middle"  class="adminDetailHeading" ><strong>&nbsp;Edit Home Page Message Details</strong></td>
                </tr>
                <tr >
                  <td align="center" valign="top" class="bordLightBlue" style="border-bottom:1px solid #FFFFFF"><table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" >
                      <tr align="center" valign="top" >
                        <td colspan="2"></td>
                      </tr>
                      <tr align="left" valign="top" >
                        <td colspan="2"><span class="msgColor">Fields with * are 
                          mandatory.</span></td>
                      </tr>
                       <?php	$qry =  "SELECT * FROM ".TBL_PROCEDURE_TITLES." WHERE is_active='Y' ORDER BY titles_order ASC";		
						$data_ph = SelectMultiRecords($qry);
						?>
                      <?php	
					   	$qry =  "SELECT * FROM ".TBL_PROCEDURE_HEADINGS." WHERE is_active='Y' ORDER BY headings_order ASC";		
						$data_ph = SelectMultiRecords($qry);
								//print_r($data_ph);
						?>
                     
                      <tr valign="middle">
                        <td align="right">&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr valign="middle">
                        <td align="right"><span class="msgColor">*</span>&nbsp;Title:</td>
                        <td><input name="title" type="text" class="flat" id="title" value="<?=$title;?>" tabindex="1" ></td>
                      </tr>
                      <tr valign="middle">
                        <td width="20%" align="right">Message [En]:</td>
                        <td width="78%"><textarea name="message" id="message" cols="100" rows="10" class="flat">
					  	<?=$message?>
                        </textarea></td>
                      </tr>
                    <tr valign="middle" >
                      <td height="26" align="right">&nbsp;</td>
                      <td align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td align="left" valign="top"></td>
                        </tr>
                        <tr>
                          <td align="left" valign="top"><span onClick="show_page_en()" style="color: blue;">Add Code</span>&nbsp;&nbsp;&nbsp;&nbsp;<span onClick="hide_page_en()" style="color: blue;">Hide Code</span></td>
                        </tr>
                        <tr>
                      <td align="left" valign="top" id="message_container">&nbsp;</td>
                    </tr>
                  </table></td>
                </tr>
                
                 <tr valign="middle">
                  <td width="20%" align="right">Message [Ar]:</td>
                  <td width="78%"><textarea name="message_ar" id="message_ar" cols="100" rows="10" class="flat"><?=$message_ar?></textarea></td>
                </tr>
                <tr valign="middle" >
                  <td height="26" align="right">&nbsp;</td>
                  <td align="left" valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td align="left" valign="top"></td>
                      </tr>
                    <tr>
                      <td align="left" valign="top"><span onClick="show_page_ar()" style="color: blue;">Add Code</span>&nbsp;&nbsp;&nbsp;&nbsp;<span onClick="hide_page_ar()" style="color: blue;">Hide Code</span></td>
                      </tr>
                    <tr>
                      <td align="left" valign="top" id="message_ar_container">&nbsp;</td>
                      </tr>
                    </table></td>
                </tr>
                
                      <tr>
                        <td>&nbsp;</td>
                        <td align="left" valign="middle">&nbsp;</td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td align="left" valign="middle"><a href="home_page_message_management.php?mid=1&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&sort=<?=$sort?>&by=<?=$by?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>" class="detailLinkColor">Back</a>&nbsp;&nbsp;
                          <input type="hidden" name="id" value="<?=$id?>">
                          <input type="reset" name="Reset" value="Reset" class="flat">
                          <input type="submit" name="Save" value="Save" class="flat">
                          <input name="ssubmit" type="hidden" id="ssubmit" value="editRecord">
                          <input name="sid" type="hidden" value="<?=$sid?>">
                          <input name="mid" type="hidden" value="1">
                          <input name="offset" type="hidden" id="offset" value="<?=$offset;?>">
                          <input name="q" type="hidden" id="q" value="<?php echo htmlspecialchars($q);?>">
                          <input name="sort" type="hidden" id="sort" value="<?=$sort?>">
                          <input name="by" type="hidden" id="by" value="<?=$by?>">
                          <input name="field" type="hidden" id="field" value="<?=$field?>">
                          <input name="LIKE2" type="hidden" value="<?=$LIKE?>"></td>
                      </tr>
                    </table></td>
                </tr>
              </table>
            </form></td>
        </tr>
      </table>
      <p><br>
      </p></td>
  </tr>
</table>
<!--	RECORD DELETE SCREEN	-->
<?php }if($mid=="4"){ ?>
<table width="100%" height="380" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr>
    <td height="30" valign="center" class="adminDetailHeading">&nbsp;Home Page Message Management 
    [Del Confirmation]</td>
  </tr>
  <tr align="center" >
    <td valign="top" ><br>
      <span class="msgColor"> Are you sure you want to delete the selected record(s) </span> <br>
      <table width="98%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center" valign="top" class="adminDetailHeading"><table width="100%" border="0" align="center"  cellpadding="2" cellspacing="1" >
              <form name="frmDirectory" onSubmit="return valueCheckedForUsersManagement();">
                <tr >
                  <td width="17%" align="center"  class="adminDetailHeading">Title</td>
                  <td width="25%" align="center"  class="adminDetailHeading">Message [en] </td>
                  <td width="25%" align="center"  class="adminDetailHeading">Message [ar] </td>
                  <td width="6%" height="24" align="center"  class="adminDetailHeading"><input name="AC" type="checkbox" id="AC" onClick="CheckAll();" checked></td>
                </tr>
                <?php 
			for($i=0;$i<count($EditBox);$i++){
				$qry ="SELECT * FROM ".TBL_HOME_PAGE_MESSAGE." WHERE id='$EditBox[$i]'";
				//echo $qry."<br />";
				
				$data = selectFrom($qry);
				$id = $data["id"];
				$title = $data["title"];
				$message = $data["message"];
				$message_ar = $data["message_ar"];
				$is_active = $data["is_active"];
				$added_date = $data["added_date"];
				
				?>
                <tr valign="middle" bgcolor="#FFFFFF">
                  <td align="center"><?=$title?>&nbsp;</td>
                  <td align="left"><?=$message?></td>
                  <td align="left"><?=$message_ar?></td>
                  <td height="20" align="center"><input name="EditBox[]" type="checkbox" id="EditBox[]" onClick="UnCheckAll();" value="<?=$id?>" checked></td>
                </tr>
                <?php }?>
                <tr>
                  <td height="20" colspan="5" align="center" valign="middle" bgcolor="#FFFFFF" ><input name="Button" type="button" class="flat" id="hide" value="I am not sure" onClick="javascript:history.back();">
                    <input name="show" type="submit" class="flat" id="show" value="Yes I am sure">
                    <input name="sid" type="hidden" id="sid" value="<?=$sid;?>">
                    <input name="offset" type="hidden" id="offset" value="<?=$offset;?>">
                    <input name="mid" type="hidden" id="mid" value="1">
                    <input name="ssubmit" type="hidden" id="show" value="recordDel">
                    <input name="q" type="hidden" id="q" value="<?php echo htmlspecialchars($q);?>">
                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>">
                    <input name="by" type="hidden" id="by" value="<?=$by?>">
                    <input name="field" type="hidden" id="field" value="<?=$field?>">
                    <input name="LIKE" type="hidden" value="<?=$LIKE?>"></td>
                </tr>
              </form>
          </table></td>
        </tr>
      </table>
  </tr>
</table>
<?php } ?>

<script language="javascript">

	var host = '<?=HOST_URL?>';
	var lan = 'ar';
	
	function get_room_names_ajax(court_name) {
		var xmlHttp, rnd, url;
		rnd = Math.floor(Math.random()*11);
		
		try{		
			xmlHttp = new XMLHttpRequest(); 
		}catch(e) {
			try{
				xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
			}catch(e) {
				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
		}
		
		//AJAX response
		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
				//alert(xmlHttp.responseText);
				$("#court_name_container").html(xmlHttp.responseText);
			}
		}
			
		//Sending AJAX request
		url = host + "/"+lan+"/court/get_room_names_ajax/"+court_name;
		xmlHttp.open("POST",url,true);
		xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlHttp.send("rnd="+rnd);
		
	}//function
</script>
</body>
</html>