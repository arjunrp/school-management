<?php	include ($_SERVER["DOCUMENT_ROOT"]."/aqdar_v3/includes/config.inc.php");
//upload.php
$output_dir = "uploads/";

$max_x = 150;
$max_y = 150;
	 
$upload_type = $_REQUEST["upload_type"];
if(isset($_FILES["myfile"]))
{
	$error = $_FILES["myfile"]["error"];
	if(!is_array($_FILES["myfile"]["name"])) //single file
	{
 	 	$file_name_original = $_FILES["myfile"]["name"];
		$Ext = strchr($file_name_original,".");
		$Ext = strtolower($Ext);
		$file_name_updated_ = getMD5_ID();
		$file_name_updated = $file_name_updated_.$Ext;
		$file_name_original_temp = $_FILES["myfile"]["tmp_name"];
		$file_type = $_FILES["myfile"]["type"];
		$file_size = $_FILES["myfile"]["size"];
		
		if ($upload_type == "audio" && strtolower($Ext) != ".mp4") {
			$arr["error_msg"] = "For audio message please upload files having .mp4 file format.";
			$arr["error"] = "1";
			echo json_encode($arr);
			exit();
		}
		
		//upload Large Image
		$copy = file_copy("$file_name_original_temp", UPLOADS_PATH."/", "$file_name_updated", 1, 1);
		$tbl_uploads_id = getMD5_ID();
	
		$file_name_updated_thumb = $file_name_updated_."_thumb".$Ext;
		$source = UPLOADS_PATH."/".$file_name_updated;
		$destination = UPLOADS_PATH."/".$file_name_updated_thumb;
		if (strtolower($Ext)== ".jpg" || strtolower($Ext)== ".jpeg" || strtolower($Ext)== ".png") {
			generate_thumb($source, $destination, $max_x, $max_y);
		}
		
		$arr["file_name_original"] = $file_name_original;		
		$arr["file_name_updated"] = $file_name_updated;
		$arr["file_name_updated_thumb"] = $file_name_updated_thumb;
		$arr["file_type"] = $file_type;
		$arr["file_size"] = $file_size;
	    echo json_encode($arr);
		exit();
	}
	echo "error";
} 
echo "error";
exit();
?>