<?php

define ("LINK_PAGES","10;");

class Paging  {

 function paging_new_style_final($url,$offset,$clicked_link,$total_records,$total_records_per_page)   { 
	 		
	     if($clicked_link=='')  {
		    $clicked_link = 1;  //For the first time to enter
	     }       
		   
        $LIMIT_PER_PAGE = $total_records_per_page;                      //This constant will contain how many records will be shown on the current page.
        $LINK_PAGES = LINK_PAGES;                                   //This constant will show how many links will be available at a time at each page.
        $current_page_number  = $offset / $LIMIT_PER_PAGE;              //This constant will provide the current page number;
        $current_page_number  = intval($current_page_number);           //This function will provide the integer part in case of wrong offset.
	    $current_page_number += 1;                                      //This will show the actual page number.
		$next_page_number     = $current_page_number + 1;               //This will contain the next page number.
		$back_page_number     = $current_page_number - 1;               //This will contain the old page number.
		$next_clicked_link    = $clicked_link + 1;                      //This will store the next link clicked value.
		$back_clicked_link    = $clicked_link - 1;                      //This will store the back link clicked value.
		$available_links      = ceil($total_records / $LIMIT_PER_PAGE); //This will define the total links.
		$actual_page_number   = $current_page_number;                   //This variable will save the number as in calculating from offset.
    	$actual_clicked_link  = $clicked_link;                          //This variable will save the number as in coming from address bar.
		$actual_offset        = $offset;                                //This variable will store the original offset of the page.

		           //For a Previous Page Procedure
	    if($back_clicked_link < 1 )
		  {
		    $back_clicked_link = 1;                                     //Less than 1 is not allowed.
		  }
		if($back_page_number < 1 )
		 {
		    $back_page_number = 1;                                      // Less than 1 is not allowed.
		 }	    
		                      
							  /*********** Begin Save Actual Page Number & Clicked_link ***********/
		                          
							  /*********** End Save Actual Page Number & Clicked_link ***********/
							  
	    if(!isset($offset)|| $offset=="" || $offset < 0)                // If you are coming for the first time on the concerned page.
            {
		      $offset=0;                                                //It is basically an offset address of the records to be shown.
		    }	
	           $end=$offset-0;                                          //This constant is another copy of offset but used in limit case.
	           $this_is=$end+$LIMIT_PER_PAGE;                           //This will be used at the end to check the Next Link of the page either it should be hyperlinked or not.
	           $back=$end- $LIMIT_PER_PAGE;                             //Similar to the next case, it will be used for the Previous Link either it should be hyperlinked or non-hyperlink.
	           $next=$end+ $LIMIT_PER_PAGE;                             //This will contain the offset of the next link following the current link.
          
           /************* Supporting Area Either Accessing Page From First or Last ***********/
		     if($clicked_link==1 && $current_page_number != 1)
		       {  
			     $link_access='back';
			   }
		     else
		       {
			     $link_access='forward';
			   }  
 
     echo  "<table  width=\"100%\">";
     echo  "<tr><td align=right>";

                      /*  #################-----First Link Area Begin------###############  */
	 if($offset!=0)                                         
	     {
		  echo "&nbsp;<a style='text-decoration:none' href=\"$url&offset=0&clicked_link=1\" title=\"Records&nbsp;From&nbsp;1&nbsp;To&nbsp;$LIMIT_PER_PAGE&nbsp;of&nbsp;$total_records\"><span style=font-size:12px; color:#3d5e70;>First&nbsp;&nbsp;</span></a>";
		 } 
	 else
	     {
	      echo "&nbsp;<span style=font-size:12px; color:#3d5e70;>First&nbsp;&nbsp;</span>"; 
		 } 
                     /*  #################-----First Link Area End------###############  */		 
		 
                    /*  #################-----Previous Link Area Begin------###############  */
					
					             /*   Start To calculate the title tooltips     */
			             //No Need of Check less than 0, because it will be alreadly de-active. (optionally Given).
				 if($offset != 0 )
				   {			  
					$back_title_start = $offset-$LIMIT_PER_PAGE + 1;           //Begin Record number in the tool tip.
					$back_title_end   = $back_title_start + $LIMIT_PER_PAGE-1; //End Record Number in the tool tip.
		           }  
						         /*   End To calculate the title tooltips       */
			    if($actual_clicked_link == $LINK_PAGES && $link_access=='forward' && ($actual_page_number + $LINK_PAGES - 1) <= $available_links )
				   {
				     //$link_access = 'back';
				   //$back_clicked_link = 1;
				   }
				if($clicked_link == $LINK_PAGES && $actual_page_number != $available_links && ($actual_page_number + $LINK_PAGES) > $available_links )
			        {  
			          $local_count = 0;
			          $i = $actual_page_number + $LINK_PAGES;
			       while( $i != $available_links)
				         {
					       $local_count++;
						   $i--;
					     }
			         //  echo "the local count is " .$local_count;
				        $back_clicked_link = $LINK_PAGES - $local_count + 1;
				  }	
					 		     
			if( $clicked_link == 1 && $current_page_number >= $LINK_PAGES )
		    	{
				  $back_clicked_link = $LINK_PAGES - 1;
			    }		 
								
						//Check when you click the last clicked number and remaining links are less than $LINK_PAGES
					if($clicked_link == $LINK_PAGES && $actual_page_number != $available_links && ($actual_page_number + $LINK_PAGES) > $available_links )
			   {   $local_count = 0;			   
			        $i = $current_page_number + $LINK_PAGES;
			     while( $i != $available_links)
				      {
					     $local_count++;
						 $i--;
					  }
			       // echo "the local count is " .$local_count;
				    $back_clicked_link = $local_count - 1;
			   }	
				
						//End of the check to be cached
			if($clicked_link==1 && $actual_page_number < $LINK_PAGES)
			   {
			  // echo "Hellow ! This is a special case. I am here";
			   //echo "the clicked link is ".$clicked_link;
			  // echo "the next clicked link is ".$next_clicked_link;	
			  $back_clicked_link = $actual_page_number - 1;
			   }	
			if($back_clicked_link < 1)
				   {
				    $back_clicked_link = 1;
				   }				 
			
								 

    if($back >=0) 
	   {                  
   	    echo "&nbsp;<a style='text-decoration:none' href=\"$url&offset=$back&clicked_link=$back_clicked_link\" title=\"Records From&nbsp;$back_title_start&nbsp;To&nbsp;$back_title_end&nbsp;of&nbsp;$total_records\"><span style=font-size:12px; color:#3d5e70;>Previous&nbsp;&nbsp;</span></a>";
	   } 
	else
	   {
	    echo "&nbsp;<span style=font-size:12px; color:#3d5e70;>Previous&nbsp;&nbsp;</span>"; 
	   } 	

                  /*  #################-----Previous Link Area End------###############  */	  
				  
				  /*  #################-----Dynamic Link Area Begin-----###############  */

		       $current_page_number -= 1;      /*  This is shifted to its original position 
			                                       in order to gets our functionality.    */
			   $initial_value = 0;          //This variable will contain the initial value of y.								   
			
			if($current_page_number < 0)
			 {
			   $current_page_number = 0;       /*  Less than 0 is not applicable.  */
			 }    									   
              $y_value = $current_page_number * $LIMIT_PER_PAGE; //To start actual page links number 
	      if($y_value < 0)
	         {
			   $y_value=0;	                 //Y value mean to calculate the offset of the next page.
			 }  
	          if(($current_page_number==1 && $clicked_link ==1) || ($current_page_number <= $LINK_PAGES && $clicked_link == 1))
			  {   
			     $show_link_numbers = 1;       //This will show the desktop link numbers.
				// echo "The show link numbers is ".$show_link_numbers;
			  }
			 // echo "I Will Be Here";
			  //echo "The clicked link is".$clickec_link;
			  
			if( $actual_clicked_link >= 2 && $actual_clicked_link < $LINK_PAGES) 
			     { 
				// echo "I am Inside";
			       $reduce_page_no = 0;
				   $i= $clicked_link;
				   while($i != 1 )
				      {
				         $reduce_page_no++;
				         $i--;
				      }
				   $show_link_numbers = $current_page_number - $reduce_page_no + 1;
				   $initial_value = ($show_link_numbers - 1) * $LIMIT_PER_PAGE;
			    }
			
			  $current_page_number += 1;    //Original Page Number to be printed
		      $clicked_link_counter = 1;    //This will count all the links.
			  		 
		  if($actual_clicked_link == $LINK_PAGES && $link_access=='forward' && ($actual_page_number + $LINK_PAGES - 1) <= $available_links )	  				
             {
			 $show_link_numbers = $actual_page_number;
			 $initial_value = $actual_offset;
			 }
			 
          if($actual_clicked_link < $LINK_PAGES && $current_page_number > $LINK_PAGES)
		    {   $just_to_count_links =0;
			for($u = $actual_clicked_link; $u >= 1; $u--)
			   {
			     $just_to_count_links++;
			   }
			   //echo "the links are ".$just_to_count_links;
			   $show_link_numbers = $current_page_number - $just_to_count_links + 1;
			   $initial_value = ( $show_link_numbers - 1 ) * $LIMIT_PER_PAGE;   //To calculate the offset of the clicked link.
			}		   	 
				
				//The case when the last link number is clicked and it is the last link number then.
			if( $actual_clicked_link == $LINK_PAGES && $current_page_number == $available_links )
				{
				  $show_link_numbers = $current_page_number - $LINK_PAGES + 1;
				  $initial_value = ( $show_link_numbers - 1 ) * $LIMIT_PER_PAGE;   //To calculate the offset of the clicked link.
				}
				//End of the special case to be created at this time event.
				
			if($clicked_link == $LINK_PAGES && $current_page_number != $available_links && ($current_page_number + $LINK_PAGES) > $available_links )
			   {   $local_count = 0;			   
			        $i = $current_page_number + $LINK_PAGES;
			     while( $i != $available_links)
				      {
					     $local_count++;
						 $i--;
					  }
			       // echo "the local count is " .$local_count;
				    $show_link_numbers = $current_page_number - $local_count + 1;
					$initial_value = ( $show_link_numbers - 1 ) * $LIMIT_PER_PAGE;
			   }	
			         //When you click at the first place.
			if( $clicked_link == 1 && $current_page_number >= $LINK_PAGES )
		    	{
				  $show_link_numbers = $current_page_number - $LINK_PAGES + 1;
				  $initial_value = ( $show_link_numbers - 1 ) * $LIMIT_PER_PAGE;
				  
			    }
			if($clicked_link==1 && $actual_page_number < $LINK_PAGES)
			   {
			  // echo "Hellow ! This is a special case. I am here";
			   //echo "the clicked link is ".$clicked_link;
			  // echo "the next clicked link is ".$next_clicked_link;	
			  $next_clicked_link = $actual_page_number + 1;
			   }	
			if($show_link_numbers < 1)
			   {
			     $show_link_numbers = 1;
				 $initial_value = 0;
			   }		
                
					 //End of the function when you click at first place.
				            /************************End of Special Check******************/
				    
        for($y = $initial_value; $y < $total_records; $y += $LIMIT_PER_PAGE)  // i am writing $y=0 inplace of $y = $y_value
          {
              		   
		   if($clicked_link_counter <= $LINK_PAGES )
			 { 
                 /********Begin Area to calculate the start and last of title 'tooltip'*******/
				           
						   $title_page_start = $y + 1;   //Start title page.
						   $title_page_end   = $y + $LIMIT_PER_PAGE;  //end title page.
						   
			       if($show_link_numbers==$available_links)
				      {    
					    if($title_page_end > $total_records)
						  {
						   $title_page_end=$total_records;  //Maximum title of the last page will be total records.
						  }
					  }

				 /********Close Area to calculate the start and last of title 'tooltip'*******/
				
             if($y <> $end )                   // It will check either it should not be a current page.
		         {
			      echo "<a style='text-decoration:none' href=\"$url&offset=$y&clicked_link=$clicked_link_counter\" title=\"From&nbsp;$title_page_start&nbsp;To&nbsp;$title_page_end&nbsp;of&nbsp;$total_records\"><span style=font-size:12px; color:#3d5e70;>&nbsp;<u>".$show_link_numbers."</u>&nbsp;&nbsp;</span></a>"; 
                 }
		     else         /*  Else case to check if it is the current page, display without active link. */
		        {
			     echo "<span style=font-size:12px; color:#3d5e70;>&nbsp;".$show_link_numbers."&nbsp;&nbsp;</span>";
			    } 
			}                            //End check to control the number of links per page.            
				
			  $show_link_numbers++;      //To get the next link number.
			  $clicked_link_counter++;   //To get next number clicked link number.
	  
		  }          //End of For Loop Brace Here.

				  /*  #################-----Dynamic Link Area End-----###############  */
				  
				  /*  ##################-----Next Link Area Begin-----###############  */  
				  
				   if($actual_clicked_link==2 && $offset==0)
				      {
					    $next_clicked_link = $actual_link; //For the first time to enter in the paging area.
					  }   
                      $next_title_start = $actual_offset + $LIMIT_PER_PAGE + 1;                    //next link page start page
			          $next_title_end   = $next_title_start + $LIMIT_PER_PAGE - 1;           //next link page start page
				   
				   if($next_title_end > $total_records )
				      {
					   $next_title_end = $total_records;
					  }    
                 if($actual_clicked_link == $LINK_PAGES && $link_access=='forward' && ($actual_page_number + $LINK_PAGES - 1) <= $available_links )					  
                     {
	                   $next_clicked_link = 2;
                     }
				if($clicked_link == $LINK_PAGES && $actual_page_number != $available_links && ($actual_page_number + $LINK_PAGES) > $available_links )
			        {  
			          $local_count = 0;
			          $i = $actual_page_number + $LINK_PAGES;
			       while( $i != $available_links)
				         {
					       $local_count++;
						   $i--;
					     }
			         //  echo "the local count is " .$local_count;
				        $next_clicked_link = $LINK_PAGES - $local_count + 2;
				  }	
					 if($next_clicked_link > $LINK_PAGES)
					    {
						$next_clicked_link = $LINK_PAGES;
						}
			if( $clicked_link == 1 && $current_page_number >= $LINK_PAGES )
		    	{
				  $next_clicked_link = 2;
			    }		 
						
						//Check when you click the last clicked number and remaining links are less than $LINK_PAGES
					if($clicked_link == $LINK_PAGES && $actual_page_number != $available_links && ($actual_page_number + $LINK_PAGES) > $available_links )
			   {   $local_count = 0;			   
			        $i = $current_page_number + $LINK_PAGES;
			     while( $i != $available_links)
				      {
					     $local_count++;
						 $i--;
					  }
			       // echo "the local count is " .$local_count;
				    $next_clicked_link = $local_count + 1;
			   }	
				
						//End of the check to be cached
					  				  
	       if($this_is < $total_records)   // Check the validity either it is last page or not.
	         {
			   echo "&nbsp;<a style='text-decoration:none' href=\"$url&offset=$next&clicked_link=$next_clicked_link\" title=\"Records From&nbsp;$next_title_start&nbsp;To&nbsp;$next_title_end&nbsp;of&nbsp;$total_records\"><span style=font-size:12px; color:#3d5e70;>Next&nbsp;&nbsp;</span></a>";
			 }  
	       else
		     {
			   echo "&nbsp;<span style=font-size:12px; color:#3d5e70;>Next&nbsp;&nbsp;</span>"; 
			 }  
			      /*  ###################-----Next Link Area End-----################  */
				  
			      /*  ##################-----Last Link Area Begin-----###############  */				  
                  $Calculate_total_records_of_available_links = $available_links * $LIMIT_PER_PAGE;     
				  $Calculate_total_records_of_available_links = $Calculate_total_records_of_available_links - $LIMIT_PER_PAGE; 
					
					if( $LINK_PAGES > $available_links )
					  {
					    $LINK_PAGES=$available_links;   //To control the link of the last page.
					  }
					  
				 $start_last_title = ( $available_links - 1 ) * $LIMIT_PER_PAGE + 1;	   //Start title of the last link.
				 
					  	
          if($next <= $Calculate_total_records_of_available_links)   /* To check either it is the link of last page or not. */
		     {
	          echo "&nbsp;<a style='text-decoration:none' href=\"$url&offset=$Calculate_total_records_of_available_links&clicked_link=$LINK_PAGES\" title=\"Records&nbsp;From&nbsp;$start_last_title&nbsp;To&nbsp;$total_records&nbsp;of&nbsp;$total_records\"><span style=font-size:12px; color:#3d5e70;>Last&nbsp;&nbsp;</span></a>";
			 }
	      else
		     {
	          echo "&nbsp;<span style=font-size:12px; color:#3d5e70;>Last&nbsp;&nbsp;</span>"; 
			 } 
     		      /*  ###################-----Last Link Area End-----################  */			 

              echo  "</td></tr>";
              echo "</table>";
			   /********************* End of the Display Procedure Function ********************/

   }   //End of paging_new_style_final function.
}//Paging


?>