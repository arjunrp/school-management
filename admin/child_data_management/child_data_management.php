<?php 
    include("../header.php");
	$sid = $_REQUEST["sid"];
	if (!loggedUser($sid)) { 
		redirect (HOST_URL ."/admin/include/messages.php?msg=relogin");
		exit();
	}
	
	
	$tbl_school_id_sess = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
	$LIB_CLASSES_FOLDER = $_SERVER['DOCUMENT_ROOT']."/aqdar/lib/classes/";
	include($LIB_CLASSES_FOLDER."Paging.php");
	
	mysql_query("SET NAMES 'utf8'");
	mysql_query('SET CHARACTER SET utf8'); 	
	
	$max_x = 100;
	$max_y = 100;
	
	/* INSERT RECORD */
	if($_REQUEST['ssubmit']=="insert_record") {

		$child_data_title_en = addslashes($child_data_title_en);
		$child_data_title_ar = addslashes($child_data_title_ar);
		$child_data_text_en = addslashes($child_data_text_en);
		$child_data_text_ar = addslashes($child_data_text_ar);
		
		// Upload logo image
		//print_r($_FILES);
		if (trim($_FILES["child_data_logo"]["name"]) != "") {
			$child_data_logo = $_FILES["child_data_logo"]["name"];
			
			$Ext = strchr($child_data_logo,".");
			$Ext = strtolower($Ext);
			$child_data_logo_3d_ = getMD5_ID();
			$child_data_logo_3d_en = $child_data_logo_3d_."".$Ext;
			$child_data_logo_temp = $_FILES["child_data_logo"]["tmp_name"];
			$copy = file_copy("$child_data_logo_temp", UPLOAD_LOGO_IMG_PATH."/", "$child_data_logo_3d_en", 1, 1);
		}

		$qry = "INSERT INTO ".TBL_CHILD_DATA." (
			`tbl_child_data_id` ,
			`tbl_class_id` ,
			`tbl_student_id` ,
			`child_data_title_en` ,
			`child_data_title_ar` ,
			`child_data_type` ,
			`child_data_logo` ,
			`child_data_text_en` ,
			`child_data_text_ar` ,
			`child_data_order` ,
			`is_active` ,
			`added_date`,
			`tbl_school_id`
			)
			VALUES (
			 '$tbl_child_data_idd', '$tbl_class_id', '$tbl_student_id', '$child_data_title_en', '$child_data_title_ar', '$child_data_type', '$child_data_logo_3d_en', '$child_data_text_en', '$child_data_text_ar', '1000', 'Y', NOW(), '$tbl_school_id_sess'
			)";

		//echo $qry;
		$val = insertInto($qry);
		$MSG="Information saved successfully.";
		$mid=1;
		
		$tbl_child_data_idd = getMD5_ID();
	}
	
	/* EDIT RECORD */
	if($ssubmit=="edit_record") {
		
		$child_data_title_en = addslashes($child_data_title_en);
		$child_data_title_ar = addslashes($child_data_title_ar);
		$child_data_text_en = addslashes($child_data_text_en);
		$child_data_text_ar = addslashes($child_data_text_ar);
		
		$qry_update = "UPDATE ".TBL_CHILD_DATA." SET 
			`tbl_class_id` = '$tbl_class_id',
			`tbl_student_id` = '$tbl_student_id',
			`child_data_title_en` = '$child_data_title_en',
			`child_data_title_ar` = '$child_data_title_ar',
			`child_data_text_en` = '$child_data_text_en',
			`child_data_text_ar` = '$child_data_text_ar',
			`child_data_type` = '$child_data_type',
			`is_active` = '$is_active'
			";
			
		// Upload Logo
		//print_r($_FILES);
		if (trim($_FILES["child_data_logo"]["name"]) != "") {
			$qry = "SELECT child_data_logo FROM ".TBL_CHILD_DATA." WHERE tbl_child_data_id='".$tbl_child_data_id."'";
			//echo $qry."<br>";
			$data_rs = selectFrom($qry);
			$child_data_logo_stored = $data_rs["child_data_logo"];
						
			// Remove image if already available
			if (trim($child_data_logo_stored) != "") {
				$file_path = UPLOAD_LOGO_IMG_PATH."/".$child_data_logo_stored;
				unlink($file_path);
			}
			$child_data_logo = $_FILES["child_data_logo"]["name"];
			$Ext = strchr($child_data_logo,".");
			$Ext = strtolower($Ext);
			$file__updated_2d_ = getMD5_ID();
			$file_updated_2d_en = $file__updated_2d_."".$Ext;
			$child_data_logo_temp = $_FILES["child_data_logo"]["tmp_name"];
			$copy = file_copy("$child_data_logo_temp", UPLOAD_LOGO_IMG_PATH."/", "$file_updated_2d_en", 1, 1);
			$qry_update .= " ,child_data_logo='".$file_updated_2d_en."'";
		}
		
		$qry_update	.= " WHERE tbl_child_data_id ='$tbl_child_data_id'";
		//echo $qry_update;
		update($qry_update);
		$MSG="Information has been updated successfully.";
		$mid = 1;
	}
	
	/* ACTIVATE/INACTIVATE RECORD */
	if($ssubmit=="recordActiveInactive") {
		if(isset($show)){
				for ($j=0; $j<count($EditBox); $j++){ 
					$Querry = "UPDATE ". TBL_CHILD_DATA ." SET is_active='Y' WHERE tbl_child_data_id='$EditBox[$j]'";			
					update($Querry);
					$mid=1;	
					$MSG = "Selected records have been activated successfully.";
				}
		}
		if(isset($hide)){
				for ($j=0; $j<count($EditBox); $j++){ 
					$Querry = "UPDATE ". TBL_CHILD_DATA ." SET is_active='N' WHERE tbl_child_data_id='$EditBox[$j]'";			
					update($Querry);
					$mid=1;	
					$MSG = "Selected records have been deactivated successfully.";
				}
			}
		if(isset($delete)){$mid = 4;}
	}
	
	/* DELETE RECORD */
	if($ssubmit == "recordDelInformation"){
		for($i=0;$i<count($EditBox);$i++){
			
			$qry = "SELECT * FROM ".TBL_UPLOADS." WHERE tbl_item_id='$EditBox[$i]'";
			$data_rs = SelectMultiRecords($qry);
			for($j=0; $j<count($data_rs); $j++) {
				$file__updated = $data_rs[$j]["file__updated"];
				//echo "<br>".UPLOADS_PATH."/".$child_data_logo;
				@unlink(UPLOADS_PATH."/".$file__updated);
			}
			
			$qry = "DELETE FROM ".TBL_UPLOADS." WHERE tbl_item_id='$EditBox[$i]'";
			deleteFrom($qry);						
						
			$qry = "SELECT child_data_logo FROM ".TBL_CHILD_DATA." WHERE tbl_child_data_id='$EditBox[$i]'";
			//echo "<br>".$qry;
			$data_rs = selectFrom($qry);
			$child_data_logo = $data_rs["child_data_logo"];
			@unlink(UPLOADS_PATH."/".$child_data_logo);
						
			deleteFrom("DELETE FROM ".TBL_CHILD_DATA." WHERE `tbl_child_data_id` = '$EditBox[$i]'");
			$mid = 1;
			$MSG = "Selected records have been deleted successfully.";
		}
	}
	
	$ssubmit = "";
	
	//echo $tbl_child_data_id;
	if ($tbl_child_data_idd == "") {
		$tbl_child_data_idd = getMD5_ID();
	}
	
	// In edit mode (mid=3), for uploading we require tbl_child_data_idd varialble
	if ($mid == 3) {
		$tbl_child_data_idd = $tbl_child_data_id;	
	}
	//echo $tbl_child_data_idd;
?>
<link href="<?=HOST_URL?>/admin/css/interface.css" type=text/css rel=stylesheet>
<style type="text/css">
	.input_text {		
			border-color: #888888;
			border-style: solid;
			border-width: 1px;
			width:300px;
	}
	.input_text {		
			border-color: #888888;
			border-style: solid;
			border-width: 1px;
			width:300px;
	}
	.lan_ar {
		text-align:right;
		direction:rtl;
		font-size:13px;
		font-family:Tahoma, Geneva, sans-serif;
	}
	.lan_ar_m {
		margin:0 auto;
		direction:rtl;
		font-size:13px;
		font-family:Tahoma, Geneva, sans-serif;
	}
	.lan_l_ar {
		text-align:left;
		direction:rtl;
		font-size:14px;
		font-family:Tahoma, Geneva, sans-serif;
	}
	
	.lan_r_ar {
		text-align:right;
		direction:rtl;
		font-size:13px;
		font-family:Tahoma, Geneva, sans-serif;
	}
	.clr_red {
		color:red;
	}
	.top_heading {
		font-size:13px;
		font-weight:bold;
	}
	.input_text_ar {
		font-size:13px;
		text-align:right;
		direction:rtl;
		font-family:Tahoma, Geneva, sans-serif;
	}
	#vid_text {
		display:none;
	}
</style>

<link href="http://hayageek.github.io/jQuery-Upload-File/uploadfile.min.css" rel="stylesheet">

<style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url(../images/delete.jpg);
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
</style>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="http://hayageek.github.io/jQuery-Upload-File/jquery.uploadfile.min.js"></script>
<script type="text/javascript" src="<?=JS_PATH?>/jquery.timer.js"></script>
<script>

$(document).ready(function()
{
	var tbl_item_id = '<?=$tbl_child_data_idd?>';

	$("#advancedUpload").uploadFile({
		url:"<?=HOST_URL?>/admin/upload.php",
		fileName:"myfile",
		formData: {"module_name":"child_data","tbl_item_id":tbl_item_id},
		onSubmit:function(files)
		{
			//if ($("#upload_content").val().trim() != "") {alert("The file is already uploaded."); return false;}

			//$("#eventsmessage").html($("#eventsmessage").html()+"<br/>Submitting:"+JSON.stringify(files));
		},
		onSuccess:function(files,data,xhr)
		{
			if (data == "error") {
				alert("Error uploading file. Please try again.");
				return;
			}
			//alert("success");
			//$("#eventsmessage").html(JSON.stringify(data));
			add_uploaded_item(files, data);
		}
	});
});

// tbl_item_id = tbl_uploads_id
function add_uploaded_item(item_, tbl_item_id) {
	//alert(item_+"-"+ tbl_item_id);
	var row_item = document.createElement('div');
	$(row_item).addClass("row_item")
		.attr("id",tbl_item_id)
		.appendTo($("#uploaded_items"))

	var upload_del = document.createElement('div');
	$(upload_del).addClass("upload_del")
		.appendTo($("#"+tbl_item_id)) //main div
		.click(function(){
			delete_file(tbl_item_id);
		})

	var upload_content = document.createElement('div');
	$(upload_content).addClass("upload_content")
		.html(item_)
		.appendTo($("#"+tbl_item_id)) //main div
	
	$("#parenting_video").css("display","none");
	$("#vid_text").css("display","block");

	return;
}

function delete_file(tbl_uploads_id) {
	if (confirm("Are you sure you want to delete?")) {
		delete_file_ajax(tbl_uploads_id)
	}
	return;
}

var connectivity_msg = "Connection timed out. Please try again.";
var connectivity_timeout_time = 10000;
var host = '<?=HOST_URL?>';
/* Function to load a navigation page */
function delete_file_ajax(tbl_uploads_id) {
	//show_loading();
	
	if ($("#child_data_type").val() == "d") {
		alert("Please choose Type to be 'Video' in order to proceed.");
		return;
	}
	var xmlHttp, rnd, url, search_param, ajax_timer;
	rnd = Math.floor(Math.random()*11);
	try{		
		xmlHttp = new XMLHttpRequest(); 
	}catch(e) {
		try{
			xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		}catch(e) {
			xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
			hide_loading();
		}
	}
	
	//AJAX response
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
			ajax_timer.stop();
			var d = document.getElementById("uploaded_items");
			var d_nested = document.getElementById(tbl_uploads_id);
			var throwawayNode = d.removeChild(d_nested);
			if ($("#child_data_type").html().trim() == "v") {
				$("#parenting_video").css("display","block");
				$("#vid_text").css("display","none");
			}
			alert("The file have been deleted successfully.");
		}
	}
	
	ajax_timer = $.timer(function() {
		xmlHttp.abort();
		alert(connectivity_msg);
		ajax_timer.stop();
	},connectivity_timeout_time,true);
	
	//Sending AJAX request
	url = host + "/admin/delete_upload.php?tbl_uploads_id="+tbl_uploads_id+"&rnd="+rnd;
	//alert(url);
	xmlHttp.open("POST",url,true);
	xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlHttp.send("rnd="+rnd);
}

/* Function to load a navigation page */
function get_all_students_against_class() {
	//show_loading();
	
	var xmlHttp, rnd, url, search_param, ajax_timer;
	var tbl_school_id = '<?=$tbl_school_id_sess?>';
	var tbl_class_id = $("#tbl_class_id").val();
	//alert(tbl_class_id);
	
	rnd = Math.floor(Math.random()*11);
	try{		
		xmlHttp = new XMLHttpRequest(); 
	}catch(e) {
		try{
			xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		}catch(e) {
			xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
			hide_loading();
		}
	}
	
	//AJAX response
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
			ajax_timer.stop();
			var data = xmlHttp.responseText;
			//alert(data);
			var ddm = '<select id="tbl_student_id" name="tbl_student_id">';
			$("#student_ddm").html(ddm+data+"</select>");
		}
	}
	
	ajax_timer = $.timer(function() {
		xmlHttp.abort();
		alert(connectivity_msg);
		ajax_timer.stop();
	},connectivity_timeout_time,true);
	
	//Sending AJAX request
	url = host + "/student/get_all_students_against_class/en/null/null/"+tbl_class_id+"/null/"+rnd;
	url = host + "/index.php?c=student&m=get_all_students_against_class&lan=en&device=android&tbl_class_id="+tbl_class_id+"&tbl_school_id="+tbl_school_id+"&rnd=2";

	//alert(url);
	xmlHttp.open("POST",url,true);
	xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlHttp.send("rnd="+rnd);
}
</script>


<script type="text/javascript" src="<?=JS_PATH?>/nice_edit/nicEdit.php"></script>
<script language="javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script>

<script language="JavaScript">
	
	
	/* CUSTOM JS - MODIFY AS PER NEED */
	function valueCheckedForUsersManagement(){
		var ml = document.frmDirectory;
		var len = ml.elements.length;
		for (var i = 0; i < len; i++){
			if (document.frmDirectory.elements[i].checked){
				return true;
			}
		}
		 alert ("Select at least one record.");
		 return false;
	}
	
	function CheckAll(){
		/*var ml = document.frmDirectory;
		var len = ml.elements.length;
		if (document.frmDirectory.AC.checked==true) {
			 for (var i = 0; i < len; i++) {
				document.frmDirectory.elements[i].checked=true;
			 }
		} else {
			  for (var i = 0; i < len; i++)  {
				document.frmDirectory.elements[i].checked=false;
			  }
		}*/
	}

	function UnCheckAll() {
		/*var ml = document.frmDirectory;
		var len = ml.elements.length;
		var count=0; var checked=0;
			for (var i = 0; i < len; i++) {	       
				if ((document.frmDirectory.elements[i].type=='checkbox') && (document.frmDirectory.elements[i]. != "AC")) {
					count = count + 1;
					if (document.frmDirectory.elements[i].checked == true){
						checked = checked + 1;
					}
				}
			 }
			 
		if (checked == count) {
			 document.frmDirectory.AC.checked = true;
		} else {
			document.frmDirectory.AC.checked = false;
		}*/
	}
		
	
	function validateForm() {
		//return;
		
		if(validateClass()== false || validateStudent()==false || validateTitle()==false || validateType()==false){
		  return false;	
		}
		var child_data_type = $("#child_data_type").html().trim();
		if (child_data_type == "d") {	//Text
			var uploaded_items = $("#uploaded_items").html().trim();
			if (uploaded_items != "") {
				alert("For type Text please remove video first to proceed.");
				return false;
			}
		}
		return true;
	}
	
	function validateClass()
	{
		var str = $("#tbl_class_id").val();
		if (str == null) {
			alert("\n Please select class.");
			return false;
		}
	}
	function validateStudent()
	{
		var str = $("#tbl_student_id").val();
		if (str == '0') {
			alert("\n Please select student.");
			return false;
		}
	}
	function validateTitle()
	{
		var str = $("#child_data_title_en").val();
		if (str == "") {
			alert("\n Please enter title.");
			return false;
		}
		var str = $("#child_data_title_ar").val();
		if (str == "") {
			alert("\n Please enter title in arabic.");
			return false;
		}
	}
	function validateType()
	{
		var str = $('input:radio[name=child_data_type]:checked').val();
 		if (str == "undefined") {
			alert("\n Please select data type.");
			return false;
		}
	}
	
	function isMenuTextEn() {
		var str = document.frmRecord.serial_no.value;
		if (str == "") {
			alert("\nThe Student Data (Videos/Text)[En] field is blank. Please write Student Data (Videos/Text)[En].");
			document.frmRecord.serial_no.value="";
			document.frmRecord.serial_no.focus();
			return false;
		}
		if (str.match(/^[a-zA-Z0-9]+/)) {
			return true;
		} else {
			alert("\n Please enter valid Student Data (Videos/Text)[En].");
			document.frmRecord.serial_no.select();
			document.frmRecord.serial_no.focus();
			return false;
		}
		return true;
	}
	
	function isMenuTextAr() {
		var str = document.frmRecord.party__ar.value;
		if (str == "") {
			alert("\nThe Student Data (Videos/Text)[Ar] field is blank. Please write Student Data (Videos/Text)[Ar].");
			document.frmRecord.party__ar.value="";
			document.frmRecord.party__ar.focus();
			return false;
		}
		return true;
	}

	function isPageTextEn() {
		var str = nicEditors.findEditor('page_text_en').getContent();
		var str_="";
		//alert($('#page_text_en_'));
		if (document.getElementById('page_text_en_') != null) {
			str_ = $('#page_text_en_').val();
		}
		//alert(str);
		//alert(">"+str_+"<");
		if ((str == "<br>" || str == "") && (str_ == "<br>" || str_ == "" || str_ == "&nbsp;" || str_ == "undefined")) {
			alert("\nThe Page Text[En] field is blank. Please write Page Text[En].");
			return false;
		}
		return true;
	}
	
	function isPageTextAr() {
		var str = nicEditors.findEditor('page_text_ar').getContent();
		var str_="";
		//alert($('#page_text_en_'));
		if (document.getElementById('page_text_ar_') != null) {
			str_ = $('#page_text_ar_').val();
		}
		//alert(str);
		//alert(">"+str_+"<");
		if ((str == "<br>" || str == "") && (str_ == "<br>" || str_ == "" || str_ == "&nbsp;" || str_ == "undefined")) {
			alert("\nThe Page Text[Ar] field is blank. Please write Page Text[Ar].");
			return false;
		}
		return true;
	}
	
	
	function isFirstName() {
		var str = document.frmRecord.page_text.value;
		if (str == "") {
			alert("\nThe First  field is blank. Please write your First .");
			document.frmRecord.page_text.value="";
			document.frmRecord.page_text.focus();
			return false;
		}
		if (!isNaN(str)) {
			alert("\nPlease write your First .");
			document.frmRecord.page_text.value="";
			document.frmRecord.page_text.select();
			document.frmRecord.page_text.focus();
			return false;
		}
		for (var i = 0; i < str.length; i++) {
			var ch = str.substring(i, i + 1);
			if  ((ch <"A" || ch > "z" ) && (ch !=" ")){
				alert("\n Please enter valid First .");
				document.frmRecord.page_text.select();
				document.frmRecord.page_text.focus();
				return false;
			}
		}
		return true;
	}
	
	function isLastName() {
		var str = document.frmRecord.menu_text.value;
		if (str == "") {
			alert("\nThe Last  field is blank. Please write your Last .");
			document.frmRecord.menu_text.value="";
			document.frmRecord.menu_text.focus();
			return false;
		}
		if (!isNaN(str)) {
			alert("\nPlease write your Last .");
			document.frmRecord.menu_text.value="";
			document.frmRecord.menu_text.select();
			document.frmRecord.menu_text.focus();
			return false;
		}
		for (var i = 0; i < str.length; i++) {
			var ch = str.substring(i, i + 1);

			if  ((ch <"A" || ch > "z" ) && (ch !=" ")){
				alert("\n Please enter valid Last .");
				document.frmRecord.menu_text.select();
				document.frmRecord.menu_text.focus();
				return false;
			}
		}
		return true;
	}
		
	function isUserType() {
		var str = document.frmRecord.user_type.value;
		if (str == "") {
			alert("\nPlease select user type.");
			document.frmRecord.user_type.focus();
			return false;
		}
		return true;
	}
	
	function isEmail() {
		var str = document.frmRecord.total_pages.value;
		if (str == "") {
			alert("\nThe Email field is blank.Please enter your valid Email.");
			document.frmRecord.total_pages.focus();
			return false;
		}
		if (!isNaN(str)) {
			alert("\nPlease write your correct Email address");
			document.frmRecord.total_pages.select();
			document.frmRecord.total_pages.focus();
			return false;
		}
		if(str.indexOf('@', 0) == -1) {
			alert("\nIt seems that your total_pages address is not valid.");
			document.frmRecord.total_pages.select();
			document.frmRecord.total_pages.focus();
			return false;
		}
		return true;
	}
	
	function isUserID() {
		var str = document.frmRecord.tbl_child_data_id.value;
		if ( str=="" ) {
			alert("\nThe User-ID is blank. Please write your User-ID.");
			document.frmRecord.tbl_child_data_id.focus();
			return false;
		}
		if (str.length < 7 ) {
			alert("\nThe User-ID should be greater than 5 Characters.");
			document.frmRecord.tbl_child_data_id.focus();
			document.frmRecord.tbl_child_data_id.select();
			return false;
		}
			
		if (!isNaN(str)) {
			alert("\nThe User-ID have only letters & digits, Please re-enter your User-ID");
			document.frmRecord.tbl_child_data_id.select();
			document.frmRecord.tbl_child_data_id.focus();
			return false;
		}
	
		for (var i = 0; i < str.length; i++) {
			var ch = str.substring(i, i + 1);
			if  ((ch < "a" || ch > "z") && (ch < "0" || "9" < ch) ) {
				alert("\nThe User-ID have only letters in lower case & digits, Please re-enter your User-ID");
				document.frmRecord.tbl_child_data_id.select();
				document.frmRecord.tbl_child_data_id.focus();
				return false;
			}
		}
		return true;
	}
	
	function isPassword() {
		var str = document.frmRecord.password.value;
		if (str == "") {
			alert("\nThe Password field is blank. Please enter Password.");
			document.frmRecord.password.focus();
			return false;
		}
			if (str.length < 7) {
			alert("\nThe Password should be greater than 6 Characters.");
			document.frmRecord.password.focus();
			document.frmRecord.password.select();
			return false;
		}
		return true;
	}
	
	function isRetypePassword() {
		var str = document.frmRecord.confirm_password.value;
		if (str == "") {
			alert("\nThe Confirm Password field is blank. Please retype password.");
			document.frmRecord.confirm_password.focus();
			return false;
		}
		return true;
	}
	
	function isPasswordSame() {
		var str1 = document.frmRecord.password.value;
		var str2 = document.frmRecord.confirm_password.value;
		if (str1 != str2) {
			alert("\nPassword mismatch, Please retype same passwords in both fields.");
			document.frmRecord.confirm_password.focus();
			return false;
		}
		return true;
	}
	
	function show_page_en() {
		if (document.getElementById("page_text_en_hidden") != null) {
			var data = document.getElementById("page_text_en_hidden").innerHTML;
			document.getElementById("page_text_en_container").innerHTML = 
			'<textarea name="page_text_en_" id="page_text_en_" style="border:1px solid #CCC" cols="100" rows="10">'+data+'</textarea>';
		} else {
			document.getElementById("page_text_en_container").innerHTML = 
			'<textarea name="page_text_en_" id="page_text_en_" style="border:1px solid #CCC" cols="100" rows="10"></textarea>';
		}
	}
	function hide_page_en() {
		document.getElementById("page_text_en_container").innerHTML = '';
	}

	function show_page_ar() {
		if (document.getElementById("page_text_ar_hidden") != null) {
			var data = document.getElementById("page_text_ar_hidden").innerHTML;
			document.getElementById("page_text_ar_container").innerHTML = 
			'<textarea name="page_text_ar_" id="page_text_ar_" style="border:1px solid #CCC" cols="100" rows="10">'+data+'</textarea>';
		} else {
			document.getElementById("page_text_ar_container").innerHTML = 
			'<textarea name="page_text_ar_" id="page_text_ar_" style="border:1px solid #CCC" cols="100" rows="10"></textarea>';
		}
	}
	function hide_page_ar() {
		document.getElementById("page_text_ar_container").innerHTML = '';
	}

	function show_page_ur() {
		if (document.getElementById("page_text_ur_hidden") != null) {
			var data = document.getElementById("page_text_ur_hidden").innerHTML;
			document.getElementById("page_text_ur_container").innerHTML = 
			'<textarea name="page_text_ur_" id="page_text_ur_" style="border:1px solid #CCC" cols="100" rows="10">'+data+'</textarea>';
		} else {
			document.getElementById("page_text_ur_container").innerHTML = 
			'<textarea name="page_text_ur_" id="page_text_ur_" style="border:1px solid #CCC" cols="100" rows="10"></textarea>';
		}
	}
	function hide_page_ur() {
		document.getElementById("page_text_ur_container").innerHTML = '';
	}
	
	function show_video() {
		$("#parenting_video").slideDown();
		$("#parenting_text").slideUp();
		
		var uploaded_items = $("#uploaded_items").html().trim();
		if (uploaded_items != "") {
			$("#parenting_video").hide();
		}
		$("#child_data_type").html("v");
	}
	
	function show_text() {
		$("#parenting_text").slideDown();
		$("#parenting_video").slideUp();
		$("#child_data_type").html("d");
	}
	
	$(document).ready(function(e) {

		var child_data_type;
		if (document.getElementById("child_data_type") != null) {
			child_data_type = $("#child_data_type").html().trim();
		}
		if (child_data_type == "v" || child_data_type == "V") {
			$("#parenting_text").slideUp();
			$("#parenting_video").slideDown();
		} else if (child_data_type == "d" || child_data_type == "D") {
			$("#parenting_text").slideDown();
			$("#parenting_video").slideUp();	
		} else {
			$("#parenting_text").slideUp();
			$("#parenting_video").slideUp();	
		}
		
		if (document.getElementById("uploaded_items") != null) {
			var uploaded_items = $("#uploaded_items").html().trim();
			if (uploaded_items != "") {
				$("#parenting_video").hide();
			}
		}
    });

</script>

	<?php /*?><link rel="stylesheet" type="text/css" href="<?=JS_PATH?>/multiselect/jquery.multiselect.css" />
    <link rel="stylesheet" type="text/css" href="<?=JS_PATH?>/multiselect/jquery.multiselect.filter.css" />
    <link rel="stylesheet" type="text/css" href="<?=JS_PATH?>/multiselect/assets/style.css" />
    <link rel="stylesheet" type="text/css" href="<?=JS_PATH?>/multiselect/assets/prettify.css" />
    <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css" />
    
    <!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>-->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?=JS_PATH?>/multiselect/src/jquery.multiselect.js"></script>
    <script type="text/javascript" src="<?=JS_PATH?>/multiselect/src/jquery.multiselect.filter.js"></script>
    <script type="text/javascript" src="<?=JS_PATH?>/multiselect/assets/prettify.js"></script>
    
    <script language="javascript">
    
    $(document).ready(function(e) {
        $("#tbl_student_id").multiselect({ 
		multiple: false,
		selectedList: 1,
        click: function(e){
	        }
        }).multiselectfilter();   
    });
    
    </script>
	<?php */?>
</head>


<?php if (!$offset || $offset<0)  { $offset =0;} ?>
<?php if (!$LIKE)  { $LIKE = "LIKE";} ?>
<!--	DEFAULT FIRST SCREEN	-->
<?php if($mid=="1"){ ?>
<table width="100%" height="570" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr>
    <td height="30" valign="center" class="adminDetailHeading">&nbsp;Student Data (Videos/Text) Management </td>
  </tr>
  <tr align="center">
    <td height="538" align="center" valign="top">
    <br>
    <form ="frmRecord" id="frmRecord" method="post" action=""  onSubmit="return validateForm();" enctype="multipart/form-data">
        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1">
          <tr>
            <td height="22" align="left" valign="middle"  class="adminDetailHeading"><strong>&nbsp;Create Student Data (Videos/Text)</strong></td>
          </tr>
          <tr>
            <td align="center" valign="top" class="bordLightBlue">
            <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                <tr align="center" valign="top">
                  <td colspan="2"></td>
                </tr>
                <tr align="left" valign="top">
                  <td colspan="2"><span class="msgColor">Fields with * are mandatory.</span></td>
                </tr>
                </table>

                 <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                <tr valign="top">
                  <td align="right">Class<span class="msgColor">*</span>:</td>
                  <td>
					<?php
						$qry_school_type = "SELECT * FROM ".TBL_SCHOOL_TYPE." WHERE tbl_school_id='$tbl_school_id_sess' AND is_active='Y' ORDER BY priority ";
						$rs_school_type = SelectMultiRecords($qry_school_type);
                    ?>
                  <select name="tbl_class_id" id="tbl_class_id" size="10" style="padding:0px 5px 0px 2px; font-size:14px" onChange="get_students_ajax()">
                    	<option value="">Select Class</option>
						
						
						<?php
                        	for ($s=0; $s<count($rs_school_type); $s++) {
								$id_s = $rs_school_type[$s]['id'];
								$tbl_school_type_id_s = $rs_school_type[$s]['tbl_school_type_id'];
								$school_type_s = $rs_school_type[$s]['school_type'];
								$school_type_ar_s = $rs_school_type[$s]['school_type_ar'];
						?>
                          <optgroup label="-- <?=ucfirst($school_type_s)?> [::] <?=$school_type_ar_s?>" style="font-style:normal; padding-bottom:5px">
									<?php
                                        $qry_class = "SELECT * FROM ".TBL_CLASS." WHERE tbl_school_id='$tbl_school_id_sess' AND tbl_school_type_id='$tbl_school_type_id_s' AND is_active='Y' ORDER BY priority ";
                                        $rs_class = SelectMultiRecords($qry_class);
									?>
									<?php
                                        for ($c=0; $c<count($rs_class); $c++) {
                                            $id_c = $rs_class[$c]['id'];
                                            $tbl_class_id_c = $rs_class[$c]['tbl_class_id'];
                                            $tbl_school_type_id_c = $rs_class[$c]['tbl_school_type_id'];
                                            $class_name_c = $rs_class[$c]['class_name'];
                                            $class_name_ar_c = $rs_class[$c]['class_name_ar'];
											
											$tbl_section_id = $rs_class[$c]['tbl_section_id'];
											$qry = "SELECT * FROM ".TBL_SECTION." WHERE tbl_section_id='".$tbl_section_id."'";
											$data_se = SelectMultiRecords($qry);
											
											$section_name =  $data_se[0]["section_name"];
											$section_name_ar =  $data_se[0]["section_name_ar"];
											
											
                                    ?>
									<option value="<?=$tbl_class_id_c?>"><?=$class_name_c?>&nbsp;<?=$section_name?>&nbsp;[::] <?=$class_name_ar_c?>&nbsp;<?=$section_name_ar?></option>
                                    <?php } ?>                          		
                          </optgroup>
                        <?php } ?> 
                    </select> </td>
                </tr>
                <tr valign="middle">
                  <td align="right">Student<span class="msgColor">*</span>:</td>
                  <td id="student_ddm"><select id="tbl_student_id" name="tbl_student_id" class="flat" style="width:200px;">
                    </select>
                  	
                  </td>
                </tr>
                <tr valign="middle">
                  <td align="right">Title [En]<span class="msgColor">*</span>: </td>
                  <td><input name="child_data_title_en" type="text" class="input_text" id="child_data_title_en" size="30" value="" >&nbsp; </td>
                </tr>
                <tr valign="middle">
                  <td align="right">Title [Ar]<span class="msgColor">*</span>: </td>
                  <td><input name="child_data_title_ar" type="text" class="input_text" dir="rtl" id="child_data_title_ar" size="30" value="">&nbsp;</td>
                </tr>
                <tr valign="middle">
                  <td align="right">Logo Image:</td>
                  <td><input type="file" name="child_data_logo" class="flat" id="child_data_logo">&nbsp;</td>
                </tr>
                <tr valign="middle">
                  <td align="right">Data Type<span class="msgColor">*</span>: </td>
                  <td>
                  <input type="radio" name="child_data_type" value="D" onClick="show_text()"> Text
                  <input name="child_data_type" type="radio" value="V" onClick="show_video()"> Video
                  </td>
                </tr>
                 
                </table>
                <div id="parenting_text">
                    <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                    <tr valign="middle">
                      <td width="20%" align="right"> Text [En]: </td>
                      <td><textarea name="child_data_text_en" id="child_data_text_en" cols="80" rows="10" class="flat"></textarea></td>
                      </tr>
                          <tr valign="middle">
                            <td align="right"> Text [Ar]: </td>
                            <td><textarea name="child_data_text_ar" id="child_data_text_ar" cols="80" rows="10" class="flat"></textarea></td>
                          </tr>
                  </table>
              </div>
                <div id="parenting_video">
                <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                <tr valign="middle">
                        <td width="20%" align="right">Video:</td>
                        <td>
                          <div id="advancedUpload">Upload</div>
                            
                  </td>
                </tr>
               </table>
               </div>
                <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                <tr valign="middle">
                  <td width="20%" align="right"><span  id="vid_text">Video:</span></td>
                    <td align="left">
                        <div id="uploaded_items"></div>
                    </td>
                </tr>
               </table>
               
              <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                <tr valign="middle">
                  <td align="right">&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr valign="middle">
                  <td align="right">Status:</td>
                  <td><input name="is_active" type="radio" value="Y" checked>
                    Yes
                    <input type="radio" name="is_active" value="N">
                  No </td>
                </tr>
                <tr>
                  <td width="20%">&nbsp;</td>
                  <td align="left">&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td align="left"><input type="hidden" name="tbl_child_data_idd" value="<?=$tbl_child_data_idd?>">
                    <input type="reset" name="Reset" value="Reset" class="flat">
                    <input name="save" type="submit" class="flat" id="ssubmit"  value="Submit">
                    <input name="ssubmit" type="hidden" id="ssubmit" value="insert_record">
                    <input name="sid" type="hidden" value="<?=$sid?>">
                    <input name="mid" type="hidden" value="1">
                    <input name="offset" type="hidden" id="offset" value="<?=$offset;?>">
                    <input name="q" type="hidden" id="q" value="<?php echo htmlspecialchars($q);?>">
                    <input name="by" type="hidden" id="by" value="<?=$by?>">
                    <input name="field" type="hidden" id="field" value="<?=$field?>">
                    <input name="LIKE" type="hidden" value="<?=$LIKE?>">
                </tr>
                 <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td></tr>
              </table></td>
          </tr>
        </table>
        <div id="child_data_type" style="display:none"></div>
        <br>
      </form>
      <br>
      <br>
      
    <form name="form1" method="post" action="">
        <br>
        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >
          <tr > 
            <td height="22" align="left" valign="middle" class="adminDetailHeading">&nbsp;Search 
              Criteria</td>
          </tr>
          <tr > 
            <td align="center" valign="top" class="bordLightBlue">
            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                <tr> 
                  <td width="21%"><span class="searchText">Field</span>:<br /><select ="field" id="field">
                      <option value="child_data_title_en" <?PHP if ($field == "child_data_title_en"){echo "selected";}?>>Text [En] </option>
                      <option value="added_date" <?PHP if ($field == "added_date"){echo "selected";}?>> Date e.g [YYYY-MM-DD] </option>
                      <option value="is_active"<?PHP if($field == "is_active"){echo "selected";}?>>Status 
                      e.g Y=Activate , N=Deactivate</option>
                    </select>
                    </td>
                  <td width="17%"><span class="searchText">Condition:</span><br /><?php
							if (!$LIKE) {
								$LIKE = "LIKE";
							}
						?>
                    <select name="LIKE" id="LIKE">
                      <option value="LIKE" <?PHP if ($LIKE == "LIKE"){echo "selected";}?>>LIKE</option>
                      <option value="=" <?PHP if (($LIKE == "=") || ($LIKE == "")){echo "selected";}?>>Equal 
                      To</option>
                      <option value="!=" <?PHP if ($LIKE == "!="){echo "selected";}?>>Not 
                      Equal To</option>
                      <option value="<" <?PHP if ($LIKE == "<"){echo "selected";}?>>Less 
                      Than</option>
                      <option value=">" <?PHP if ($LIKE == ">"){echo "selected";}?>>Greater 
                      Than</option>
                      <option value="<=" <?PHP if ($LIKE == "<="){echo "selected";}?>>Less 
                      Than or Equal To</option>
                      <option value=">=" <?PHP if ($LIKE == ">="){echo "selected";}?>>Greater 
                      Than or Equal To</option>
                    </select></td>
                   <td width="18%"><span class="searchText">Search 
                    Name:&nbsp;</span><br /><input name="q" type="text" id="q" value="<?php echo htmlspecialchars($q);?>" size="30"></td>
                   <td width="10%"><span class="searchText">Order 
                    By:&nbsp;</span><br /><select name="by" id="by">
                      <option value="ASC" <?PHP if (($by == "ASC") || ($by == "")) { echo "selected";}?>>ASCENDING</option>
                      <option value="DESC" <?PHP if ($by == "DESC"){ echo "selected";}?>>DESCENDING</option>
                    </select>
                        </td>
                   <td width="34%" valign="bottom" style="padding-top:15px;"><br />
                    <input name="mid" type="hidden" value="1">
                    <input name="offset" type="hidden" value="0">
                    <input name="Search" type="submit" id="Search" value="Search" class="flat">
                    <input name="ssubmit" type="hidden" id="ssubmit" value="" class="flat">          &nbsp;
                  </td>
                  
                </tr>
                <tr> 
                  <td width="21%">&nbsp;</td>
                  <td width="17%"> <a href="<?=HOST_URL?>/admin/child_data_management/child_data_management.php?mid=<?=$mid?>&sid=<?=$sid?>&tab=members&subtab=student"><span class="detailLinkColor">All 
                    Records</span></a></td>
                  <td width="18%" colspan="4"></td>
                </tr>
              </table></td>
          </tr>
        </table>
      </form>  
    
      <br>
      <span class="msgColor"><?php echo $MSG;?></span><br>
      <?php
		$q = addslashes($q);
		$CountRec = "SELECT * FROM ".TBL_CHILD_DATA." WHERE tbl_school_id='$tbl_school_id_sess' ";	
		$Query = "SELECT * FROM ".TBL_CHILD_DATA." WHERE tbl_school_id='$tbl_school_id_sess' ";	
		if(!$by){$by="DESC";}
		
		if($field && !empty($q)){	    	
			
			if($LIKE=="LIKE"){
					$CountRec .= " AND `$field` $LIKE '%$q%' ";
					$Query .= " AND  `$field` $LIKE '%$q%' ";
			}else{
					$CountRec .= " AND  `$field` $LIKE '$q' ";	
					$Query .= " AND  `$field` $LIKE '$q' ";
			}
		}	
		 
		 $Query .= " ORDER BY added_date ASC ";
		 $Query .=" LIMIT $offset, ".TBL_CHILD_DATA_PAGING;
   	  	// echo $Query;
		 $q = stripslashes($q);
		 $total_record = CountRecords($CountRec);
		 $data = SelectMultiRecords($Query);
		  		 if ($total_record =="")
					   echo '<span class="msgColor">'.MSG_NO_RECORD_FOUND."</span>";
					else{	
					    echo '<span class="msgColor">';
					    echo " <b> ". $total_record ." </b> Record(s) found. Showing <b>";
						if ($total_record>$offset){
							echo $offset+1;
							echo " </b>to<b> ";
							if ($offset >= $total_record - TBL_CHILD_DATA_PAGING)	{ 
								  echo $total_record; 
							}else { 
							   	echo $offset + TBL_CHILD_DATA_PAGING ;
							}
						}else{ 
							echo $offset+1;
							echo "</b> - ". $total_record;
							echo " to ". $total_record ." Record(s) ";		
						}
						echo "</b>.</span>";	
					}
	   ?>
      <?php  if ($total_record !=""){?>
      <table width="98%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="18"><span class="msgColor">&nbsp;&nbsp;Note: To view detail 
            click on Title [En].</span></td>
        </tr>
        <tr>
          <td align="center" valign="top" class="adminDetailHeading"><table width="100%" border="0" align="center"  cellpadding="2" cellspacing="1">
              <form ="frmDirectory" onSubmit="return valueCheckedForUsersManagement()">
                <tr align="center">
                  <td width="20%" align="center" class="adminDetailHeading">Title</td>
                  <td width="15%" class="adminDetailHeading">Class</td>
                  <td width="15%" class="adminDetailHeading">Student</td>
                  <td width="20%" class="adminDetailHeading">Type</td>
                  <td width="13%" height="24" align="center" class="adminDetailHeading">Date</td>
                  <td width="8%" align="center" class="adminDetailHeading">Status&nbsp;</td>
                  <td width="6%" align="center" class="adminDetailHeading">Action</td>
                  <td width="6%" height="24" align="center" class="adminDetailHeading"><input name="AC" type="checkbox" onClick="CheckAll();"></td>
                </tr>
                <?php
				 for($i=0;$i<count($data);$i++){
						$id = $data[$i]["id"];
						$tbl_child_data_id = $data[$i]["tbl_child_data_id"];
						$tbl_class_id = $data[$i]["tbl_class_id"];
						$tbl_student_id = $data[$i]["tbl_student_id"];
						$child_data_logo = $data[$i]["child_data_logo"];
						$child_data_type = $data[$i]["child_data_type"];
						$child_data_title_en = $data[$i]["child_data_title_en"];
						$child_data_title_ar = $data[$i]["child_data_title_ar"];
						$child_data_text_en = $data[$i]["child_data_text_en"];
						$child_data_text_ar = $data[$i]["child_data_text_ar"];
						$added_date = $data[$i]["added_date"];				
						$is_active = $data[$i]["is_active"];
						
						$qry = "SELECT * FROM ".TBL_STUDENT." WHERE tbl_student_id='".$tbl_student_id."'";
						$data_rs = selectFrom($qry);
						$name_en = $data_rs["first_name"]." ".$data_rs["last_name"];
						$name_ar = $data_rs["first_name_ar"]." ".$data_rs["last_name_ar"];
						
						$qry = "SELECT * FROM ".TBL_CLASS." WHERE tbl_class_id='".$tbl_class_id."'";
						$data_rs = selectFrom($qry);
						$class_name = $data_rs["class_name"];
						$class_name_ar = $data_rs["class_name_ar"];
						
				?>
                <tr valign="middle" style="background-color:#FFF">
                  <td align="left">
                  		<div style="margin-left:5px"><a href="<?=HOST_URL?>/admin/child_data_management/child_data_management.php?mid=2&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&by=<?=$by?>&tbl_child_data_id=<?=$tbl_child_data_id?>" title="Click to view detail" class="detailLinkColor"><?=$child_data_title_en?></a></div>
                        <div style="padding:5px" class="lan_r_ar"><?php echo $child_data_title_ar;?></div>

                  </td>
                  <td align="left"><span style="margin-left:5px">
                    <?php	echo $class_name;	?>
                    <div style="padding:5px" class="lan_r_ar"><?php echo $class_name_ar;?></div>
                  </span></td>
                  <td align="center"><span style="margin-left:5px">
                    <?php	echo $name_en;	?>
                    <div style="padding:5px" class="lan_r_ar"><?php echo $name_ar;?></div>
                  </span></td>
                  <td align="center"><span style="margin-left:5px">
                    <?php if ($child_data_type == "v") {echo "Video";} else {echo "Text";}?>
                  </span></td>
                  <td height="20" align="center"><?php echo date("d M, Y H:i:s",strtotime($added_date));?></td>
                  <td align="center"><?php 
		 			 if($is_active == 'Y'){?>
                    <img src="<?=IMG_PATH?>/yes.gif">
                    <?php } else {?>
                    <img src="<?=IMG_PATH?>/no.gif">
                    <?php }  ?></td>
                  <td align="center"><a href="?mid=3&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&by=<?=$by?>&tbl_child_data_id=<?=$tbl_child_data_id?>" class="detailLinkColor">Edit</a></td>
                  <td align="center"><input name="EditBox[]" type="checkbox" value="<?=$tbl_child_data_id?>"></td>
                </tr>
                <?php }?>
                <tr>
                  <td height="20" colspan="9" align="center" valign="middle" bgcolor="#FFFFFF">
                  
                    <input name="reset" type="reset" class="flat"  value="Reset">
                    <input name="show" type="submit" class="flat" id="ssubmit" value="Active">
                    <input name="hide" type="submit" class="flat"  value="Deactive">
                    <input name="delete" type="submit" class="flat"  value="Delete">
                    <input name="ssubmit" type="hidden" id="ssubmit" value="recordActiveInactive">
                    <input name="mid" type="hidden" id="mid2" value="1">
                    <input name="sid" type="hidden" id="sid" value="<?=$sid?>">
                    <input name="offset" type="hidden" id="offset" value="<?=$offset?>">
                    <input name="q" type="hidden" id="q" value="<? echo htmlspecialchars($q);?>">
                    <input name="tbl_child_data_idd" type="hidden" value="<?=$tbl_child_data_idd?>">
                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>">
                    <input name="by" type="hidden" id="by" value="<?=$by?>">
                    <input name="field" type="hidden" id="field" value="<?=$field?>">
                    <input name="LIKE" type="hidden" value="<?=$LIKE?>">
                    </td>
                </tr>
                <tr align="right">
                  <td height="20" colspan="9" valign="middle" bgcolor="#FFFFFF"><?php 
					   if ($total_record != "" && $total_record>TBL_CHILD_DATA_PAGING) {
							$url = "child_data_management.php?mid=1&act=$act&sid=$sid&field=$field&q=$q&LIKE=$LIKE&by=$by";
							$Paging_object = new Paging();
							$Paging_object->paging_new_style_final($url,$offset,$clicked_link,$total_record,TBL_CHILD_DATA_PAGING);
						}
					?></td>
                </tr>
              </form>
            </table></td>
        </tr>
      </table>
      <br>
      <?php } ?>
      <br>
      <br>
      
      </td>
  </tr>
</table>
<!--	RECORD DETAILS SCREEN	-->
<?php } if($mid=="2") { 
		$qry = "SELECT * FROM ".TBL_CHILD_DATA." WHERE tbl_child_data_id='$tbl_child_data_id'";
		//echo $qry;
		$data = selectFrom($qry);
		$tbl_child_data_id = $data["tbl_child_data_id"];
		$tbl_student_id = $data["tbl_student_id"];
		$child_data_logo = $data["child_data_logo"];
		$child_data_type = $data["child_data_type"];		//d or v
		$child_data_title_en = $data["child_data_title_en"];
		$child_data_title_ar = $data["child_data_title_ar"];
		$child_data_text_en = $data["child_data_text_en"];
		$child_data_text_ar = $data["child_data_text_ar"];
		$added_date = $data["added_date"];				
		$is_active = $data["is_active"];
		
		$qry = "SELECT * FROM ".TBL_UPLOADS." WHERE tbl_item_id='".$tbl_child_data_id."'";
		
		$data_rsu = SelectMultiRecords($qry);
?>
        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1">
         <tr>
            <td height="25" align="left" valign="middle"></td>
          </tr>
          <tr>
            <td height="22" align="left" valign="middle"  class="adminDetailHeading">&nbsp;&nbsp; <strong> Student Data (Videos/Text) Details</strong></td>
          </tr>
          <tr>
            <td align="center" valign="top" class="bordLightBlue"><table width="100%" border="0" align="center" cellpadding="5" cellspacing="2">
                <tr align="center" valign="top">
                  <td colspan="2"></td>
                </tr>
                <tr valign="middle">
                  <td width="20%" align="right">&nbsp;</td>
                  <td width="78%">&nbsp;</td>
                </tr>
                <tr valign="middle">
                  <td align="right">  Title [En]: </td>
                  <td><?=$child_data_title_en?></td>
                </tr>
                <tr valign="middle">
                  <td align="right">  Title [Ar]: </td>
                  <td  dir="ltr" ><?=$child_data_title_ar?></td>
                </tr>
                <tr valign="middle">
                  <td align="right">Thumb Image(Video):</td>
                  <td><img src="<?=IMG_LOGO_IMG_PATH?>/<?=$child_data_logo?>" width="100" height="100"></td>
                </tr>
                <tr valign="middle">
                  <td align="right"> Type: </td>
                  <td><?php if ($child_data_type == "v") {echo "Video";} else {echo "Text";}?></td>
                </tr>
                <?php if ($child_data_type == "d") {?>
                <tr valign="middle">
                  <td align="right"> Text [En]: </td>
                  <td><?=$child_data_text_en?></td>
                  </tr>
                      <tr valign="middle">
                        <td align="right"> Text [Ar]: </td>
                        <td class="lan_l_ar"><?=$child_data_text_ar?></td>
                      </tr>
                <?php	} else {	?>      
                <tr valign="middle">
                        <td align="right" valign="top">Video:</td>
                        <td align="left" valign="top">
                        <?php	
						for ($i=0; $i<count($data_rsu); $i++) {	?>
                        	<div style="padding-bottom:6px">
                                  <a href="<?=HOST_URL?>/admin/download.php?id=<?=$data_rsu[$i]["tbl_uploads_id"]?>" target="_blank" class="detailLinkColor">
                                    <?=$data_rsu[$i]["file_name_original"]?>
                                  </a> 
                            </div>
						<?php	}	?>
                  </td>
                </tr>
                <?php	}	?>
                <tr valign="middle">
                  <td align="right">Status:&nbsp;</td>
                  <td align="left" valign="top"><?php echo $is_active; ?></td>
                </tr>
                <tr valign="middle">
                  <td align="right">&nbsp;</td>
                  <td align="left" valign="top">&nbsp;</td>
                </tr>
                <tr valign="middle">
                  <td colspan="2" align="center" height="10"><a href="<?=HOST_URL?>/admin/child_data_management/child_data_management.php?mid=1&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&by=<?=$by?>&tbl_child_data_id=<?=$tbl_child_data_id?>" class="detailLinkColor">Back </a>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td align="left" valign="middle">&nbsp;</td>
                </tr>
              </table></td>
          </tr>
        </table>

<!--	RECORD EDIT SCREEN	-->
<?php }if($mid=="3"){ 

		$data = selectFrom("SELECT * FROM ".TBL_CHILD_DATA." WHERE tbl_child_data_id='$tbl_child_data_id'");
		$id = $data["id"];
		$tbl_child_data_id = $data["tbl_child_data_id"];
		$tbl_class_id = $data["tbl_class_id"];
		$tbl_student_id = $data["tbl_student_id"];
		$child_data_logo = $data["child_data_logo"];
		$child_data_type = $data["child_data_type"];		//d or v
		$child_data_title_en = $data["child_data_title_en"];
		$child_data_title_ar = $data["child_data_title_ar"];
		$child_data_text_en = $data["child_data_text_en"];
		$child_data_text_ar = $data["child_data_text_ar"];
		
		$child_data_title_en = stripslashes($child_data_title_en);
		$child_data_title_ar = stripslashes($child_data_title_ar);
		$child_data_text_en = stripslashes($child_data_text_en);
		$child_data_text_ar = stripslashes($child_data_text_ar);
		
		$added_date = $data["added_date"];				
		$is_active = $data["is_active"];

		$qry = "SELECT * FROM ".TBL_UPLOADS." WHERE tbl_item_id='".$tbl_child_data_id."'";
		$data_rsu = SelectMultiRecords($qry);
?>
<div id="page_text_en_hidden" style="display:none"><?=$page_text_en?></div>
<div id="page_text_ar_hidden" style="display:none"><?=$page_text_ar?></div>
<div id="page_text_ur_hidden" style="display:none"><?=$page_text_ur?></div>
<table width="100%" height="451" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr>
    <td height="30" valign="center" class="adminDetailHeading">&nbsp;Student Data (Videos/Text) Management 
      [Edit] </td>
  </tr>
  <tr>
    <td align="center" valign="top"><br>
      <span class="msgColor"><?php echo $MSG;?></span> <br>
      <form ="frmRecord" id="frmRecord" method="post" action=""  onSubmit="return validateForm();" enctype="multipart/form-data">
              <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1">
                <tr>
                  <td height="22" align="left" valign="middle"  class="adminDetailHeading"><strong>&nbsp;Edit Student Data (Videos/Text) </strong></td>
                </tr>
                <tr>
                  <td align="center" valign="top" class="bordLightBlue" style=""><table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                <tr valign="middle">
                  <td width="25%" align="left" class="msgColor">&nbsp;</td>
                  <td width="75%">&nbsp;</td>
                </tr>
                <tr valign="middle">
                  <td colspan="2" align="left" class="msgColor"><span style="padding-left:150px;">Fields with * are mandatory.&nbsp;</span></td>
                  </tr>
                 <tr><td colspan="2"></td></tr>
                <?php	
					$qry = "SELECT * FROM ".TBL_STUDENT." WHERE tbl_school_id='$tbl_school_id_sess' AND is_active='Y'";
					$data_rs = SelectMultiRecords($qry);
				?>
                <tr valign="middle">
                  <td align="right">Class:</td>
                  <td>
					<?php
						$qry_school_type = "SELECT * FROM ".TBL_SCHOOL_TYPE." WHERE tbl_school_id='$tbl_school_id_sess' AND is_active='Y' ORDER BY priority ";
						$rs_school_type = SelectMultiRecords($qry_school_type);
                    ?>
	                  <select name="tbl_class_id" id="tbl_class_id" size="1"  class="flat" onChange="get_students_ajax()">
                    	<option value="0">--Select Class--</option>
                    	<?php
                        	for ($s=0; $s<count($rs_school_type); $s++) {
								$id_s = $rs_school_type[$s]['id'];
								$tbl_school_type_id_s = $rs_school_type[$s]['tbl_school_type_id'];
								$school_type_s = $rs_school_type[$s]['school_type'];
								$school_type_ar_s = $rs_school_type[$s]['school_type_ar'];
						?>
                          <optgroup label="-- <?=ucfirst($school_type_s)?> [::] <?=$school_type_ar_s?>" style="font-style:normal; padding-bottom:5px">
									<?php
                                        $qry_class = "SELECT * FROM ".TBL_CLASS." WHERE tbl_school_id='$tbl_school_id_sess' AND tbl_school_type_id='$tbl_school_type_id_s' AND is_active='Y' ORDER BY priority ";
                                        $rs_class = SelectMultiRecords($qry_class);
									?>
									<?php
                                        for ($c=0; $c<count($rs_class); $c++) {
                                            $id_c = $rs_class[$c]['id'];
                                            $tbl_class_id_c = $rs_class[$c]['tbl_class_id'];
                                            $tbl_school_type_id_c = $rs_class[$c]['tbl_school_type_id'];
                                            $class_name_c = $rs_class[$c]['class_name'];
                                            $class_name_ar_c = $rs_class[$c]['class_name_ar'];
                                    ?>
									<option value="<?=$tbl_class_id_c?>" <?php if ($tbl_class_id_c == $tbl_class_id){echo "selected";}?>><?=$class_name_c?> [::] <?=$class_name_ar_c?></option>
                                    <?php } ?>                          		
                          </optgroup>
                        <?php } ?> 
                  </select>&nbsp;</td>
                </tr>
                <tr valign="middle">
                  <td align="right">Student:</td>
                  <td id="student_ddm">
                  		<?php	$qry = "SELECT * FROM ".TBL_STUDENT." WHERE tbl_class_id='".$tbl_class_id."' AND is_active='Y'";	
								$data_rs = SelectMultiRecords($qry);
								//echo $qry;
						?>
                        <select id="tbl_student_id" name="tbl_student_id" class="flat">
                        	<?php	for($i=0; $i<count($data_rs); $i++) {	
										$tbl_student_id_s = $data_rs[$i]["tbl_student_id"];
										$name = $data_rs[$i]["first_name"]." ". $data_rs[$i]["last_name"];
							?>
                            		<option value="<?=$tbl_student_id_s ?>" <?php if ($tbl_student_id_s == $tbl_student_id){echo "selected";}?>><?=$name?></option>
                            <?php	}	?>
                        </select>
                  </td>
                </tr>
                <tr valign="middle">
                  <td align="right">Title [En]: </td>
                  <td><input name="child_data_title_en" type="text" class="input_text" id="child_data_title_en" size="50" value="<?=$child_data_title_en?>">&nbsp;</td>
                </tr>
                <tr valign="middle">
                  <td align="right">Title [Ar]: </td>
                  <td><input name="child_data_title_ar" type="text" class="input_text flat" dir="rtl" id="child_data_title_ar" size="50" value="<?=$child_data_title_ar?>">&nbsp;</td>
                </tr>
                <tr valign="middle">
                  <td align="right" valign="top">Thumb Image (<?=$max_x?> x <?=$max_y?>):</td>
                        <td align="left" valign="top"><?php if ($child_data_logo  != "") { ?>
                          <img style="float:left" src="<?=IMG_LOGO_IMG_PATH?>/<?=$child_data_logo?>" width="100" height="100">
                          <div style="clear:both"></div>
                        
                          <br>
                          <?php		}	?>
                          <div style="clear:both">
                            <input type="file" name="child_data_logo" class="flat" id="child_data_logo">
                          </div>
                          Browse to select a different Thumb Image [.jpg, .png]
                           </td>
                </tr>
                <tr valign="middle">
                  <td align="right">Type: </td>
                  <td>
                  <input type="radio" name="child_data_type" value="d" onClick="show_text()" <?php if($child_data_type == 'd'){?>checked="checked"<? } ?>> Text
                  <input name="child_data_type" type="radio" value="v" onClick="show_video()" <?php if($child_data_type == 'v'){?>checked="checked"<? } ?>> Video
                  </td>
                </tr>
                 
                </table>
                <div id="parenting_text">
                      <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                    <tr valign="middle">
                  <td align="right"> Text [En]: </td>
                  <td><textarea name="child_data_text_en" id="child_data_text_en" cols="80" rows="10" class="flat"><?=$child_data_text_en?></textarea></td>
                  </tr>
                      <tr valign="middle">
                        <td align="right"> Text [Ar]: </td>
                        <td><textarea name="child_data_text_ar" id="child_data_text_ar" cols="80" rows="10" class="flat"><?=$child_data_text_ar?></textarea></td>
                      </tr>
                    </table>
                    </div>
                    
                    <div id="parenting_video" style="display:none;">
                        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                        <tr valign="middle">
                                <td width="20%" align="right">Video:</td>
                                <td>
                                  <div id="advancedUpload">Upload</div>
                                    
                          </td>
                        </tr>
                       </table>
                      </div>
                       <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                      <tr valign="middle">
                        <td width="20%" align="right" valign="top">
                        	<?php	if (count($data_rsu)>0) {	?>
	                        	<span id="vidd_txt">Video:</span>
                            <?php	}	?>
                        </td>
                        <td align="left" valign="top" width="80%">
                       	  	 
                             <div id="uploaded_items">
                        	 <?php	for ($i=0; $i<count($data_rsu); $i++) {	?>
		                            <div class="row_item" id="<?=$data_rsu[$i]["tbl_uploads_id"]?>">
                                    	<div class="upload_del" onClick="delete_file('<?=$data_rsu[$i]["tbl_uploads_id"]?>')"></div>
                                        <div class="upload_content">
                                        	<a href="<?=HOST_URL?>/admin/download.php?id=<?=$data_rsu[$i]["tbl_uploads_id"]?>" target="_blank" class="detailLinkColor">
												<?=$data_rsu[$i]["file_name_original"]?>
	                                        </a>
                                        </div>
                                    </div>
                             <?php	}	?>
                             </div>
                             
                        </td>
                      </tr>
                      </table>
                      <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                      <tr valign="middle">
                        <td width="20%" align="right">Status:</td>
                        <td><input type="radio" name="is_active" value="Y" <?php if($is_active == 'Y'){?>checked="checked"<? } ?>>
                          Yes
                          <input type="radio" name="is_active" value="N" <?php if($is_active == 'N'){?>checked="checked"<? } ?>>
                        No </td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td align="left" valign="middle">&nbsp;</td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td align="left" valign="middle"><a href="child_data_management.php?mid=1&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&sort=<?=$sort?>&by=<?=$by?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>" class="detailLinkColor">Back</a>&nbsp;&nbsp;
                          <div id="child_data_type" style="display:none;"><?=$child_data_type?></div>
                          <input type="hidden" name="tbl_child_data_id" value="<?=$tbl_child_data_id?>">
                          <input type="reset" name="Reset" value="Reset" class="flat">
                          <input type="submit" name="Save" value="Save" class="flat">
                          <input name="ssubmit" type="hidden" id="ssubmit" value="edit_record">
                          <input name="sid" type="hidden" value="<?=$sid?>">
                          <input name="mid" type="hidden" value="1">
                          <input name="offset" type="hidden" id="offset" value="<?=$offset;?>">
                          <input name="q" type="hidden" id="q" value="<?php echo htmlspecialchars($q);?>">
                          <input name="sort" type="hidden" id="sort" value="<?=$sort?>">
                          <input name="by" type="hidden" id="by" value="<?=$by?>">
                          <input name="field" type="hidden" id="field" value="<?=$field?>">
                          <input name="LIKE2" type="hidden" value="<?=$LIKE?>">
                      </tr>
                  </table></td>
                </tr>
              </table>
            </form>
      <p><br>
      </p></td>
  </tr>
</table>
<!--	RECORD DELETE SCREEN	-->
<?php }if($mid=="4"){ ?>
<table width="100%" height="380" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr>
    <td height="30" valign="center" class="adminDetailHeading">&nbsp;Student Data (Videos/Text) Management 
      [Del Confirmation]</td>
  </tr>
  <tr align="center">
    <td valign="top"><br>
      <span class="msgColor"> Are you sure you want to delete the selected record(s) </span> <br>
      <table width="98%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center" valign="top" class="adminDetailHeading"><table width="100%" border="0" align="center"  cellpadding="2" cellspacing="1">
              <form ="frmDirectory" onSubmit="return valueCheckedForUsersManagement();">
                <tr>
                  <td width="20%" align="center" class="adminDetailHeading">Title</td>
                  <td align="center" class="adminDetailHeading">Class</td>
                  <td width="20%" align="center" class="adminDetailHeading">Type</td>
                  <td width="15%" align="center"  class="adminDetailHeading">Date</td>
                  <td width="10%" align="center"  class="adminDetailHeading">Status</td>
                  <td width="4%" height="24" align="center"  class="adminDetailHeading"><input name="AC" type="checkbox" id="AC" onClick="CheckAll();" checked></td>
                </tr>
                <?php 
				for($i=0;$i<count($EditBox);$i++){
					$Query ="SELECT * FROM ".TBL_CHILD_DATA." WHERE tbl_child_data_id='$EditBox[$i]'";
					$data = selectFrom($Query);
					$id = $data["id"];
					$tbl_class_id = $data["tbl_class_id"];
					$tbl_child_data_id = $data["tbl_child_data_id"];
					$tbl_student_id = $data["tbl_student_id"];
					$child_data_logo = $data["child_data_logo"];
					$child_data_type = $data["child_data_type"];		//d or v
					$child_data_title_en = $data["child_data_title_en"];
					$child_data_title_ar = $data["child_data_title_ar"];
					$child_data_text_en = $data["child_data_text_en"];
					$child_data_text_ar = $data["child_data_text_ar"];
					
					$child_data_title_en = stripslashes($child_data_title_en);
					$child_data_title_ar = stripslashes($child_data_title_ar);
					$child_data_text_en = stripslashes($child_data_text_en);
					$child_data_text_ar = stripslashes($child_data_text_ar);
					
					$added_date = $data["added_date"];				
					$is_active = $data["is_active"];
					
					$qry = "SELECT * FROM ".TBL_STUDENT." WHERE tbl_class_id='".$tbl_class_id."'";
					$data_rs = selectFrom($qry);
					$name_en = $data_rs["first_name"]." ".$data_rs["last_name"];
					$name_ar = $data_rs["first_name_ar"]." ".$data_rs["last_name_ar"];
				?>
                <tr valign="middle" bgcolor="#FFFFFF">
                  <td align="left"><div style="margin-left:5px">
<?=$child_data_title_en?>
                  </div>
                    <div style="padding:5px" class="lan_r_ar"><?php echo $child_data_title_ar;?></div></td>
                  <td align="left"><span style="margin-left:5px">
                    <?php	echo $name_en;	?>
                    <div style="padding:5px" class="lan_r_ar"><?php echo $name_ar;?></div>
                  </span></td>
                  <td align="center"><span style="margin-left:5px">
                    <?php if ($child_data_type == "v") {echo "Video";} else {echo "Text";}?>
                  </span></td>
                  <td height="20" align="center"><?php echo date("d M, Y H:i:s",strtotime($added_date));?></td>
                  <td align="center"><?php 
		  			if($is_active == 'Y'){?>
                    <img src="<?=IMG_PATH?>/yes.gif">
                    <?php } else {?>
                    <img src="<?=IMG_PATH?>/no.gif">
                    <?php }  ?></td>
                  <td height="20" align="center"><input name="EditBox[]" type="checkbox" id="EditBox[]" onClick="UnCheckAll();" value="<?=$tbl_child_data_id?>" checked></td>
                </tr>
                <?php }?>
                <tr>
                  <td height="20" colspan="7" align="center" valign="middle" bgcolor="#FFFFFF"><input name="Button" type="button" class="flat" id="hide" value="I am not sure" onClick="javascript:history.back();">
                    <input name="show" type="submit" class="flat" id="show" value="Yes I am sure">
                    <input name="sid" type="hidden" id="sid" value="<?=$sid;?>">
                    <input name="offset" type="hidden" id="offset" value="<?=$offset;?>">
                    <input name="mid" type="hidden" id="mid" value="1">
                    <input name="ssubmit" type="hidden" id="show" value="recordDelInformation">
                    <input name="q" type="hidden" id="q" value="<?php echo htmlspecialchars($q);?>">
                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>">
                    <input name="by" type="hidden" id="by" value="<?=$by?>">
                    <input name="field" type="hidden" id="field" value="<?=$field?>">
                    <input name="LIKE" type="hidden" value="<?=$LIKE?>">
                </tr>
              </form>
            </table></td>
        </tr>
      </table>
  </tr>
</table>
<?php } ?>
<script language="javascript">
	var connectivity_msg = "Connection timed out. Please try again.";
	var connectivity_timeout_time = 10000;
	var host = '<?=HOST_URL?>';

	function get_students_ajax() {
		//show_loading();
		
		var tbl_class_id = $("#tbl_class_id").val();
		//alert(tbl_class_id);
		//return;
		var xmlHttp, rnd, url, search_param, ajax_timer;
		rnd = Math.floor(Math.random()*11);
		try{		
			xmlHttp = new XMLHttpRequest(); 
		}catch(e) {
			try{
				xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
			}catch(e) {
				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
				hide_loading();
			}
		}
		
		//AJAX response
		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
				ajax_timer.stop();
				var data = xmlHttp.responseText;
				$("#tbl_student_id").html(data);
				$("#tbl_student_id").multiselect('refresh');
				return;
			}
		}
		
		ajax_timer = $.timer(function() {
			xmlHttp.abort();
			alert(connectivity_msg);
			ajax_timer.stop();
		},connectivity_timeout_time,true);
		
		//Sending AJAX request
		url = host + "/student/get_all_students_against_class/?lan=en&tbl_class_id="+tbl_class_id+"&rnd="+rnd;
		//alert(url);
		xmlHttp.open("POST",url,true);
		xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlHttp.send("rnd="+rnd);
	}
</script>
<?php include("../footer.php"); ?>
