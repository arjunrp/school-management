<?php include ($_SERVER["DOCUMENT_ROOT"]."/aqdar/includes/config.inc.php"); ?>
<?php
$sid = $_REQUEST["sid"];
if (!loggedUser($sid)) { 
	redirect (HOST_URL_ADMIN ."/include/messages.php?msg=relogin");
exit();
}
?>
<?php
 	mysql_query("SET NAMES 'utf8'");
	mysql_query('SET CHARACTER SET utf8'); 

	$q = stripslashes($q);	
	if($ssubmit=="insertNews"){
		/*Upload Video START*/
			//echo "here 1 <br />";
			$is_uploaded = "N";
			$title = trim(addslashes($title));
			$speaker = trim(addslashes($speaker));
			$description = trim(addslashes($description));
			
			if ($file_video && trim($_FILES["file_video"]["name"]) != "") {
				//Movie Upload
				$video_file_name_original = $_FILES["file_video"]["name"];
				$video_file_name_tmp = $_FILES["file_video"]["tmp_name"];
				$video_file_type = $_FILES["file_video"]["type"];
				$video_file_size = $_FILES["file_video"]["size"];
				$Ext = strchr($video_file_name_original,".");
				$Ext = strtolower($Ext);
				
		
				if($Ext==".mp4" || $Ext==".ogg") {
					$video_file_name_updated = substr(md5(uniqid(rand())),0,15);
					$video_file_name_updated = $video_file_name_updated.$Ext;
					$copy = file_copy("$video_file_name_tmp", VIDEO_UPLOAD_PATH."/", "$video_file_name_updated", 1, 1);
					$is_uploaded = "Y";
				} else {
					$MSG .= " Video extension is not valid. Only .mp4 and .ogg allowed.";
				}
			}//if ($file_video && trim($_FILES["file_video"]["name"]) != "")
		/*Upload Video END*/


		if ($is_uploaded == "N" && trim($video_name) == "") {
			$MSG .= " Video file or video file name is mandatory. <br />";
		} else {
			$video_file_name_original = trim($video_name);
			$video_file_name_updated = trim($video_name);
			$video_file_type = "video/mp4";
			$image_file_size = 0;
		
			$qry_ins = "INSERT INTO ".TBL_VIDEO." (`tbl_video_id` ,`title` ,`title_ar` ,`speaker` ,`description` ,`description_ar` ,`video_file_name_original` ,`video_file_name_updated` ,`video_file_size` ,`video_file_type` ,`image_file_name_original` ,`image_file_name_updated` ,`image_file_size` ,`image_file_type` ,`added_date` ,`is_active` ,`tbl_school_id`)
			VALUES('$tbl_video_id','$title','$title_ar','$speaker','$description','$description_ar','$video_file_name_original','$video_file_name_updated','$video_file_size','$video_file_type','$image_file_name_original','$image_file_name_updated','$image_file_size','$image_file_type', NOW(), '$is_active', '$tbl_school_id_sess')";
			//echo $qry_ins."<br>";
			insertInto($qry_ins);

			for ($i=0; $i<count($tbl_category_video_id); $i++) {
				$tbl_category_video_id_i = $tbl_category_video_id[$i];
				$qry_ins = "INSERT INTO ".TBL_VIDEO_CATEGORY." (`tbl_video_id` ,`tbl_category_video_id` ,`added_date` ,`is_active` ,`tbl_school_id`)
							VALUES ('$tbl_video_id', '$tbl_category_video_id_i', NOW(), 'Y', '$tbl_school_id_sess')";
				//echo $qry_ins."<br />";			
				insertInto($qry_ins);
			}
			
			$title = "";
			$title_ar = "";
			$speaker = "";
			$description = "";
			$description_ar = "";
			$MSG .= " Information saved successfully.";
		}


		/*Upload Thumb Image START*/
			if ($file_thumb && trim($_FILES["file_thumb"]["name"]) != "") {
				//Image Upload
				$image_file_name_original = $_FILES["file_thumb"]["name"];
				$image_file_name_tmp = $_FILES["file_thumb"]["tmp_name"];
				$image_file_type = $_FILES["file_thumb"]["type"];
				$image_file_size = $_FILES["file_thumb"]["size"];
				
				$Ext = strchr($image_file_name_original,".");
				$Ext = strtolower($Ext);
	
				if($Ext==".jpg" || $Ext==".gif" || $Ext==".png") {
					$image_file_name_updated = substr(md5(uniqid(rand())),0,15);
					$image_file_name_updated = $image_file_name_updated.$Ext;
					$copy = file_copy("$image_file_name_tmp", Thumb Image_UPLOAD_PATH."/", "$image_file_name_updated", 1, 1);
				} else {
					$image_file_name_original = "";
					$image_file_name_updated = "";
					$image_file_type = "";
					$image_file_size = "";
					$is_error = "Y";
					$MSG .= " Thumb Image extension is not valid. <br />";
				}
			}
		/*Upload Thumb Image END*/
		
		
		$mid=1;
	}//if 
	
if($ssubmit=="editNews") {

		if ($file_video && trim($_FILES["file_video"]["name"]) != "") {
			//Movie Upload
			$video_file_name_original = $_FILES["file_video"]["name"];
			$video_file_name_tmp = $_FILES["file_video"]["tmp_name"];
			$video_file_type = $_FILES["file_video"]["type"];
			$video_file_size = $_FILES["file_video"]["size"];
			$Ext = strchr($video_file_name_original,".");
			$Ext = strtolower($Ext);
			
	
			if($Ext==".mp4" || $Ext==".ogg") {
				$video_file_name_updated = substr(md5(uniqid(rand())),0,15);
				$video_file_name_updated = $video_file_name_updated.$Ext;
				$copy = file_copy("$video_file_name_tmp", VIDEO_UPLOAD_PATH."/", "$video_file_name_updated", 1, 1);

				$qry_update = "UPDATE ".TBL_VIDEO." SET video_file_name_original='$video_file_name_original', video_file_name_updated='$video_file_name_updated', video_file_type='$video_file_type', video_file_size='$video_file_size' WHERE id=$id ";
				update($qry_update);
			} else {
				$is_error = "Y";
				$MSG .= " Video extension is not valid. Only .mp4 and .ogg allowed.";
			}
		}


		if ($file_thumb && trim($_FILES["file_thumb"]["name"]) != "") {
			//Movie Upload
			$image_file_name_original = $_FILES["file_thumb"]["name"];
			$image_file_name_tmp = $_FILES["file_thumb"]["tmp_name"];
			$image_file_type = $_FILES["file_thumb"]["type"];
			$image_file_size = $_FILES["file_thumb"]["size"];
			$Ext = strchr($image_file_name_original,".");
			$Ext = strtolower($Ext);
			
	
			if($Ext==".jpg" || $Ext==".gif" || $Ext==".png") {
				$image_file_name_updated = substr(md5(uniqid(rand())),0,15);
				$image_file_name_updated = $image_file_name_updated.$Ext;
				$copy = file_copy("$image_file_name_tmp", Thumb Image_UPLOAD_PATH."/", "$image_file_name_updated", 1, 1);

				$qry_update = "UPDATE ".TBL_VIDEO." SET image_file_name_original='$image_file_name_original', image_file_name_updated='$image_file_name_updated', image_file_type='$image_file_type', image_file_size='$image_file_size' WHERE id=$id ";
				update($qry_update);
			} else {
				$is_error = "Y";
				$MSG .= " Thumb Image extension is not valid. <br />";
			}
		}

		$qry_update = "UPDATE ".TBL_VIDEO." SET title='$title', title_ar='$title_ar', description='$description', description_ar='$description_ar', is_active='$is_active' WHERE id=$id ";
		update($qry_update);
		$MSG .= "<br /> Changes saved successfully.";


		$qry_sel = "SELECT tbl_video_id FROM ".TBL_VIDEO." WHERE id=$id ";
		$rs_sel = selectFrom($qry_sel);
		$tbl_video_id = $rs_sel['tbl_video_id'];

		$qry_del = "DELETE FROM ".TBL_VIDEO_CATEGORY." WHERE tbl_video_id='$tbl_video_id' ";
		//echo $qry_del;
		deleteFrom($qry_del);
		
		for ($i=0; $i<count($tbl_category_video_id); $i++) {
			$tbl_category_video_id_i = $tbl_category_video_id[$i];
			$qry_ins = "INSERT INTO ".TBL_VIDEO_CATEGORY." (`tbl_video_id` ,`tbl_category_video_id` ,`added_date` ,`is_active` ,`tbl_school_id`)
						VALUES ('$tbl_video_id', '$tbl_category_video_id_i', NOW(), 'Y', '$tbl_school_id_sess')";
			//echo $qry_ins."<br />";			
			insertInto($qry_ins);
		}
		
		if (trim($remove_thumb) == "Y") {
			$qry_update = "UPDATE ".TBL_VIDEO." SET image_file_name_original='', image_file_name_updated='', image_file_size='', image_file_type='' WHERE id=$id ";
			update($qry_update);
		}
		
	$mid=3;
}

if($ssubmit=="newsDetail"){
		if(isset($show)){
				for ($j=0; $j<count($EditBox); $j++){ 
					$Querry = "UPDATE ". TBL_VIDEO ." SET is_active='Y' WHERE id=$EditBox[$j]";			
					update($Querry);
					
					/*
					$qry_sel = "SELECT tbl_video_id FROM ".TBL_VIDEO." WHERE id=$EditBox[$j] ";
					$rs_sel = selectFrom($qry_sel);
					$tbl_video_id = $rs_sel['tbl_video_id'];
					
					$Querry = "UPDATE ". TBL_VIDEO_CATEGORY ." SET is_active='Y' WHERE tbl_video_id='$tbl_video_id'";			
					update($Querry);
					*/
					
					$mid=1;	
					$MSG = "Selected video(s) have been activated successfully!";
				}
		}
		if(isset($hide)){
				for ($j=0; $j<count($EditBox); $j++){ 
					$Querry = "UPDATE ". TBL_VIDEO ." SET is_active='N' WHERE id=$EditBox[$j]";			
					update($Querry);
					
					/*
					$qry_sel = "SELECT tbl_video_id FROM ".TBL_VIDEO." WHERE id=$EditBox[$j] ";
					$rs_sel = selectFrom($qry_sel);
					$tbl_video_id = $rs_sel['tbl_video_id'];
					
					$Querry = "UPDATE ". TBL_VIDEO_CATEGORY ." SET is_active='N' WHERE tbl_video_id='$tbl_video_id'";			
					update($Querry);
					*/
					
					$mid=1;	
					$MSG = "Selected video(s) have been deactivated successfully!";
				}
			}
		if(isset($delete)){$mid = 4;}
	}

if($ssubmit == "newsDelConfirmation"){
		for($i=0;$i<count($EditBox);$i++){
				
				$qry_sel = "SELECT tbl_video_id FROM ".TBL_VIDEO." WHERE id=$EditBox[$i] ";
				$rs_sel = selectFrom($qry_sel);
				$tbl_video_id = $rs_sel['tbl_video_id'];
				
				$Querry = "DELETE FROM ". TBL_VIDEO_CATEGORY ." WHERE tbl_video_id='$tbl_video_id'";			

				deleteFrom("DELETE FROM ".TBL_VIDEO." WHERE `id` =$EditBox[$i]");
				deleteFrom($Querry);
				
				$mid = 1;
				$MSG = "Selected video(s) have been deleted successfully!";
			
		}//for
}	
?>
<html>
<head>
<title>Admin (News Management)</title>
<link rel="stylesheet" href="<?=ADMIN_CSS_PATH?>interface.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script src="<?=ADMIN_SCRIPT_PATH?>/validation.js">CopyDatarichtext1();</script>

<script src="<?=JS_PATH?>/nice_edit/nicEdit.php"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script>

<script language="JavaScript">
function valueCheckedForNewsManagement(){
	var ml = document.frmDirectory;
	var len = ml.elements.length;
	for (var i = 0; i < len; i++){
	   	if (document.frmDirectory.elements[i].checked){
			return true;
		}
	}
	 alert ("Select at least one video!");
	 return false;
}

function CheckAll(){
	var ml = document.frmDirectory;
	var len = ml.elements.length;
	if (document.frmDirectory.AC.checked==true) {
		 for (var i = 0; i < len; i++) {
			document.frmDirectory.elements[i].checked=true;
		 }
	} else {
		  for (var i = 0; i < len; i++)  {
			document.frmDirectory.elements[i].checked=false;
		  }
	}
}
function UnCheckAll() {
	var ml = document.frmDirectory;
	var len = ml.elements.length;
	var count=0; var checked=0;
	    for (var i = 0; i < len; i++) {	       
			if ((document.frmDirectory.elements[i].type=='checkbox') && (document.frmDirectory.elements[i].name != "AC")) {
				count = count + 1;
	  		 	if (document.frmDirectory.elements[i].checked == true){
					checked = checked + 1;
				}
			}
	     }
		 
	if (checked == count) {
		 document.frmDirectory.AC.checked = true;
	} else {
		document.frmDirectory.AC.checked = false;
	}
}
</script>
<script language="javascript">
function validateForm() {
	if (validate_category() == false || validate_title() == false || validate_speaker() == false || validate_video() == false) {
		return false;	
	}	
return true;	
}

function validateFormEdit() {
	if (validate_category() == false || validate_title() == false || validate_speaker() == false) {
		return false;	
	}	
return true;	
}

	function validate_category() {
		var len = document.getElementById("tbl_category_video_id[]").length;
		var is_error = 'Y';
		
		for (i=0; i<parseInt(len); i++) {
			if(document.getElementById("tbl_category_video_id[]")[i].selected) {
				is_error = 'N';
				break;	
			}	
		}
		if (is_error == "Y") {
			alert("The Category is mandatory, Please select Category.");
		return false;
		}
	}	

	function validate_title() {
		var regExp = / /g;
		var str = document.frmRegister.title.value;
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			alert("The Title is blank. Please write Title.")
			document.frmRegister.title.value = "";
			document.frmRegister.title.focus();
		return false;
		}

		var regExp = / /g;
		var str = document.frmRegister.title_ar.value;
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			alert("The Title Arabic is blank. Please write Title Arabic.")
			document.frmRegister.title_ar.value = "";
			document.frmRegister.title_ar.focus();
		return false;
		}
	}	

	function validate_speaker() {
		var regExp = / /g;
		var str = document.frmRegister.speaker.value;
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			alert("The Speaker is blank. Please write Speaker.")
			document.frmRegister.speaker.value = "";
			document.frmRegister.speaker.focus();
		return false;
		}
	}	

	function validate_video() {
		is_error = "N";
		
		var regExp = / /g;
		var str = document.frmRegister.video_name.value;
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			//alert("The Video is mandatory, Please select video.")
			//document.frmRegister.file_video.focus();
			is_error = "Y";
		//return false;
		} else {
			return true;	
		}
		
		
		var regExp = / /g;
		var str = document.frmRegister.file_video.value;
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			alert("The Video is mandatory, Please select video.")
			document.frmRegister.file_video.focus();
		return false;
		}
	}	

</script>
<style>
.txt_en {
	text-align:left;
	padding-left:2px;
}
.txt_ar {
	text-align:right;
	padding-right:2px;	
	direction:rtl;		
}
</style>
</head>
<?php if (!$offset || $offset<0)  { $offset =0;} ?>
<?php if (!$LIKE)  { $LIKE = "LIKE";} ?>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<STYLE type=text/css>#dek {
	Z-INDEX: 200; VISIBILITY: hidden; POSITION: absolute
}
</STYLE>

<?php if($mid=="1"){ ?>
<table width="100%" height="570" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr> 
    <td height="30" valign="center" class="adminDetailHeading">&nbsp;Video(s) Management 
    </td>
  </tr>
  <tr align="center" > 
    <td height="538" align="center" valign="top" > <form name="form1" method="post" action="">
        <br>
        <table width="80%" border="0" align="center" cellpadding="0" cellspacing="1" >
          <tr > 
            <td height="22" align="left" valign="middle" class="adminDetailHeading">&nbsp;Search 
              Criteria</td>
          </tr>
          <tr > 
            <td align="center" valign="top" class="bordLightBlue"><table width="100%" border="0" cellspacing="3" cellpadding="2">
                <tr> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr> 
                  <td width="32%">&nbsp;</td>
                  <td width="48%"> <a href="video_mgmt.php?mid=<?=$mid?>&sid=<?=$sid?>&news_type=<?=$news_type?>"><span class="detailLinkColor">All 
                    Records</span></a>: or&nbsp;</td>
                  <td width="20%"><i></i></td>
                </tr>
                <tr> 
                  <td colspan="3"><hr width="80%" size="1"></td>
                </tr>
                <tr> 
                  <td align="right" valign="top" ><span class="searchText">Field:&nbsp;</span></td>
                  <td align="left" valign="top"> <select name="field" id="field">
                      <option value="title" <?PHP if ($field == "title"){echo "selected";}?>>Title</option>
                      <option value="title_ar" <?PHP if ($field == "title_ar"){echo "selected";}?>>Title Arabic</option>
                      <option value="description" <?PHP if ($field == "description"){echo "selected";}?>>Description</option>
                      <option value="description_ar" <?PHP if ($field == "description_ar"){echo "selected";}?>>Description Arabic</option>
                      <option value="image_file_name_original" <?PHP if ($field == "image_file_name_original"){echo "selected";}?>>Thumb Image Name</option>
                      <option value="video_file_name_original" <?PHP if ($field == "video_file_name_original"){echo "selected";}?>>Video Name</option>
                      <option value="added_date" <?PHP if ($field == "added_date"){echo "selected";}?>>Date e.g [YYYY-MM-DD]</option>
                      <option value="is_active"<?PHP if($field == "is_active"){echo "selected";}?>>Status e.g Y=Activate , N=Deactivate</option></option>
                      </select></td>
                  <td><i><span onMouseOver="window.status='';do_info(22);return true" onmouseout=kill() href="javascript:void(0)";><img src="<?=HOST_URL?>/images/help_form.gif"></span></i></td>
                </tr>
                <tr> 
                  <td align="right" valign="top"><span class="searchText">Condition:&nbsp;</span></td>
                  <td align="left" valign="top"> 
                    <?php
							if (!$LIKE) {
								$LIKE = "LIKE";
							}
						?>
                    <select name="LIKE" id="LIKE">
                      <option value="LIKE" <?PHP if ($LIKE == "LIKE"){echo "selected";}?>>LIKE</option>
                      <option value="=" <?PHP if (($LIKE == "=") || ($LIKE == "")){echo "selected";}?>>Equal 
                      To</option>
                      <option value="!=" <?PHP if ($LIKE == "!="){echo "selected";}?>>Not 
                      Equal To</option>
                      <option value="<" <?PHP if ($LIKE == "<"){echo "selected";}?>>Less 
                      Than</option>
                      <option value=">" <?PHP if ($LIKE == ">"){echo "selected";}?>>Greater 
                      Than</option>
                      <option value="<=" <?PHP if ($LIKE == "<="){echo "selected";}?>>Less 
                      Than or Equal To</option>
                      <option value=">=" <?PHP if ($LIKE == ">="){echo "selected";}?>>Greater 
                      Than or Equal To</option>
                    </select></td>
                  <td><i><span onMouseOver="window.status='';do_info(23);return true" onmouseout=kill() href="javascript:void(0)";><img src="<?=HOST_URL?>/images/help_form.gif"></span></i></td>
                </tr>
                <tr> 
                  <td align="right" valign="top"><span class="searchText">Search 
                    Name:&nbsp;</span></td>
                  <td align="left" valign="top"> <input name="q" type="text" id="q" value="<?php echo htmlspecialchars($q);?>" size="40"> 
                  </td>
                  <td><i><span onMouseOver="window.status='';do_info(24);return true" onmouseout=kill() href="javascript:void(0)";><img src="<?=HOST_URL?>/images/help_form.gif"></span></i></td>
                </tr>
                <tr> 
                  <td align="right" valign="top"><span class="searchText">Order 
                    By:&nbsp;</span></td>
                  <td align="left" valign="top"> 
                  	<select name="by" id="by">
                      <option value="ASC" <?PHP if (($by == "ASC")) { echo "selected";}?>>ASCENDING</option>
                      <option value="DESC" <?PHP if ($by == "DESC" || $by == ""){ echo "selected";}?>>DESCENDING</option>
                    </select> &nbsp;</td>
                  <td><i><span onMouseOver="window.status='';do_info(25);return true" onmouseout=kill() href="javascript:void(0)";><img src="<?=HOST_URL?>/images/help_form.gif"></span></i></td>
                </tr>
                <tr> 
                  <td colspan="3" style="color:#CC0000">&nbsp;</td>
                </tr>
                <tr> 
                  <td align="right" valign="top"><span class="searchText">Category:&nbsp;</span></td>
                  <td align="left" valign="top"> 
                  <?php
				  	$qry_cat_search = "SELECT * FROM ".TBL_CATEGORY_VIDEO." WHERE is_active='Y' ORDER BY category_name ";
					//echo $qry_cat."<br />";
					$rs_cat_search = SelectMultiRecords($qry_cat_search);
				  ?>
                  <select name="tbl_category_video_id_search" id="tbl_category_video_id_search">
						<option value="">--Select Category--</option>
                  		<?php
                        for ($ii=0; $ii<count($rs_cat_search); $ii++) { 
							$tbl_video_category_id_ii = $rs_cat_search[$ii]['tbl_video_category_id'];
							$category_name_ii = $rs_cat_search[$ii]['category_name'];
							$category_name_ar_ii = $rs_cat_search[$ii]['category_name_ar'];
						?>
							<option value="<?=$tbl_video_category_id_ii?>" <?php if(trim($tbl_video_category_id_ii) == $tbl_category_video_id_search) {echo "selected";}?> ><?=$category_name_ii?> [::] <?=$category_name_ar_ii?></option>
                        <?php
						}
						?>    
                  </select>
                    
                    </td>
                  <td>&nbsp;</td>
                </tr>
                <tr> 
                  <td colspan="3" style="color:#CC0000">&nbsp;</td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                  <td align="left"> 
                    <input name="mid" type="hidden" value="1"> 
                    <input name="offset" type="hidden" value="0"> 
                    <input type="hidden" name="news_type" value="<?=$news_type?>">
                    <input name="Search" type="submit" id="Search" value="Search" class="flat"> 
                    &nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr> 
                  <td colspan="3" style="color:#CC0000">&nbsp;</td>
                </tr>
              </table></td>
          </tr>
        </table>
      </form>
      <br>
      <span class="msgColor"><?php echo $MSG;?></span> <br> 
      <?php 
		$q = addslashes($q);
		$CountRec .= "SELECT * FROM ".TBL_VIDEO." WHERE tbl_school_id='$tbl_school_id_sess' ";	
		$Query .= "SELECT * FROM ".TBL_VIDEO." WHERE tbl_school_id='$tbl_school_id_sess' ";		
		if(!$by){$by="DESC";}
		
		if($field && !empty($q)){	    	
			
			if($LIKE=="LIKE"){
					$CountRec .= " AND `$field` $LIKE '%$q%' ";
//					$Query .= " WHERE `$field` $LIKE '%$q%' ORDER BY `$field` $by ";
					$Query .= " AND `$field` $LIKE '%$q%' ";
			}else{
					$CountRec .= " AND `$field` $LIKE '$q' ";	
//					$Query .= " WHERE `$field` $LIKE '$q' ORDER BY `$field` $by ";
					$Query .= " AND `$field` $LIKE '$q' ";
			}
		}	
		
		
		if (trim($tbl_category_video_id_search) != "") {
			$CountRec .= " AND tbl_video_id IN (SELECT tbl_video_id FROM ".TBL_VIDEO_CATEGORY." WHERE tbl_category_video_id ='$tbl_category_video_id_search' ) ";	
			$Query .= " AND tbl_video_id IN (SELECT tbl_video_id FROM ".TBL_VIDEO_CATEGORY." WHERE tbl_category_video_id ='$tbl_category_video_id_search' ) ";
		}
		
		 $Query .= " ORDER BY id $by ";
	     $Query .=" LIMIT $offset, ".TBL_VIDEO_PAGING;
   	     $q = stripslashes($q);
		 //echo $Query;
		 $total_record = CountRecords($CountRec);
		 $data = SelectMultiRecords($Query);
		  		 if ($total_record =="")
					   echo '<span class="msgColor">'.NO_RECORD_FOUND."</span>";
					else{	
					    echo '<span class="msgColor">';
					    echo " <b> ". $total_record ." </b> Video(s) found. Showing <b>";
						if ($total_record>$offset){
							echo $offset+1;
							echo " </b>of<b> ";
							if ($offset >= $total_record - TBL_VIDEO_PAGING)	{ 
								  echo $total_record; 
							}else { 
							   	echo $offset + TBL_VIDEO_PAGING ;
							}
						}else{ 
							echo $offset+1;
							echo "</b> - ". $total_record;
							echo " of ". $total_record ." Video(s) ";		
						}
						echo "</b>.</span>";	
					}
	   ?>
      <?php  if ($total_record !=""){?>
      <table width="98%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td height="18"><span class="msgColor">&nbsp;&nbsp;Note: To view detail 
            click Title.</span></td>
        </tr>
        <tr> 
          <td align="center" valign="top" class="adminDetailHeading"> <table width="100%" border="0" align="center"  cellpadding="2" cellspacing="1">
              <form name="frmDirectory" onSubmit="return valueCheckedForNewsManagement()">
                <tr align="center" > 
                  <td width="27%" height="24" align="center" class="adminDetailHeading"><span class="searchText">Title</span></td>
                  <td width="22%" height="24" align="center" class="adminDetailHeading"> Thumb Image</td>
                  <td width="8%" height="24" align="center" class="adminDetailHeading">Views</td>
                  <td width="12%" align="center" class="adminDetailHeading">Date</td>
                  <td width="8%" height="24" align="center" class="adminDetailHeading">Status</td>
                  <td width="15%" height="24" align="center" class="adminDetailHeading">Action</td>
                  <td width="8%" height="24" class="adminDetailHeading"> <input name="AC" type="checkbox" onClick="CheckAll();"></td>
                </tr>
                <?php
			 for($i=0;$i<count($data);$i++){
				$id = $data[$i]["id"];
			
				$tbl_video_id_db = $data[$i]["tbl_video_id"];
				$title_db = $data[$i]["title"];
				$title_ar_db = $data[$i]["title_ar"];
				$description_db = $data[$i]["description"];
				$description_ar_db = $data[$i]["description_ar"];
				$visits_db = $data[$i]["visits"];
				
				$video_file_name_original_db = $data[$i]["video_file_name_original"];
				$video_file_name_updated_db = $data[$i]["video_file_name_updated"];
				$video_file_size_db = $data[$i]["video_file_size"];
				$video_file_type_db = $data[$i]["video_file_type"];
				
				$image_file_name_original_db = $data[$i]["image_file_name_original"];
				$image_file_name_updated_db = $data[$i]["image_file_name_updated"];
				$image_file_size_db = $data[$i]["image_file_size"];
				$image_file_type_db = $data[$i]["image_file_type"];

				$added_date = $data[$i]["added_date"];
				$is_active = $data[$i]["is_active"];
				
				$img_path = Thumb Image_SHOW_PATH . "/".$image_file_name_updated_db;
				$video_path = VIDEO_SHOW_PATH . "/".$video_file_name_updated_db;

				//$view_comments = "";
				//$total_video_comments = total_video_comments($tbl_video_id_db);
				//$total_video_not_approved_comments = total_video_not_approved_comments($tbl_video_id_db);
				/*
				if($total_video_comments>0 || $total_video_not_approved_comments>0) {
					$view_comments = "<br>&raquo; <a title='Click here to activate/deactivate/delete comments' target='_Blank' style='color:blue' href='video_comments.php?mid=$mid&sid=$sid&tbl_video_id=$tbl_video_id_db'>View Comments</a>";
				}
				*/
			?>
                <tr valign="middle" bgcolor="#FFFFFF"> 
                  <td height="20" align="left" >&nbsp; <a href="?mid=2&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&by=<?=$by?>&id=<?=$id?>&tbl_category_video_id_search=<?=$tbl_category_video_id_search?>" title="Click to view detail" class="detailLinkColor">
                      <div class="txt_en"><?=$title_db?></div>
                      <div class="txt_ar"><?=$title_ar_db?></div>
                  </a>
                  <br>
                  	<!-- 
                    &nbsp;&nbsp;<?php echo "<span style='color:green'>Total Comments: ".$total_video_comments."</span><br>";?>
                    &nbsp;&nbsp;<?php echo "<span style='color:green'>Comments Awaiting Approval: ".$total_video_not_approved_comments."</span>$view_comments";?>
                    -->
</td>
                  <td height="20" align="center"> 
                  	<?php
                    	if (trim($image_file_name_original_db) == "") {
							//echo "No Thumb Image";
					?>
						<img src="<?=IMG_PATH?>/no_video.jpg" width="235" height="140" />
					<?		
						} else {
					?>
						<img src="<?=$img_path?>" width="235" height="140" />
					<?php		
						}
					?>
                  </td>
                  <td height="20" align="center"><?=$visits_db?></td>
                  <td align="center"><? echo date("d M, Y ",strtotime($data[$i]["added_date"]));?></td>
                  <td align="center"> 
                    <?php 
				    if($is_active == 'Y'){?>
                    <img src="<?=IMG_PATH?>/yes.gif"> 
                    <?php } else {?>
                    <img src="<?=IMG_PATH?>/no.gif"> 
                    <?php }  ?>
                  </td>
                  <td align="center"> <a href="?mid=3&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&by=<?=$by?>&id=<?=$id?>&tbl_category_video_id_search=<?=$tbl_category_video_id_search?>" class="detailLinkColor">Edit</a> 
                  </td>
                  <td align="center" > <input name="EditBox[]" type="checkbox" onClick="UnCheckAll();" value="<?=$id?>"> 
                  </td>
                </tr>
                <?php }?>
                <tr> 
                  <td height="20" colspan="8" align="center" valign="middle" bgcolor="#FFFFFF">
                    <input name="reset" type="reset" class="flat"  value="Reset"> 
                    <input name="show" type="submit" class="flat" id="ssubmit" value="Active"> 
                    <input name="hide" type="submit" class="flat"  value="Deactive"> 
                    <input name="delete" type="submit" class="flat"  value="Delete"> 
                    <input name="ssubmit" type="hidden" id="ssubmit" value="newsDetail"> 
                    <input name="mid" type="hidden" id="mid2" value="1"> <input name="sid" type="hidden" id="sid" value="<?=$sid?>"> 
                    <input name="offset" type="hidden" id="offset" value="<?=$offset?>"> 
                    <input name="q" type="hidden" id="q" value="<? echo htmlspecialchars($q);?>"> 
                    <input name="id" type="hidden" value="<?=$id?>"> <input name="sort" type="hidden" id="sort" value="<?=$sort?>"> 
                    <input name="by" type="hidden" id="by" value="<?=$by?>"> <input name="field" type="hidden" id="field" value="<?=$field?>"> 
                    <input name="LIKE" type="hidden" value="<?=$LIKE?>"></td>
                </tr>
                <tr align="right"> 
                  <td height="20" colspan="8" valign="middle" bgcolor="#FFFFFF"> 
                    <?php $url = "video_mgmt.php?mid=1&sid=$sid&field=$field&q=$q&LIKE=$LIKE&by=$by&tbl_category_video_id_search=$tbl_category_video_id_search";
	 	 getPaging($offset, $total_record, TBL_VIDEO_PAGING, $url); ?>
                  </td>
                </tr>
              </form>
            </table></td>
        </tr>
      </table>
      <br> 
      <?php } ?>
      <br> <br> <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >
        <tr > 
          <td height="22" align="center" valign="middle"  class="adminDetailHeading" > 
            <strong>&nbsp;Add Video(s)</strong></td>
        </tr>
        <tr > 
          <td align="center" valign="top" class="bordLightBlue"> <form name="frmRegister" method="post" action="" onSubmit="return validateForm()"  enctype="multipart/form-data">
              <table width="100%" border="0" align="center" cellpadding="4" cellspacing="4" >
                <tr align="center" valign="top" > 
                  <td colspan="2"></td>
                </tr>
                <tr align="left" valign="top" > 
                  <td colspan="2"><span class="msgColor">Fields marked with * are mandatory!</span></td>
                </tr>
                <tr valign="middle" > 
                  <td width="22%" align="right" valign="top"><span class="msgColor">*</span>Category: 
                  </td>
                  <td width="78%" align="left" valign="top"> 
                  <?php
				  	$qry_cat = "SELECT * FROM ".TBL_CATEGORY_VIDEO." WHERE is_active='Y' AND tbl_school_id='$tbl_school_id_sess' ORDER BY category_name ";
					//echo $qry_cat."<br />";
					$rs_cat = SelectMultiRecords($qry_cat);
				  ?>
                  <select name="tbl_category_video_id[]" size="10" multiple id="tbl_category_video_id[]">
                  		<?php
                        for ($j=0; $j<count($rs_cat); $j++) { 
							$tbl_video_category_id_j = $rs_cat[$j]['tbl_video_category_id'];
							$category_name_j = $rs_cat[$j]['category_name'];
							$category_name_ar_j = $rs_cat[$j]['category_name_ar'];
						?>
							<option value="<?=$tbl_video_category_id_j?>" <?php if(trim($tbl_video_category_id_j) == $tbl_video_category_id) {echo "selected";}?> ><?=$category_name_j?> [::] <?=$category_name_ar_j?></option>
                        <?php
						}
						?>    
                  </select> 
                  <strong>Ctrl+Click</strong> to select multiple categories</td>
                </tr>
                <tr valign="middle" > 
                  <td width="22%" align="right" valign="top"><span class="msgColor">*</span>Title: 
                  </td>
                  <td width="78%" align="left" valign="top"> 
                  <input name="title" type="text" class="flat" id="title" maxlength="70" value="<?=$title?>"></td>
                </tr>
                <tr valign="middle" > 
                  <td width="22%" align="right" valign="top"><span class="msgColor">*</span>Title Arabic: 
                  </td>
                  <td width="78%" align="left" valign="top"> 
                  <input dir="rtl" name="title_ar" type="text" class="flat" id="title_ar" maxlength="70" value="<?=$title_ar?>"></td>
                </tr>
                <tr valign="middle" >
                  <td height="" align="right" valign="top">Description: </td>
                  <td align="left" valign="top"><textarea name="description" id="description" cols="45" rows="5"><?=$description?></textarea></td>
                </tr>
                <tr valign="middle" >
                  <td height="" align="right" valign="top">Description Arabic: </td>
                  <td align="left" valign="top"><textarea dir="rtl" name="description_ar" id="description_ar" cols="45" rows="5"><?=$description_ar?></textarea></td>
                </tr>
                <tr valign="middle" > 
                  <td width="22%" height="" align="right" valign="top">Thumb Image: </td>
                  <td width="78%" align="left" valign="top"><input type="file" name="file_thumb" class="flat" id="file_thumb"> 
                  [235 X 140]</td>
                </tr>
                <tr valign="middle" >
                  <td height="" align="right" valign="top">&nbsp;</td>
                  <td align="left" valign="top">&nbsp;</td>
                </tr>
                <tr valign="middle" > 
                  <td width="22%" height="" align="right" valign="top" bgcolor="#E8E8E8"><span class="msgColor">*</span>Video: </td>
                  <td width="78%" align="left" valign="top" bgcolor="#E8E8E8"><input type="file" name="file_video" class="flat" id="file_video">
                  Supported file format is<strong> .mp4</strong></td>
                </tr>
                <tr valign="middle" >
                  <td align="right" valign="top" bgcolor="#E8E8E8">-- OR--</td>
                  <td align="left" valign="top" bgcolor="#E8E8E8">&nbsp;</td>
                </tr>
                <tr valign="middle" >
                  <td align="right" valign="top" bgcolor="#E8E8E8"><span class="msgColor">*</span>Video Name:</td>
                  <td align="left" valign="top" bgcolor="#E8E8E8"><input type="text" name="video_name" id="video_name"> 
                  Include the extension</td>
                </tr>
                <tr valign="middle" >
                  <td align="right" valign="top">&nbsp;</td>
                  <td align="left" valign="top">&nbsp;</td>
                </tr>
                <tr valign="middle" > 
                  <td align="right" valign="top">Status:</td>
                  <td align="left" valign="top"> <input name="is_active" type="radio" value="Y" checked <?php if($data["is_active"]=='Y'){echo "checked";}?>>
                    Yes 
                    <input type="radio" name="is_active" value="N" <?php if($data["is_active"]=='N'){echo "checked";}?>>
                    No </td>
                </tr>
                <tr valign="middle" > 
                  <td colspan="2" align="center"> 
    	              <?php
	    	    	  	$tbl_video_id = substr(md5(uniqid(rand())),0,15);
					  ?>
                    <input type="hidden" name="tbl_video_id" value="<?=$tbl_video_id?>">  
                    <input type="reset" name="Reset" value="Reset" class="flat"> 
                    <input name="ssubmit" type="submit" class="flat" id="ssubmit"  value="Submit"> 
                    <input name="ssubmit" type="hidden" id="ssubmit" value="insertNews"> 
                    <input name="sid" type="hidden" value="<?=$sid?>"> <input name="mid" type="hidden" value="1"> 
                    <input name="offset" type="hidden" id="offset" value="<?=$offset;?>"> 
                    <input name="q" type="hidden" id="q" value="<?php echo htmlspecialchars($q);?>"> 
                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>"> 
                    <input name="by" type="hidden" id="by" value="<?=$by?>"> <input name="field" type="hidden" id="field" value="<?=$field?>"> 
                    <input name="LIKE" type="hidden" value="<?=$LIKE?>"> </td>
                </tr>
                <tr valign="middle" > 
                  <td colspan="2" align="center"> </tr>
              </table>
          </form></td>
        </tr>
      </table></td>
  </tr>
</table>
<?php }if($mid=="2"){ 
	$data = selectFrom("SELECT * FROM ".TBL_VIDEO." WHERE id=$id");	 

		$tbl_video_id = $data["tbl_video_id"];
		$title = $data["title"];
		$title_ar = $data["title_ar"];
		$speaker = $data["speaker"]; if(trim($speaker) == "") {$speaker = "Not Specified";}
		$description = $data["description"]; //if(trim(strip_tags($description)) == "") {$description = "Not Specified";}
		$description_ar = $data["description_ar"]; //if(trim(strip_tags($description)) == "") {$description = "Not Specified";}
		$visits = $data["visits"];
	
		$video_file_name_original = $data["video_file_name_original"];
		$video_file_name_updated = $data["video_file_name_updated"];
		$video_file_size = $data["video_file_size"];
		$video_file_type = $data["video_file_type"];
		
		$image_file_name_original = $data["image_file_name_original"];
		$image_file_name_updated = $data["image_file_name_updated"];
		$image_file_size = $data["image_file_size"];
		$image_file_type = $data["image_file_type"];
	
		$added_date = $data["added_date"];
		$is_active = $data["is_active"];
		
		$img_path = Thumb Image_SHOW_PATH . "/".$image_file_name_updated;
		$video_path = VIDEO_SHOW_PATH . "/".$video_file_name_updated;
		
?>
<table width="100%"  border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr> 
    <td height="30" valign="center"  class="adminDetailHeading">&nbsp;Video(s) Management 
      [Detail]</td>
  </tr>
  <tr> 
    <td height="200" align="center" valign="top"> <br> <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >
        <tr > 
          <td height="22" align="center" valign="middle"  class="adminDetailHeading">&nbsp;Video(s) 
            Detail</td>
        </tr>
        <tr > 
          <td align="center" valign="top" class="bordLightBlue"><table width="100%" border="0" align="center" cellpadding="3" cellspacing="0" >
              <tr align="center" valign="top" > 
                <td colspan="2">&nbsp;</td>
              </tr>
                  <?php
				  	$qry_video_cat = "SELECT * FROM ".TBL_VIDEO_CATEGORY." WHERE tbl_video_id='$tbl_video_id' AND is_active='Y' ";
					//echo $qry_video_cat."<br />";
					$rs_video_cat = SelectMultiRecords($qry_video_cat);
					
					//print_r($rs_video_cat);
				  ?>
<!--              <tr valign="top" >
                <td height="26" align="right"><strong>Views:</strong></td>
                <td><?=$visits?> 
                </td>
              </tr>-->
              <tr valign="top" >
                <td align="right"><strong>Category:</strong></td>
                <td>
					<?php
                    for ($jj=0; $jj<count($rs_video_cat); $jj++) { 
                        $tbl_video_id_jj = $rs_video_cat[$jj]['tbl_video_id'];
                        $tbl_category_video_id_jj = $rs_video_cat[$jj]['tbl_category_video_id'];
						
						//echo $tbl_category_video_id_jj;
						if (count($rs_video_cat) == 1) {
							echo get_video_catname($tbl_category_video_id_jj)."<br />";						
						} else {
							echo ($jj+1)."- ".get_video_catname($tbl_category_video_id_jj)."<br />";	
						}
						
                    }
                    ?>    
                </td>
              </tr>
              <tr valign="top" > 
                <td width="23%" align="right"><strong> Title: </strong></td>
                <td width="77%"><?=$title?> </td>
              </tr>
              <tr valign="top" > 
                <td width="23%" align="right"><strong> Title Arabic: </strong></td>
                <td width="77%"><?=$title_ar?> </td>
              </tr>
              <tr valign="top" > 
                <td width="23%" align="right"><strong> Description: </strong></td>
                <td width="77%"><?=$description?> </td>
              </tr>
              <tr valign="top" > 
                <td width="23%" align="right"><strong> Description Arabic: </strong></td>
                <td width="77%"><?=$description_ar?> </td>
              </tr>
              <tr valign="top" > 
                <td width="23%" align="right"><strong> Thumb Image: </strong></td>
                <td width="77%">
                  	<?php
                    	if (trim($image_file_name_original) == "") {
					?>
						<img src="<?=IMG_PATH?>/no_video.jpg" width="235" height="140" />
					<?		
						} else {
					?>
						<img src="<?=$img_path?>" width="235" height="140" />
					<?php		
						}
					?>
                 </td>
              </tr>
              <tr valign="top" > 
                <td width="23%" align="right"><strong> Video: </strong></td>
                <td width="77%">
                	<video width="320" height="240" controls>
                      <source src="<?=$video_path?>" type="<?=$video_file_type?>">
                      Your browser does not support the video.
                    </video> 
                </td>
              </tr>
              <tr valign="top" > 
                <td align="right"><strong>Status:</strong></td>
                <td> 
					<?php 
				    if($is_active == 'Y'){ ?>
                    <img src="<?=IMG_PATH?>/yes.gif"> 
                    <?php } else { ?>
                    <img src="<?=IMG_PATH?>/no.gif"> 
                    <?php } ?>                  
                </td>
              </tr>
              <tr align="center" valign="top" > 
                <td align="right"><strong>Date:</strong></td>
                <td align="left"><?php echo date("d M, Y",strtotime($data["added_date"]));?></td>
              </tr>
              <tr align="center" valign="top" > 
                <td align="right">&nbsp;</td>
                <td align="left">&nbsp;</td>
              </tr>
              <tr valign="middle" > 
                <td colspan="2" align="center"> 
                  <?php $id=$data["id"];?>
                  <a href="video_mgmt.php?mid=1&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&sort=<?=$sort?>&by=<?=$by?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&tbl_category_video_id_search=<?=$tbl_category_video_id_search?>" class="detailLinkColor"> 
                  Back </a> </td>
              </tr>
            </table></td>
        </tr>
      </table>
      <br></td>
  </tr>
</table>
<?php }if($mid=="3"){ 
	$data = selectFrom("SELECT * FROM ".TBL_VIDEO." WHERE id=$id");

		$id = $data["id"];
		$tbl_video_id = $data["tbl_video_id"];
		$title = $data["title"];
		$title_ar = $data["title_ar"];
		$speaker = $data["speaker"];
		$description = $data["description"];
		$description_ar = $data["description_ar"];
	
		$video_file_name_original = $data["video_file_name_original"];
		$video_file_name_updated = $data["video_file_name_updated"];
		$video_file_size = $data["video_file_size"];
		$video_file_type = $data["video_file_type"];
		
		$image_file_name_original = $data["image_file_name_original"];
		$image_file_name_updated = $data["image_file_name_updated"];
		$image_file_size = $data["image_file_size"];
		$image_file_type = $data["image_file_type"];
	
		$added_date = $data["added_date"];
		$is_active = $data["is_active"];
		
		$img_path = Thumb Image_SHOW_PATH . "/".$image_file_name_updated;
		$video_path = VIDEO_SHOW_PATH . "/".$video_file_name_updated;
		
?>
<table width="100%" height="451" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr> 
    <td height="30" valign="center" class="adminDetailHeading">&nbsp;Video(s) Management 
      [Edit] </td>
  </tr>
  <tr> 
    <td align="center" valign="top"> <br>
      <span class="msgColor"><?php echo $MSG;?></span> <br> <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >
        <tr > 
          <td height="22" align="center" valign="middle"  class="adminDetailHeading" >&nbsp;Edit 
            Video(s)</td>
        </tr>
        <tr > 
          <td align="left" valign="top" class="bordLightBlue"> <form name="frmRegister" method="post" action="" onSubmit="return validateFormEdit()" enctype="multipart/form-data">
              <table width="100%" border="0" align="center" cellpadding="4" cellspacing="4" >
                <tr align="center" valign="top" > 
                  <td colspan="2"></td>
                </tr>
                <tr align="left" valign="top" > 
                  <td colspan="2"><span class="msgColor">Fields marked with * are mandatory!</span></td>
                </tr>
                <tr valign="middle" > 
                  <td width="22%" align="right" valign="top"><span class="msgColor">*</span>Category: 
                  </td>
                  <td width="78%" align="left" valign="top"> 
                  <?php
				  	$qry_cat = "SELECT * FROM ".TBL_CATEGORY_VIDEO." WHERE is_active='Y' AND tbl_school_id='$tbl_school_id_sess' ORDER BY category_name ";
					$rs_cat = SelectMultiRecords($qry_cat);
				  ?>
                  <?php
					$cat_arr = array();
					$qry_video_cat = "SELECT tbl_category_video_id  FROM ".TBL_VIDEO_CATEGORY." WHERE tbl_video_id='$tbl_video_id' AND is_active='Y' ";
					//echo $qry_video_cat;
					$rs_video_cat = SelectMultiRecords($qry_video_cat);
					//print_r($rs_video_cat);

					for ($k=0; $k<count($rs_video_cat); $k++) {
						$cat_arr[$k] = $rs_video_cat[$k]['tbl_category_video_id'];
						
					}
				  ?>
                  <select name="tbl_category_video_id[]" size="10" multiple id="tbl_category_video_id[]">
                  		<?php
                        for ($j=0; $j<count($rs_cat); $j++) { 
							$tbl_video_category_id_j = $rs_cat[$j]['tbl_video_category_id'];
							$category_name_j = $rs_cat[$j]['category_name'];
							$category_name_ar_j = $rs_cat[$j]['category_name_ar'];
						?>
							<option value="<?=$tbl_video_category_id_j?>" <?php if(in_array(trim($tbl_video_category_id_j),$cat_arr)) {echo "selected='selected'";}?> ><?=$category_name_j?> [::] <?=$category_name_ar_j?></option>
                        <?php
						}
						?>    
                  </select> 
                  <strong>Ctrl+Click</strong> to select multiple categories</td>
                </tr>
                <tr valign="middle" > 
                  <td width="22%" align="right" valign="top"><span class="msgColor">*</span>Title: 
                  </td>
                  <td width="78%" align="left" valign="top"> 
                    <input name="title" type="text" class="flat" id="title" maxlength="70" value="<?=$title?>"></td>
                </tr>
                <tr valign="middle" > 
                  <td width="22%" align="right" valign="top"><span class="msgColor">*</span>Title Arabic: 
                  </td>
                  <td width="78%" align="left" valign="top"> 
                    <input dir="rtl" name="title_ar" type="text" class="flat" id="title_ar" maxlength="70" value="<?=$title_ar?>"></td>
                </tr>
<!--                
                <tr valign="middle" > 
                  <td width="22%" align="right" valign="top"><span class="msgColor">*</span>Speaker: 
                  </td>
                  <td width="78%" align="left" valign="top"> 
                  <input name="speaker" type="text" class="flat" id="speaker" maxlength="70" value="<?=$speaker?>"></td>
                </tr>
-->                
                <tr valign="middle" >
                  <td height="" align="right" valign="top">Description: </td>
                  <td align="left" valign="top"><textarea name="description" id="description" cols="45" rows="5"><?=$description?></textarea></td>
                </tr>
                <tr valign="middle" >
                  <td height="" align="right" valign="top">Description Arabic: </td>
                  <td align="left" valign="top"><textarea dir="rtl" name="description_ar" id="description_ar" cols="45" rows="5"><?=$description_ar?></textarea></td>
                </tr>
                <tr valign="middle" > 
                  <td width="22%" height="" align="right" valign="top">Thumb Image: </td>
                  <td width="78%" align="left" valign="top">
              
					<?php
                    	if (trim($image_file_name_original) == "") {
					?>
						<img src="<?=IMG_PATH?>/no_video.jpg" width="235" height="140" />
					<?		
						} else {
					?>
						<img src="<?=$img_path?>" width="235" height="140" />
					<?php		
						}
					?>
<br>

                  <br>
				<input type="file" name="file_thumb" class="flat" id="file_thumb">  
                  [235 X 140]

                	<br>
                  <input type="checkbox" name="remove_thumb" id="remove_thumb" value="Y"> Delete Thumb Image
                  </td>
                </tr>
                <tr valign="middle" > 
                  <td width="22%" height="" align="right" valign="top"><span class="msgColor">*</span>Video: </td>
                  <td width="78%" align="left" valign="top">
                    <video width="320" height="240" controls>
                      <source src="<?=$video_path?>" type="<?=$video_file_type?>">
                      Your browser does not support the video.
                    </video> 
                    <br>
                  <input type="file" name="file_video" class="flat" id="file_video">
                  Supported file format is<strong> .mp4</strong></td>
                </tr>
                <tr valign="middle" > 
                  <td align="right" valign="top">Status:</td>
                  <td align="left" valign="top"> <input name="is_active" type="radio" value="Y" checked <?php if($data["is_active"]=='Y'){echo "checked";}?>>
                    Yes 
                    <input type="radio" name="is_active" value="N" <?php if($data["is_active"]=='N'){echo "checked";}?>>
                    No </td>
                </tr>
                <tr valign="middle" > 
                  <td colspan="2" align="center"> 
    	              <a href="video_mgmt.php?mid=1&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&sort=<?=$sort?>&by=<?=$by?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&tbl_category_video_id_search=<?=$tbl_category_video_id_search?>" class="detailLinkColor"> 
                    Back</a>&nbsp; 
                    
                    <input name="tbl_category_video_id_search" type="hidden" class="flat" id="tbl_category_video_id_search" value="<?=$tbl_category_video_id_search?>"> 
                    <input name="Reset" type="reset" class="flat" id="Reset" value="Reset Values"> 
                    <input name="ssubmit" type="submit" class="flat" id="ssubmit"  value="Save Changes"> 
                    <input name="ssubmit" type="hidden" id="ssubmit" value="editNews"> 
                    <input name="sid" type="hidden" id="sid" value="<?=$sid?>"> 
                    <input name="mid" type="hidden" id="mid" value="1"> <input name="offset" type="hidden" id="offset" value="<?=$offset;?>"> 
                    <input name="q" type="hidden" id="q" value="<? echo htmlspecialchars($q);?>"> 
                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>"> 
                    <input name="field" type="hidden" id="field" value="<?=$field?>"> 
                    <input name="LIKE" type="hidden" id="LIKE" value="<?=$LIKE?>"> </td>
                </tr>
                <tr valign="middle" > 
                  <td colspan="2" align="center"> </tr>
              </table>
          </form></td>
        </tr>
      </table>
      <p><br>
      </p></td>
  </tr>
</table>
<?php }if($mid=="4"){ ?>
<table width="100%"  border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr> 
    <td height="30" valign="center" class="adminDetailHeading">&nbsp;Video(s) Management 
      [Del Confirmation]</td>
  </tr>
  <tr align="center" > 
    <td valign="top" > <br>
      <span class="msgColor"> Are you sure you want to delete the selected Video(s)?</span> 
      <br> 
      <table width="98%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="center" valign="top" class="adminDetailHeading"> <table width="100%" border="0" align="center"  cellpadding="2" cellspacing="1" >
              <form name="frmDirectory" onSubmit="return valueCheckedForNewsManagement();">
                <tr > 
                  <td width="34%" height="24" align="center"  class="adminDetailHeading"><span class="searchText">Title</span></td>
                  <td width="27%" align="center"  class="adminDetailHeading"> Thumb Image</td>
                  <td width="10%" align="center"  class="adminDetailHeading">Views</td>
                  <td width="19%" align="center"  class="adminDetailHeading">Date</td>
                  <td width="7%" align="center"  class="adminDetailHeading">Status</td>
                  <td width="3%" height="24" align="center"  class="adminDetailHeading"> 
                    <input name="AC" type="checkbox" id="AC" onClick="CheckAll();" checked> 
                  </td>
                </tr>
                <?php for($i=0;$i<count($EditBox);$i++){
			//echo $EditBox[$i]."<br>";
			$Query ="SELECT * FROM ".TBL_VIDEO." WHERE id=$EditBox[$i]";
			$data = selectFrom($Query);
			
				$id = $data["id"];
				$tbl_video_id = $data["tbl_video_id"];
				$title = $data["title"];
				$title_ar = $data["title_ar"];
				$description = $data["description"];
				$description_ar = $data["description_ar"];
				$visits = $data["visits"];
			
				$video_file_name_original = $data["video_file_name_original"];
				$video_file_name_updated = $data["video_file_name_updated"];
				$video_file_size = $data["video_file_size"];
				$video_file_type = $data["video_file_type"];
				
				$image_file_name_original = $data["image_file_name_original"];
				$image_file_name_updated = $data["image_file_name_updated"];
				$image_file_size = $data["image_file_size"];
				$image_file_type = $data["image_file_type"];
			
				$added_date = $data["added_date"];
				$is_active = $data["is_active"];
				
				$img_path = Thumb Image_SHOW_PATH . "/".$image_file_name_updated;
				$video_path = VIDEO_SHOW_PATH . "/".$video_file_name_updated;
				
		?>
                <tr valign="middle" bgcolor="#FFFFFF"> 
                  <td height="20" align="left">
                      <div class="txt_en"><?=$title?></div>
                      <div class="txt_ar"><?=$title_ar?></div>
                  </td>
                  <td height="20" align="center">                  	
				 	<?php
                    	if (trim($image_file_name_original) == "") {
					?>
						<img src="<?=IMG_PATH?>/no_video.jpg" width="235" height="140" />
					<?		
						} else {
					?>
						<img src="<?=$img_path?>" width="235" height="140" />
					<?php		
						}
					?>
                  </td>
                  <td align="center"><?=$visits?></td>
                  <td height="20" align="center"><? echo date("d M, Y ",strtotime($data["added_date"]));?></td>
                  <td height="20" align="center"> 
                     <?php 
		  if($is_active == 'Y'){?>
                    <img src="<?=IMG_PATH?>/yes.gif"> 
                    <?php } else {?>
                    <img src="<?=IMG_PATH?>/no.gif"> 
                    <?php }  ?></td>
                  <td height="20" align="center"> <input name="EditBox[]" type="checkbox" id="EditBox[]" onClick="UnCheckAll();" value="<?=$id?>" checked> 
                  </td>
                </tr>
                <?php }?>
                <tr> 
                  <td height="20" colspan="7" align="center" valign="middle" bgcolor="#FFFFFF" > 
                    <input name="Button" type="button" class="flat" id="hide" value="I am not sure" onClick="javascript:history.back();"> 
                    <input name="show" type="submit" class="flat" id="show" value="Yes I am sure"> 
                    <input name="sid" type="hidden" id="sid" value="<?=$sid;?>"> 
                    <input name="offset" type="hidden" id="offset" value="<?=$offset;?>"> 
                    <input name="mid" type="hidden" id="mid" value="1"> <input name="ssubmit" type="hidden" id="show" value="newsDelConfirmation"> 
                    <input name="q" type="hidden" id="q" value="<?php echo htmlspecialchars($q);?>"> 
                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>"> 
                    <input name="by" type="hidden" id="by" value="<?=$by?>"> <input name="field" type="hidden" id="field" value="<?=$field?>"> 
                    <input name="LIKE" type="hidden" value="<?=$LIKE?>"> </td>
                </tr>
              </form>
            </table></td>
        </tr>
      </table></tr>
</table>
<?php } ?>
<DIV id=dek></DIV>
<SCRIPT type=text/javascript src="<?=JS_PATH?>/popupscript.js" ></SCRIPT>
</body>
</html>
