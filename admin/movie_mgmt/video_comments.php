<?php include ($_SERVER["DOCUMENT_ROOT"]."/aqdar/includes/config.inc.php"); ?>
<?php
$sid = $_REQUEST["sid"];
if (!loggedUser($sid)) { 
	redirect (HOST_URL_ADMIN ."/include/messages.php?msg=relogin");
exit();
}
?>
<?php
$q = stripslashes($q);	
if($ssubmit=="newsDetail"){
	if(isset($show)){
		for ($j=0; $j<count($EditBox); $j++){ 
			$Querry = "UPDATE ". TBL_VIDEO_COMMENTS ." SET is_approved='Y' WHERE id=$EditBox[$j]";			
			update($Querry);
			$mid=1;	
			$MSG = "Selected Comment(s) have been activated successfully!";
		}
	}
	if(isset($hide)){
		for ($j=0; $j<count($EditBox); $j++){ 
			$Querry = "UPDATE ". TBL_VIDEO_COMMENTS ." SET is_approved='N' WHERE id=$EditBox[$j]";			
			update($Querry);
			$mid=1;	
			$MSG = "Selected Comment(s) have been deactivated successfully!";
		}
	}
	if(isset($delete)){$mid = 4;}
}

if($ssubmit == "newsDelConfirmation"){
	for($i=0;$i<count($EditBox);$i++){
		$qry_del = "DELETE FROM ".TBL_VIDEO_COMMENTS." WHERE `id`=$EditBox[$i]";
		deleteFrom($qry_del);
		$mid = 1;
		$MSG = "Selected Comment(s) have been deleted successfully!";
	}
}	
?>
<html>
<head>
<title>Admin (News Management)</title>
<link rel="stylesheet" href="<?=ADMIN_CSS_PATH?>interface.css" type="text/css">
<script src="<?=ADMIN_SCRIPT_PATH?>/validation.js">CopyDatarichtext1();</script>
<script language="JavaScript" src="calendar1.js"></script>
<script language="JavaScript">
function valueCheckedForNewsManagement(){
	var ml = document.frmDirectory;
	var len = ml.elements.length;
	for (var i = 0; i < len; i++){
	   	if (document.frmDirectory.elements[i].checked){
			return true;
		}
	}
	 alert ("Select at least one Comment!");
	 return false;
}

function CheckAll(){
	var ml = document.frmDirectory;
	var len = ml.elements.length;
	if (document.frmDirectory.AC.checked==true) {
		 for (var i = 0; i < len; i++) {
			document.frmDirectory.elements[i].checked=true;
		 }
	} else {
		  for (var i = 0; i < len; i++)  {
			document.frmDirectory.elements[i].checked=false;
		  }
	}
}
function UnCheckAll() {
	var ml = document.frmDirectory;
	var len = ml.elements.length;
	var count=0; var checked=0;
	    for (var i = 0; i < len; i++) {	       
			if ((document.frmDirectory.elements[i].type=='checkbox') && (document.frmDirectory.elements[i].name != "AC")) {
				count = count + 1;
	  		 	if (document.frmDirectory.elements[i].checked == true){
					checked = checked + 1;
				}
			}
	     }
		 
	if (checked == count) {
		 document.frmDirectory.AC.checked = true;
	} else {
		document.frmDirectory.AC.checked = false;
	}
}
</script>
</head>
<?php if (!$offset || $offset<0)  { $offset =0;} ?>
<?php if (!$LIKE)  { $LIKE = "LIKE";} ?>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<STYLE type=text/css>#dek {
	Z-INDEX: 200; VISIBILITY: hidden; POSITION: absolute
}
</STYLE>
<?php if($mid=="1"){ ?>
<table width="100%" height="570" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr> 
    <td height="30" valign="center" class="adminDetailHeading">&nbsp;Comments Management 
    </td>
  </tr>
  <tr align="center" > 
    <td height="538" align="center" valign="top" > <form name="form1" method="post" action="">
        <br>
        <table width="80%" border="0" align="center" cellpadding="0" cellspacing="1" >
          <tr > 
            <td height="22" align="left" valign="middle" class="adminDetailHeading">&nbsp;Search 
              Criteria</td>
          </tr>
          <tr > 
            <td align="center" valign="top" class="bordLightBlue"><table width="100%" border="0" cellspacing="3" cellpadding="2">
                <tr> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr> 
                  <td width="32%">&nbsp;</td>
                  <td width="48%"> <a href="video_comments.php?mid=<?=$mid?>&sid=<?=$sid?>&tbl_video_id=<?=$tbl_video_id?>"><span class="detailLinkColor">All 
                    Records</span></a>: or&nbsp;</td>
                  <td width="20%"><i></i></td>
                </tr>
                <tr> 
                  <td colspan="3"><hr width="80%" size="1"></td>
                </tr>
                <tr> 
                  <td align="right" valign="top" ><span class="searchText">Field:&nbsp;</span></td>
                  <td align="left" valign="top"> <select name="field" id="field">
                      <option value="full_name" <?PHP if ($field == "full_name"){echo "selected";}?>>Full Name</option>
                      <option value="email" <?PHP if ($field == "email"){echo "selected";}?>>Email</option>
                      <option value="comments" <?PHP if ($field == "comments"){echo "selected";}?>>Comments</option>
                      <option value="added_date" <?PHP if ($field == "added_date"){echo "selected";}?>>Date e.g [YYYY-MM-DD]</option>
                      <option value="is_active"<?PHP if($field == "is_active"){echo "selected";}?>>Status e.g Y=Activate , N=Deactivate</option></option>
                      </select></td>
                  <td><i><span onMouseOver="window.status='';do_info(22);return true" onmouseout=kill() href="javascript:void(0)";><img src="<?=HOST_URL?>/images/help_form.gif"></span></i></td>
                </tr>
                <tr> 
                  <td align="right" valign="top"><span class="searchText">Condition:&nbsp;</span></td>
                  <td align="left" valign="top"> 
                    <?php
							if (!$LIKE) {
								$LIKE = "LIKE";
							}
						?>
                    <select name="LIKE" id="LIKE">
                      <option value="LIKE" <?PHP if ($LIKE == "LIKE"){echo "selected";}?>>LIKE</option>
                      <option value="=" <?PHP if (($LIKE == "=") || ($LIKE == "")){echo "selected";}?>>Equal 
                      To</option>
                      <option value="!=" <?PHP if ($LIKE == "!="){echo "selected";}?>>Not 
                      Equal To</option>
                      <option value="<" <?PHP if ($LIKE == "<"){echo "selected";}?>>Less 
                      Than</option>
                      <option value=">" <?PHP if ($LIKE == ">"){echo "selected";}?>>Greater 
                      Than</option>
                      <option value="<=" <?PHP if ($LIKE == "<="){echo "selected";}?>>Less 
                      Than or Equal To</option>
                      <option value=">=" <?PHP if ($LIKE == ">="){echo "selected";}?>>Greater 
                      Than or Equal To</option>
                    </select></td>
                  <td><i><span onMouseOver="window.status='';do_info(23);return true" onmouseout=kill() href="javascript:void(0)";><img src="<?=HOST_URL?>/images/help_form.gif"></span></i></td>
                </tr>
                <tr> 
                  <td align="right" valign="top"><span class="searchText">Search 
                    Name:&nbsp;</span></td>
                  <td align="left" valign="top"> <input name="q" type="text" id="q" value="<?php echo htmlspecialchars($q);?>" size="40"> 
                  </td>
                  <td><i><span onMouseOver="window.status='';do_info(24);return true" onmouseout=kill() href="javascript:void(0)";><img src="<?=HOST_URL?>/images/help_form.gif"></span></i></td>
                </tr>
                <tr> 
                  <td align="right" valign="top"><span class="searchText">Order 
                    By:&nbsp;</span></td>
                  <td align="left" valign="top"> <select name="by" id="by">
                      <option value="ASC" <?PHP if (($by == "ASC")) { echo "selected";}?>>ASCENDING</option>
                      <option value="DESC" <?PHP if ($by == "DESC" || $by == ""){ echo "selected";}?>>DESCENDING</option>
                    </select> &nbsp;</td>
                  <td><i><span onMouseOver="window.status='';do_info(25);return true" onmouseout=kill() href="javascript:void(0)";><img src="<?=HOST_URL?>/images/help_form.gif"></span></i></td>
                </tr>
                <tr> 
                  <td colspan="3" style="color:#CC0000">&nbsp;</td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                  <td align="left"> 
                    <input name="mid" type="hidden" value="1"> 
                    <input name="offset" type="hidden" value="0"> 
                    <input type="hidden" name="tbl_video_id" value="<?=$tbl_video_id?>">
                    <input name="Search" type="submit" id="Search" value="Search" class="flat"> 
                    &nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr> 
                  <td colspan="3" style="color:#CC0000">&nbsp;</td>
                </tr>
              </table></td>
          </tr>
        </table>
      </form>
      <br>
      <span class="msgColor"><?php echo $MSG;?></span> <br> 
      <?php 
		$q = addslashes($q);
		$CountRec .= "SELECT * FROM ".TBL_VIDEO_COMMENTS." WHERE tbl_video_id='$tbl_video_id' ";		
		$Query .= "SELECT * FROM ".TBL_VIDEO_COMMENTS." WHERE tbl_video_id='$tbl_video_id' ";
		if(!$by){$by="DESC";}
		
		if($field && !empty($q)){	    	
			
			if($LIKE=="LIKE"){
					$CountRec .= " AND `$field` $LIKE '%$q%' ";
					$Query .= " AND `$field` $LIKE '%$q%' ";
			}else{
					$CountRec .= " AND `$field` $LIKE '$q' ";	
					$Query .= " AND `$field` $LIKE '$q' ";
			}
		}	
		$Query .= " ORDER BY id $by ";
		$Query .=" LIMIT $offset, ".TBL_VIDEO_PAGING;
		$q = stripslashes($q);
		 //echo $Query;
		 $total_record = CountRecords($CountRec);
		 $data = SelectMultiRecords($Query);
		  		 if ($total_record =="")
					   echo '<span class="msgColor">'.NO_RECORD_FOUND."</span>";
					else{	
					    echo '<span class="msgColor">';
					    echo " <b> ". $total_record ." </b> Comment(s) found. Showing <b>";
						if ($total_record>$offset){
							echo $offset+1;
							echo " </b>of<b> ";
							if ($offset >= $total_record - TBL_VIDEO_PAGING)	{ 
								  echo $total_record; 
							}else { 
							   	echo $offset + TBL_VIDEO_PAGING ;
							}
						}else{ 
							echo $offset+1;
							echo "</b> - ". $total_record;
							echo " of ". $total_record ." Comment(s) ";		
						}
						echo "</b>.</span>";	
					}
	   ?>
      <?php  if ($total_record !=""){?>
      <table width="98%" border="0" cellspacing="0" cellpadding="0">
<!--        <tr> 
          <td height="18"><span class="msgColor">&nbsp;&nbsp;Note: To view detail 
            click a Heading.</span></td>
        </tr>
-->        <tr> 
          <td align="center" valign="top" class="adminDetailHeading"> <table width="100%" border="0" align="center"  cellpadding="2" cellspacing="1">
              <form name="frmDirectory" onSubmit="return valueCheckedForNewsManagement()">
                <tr align="center" > 
                  <td width="27%" height="24" align="center" class="adminDetailHeading">Full Name</td>
                  <td width="22%" height="24" align="center" class="adminDetailHeading">Email</td>
                  <td width="15%" height="24" align="center" class="adminDetailHeading">Comments</td>
                  <td width="20%" height="24" align="center" class="adminDetailHeading">Date  </td>
                  <td width="13%" height="24" align="center" class="adminDetailHeading">Status</td>
                  <td width="3%" height="24" class="adminDetailHeading"> <input name="AC" type="checkbox" onClick="CheckAll();"></td>
                </tr>
                <?php
			 for ($i=0;$i<count($data);$i++) {
				$id = $data[$i]["id"];
				$tbl_video_comments_id 	 = $data[$i]['tbl_video_comments_id 	'];
				//$tbl_video_id = $data[$i]['tbl_video_id'];
				$full_name = urldecode($data[$i]['full_name']);
				$email = $data[$i]['email'];
				$comments = urldecode($data[$i]['comments']);
				$added_date = $data[$i]['added_date'];
				$is_approved = $data[$i]['is_approved'];
				
				$added_date = date("d F Y ",strtotime($added_date));
			?>
                <tr valign="middle" bgcolor="#FFFFFF"> 
                  <td height="20" align="left" ><span style="padding-left:2px">
                    <?php echo $full_name;?></span>
                    </td>
                  <td height="20" align="left"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td width="5%">&nbsp;</td>
                        <td>
                         <?=$email?>
                        </td>
                      </tr>
                    </table></td>
                  <td height="20" align="center" >&nbsp; 
                    <span style="padding-left:2px"><?=$comments?></span>
                  </td>
                  <td height="20" align="center" ><?=$added_date;?></td>
                  <td align="center"> 
                    <?php 
		  if($is_approved == 'Y'){?>
                    <img src="<?=IMG_PATH?>/yes.gif"> 
                    <?php } else {?>
                    <img src="<?=IMG_PATH?>/no.gif"> 
                    <?php }  ?>
                  </td>
                  <td align="center" > <input name="EditBox[]" type="checkbox" onClick="UnCheckAll();" value="<?=$id?>"> 
                  </td>
                </tr>
                <?php }?>
                <tr> 
                  <td height="20" colspan="7" align="center" valign="middle" bgcolor="#FFFFFF"> 
                    <input type="hidden" name="tbl_video_id" value="<?=$tbl_video_id?>">
                    <input name="reset" type="reset" class="flat"  value="Reset"> 
                    <input name="show" type="submit" class="flat" id="ssubmit" value="Active"> 
                    <input name="hide" type="submit" class="flat"  value="Deactive"> 
                    <input name="delete" type="submit" class="flat"  value="Delete"> 
                    <input name="ssubmit" type="hidden" id="ssubmit" value="newsDetail"> 
                    <input name="mid" type="hidden" id="mid2" value="1"> <input name="sid" type="hidden" id="sid" value="<?=$sid?>"> 
                    <input name="offset" type="hidden" id="offset" value="<?=$offset?>"> 
                    <input name="q" type="hidden" id="q" value="<? echo htmlspecialchars($q);?>"> 
                    <input name="id" type="hidden" value="<?=$id?>"> <input name="sort" type="hidden" id="sort" value="<?=$sort?>"> 
                    <input name="by" type="hidden" id="by" value="<?=$by?>"> <input name="field" type="hidden" id="field" value="<?=$field?>"> 
                    <input name="LIKE" type="hidden" value="<?=$LIKE?>"></td>
                </tr>
                <tr align="right"> 
                  <td height="20" colspan="7" valign="middle" bgcolor="#FFFFFF"> 
                    <?php $url = "video_comments.php?mid=1&sid=$sid&field=$field&q=$q&LIKE=$LIKE&by=$by&tbl_video_id=$tbl_video_id";
	 	 getPaging($offset, $total_record, TBL_VIDEO_PAGING, $url); ?>
                  </td>
                </tr>
              </form>
            </table></td>
        </tr>
      </table>
      <br> 
      <?php } ?>
 </td>
  </tr>
</table>
<?php }if($mid=="2"){ 
?>

<?php }if($mid=="3"){ 
?>
<?php }if($mid=="4"){ ?>
<table width="100%"  border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr> 
    <td height="30" valign="center" class="adminDetailHeading">&nbsp;Comments Management 
      [Del Confirmation]</td>
  </tr>
  <tr align="center" > 
    <td valign="top" > <br>
      <span class="msgColor"> Are you sure you want to delete the selected Comment(s)?</span> 
      <br> <table width="98%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="center" valign="top" class="adminDetailHeading"> <table width="100%" border="0" align="center"  cellpadding="2" cellspacing="1" >
              <form name="frmDirectory" onSubmit="return valueCheckedForNewsManagement();">
                <tr > 
                  <td width="34%" height="24" align="center"  class="adminDetailHeading">Full Name</td>
                  <td width="21%" align="center"  class="adminDetailHeading">Email</td>
                  <td width="16%" align="center"  class="adminDetailHeading">Comments</td>
                  <td width="19%" align="center"  class="adminDetailHeading">Date  </td>
                  <td width="7%" align="center"  class="adminDetailHeading">Status</td>
                  <td width="3%" height="24" align="center"  class="adminDetailHeading"> 
                    <input name="AC" type="checkbox" id="AC" onClick="CheckAll();" checked> 
                  </td>
                </tr>
                <?php for($i=0;$i<count($EditBox);$i++){
			//echo $EditBox[$i]."<br>";
			$Query ="SELECT * FROM ".TBL_VIDEO_COMMENTS." WHERE id=$EditBox[$i]";
			$data = selectFrom($Query);
			
				$id_nn = $data['id'];
				$tbl_video_comments_id 	 = $data['tbl_video_comments_id 	'];
				//$tbl_video_id = $data['tbl_video_id'];
				$full_name = $data['full_name'];
				$email = $data['email'];
				$comments = $data['comments'];
				$added_date = $data['added_date'];
				$is_approved = $data['is_approved'];
				
				$added_date = date("d F Y ",strtotime($added_date));
		?>
                <tr valign="middle" bgcolor="#FFFFFF"> 
                  <td height="20" align="left"> &nbsp;<?=$full_name;?> 
                  </td>
                  <td height="20" align="left">&nbsp; 
                    <?=$email?>
                  </td>
                  <td height="20" align="center"> &nbsp;<span style="padding-left:2px">
                    <?=$comments?>
                  </span></td>
                  <td height="20" align="center"><?=$added_date;?></td>
                  <td height="20" align="center"><?php 
		  if($is_approved == 'Y'){?>
                    <img src="<?=IMG_PATH?>/yes.gif">
                    <?php } else {?>
                    <img src="<?=IMG_PATH?>/no.gif">
                  <?php }  ?></td>
                  <td height="20" align="center"> <input name="EditBox[]" type="checkbox" id="EditBox[]" onClick="UnCheckAll();" value="<?=$id_nn?>" checked> 
                  </td>
                </tr>
                <?php }?>
                <tr> 
                  <td height="20" colspan="7" align="center" valign="middle" bgcolor="#FFFFFF" > 
                    <input type="hidden" name="tbl_video_id" value="<?=$tbl_video_id?>">
                    <input name="Button" type="button" class="flat" id="hide" value="I am not sure" onClick="javascript:history.back();"> 
                    <input name="show" type="submit" class="flat" id="show" value="Yes I am sure"> 
                    <input name="sid" type="hidden" id="sid" value="<?=$sid;?>"> 
                    <input name="offset" type="hidden" id="offset" value="<?=$offset;?>"> 
                    <input name="mid" type="hidden" id="mid" value="1"> <input name="ssubmit" type="hidden" id="show" value="newsDelConfirmation"> 
                    <input name="q" type="hidden" id="q" value="<?php echo htmlspecialchars($q);?>"> 
                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>"> 
                    <input name="by" type="hidden" id="by" value="<?=$by?>"> <input name="field" type="hidden" id="field" value="<?=$field?>"> 
                    <input name="LIKE" type="hidden" value="<?=$LIKE?>"> </td>
                </tr>
              </form>
            </table></td>
        </tr>
      </table></tr>
</table>
<?php } ?>
<DIV id=dek></DIV>
<script language="JavaScript">
	var cal = new calendar1(document.forms['frm_news'].elements['date_published']);
	cal.year_scroll = true;
	cal.time_comp = false;	
</script>

<SCRIPT type=text/javascript src="<?=JS_PATH?>/popupscript.js" ></SCRIPT>
</body>
</html>
