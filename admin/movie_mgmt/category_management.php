<?php include ($_SERVER["DOCUMENT_ROOT"]."/aqdar/includes/config.inc.php"); ?>
<?php
$sid = $_REQUEST["sid"];
if (!loggedUser($sid)) { 
	redirect (HOST_URL_ADMIN ."/include/messages.php?msg=relogin");
exit();
}
?>
<?php
	mysql_query("SET NAMES 'utf8'");
	mysql_query('SET CHARACTER SET utf8'); 

	$q = stripslashes($q);
		
	if($ssubmit=="insertPages"){
		$category_name = trim($category_name);
		
		$tbl_video_category_id = md5(uniqid(rand()));
		$tbl_video_category_id = substr($tbl_video_category_id,0,15);
		$tbl_video_category_id = $tbl_video_category_id;
		
		$query_check = "SELECT * FROM ".TBL_CATEGORY_VIDEO." WHERE category_name='$category_name' AND tbl_school_id='$tbl_school_id_sess' ";
		
		if (!isExist($query_check)){
	
				if (trim($_FILES["logo"]["name"]) != "") {
					//Movie Upload
					$logo_name = $_FILES["logo"]["name"];
					$source_img = $_FILES["logo"]["tmp_name"];
					$type_img = $_FILES["logo"]["type"];
					$size_img = $_FILES["logo"]["size"];
					$Ext = strchr($logo_name,".");
					$Ext = strtolower($Ext);
			
					if($Ext==".jpg" || $Ext==".jpeg" || $Ext==".gif" || $Ext==".bmp" || $Ext==".png") {
						$logo_name = substr(md5(uniqid(rand())),0,15);
						$logo_name = $logo_name.$Ext;
						$copy = file_copy("$source_img", IMG_UPLOAD_PATH_LOGO."/", "$logo_name", 1, 1);
		
						$query_insert = "INSERT INTO ".TBL_CATEGORY_VIDEO." (`tbl_video_category_id`,`category_name`,`category_name_ar`,`logo`,`is_active`,`added_date`,`tbl_school_id`) 
										 VALUES ('$tbl_video_category_id','$category_name','$category_name_ar','$logo_name','$is_active',NOW(), '$tbl_school_id_sess' ) ";
						//echo $query_insert;
						insertInto($query_insert);
						$category_name = "";
						$category_name_ar = "";
						$MSG = "Category information has been saved successfully!";
					} else {
						$is_error = "Y";
						$MSG .= "Image extension is not valid.<br>";
					}
				} else {
					$MSG .= "Logo is mandatory.<br>";
				}		
		} else {
			$MSG="Category already exists!";
		}
		$mid=1;
	}//if 
//===========================================================================================
if($ssubmit=="editPages"){
		$category_name = trim($category_name);
		
		$qry = "SELECT * FROM ".TBL_CATEGORY_VIDEO." WHERE category_name = '$category_name' AND id != $id AND tbl_school_id='$tbl_school_id_sess' ";
		if (isExist($qry)) {
			$MSG="Category with same name already exists!";
		} else {
			$logo_name = $_FILES["logo"]["name"];
			$source_img = $_FILES["logo"]["tmp_name"];
			$type_img = $_FILES["logo"]["type"];
			$size_img = $_FILES["logo"]["size"];
			$Ext = strchr($logo_name,".");
			$Ext = strtolower($Ext);
	
			if($Ext==".jpg" || $Ext==".jpeg" || $Ext==".gif" || $Ext==".bmp" || $Ext==".png") {
				$logo_name = substr(md5(uniqid(rand())),0,15);
				$logo_name = $logo_name.$Ext;
				$copy = file_copy("$source_img", IMG_UPLOAD_PATH_LOGO."/", "$logo_name", 1, 1);

				$query_update = "UPDATE ".TBL_CATEGORY_VIDEO." SET `logo` = '$logo_name' WHERE id = $id";	
				update($query_update);

				$MSG="Category has been upadated successfully!";
			} else {
				$MSG .= "Logo extension is not valid.<br>";
			}

			$query_update = "UPDATE ".TBL_CATEGORY_VIDEO." SET `category_name` = '$category_name',`category_name_ar` = '$category_name_ar', `is_active` = '$is_active' WHERE id = $id";	
			//echo $query_update;
			update($query_update);

			$category_name = "";
			$category_name_ar = "";
			$MSG .= "Category has been upadated successfully!";
		}		
		$mid=3;
	}
//=============================================================================================
if($ssubmit=="newsDetail"){
		if(isset($show)){
				for ($j=0; $j<count($EditBox); $j++){ 
					$Querry = "UPDATE ". TBL_CATEGORY_VIDEO ." SET is_active='Y' WHERE id=$EditBox[$j]";			
					update($Querry);

					$qry_cat = "SELECT tbl_video_category_id FROM ".TBL_CATEGORY_VIDEO." WHERE id=$EditBox[$j] ";	
					$rs_cat = selectFrom($qry_cat);
					$tbl_category_video_id = $rs_cat['tbl_video_category_id'];
					
					$Querry = "UPDATE ". TBL_VIDEO ." SET is_active='Y' WHERE tbl_video_id IN(SELECT tbl_video_id FROM ".TBL_VIDEO_CATEGORY." WHERE tbl_category_video_id='$tbl_category_video_id')";		
					update($Querry);
					
					$mid=1;	
					$MSG = "Selected Category(s) have been activated successfully!";
				}
		}
		if(isset($hide)){
				for ($j=0; $j<count($EditBox); $j++){ 
					$Querry = "UPDATE ". TBL_CATEGORY_VIDEO ." SET is_active='N' WHERE id=$EditBox[$j]";			
					update($Querry);

					$qry_cat = "SELECT tbl_video_category_id FROM ".TBL_CATEGORY_VIDEO." WHERE id=$EditBox[$j] ";	
					$rs_cat = selectFrom($qry_cat);
					$tbl_category_video_id = $rs_cat['tbl_video_category_id'];
					
					$Querry = "UPDATE ". TBL_VIDEO ." SET is_active='N' WHERE tbl_video_id IN(SELECT tbl_video_id FROM ".TBL_VIDEO_CATEGORY." WHERE tbl_category_video_id='$tbl_category_video_id')";		
					update($Querry);

					$mid=1;	
					$MSG = "Selected Category(s) have been deactivated successfully!";
				}
			
			}
		if(isset($delete)){$mid = 4;}
	}

if($ssubmit == "newsDelConfirmation"){
		for($i=0;$i<count($EditBox);$i++){
				$qry_cat = "SELECT tbl_video_category_id FROM ".TBL_CATEGORY_VIDEO." WHERE id=$EditBox[$i] ";	
				$rs_cat = selectFrom($qry_cat);
				$tbl_category_video_id = $rs_cat['tbl_video_category_id'];
				
				$Querry = "DELETE FROM  ". TBL_VIDEO ." WHERE tbl_video_id IN(SELECT tbl_video_id FROM ".TBL_VIDEO_CATEGORY." WHERE tbl_category_video_id='$tbl_category_video_id')";		
				deleteFrom($Querry);

				deleteFrom("DELETE FROM ".TBL_CATEGORY_VIDEO." WHERE `id` =$EditBox[$i]");		

				$mid = 1;
				$MSG = "Selected Category(s) have been deleted successfully!";
		}//for
}	

?>
<html>
<head>
<title>Admin (Category(s) Management)</title>
<link rel="stylesheet" href="<?=ADMIN_CSS_PATH?>interface.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script language="JavaScript">
function valueCheckedForPagesManagement(){
	var ml = document.frmDirectory;
	var len = ml.elements.length;
	for (var i = 0; i < len; i++){
	   	if (document.frmDirectory.elements[i].checked){
			return true;
		}
	}
	 alert ("Select at least one Category!");
	 return false;
}

function CheckAll(){
	var ml = document.frmDirectory;
	var len = ml.elements.length;
	if (document.frmDirectory.AC.checked==true) {
		 for (var i = 0; i < len; i++) {
			document.frmDirectory.elements[i].checked=true;
		 }
	} else {
		  for (var i = 0; i < len; i++)  {
			document.frmDirectory.elements[i].checked=false;
		  }
	}
}
function UnCheckAll() {
	var ml = document.frmDirectory;
	var len = ml.elements.length;
	var count=0; var checked=0;
	    for (var i = 0; i < len; i++) {	       
			if ((document.frmDirectory.elements[i].type=='checkbox') && (document.frmDirectory.elements[i].name != "AC")) {
				count = count + 1;
	  		 	if (document.frmDirectory.elements[i].checked == true){
					checked = checked + 1;
				}
			}
	     }
		 
	if (checked == count) {
		 document.frmDirectory.AC.checked = true;
	} else {
		document.frmDirectory.AC.checked = false;
	}
}
</script>
<script language="javascript">
	function validateCategoryName(){
		var regExp = / /g;
		var str = document.frmRegister.category_name.value;
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			alert("The Category Name is blank. Please write Category Name.")
			document.frmRegister.category_name.value = "";
			document.frmRegister.category_name.focus();
		return false;
		}

		var regExp = / /g;
		var str = document.frmRegister.category_name_ar.value;
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			alert("The Category Name Arabic is blank. Please write Category Name Arabic.")
			document.frmRegister.category_name_ar.value = "";
			document.frmRegister.category_name_ar.focus();
		return false;
		}
	}
</script>
<style>
.txt_en {
	text-align:left;
	padding-left:2px;
}
.txt_ar {
	text-align:right;
	padding-right:2px;	
	direction:rtl;		
}
</style>
</head>
<?php if (!$offset || $offset<0)  { $offset =0;} ?>
<?php if (!$LIKE)  { $LIKE = "LIKE";} ?>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<STYLE type=text/css>
#dek {
	Z-INDEX: 200; VISIBILITY: hidden; POSITION: absolute
}
</STYLE>
<?php if($mid=="1"){ ?>
<table width="100%" height="570" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr>
    <td height="30" valign="center" class="adminDetailHeading">&nbsp;Category(s) 
      Management </td>
  </tr>
  <tr align="center" >
    <td height="538" align="center" valign="top" ><form name="form1" method="post" action="">
        <br>
        <table width="80%" border="0" align="center" cellpadding="0" cellspacing="1" >
          <tr >
            <td height="22" align="left" valign="middle" class="adminDetailHeading">&nbsp;Search Criteria</td>
          </tr>
          <tr >
            <td align="center" valign="top" class="bordLightBlue"><table width="100%" border="0" cellspacing="3" cellpadding="2">
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td width="32%">&nbsp;</td>
                  <td width="48%"><a href="category_management.php?mid=<?=$mid?>&sid=<?=$sid?>"><span class="detailLinkColor">All 
                    Records</span></a>: 
                    or&nbsp;</td>
                  <td width="20%"><i></i></td>
                </tr>
                <tr>
                  <td colspan="3"><hr width="80%" size="1"></td>
                </tr>
                <tr>
                  <td align="right" valign="top" ><span class="searchText">Field:&nbsp;</span></td>
                  <td align="left" valign="top"><select name="field" id="field" >
                      <option value="category_name" <?PHP if ($field == "category_name"){echo "selected";}?>>Category Name</option>
                      <option value="category_name_ar" <?PHP if ($field == "category_name_ar"){echo "selected";}?>>Category Name Arabic</option>
                      <option value="added_date" <?PHP if ($field == "added_date"){echo "selected";}?>>Date e.g [YYYY-MM-DD] </option>
                      <option value="is_active"<?PHP if($field == "is_active"){echo "selected";}?>>Status e.g Y=Activate , N=Deactivate</option>
                    </select></td>
                  <td><i><span onMouseOver="window.status='';do_info(22);return true" onmouseout=kill() href="javascript:void(0)";><img src="<?=HOST_URL?>/images/help_form.gif"></span></i></td>
                </tr>
                <tr>
                  <td align="right" valign="top"><span class="searchText">Condition:&nbsp;</span></td>
                  <td align="left" valign="top"><?php
							if (!$LIKE) {
								$LIKE = "LIKE";
							}
						?>
                    <select name="LIKE" id="LIKE">
                      <option value="LIKE" <?PHP if ($LIKE == "LIKE"){echo "selected";}?>>LIKE</option>
                      <option value="=" <?PHP if (($LIKE == "=") || ($LIKE == "")){echo "selected";}?>>Equal 
                      To</option>
                      <option value="!=" <?PHP if ($LIKE == "!="){echo "selected";}?>>Not 
                      Equal To</option>
                      <option value="<" <?PHP if ($LIKE == "<"){echo "selected";}?>>Less 
                      Than</option>
                      <option value=">" <?PHP if ($LIKE == ">"){echo "selected";}?>>Greater 
                      Than</option>
                      <option value="<=" <?PHP if ($LIKE == "<="){echo "selected";}?>>Less 
                      Than or Equal To</option>
                      <option value=">=" <?PHP if ($LIKE == ">="){echo "selected";}?>>Greater 
                      Than or Equal To</option>
                    </select></td>
                  <td><i><span onMouseOver="window.status='';do_info(23);return true" onmouseout=kill() href="javascript:void(0)";><img src="<?=HOST_URL?>/images/help_form.gif"></span></i></td>
                </tr>
                <tr>
                  <td align="right" valign="top"><span class="searchText">Search Name:&nbsp;</span></td>
                  <td align="left" valign="top"><input name="q" type="text" id="q" value="<?php echo htmlspecialchars($q);?>" size="40"></td>
                  <td><i><span onMouseOver="window.status='';do_info(24);return true" onmouseout=kill() href="javascript:void(0)";><img src="<?=HOST_URL?>/images/help_form.gif"></span></i></td>
                </tr>
                <tr>
                  <td align="right" valign="top"><span class="searchText">Order By:&nbsp;</span></td>
                  <td align="left" valign="top"><select name="by" id="by">
                      <option value="ASC" <?PHP if (($by == "ASC") || ($by == "")) { echo "selected";}?>>ASCENDING</option>
                      <option value="DESC" <?PHP if ($by == "DESC"){ echo "selected";}?>>DESCENDING</option>
                    </select>
                    &nbsp;</td>
                  <td><i><span onMouseOver="window.is_active='';do_info(25);return true" onmouseout=kill() href="javascript:void(0)";><img src="<?=HOST_URL?>/images/help_form.gif"></span></i></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td align="center"><input name="mid" type="hidden" value="1">
                    <input name="offset" type="hidden" value="0">
                    <input name="Search" type="submit" id="Search" value="Search" class="flat">
                    &nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
              </table></td>
          </tr>
        </table>
      </form>
      <br>
      <span class="msgColor"><?php echo $MSG;?></span> <br>
      <?php 
		$q = addslashes($q);
		$CountRec .= "SELECT * FROM ".TBL_CATEGORY_VIDEO." WHERE tbl_school_id='$tbl_school_id_sess' ";	
		$Query .= "SELECT * FROM ".TBL_CATEGORY_VIDEO." WHERE tbl_school_id='$tbl_school_id_sess' ";	
		if(!$by){$by="DESC";}
		
		if($field && !empty($q)){	    	
			
			if($LIKE=="LIKE"){
					$CountRec .= " AND `$field` $LIKE '%$q%' ";
//					$Query .= " WHERE `$field` $LIKE '%$q%' ORDER BY `$field` $by ";
					$Query .= " AND `$field` $LIKE '%$q%' ";
			}else{
					$CountRec .= " AND `$field` $LIKE '$q' ";	
//					$Query .= " WHERE `$field` $LIKE '$q' ORDER BY `$field` $by ";
					$Query .= " AND `$field` $LIKE '$q' ";
			}
		}	
		 $Query .= " ORDER BY id $by ";
		 $Query .=" LIMIT $offset, ".TBL_CATEGORY_VIDEO_PAGING;
   	        $q = stripslashes($q);
		 //echo $Query;
		 $total_record = CountRecords($CountRec);
		 $data = SelectMultiRecords($Query);
		  		 if ($total_record =="")
					   echo '<span class="msgColor">'.NO_RECORD_FOUND."</span>"; 
					else{	
					    echo '<span class="msgColor">';
					    echo " <b> ". $total_record ." </b> Category(s) found. Showing <b>";
						if ($total_record>$offset){
							echo $offset+1;
							echo " </b>of<b> ";
							if ($offset >= $total_record - TBL_CATEGORY_VIDEO_PAGING)	{ 
								  echo $total_record; 
							}else { 
							   	echo $offset + TBL_CATEGORY_VIDEO_PAGING ;
							}
						}else{ 
							echo $offset+1;
							echo "</b> - ". $total_record;
							echo " of ". $total_record ." Category ";		
						}
						echo "</b>.</span>";	
					}
	   ?>
      <?php  if ($total_record !=""){?>
      <table width="98%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="18"><span class="msgColor">&nbsp;&nbsp;Note: Deactivating a Category will Deactivate All Movie(s) against it. </span></td>
        </tr>
        <tr>
          <td align="center" valign="top" class="adminDetailHeading"><table width="100%" border="0" align="center"  cellpadding="2" cellspacing="1">
              <form name="frmDirectory" onSubmit="return valueCheckedForPagesManagement()">
                <tr align="center" >
                  <td width="30%" height="24" align="center" class="adminDetailHeading">Category 
                    Name</td>
                  <td width="15%" align="center" class="adminDetailHeading">Logo</td>
                  <td width="15%" height="24" align="center" class="adminDetailHeading">Date</td>
                  <td width="8%" height="24" align="center" class="adminDetailHeading">Status</td>
                  <td width="8%" height="24" align="center" class="adminDetailHeading">Action</td>
                  <td width="4%" height="24" class="adminDetailHeading"><input name="AC" type="checkbox" onClick="CheckAll();"></td>
                </tr>
                <?php for($i=0;$i<count($data);$i++){
					$id = $data[$i]["id"];
					$tbl_video_category_id = $data[$i]["tbl_video_category_id"];
					$category_name_db = $data[$i]["category_name"];
					$category_name_ar_db = $data[$i]["category_name_ar"];

					$logo = $data[$i]["logo"];
					$img_path = IMG_SHOW_PATH_LOGO."/".$logo;
				?>
                <tr valign="middle" bgcolor="#FFFFFF">
                  <td height="20" align="left" >
                      <div class="txt_en"><?=$category_name_db?></div>
                      <div class="txt_ar"><?=$category_name_ar_db?></div>
                  </td>
                  <td align="center" ><img src="<?=$img_path?>" /></td>
                  <td height="20" align="center" ><? echo date("d M, Y ",strtotime($data[$i]["added_date"]));?></td>
                  <td align="center"><?php 
					  $is_active = $data[$i]["is_active"];
					  if($is_active == 'Y'){?>
                    <img src="<?=IMG_PATH?>/yes.gif">
                    <?php } else {?>
                    <img src="<?=IMG_PATH?>/no.gif">
                    <?php }  ?></td>
                  <td align="center"><a href="?mid=3&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&by=<?=$by?>&id=<?=$id?>" class="detailLinkColor">Edit</a></td>
                  <td align="center" ><input name="EditBox[]" type="checkbox" onClick="UnCheckAll();" value="<?=$id?>"></td>
                </tr>
                <?php }?>
                <tr>
                  <td height="20" colspan="8" align="center" valign="middle" bgcolor="#FFFFFF"><input name="reset" type="reset" class="flat"  value="Reset">
                    <input name="show" type="submit" class="flat" id="ssubmit" value="Active">
                    <input name="hide" type="submit" class="flat"  value="Deactive">
                    <input name="delete" type="submit" class="flat"  value="Delete">
                    <input name="ssubmit" type="hidden" id="ssubmit" value="newsDetail">
                    <input name="mid" type="hidden" id="mid2" value="1">
                    <input name="sid" type="hidden" id="sid" value="<?=$sid?>">
                    <input name="offset" type="hidden" id="offset" value="<?=$offset?>">
                    <input name="q" type="hidden" id="q" value="<? echo htmlspecialchars($q);?>">
                    <input name="id" type="hidden" value="<?=$id?>">
                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>">
                    <input name="by" type="hidden" id="by" value="<?=$by?>">
                    <input name="field" type="hidden" id="field" value="<?=$field?>">
                    <input name="LIKE" type="hidden" value="<?=$LIKE?>"></td>
                </tr>
                <tr align="right">
                  <td height="20" colspan="8" valign="middle" bgcolor="#FFFFFF"><?php $url = "category_management.php?mid=1&sid=$sid&field=$field&q=$q&LIKE=$LIKE&by=$by";
	 	 getPaging($offset, $total_record, TBL_CATEGORY_VIDEO_PAGING, $url); ?></td>
                </tr>
              </form>
            </table></td>
        </tr>
      </table>
      <br>
      <?php } ?>
      <br>
      <br>
      <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >
        <tr >
          <td height="22" align="center" valign="middle"  class="adminDetailHeading" ><strong>&nbsp;Add Category</strong></td>
        </tr>
        <tr >
          <td align="center" valign="top" class="bordLightBlue"><form name="frmRegister" method="post" action=""  onSubmit="return validateCategoryName();" enctype="multipart/form-data">
              <table width="100%" border="0" align="center" cellpadding="4" cellspacing="4" >
                <tr align="center" valign="top" >
                  <td colspan="2"></td>
                </tr>
                <tr align="left" valign="top" >
                  <td colspan="2"><span class="msgColor">*Fields 
                    in 
                    Red 
                    are 
                    mandatory!</span></td>
                </tr>
                <tr valign="middle" >
                  <td width="22%" align="right" valign="top"><span class="msgColor">*</span>Category 
                    Name: </td>
                  <td width="78%"><input name="category_name" type="text" class="flat" id="category_name" value="<? echo htmlspecialchars($category_name);?>" size="30" maxlength="60"></td>
                </tr>
                <tr valign="middle" >
                  <td width="22%" align="right" valign="top"><span class="msgColor">*</span>Category 
                    Name Arabic: </td>
                  <td width="78%"><input dir="rtl" name="category_name_ar" type="text" class="flat" id="category_name_ar" value="<? echo htmlspecialchars($category_name_ar);?>" size="30" maxlength="60"></td>
                </tr>
                <tr valign="middle" >
                  <td align="right" valign="top"><span class="msgColor">*</span>Logo:</td>
                  <td><input type="file" name="logo" class="flat"> 
                  200 X 200</td>
                </tr>
                <tr valign="middle" >
                  <td align="right">Status:</td>
                  <td><input name="is_active" type="radio" value="Y" checked <?php if($data["is_active"]=='Y'){echo "checked";}?>>
                    Yes
                    <input type="radio" name="is_active" value="N" <?php if($data["is_active"]=='N'){echo "checked";}?>>
                    No </td>
                </tr>
                <tr valign="middle" >
                  <td colspan="2" align="center"><input type="reset" name="Reset" value="Reset" class="flat">
                    <input name="ssubmit" type="submit" class="flat" id="ssubmit"  value="Submit">
                    <input name="ssubmit" type="hidden" id="ssubmit" value="insertPages">
                    <input name="sid" type="hidden" value="<?=$sid?>">
                    <input name="mid" type="hidden" value="1">
                    <input name="offset" type="hidden" id="offset" value="<?=$offset;?>">
                    <input name="q" type="hidden" id="q" value="<?php echo htmlspecialchars($q);?>">
                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>">
                    <input name="by" type="hidden" id="by" value="<?=$by?>">
                    <input name="field" type="hidden" id="field" value="<?=$field?>">
                    <input name="LIKE" type="hidden" value="<?=$LIKE?>"></td>
                </tr>
                <tr valign="middle" >
                  <td colspan="2" align="center">
                </tr>
              </table>
          </form></td>
        </tr>
      </table>
      <br></td>
  </tr>
</table>
<?php }if($mid=="2"){ 
	$data = selectFrom("SELECT * FROM ".TBL_CATEGORY_VIDEO." WHERE id=$id");	 
		$tbl_video_category_id = $data["tbl_video_category_id"];
		$logo = $data["logo"];
		
		$img_path = IMG_SHOW_PATH_LOGO."/".$logo;
?>
<table width="100%"  border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr>
    <td height="30" valign="center"  class="adminDetailHeading">&nbsp;Category 
      Management [Detail]</td>
  </tr>
  <tr>
    <td height="200" align="center" valign="top"><br>
      <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >
        <tr >
          <td height="22" align="center" valign="middle"  class="adminDetailHeading">&nbsp;Category 
            Detail</td>
        </tr>
        <tr >
          <td align="center" valign="top" class="bordLightBlue"><table width="100%" border="0" align="center" cellpadding="3" cellspacing="0" >
              <tr align="center" valign="top" >
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr valign="top" >
                <td width="23%" align="right"><strong>Category Name:</strong></td>
                <td width="77%"><?php echo $data["category_name"];?></td>
              </tr>
              <tr valign="top" >
                <td width="23%" align="right"><strong>Category Name Arabic:</strong></td>
                <td width="77%"><?php echo $data["category_name_ar"];?></td>
              </tr>
              <tr valign="top" >
                <td align="right"><strong>Status:</strong></td>
                <td><?php  if($data["is_active"]=='Y'){echo "Active";}
		  		if($data["is_active"]=='N'){echo "Deactive";}?>
                </td>
              </tr>
              <tr valign="top" >
                <td align="right"><strong>Logo:</strong></td>
                <td><img src="<?=$img_path?>">
                </td>
              </tr>
              <tr align="center" valign="top" >
                <td align="right"><strong>Date:</strong></td>
                <td align="left"><? echo getFormatedDate("TBL_CATEGORY_VIDEO","added_date","id",$id);?></td>
              </tr>
              <tr align="center" valign="top" >
                <td align="right">&nbsp;</td>
                <td align="left">&nbsp;</td>
              </tr>
              <tr valign="middle" >
                <td colspan="2" align="center"><?php $id=$data["id"];?>
                  <a href="category_management.php?mid=1&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&sort=<?=$sort?>&by=<?=$by?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>" class="detailLinkColor"> Back </a></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
<?php }if($mid=="3"){ 
	$data = selectFrom("SELECT * FROM ".TBL_CATEGORY_VIDEO." WHERE id=$id");
		$tbl_video_category_id_db = $data["tbl_video_category_id"];
		$id_main = $data["id"];
		$logo = $data["logo"];
		
		$img_path = IMG_SHOW_PATH_LOGO."/".$logo;
?>
<table width="100%" height="451" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr>
    <td height="30" valign="center" class="adminDetailHeading">&nbsp;Category 
      Management [Edit] </td>
  </tr>
  <tr>
    <td align="center" valign="top"><br>
      <span class="msgColor"><?php echo $MSG;?></span> <br>
      <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >
        <tr >
          <td height="22" align="center" valign="middle"  class="adminDetailHeading" >&nbsp;Edit 
            Category</td>
        </tr>
        <tr >
          <td align="left" valign="top" class="bordLightBlue"><form name="frmRegister" method="post" action=""  onSubmit="return validateCategoryName();" enctype="multipart/form-data">
              <table width="100%" border="0" cellpadding="3" cellspacing="0" >
               
                <tr align="center" valign="top" >
                  <td colspan="2"></td>
                </tr>
                <tr align="left" valign="top" >
                  <td colspan="2"><span class="msgColor">Fields with * are mandatory!</span></td>
                </tr>
                <tr align="center" valign="top" >
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr valign="middle" >
                  <td width="25%" align="right" valign="top"><span class="msgColor">*</span>Category 
                    Name:</td>
                  <td width="75%"><input name="category_name" type="text" class="flat" id="category_name" value="<?php echo htmlspecialchars($data["category_name"]);?>" size="30"></td>
                </tr>
                <tr valign="middle" >
                  <td width="25%" align="right" valign="top"><span class="msgColor">*</span>Category 
                    Name:</td>
                  <td width="75%"><input dir="rtl" name="category_name_ar" type="text" class="flat" id="category_name_ar" value="<?php echo $data["category_name_ar"];?>" size="30"></td>
                </tr>
                <tr valign="middle" >
                  <td align="right" valign="top"><span class="msgColor">*</span>Logo:</td>
                  <td><input type="file" name="logo" class="flat">
                  200 X 200<br>
<img src="<?=$img_path?>"></td>
                </tr>
                <tr valign="middle" >
                  <td align="right" valign="top">Status:</td>
                  <td><input name="is_active" type="radio" value="Y" checked  <?php if($data["is_active"]=='Y'){echo "checked";}?>>
                    Yes
                    <input type="radio" name="is_active" value="N" <?php if($data["is_active"]=='N'){echo "checked";}?>>
                  No </td>
                </tr>
                <tr valign="middle" >
                  <td align="right">&nbsp;</td>
                  <td><a href="category_management.php?mid=1&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&sort=<?=$sort?>&by=<?=$by?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>" class="detailLinkColor">Back</a>&nbsp;
                    <input name="Reset" type="reset" class="flat" id="Reset" value="Reset Values">
                    <input name="ssubmit" type="submit" class="flat" id="ssubmit"  value="Save Changes">
                    <input name="ssubmit" type="hidden" id="ssubmit" value="editPages">
                    <input name="sid" type="hidden" id="sid" value="<?=$sid?>">
                    <input name="mid" type="hidden" id="mid" value="1">
                    <input name="offset" type="hidden" id="offset" value="<?=$offset;?>">
                    <input name="q" type="hidden" id="q" value="<? echo htmlspecialchars($q);?>">
                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>">
                    <input name="field" type="hidden" id="field" value="<?=$field?>">
                    <input name="LIKE" type="hidden" id="LIKE" value="<?=$LIKE?>"></td>
                </tr>
                <tr valign="middle" >
                  <td colspan="2" align="center">
                </tr>
              </table>
          </form></td>
        </tr>
      </table></td>
  </tr>
</table>
<?php }if($mid=="4"){ ?>
<table width="100%" height="380" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr>
    <td height="30" valign="center" class="adminDetailHeading">&nbsp;Category 
      Management [Del Confirmation]</td>
  </tr>
  <tr align="center" >
    <td valign="top" ><br>
      <span class="msgColor"> Are you sure you want to delete the selected Category(s)</span>.
      <table width="98%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><span class="msgColor">&nbsp;&nbsp;Note: Deleting a Category will Delete All Movie(s)  against it. </span>&nbsp;&nbsp;</td>
        </tr>
      </table>
      <table width="98%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center" valign="top" class="adminDetailHeading"><table width="100%" border="0" align="center"  cellpadding="2" cellspacing="1" >
                <form name="frmDirectory" onSubmit="return valueCheckedForPagesManagement();">
              
              
                <td width="34%" height="24" align="center"  class="adminDetailHeading">Category 
                  Name </td>
                <td width="19%" align="center"  class="adminDetailHeading">Date</td>
                <td width="7%" align="center"  class="adminDetailHeading">Status</td>
                <td width="3%" height="24" align="center"  class="adminDetailHeading"><input name="AC" type="checkbox" id="AC" onClick="CheckAll();" checked></td>
              </tr>
              <?php for($i=0;$i<count($EditBox);$i++){
			//echo $EditBox[$i]."<br>";
			$Query ="SELECT * FROM ".TBL_CATEGORY_VIDEO." WHERE id=$EditBox[$i]";
			$rows = selectFrom($Query);
			$id =  $rows["id"];
			$category_name = $rows["category_name"];
			$category_name_ar = $rows["category_name_ar"];
			$added_date = $rows["added_date"];												
			$is_active = $rows["is_active"];
		?>
              <tr valign="middle" bgcolor="#FFFFFF">
                <td height="20" align="left">
                      <div class="txt_en"><?=$category_name?></div>
                      <div class="txt_ar"><?=$category_name_ar?></div>
                </td>
                <td height="20" align="center"><? echo date("d M, Y ",strtotime($added_date));?></td>
                <td height="20" align="center"><?php 	
						if($is_active == 'Y'){?>
                  <img src="<?=IMG_PATH?>/yes.gif">
                  <?php } else {?>
                  <img src="<?=IMG_PATH?>/no.gif">
                  <?php }	 ?></td>
                <td height="20" align="center"><input name="EditBox[]" type="checkbox" id="EditBox[]" onClick="UnCheckAll();" value="<?=$id?>" checked></td>
              </tr>
              <?php }?>
              <tr>
                <td height="20" colspan="7" align="center" valign="middle" bgcolor="#FFFFFF" ><input name="Button" type="button" class="flat" id="hide" value="I am not sure" onClick="javascript:history.back();">
                  <input name="show" type="submit" class="flat" id="show" value="Yes I am sure">
                  <input name="sid" type="hidden" id="sid" value="<?=$sid;?>">
                  <input name="offset" type="hidden" id="offset" value="<?=$offset;?>">
                  <input name="mid" type="hidden" id="mid" value="1">
                  <input name="ssubmit" type="hidden" id="show" value="newsDelConfirmation">
                  <input name="q" type="hidden" id="q" value="<?php echo htmlspecialchars($q);?>">
                  <input name="sort" type="hidden" id="sort" value="<?=$sort?>">
                  <input name="by" type="hidden" id="by" value="<?=$by?>">
                  <input name="field" type="hidden" id="field" value="<?=$field?>">
                  <input name="LIKE" type="hidden" value="<?=$LIKE?>"></td>
              </tr>
                </form>
              
            </table></td>
        </tr>
      </table>
  </tr>
</table>
<?php } ?>
<DIV id=dek></DIV>
<SCRIPT type=text/javascript src="<?=JS_PATH?>/popupscript.js" ></SCRIPT>
</body>
</html>
