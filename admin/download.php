<?php include ($_SERVER["DOCUMENT_ROOT"]."/aqdar/includes/config.inc.php");
define("HOST_URL", "http://".$_SERVER["HTTP_HOST"]."/aqdar");
$tbl_uploads_id = $_GET['id'];
$qry = "SELECT * FROM ".TBL_UPLOADS." WHERE tbl_uploads_id='".$tbl_uploads_id."'";
$data_rs = SelectMultiRecords($qry);
$file_name_original = $data_rs[0]["file_name_original"];
$file_name_updated = $data_rs[0]["file_name_updated"];
$content_type = $data_rs[0]["file_type"];
$ext = strchr($file_name_updated,".");
if (trim($content_type) == "") {
	if ($ext == "jpg" || $ext == "JPG" ) {
		$content_type = "image/jpeg";
	}else if ($ext == "doc" || $ext == "DOC" ) {
		$content_type = "application/msword";
	}else if ($ext == ".docx" || $ext == "DOCX" ) {
		$content_type = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
	}else if ($ext == "pdf" || $ext == "PDF" ) {
		$content_type = "application/pdf";
	}else if ($ext == "xlsx" || $ext == "XLSX" ) {
		$content_type = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	}else if ($ext == "txt" || $ext == "TXT" ) {
		$content_type = "text/plain";
	}else if ($ext == "zip" || $ext == "ZIP" ) {
		$content_type = "application/zip";
	}else if ($ext == "swf" || $ext == "SWF" ) {
		$content_type = "application/x-shockwave-flash";
	}
}
$file_path = HOST_URL."/admin/uploads/".rawurlencode($file_name_updated);
header('Content-type: '.$content_type);
header('Content-Disposition: attachment; filename="'.$file_name_original.'"');
readfile($file_path);
//echo "------------>".$file_path;
?>