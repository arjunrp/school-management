<?php include("../header.php"); 
if (!loggedUser($sid)){
	//redirect (HOST_URL ."/admin/include/messages.php?msg=relogin"); 
	//exit();
}
$date_time = date('d-m-Y  h:i:s A');
$user_id = loggedUser($sid);
$contents = selectFrom("SELECT id,user_id FROM " . TBL_ADMIN_USERS . " WHERE user_id='$user_id'");
$admin_name = $contents['user_id'];
?>
<link rel="stylesheet" href="<?=ADMIN_CSS_PATH?>interface.css" type="text/css">
<script language="javascript" src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script language="javascript">

	function validate_form() {
		var is_male = $('#is_male').is(':checked');	
		var is_female = $('#is_female').is(':checked');	
		
		if (is_male == false && is_female == false) {
			alert("Select atleast one gender.")
			$('#is_male').prop("checked",true);
		}

		/*	
		var school_type_public = $('#school_type_public').is(':checked');	
		var school_type_private = $('#school_type_private').is(':checked');	
		
		if (school_type_public == false && school_type_private == false) {
			alert("Select atleast one school type.")
			$('#school_type_public').prop("checked",true);
		}
		*/		
	}
</script>
<style>
input {
	direction: rtl;
}
table {
	border-collapse: collapse;
}
/*
table, tbody, th, td {
    border: 1px solid #E0E0E0;
}

table thead {
	font-weight:bold;	
}
*/
</style>
<?php
	function get_School_name($tbl_school_id) {
		$qry_school = "SELECT school_name FROM ".TBL_SCHOOL." WHERE tbl_school_id='$tbl_school_id' ";
		$rs_school = selectFrom($qry_school);
	return $rs_school['school_name'];	
	}

	mysql_query("SET NAMES 'utf8'");
	mysql_query('SET CHARACTER SET utf8'); 

	$today_date = date('Y-m-d');
?>
<?php

	$is_male = "N";
	$is_female = "N";
	
	if (!$_POST) {
		$is_male = "Y";
		$is_female = "Y";
	} else {
		if ($_POST['is_male']=="Y") {
			$is_male = "Y";
		}
		if ($_POST['is_female']=="Y") {
			$is_female = "Y";
		}
	}

	$school_name_arr = array();
	$total_students_arr = array();
	$total_students_present_arr = array();

	$school_id_str = "";
	for ($a=0; $a<count($tbl_school_id_param); $a++) {
		$school_id_str .= "'".$tbl_school_id_param[$a]."'";
		
		if (count($tbl_school_id_param) != $a+1) {
			$school_id_str .= ",";
		}
	}

	$male_present = 0;
	$female_present = 0;
	$male_total = 0;
	$female_total = 0;
	
	/*MAIN QUERY START*/
	$qry_sum = "SELECT SUM(male_present) as male_present, SUM(female_present) as female_present, SUM(male_total) as male_total, SUM(female_total) AS female_total, tbl_school_id 
				  FROM ".TBL_REPORT_ATTENDANCE." WHERE 1 
				  AND attendance_date='$today_date' 
				  ";
	
	if (trim($school_id_str) !="") {
		$qry_sum .= " AND tbl_school_id IN($school_id_str)";
	}
	
	if (trim($country) != "") {
		$qry_sum .= " AND country='$country' ";
	}

	$qry_sum .= " GROUP BY tbl_school_id ";
	
	//echo "<br />".$qry_sum."<br /><br />";
	/*MAIN QUERY END*/
	
	$rs_sum = SelectMultiRecords($qry_sum);
	
	for ($i=0; $i<count($rs_sum); $i++) {
		$tbl_school_id = $rs_sum[$i]['tbl_school_id'];	
	
		$male_present = $rs_sum[$i]['male_present'];
		$female_present = $rs_sum[$i]['female_present'];
		$male_total = $rs_sum[$i]['male_total'];
		$female_total = $rs_sum[$i]['female_total'];
		
		$total_present = 0;
		$total_students = 0;
		
		if (trim($is_male) == "Y" && trim($is_female) == "Y") {
			$total_present = $male_present + $female_present;
			$total_students = $male_total + $female_total;
		}
		if (trim($is_male) == "Y" && trim($is_female) == "N") {
			$total_present = $male_present;
			$total_students = $male_total;
		}
		if (trim($is_male) == "N" && trim($is_female) == "Y") {
			$total_present = $female_present;
			$total_students = $female_total;
		}
		
		$school_name_arr[$i] = $tbl_school_id;
		$total_students_arr[$i] = $total_students;
		$total_students_present_arr[$i] = $total_present;
	}
?>
<table width="100%" height="870" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr>
    <td  valign="center" height='30'  class="adminDetailHeading">&nbsp;Attendance Reports</td>
  </tr>
  <tr align="center" >
    <td height="838" align="center" valign="top"><br>
      <br>
      <center>
        <span style="color:#CC0000">
        <?=$MSG?>
        </span>
      </center>
      <h1 style="text-decoration:underline">Attendance Report for
        <?=$today_date?>
      </h1>
      <form name="frm_search" method="post" action="">
        <table width="80%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100" align="left" valign="top" style="font-weight:bold">Nationality: </td>
            <td><select name="country"  id="country" style="width:134px" class="flat">
                <option value=""  <?php if (trim($country) == "") {echo "selected";}?>>All</option>
                <option value="Afghanistan" <?php if (trim($country) == "Afghanistan") {echo "selected";}?>>Afghanistan</option>
                <option value="Albania" <?php if (trim($country) == "Albania") {echo "selected";}?> >Albania</option>
                <option value="Algeria" <?php if (trim($country) == "Algeria") {echo "selected";}?> >Algeria</option>
                <option value="Andorra" <?php if (trim($country) == "Andorra") {echo "selected";}?> >Andorra</option>
                <option value="Argentina" <?php if (trim($country) == "Argentina") {echo "selected";}?> >Argentina</option>
                <option value="Angola" <?php if (trim($country) == "Angola") {echo "selected";}?> >Angola</option>
                <option value="Anguilla" <?php if (trim($country) == "Anguilla") {echo "selected";}?> >Anguilla</option>
                <option value="Armenia" <?php if (trim($country) == "Armenia") {echo "selected";}?> >Armenia</option>
                <option value="Aruba" <?php if (trim($country) == "Aruba") {echo "selected";}?> >Aruba</option>
                <option value="Australia" <?php if (trim($country) == "Australia") {echo "selected";}?> >Australia</option>
                <option value="Austria" <?php if (trim($country) == "Austria") {echo "selected";}?> >Austria</option>
                <option value="Azerbaijan" <?php if (trim($country) == "Azerbaijan") {echo "selected";}?> >Azerbaijan</option>
                <option value="Bahamas" <?php if (trim($country) == "Bahamas") {echo "selected";}?> >Bahamas</option>
                <option value="Bahrain" <?php if (trim($country) == "Bahrain") {echo "selected";}?> >Bahrain</option>
                <option value="Bangladesh" <?php if (trim($country) == "Bangladesh") {echo "selected";}?> >Bangladesh</option>
                <option value="Barbados" <?php if (trim($country) == "Barbados") {echo "selected";}?> >Barbados</option>
                <option value="Belarus" <?php if (trim($country) == "Belarus") {echo "selected";}?> >Belarus</option>
                <option value="Belgium" <?php if (trim($country) == "Belgium") {echo "selected";}?> >Belgium</option>
                <option value="Belize" <?php if (trim($country) == "Belize") {echo "selected";}?> >Belize</option>
                <option value="Benin" <?php if (trim($country) == "Benin") {echo "selected";}?> >Benin</option>
                <option value="Bermuda" <?php if (trim($country) == "Bermuda") {echo "selected";}?> >Bermuda</option>
                <option value="Bhutan" <?php if (trim($country) == "Bhutan") {echo "selected";}?> >Bhutan</option>
                <option value="Bolivia" <?php if (trim($country) == "Bolivia") {echo "selected";}?> >Bolivia</option>
                <option value="Bosnia" <?php if (trim($country) == "Bosnia") {echo "selected";}?> >Bosnia</option>
                <option value="Botswana" <?php if (trim($country) == "Botswana") {echo "selected";}?> >Botswana</option>
                <option value="Brazil" <?php if (trim($country) == "Brazil") {echo "selected";}?> >Brazil</option>
                <option value="Brunei" <?php if (trim($country) == "Brunei") {echo "selected";}?> >Brunei</option>
                <option value="Bulgaria" <?php if (trim($country) == "Bulgaria") {echo "selected";}?> >Bulgaria</option>
                <option value="Burkina-Faso" <?php if (trim($country) == "Burkina-Faso") {echo "selected";}?> >Burkina-Faso</option>
                <option value="Burundi" <?php if (trim($country) == "Burundi") {echo "selected";}?> >Burundi</option>
                <option value="Canada" <?php if (trim($country) == "Canada") {echo "selected";}?> >Canada</option>
                <option value="Cambodia" <?php if (trim($country) == "Cambodia") {echo "selected";}?> >Cambodia</option>
                <option value="Camaroon" <?php if (trim($country) == "Camaroon") {echo "selected";}?> >Camaroon</option>
                <option value="Cape-Verde" <?php if (trim($country) == "Cape-Verde") {echo "selected";}?> >Cape-Verde</option>
                <option value="Cayman-Islands" <?php if (trim($country) == "Cayman-Islands") {echo "selected";}?> >Cayman-Islands</option>
                <option value="Chad" <?php if (trim($country) == "Chad") {echo "selected";}?> >Chad</option>
                <option value="China" <?php if (trim($country) == "China") {echo "selected";}?> >China</option>
                <option value="Chile" <?php if (trim($country) == "Chile") {echo "selected";}?> >Chile</option>
                <option value="Cocos" <?php if (trim($country) == "Cocos") {echo "selected";}?> >Cocos</option>
                <option value="Colombia" <?php if (trim($country) == "Colombia") {echo "selected";}?> >Colombia</option>
                <option value="Comoros" <?php if (trim($country) == "Comoros") {echo "selected";}?> >Comoros</option>
                <option value="Congo" <?php if (trim($country) == "Congo") {echo "selected";}?> >Congo</option>
                <option value="Cook-Islands" <?php if (trim($country) == "Cook-Islands") {echo "selected";}?> >Cook-Islands</option>
                <option value="Costa-Rica" <?php if (trim($country) == "Costa-Rica") {echo "selected";}?> >Costa-Rica</option>
                <option value="Cote-D-Ivoire" <?php if (trim($country) == "Cote-D-Ivoire") {echo "selected";}?> >Cote-D-Ivoire</option>
                <option value="Croatia" <?php if (trim($country) == "Croatia") {echo "selected";}?> >Croatia</option>
                <option value="Cuba" <?php if (trim($country) == "Cuba") {echo "selected";}?> >Cuba</option>
                <option value="Cyprus" <?php if (trim($country) == "Cyprus") {echo "selected";}?> >Cyprus</option>
                <option value="Czech-Republic" <?php if (trim($country) == "Czech-Republic") {echo "selected";}?> >Czech</option>
                <option value="Denmark" <?php if (trim($country) == "Denmark") {echo "selected";}?> >Denmark</option>
                <option value="Djibouti" <?php if (trim($country) == "Djibouti") {echo "selected";}?> >Djibouti</option>
                <option value="Dominica" <?php if (trim($country) == "Dominica") {echo "selected";}?> >Dominica</option>
                <option value="Egypt" <?php if (trim($country) == "Egypt") {echo "selected";}?> >Egypt</option>
                <option value="El-Salvador" <?php if (trim($country) == "El-Salvador") {echo "selected";}?> >El-Salvador</option>
                <option value="Ecuador" <?php if (trim($country) == "Ecuador") {echo "selected";}?> >Ecuador</option>
                <option value="Equatorial" <?php if (trim($country) == "Guinea") {echo "selected";}?> >Guinea</option>
                <option value="Eritrea" <?php if (trim($country) == "Eritrea") {echo "selected";}?> >Eritrea</option>
                <option value="Estonia" <?php if (trim($country) == "Estonia") {echo "selected";}?> >Estonia</option>
                <option value="Ethiopia" <?php if (trim($country) == "Ethiopia") {echo "selected";}?> >Ethiopia</option>
                <option value="Fiji" <?php if (trim($country) == "Fiji") {echo "selected";}?> >Fiji</option>
                <option value="Finland" <?php if (trim($country) == "Finland") {echo "selected";}?> >Finland</option>
                <option value="France" <?php if (trim($country) == "France") {echo "selected";}?> >France</option>
                <option value="French-Guiana" <?php if (trim($country) == "French-Guiana") {echo "selected";}?> >French-Guiana</option>
                <option value="Gabon" <?php if (trim($country) == "Gabon") {echo "selected";}?> >Gabon</option>
                <option value="Gambia" <?php if (trim($country) == "Gambia") {echo "selected";}?> >Gambia</option>
                <option value="Gaza-Strip" <?php if (trim($country) == "Gaza-Strip") {echo "selected";}?> >Gaza-Strip</option>
                <option value="Georgia" <?php if (trim($country) == "Georgia") {echo "selected";}?> >Georgia</option>
                <option value="Germany" <?php if (trim($country) == "Germany") {echo "selected";}?> >Germany</option>
                <option value="Ghana" <?php if (trim($country) == "Ghana") {echo "selected";}?> >Ghana</option>
                <option value="Gibralter" <?php if (trim($country) == "Gibralter") {echo "selected";}?> >Gibralter</option>
                <option value="Greece" <?php if (trim($country) == "Greece") {echo "selected";}?> >Greece</option>
                <option value="Greenland" <?php if (trim($country) == "Greenland") {echo "selected";}?> >Greenland</option>
                <option value="Grenada" <?php if (trim($country) == "Grenada") {echo "selected";}?> >Grenada</option>
                <option value="Guadeloupe" <?php if (trim($country) == "Guadeloupe") {echo "selected";}?> >Guadeloupe</option>
                <option value="Guam" <?php if (trim($country) == "Guam") {echo "selected";}?> >Guam</option>
                <option value="Guatemala" <?php if (trim($country) == "Guatemala") {echo "selected";}?> >Guatemala</option>
                <option value="Guernsey" <?php if (trim($country) == "Guernsey") {echo "selected";}?> >Guernsey</option>
                <option value="Guinea" <?php if (trim($country) == "Guinea") {echo "selected";}?> >Guinea</option>
                <option value="Guinea-Bissau" <?php if (trim($country) == "Guinea-Bissau") {echo "selected";}?> >Guinea-Bissau</option>
                <option value="Guyana" <?php if (trim($country) == "Guyana") {echo "selected";}?> >Guyana</option>
                <option value="Haiti" <?php if (trim($country) == "Haiti") {echo "selected";}?> >Haiti</option>
                <option value="Holy-See" <?php if (trim($country) == "Holy-See") {echo "selected";}?> >Holy-See</option>
                <option value="Honduras" <?php if (trim($country) == "Honduras") {echo "selected";}?> >Honduras</option>
                <option value="Hong-Kong" <?php if (trim($country) == "Hong-Kong") {echo "selected";}?> >Hong-Kong</option>
                <option value="Hungary" <?php if (trim($country) == "Hungary") {echo "selected";}?> >Hungary</option>
                <option value="Iceland" <?php if (trim($country) == "Iceland") {echo "selected";}?> >Iceland</option>
                <option value="India" <?php if (trim($country) == "India") {echo "selected";}?> >India</option>
                <option value="Indonesia" <?php if (trim($country) == "Indonesia") {echo "selected";}?> >Indonesia</option>
                <option value="Iran" <?php if (trim($country) == "Iran") {echo "selected";}?> >Iran</option>
                <option value="Iraq" <?php if (trim($country) == "Iraq") {echo "selected";}?> >Iraq</option>
                <option value="Ireland" <?php if (trim($country) == "Ireland") {echo "selected";}?> >Ireland</option>
                <option value="Israel" <?php if (trim($country) == "Israel") {echo "selected";}?> >Israel</option>
                <option value="Italy" <?php if (trim($country) == "Italy") {echo "selected";}?> >Italy</option>
                <option value="Jamaica" <?php if (trim($country) == "Jamaica") {echo "selected";}?> >Jamaica</option>
                <option value="Jarvis-Island" <?php if (trim($country) == "Jarvis-Island") {echo "selected";}?> >Jarvis-Island</option>
                <option value="Japan" <?php if (trim($country) == "Japan") {echo "selected";}?> >Japan</option>
                <option value="Jordan" <?php if (trim($country) == "Jordan") {echo "selected";}?> >Jordan</option>
                <option value="Kazakhstan" <?php if (trim($country) == "Kazakhstan") {echo "selected";}?> >Kazakhstan</option>
                <option value="Kenya" <?php if (trim($country) == "Kenya") {echo "selected";}?> >Kenya</option>
                <option value="Kiribati" <?php if (trim($country) == "Kiribati") {echo "selected";}?> >Kiribati</option>
                <option value="Korea" <?php if (trim($country) == "Korea") {echo "selected";}?> >Korea</option>
                <option value="Kuwait" <?php if (trim($country) == "Kuwait") {echo "selected";}?> >Kuwait</option>
                <option value="Laos" <?php if (trim($country) == "Laos") {echo "selected";}?> >Laos</option>
                <option value="Latvia" <?php if (trim($country) == "Latvia") {echo "selected";}?> >Latvia</option>
                <option value="Lebanon" <?php if (trim($country) == "Lebanon") {echo "selected";}?> >Lebanon</option>
                <option value="Lesotho" <?php if (trim($country) == "Lesotho") {echo "selected";}?> >Lesotho</option>
                <option value="Liberia" <?php if (trim($country) == "Liberia") {echo "selected";}?> >Liberia</option>
                <option value="Liechtenstein" <?php if (trim($country) == "Liechtenstein") {echo "selected";}?> >Liechtenstein</option>
                <option value="Lithuania" <?php if (trim($country) == "Lithuania") {echo "selected";}?> >Lithuania</option>
                <option value="Luxembourg" <?php if (trim($country) == "Luxembourg") {echo "selected";}?> >Luxembourg</option>
                <option value="Macau" <?php if (trim($country) == "Macau") {echo "selected";}?> >Macau</option>
                <option value="Macedonia" <?php if (trim($country) == "Macedonia") {echo "selected";}?> >Macedonia</option>
                <option value="Madagascar" <?php if (trim($country) == "Madagascar") {echo "selected";}?> >Madagascar</option>
                <option value="Malawi" <?php if (trim($country) == "Malawi") {echo "selected";}?> >Malawi</option>
                <option value="Malaysia" <?php if (trim($country) == "Malaysia") {echo "selected";}?> >Malaysia</option>
                <option value="Maldives" <?php if (trim($country) == "Maldives") {echo "selected";}?> >Maldives</option>
                <option value="Mali" <?php if (trim($country) == "Mali") {echo "selected";}?> >Mali</option>
                <option value="Malta" <?php if (trim($country) == "Malta") {echo "selected";}?> >Malta</option>
                <option value="Marshall-Islands" <?php if (trim($country) == "Marshall-Islands") {echo "selected";}?> >Marshall-Islands</option>
                <option value="Martinique" <?php if (trim($country) == "Martinique") {echo "selected";}?> >Martinique</option>
                <option value="Mauritius" <?php if (trim($country) == "Mauritius") {echo "selected";}?> >Mauritius</option>
                <option value="Mayotte" <?php if (trim($country) == "Mayotte") {echo "selected";}?> >Mayotte</option>
                <option value="Mexico" <?php if (trim($country) == "Mexico") {echo "selected";}?> >Mexico</option>
                <option value="Moldavia" <?php if (trim($country) == "Moldavia") {echo "selected";}?> >Moldavia</option>
                <option value="Monaco" <?php if (trim($country) == "Monaco") {echo "selected";}?> >Monaco</option>
                <option value="Mongolia" <?php if (trim($country) == "Mongolia") {echo "selected";}?> >Mongolia</option>
                <option value="Montenegro" <?php if (trim($country) == "Montenegro") {echo "selected";}?> >Montenegro</option>
                <option value="Montserrat" <?php if (trim($country) == "Montserrat") {echo "selected";}?> >Montserrat</option>
                <option value="Morocco" <?php if (trim($country) == "Morocco") {echo "selected";}?> >Morocco</option>
                <option value="Mozambique" <?php if (trim($country) == "Mozambique") {echo "selected";}?> >Mozambique</option>
                <option value="Myanmar" <?php if (trim($country) == "Myanmar") {echo "selected";}?> >Myanmar</option>
                <option value="Namibia" <?php if (trim($country) == "Namibia") {echo "selected";}?> >Namibia</option>
                <option value="Nauru" <?php if (trim($country) == "Nauru") {echo "selected";}?> >Nauru</option>
                <option value="Nepal" <?php if (trim($country) == "Nepal") {echo "selected";}?> >Nepal</option>
                <option value="Netherlands" <?php if (trim($country) == "Netherlands") {echo "selected";}?> >Netherlands</option>
                <option value="New-Caledonia" <?php if (trim($country) == "New-Caledonia") {echo "selected";}?> >New-Caledonia</option>
                <option value="New-Zealand" <?php if (trim($country) == "New-Zealand") {echo "selected";}?> >New-Zealand</option>
                <option value="Nicaragua" <?php if (trim($country) == "Nicaragua") {echo "selected";}?> >Nicaragua</option>
                <option value="Niger" <?php if (trim($country) == "Niger") {echo "selected";}?> >Niger</option>
                <option value="Nigeria" <?php if (trim($country) == "Nigeria") {echo "selected";}?> >Nigeria</option>
                <option value="Niue" <?php if (trim($country) == "Niue") {echo "selected";}?> >Niue</option>
                <option value="Norfolk-Island" <?php if (trim($country) == "Norfolk-Island") {echo "selected";}?> >Norfolk-Island</option>
                <option value="Northern-Cypress" <?php if (trim($country) == "Cypress") {echo "selected";}?> >Cypress</option>
                <option value="NorthernIslands" <?php if (trim($country) == "Northern") {echo "selected";}?> >Northern</option>
                <option value="Norway" <?php if (trim($country) == "Norway") {echo "selected";}?> >Norway</option>
                <option value="Oman" <?php if (trim($country) == "Oman") {echo "selected";}?> >Oman</option>
                <option value="Palau" <?php if (trim($country) == "Palau") {echo "selected";}?> >Palau</option>
                <option value="Pakistan" <?php if (trim($country) == "Pakistan") {echo "selected";}?> >Pakistan</option>
                <option value="Panama" <?php if (trim($country) == "Panama") {echo "selected";}?> >Panama</option>
                <option value="Papua-New-Guinea" <?php if (trim($country) == "Papua") {echo "selected";}?> >Papua</option>
                <option value="Paraguay" <?php if (trim($country) == "Paraguay") {echo "selected";}?> >Paraguay</option>
                <option value="Peru" <?php if (trim($country) == "Peru") {echo "selected";}?> >Peru</option>
                <option value="Philippines" <?php if (trim($country) == "Philippines") {echo "selected";}?> >Philippines</option>
                <option value="Pitcairn" <?php if (trim($country) == "Pitcairn") {echo "selected";}?> >Pitcairn</option>
                <option value="Poland" <?php if (trim($country) == "Poland") {echo "selected";}?> >Poland</option>
                <option value="Portugal" <?php if (trim($country) == "Portugal") {echo "selected";}?> >Portugal</option>
                <option value="Puerto-Rico" <?php if (trim($country) == "Puerto-Rico") {echo "selected";}?> >Puerto-Rico</option>
                <option value="Qatar" <?php if (trim($country) == "Qatar") {echo "selected";}?> >Qatar</option>
                <option value="Reunion" <?php if (trim($country) == "Reunion") {echo "selected";}?> >Reunion</option>
                <option value="Romania" <?php if (trim($country) == "Romania") {echo "selected";}?> >Romania</option>
                <option value="Russian-Federation" <?php if (trim($country) == "Russian") {echo "selected";}?> >Russian</option>
                <option value="Rwanda" <?php if (trim($country) == "Rwanda") {echo "selected";}?> >Rwanda</option>
                <option value="San-Marino" <?php if (trim($country) == "San-Marino") {echo "selected";}?> >San-Marino</option>
                <option value="Saudi-Arabia" <?php if (trim($country) == "Saudi-Arabia") {echo "selected";}?> >Saudi-Arabia</option>
                <option value="Senegal" <?php if (trim($country) == "Senegal") {echo "selected";}?> >Senegal</option>
                <option value="Serbia" <?php if (trim($country) == "Serbia") {echo "selected";}?> >Serbia</option>
                <option value="Seychelles" <?php if (trim($country) == "Seychelles") {echo "selected";}?> >Seychelles</option>
                <option value="Sierra-Leone" <?php if (trim($country) == "Sierra-Leone") {echo "selected";}?> >Sierra-Leone</option>
                <option value="Singapore" <?php if (trim($country) == "Singapore") {echo "selected";}?> >Singapore</option>
                <option value="Sri-Lanka" <?php if (trim($country) == "Sri-Lanka") {echo "selected";}?> >Sri-Lanka</option>
                <option value="Slovakia" <?php if (trim($country) == "Slovakia") {echo "selected";}?> >Slovakia</option>
                <option value="Slovenia" <?php if (trim($country) == "Slovenia") {echo "selected";}?> >Slovenia</option>
                <option value="Solomon-Islands" <?php if (trim($country) == "Solomon-Islands") {echo "selected";}?> >Solomon-Islands</option>
                <option value="Somalia" <?php if (trim($country) == "Somalia") {echo "selected";}?> >Somalia</option>
                <option value="South-Africa" <?php if (trim($country) == "South-Africa") {echo "selected";}?> >South-Africa</option>
                <option value="South-Georgia" <?php if (trim($country) == "South-Georgia") {echo "selected";}?> >South-Georgia</option>
                <option value="Spain" <?php if (trim($country) == "Spain") {echo "selected";}?> >Spain</option>
                <option value="St.-Helena" <?php if (trim($country) == "St.-Helena") {echo "selected";}?> >St.-Helena</option>
                <option value="St.-Lucia" <?php if (trim($country) == "St.-Lucia") {echo "selected";}?> >St.-Lucia</option>
                <option value="Sudan" <?php if (trim($country) == "Sudan") {echo "selected";}?> >Sudan</option>
                <option value="Suriname" <?php if (trim($country) == "Suriname") {echo "selected";}?> >Suriname</option>
                <option value="Svalbard" <?php if (trim($country) == "Svalbard") {echo "selected";}?> >Svalbard</option>
                <option value="Swaziland" <?php if (trim($country) == "Swaziland") {echo "selected";}?> >Swaziland</option>
                <option value="Sweden" <?php if (trim($country) == "Sweden") {echo "selected";}?> >Sweden</option>
                <option value="Switzerland" <?php if (trim($country) == "Switzerland") {echo "selected";}?> >Switzerland</option>
                <option value="Syrian" <?php if (trim($country) == "Syrian") {echo "selected";}?> >Syrian-Arab</option>
                <option value="Tajikistan" <?php if (trim($country) == "Tajikistan") {echo "selected";}?> >Tajikistan</option>
                <option value="Tanzania" <?php if (trim($country) == "Tanzania") {echo "selected";}?> >Tanzania</option>
                <option value="Taiwan" <?php if (trim($country) == "Taiwan") {echo "selected";}?> >Taiwan</option>
                <option value="Thailand" <?php if (trim($country) == "Thailand") {echo "selected";}?> >Thailand</option>
                <option value="Togo" <?php if (trim($country) == "Togo") {echo "selected";}?> >Togo</option>
                <option value="Tokelau" <?php if (trim($country) == "Tokelau") {echo "selected";}?> >Tokelau</option>
                <option value="Tonga" <?php if (trim($country) == "Tonga") {echo "selected";}?> >Tonga</option>
                <option value="Tunisia" <?php if (trim($country) == "Tunisia") {echo "selected";}?> >Tunisia</option>
                <option value="Turkey" <?php if (trim($country) == "Turkey") {echo "selected";}?> >Turkey</option>
                <option value="Turkmenistan" <?php if (trim($country) == "Turkmenistan") {echo "selected";}?> >Turkmenistan</option>
                <option value="Turks" <?php if (trim($country) == "Turks") {echo "selected";}?> >Turks</option>
                <option value="Tuvalu" <?php if (trim($country) == "Tuvalu") {echo "selected";}?> >Tuvalu</option>
                <option value="Uganda" <?php if (trim($country) == "Uganda") {echo "selected";}?> >Uganda</option>
                <option value="Ukraine" <?php if (trim($country) == "Ukraine") {echo "selected";}?> >Ukraine</option>
                <option value="UAE" <?php if (trim($country) == "UAE") {echo "selected";}?> >UAE</option>
                <option value="United States" <?php if (trim($country) == "United States") {echo "selected";}?> >United States</option>
                <option value="United-Kingdom" <?php if (trim($country) == "United-Kingdom") {echo "selected";}?> >United-Kingdom</option>
                <option value="Uruguay" <?php if (trim($country) == "Uruguay") {echo "selected";}?> >Uruguay</option>
                <option value="Uzbekistan" <?php if (trim($country) == "Uzbekistan") {echo "selected";}?> >Uzbekistan</option>
                <option value="Vanuatu" <?php if (trim($country) == "Vanuatu") {echo "selected";}?> >Vanuatu</option>
                <option value="Venezuela" <?php if (trim($country) == "Venezuela") {echo "selected";}?> >Venezuela</option>
                <option value="Viet-Nam" <?php if (trim($country) == "Viet-Nam") {echo "selected";}?> >Viet-Nam</option>
                <option value="West-Bank" <?php if (trim($country) == "West-Bank") {echo "selected";}?> >West-Bank</option>
                <option value="Western-Sahara" <?php if (trim($country) == "Western-Sahara") {echo "selected";}?> >Western-Sahara</option>
                <option value="Western-Samoa" <?php if (trim($country) == "Western-Samoa") {echo "selected";}?> >Western-Samoa</option>
                <option value="Yemen" <?php if (trim($country) == "Yemen") {echo "selected";}?> >Yemen</option>
                <option value="Yugoslavia" <?php if (trim($country) == "Yugoslavia") {echo "selected";}?> >Yugoslavia</option>
                <option value="Zaire" <?php if (trim($country) == "Zaire") {echo "selected";}?> >Zaire</option>
                <option value="Zambia" <?php if (trim($country) == "Zambia") {echo "selected";}?> >Zambia</option>
                <option value="Zimbabwe" <?php if (trim($country) == "Zimbabwe") {echo "selected";}?> >Zimbabwe</option>
                <option value="Burma" <?php if (trim($country) == "Burma") {echo "selected";}?> >Burma</option>
                <option value="Cameroon" <?php if (trim($country) == "Cameroon") {echo "selected";}?> >Cameroon</option>
                <option value="Isle-of-Man" <?php if (trim($country) == "Isle-of-Man") {echo "selected";}?> >Isle-of-Man</option>
                <option value="Jersey" <?php if (trim($country) == "Jersey") {echo "selected";}?> >Jersey</option>
                <option value="Kyrgyzstan" <?php if (trim($country) == "Kyrgyzstan") {echo "selected";}?> >Kyrgyzstan</option>
                <option value="Libya" <?php if (trim($country) == "Libya") {echo "selected";}?> >Libya</option>
                <option value="The-Bahamas" <?php if (trim($country) == "The-Bahamas") {echo "selected";}?> >The-Bahamas</option>
                <option value="The-Gambia" <?php if (trim($country) == "The-Gambia") {echo "selected";}?> >The-Gambia</option>
              </select></td>
          </tr>
          <tr>
            <td align="left" valign="top" style="font-weight:bold">Gender: </td>
            <td><input name="is_male" type="checkbox" id="is_male" value="Y" onClick="validate_form()" <?php if (trim($is_male) == "Y") {echo 'checked="CHECKED"';}?> >
              Male
              <input name="is_female" type="checkbox" id="is_female" value="Y" onClick="validate_form()" <?php if (trim($is_female) == "Y") {echo "checked";}?> >
              Female</td>
          </tr>

		  <!-- 	
          <tr>
            <td align="left" valign="top" style="font-weight:bold">School Type:</td>
            <td><input name="school_type_public" type="checkbox" id="school_type_public" checked="CHECKED" onClick="validate_form()">
              Public School
              <input name="school_type_private" type="checkbox" id="school_type_private" checked="CHECKED" onClick="validate_form()">
              Private Schools</td>
          </tr>
          -->
          
          <tr>
            <td align="left" valign="top" style="font-weight:bold">Schools:</td>
            <td><select name="tbl_school_id_param[]" size="6" multiple="MULTIPLE" id="tbl_school_id_param[]">
                <?php 
				$qry_schools = "SELECT * FROM ".TBL_SCHOOL." WHERE is_active='Y' ";
				$rs_schools = selectMultiRecords($qry_schools);
			
					for ($s=0; $s<count($rs_schools); $s++) { 
						$tbl_school_id_s = $rs_schools[$s]['tbl_school_id'];
						$school_name_s = $rs_schools[$s]['school_name'];
				?>
	                <option value="<?=$tbl_school_id_s?>" <?php if (in_array($tbl_school_id_s, $tbl_school_id_param) || !isset($tbl_school_id_param)) {echo "selected";}?>><?=$school_name_s?></option>
                <?php } ?>
              </select>
              [Hold Down CTRL key to select more than one school]</td>
          </tr>
          <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td><input type="submit" name="submitt" id="submitt" value="Generate Report"></td>
          </tr>
          <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </form>
      <table width="80%" border="0" cellspacing="0" cellpadding="0" style="border:1px solid #E0E0E0">
        <thead>
          <tr>
            <td width="40%" height="25" align="center" valign="middle" bgcolor="#E0E0E0" style="font-weight:bold">School Name</td>
            <td width="20%" align="center" valign="middle" bgcolor="#E0E0E0" style="font-weight:bold">Total Students</td>
            <td width="20%" align="center" valign="middle" bgcolor="#E0E0E0" style="font-weight:bold">Students Present</td>
            <td width="20%" align="center" valign="middle" bgcolor="#E0E0E0" style="font-weight:bold">Students Absent</td>
          </tr>
        </thead>
        <tbody>
          <?php
                for ($i=0; $i<count($school_name_arr); $i++) {
                    $total_students_absent = $total_students_arr[$i] - $total_students_present_arr[$i];
              ?>
          <tr>
            <td height="20" align="center" valign="middle" style="border: 1px solid #E0E0E0;"><?=get_School_name($school_name_arr[$i]);?></td>
            <td align="center" valign="middle" style="border: 1px solid #E0E0E0;"><?=$total_students_arr[$i]?></td>
            <td align="center" valign="middle" style="border: 1px solid #E0E0E0;"><?=$total_students_present_arr[$i]?></td>
            <td align="center" valign="middle" style="border: 1px solid #E0E0E0;"><?=$total_students_absent?></td>
          </tr>
          <?php
                }
              ?>
        </tbody>
      </table>
      <br>
      <br>
      <br>
      <br>
      
      <!--Chart START--> 
      <script type="text/javascript">
      window.onload = function () {
        var chart = new CanvasJS.Chart("chartContainer", {
        
          axisX:{
            title: "School Name",
          },		
          axisY:{
            title: "Absent Students",
          },		
          title:{
            text: ""              
          },
          data: [         
            {  
             type: "column",
             dataPoints: [
        <?php
			//$tmp = 100;
          	for ($s=0; $s<count($school_name_arr); $s++) { 
					$school_name = $school_name_arr[$s];
					$total_students = $total_students_arr[$s];
					$total_students_present = $total_students_present_arr[$s];
					
					//$total_students += $tmp;
					//$tmp += 100;	
					
					$absent_students = $total_students - $total_students_present;
		?>				 
		
				 { label: "<?=get_School_name($school_name);?>", y: <?=$absent_students?>, indexLabel: "Total:<?=$total_students ?>, Absent:<?=$absent_students?>" } <?php if ($s+1 != count($school_name_arr)) {echo ",";}?>
		<?php
			}
		?>		 
             ]
           }
           ]
         });
    
        chart.render();
      }
      </script> 
      <script type="text/javascript" src="<?=HOST_URL?>/js/canvasjs.min.js"></script>
      <div id="chartContainer" class="chart_height" style="width: 80%; margin:auto"> </div>
      
      <!--Chart END--></td>
  </tr>
</table>
<?php include("../footer.php"); ?>
