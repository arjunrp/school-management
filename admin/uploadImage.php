    <?php	include (ROOT_PATH."/includes/config.inc.php");
    header('Content-Type: application/json; charset=utf-8');
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST');
	
	$output_dir = "uploads/";
	$module_name = $_REQUEST["module_name"];
	$lan = $_REQUEST["lan"];
	$device = $_REQUEST["device"];
	$tbl_teacher_id = $_REQUEST["tbl_teacher_id"];
	$tbl_student_id = $_REQUEST["tbl_student_id"];
	$application_message = $_REQUEST["application_message"];
	$tbl_class_id = $_REQUEST["tbl_class_id"];

	$file_name_original = basename($_FILES['Filedata']['name']);
	$Ext = strchr($file_name_original,".");
	$Ext = strtolower($Ext);
	$file_name_updated_ = getMD5_ID();
	$file_name_updated = $file_name_updated_.$Ext;
	$file_name_original_temp = $_FILES["Filedata"]["tmp_name"];
	$file_type = $_FILES["Filedata"]["type"];
	$file_size = $_FILES["Filedata"]["size"];
	
	$copy = file_copy("$file_name_original_temp", UPLOADS_PATH."/", "$file_name_updated", 1, 1);

	$tbl_item_id = getMD5_ID();
	$tbl_uploads_id = getMD5_ID();
	
	$qry = "INSERT INTO ".TBL_UPLOADS." (
		`tbl_uploads_id` ,
		`module_name` ,
		`tbl_item_id` ,
		`file_name_original` ,
		`file_name_updated` ,
		`file_type` ,
		`file_size` ,
		`is_active` ,
		`added_date`
		)
		VALUES (
			'$tbl_uploads_id', '$module_name', '$tbl_item_id', '$file_name_original', '$file_name_updated', '$file_type', '$file_size', 'Y', NOW()
		)";
	insertInto($qry);
	
	// SEND MESSAGE
	if ($tbl_student_id == "all_students") { // Send message to all parents in the class.
			
			$qry = "SELECT * FROM ".TBL_STUDENT." WHERE tbl_class_id='".$tbl_class_id."' AND is_active='Y' ORDER BY first_name ASC ";
			$data_rs = selectMultiRecords($qry);
			
			for($i=0; $i<count($data_rs); $i++) {
					$tbl_student_id = $data_rs[$i]["tbl_student_id"];
					
					$tbl_instant_pics_id = getMD5_ID();
					$qry = "INSERT INTO ".TBL_INSTANT_PICS." (
						`tbl_instant_pics_id` ,
						`tbl_teacher_id` ,
						`tbl_student_id` ,
						`message` ,
						`device` ,
						`lan` ,
						`tbl_uploads_id` ,
						`is_watched` ,
						`is_active` ,
						`added_date`
						)
						VALUES (
						'$tbl_instant_pics_id', '$tbl_teacher_id', '$tbl_student_id', '$application_message', '$device', '$lan','$tbl_uploads_id', 'N', 'Y', NOW()
						)";
					insertInto($qry);
					//echo "<br>".$qry;
					
					// Get parent details from student id
					$tbl_student_id = mysql_real_escape_string(trim($tbl_student_id));
					$qry = "SELECT tbl_parent_id FROM ".TBL_PARENT_STUDENT." WHERE tbl_student_id='".$tbl_student_id."' AND is_active='Y' ";
					$rs = SelectMultiRecords($qry);
					$tbl_parent_id = $rs[0]["tbl_parent_id"];
					
					$qry = "SELECT * FROM ".TBL_PARENT." WHERE tbl_parent_id='".$tbl_parent_id."'";
					$rs = SelectMultiRecords($qry);
					$first_name_parent = $rs[0]["first_name"];
					$last_name_parent = $rs[0]["last_name"];
					
					// Get teacher details from teacher id
					$qry = "SELECT * FROM ".TBL_TEACHER." WHERE tbl_teacher_id='".$tbl_teacher_id."'";
					$rs = SelectMultiRecords($qry);
					$first_name_teacher = $rs[0]["first_name"];
					$last_name_teacher = $rs[0]["last_name"];
					
					if (ENABLE_PUSH == "Y") {
						$message = 'Dear '.$first_name_parent.' '.$last_name_parent.', Teacher '.$first_name_teacher.' '.$last_name_teacher.' has sent you an instant picture' ;
						$message = urlencode($message);
						$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=nYTmk7YnGxHBOcFy-1J5e6ERhgN0Z5oY9Trn26ZvQ40&user=".$tbl_parent_id."&msg=".$message."&data=";
						//echo $url;
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
						curl_setopt($ch, CURLOPT_URL, $url);
						curl_setopt($ch, CURLOPT_HEADER, 0);
						curl_exec($ch);
						curl_close($ch);
					}
				
			} //for($i=0; $i<count($data_rs); $i++) {
	
	//if ($tbl_student_id == "all_students") {
	} else {
	
			$tbl_instant_pics_id = getMD5_ID();
			$qry = "INSERT INTO ".TBL_INSTANT_PICS." (
				`tbl_instant_pics_id` ,
				`tbl_teacher_id` ,
				`tbl_student_id` ,
				`message` ,
				`device` ,
				`lan` ,
				`tbl_uploads_id` ,
				`is_watched` ,
				`is_active` ,
				`added_date`
				)
				VALUES (
				'$tbl_instant_pics_id', '$tbl_teacher_id', '$tbl_student_id', '$application_message', '$device', '$lan','$tbl_uploads_id', 'N', 'Y', NOW()
				)";
			insertInto($qry);
			//echo "<br>".$qry;
			
			// Get parent details from student id
			$tbl_student_id = mysql_real_escape_string(trim($tbl_student_id));
			$qry = "SELECT tbl_parent_id FROM ".TBL_PARENT_STUDENT." WHERE tbl_student_id='".$tbl_student_id."' AND is_active='Y' ";
			$rs = SelectMultiRecords($qry);
			$tbl_parent_id = $rs[0]["tbl_parent_id"];
			
			$qry = "SELECT * FROM ".TBL_PARENT." WHERE tbl_parent_id='".$tbl_parent_id."'";
			$rs = SelectMultiRecords($qry);
			$first_name_parent = $rs[0]["first_name"];
			$last_name_parent = $rs[0]["last_name"];
			
			// Get teacher details from teacher id
			$qry = "SELECT * FROM ".TBL_TEACHER." WHERE tbl_teacher_id='".$tbl_teacher_id."'";
			$rs = SelectMultiRecords($qry);
			$first_name_teacher = $rs[0]["first_name"];
			$last_name_teacher = $rs[0]["last_name"];
			
			if (ENABLE_PUSH == "Y") {
				$message = 'Dear '.$first_name_parent.' '.$last_name_parent.', Teacher '.$first_name_teacher.' '.$last_name_teacher.' sent you an instant picture' ;
				$message = urlencode($message);
				$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=nYTmk7YnGxHBOcFy-1J5e6ERhgN0Z5oY9Trn26ZvQ40&user=".$tbl_parent_id."&msg=".$message."&data=";
				//echo $url;
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_exec($ch);
				curl_close($ch);
			}
	// if ($tbl_student_id == "all_students") {
	}
	?>
