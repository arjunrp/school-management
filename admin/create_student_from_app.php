<?php	@session_start(); error_reporting(0);
include ($_SERVER["DOCUMENT_ROOT"] ."/aqdar/includes/config.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Aqdar</title>
<script src="<?=JS_PATH?>/jquery-1.3.2.min.js"></script>
<script language="javascript">
	$(document).ready(function(e) {
	});
	
	function ajax_create() {
		//alert("<?=HOST_URL?>/en/admin/student/add_student_from_app");
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/en/admin/student/add_student_from_app",
			data: {
				student_id_enc: "student001",
				school_id	  : "9162b77c3c344ea",	
				first_name    : "first_name",
				last_name     : "last_name",
				first_name_ar : "first_name_ar",
				last_name_ar  : "last_name_ar",
				dob_month     : "dob_month",
				dob_day       : "dob_day",
				dob_year      : "dob_year",
				gender        : "male",
				mobile        : "mobile",
				email         : "email",
				tbl_emirates_id : "tbl_emirates_iddddd",
				country       : "country",
				emirates_id_father : "emirates_id_father",
				emirates_id_mother : "emirates_id_mother",
				tbl_academic_year_id : "tbl_academic_year_id",
				tbl_class_id         : "tbl_class_id",
				
				first_name_parent      : "first_name_parent",
				first_name_parent_ar   : "first_name_parent_ar",
				last_name_parent       : "last_name_parent",
				last_name_parent_ar    : "last_name_parent_ar",
				dob_month_parent       : "dob_month_parent",
				dob_day_parent         : "dob_day_parent",
				dob_year_parent        : "dob_year_parent",
				gender_parent          : "male", 
				mobile_parent          : "mobile_parent",
				email_parent           : "email_parentx",
				emirates_id_parent     : "emirates_id_parenttttt",
				parent_user_id         : "parent_user_idx",
				password               : "password",
				is_ajax: true
			},
			success: function(data) {
				alert(data);
			},
			error: function() {
				//$("#pre-loader").hide();
			}, 
			complete: function() {
				//$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_create_student() {
		//alert("<?=HOST_URL?>/en/admin/student/add_student_from_app");
		//alert("<?=HOST_URL?>/en/admin/student/add_only_student_from_app/?user_id=ff0dbafd316840ed6452439b182a4767&role=P&lan=ar&device=none&device_uid=none&school_id=9162b77c3c344ea&rnd=8");
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/en/admin/student/add_only_student_from_app/?user_id=ff0dbafd316840ed6452439b182a4767&role=P&lan=ar&device=none&device_uid=none&school_id=9162b77c3c344ea&rnd=8",
			data: {
				student_id_enc: "student001xx",
				school_id	  : "9162b77c3c344ea",	
				first_name    : "first_name",
				last_name     : "last_name",
				first_name_ar : "first_name_ar",
				last_name_ar  : "last_name_ar",
				dob_month     : "dob_month",
				dob_day       : "dob_day",
				dob_year      : "dob_year",
				gender        : "male",
				mobile        : "mobile",
				email         : "email",
				tbl_emirates_id : "tbl_emirates_idddddxx",
				country       : "country",
				emirates_id_father : "emirates_id_father",
				emirates_id_mother : "emirates_id_mother",
				tbl_academic_year_id : "tbl_academic_year_id",
				tbl_class_id         : "tbl_class_id",
				
				is_ajax: true
			},
			success: function(data) {
				alert(data);
				
			},
			error: function() {
				//$("#pre-loader").hide();
			}, 
			complete: function() {
				//$("#pre-loader").hide();
			}
		});
	}
</script>
</head>
<body>
	<input type="button" name="clickme" value="Register Parent &amp; Student" onclick="ajax_create()" />
    <input type="button" name="clickme" value="Register Student Only" onclick="ajax_create_student()" />
</body>
</html>