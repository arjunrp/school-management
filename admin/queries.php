<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<body>
<?php	
	$qry = "SELECT 
	vmoj_barza_hearlist.C_HEARING_DT AS C_HEARING_DT,
	vmoj_barza_hearlist.C_SCHEDULED_START_TIME AS C_SCHEDULED_START_TIME,
	vmoj_barza_hearlist.C_SESSION_NO AS C_SESSION_NO,
	vmoj_barza_hearlist.C_CASE_SERIAL_NUM AS C_CASE_SERIAL_NUM,
	vmoj_barza_hearlist.C_CASE_YEAR AS C_CASE_YEAR,
	vmoj_barza_hearlist.C_COURT_ROOM_NAME AS C_COURT_ROOM_NAME,
	vmoj_barza_hearlist.C_LABEL AS C_LABEL,
	vmoj_barza_hearlist.C_LOCATION_NAME AS C_LOCATION_NAME,
	vmoj_barza_hearlist.C_COURT_NAME AS C_COURT_NAME,
	vmoj_barza_hearlist.C_PROCEEDING_NAME AS C_PROCEEDING_NAME,
	vmoj_barza_hearlist.C_LOCALIZE_ROOM_NAME AS C_LOCALIZE_ROOM_NAME,
	vmoj_barza_hearlist.C_LOCALIZE_LABEL AS C_LOCALIZE_LABEL,
	vmoj_barza_hearlist.C_LOCALIZE_LOCATION AS C_LOCALIZE_LOCATION,
	vmoj_barza_hearlist.C_LOCALIZE_COURT AS C_LOCALIZE_COURT,
	vmoj_barza_hearlist.C_LOCALIZE_PROCEEDING AS C_LOCALIZE_PROCEEDING,
	
	vmoj_barza_hearatt.C_LABEL AS judge,
	vmoj_barza_hearatt.C_LOCALIZE_LABEL AS judge_ar,
	
	vmoj_kiosk_casedetails.C_CREATED_DT AS C_CREATED_DT,  
	vmoj_kiosk_casedetails.C_CASE_DETAILS AS C_CASE_DETAILS,
	vmoj_kiosk_casedetails.C_CONCLUSION_REMARKS AS C_CONCLUSION_REMARKS,	
	
	vmoj_kiosk_partydetails.C_NAME AS person_name,
	vmoj_kiosk_partydetails.C_LABEL AS country_of_origin,
	vmoj_kiosk_partydetails.C_LOCALIZE_LABEL AS country_of_origin_ar,
	vmoj_kiosk_partydetails.C_PARTY_TYPE_NAME AS C_PARTY_TYPE_NAME,
	vmoj_kiosk_partydetails.C_LOCALIZE_PARTYTYPE AS C_LOCALIZE_PARTYTYPE
		
	FROM
	
	vmoj_barza_hearlist, vmoj_barza_hearatt, vmoj_kiosk_casedetails, vmoj_kiosk_partydetails
	
	WHERE
	
	vmoj_barza_hearlist.C_COURT_NAME = vmoj_barza_hearatt.C_COURT_NAME AND
	vmoj_barza_hearlist.C_CASE_SERIAL_NUM = vmoj_barza_hearatt.C_CASE_SERIAL_NUM AND 
	vmoj_barza_hearlist.C_PROCEEDING_NAME = vmoj_barza_hearatt.C_PROCEEDING_NAME AND 
	
	vmoj_barza_hearatt.C_COURT_NAME = vmoj_kiosk_casedetails.C_COURT_NAME AND
	vmoj_barza_hearatt.C_CASE_SERIAL_NUM = vmoj_kiosk_casedetails.C_CASE_SERIAL_NUM AND 
	vmoj_barza_hearatt.C_PROCEEDING_NAME = vmoj_kiosk_casedetails.C_PROCEEDING_NAME AND
	
	vmoj_kiosk_casedetails.C_COURT_NAME = vmoj_kiosk_partydetails.C_COURT_NAME AND
	vmoj_kiosk_casedetails.C_CASE_SERIAL_NUM = vmoj_kiosk_partydetails.C_CASE_SERIAL_NUM AND 
	vmoj_kiosk_casedetails.C_PROCEEDING_NAME = vmoj_kiosk_partydetails.C_PROCEEDING_NAME
	";
?>
<table width="100%" border="0" cellpadding="2" cellspacing="2">
  <tr>
    <td width="140" align="center" valign="middle">C_HEARING_DT</td>
    <td width="140" align="center" valign="middle">C_SCHEDULED_START_TIME</td>
    <td width="140" align="center" valign="middle">C_SESSION_NO</td>
    <td width="140" align="center" valign="middle">C_CASE_SERIAL_NUM</td>
    <td width="140" align="center" valign="middle">C_CASE_YEAR</td>
    <td width="140" align="center" valign="middle">C_COURT_ROOM_NAME</td>
    <td width="140" align="center" valign="middle">C_LABEL</td>
    <td width="140" align="center" valign="middle">C_LOCATION_NAME</td>
    <td width="140" align="center" valign="middle">C_COURT_NAME</td>
    <td width="140" align="center" valign="middle">C_PROCEEDING_NAME</td>
    <td width="140" align="center" valign="middle">C_LOCALIZE_ROOM_NAME</td>
    <td width="140" align="center" valign="middle">C_LOCALIZE_LABEL</td>
    <td width="140" align="center" valign="middle">C_LOCALIZE_LOCATION</td>
    <td width="140" align="center" valign="middle">C_LOCALIZE_COURT</td>
    <td width="140" align="center" valign="middle">C_LOCALIZE_PROCEEDING</td>
    <td width="140" align="center" valign="middle" bgcolor="#F3F3F3">judge</td>
    <td width="140" align="center" valign="middle" bgcolor="#F3F3F3">judge_ar</td>
    <td width="140" align="center" valign="middle" bgcolor="#D5FFD5">C_CREATED_DT</td>
    <td width="140" align="center" valign="middle" bgcolor="#D5FFD5">C_CASE_DETAILS</td>
    <td width="140" align="center" valign="middle" bgcolor="#D5FFD5">C_CONCLUSION_REMARKS</td>
    <td width="140" align="center" valign="middle" bgcolor="#D9D9FF">person_name</td>
    <td width="140" align="center" valign="middle" bgcolor="#D9D9FF">country_of_origin</td>
    <td width="140" align="center" valign="middle" bgcolor="#D9D9FF">country_of_origin_ar</td>
    <td width="140" align="center" valign="middle" bgcolor="#D9D9FF">C_PARTY_TYPE_NAME</td>
    <td width="140" align="center" valign="middle" bgcolor="#D9D9FF">C_LOCALIZE_PARTYTYPE</td>
  </tr>
  <?php
  for ($i=0; $i<count($data); $i++) {	
			  $C_HEARING_DT = $data[$i]["C_HEARING_DT"];
			  $C_SCHEDULED_START_TIME = $data[$i]["C_SCHEDULED_START_TIME"];
			  $C_SESSION_NO = $data[$i]["C_SESSION_NO"];
			  $C_CASE_SERIAL_NUM = $data[$i]["C_CASE_SERIAL_NUM"];
			  $C_CASE_YEAR = $data[$i]["C_CASE_YEAR"];
			  $C_COURT_ROOM_NAME = $data[$i]["C_COURT_ROOM_NAME"];
			  $C_LABEL = $data[$i]["C_LABEL"];
			  $C_LOCATION_NAME = $data[$i]["C_LOCATION_NAME"];
			  $C_COURT_NAME = $data[$i]["C_COURT_NAME"];
			  $C_PROCEEDING_NAME = $data[$i]["C_PROCEEDING_NAME"];
			  $C_LOCALIZE_ROOM_NAME = $data[$i]["C_LOCALIZE_ROOM_NAME"];
			  $C_LOCALIZE_LABEL = $data[$i]["C_LOCALIZE_LABEL"];
			  $C_LOCALIZE_LOCATION = $data[$i]["C_LOCALIZE_LOCATION"];
			  $C_LOCALIZE_COURT = $data[$i]["C_LOCALIZE_COURT"];
			  $C_LOCALIZE_PROCEEDING = $data[$i]["C_LOCALIZE_PROCEEDING"];
			  $judge = $data[$i]["judge"];
			  $judge_ar = $data[$i]["judge_ar"];
			  $C_CREATED_DT = $data[$i]["C_CREATED_DT"];
			  $C_CASE_DETAILS = $data[$i]["C_CASE_DETAILS"];
			  $C_CONCLUSION_REMARKS = $data[$i]["C_CONCLUSION_REMARKS"];
			  $person_name = $data[$i]["person_name"];
			  $country_of_origin = $data[$i]["country_of_origin"];
			  $country_of_origin_ar = $data[$i]["country_of_origin_ar"];
			  $C_PARTY_TYPE_NAME = $data[$i]["C_PARTY_TYPE_NAME"];
			  $C_LOCALIZE_PARTYTYPE = $data[$i]["C_LOCALIZE_PARTYTYPE"];
  ?>
          <tr>
            <td width="140" align="center" valign="middle"><?=$C_HEARING_DT;?></td>
            <td width="140" align="center" valign="middle"><?=$C_SCHEDULED_START_TIME;?></td>
            <td width="140" align="center" valign="middle"><?=$C_SESSION_NO;?></td>
            <td width="140" align="center" valign="middle"><?=$C_CASE_SERIAL_NUM;?></td>
            <td width="140" align="center" valign="middle"><?=$C_CASE_YEAR;?></td>
            <td width="140" align="center" valign="middle"><?=$C_COURT_ROOM_NAME;?></td>
            <td width="140" align="center" valign="middle"><?=$C_LABEL;?></td>
            <td width="140" align="center" valign="middle"><?=$C_LOCATION_NAME;?></td>
            <td width="140" align="center" valign="middle"><?=$C_COURT_NAME;?></td>
            <td width="140" align="center" valign="middle"><?=$C_PROCEEDING_NAME;?></td>
            <td width="140" align="center" valign="middle"><?=$C_LOCALIZE_ROOM_NAME;?></td>
            <td width="140" align="center" valign="middle"><?=$C_LOCALIZE_LABEL;?></td>
            <td width="140" align="center" valign="middle"><?=$C_LOCALIZE_LOCATION;?></td>
            <td width="140" align="center" valign="middle"><?=$C_LOCALIZE_COURT;?></td>
            <td width="140" align="center" valign="middle"><?=$C_LOCALIZE_PROCEEDING;?></td>
            <td width="140" align="center" valign="middle" bgcolor="#F3F3F3"><?=$judge;?></td>
            <td width="140" align="center" valign="middle" bgcolor="#F3F3F3"><?=$judge_ar;?></td>
            <td width="140" align="center" valign="middle" bgcolor="#D5FFD5"><?=$C_CREATED_DT;?></td>
            <td width="140" align="center" valign="middle" bgcolor="#D5FFD5"><?=$C_CASE_DETAILS;?></td>
            <td width="140" align="center" valign="middle" bgcolor="#D5FFD5"><?=$C_CONCLUSION_REMARKS;?></td>
            <td width="140" align="center" valign="middle" bgcolor="#D9D9FF"><?=$person_name;?></td>
            <td width="140" align="center" valign="middle" bgcolor="#D9D9FF"><?=$country_of_origin;?></td>
            <td width="140" align="center" valign="middle" bgcolor="#D9D9FF"><?=$country_of_origin_ar;?></td>
            <td width="140" align="center" valign="middle" bgcolor="#D9D9FF"><?=$C_PARTY_TYPE_NAME;?></td>
            <td width="140" align="center" valign="middle" bgcolor="#D9D9FF"><?=$C_LOCALIZE_PARTYTYPE;?></td>
          </tr>
  
    <?php	}	?>
  
</table>
</body>
</html>
