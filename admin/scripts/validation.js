//********************** End News Management ************
function frmNewValidation(){
	if( document.frmRegister.author_name.value == "" ) {
		  alert ("Author Name cannot be empty!") ;
	  	  document.frmRegister.author_name.focus();
		  return false ; 
	  }
	
		
	if(document.frmRegister.author_name.value!="")	{
		var str = document.frmRegister.author_name.value;
		var regExp = / /g;
		var tmp = document.frmRegister.author_name.value;
		tmp = tmp.replace(regExp,'');
		if (tmp.length <= 0) {
			alert("Enter valid Author Name!");
			document.frmRegister.author_name.value="";
			document.frmRegister.author_name.focus();
			return false;
		}	
	}	  		
		for (var i = 0; i < str.length; i++) {
			var ch = str.substring(i, i + 1);
			if  ((ch < "a" || ch > "z" ) && (ch < "A" || ch > "Z" )  && (ch != " ") ) {
				alert("Enter valid Author Name!");						
				document.frmRegister.author_name.select();
				document.frmRegister.author_name.focus();
				return false;
				break;
			}
		}
	if(!isNaN(document.frmRegister.author_name.value) ) {
		  alert("Enter valid Author Name!");
		  document.frmRegister.author_name.focus();
		  document.frmRegister.author_name.select();		  
		  return false ; 
	}
	

			
  //----- author description ------
    if( document.frmRegister.author_description.value == "" ) {
		  alert ("Author Description cannot be empty!") ;
	  	  document.frmRegister.author_description.focus();
	      return false ; 
	  }
	if(document.frmRegister.author_description.value!="")	{
	 		var str = document.frmRegister.author_description.value;
		    	var regExp = / /g;
			var tmp = document.frmRegister.author_description.value;
			tmp = tmp.replace(regExp,'');
			if (tmp.length <= 0) 	{
					alert("Enter valid Author Description!");
					document.frmRegister.author_description.value="";
					document.frmRegister.author_description.focus();
					return false;
			}					
	}
   
   
   //--------- news_heading ----------
    if( document.frmRegister.news_heading.value == "" )  {
		  alert ("News Heading cannot be empty!") ;
	  	  document.frmRegister.news_heading.focus();
	      	  return false ; 
	  }
	if(document.frmRegister.news_heading.value!="")
	 	{
	 		var str = document.frmRegister.news_heading.value;
		    	var regExp = / /g;
			var tmp = document.frmRegister.news_heading.value;
			tmp = tmp.replace(regExp,'');
			if (tmp.length <= 0) 
				{
					alert("Enter valid News Heading!");
					document.frmRegister.news_heading.value="";
					document.frmRegister.news_heading.focus();
					return false;
				}					
		}
		
    //--------- news_heading ----------
   if (navigator.appName == "Microsoft Internet Explorer") { 
	   if (document.getElementById('richtext1').value == ""){
				alert("Description cannot be empty!");
				richtext1_x5.focus();
				return false;
		}
	}  else {
	 		var str = document.frmRegister.richtext1.value;
		    var regExp = / /g;
			var tmp = document.frmRegister.richtext1.value;
			tmp = tmp.replace(regExp,'');
			if (tmp.length <= 0) 
				{
					alert("Description cannot be empty!");
					document.frmRegister.richtext1.value="";
					document.frmRegister.richtext1.focus();
					return false;
				}					
	}
   
  		return true ;
	
	}
//------------------------------

function CheckAll(){
	var ml = document.frmDirectory;
	var len = ml.elements.length;
	if (document.frmDirectory.AC.checked==true) {
		 for (var i = 0; i < len; i++) {
			document.frmDirectory.elements[i].checked=true;
		 }
	} else {
		  for (var i = 0; i < len; i++)  {
			document.frmDirectory.elements[i].checked=false;
		  }
	}
}
//-----------------------------------------------------------------------
function UnCheckAll() {
	var ml = document.frmDirectory;
	var len = ml.elements.length;
	var count=0; var checked=0;
	    for (var i = 0; i < len; i++) {	       
			if ((document.frmDirectory.elements[i].type=='checkbox') && (document.frmDirectory.elements[i].name != "AC")) {
				count = count + 1;
	  		 	if (document.frmDirectory.elements[i].checked == true){
					checked = checked + 1;
				}
			}
	     }
		 
	if (checked == count) {
		 document.frmDirectory.AC.checked = true;
	} else {
		document.frmDirectory.AC.checked = false;
	}
}