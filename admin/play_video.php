<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<script type="text/javascript" charset="utf-8" src="jquery-1.7.1.min.js"></script>
<script language="javascript">
	$(document).ready(function(e) {
		var myVideo = document.getElementById('video_player_school_app');
		
		myVideo.addEventListener('canplay', function() {
			myVideo.play();
		});
	
		myVideo.load();
		myVideo.play();
	});
</script>
<style type="text/css">
	body {
		background-image: url(images/loading.gif);
		background-repeat: no-repeat;
		background-position: 50% 50%;	
	}
</style>
</head>
<body onload="play_vid()">
<video id="video_player_school_app" width="100%" height="100%" controls autoplay="autoplay">
<source id="temp_video" src="<?=IMG_UPLOAD_PATH?>/<?=$_REQUEST["video_name"]?>" type="video/mp4">
  Video is not supported </video>
</body>
</html>
