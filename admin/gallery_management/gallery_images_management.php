<?php	
    include("../header.php"); 
 	$sid =$_REQUEST["sid"];
	if (!loggedUser($sid)) { 
		redirect (HOST_URL ."/admin/include/messages.php?msg=relogin");
		exit();
	}
	$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
	
	$LIB_CLASSES_FOLDER =$_SERVER['DOCUMENT_ROOT']."/aqdar/lib/classes/";
	include($LIB_CLASSES_FOLDER."Paging.php");
	
	$q = stripslashes($q);	
	mysql_query("SET NAMES 'utf8'");
	mysql_query('SET CHARACTER SET utf8'); 
	// Thumb Image dimensions
	$max_x = 150;
	$max_y = 150;
	
	/*	INSERT RECORD	*/
	if($_REQUEST['ssubmit']=="insert_images") {
		 if(empty($_FILES["large_image"]["name"])){
			 $MSG = "Please upload images";
		 }else{
			for ($i=0; $i<=4; $i++) {
				if (trim($_FILES["large_image"]["name"][$i]) != "") {
					//Image Upload
					$file_name_original =$_FILES["large_image"]["name"][$i];
					$Ext = strchr($file_name_original,".");
					$Ext = strtolower($Ext);
			
					if ($Ext==".jpg" || $Ext==".jpeg" || $Ext==".gif" || $Ext==".bmp" || $Ext==".png") {
						$file_name_updated_ = getMD5_ID();
						$file_name_updated =$file_name_updated_.$Ext;
						$file_name_original_temp =$_FILES["large_image"]["tmp_name"][$i];
						
						//upload Large Image
						$copy = file_copy("$file_name_original_temp", UPLOAD_IMG_GALLERY_PATH."/", "$file_name_updated", 1, 1);
	
						//upload Thumb Image
						$thumb_name_original =$_FILES["thumb_image"]["name"][$i];
						if ($thumb_name_original != "") {
							$Ext = strchr($thumb_name_original,".");
							$Ext = strtolower($Ext);
							$file_name_updated_thumb =$file_name_updated_."_thumb".$Ext;
							$file_name_original_thumb_temp =$_FILES["thumb_image"]["tmp_name"][$i];
							
							//upload Thumb Image
							$copy = file_copy("$file_name_original_thumb_temp", UPLOAD_IMG_GALLERY_PATH."/", "$file_name_updated_thumb", 1, 1);
						} else {
							$file_name_updated_thumb =$file_name_updated_."_thumb".$Ext;
							$source = UPLOAD_IMG_GALLERY_PATH."/".$file_name_updated;
							$destination = UPLOAD_IMG_GALLERY_PATH."/".$file_name_updated_thumb;
							generate_thumb($source, $destination, $max_x, $max_y);
						}
						
						//upload Large Image
						//$copy = file_copy("$file_name_original_thumb_temp", UPLOAD_IMG_GALLERY_PATH."/", "$file_name_updated_thumb", 1, 1);
						$image_text_en_ = strip_tags($image_text_en[$i]);
						$image_text_ar_ = strip_tags($image_text_ar[$i]);
						$image_text_ur_ = strip_tags($image_text_ur[$i]);
						$tbl_image_gallery_id = getMD5_ID();
						$is_active_str = "is_active".($i+1);
						$is_active_ =$$is_active_str;
						$qry = "
							INSERT INTO ".TBL_IMAGE_GALLERY." (
							`tbl_image_gallery_id` ,
							`tbl_gallery_category_id` ,
							`file_name_original` ,
							`file_name_updated` ,
							`file_name_updated_thumb` ,
							`image_text_en` ,
							`image_text_ar` ,
							`image_text_ur` ,
							`image_order` ,
							`is_active` ,
							`added_date` ,
							`tbl_school_id`
							)
							VALUES (
							'$tbl_image_gallery_id', '$tbl_gallery_category_id','$file_name_original', '$file_name_updated', '$file_name_updated_thumb', '$image_text_en_', '$image_text_ar_', '$image_text_ur_', '1000', '$is_active_', NOW(), '$tbl_school_id'
						)";
						//echo $qry; exit;
						$MSG = "Images and their details have been saved successfully.";
						insertInto($qry);
						//echo $qry."<br><br>";
		
					} else {
						$MSG = "Image extension is not valid.<br>";
					}// if ($large_image && trim($_FILES["large_image"]["name"][$i]) != "") {
				} 
			}// for
	  }
	
	}
	
	/*	EDIT RECORD	*/
	if($ssubmit=="editRecord") {
				$qry = "SELECT * FROM ".TBL_IMAGE_GALLERY." WHERE tbl_image_gallery_id='$tbl_image_gallery_id'";	
				// echo $qry."<br>";
				$data = selectFrom($qry);
				$file_name_updated_stored =$data["file_name_updated"];
				$file_name_updated_thumb_stored =$data["file_name_updated_thumb"];
				
				// Code to handle upload of large image
				$file_name_original =$_FILES["large_image"]["name"];
				if (trim($file_name_original) != "") {
					// Physically remove old large image
					$file_path = UPLOAD_IMG_GALLERY_PATH."/".$file_name_updated_stored;
					unlink($file_path);
					
					// Upload new large image
					$file_name_original =$_FILES["large_image"]["name"];
					$Ext = strchr($file_name_original,".");
					$Ext = strtolower($Ext);
				
					if ($Ext==".jpg" || $Ext==".jpeg" || $Ext==".gif" || $Ext==".bmp" || $Ext==".png") {
						$file_name_updated_ = getMD5_ID();
						$file_name_updated =$file_name_updated_.$Ext;
						$file_name_original_temp =$_FILES["large_image"]["tmp_name"];
						
						// Upload Large Image
						$copy = file_copy("$file_name_original_temp", UPLOAD_IMG_GALLERY_PATH."/", "$file_name_updated", 1, 1);
					}
					$qry = "UPDATE ".TBL_IMAGE_GALLERY." SET file_name_updated='".$file_name_updated."' WHERE tbl_image_gallery_id='$tbl_image_gallery_id'";
					update($qry);
				}
				
				// Code to handle upload of Thumb Image				
				$thumb_name_original =$_FILES["thumb_image"]["name"];
				if (trim($thumb_name_original) != "") {
					// Physically remove old Thumb Image
					$file_path = UPLOAD_IMG_GALLERY_PATH."/".$file_name_updated_thumb_stored;
					unlink($file_path);
					
					$Ext = strchr($thumb_name_original,".");
					$Ext = strtolower($Ext);
					$file_name_updated_thumb =$file_name_updated_."_thumb".$Ext;
					$file_name_original_thumb_temp =$_FILES["thumb_image"]["tmp_name"];
					
					// Upload Thumb Image
					$copy = file_copy("$file_name_original_thumb_temp", UPLOAD_IMG_GALLERY_PATH."/", "$file_name_updated_thumb", 1, 1);
					
					$qry = "UPDATE ".TBL_IMAGE_GALLERY." SET file_name_updated_thumb='".$file_name_updated_thumb."' WHERE tbl_image_gallery_id='$tbl_image_gallery_id'";
					update($qry);
				}
				
				// If only large image was uploaded then create auto thumnanil in this case
				if (trim($file_name_original) != "" && trim($thumb_name_original) == "") {
					$file_name_updated_thumb =$file_name_updated_."_thumb".$Ext;
					$source = UPLOAD_IMG_GALLERY_PATH."/".$file_name_updated;
					$destination = UPLOAD_IMG_GALLERY_PATH."/".$file_name_updated_thumb;
					generate_thumb($source, $destination, $max_x, $max_y);
					
					$qry = "UPDATE ".TBL_IMAGE_GALLERY." SET file_name_updated_thumb='".$file_name_updated_thumb."' WHERE tbl_image_gallery_id='$tbl_image_gallery_id'";
					update($qry);
				}

				$qry = "UPDATE ".TBL_IMAGE_GALLERY." SET tbl_gallery_category_id='$tbl_gallery_category_id', image_text_en='".$image_text_en."', image_text_ar='".$image_text_ar."', image_text_ur='".$image_text_ur."', is_active='$is_active' WHERE tbl_image_gallery_id='$tbl_image_gallery_id'";
				update($qry);
				//echo $qry;
		$MSG = "Information has been updated successfully!";
		$mid = 1;
	}
	
	/*	ACTIVATE/INACTIVATE RECORD	*/
	if($ssubmit=="recordActiveInactive") {
		if(isset($show)){
				for ($j=0; $j<count($EditBox); $j++){ 
					$Querry = "UPDATE ". TBL_IMAGE_GALLERY ." SET is_active='Y' WHERE tbl_image_gallery_id='$EditBox[$j]'";			
					//echo $Querry;
					update($Querry);
					$mid=1;	
					$MSG = "Selected record(s) have been activated successfully.";
				}
		}
		if(isset($hide)){
				for ($j=0; $j<count($EditBox); $j++){ 
					$Querry = "UPDATE ". TBL_IMAGE_GALLERY ." SET is_active='N' WHERE tbl_image_gallery_id='$EditBox[$j]'";			
					update($Querry);
					//echo $Querry;
					$mid=1;	
					$MSG = "Selected record(s) have been deactivated successfully.";
				}
			}
		if(isset($delete)){$mid = 4;}
	}
	
	/* DELETE RECORD */
	if($ssubmit == "recordDelInformation"){
		for($i=0;$i<count($EditBox);$i++){
			$qry = "SELECT * FROM ".TBL_IMAGE_GALLERY." WHERE `tbl_image_gallery_id` = '$EditBox[$i]'";
			//echo $qry."<br>";
			$data = selectFrom($qry);
			$file_name_updated_stored =$data["file_name_updated"];
			$file_name_updated_thumb_stored =$data["file_name_updated_thumb"];
			
			// Physically remove old Thumb Image
			if (trim($file_name_updated_stored) != "") {
				$file_path = UPLOAD_IMG_GALLERY_PATH."/".$file_name_updated_stored;
				unlink($file_path);
			}
			
			// Physically remove old Thumb Image
			if (trim($file_name_updated_thumb_stored) != "") {
				$file_path = UPLOAD_IMG_GALLERY_PATH."/".$file_name_updated_thumb_stored;
				unlink($file_path);
			}
			deleteFrom("DELETE FROM ".TBL_IMAGE_GALLERY." WHERE `tbl_image_gallery_id` = '$EditBox[$i]'");
			$mid = 1;
			$MSG = "Selected record(s) has been deleted successfully.";
		}
	}

	if ($by == "") {
		$by = "ASC";
	}
	
?>
<link href="<?=HOST_URL?>/admin/css/interface.css" type=text/css rel=stylesheet>
<style type="text/css">
	select {
		width:200px;
	}

	.col_head {
		color:#005B90;
	}
	
	.input_text {
		border:1px solid #CCC;
		width: 160px;
		height:22px;
	}
</style>
<script  src="<?=JS_PATH?>/jquery-1.3.2.min.js"></script>
<script language="JavaScript">
	
	$(document).ready(function(){
	});
	
	/* CUSTOM JS - MODIFY AS PER NEED */
	function valueCheckedForUsersManagement(){
		var ml = document.frmDirectory;
		var len = ml.elements.length;
		for (var i = 0; i < len; i++){
			if (document.frmDirectory.elements[i].checked){
				return true;
			}
		}
		 alert ("Select at least one record.");
		 return false;
	}
	
	function CheckAll(){
		var ml = document.frmDirectory;
		var len = ml.elements.length;
		if (document.frmDirectory.AC.checked==true) {
			 for (var i = 0; i < len; i++) {
				document.frmDirectory.elements[i].checked=true;
			 }
		} else {
			  for (var i = 0; i < len; i++)  {
				document.frmDirectory.elements[i].checked=false;
			  }
		}
	}
	
	function UnCheckAll() {
		var ml = document.frmDirectory;
		var len = ml.elements.length;
		var count=0; var checked=0;
			for (var i = 0; i < len; i++) {	       
				if ((document.frmDirectory.elements[i].type=='checkbox') && (document.frmDirectory.elements[i].name != "AC")) {
					count = count + 1;
					if (document.frmDirectory.elements[i].checked == true){
						checked = checked + 1;
					}
				}
			 }
			 
		if (checked == count) {
			 document.frmDirectory.AC.checked = true;
		} else {
			document.frmDirectory.AC.checked = false;
		}
	}
	
	function validateForm() {
		//return true;
		if(isCategory()==false || validate_format()==false) {
				return false;
		} else {
			return true; 
		}
	}
	
	function validate_format() {
		var i, str;
				
		if ($("#large_image1").val() == "" && $("#large_image2").val() == "" && $("#large_image3").val() == "" && $("#large_image4").val() == "" && $("#large_image5").val() == "") {
			alert("Please provide at least one image to proceed.");
			return false;
		}
		
		if ($("#large_image1").val() == ""  && $("#thumb_image1").val() != "") {
			alert("Please provide Large Image for Image 1");
			return false;
		}
		
		if ($("#large_image2").val() == ""  && $("#thumb_image2").val() != "") {
			alert("Please provide Large Image for Image 2");
			return false;
		}
		
		if ($("#large_image3").val() == ""  && $("#thumb_image3").val() != "") {
			alert("Please provide Large Image for Image 3");
			return false;
		}
		
		if ($("#large_image4").val() == ""  && $("#thumb_image4").val() != "") {
			alert("Please provide Large Image for Image 4");
			return false;
		}
		
		if ($("#large_image5").val() == ""  && $("#thumb_image5").val() != "") {
			alert("Please provide Large Image for Image 5");
			return false;
		}
		
		for (i=1; i<=5; i++ ) {
			if ($("#large_image"+i).val() == "") {continue;}
			str = get_extension($("#large_image"+i).val().toLowerCase());
			if (str == "jpg" || str == "jpeg" || str == "png") {} else {
				alert("\nYou are only allowed to upload 'jpg' or 'png' image file formats for Large Image "+i);
				return false;
			}
		}
		
		for (i=1; i<=5; i++ ) {
			if ($("#thumb_image"+i).val() == "") {continue;}
			str = get_extension($("#thumb_image"+i).val().toLowerCase());
			if (str == "jpg" || str == "jpeg" || str == "png") {} else {
				alert("\nYou are only allowed to upload 'jpg' or 'png' image file formats for Thumb Image "+i);
				return false;
			}
		}
		
		//alert(large_image)
		//alert();
		return true;
		
		
	}
	
	function get_extension(file_name) {
		if (file_name == "") {return true;}
		var arr = file_name.split(".");
		return arr[1];
	}
	
	function isCategory() {
		var str = document.frmRecord.tbl_gallery_category_id.value;
		if (str == "") {
			alert("\n Please select Category from the list.");
			$("#tbl_gallery_category_id").focus();
			return false;
		}
		return true;
	}
	
	function isLastName() {
		var str = document.frmRecord.file_name_updated.value;
		if (str == "") {
			alert("\nThe Last name field is blank. Please write your Last name.");
			document.frmRecord.file_name_updated.value="";
			document.frmRecord.file_name_updated.focus();
			return false;
		}
		if (!isNaN(str)) {
			alert("\nPlease write your Last name.");
			document.frmRecord.file_name_updated.value="";
			document.frmRecord.file_name_updated.select();
			document.frmRecord.file_name_updated.focus();
			return false;
		}
		for (var i = 0; i < str.length; i++) {
			var ch = str.substring(i, i + 1);
			if  ((ch <"A" || ch > "z" ) && (ch !=" ")){
				alert("\n Please enter valid Last name.");
				document.frmRecord.file_name_updated.select();
				document.frmRecord.file_name_updated.focus();
				return false;
			}
		}
		return true;
	}
	
	
	function isUserType() {
		var str = document.frmRecord.user_type.value;
		if (str == "") {
			alert("\nPlease select user type.");
			document.frmRecord.user_type.focus();
			return false;
		}
		return true;
	}
	
	function isEmail() {
		var str = document.frmRecord.email.value;
		if (str == "") {
			alert("\nThe Email field is blank.Please enter your valid Email.");
			document.frmRecord.email.focus();
			return false;
		}
		if (!isNaN(str)) {
			alert("\nPlease write your correct Email address");
			document.frmRecord.email.select();
			document.frmRecord.email.focus();
			return false;
		}
		if(str.indexOf('@', 0) == -1) {
			alert("\nIt seems that your email address is not valid.");
			document.frmRecord.email.select();
			document.frmRecord.email.focus();
			return false;
		}
		return true;
	}
	
	function isUserID() {
		var str = document.frmRecord.tbl_image_gallery_id.value;
		if ( str=="" ) {
			alert("\nThe User-ID is blank. Please write your User-ID.");
			document.frmRecord.tbl_image_gallery_id.focus();
			return false;
		}
		if (str.length < 7 ) {
			alert("\nThe User-ID should be greater than 5 Characters.");
			document.frmRecord.tbl_image_gallery_id.focus();
			document.frmRecord.tbl_image_gallery_id.select();
			return false;
		}
			
		if (!isNaN(str)) {
			alert("\nThe User-ID have only letters & digits, Please re-enter your User-ID");
			document.frmRecord.tbl_image_gallery_id.select();
			document.frmRecord.tbl_image_gallery_id.focus();
			return false;
		}
	
		for (var i = 0; i < str.length; i++) {
			var ch = str.substring(i, i + 1);
			if  ((ch < "a" || ch > "z") && (ch < "0" || "9" < ch) ) {
				alert("\nThe User-ID have only letters in lower case & digits, Please re-enter your User-ID");
				document.frmRecord.tbl_image_gallery_id.select();
				document.frmRecord.tbl_image_gallery_id.focus();
				return false;
			}
		}
		return true;
	}
	
	function isPassword() {
		var str = document.frmRecord.password.value;
		if (str == "") {
			alert("\nThe Password field is blank. Please enter Password.");
			document.frmRecord.password.focus();
			return false;
		}
			if (str.length < 7) {
			alert("\nThe Password should be greater than 6 Characters.");
			document.frmRecord.password.focus();
			document.frmRecord.password.select();
			return false;
		}
		return true;
	}
	
	function isRetypePassword() {
		var str = document.frmRecord.confirm_password.value;
		if (str == "") {
			alert("\nThe Confirm Password field is blank. Please retype password.");
			document.frmRecord.confirm_password.focus();
			return false;
		}
		return true;
	}
	
	function isPasswordSame() {
		var str1 = document.frmRecord.password.value;
		var str2 = document.frmRecord.confirm_password.value;
		if (str1 != str2) {
			alert("\nPassword mismatch, Please retype same passwords in both fields.");
			document.frmRecord.confirm_password.focus();
			return false;
		}
		return true;
	}
</script>
<?php if (!$offset || $offset<0)  { $offset =0;} ?>
<?php if (!$LIKE)  { $LIKE = "LIKE";} ?>
<!--	DEFAULT FIRST SCREEN	-->
<?php if($mid=="1"){ ?>
<table width="100%" height="570" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr>
    <td height="30" valign="center" class="adminDetailHeading">&nbsp;Image Gallery Management </td>
  </tr>
  
  <tr align="center" >
    <td height="538" align="center" valign="top" >
    <br>
    <form name="frmRecord" id="frmRecord" method="post" action=""  onSubmit="return validateForm();" enctype="multipart/form-data">
        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >
          
			  <?php if ($MSG != "") {	?>
                <tr height="22" align="center" valign="top" > 
                      <td height="30" colspan="4" align="center" valign="middle">
                            <span class="msgColor"><?php echo $MSG;?></span>
                      </td>
                </tr>        
            <?php		}	?> 
          
            <tr height="22" align="center" valign="top" > 
                  <td colspan="4" align="right" height="22"><a href="<?=HOST_URL?>/admin/gallery_management/gallery_images_order_management.php?mid=1&sid=<?=$sid?>&tab=user&subtab=gallery" style="color:#1d7543; text-decoration:underline;">Arrange Gallery Images</a>&nbsp;|&nbsp;
                  <a href="<?=HOST_URL?>/admin/gallery_management/gallery_category_management.php?mid=1&sid=<?=$sid?>&tab=user&subtab=gallery" style="color:#1d7543; text-decoration:underline;">Category Management</a>&nbsp;|&nbsp;
                   <a href="<?=HOST_URL?>/admin/gallery_management/gallery_category_order_management.php?mid=1&sid=<?=$sid?>&tab=user&subtab=gallery" style="color:#1d7543; text-decoration:underline;">Arrange Categories</a></td>
           </tr>
          <tr>
            <td height="22" align="left" valign="middle"  class="adminDetailHeading" ><strong>&nbsp;Add Images</strong></td>
          </tr>
          <tr >
            <td align="center" valign="top" class="bordLightBlue"><table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" >
                <tr align="left" valign="top" >
                  <td colspan="5"><span class="msgColor">Fields with * are mandatory.</span></td>
                </tr>
                <tr valign="middle" >
                  <td align="right">&nbsp;</td>
                  <td align="left" colspan="4">                
                </tr>
                <tr valign="middle" >
                  <td align="right">Category<span class="msgColor">*</span>:</td>
                  <td align="left" colspan="4">
                  	<?php	
						$qry = "SELECT * FROM ".TBL_GALLERY_CATEGORY." WHERE is_active='Y' and tbl_school_id='$tbl_school_id' ORDER BY category_name_en ASC, category_name_ar ASC";
						$data_rs = SelectMultiRecords($qry);
					?>
                    <select name="tbl_gallery_category_id" id="tbl_gallery_category_id" class="flat">
                       <option value="">Select Category</option>
                    	<?php	for ($i=0; $i<count($data_rs); $i++) {	
								$tbl_gallery_category_id = $data_rs[$i]["tbl_gallery_category_id"];
						
						?>
                    		<option value="<?=$tbl_gallery_category_id?>"><?=$data_rs[$i]["category_name_en"]?></option>
                        <?php	}	?>
                    </select>
                 
                </tr>
                <tr valign="middle" >
                  <td align="right">&nbsp;</td>
                  <td align="left" colspan="4">                
                </tr>
                <tr valign="middle" >
                  <td align="left" valign="top"><span class="msgColor">Note:</span></td>
                  <td colspan="4" align="left" valign="top"><span class="msgColor">* You can upload 5 images at one time.<br />
* Thumb Image is auto generated if not provided.<br />
* Once image is uploaded, scroll down to view it in listing. </span></tr>
                <tr valign="middle" >
                  <td width="8%" align="right" height="30">&nbsp;</td>
                  <td width="18%" align="center"><strong class="col_head">Large Image
                  </strong>(580 x 580)</td>
                  <td width="18%" align="center"><strong class="col_head">Thumb Image</strong>
                  (150 x 150)</td>
                  <td width="15%" align="center"><strong class="col_head">Image Caption [En]</strong></td>
                  <td width="15%" align="center"><strong class="col_head">Image Caption [Ar]</strong></td>
                </tr>
                <tr valign="middle" >
                  <td height="20" align="right"><strong>Image 1:</strong></td>
                  <td align="center" valign="middle"><input type="file" name="large_image[]" class="flat" id="large_image1"></td>
                  <td align="center" valign="middle"><input type="file" name="thumb_image[]" class="flat" id="thumb_image1"></td>
                  <td align="center" valign="middle"><input type="text" name="image_text_en[]" id="image_text" value="" class="input_text"></td>
                  <td align="center"><input type="text" name="image_text_ar[]" id="image_text" value="" class="input_text input_text_ar"></td>
                 
                </tr>
                <tr>
                 <td height="20" align="right">Status:</td>
                 <td align="left" colspan="4"><input name="is_active1" type="radio" value="Y" checked>
                    Yes
                    <input type="radio" name="is_active1" value="N">
                    No &nbsp;</td>
                </tr>
                
                 <tr>
                 <td height="26" align="right" colspan="5">&nbsp;</td>
                </tr>
                
                <tr valign="middle" >
                  <td height="26" align="right"><strong>Image 2:</strong></td>
                  <td align="center" valign="middle"><input type="file" name="large_image[]" class="flat" id="large_image2"></td>
                  <td align="center" valign="middle"><input type="file" name="thumb_image[]" class="flat" id="thumb_image2"></td>
                  <td align="center" valign="middle"><input type="text" name="image_text_en[]" id="image_text" value="" class="input_text"></td>
                  <td align="center"><input type="text" name="image_text_ar[]" id="image_text" value="" class="input_text input_text_ar"></td>
                </tr>
                
                <tr>
                 <td height="20" align="right"><strong>Status:</strong></td>
                 <td align="left" colspan="4"><input name="is_active2" type="radio" value="Y" checked>
                    Yes
                    <input type="radio" name="is_active2" value="N">
                    No &nbsp;</td>
                </tr>
                 
                 <tr>
                 <td height="26" align="right" colspan="5">&nbsp;</td>
                </tr>
                
                
                <tr valign="middle" >
                  <td height="26" align="right"><strong>Image 3:</strong></td>
                  <td align="center" valign="middle"><input type="file" name="large_image[]"class="flat" id="large_image3"></td>
                  <td align="center" valign="middle"><input type="file" name="thumb_image[]" class="flat" id="thumb_image3"></td>
                  <td align="center" valign="middle"><input type="text" name="image_text_en[]" id="image_text" value="" class="input_text"></td>
                  <td align="center"><input type="text" name="image_text_ar[]" id="image_text" value="" class="input_text input_text_ar"></td>
                </tr>
                
                <tr>
                 <td height="20" align="right">Status:</td>
                 <td align="left" colspan="4"><input name="is_active3" type="radio" value="Y" checked>
                    Yes
                    <input type="radio" name="is_active3" value="N">
                    No &nbsp;</td>
                </tr>
                 
                <tr>
                 <td height="26" align="right" colspan="5">&nbsp;</td>
                </tr>
                
                
                <tr valign="middle" >
                  <td height="26" align="right"><strong>Image 4:</strong></td>
                  <td align="center" valign="middle"><input type="file" name="large_image[]" class="flat" id="large_image4"></td>
                  <td align="center" valign="middle"><input type="file" name="thumb_image[]" class="flat" id="thumb_image4"></td>
                  <td align="center" valign="middle"><input type="text" name="image_text_en[]" id="image_text" value="" class="input_text"></td>
                  <td align="center"><input type="text" name="image_text_ar[]" id="image_text" value="" class="input_text input_text_ar"></td>
                </tr>
                
                <tr>
                 <td height="20" align="right">Status:</td>
                 <td align="left" colspan="4"><input name="is_active4" type="radio" value="Y" checked>
                    Yes
                    <input type="radio" name="is_active4" value="N">
                    No &nbsp;</td>
                </tr>
                <tr>
                 <td height="26" align="right" colspan="5">&nbsp;</td>
                </tr>
                
                <tr valign="middle" >
                  <td height="26" align="right"><strong>Image 5:</strong></td>
                  <td align="center" valign="middle"><input type="file" name="large_image[]" class="flat" id="large_image5"></td>
                  <td align="center" valign="middle"><input type="file" name="thumb_image[]" class="flat" id="thumb_image5"></td>
                  <td align="center" valign="middle"><input type="text" name="image_text_en[]" id="image_text" value="" class="input_text"></td>
                  <td align="center"><input type="text" name="image_text_ar[]" id="image_text" value="" class="input_text input_text_ar"></td>
                </tr>
                
                <tr>
                 <td height="20" align="right">Status:</td>
                 <td align="left" colspan="4"><input name="is_active5" type="radio" value="Y" checked>
                    Yes
                    <input type="radio" name="is_active5" value="N">
                    No &nbsp;</td>
                </tr>
                <tr>
                 <td height="26" align="right" colspan="5">&nbsp;</td>
                </tr>
                </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="center" valign="middle">
                    <input type="hidden" name="tbl_image_gallery_id" value="<?=$tbl_image_gallery_id?>">
                    <input type="reset" name="Reset" value="Reset" class="flat">
                    <input name="save" type="submit" class="flat" id="ssubmit"  value="Submit">
                    <input name="ssubmit" type="hidden" id="ssubmit" value="insert_images">
                    <input name="sid" type="hidden" value="<?=$sid?>">
                    <input name="mid" type="hidden" value="1">
                    <input name="offset" type="hidden" id="offset" value="<?=$offset;?>">
                    <input name="q" type="hidden" id="q" value="<?php echo htmlspecialchars($q);?>">
                    <input name="by" type="hidden" id="by" value="<?=$by?>">
                    <input name="field" type="hidden" id="fiel3" value="<?=$field?>">
                    <input name="LIKE" type="hidden" value="<?=$LIKE?>">
                    <input name="tab" type="hidden" value="events">
            		<input name="subtab" type="hidden"  value="gallery">
                  </td>
                </tr>
                <tr>
                  <td align="center" valign="middle">&nbsp;</td>
                </tr>
            </table></td>
          </tr>
        </table>
        <br>
      </form>
    
    <form name="form1" method="post" action="">
        <br>
        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >
          <tr > 
            <td height="22" align="left" valign="middle" class="adminDetailHeading">&nbsp;Search 
              Criteria</td>
          </tr>
          <tr > 
            <td align="center" valign="top" class="bordLightBlue">
            <table width="100%" border="0" cellspacing="2" cellpadding="2">
                <tr> 
                  <td width="21%"><span class="searchText">Field</span>:<br /><select name="field" id="field">
                      <option value="image_text_en" <?PHP if ($field == "image_text"){echo "selected";}?>>Image Caption [En]</option>
                      <option value="category_name_en" <?PHP if ($field == "category_name_en"){echo "selected";}?>>Category Name [En] </option>
                      
                      <option value="is_active"<?PHP if($field == "is_active"){echo "selected";}?>>Status 
                      e.g Y=Activate , N=Deactivate</option>
                    </select>
                    </td>
                  <td width="17%"><span class="searchText">Condition:</span><br /><?php
							if (!$LIKE) {
								$LIKE = "LIKE";
							}
						?>
                    <select name="LIKE" id="LIKE" class="flat">
                      <option value="LIKE" <?PHP if ($LIKE == "LIKE"){echo "selected";}?>>LIKE</option>
                      <option value="=" <?PHP if (($LIKE == "=") || ($LIKE == "")){echo "selected";}?>>Equal 
                      To</option>
                      <option value="!=" <?PHP if ($LIKE == "!="){echo "selected";}?>>Not 
                      Equal To</option>
                      <option value="<" <?PHP if ($LIKE == "<"){echo "selected";}?>>Less 
                      Than</option>
                      <option value=">" <?PHP if ($LIKE == ">"){echo "selected";}?>>Greater 
                      Than</option>
                      <option value="<=" <?PHP if ($LIKE == "<="){echo "selected";}?>>Less 
                      Than or Equal To</option>
                      <option value=">=" <?PHP if ($LIKE == ">="){echo "selected";}?>>Greater 
                      Than or Equal To</option>
                    </select></td>
                   <td width="18%"><span class="searchText">Search 
                    Name:&nbsp;</span><br /><input name="q" type="text" id="q" value="<?php echo htmlspecialchars($q);?>" size="30" class="flat"></td>
                   <td width="18%"><span class="searchText">Order 
                    By:&nbsp;</span><br /><select name="by" id="by" class="flat">
                      <option value="ASC" <?PHP if (($by == "ASC") || ($by == "")) { echo "selected";}?>>ASCENDING</option>
                      <option value="DESC" <?PHP if ($by == "DESC"){ echo "selected";}?>>DESCENDING</option>
                    </select>
                        </td>
                   <td width="34%" valign="bottom" style="padding-top:30px;">
                     <input name="mid" type="hidden" value="1">
                    <input name="offset" type="hidden" value="0">
                    <input name="Search" type="submit" id="Search" value="Search" class="flat">
                    <input name="ssubmit" type="hidden" id="ssubmit" value="" class="flat">&nbsp;
                  </td>
                  
                </tr>
              </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td height="30" align="center" valign="top"><a href="<?=HOST_URL?>/admin/gallery_management/gallery_images_management.php?mid=<?=$mid?>&amp;sid=<?=$sid?>&amp;tab=user&amp;subtab=gallery"><span class="detailLinkColor">All 
                  Records</span></a></td>
              </tr>
            </table></td>
          </tr>
        </table>
      </form>
    
      <br>
      <span class="msgColor"><?php echo $MSG;?></span><br>
      <?php 		
		$q = addslashes($q);
		$CountRec = "SELECT * FROM ".TBL_IMAGE_GALLERY." WHERE tbl_school_id='$tbl_school_id' ";		
		$Query = "SELECT * FROM ".TBL_IMAGE_GALLERY." WHERE tbl_school_id='$tbl_school_id' ";
		//echo $Query;
		if ($field == "category_name_en") {
			$CountRec .= " AND tbl_gallery_category_id IN (SELECT tbl_gallery_category_id FROM ".TBL_GALLERY_CATEGORY." WHERE category_name_en LIKE'%".$q."%' AND tbl_school_id='$tbl_school_id')";
			$Query .=  " AND tbl_gallery_category_id IN (SELECT tbl_gallery_category_id FROM ".TBL_GALLERY_CATEGORY." WHERE category_name_en LIKE'%".$q."%' AND tbl_school_id='$tbl_school_id')";
		} else {
			if($field && !empty($q)){	    	
				if($LIKE=="LIKE"){
						$CountRec .= " AND $field $LIKE '%$q%' ";
						$Query .= " AND  $field $LIKE '%$q%' ";
				}else{
						$CountRec .= " AND  $field $LIKE '$q' ";	
						$Query .= " AND  $field $LIKE '$q' ";
				}
			}	
		}
		$Query .= " ORDER BY image_order $by, id desc ";
		$Query .=" LIMIT $offset, ".TBL_IMAGE_GALLERY_PAGING;
		$q = stripslashes($q);

		//echo $Query;

		$total_record = CountRecords($CountRec);
		$data = SelectMultiRecords($Query);
		  		 if ($total_record =="")
					   echo '<span class="msgColor">'."No records found."."</span>";
					else{	
					    echo '<span class="msgColor">';
					    echo " <b> ". $total_record ." </b> Record(s) found. Showing <b>";
						if ($total_record>$offset){
							echo $offset+1;
							echo " </b>to<b> ";
							if ($offset >=$total_record - TBL_IMAGE_GALLERY_PAGING)	{ 
								  echo $total_record; 
							}else { 
							   	echo $offset + TBL_IMAGE_GALLERY_PAGING ;
							}
						}else{ 
							echo $offset+1;
							echo "</b> - ". $total_record;
							echo " to ". $total_record ." Record(s) ";		
						}
						echo "</b>.</span>";	
					}
	   ?>
      <?php  if ($total_record !=""){?>
      <table width="98%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="18"><span class="msgColor">&nbsp;&nbsp;Note: To view detail 
            click on Image Caption.</span></td>
        </tr>
        <tr>
          <td align="center" valign="top" class="adminDetailHeading"><table width="100%" border="0" align="center"  cellpadding="2" cellspacing="1">
              <form name="frmDirectory" onSubmit="return valueCheckedForUsersManagement()">
                <tr align="center" >
                  <td width="20%" align="center" class="adminDetailHeading">Image Caption</td>
                  <td width="20%" align="center" class="adminDetailHeading">Category</td>
                  <td align="center" class="adminDetailHeading">Thumb Image</td>
                  <td width="10%" height="24" align="center" class="adminDetailHeading">Date</td>
                  <td width="8%" align="center" class="adminDetailHeading">Status&nbsp;</td>
                  <td width="6%" align="center" class="adminDetailHeading">Action</td>
                  <td width="6%" height="24" align="center" class="adminDetailHeading"><input name="AC" type="checkbox" onClick="CheckAll();"></td>
                </tr>
                <?php
				 for($i=0;$i<count($data);$i++){
						$id =$data[$i]["id"];
						$tbl_image_gallery_id =$data[$i]["tbl_image_gallery_id"];
						$tbl_gallery_category_id =$data[$i]["tbl_gallery_category_id"];
						$file_name_original =$data[$i]["file_name_original"];
						$file_name_updated =$data[$i]["file_name_updated"];
						$file_name_updated_thumb =$data[$i]["file_name_updated_thumb"];
						$image_text_en =$data[$i]["image_text_en"];
						$image_text_ar =$data[$i]["image_text_ar"];
						$image_text_ur =$data[$i]["image_text_ur"];
						$image_order =$data[$i]["image_order"];
						$added_date =$data[$i]["added_date"];				
						$is_active =$data[$i]["is_active"];
						
						$qry = "SELECT * FROM ".TBL_GALLERY_CATEGORY." WHERE tbl_gallery_category_id='".$tbl_gallery_category_id."' and tbl_school_id='".$tbl_school_id."'";
						
						$data_cat = selectFrom($qry);
						$category_name_en = $data_cat["category_name_en"];
						$category_name_ar = $data_cat["category_name_ar"];
				?>
                <tr valign="middle" style="background-color:#FFF">
                  <td align="left" >&nbsp;<div style="margin-left:5px"><a href="<?=HOST_URL?>/admin/gallery_management/gallery_images_management.php?mid=2&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&by=<?=$by?>&tbl_image_gallery_id=<?=$tbl_image_gallery_id?>&tab=user&subtab=gallery" title="Click to view detail" class="detailLinkColor"><?=$image_text_en?></a></div>

                  						<div style="padding:5px" class="lan_r_ar"><?php echo$image_text_ar;?></div>
                                        
                  
                  </td>
                  <td align="left" style="margin-left:5px"><div style="padding:5px"><?php echo $category_name_en;?></div>
                        <div style="padding:5px" class="lan_r_ar"><?php echo $category_name_ar;?></div></td>
                  <td height="20" align="center" style="margin-left:5px">&nbsp;
                   <img src="<?=IMG_GALLERY_PATH?>/<?=$file_name_updated_thumb?>" alt="<?=$image_text?>" title="" width="100" height="100"> </td>
                  <td height="20" align="center" ><?php echo date("d M, Y H:i:s",strtotime($added_date));?></td>
                  <td align="center"><?php 
		 			 if($is_active == 'Y'){?>
                    <img src="<?=IMG_PATH?>/yes.gif">
                    <?php } else {?>
                    <img src="<?=IMG_PATH?>/no.gif">
                    <?php }  ?></td>
                  <td align="center"><a href="?mid=3&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&by=<?=$by?>&tbl_image_gallery_id=<?=$tbl_image_gallery_id?>&tab=user&subtab=gallery" class="detailLinkColor">Edit</a></td>
                  <td align="center"><input name="EditBox[]" type="checkbox" value="<?=$tbl_image_gallery_id?>"></td>
                </tr>
                <?php }?>
                <tr>
                  <td height="20" colspan="8" align="center" valign="middle" bgcolor="#FFFFFF"><input name="reset" type="reset" class="flat"  value="Reset">
                    <input name="show" type="submit" class="flat" id="ssubmit" value="Active">
                    <input name="hide" type="submit" class="flat"  value="Deactive">
                    <input name="delete" type="submit" class="flat"  value="Delete">
                    <input name="ssubmit" type="hidden" id="ssubmit" value="recordActiveInactive">
                    <input name="mid" type="hidden" id="mid2" value="1">
                    <input name="sid" type="hidden" id="sid" value="<?=$sid?>">
                    <input name="offset" type="hidden" id="offset" value="<?=$offset?>">
                    <input name="q" type="hidden" id="q" value="<? echo htmlspecialchars($q);?>">
                    <input name="tbl_image_gallery_id" type="hidden" value="<?=$tbl_image_gallery_id?>">
                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>">
                    <input name="by" type="hidden" id="by" value="<?=$by?>">
                    <input name="field" type="hidden" id="field" value="<?=$field?>">
                    <input name="LIKE" type="hidden" value="<?=$LIKE?>">
                    <input name="tab" type="hidden" value="events">
            		<input name="subtab" type="hidden"  value="gallery">
                    </td>
                </tr>
                <tr align="right">
                  <td height="20" colspan="8" valign="middle" bgcolor="#FFFFFF"><?php 
					   if ($total_record != "" && $total_record>TBL_IMAGE_GALLERY_PAGING) {
							$url = "gallery_images_management.php?mid=1&sid=$sid&field=$field&q=$q&LIKE=$LIKE&by=$by";
							$Paging_object = new Paging();
							$Paging_object->paging_new_style_final($url,$offset,$clicked_link,$total_record,TBL_IMAGE_GALLERY_PAGING);
						}
					?></td>
                </tr>
              </form>
            </table></td>
        </tr>
      </table>
      <br>
      <?php } ?>
      <br>
      <br>
      </td>
  </tr>
</table>
<!--	RECORD DETAILS SCREEN	-->
<?php } if($mid=="2") { 
		$qry = "SELECT * FROM ".TBL_IMAGE_GALLERY." WHERE tbl_image_gallery_id='$tbl_image_gallery_id'";
		//echo $qry;
		$data = selectFrom($qry);
		$tbl_image_gallery_id =$data["tbl_image_gallery_id"];
		$tbl_gallery_category_id =$data["tbl_gallery_category_id"];
		$file_name_original =$data["file_name_original"];
		$file_name_updated =$data["file_name_updated"];
		$file_name_updated_thumb =$data["file_name_updated_thumb"];
		$image_text_en =$data["image_text_en"];
		$image_text_ar =$data["image_text_ar"];
		$image_text_ur =$data["image_text_ur"];
		$image_order =$data["image_order"];	
		$added_date =$data["added_date"];				
		$is_active =$data["is_active"];
		
		$qry = "SELECT * FROM ".TBL_GALLERY_CATEGORY." WHERE tbl_gallery_category_id='".$tbl_gallery_category_id."'";
		$data_cat = selectFrom($qry);
		$category_name_en = $data_cat["category_name_en"];
		$category_name_ar = $data_cat["category_name_ar"];
		$category_name_ur = $data_cat["category_name_ur"];
?>
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="1" >
  <tr >
    <td height="22" align="left" valign="middle"  class="adminDetailHeading" > &nbsp;Image Details</td>
  </tr>
  <tr >
    <td align="center" valign="top" class="bordLightBlue"><form name="frmRegister" id="frmRegister" method="post" action=""  onSubmit="return validateForm();">
        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >
          
          <tr >
            <td align="center" valign="top"><table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" >
                <tr align="center" valign="top" >
                  <td colspan="2"></td>
                </tr>
                <tr valign="middle" >
                  <td align="right">&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr valign="middle" >
                  <td align="right">Category[En]:</td>
                  <td><?php echo $category_name_en; ?></td>
                </tr>
                <tr valign="middle" >
                  <td align="right">Category [Ar]:</td>
                  <td class="lan_l_ar"><?php echo $category_name_ar; ?></td>
                </tr>
                <tr valign="middle" >
                  <td width="20%" align="right">Large Image: </td>
                  <td width="78%"><span style="margin-left:5px"><img src="<?=IMG_GALLERY_PATH?>/<?=$file_name_updated?>" alt="<?=$image_text?>" title="" width="300" height="300"></span></td>
                </tr>
                <tr valign="middle" >
                  <td align="right">Thumb Image: </td>
                  <td align="left" valign="top"><span style="margin-left:5px"><img src="<?=IMG_GALLERY_PATH?>/<?=$file_name_updated_thumb?>" alt="<?=$image_text?>" title="" width="100" height="100"></span></td>
                </tr>
                <tr valign="middle" >
                  <td align="right"> Image Caption [En]: </td>
                  <td align="left" valign="top"><?php echo $image_text_en; ?></td>
                </tr>
                <tr valign="middle" >
                  <td align="right"> Image Caption [Ar]: </td>
                  <td align="left" valign="top" class="lan_l_ar"><?php echo $image_text_ar; ?></td>
                </tr>
                <tr valign="middle" >
                  <td align="right">Status:&nbsp;</td>
                  <td align="left" valign="top"><?php echo $is_active; ?></td>
                </tr>
                <tr valign="middle" >
                  <td align="right">&nbsp;</td>
                  <td align="left" valign="top">&nbsp;</td>
                </tr>
                <tr valign="middle" >
                  <td colspan="2" align="center" height="10"><a href="gallery_images_management.php?mid=1&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&sort=<?=$sort?>&by=<?=$by?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&tab=user&subtab=gallery" class="detailLinkColor">Back </a>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td align="left" valign="middle">&nbsp;</td>
                </tr>
              </table></td>
          </tr>
        </table>
      </form></td>
  </tr>
</table>
<!--	RECORD EDIT SCREEN	-->
<?php }if($mid=="3"){ 
		
		$qry = "SELECT * FROM ".TBL_IMAGE_GALLERY." WHERE tbl_image_gallery_id='$tbl_image_gallery_id'";
		//echo $qry;
		$data = selectFrom($qry);
		$tbl_image_gallery_id =$data["tbl_image_gallery_id"];
		$tbl_gallery_category_id =$data["tbl_gallery_category_id"];
		$file_name_original =$data["file_name_original"];
		$file_name_updated =$data["file_name_updated"];
		$file_name_updated_thumb =$data["file_name_updated_thumb"];
		$image_text_en =$data["image_text_en"];
		$image_text_ar =$data["image_text_ar"];
		$image_text_ur =$data["image_text_ur"];
		$image_order =$data["image_order"];	
		$added_date =$data["added_date"];				
		$is_active =$data["is_active"];
		
		$qry = "SELECT * FROM ".TBL_GALLERY_CATEGORY." WHERE tbl_gallery_category_id='".$tbl_gallery_category_id."'";
		$data_cat = selectFrom($qry);
		$category_name_en = $data_cat["category_name_en"];
		$category_name_ar = $data_cat["category_name_ar"];
		$category_name_ur = $data_cat["category_name_ur"];
?>
<table width="100%" height="451" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr>
    <td height="30" valign="center" class="adminDetailHeading">&nbsp;Image Gallery Management 
    [Edit] </td>
  </tr>
  <tr>
    <td align="center" valign="top"><br>
      <span class="msgColor"><?php echo $MSG;?></span> <br>
      <table width="98%" border="0" align="center" cellpadding="0" cellspacing="1" >
       
        <tr >
          <td align="center" valign="top"><form name="frmRecord" id="frmRecord" method="post" action=""  onSubmit="return validateForm();" enctype="multipart/form-data">
              <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >
                <tr >
                  <td height="22" align="left" valign="middle"  class="adminDetailHeading" ><strong>&nbsp;Edit Image Details</strong></td>
                </tr>
                <tr >
                  <td align="center" valign="top" class="bordLightBlue" style="border-bottom:1px solid #FFFFFF"><table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" >
                      <tr align="center" valign="top" >
                        <td colspan="2"></td>
                      </tr>
                      <tr align="left" valign="top" >
                        <td colspan="2"><span class="msgColor">Fields with * are 
                          mandatory.</span></td>
                      </tr>
                      <tr valign="middle" >
                        <td align="right">&nbsp;</td>
                        <td align="left" valign="top">&nbsp;</td>
                      </tr>
                      <tr valign="middle" >
                        <td align="right">Category:</td>
                        <td align="left"><?php	
						$qry = "SELECT * FROM ".TBL_GALLERY_CATEGORY." WHERE is_active='Y' and tbl_school_id='$tbl_school_id' ORDER BY gallery_category_order ASC";
						$data_rs = SelectMultiRecords($qry);
						?>
                          <select name="tbl_gallery_category_id" id="tbl_gallery_category_id" class="flat">
                             <option  value="">Select Category</option>
                            <?php	for ($i=0; $i<count($data_rs); $i++) {	
								$tbl_gallery_category_idd = $data_rs[$i]["tbl_gallery_category_id"];
						?>
                                <option value="<?=$tbl_gallery_category_idd?>" <?php if ($tbl_gallery_category_idd  == $tbl_gallery_category_id) {echo 'selected';} ?>>
								  <?=$data_rs[$i]["category_name_en"]?>
                                </option>
						<?php	}	?>
                          </select></td>
                      </tr>
                      <tr valign="middle" >
                        <td align="right"> Image Caption [En]: </td>
                        <td align="left" valign="top"><input type="text" name="image_text_en" id="image_text" value="<?=$image_text_en?>" class="flat"></td>
                      </tr>
                      <tr valign="middle" >
                        <td align="right"> Image Caption [Ar]: </td>
                        <td align="left" valign="top"><input type="text" name="image_text_ar" id="image_text" value="<?=$image_text_ar?>" class="flat" dir="rtl"></td>
                      </tr>
                      <tr valign="middle" >
                        <td width="20%" height="6"></td>
                        <td width="78%"></td>
                      </tr>
                      <tr valign="middle" >
                        <td align="right" valign="top">Large Image (580 x 580):</td>
                        <td align="left" valign="top">
                         <?php if ($file_name_updated  != "") { ?>
                            <img style="float:left" src="<?=IMG_GALLERY_PATH?>/<?=$file_name_updated?>" alt="<?=$image_text?>" title="" width="300" height="300">
                            <div style="clear:both"></div>
                            <br>
                          <?php		}	?>
                          <div style="clear:both">
                          <input type="file" name="large_image" class="flat" id="large_image">
                          </div>
        Browse to select different image [.jpg, .png]
                        <div style="height:10px; width:60%; border-bottom:1px dashed #000"></div>
                        </td>
                      </tr>
                      <tr valign="middle" >
                        <td align="right" valign="top">&nbsp;</td>
                        <td align="left" valign="top">&nbsp;</td>
                      </tr>
                      <tr valign="middle" >
                        <td align="right" valign="top">Thumb Image (150 x 150):</td>
                        <td align="left" valign="top"><?php if ($file_name_updated_thumb  != "") { ?>
                          <img style="float:left" src="<?=IMG_GALLERY_PATH?>/<?=$file_name_updated_thumb?>" alt="<?=$image_text?>" title="" height="100" width="100">
                          <div style="clear:both"></div>
                        
                          <br>
                          <?php		}	?>
                          <div style="clear:both">
                            <input type="file" name="thumb_image" class="flat" id="thumb_image">
                          </div>
                          Browse to select different Thumb Image [.jpg, .png]
                          <div style="height:10px; width:60%; border-bottom:1px dashed #000"></div>
                           </td>
                      </tr>
                      <tr valign="middle" >
                        <td align="right">&nbsp;</td>
                        <td align="left" valign="top">&nbsp;</td>
                      </tr>
                      <tr valign="middle" >
                        <td align="right">Status:</td>
                        <td><input type="radio" name="is_active" value="Y" <?php if($is_active == 'Y'){?>checked="checked"<? } ?>>
                          Yes
                          <input type="radio" name="is_active" value="N" <?php if($is_active == 'N'){?>checked="checked"<? } ?>>
                          No </td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td align="left" valign="middle">&nbsp;</td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td align="left" valign="middle"><a href="gallery_images_management.php?mid=1&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&sort=<?=$sort?>&by=<?=$by?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&tab=user&subtab=gallery" class="detailLinkColor">Back</a>&nbsp;&nbsp;
                          <input type="hidden" name="tbl_image_gallery_id" value="<?=$tbl_image_gallery_id?>">
                          <input type="reset" name="Reset" value="Reset" class="flat">
                          <input type="submit" name="Save" value="Save" class="flat">
                          <input name="ssubmit" type="hidden" id="ssubmit" value="editRecord">
                          <input name="sid" type="hidden" value="<?=$sid?>">
                          <input name="mid" type="hidden" value="1">
                          <input name="offset" type="hidden" id="offset" value="<?=$offset;?>">
                          <input name="q" type="hidden" id="q" value="<?php echo htmlspecialchars($q);?>">
                          <input name="sort" type="hidden" id="sort" value="<?=$sort?>">
                          <input name="by" type="hidden" id="by" value="<?=$by?>">
                          <input name="field" type="hidden" id="field" value="<?=$field?>">
                          <input name="LIKE2" type="hidden" value="<?=$LIKE?>">
                          <input name="tab" type="hidden" value="events">
		            		<input name="subtab" type="hidden"  value="gallery">
                          </td>
                      </tr>
                    </table></td>
                </tr>
              </table>
            </form></td>
        </tr>
      </table>
      <p><br>
      </p></td>
  </tr>
</table>
<!--	RECORD DELETE SCREEN	-->
<?php }if($mid=="4"){ ?>
<table width="100%" height="380" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr>
    <td height="30" valign="center" class="adminDetailHeading">&nbsp;Image Gallery Management 
    [Del Confirmation]</td>
  </tr>
  <tr align="center" >
    <td valign="top" ><br>
      <span class="msgColor"> Are you sure you want to delete the selected record(s) </span> <br>
      <table width="98%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center" valign="top" class="adminDetailHeading"><table width="100%" border="0" align="center"  cellpadding="2" cellspacing="1" >
              <form name="frmDirectory" onSubmit="return valueCheckedForUsersManagement();">
                <tr >
                  <td width="20%" align="center" class="adminDetailHeading">Image Caption</td>
                  <td width="20%" align="center" class="adminDetailHeading">Category</td>
                  <td align="center"  class="adminDetailHeading">Thumb Image</td>
                  <td width="10%" align="center"  class="adminDetailHeading">Date</td>
                  <td width="10%" align="center"  class="adminDetailHeading">Status</td>
                  <td width="4%" height="24" align="center"  class="adminDetailHeading"><input name="AC" type="checkbox" id="AC" onClick="CheckAll();" checked></td>
                </tr>
                <?php 
			for($i=0;$i<count($EditBox);$i++){
				$qry ="SELECT * FROM ".TBL_IMAGE_GALLERY." WHERE tbl_image_gallery_id='$EditBox[$i]'";
				$data = selectFrom($qry);
				$tbl_image_gallery_id =$data["tbl_image_gallery_id"];
				$tbl_gallery_category_id =$data["tbl_gallery_category_id"];
				$file_name_original =$data["file_name_original"];
				$file_name_updated =$data["file_name_updated"];
				$file_name_updated_thumb =$data["file_name_updated_thumb"];
				$image_text_en =$data["image_text_en"];
				$image_text_ar =$data["image_text_ar"];
				$image_text_ur =$data["image_text_ur"];
				$image_order =$data["image_order"];	
				$added_date =$data["added_date"];				
				$is_active =$data["is_active"];
				
				$qry = "SELECT * FROM ".TBL_GALLERY_CATEGORY." WHERE tbl_gallery_category_id='".$tbl_gallery_category_id."'";
				$data_cat = selectFrom($qry);
				$category_name_en = $data_cat["category_name_en"];
				$category_name_ar = $data_cat["category_name_ar"];
				$category_name_ur = $data_cat["category_name_ur"];

				?>
                <tr valign="middle" bgcolor="#FFFFFF">
                  <td align="left" >&nbsp;
                    <div style="margin-left:5px">
<?=$image_text_en?>
                    </div>
                    <div style="padding:5px" class="lan_r_ar"><?php echo$image_text_ar;?></div></td>
                  <td align="left" style="margin-left:5px"><div style="padding:5px"><?php echo $category_name_en;?></div>
                    <div style="padding:5px" class="lan_r_ar"><?php echo $category_name_ar;?></div></td>
                  <td align="center"><span style="margin-left:5px">&nbsp;<img src="<?=IMG_GALLERY_PATH?>/<?=$file_name_updated_thumb?>" alt="<?=$image_text?>" title="" width="100" height="100"></span></td>
                  <td height="20" align="center"><?php echo date("d M, Y H:i:s",strtotime($added_date));?></td>
                  <td align="center"><?php 
		  			if($is_active == 'Y'){?>
                    <img src="<?=IMG_PATH?>/yes.gif">
                    <?php } else {?>
                    <img src="<?=IMG_PATH?>/no.gif">
                    <?php }  ?></td>
                  <td height="20" align="center"><input name="EditBox[]" type="checkbox" id="EditBox[]" onClick="UnCheckAll();" value="<?=$tbl_image_gallery_id?>" checked></td>
                </tr>
                <?php }?>
                <tr>
                  <td height="20" colspan="7" align="center" valign="middle" bgcolor="#FFFFFF" ><input name="Button" type="button" class="flat" id="hide" value="I am not sure" onClick="javascript:history.back();">
                    <input name="show" type="submit" class="flat" id="show" value="Yes I am sure">
                    <input name="sid" type="hidden" id="sid" value="<?=$sid;?>">
                    <input name="offset" type="hidden" id="offset" value="<?=$offset;?>">
                    <input name="mid" type="hidden" id="mid" value="1">
                    <input name="ssubmit" type="hidden" id="show" value="recordDelInformation">
                    <input name="q" type="hidden" id="q" value="<?php echo htmlspecialchars($q);?>">
                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>">
                    <input name="by" type="hidden" id="by" value="<?=$by?>">
                    <input name="field" type="hidden" id="field" value="<?=$field?>">
                    <input name="LIKE" type="hidden" value="<?=$LIKE?>">
                    <input name="tab" type="hidden" value="events">
            		<input name="subtab" type="hidden"  value="gallery">
                    </td>
                </tr>
              </form>
            </table></td>
        </tr>
      </table>
  </tr>
</table>
<?php } ?>

<script language="javascript">

	var host = '<?=HOST_URL?>';
	var lan = 'ar';
	
	function get_room_names_ajax(court_name) {
		var xmlHttp, rnd, url;
		rnd = Math.floor(Math.random()*11);
		
		try{		
			xmlHttp = new XMLHttpRequest(); 
		}catch(e) {
			try{
				xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
			}catch(e) {
				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
		}
		
		//AJAX response
		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
				//alert(xmlHttp.responseText);
				$("#court_name_container").html(xmlHttp.responseText);
			}
		}
			
		//Sending AJAX request
		url = host + "/"+lan+"/court/get_room_names_ajax/"+court_name;
		xmlHttp.open("POST",url,true);
		xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlHttp.send("rnd="+rnd);
		
	}//function
</script>
<?php include("../footer.php"); ?>
