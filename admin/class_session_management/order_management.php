<?php	
	include ($_SERVER["DOCUMENT_ROOT"]."/aqdar/includes/config.inc.php");
	$sid = $_REQUEST["sid"];
	
	$tbl_school_id_sess = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
	
	if (!loggedUser($sid)) { 
		redirect (HOST_URL ."/admin/include/messages.php?msg=relogin");
		exit();
	}
	
	$LIB_CLASSES_FOLDER = $_SERVER['DOCUMENT_ROOT']."/aqdar/lib/classes/";
	include($LIB_CLASSES_FOLDER."Paging.php");
	
	foreach($_REQUEST as $key => $value){
		$$key = $value;
	} 

	$q = stripslashes($q);	
	
	/* UPDATE RECORD */
	if($_REQUEST['ssubmit']=="insertColumns") {
		$qry = "UPDATE ".TBL_CLASS_SESSIONS." SET is_active = 'N', sessions_order = '1000' WHERE 1 AND tbl_school_id='".$tbl_school_id_sess."'";
		//echo $qry."<br><br>";
		update($qry);
		
		for($i=0; $i<count($sessions_order); $i++) {
			//echo $sessions_order[$i]."<br>";
			$qry = "UPDATE ".TBL_CLASS_SESSIONS." SET is_active = 'Y', sessions_order = ".($i+1)." WHERE tbl_class_sessions_id='".$sessions_order[$i]."' AND tbl_school_id='".$tbl_school_id_sess."'";
			//echo $qry."<br><br>";
			update($qry);
			$MSG = "Item order information has been updated successfully.";
		}
		//print_r($sessions_order);
		$mid=1;
	}
	

?>
<html>
    <title>Admin (Column Configuration)</title>
    <style type="text/css">
    .chkbox1 {float:left; padding-left:10px}
.chkbox1 {float:left; padding-left:10px}
    </style>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="<?=HOST_URL?>/admin/css/interface.css" type=text/css rel=stylesheet>
    
    <link rel="stylesheet" href="<?=JS_UI?>/themes/base/jquery.ui.all.css">
	<script src="<?=JS_UI?>/jquery-1.8.0.js"></script>
	<script src="<?=JS_UI?>/ui/jquery.ui.core.js"></script>
	<script src="<?=JS_UI?>/ui/jquery.ui.widget.js"></script>

	<script src="<?=JS_UI?>/ui/jquery.ui.mouse.js"></script>
	<script src="<?=JS_UI?>/ui/jquery.ui.sortable.js"></script>
	<link rel="stylesheet" href="<?=JS_UI?>/demos/demos.css">
	<style>
	#sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
	#sortable li { margin: 0 5px 5px 5px; padding: 5px; font-size: 1.2em; text-align:left;}
	html>body #sortable li { height: 1.5em; line-height: 1.2em; }
	.ui-state-highlight { height: 1.5em; line-height: 1.2em; }
	.col_txt {margin:0 auto;}
	/* Custom CSS */
	.chkbox {float:left; padding-left:10px}
	.img_frame {background-color:#FFF; padding:2px; border:1px solid #CCC;}
	</style>
	<script>
	$(function() {
		$( "#sortable" ).sortable({
			placeholder: "ui-state-highlight"
		});
		$( "#sortable" ).disableSelection();
	});
	function Sort() {
		var result = $('#sortable').sortable('toArray');
		for(i=0; i<result.length; i++) {
			alert(result[i]);
		}
	}
	</script>
</head>



<?php if (!$offset || $offset<0)  { $offset =0;} ?>
<?php if (!$LIKE)  { $LIKE = "LIKE";} ?>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<!--	COLUMN ORDER MANAGEMENT	-->
<?php if($mid=="1"){ ?>
<?php	
	$qry = "SELECT * FROM ".TBL_CLASS_SESSIONS." WHERE 1 AND tbl_school_id='".$tbl_school_id_sess."' ORDER BY sessions_order ASC";
	$data = SelectMultiRecords($qry);
	//echo $qry;
?>
<table width="100%" height="570" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr>
    <td height="30" valign="center" class="adminDetailHeading">&nbsp;Class Sessions Order Management </td>
  </tr>
  <tr align="center" >
    <td height="538" align="center" valign="top" >
      <br>
      <span class="msgColor"><?php echo $MSG;?></span><br>
      <form name="frmRecord" id="frmRecord" method="post" action=""  onSubmit="return validateForm();">
        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >
          <tr >
            <td height="22" align="left" valign="middle"><span class="msgColor">Note: Drag items to arrange them.</span></td>
          </tr>
          <tr >
            <td height="22" align="left" valign="middle"  class="adminDetailHeading" ><strong>&nbsp;</strong></td>
          </tr>
          <tr >
            <td align="center" valign="top" class="bordLightBlue">
            <div class="demo">
                <ul id="sortable">
                	<?php	for($i=0; $i<count($data); $i++) {
								$id = $data[$i]["id"];
								$tbl_class_sessions_id = $data[$i]["tbl_class_sessions_id"];
								$title = $data[$i]["title"];
								$start_time = $data[$i]["start_time"];
								$end_time = $data[$i]["end_time"];
								$sessions_order = $data[$i]["sessions_order"];
								$added_date = $data[$i]["added_date"];				
								$is_active = $data[$i]["is_active"];
								
								$chk = "";
								if ($is_active == "Y") {
									$chk = 'checked="checked"';
								}
					?>
                    <li id="<?=$sessions_order?>" class="ui-state-default" style="background-image:none; background-color:#E9E9E9">
                      <div class="col_txt">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="7%" align="left" valign="middle"><div class="chkbox1">
                              <input type="checkbox" name="sessions_order[]" value="<?=$tbl_class_sessions_id?>" <?=$chk?>>
                            </div></td>
                            <td align="center" valign="top"><strong><?=$title?></strong> <?=$start_time?> to <?=$end_time?></td>
                          </tr>
                        </table>
</span>
                    </li>
                    <?php	}	?>
                </ul>
            </div>
            </td>
          </tr>
        </table>
        <!--<input type="button" name="btn" value="Button" onClick="Sort()">-->
        <br>
        <input type="reset" name="Reset" value="Reset" class="flat">
        <input name="save" type="submit" class="flat" id="ssubmit"  value="Submit">
        <input name="ssubmit" type="hidden" id="ssubmit" value="insertColumns">
        <input name="sid" type="hidden" value="<?=$sid?>">
        <input name="mid" type="hidden" value="1">
        <br>
      </form>
      <br>
      <br>
      </td>
  </tr>
</table>
<?php }  ?>
</body>
</html>