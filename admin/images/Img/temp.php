<table width="70%" border='0' cellpadding='0' cellspacing='0' id='content2' >
  <tr> 
    <td height="113" >&nbsp;</td>
    <td valign="top"> 
<script language="JavaScript" type="text/javascript" >

 function RTB_HRule(editor,htmlmode) { RTB_Format(editor,htmlmode,'inserthorizontalrule'); }
 
 function RTB_SetFont(editor,htmlmode,name,value)
 {
 if (htmlmode) return;
 editor.focus();
 editor.document.execCommand('fontname','',value);
 }
 
 function RTB_BackColorPicker(editor,htmlmode)
 {
 if (htmlmode) return;
 showModalDialog('/richtextbox/colorpicker.aspx?editor=' + editor.name + '&command=backcolor',window,'dialogHeight=450px;dialogWidth=220px;status=no');
 }
 
 function RTB_InsertMenu(editor,htmlmode,name,value)
 {
 if (htmlmode) return;
 editor.focus();
 sel = editor.document.selection.createRange();
 sel.pasteHTML(value);
 }
 
function RTB_Outdent(editor,htmlmode) { RTB_Format(editor,htmlmode,'outdent'); }
 
function RTB_Superscript(editor,htmlmode) { RTB_Format(editor,htmlmode,'superscript'); }
 
 function RTB_InsertLink(editor,htmlmode,name,value)
 {
 if (htmlmode) return;
 editor.focus();
 sel = editor.document.selection.createRange();
 sel.pasteHTML('<a href=' + value + '>' + name + '</a>');
 }
 
function RTB_CJustify(editor,htmlmode) { RTB_Format(editor,htmlmode,'justifycenter'); }
 
function RTB_Paste(editor,htmlmode)
{
 editor.focus();
 editor.document.execCommand('paste','',null);
}
 
function RTB_InsertEmoticon(editor,htmlmode,name,value)
{
	 if (htmlmode) return;
	 editor.focus();
	 sel = editor.document.selection.createRange();
	 sel.pasteHTML('<img src=' + value + ' />');
}

function RTB_SetFontSize(editor,htmlmode,name,value)
{
	 if (htmlmode) return;
	 editor.focus();
	 editor.document.execCommand('fontsize','',value);
 }
 
function RTB_Undo(editor,htmlmode)
{
 editor.focus();
 editor.document.execCommand('undo','',null);
}
 
function RTB_DeleteRow(editor,htmlmode)
{
 if (htmlmode) return;
 rtb_ID = editor;
 objReference=RTB_GetRangeReference(rtb_ID);
 objReference=RTB_CheckTag(objReference,'/^(TABLE)|^(TR)|^(TD)|^(TBODY)/');
 switch(objReference.tagName)
 {
 case 'TR' :var rowIndex = objReference.rowIndex;//Get rowIndex
 var parentTable=objReference.parentElement.parentElement;
 parentTable.deleteRow(rowIndex);
 break;
 case 'TD' :var cellIndex=objReference.cellIndex;
 var parentRow=objReference.parentElement;//Get Parent Row
 var rowIndex = parentRow.rowIndex;//Get rowIndex
 var parentTable=objReference.parentElement.parentElement.parentElement;
 parentTable.deleteRow(rowIndex);
 if (rowIndex>=parentTable.rows.length)
 {
 rowIndex=parentTable.rows.length-1;
 }
 if (rowIndex>=0)
 {
 var r = rtb_ID.document.body.createTextRange();
 r.moveToElementText(parentTable.rows[rowIndex].cells[cellIndex]);
 r.moveStart('character',r.text.length);
 r.select();
 }
 else
 {
 parentTable.removeNode(true);
 }
 break;
 
 default :return;
 }
 }
 
function RTB_Strikethrough(editor,htmlmode) { RTB_Format(editor,htmlmode,'strikethrough'); }
 
function RTB_Copy(editor,htmlmode)
{
 editor.focus();
 editor.document.execCommand('copy','',null);
}
 
function RTB_Indent(editor,htmlmode) { RTB_Format(editor,htmlmode,'indent'); }
 
function RTB_Underline(editor,htmlmode) { RTB_Format(editor,htmlmode,'underline'); }
 
function RTB_SetFontBackColor(editor,htmlmode,name,value)
{
 if (htmlmode) return;
 editor.focus();
 editor.document.execCommand('backcolor','',value);
}
 
function RTB_Numbered(editor,htmlmode) { RTB_Format(editor,htmlmode,'insertorderedlist'); }
 
function RTB_InsertDefaultTable(editor,htmlmode)
{
 if (htmlmode) return;
 RTB_InsertTable(editor,3,3);
}
 
function RTB_Italic(editor,htmlmode) { RTB_Format(editor,htmlmode,'italic'); }
 
function RTB_RJustify(editor,htmlmode) { RTB_Format(editor,htmlmode,'justifyright'); }
 
function button_over(eButton,command) { eButton.src = '<?=IMG_RICH_PATH?>/' + command + '_over.gif'; } function button_out(eButton,command) { eButton.src = '<?=IMG_RICH_PATH?>/' + command + '.gif'; } 
function CopyDatarichtext1()
{
 d = richtext1_x5.document;
 if (RTB_HtmlMode_richtext1())
 {
  document.getElementById('richtext1').value = d.body.innerText; 
 }
 else
 {
 document.getElementById('richtext1').value = d.body.innerHTML;  
 }
 if (document.getElementById('richtext1').value == '<P>&nbsp;</P>')
 {
 document.getElementById('richtext1').value = '';
 }
 }
 
 function RTB_onKeyDown_richtext1()
 {
 editor = richtext1_x5;
 if (richtext1_x5.event.keyCode == 9)
 {
 richtext1_x5.event.cancelBubble = true;
 richtext1_x5.event.returnValue = false;
 }
 }
 
 function RTB_HtmlMode_richtext1()
 {
 var chk = document.getElementById('richtext1_ShowHTML');
 if (chk == null)
 {
 return false;
 }
 else
 {
 return chk.checked;
 }
 }
 
 function richtext1_SetChk()
 {
   document.getElementById('richtext1_ShowHTML').checked = false
 }
 
 function RTB_InsertTable(rtb_ID,rows,columns)
 {
 rtb_ID.focus();
 var newTable = rtb_ID.document.createElement('TABLE');
 for(y=0; y<rows; y++)
 {
 var newRow = newTable.insertRow();
 
 for(x=0; x<columns; x++)
 {
 var newCell = newRow.insertCell();
 if ((y==0)&&(x==0)) newCell.id='ura';
 }
 }
 newTable.border = 1;
 newTable.width = 25 * columns;
 if (rtb_ID.document.selection.type=='Control')
 {
 sel.pasteHTML(newTable.outerHTML);
 }
 else
 {
 sel = rtb_ID.document.selection.createRange();
 sel.pasteHTML(newTable.outerHTML);
 }
 
 var r = rtb_ID.document.body.createTextRange();
 var item=rtb_ID.document.getElementById('ura');
 item.id='';
 r.moveToElementText(item);
 r.moveStart('character',r.text.length);
 r.select();
 }
 
 function RTB_DeleteColumn(editor,htmlmode)
 {
 if (htmlmode) return;
 rtb_ID = editor;
 objReference=RTB_GetRangeReference(rtb_ID);
 objReference=RTB_CheckTag(objReference,'/^(TABLE)|^(TR)|^(TD)|^(TBODY)/');
 switch(objReference.tagName)
 {
 
 case 'TD' :var rowIndex=objReference.parentElement.rowIndex;
 var cellIndex = objReference.cellIndex;//Get cellIndex
 var parentTable=objReference.parentElement.parentElement.parentElement;
 var newTable=parentTable.cloneNode(true);
 if (newTable.rows[0].cells.length==1)
 {
 parentTable.removeNode(true);
 return;
 }
 for(x=0; x<newTable.rows.length; x++)
 {
 if (newTable.rows[x].cells[cellIndex]=='[object]')
 {
 newTable.rows[x].deleteCell(cellIndex);
 }
 }
 if (cellIndex>=newTable.rows[0].cells.length)
 {
 cellIndex=newTable.rows[0].cells.length-1;
 }
 if (cellIndex>=0)  newTable.rows[rowIndex].cells[cellIndex].id='ura';
 parentTable.outerHTML=newTable.outerHTML;
 if (cellIndex>=0){
 var r = rtb_ID.document.body.createTextRange();
 var item=rtb_ID.document.getElementById('ura');
 item.id='';
 r.moveToElementText(item);
 r.moveStart('character',r.text.length);
 r.select();
 }
 break;
 default :return;
 }
 }
 
 
function RTB_LJustify(editor,htmlmode) { RTB_Format(editor,htmlmode,'justifyleft'); }
 
function RTB_Cut(editor,htmlmode)
 {
 editor.focus();
     editor.document.execCommand('cut','',null);
 }
 
function RTB_Setup(editor,hidden)
 {
 editor.document.designMode='On';
 if (editor.value == null) editor.value = editor.innerHTML;
 var d = editor.document;
 d.designMode = 'On';
 d.open();
 d.write(hidden.value);
 d.close();
 editor.document.createStyleSheet('styles.css');
 editor.document.createStyleSheet('');
 editor.document.styleSheets[1].disabled = true;
 richtext1_x5.document.body.contentEditable = 'True';
 RTB_ChangeMode(richtext1_x5,'richtext1_toolbar',true,false);
 RTB_ChangeMode(richtext1_x5,'richtext1_toolbar',false,false);
 } 
 function RTB_ChangeMode(editor,toolbarID,newSetting,autohide)
 {
 var toolbar = document.getElementById(toolbarID);
 var sTmp;
 if (newSetting)
 {
 sTmp=editor.document.body.innerHTML;
 editor.document.styleSheets[0].disabled = true;
 editor.document.styleSheets[1].disabled = false;
 if (toolbar != null && autohide)
 {
 toolbar.style.display = 'none';
 }
 editor.document.body.innerText=sTmp;
 }
 else
 {
 sTmp=editor.document.body.innerText;
 editor.document.styleSheets[0].disabled = false;
 editor.document.styleSheets[1].disabled = true;
 if (toolbar != null && autohide)
 {
 toolbar.style.display = 'inline';
 }
 editor.document.body.innerHTML=sTmp;
 }
 editor.focus(); } 
 function RTB_CheckTag(item,tagName)
 {
 if (item.tagName.search(tagName)!=-1)
 {
 return item;
 }
 if (item.tagName=='BODY')
 {
 return false;
 }
 item=item.parentElement;
 return RTB_CheckTag(item,tagName);
 }
 
 function RTB_Format(editor,htmlmode,format)
 {
 if (!htmlmode)
 {
 editor.focus();
 editor.document.execCommand(format,'',null);
 }
 else
 {
 alert('To use the toolbar, change the HTML view setting');
 }
 }
 
 function RTB_insertColumn(editor,htmlmode)
 {
 if (htmlmode) return;
 rtb_ID = editor;
 objReference= RTB_GetRangeReference(rtb_ID);
 objReference=RTB_CheckTag(objReference,'/^(TABLE)|^(TR)|^(TD)|^(TBODY)/');
 switch(objReference.tagName)
 {
 case 'TABLE' :// IF a table is selected, it adds a new column on the right hand side of the table.
 var newTable=objReference.cloneNode(true);
 for(x=0; x<newTable.rows.length; x++)
 {
 var newCell = newTable.rows[x].insertCell();
 }
 newCell.focus();
 objReference.outerHTML=newTable.outerHTML;
 break;
 case 'TBODY' :// IF a table is selected, it adds a new column on the right hand side of the table.
 var newTable=objReference.cloneNode(true);
 for(x=0; x<newTable.rows.length; x++)
 {
 var newCell = newTable.rows[x].insertCell();
 }
 objReference.outerHTML=newTable.outerHTML;
 break;
 case 'TR' :// IF a table is selected, it adds a new column on the right hand side of the table.
 objReference=objReference.parentElement.parentElement;
 var newTable=objReference.cloneNode(true);
 for(x=0; x<newTable.rows.length; x++)
 {
 var newCell = newTable.rows[x].insertCell();
 }
 objReference.outerHTML=newTable.outerHTML;
 break;
 case 'TD' :// IF the cursor is in a cell, or a cell is selected, it adds a new column to the right of that cell.
 var cellIndex = objReference.cellIndex;//Get cellIndex
 var rowIndex=objReference.parentElement.rowIndex;
 var parentTable=objReference.parentElement.parentElement.parentElement;
 var newTable=parentTable.cloneNode(true);
 for(x=0; x<newTable.rows.length; x++)
 {
 var newCell = newTable.rows[x].insertCell(cellIndex+1);
 if (x==rowIndex)newCell.id='ura';
 }
 parentTable.outerHTML=newTable.outerHTML;
 var r = rtb_ID.document.body.createTextRange();
 var item=rtb_ID.document.getElementById('ura');
 item.id='';
 r.moveToElementText(item);
 r.moveStart('character',r.text.length);
 r.select();
 break;
 default :// IF the cursor is not in a table, it acts as if they clicked Insert Table.
 RTB_InsertTable(rtb_ID,3,1);
 return;
 }
 }

 function RTB_InsertImage(editor,htmlmode)
 {
     if (!htmlmode)
 {
     editor.focus();
         editor.document.execCommand('InsertImage','1','');
 }
 }

 function RTB_ImageMenu(editor,htmlmode,name,value)
 {
 if (value != '')
 {
 if (htmlmode) return;
 editor.focus();
 editor.document.execCommand('InsertImage','',value);
 }
 }

 function RTB_Redo(editor,htmlmode)
 {
 editor.focus();
     editor.document.execCommand('redo','',null);
 }
 
 function RTB_Bullets(editor,htmlmode) { RTB_Format(editor,htmlmode,'insertunorderedlist'); }
 
 function RTB_InsertSymbol(editor,htmlmode,name,value)
 {
 if (htmlmode) return;
 editor.focus();
 sel = editor.document.selection.createRange();
 sel.pasteHTML(value);
 }
 
 function RTB_insertRow(editor,htmlmode)
 {
 if (htmlmode) return;
 rtb_ID = editor;
 objReference=RTB_GetRangeReference(rtb_ID);
 objReference=RTB_CheckTag(objReference,'/^(TABLE)|^(TR)|^(TD)|^(TBODY)/');
 switch(objReference.tagName)
 {
 case 'TABLE' :
 var newTable=objReference.cloneNode(true);
 var newRow= newTable.insertRow();
 
 for(x=0; x<newTable.rows[0].cells.length; x++)
 {
 var newCell = newRow.insertCell();
 }
 objReference.outerHTML=newTable.outerHTML;
 break;
 case 'TBODY' :
 var newTable=objReference.cloneNode(true);
 var newRow = newTable.insertRow();
 for(x=0; x<newTable.rows[0].cells.length; x++)
 {
 var newCell = newRow.insertCell();
 }
 objReference.outerHTML=newTable.outerHTML;
 break;
 case 'TR' :
 var rowIndex = objReference.rowIndex;
 var parentTable=objReference.parentElement.parentElement;
 var newTable=parentTable.cloneNode(true);
 var newRow = newTable.insertRow(rowIndex+1);
 for(x=0; x< newTable.rows[0].cells.length; x++)
 {
 var newCell = newRow.insertCell();
 }
 parentTable.outerHTML=newTable.outerHTML;
 break;
 case 'TD' :
 var parentRow=objReference.parentElement;
 var rowIndex = parentRow.rowIndex;
 var cellIndex=objReference.cellIndex;
 var parentTable=objReference.parentElement.parentElement.parentElement;
 var newTable=parentTable.cloneNode(true);
 var newRow = newTable.insertRow(rowIndex+1);
 for(x=0; x< newTable.rows[0].cells.length; x++)
 {
 var newCell = newRow.insertCell();
 if (x==cellIndex)newCell.id='ura';
 }
 parentTable.outerHTML=newTable.outerHTML;
 var r = rtb_ID.document.body.createTextRange();
 var item=rtb_ID.document.getElementById('ura');
 item.id='';
 r.moveToElementText(item);
 r.moveStart('character',r.text.length);
 r.select();
 break;
 default :
 RTB_InsertTable(rtb_ID,1,3);
 return;
 }
 
 }

 function RTB_Subscript(editor,htmlmode) { RTB_Format(editor,htmlmode,'subscript'); }
 
 function RTB_ForeColorPicker(editor,htmlmode)
 {
 if (htmlmode) return;
 showModalDialog('<?=INC_ADMIN_PATH?>colorpicker.php',window,'dialogHeight=450px;dialogWidth=220px;status=no');
 }
 
 function RTB_SetFontForeColor(editor,htmlmode,name,value)
 {
 if (htmlmode) return;
 editor.focus();
 editor.document.execCommand('forecolor','',value);
 }
 
 function RTB_Print(editor,htmlmode) { editor.document.execCommand('print','',null); }
 
 function RTB_Bold(editor,htmlmode) { RTB_Format(editor,htmlmode,'bold'); }
 
 function RTB_GetRangeReference(rtb_ID)
 {
 rtb_ID.focus();
 var objReference = null;
 var RangeType = rtb_ID.document.selection.type;
 var selectedRange = rtb_ID.document.selection.createRange();
 
 switch(RangeType)
 {
 case 'Control' :
 if (selectedRange.length > 0 ) 
 {
 objReference = selectedRange.item(0);
 }
 break;
 
 case 'None' :
 objReference = selectedRange.parentElement();
 break;
 
 case 'Text' :
 objReference = selectedRange.parentElement();
 break;
 }
 return objReference
 }
 
 function RTB_SetParagraph(editor,htmlmode,name,value)
 {
 if (htmlmode) return;
 editor.focus();
 if (value == '<body>')
 {
 editor.document.execCommand('formatBlock','','Normal');
 editor.document.execCommand('removeFormat');
 return;
 }
 editor.document.execCommand('formatblock','',value);
 }
 
 function RTB_SetStyle(editor,htmlmode,name,value)
 {
 if (htmlmode) return;
 if (value != '')
 {
 editor.focus();
 editor.document.execCommand('removeFormat');
 editor.document.execCommand('formatBlock','','Normal');
 sel = editor.document.selection.createRange();
 html = '<font class=\'' + value + '\'>' + sel.htmlText + '</font>';
 sel.pasteHTML(html);
 }
 }
 
</script> 
      <table width="81%" border="0" cellpadding="0" cellspacing="0" style="border-width:0px;border-style:Solid;width:100%;border-collapse:collapse;">
        <tr id="richtext1_toolbar"> 
          <td style="background-color:#EFEFDE;"> <table width="73%" border="0" cellpadding="0" cellspacing="0" style="border-width:0px;border-style:Solid;width:100%;border-collapse:collapse;">
              <tr> 
                <td align="Left" style="background-color:#EFEFDE;"><table cellspacing="0" cellpadding="1" border="0" style="border-collapse:collapse;">
                    <tr> 
                      <!-- <td><select class="" onchange="RTB_SetStyle(richtext1_x5,RTB_HtmlMode_richtext1(),this[this.selectedIndex].innerText,this[this.selectedIndex].value);">
                            <option selected="selected">Style</option>
                            <option value="MainHeading">Main Heading</option>
                            <option value="SubHeading">Sub Heading</option>
                            <option value="">None</option>
                          </select></td>-->
                      <td><select class="" onchange="RTB_SetParagraph(richtext1_x5,RTB_HtmlMode_richtext1(),this[this.selectedIndex].innerText,this[this.selectedIndex].value);">
                          <option selected="selected">Paragraph</option>
                          <option value="<body>">Normal</option>
                          <option value="<h1>"> Heading 1</option>
                          <option value="<h2>">Heading 2</option>
                          <option value="<h3>">Heading 3</option>
                          <option value="<h4>">Heading 4</option>
                          <option value="<h5>">Heading 5</option>
                          <option value="<h6>">Heading 6</option>
                          <option value="<dir>">Directory List</option>
                          <option value="<menu>">Menu List</option>
                          <option value="<pre>">Formatted</option>
                          <option value="<address>">Address</option>
                        </select></td>
                      <td><select class="" onchange="RTB_SetFont(richtext1_x5,RTB_HtmlMode_richtext1(),this[this.selectedIndex].innerText,this[this.selectedIndex].value);">
                          <option selected="selected">Font</option>
                          <option value="Arial">Arial</option>
                          <option value="Arial Black">Arial Black</option>
                          <option value="Comic Sans MS">Comic Sans MS</option>
                          <option value="Courier New">Courier New</option>
                          <option value="Georgia">Georgia</option>
                          <option value="Impact">Impact</option>
                          <option value="Lucida Console">Lucida Console</option>
                          <option value="Palatino Linotype">Palatino Linotype</option>
                          <option value="Tahoma">Tahoma</option>
                          <option value="Times New Roman">Times New Roman</option>
                          <option value="Trebuchet MS">Trebuchet MS</option>
                          <option value="Verdana">Verdana</option>
                        </select></td>
                      <td><select class="" onchange="RTB_SetFontSize(richtext1_x5,RTB_HtmlMode_richtext1(),this[this.selectedIndex].innerText,this[this.selectedIndex].value);">
                          <option selected="selected">Size</option>
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                          <option value="6">6</option>
                        </select></td>
                      <td><select class="" onchange="RTB_SetFontForeColor(richtext1_x5,RTB_HtmlMode_richtext1(),this[this.selectedIndex].innerText,this[this.selectedIndex].value);">
                          <option selected="selected">Color</option>
                          <option value="Black" style="background-color: Black">Black</option>
                          <option value="Gray" style="background-color: Gray">Gray</option>
                          <option value="DarkGray" style="background-color: DarkGray">DarkGray</option>
                          <option value="LightGrey" style="background-color: LightGrey">LightGray</option>
                          <option value="White" style="background-color: White">White</option>
                          <option value="Aquamarine" style="background-color: Aquamarine">Aquamarine</option>
                          <option value="Blue" style="background-color: Blue">Blue</option>
                          <option value="Navy" style="background-color: Navy">Navy</option>
                          <option value="Purple" style="background-color: Purple">Purple</option>
                          <option value="DeepPink" style="background-color: DeepPink">DeepPink</option>
                          <option value="Violet" style="background-color: Violet">Violet</option>
                          <option value="Pink" style="background-color: Pink">Pink</option>
                          <option value="DarkGreen" style="background-color: DarkGreen">DarkGreen</option>
                          <option value="Green" style="background-color: Green">Green</option>
                          <option value="YellowGreen" style="background-color: YellowGreen">YellowGreen</option>
                          <option value="Yellow" style="background-color: Yellow">Yellow</option>
                          <option value="Orange" style="background-color: Orange">Orange</option>
                          <option value="Red" style="background-color: Red">Red</option>
                          <option value="Brown" style="background-color: Brown">Brown</option>
                          <option value="BurlyWood" style="background-color: BurlyWood">BurlyWood</option>
                          <option value="Beige" style="background-color: Beige">Beige</option>
                        </select></td>
                      <td>&nbsp;</td>
                      <td><select class="" onchange="RTB_SetFontBackColor(richtext1_x5,RTB_HtmlMode_richtext1(),this[this.selectedIndex].innerText,this[this.selectedIndex].value);">
                          <option selected="selected">Highlight</option>
                          <option value="Black" style="background-color: Black">Black</option>
                          <option value="Gray" style="background-color: Gray">Gray</option>
                          <option value="DarkGray" style="background-color: DarkGray">DarkGray</option>
                          <option value="LightGrey" style="background-color: LightGrey">LightGray</option>
                          <option value="White" style="background-color: White">White</option>
                          <option value="Aquamarine" style="background-color: Aquamarine">Aquamarine</option>
                          <option value="Blue" style="background-color: Blue">Blue</option>
                          <option value="Navy" style="background-color: Navy">Navy</option>
                          <option value="Purple" style="background-color: Purple">Purple</option>
                          <option value="DeepPink" style="background-color: DeepPink">DeepPink</option>
                          <option value="Violet" style="background-color: Violet">Violet</option>
                          <option value="Pink" style="background-color: Pink">Pink</option>
                          <option value="DarkGreen" style="background-color: DarkGreen">DarkGreen</option>
                          <option value="Green" style="background-color: Green">Green</option>
                          <option value="YellowGreen" style="background-color: YellowGreen">YellowGreen</option>
                          <option value="Yellow" style="background-color: Yellow">Yellow</option>
                          <option value="Orange" style="background-color: Orange">Orange</option>
                          <option value="Red" style="background-color: Red">Red</option>
                          <option value="Brown" style="background-color: Brown">Brown</option>
                          <option value="BurlyWood" style="background-color: BurlyWood">BurlyWood</option>
                          <option value="Beige" style="background-color: Beige">Beige</option>
                        </select></td>
                      <!--                        <td><img src="<?=IMG_RICH_PATH?>/fontbackcolorpicker.gif" width="23" height="22" unselectable="on" alt="Text highlight" onclick="RTB_BackColorPicker(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'fontbackcolorpicker');" onmouseout="button_out(this,'fontbackcolorpicker');" /></td>-->
                    </tr>
                  </table>
                  <table cellspacing="0" cellpadding="1" border="0" style="border-collapse:collapse;">
                    <tr> 
                      <td><img src="<?=IMG_RICH_PATH?>/bold.gif" width="23" height="22" unselectable="on" alt="Bold" onclick="RTB_Bold(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'bold');" onmouseout="button_out(this,'bold');" /></td>
                      <td><img src="<?=IMG_RICH_PATH?>/italic.gif" width="23" height="22" unselectable="on" alt="Italic" onclick="RTB_Italic(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'italic');" onmouseout="button_out(this,'italic');" /></td>
                      <td><img src="<?=IMG_RICH_PATH?>/underline.gif" width="23" height="22" unselectable="on" alt="Underline" onclick="RTB_Underline(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'underline');" onmouseout="button_out(this,'underline');" /></td>
                      <!-- <td><img src="<?=IMG_RICH_PATH?>/superscript.gif" width="23" height="22" unselectable="on" alt="Superscript" onclick="RTB_Superscript(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'superscript');" onmouseout="button_out(this,'superscript');" /></td>-->
                      <!--<td><img src="<?=IMG_RICH_PATH?>/subscript.gif" width="23" height="22" unselectable="on" alt="Subscript" onclick="RTB_Subscript(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'subscript');" onmouseout="button_out(this,'subscript');" /></td>-->
                      <!-- <td><img src="<?=IMG_RICH_PATH?>/strikethrough.gif" width="23" height="22" unselectable="on" alt="Strikethrough" onclick="RTB_Strikethrough(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'strikethrough');" onmouseout="button_out(this,'strikethrough');" /></td>-->
                      <td><img src="<?=IMG_RICH_PATH?>/separator.gif" width="5" height="22" unselectable="on" /></td>
                      <td><img src="<?=IMG_RICH_PATH?>/leftjustify.gif" width="23" height="22" unselectable="on" alt="Left justify" onclick="RTB_LJustify(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'leftjustify');" onmouseout="button_out(this,'leftjustify');" /></td>
                      <td><img src="<?=IMG_RICH_PATH?>/centerjustify.gif" width="23" height="22" unselectable="on" alt="Center justify" onclick="RTB_CJustify(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'centerjustify');" onmouseout="button_out(this,'centerjustify');" /></td>
                      <td><img src="<?=IMG_RICH_PATH?>/rightjustify.gif" width="23" height="22" unselectable="on" alt="Right justify" onclick="RTB_RJustify(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'rightjustify');" onmouseout="button_out(this,'rightjustify');" /></td>
                      <td><img src="<?=IMG_RICH_PATH?>/separator.gif" width="5" height="22" unselectable="on" /></td>
                      <td><img src="<?=IMG_RICH_PATH?>/numberedlist.gif" width="23" height="22" unselectable="on" alt="Numbered list" onclick="RTB_Numbered(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'numberedlist');" onmouseout="button_out(this,'numberedlist');" /></td>
                      <td><img src="<?=IMG_RICH_PATH?>/bullets.gif" width="23" height="22" unselectable="on" alt="Bullet list" onclick="RTB_Bullets(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'bullets');" onmouseout="button_out(this,'bullets');" /></td>
                      <!-- <td><img src="<?=IMG_RICH_PATH?>/outdent.gif" width="23" height="22" unselectable="on" alt="Outdent" onclick="RTB_Outdent(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'outdent');" onmouseout="button_out(this,'outdent');" /></td>
                      <td><img src="<?=IMG_RICH_PATH?>/indent.gif" width="23" height="22" unselectable="on" alt="Indent" onclick="RTB_Indent(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'indent');" onmouseout="button_out(this,'indent');" /></td>-->
                      <td><img src="<?=IMG_RICH_PATH?>/separator.gif" width="5" height="22" unselectable="on" /></td>
                      <td><img src="<?=IMG_RICH_PATH?>/cut.gif" width="23" height="22" unselectable="on" alt="Cut" onclick="RTB_Cut(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'cut');" onmouseout="button_out(this,'cut');" /></td>
                      <td><img src="<?=IMG_RICH_PATH?>/copy.gif" width="23" height="22" unselectable="on" alt="Copy" onclick="RTB_Copy(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'copy');" onmouseout="button_out(this,'copy');" /></td>
                      <td><img src="<?=IMG_RICH_PATH?>/paste.gif" width="23" height="22" unselectable="on" alt="Paste" onclick="RTB_Paste(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'paste');" onmouseout="button_out(this,'paste');" /></td>
                      <td><img src="<?=IMG_RICH_PATH?>/separator.gif" width="5" height="22" unselectable="on" /></td>
                      <td><img src="<?=IMG_RICH_PATH?>/undo.gif" width="23" height="22" unselectable="on" alt="Undo" onclick="RTB_Undo(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'undo');" onmouseout="button_out(this,'undo');" /></td>
                      <td><img src="<?=IMG_RICH_PATH?>/redo.gif" width="23" height="22" unselectable="on" alt="Redo" onclick="RTB_Redo(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'redo');" onmouseout="button_out(this,'redo');" /></td>
                      <td><img src="<?=IMG_RICH_PATH?>/separator.gif" width="5" height="22" unselectable="on" /></td>
                      <!-- <td><img src="<?=IMG_RICH_PATH?>/insertimage.gif" width="23" height="22" unselectable="on" alt="Insert image" onclick="RTB_InsertImage(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'insertimage');" onmouseout="button_out(this,'insertimage');" /></td>-->
                      <!--<td><select class="" onchange="RTB_ImageMenu(richtext1_x5,RTB_HtmlMode_richtext1(),this[this.selectedIndex].innerText,this[this.selectedIndex].value);this.selectedIndex=0;">
                            <option selected="selected">Image</option>
                            <option value="Img/rtblogo.gif">Company logo</option>
                            <option value="Img/demo1.png">Screenshot</option>
                            <option value="Img/sig.jpg">Signature</option>
                          </select></td>-->
                      <td><img src="<?=IMG_RICH_PATH?>/insertrule.gif" width="23" height="22" unselectable="on" alt="Insert rule" onclick="RTB_HRule(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'insertrule');" onmouseout="button_out(this,'insertrule');" /></td>
                      <td><table cellspacing="0" cellpadding="1" border="0" style="border-collapse:collapse;">
                          <tr> 
                            <!-- <td><img src="<?=IMG_RICH_PATH?>/Save.gif" width="23" height="22" unselectable="on" alt="Save" onclick="CopyDatarichtext1();getElementById('richtext1').form.submit();" onmouseover="button_over(this,'save');" onmouseout="button_out(this,'save');" /></td>-->
                            <td><img src="<?=IMG_RICH_PATH?>/inserttable.gif" width="23" height="22" unselectable="on" alt="Insert table" onclick="RTB_InsertDefaultTable(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'inserttable');" onmouseout="button_out(this,'inserttable');" /></td>
                            <td><img src="<?=IMG_RICH_PATH?>/insertrow.gif" width="23" height="22" unselectable="on" alt="Insert table row" onclick="RTB_insertRow(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'insertrow');" onmouseout="button_out(this,'insertrow');" /></td>
                            <td><img src="<?=IMG_RICH_PATH?>/insertcolumn.gif" width="23" height="22" unselectable="on" alt="Insert table column" onclick="RTB_insertColumn(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'insertcolumn');" onmouseout="button_out(this,'insertcolumn');" /></td>
                            <td><img src="<?=IMG_RICH_PATH?>/deleterow.gif" width="23" height="22" unselectable="on" alt="Delete table row" onclick="RTB_DeleteRow(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'deleterow');" onmouseout="button_out(this,'deleterow');" /></td>
                            <td><img src="<?=IMG_RICH_PATH?>/deletecolumn.gif" width="23" height="22" unselectable="on" alt="Delete table column" onclick="RTB_DeleteColumn(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'deletecolumn');" onmouseout="button_out(this,'deletecolumn');" /></td>
                            <!-- <td><select class="" onchange="RTB_InsertSymbol(richtext1_x5,RTB_HtmlMode_richtext1(),this[this.selectedIndex].innerText,this[this.selectedIndex].value);this.selectedIndex=0;">
                            <option selected="selected">Symbols</option>
                            <option value="&amp;#162;">&#162;</option>
                            <option value="&amp;#163;">&#163;</option>
                            <option value="&amp;#165;">&#165;</option>
                            <option value="&amp;#166;">&#166;</option>
                            <option value="&amp;#169;">&#169;</option>
                            <option value="&amp;#174;">&#174;</option>
                            <option value="&amp;#176;">&#176;</option>
                            <option value="&amp;#177;">&#177;</option>
                            <option value="&amp;#183;">&#183;</option>
                            <option value="&amp;#171;">&#171;</option>
                            <option value="&amp;#187;">&#187;</option>
                            <option value="&amp;#188;">&#188;</option>
                            <option value="&amp;#189;">&#189;</option>
                            <option value="&amp;#190;">&#190;</option>
                            <option value="&amp;#247;">&#247;</option>
                            <option value="&amp;#8224;">&#8224;</option>
                            <option value="&amp;#8225;">&#8225;</option>
                            <option value="&amp;#8364;">&#8364;</option>
                            <option value="&amp;#8482;">&#8482;</option>
                          </select></td>-->
                            <!--<td><select class="" onchange="RTB_InsertLink(richtext1_x5,RTB_HtmlMode_richtext1(),this[this.selectedIndex].innerText,this[this.selectedIndex].value);this.selectedIndex=0;">
                            <option selected="selected">Links</option>
                            <option value="http://www.ameersoft.com/">AmeerSoft</option>
							<option value="http://www.sohnapakistan.com">Sohna Pakistan</option>
                            <option value="http://www.microsoft.com/">Microsoft</option>
                            
                          </select></td>-->
                            <!--<td><select class="" onchange="RTB_InsertMenu(richtext1_x5,RTB_HtmlMode_richtext1(),this[this.selectedIndex].innerText,this[this.selectedIndex].value);this.selectedIndex=0;">
                            <option selected="selected">Insert</option>
                            <option value="click here to go to <a href=http://www.ameersoft.com/>ameersoft.com</a>">AmeerSoft.com</option>
                            <option value="<b>SOME BOLD TEXT</b>">Bold text</option>
                            <option value="Copyright ameersoft.com 2002">Copyright</option>
                            <option value="<hr style='color:gray;' />My footer goes here">Footer</option>
                          </select></td>-->
                            <!--<td><select class="" onchange="RTB_InsertEmoticon(richtext1_x5,RTB_HtmlMode_richtext1(),this[this.selectedIndex].innerText,this[this.selectedIndex].value);this.selectedIndex=0;">
                            <option selected="selected">Emoticons</option>
                            <option value="http://www.asp.net/Forums/skins/default/images/emotions/emotion-13.gif">Angel</option>
                            <option value="http://www.asp.net/Forums/skins/default/images/emotions/emotion-12.gif">Angry</option>
                            <option value="http://www.asp.net/Forums/skins/default/images/emotions/emotion-14.gif">Devil</option>
                            <option value="http://www.asp.net/Forums/skins/default/images/emotions/emotion-7.gif">Confused</option>
                            <option value="http://www.asp.net/Forums/skins/default/images/emotions/emotion-9.gif">Crying</option>
                            <option value="http://www.asp.net/Forums/skins/default/images/emotions/emotion-8.gif">Disappointed</option>
                            <option value="http://www.asp.net/Forums/skins/default/images/emotions/emotion-10.gif">Embarrassed</option>
                            <option value="http://www.asp.net/Forums/skins/default/images/emotions/emotion-11.gif">Hot</option>
                            <option value="http://www.asp.net/Forums/skins/default/images/emotions/emotion-2.gif">Open-mouth</option>
                            <option value="http://www.asp.net/Forums/skins/default/images/emotions/emotion-6.gif">Sad</option>
                            <option value="http://www.asp.net/Forums/skins/default/images/emotions/emotion-1.gif">Smiley</option>
                            <option value="http://www.asp.net/Forums/skins/default/images/emotions/emotion-3.gif">Surprised</option>
                            <option value="http://www.asp.net/Forums/skins/default/images/emotions/emotion-4.gif">Tongue 
                            out</option>
                            <option value="http://www.asp.net/Forums/skins/default/images/emotions/emotion-5.gif">Winking</option>
                          </select></td>-->
                          </tr>
                        </table></td>
                      <!-- <td><img src="<?=IMG_RICH_PATH?>/print.gif" width="23" height="22" unselectable="on" alt="Print" onclick="RTB_Print(richtext1_x5,RTB_HtmlMode_richtext1());" onmouseover="button_over(this,'print');" onmouseout="button_out(this,'print');" /></td>-->
                      <!--<td><a href="../help.aspx" target="_blank"><img src="<?=IMG_RICH_PATH?>/help.gif" onmouseout="button_out(this,'help');" border="0" width="23" unselectable="on" height="22" onclick="" onmouseover="button_over(this,'help');" alt="Help" /></a></td>-->
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
        <tr> 
          <td height="19"> 
            <table cellspacing="0" cellpadding="0" border="0" style="border-width:0px;border-style:Solid;width:100%;border-collapse:collapse;">
              <tr> 
                <td style="height:100%;vertical-align: top"><iframe id="richtext1_x5" name="richtext1_x5" height="300" width="100%" onblur="CopyDatarichtext1();"></iframe></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <input id="richtext1" name="richtext1" type="hidden" value="<? echo HTMLSpecialChars($text_message);?>"></input>
      <script language="JavaScript" type="text/javascript" >
imagefontforecolorpicker = new Image(); imagefontforecolorpicker.src = " <?=IMG_RICH_PATH?>/fontforecolorpicker.gif"; imagefontforecolorpickero = new Image(); imagefontforecolorpickero.src = " <?=IMG_RICH_PATH?>/fontforecolorpicker_over.gif"; imagefontbackcolorpicker = new Image(); imagefontbackcolorpicker.src = " <?=IMG_RICH_PATH?>/fontbackcolorpicker.gif"; imagefontbackcolorpickero = new Image(); imagefontbackcolorpickero.src = " <?=IMG_RICH_PATH?>/fontbackcolorpicker_over.gif"; imagebold = new Image(); imagebold.src = " <?=IMG_RICH_PATH?>/bold.gif"; imageboldo = new Image(); imageboldo.src = " <?=IMG_RICH_PATH?>/bold_over.gif"; imageitalic = new Image(); imageitalic.src = " <?=IMG_RICH_PATH?>/italic.gif"; imageitalico = new Image(); imageitalico.src = " <?=IMG_RICH_PATH?>/italic_over.gif"; imageunderline = new Image(); imageunderline.src = " <?=IMG_RICH_PATH?>/underline.gif"; imageunderlineo = new Image(); imageunderlineo.src = " <?=IMG_RICH_PATH?>/underline_over.gif"; Imguperscript = new Image(); Imguperscript.src = " <?=IMG_RICH_PATH?>/superscript.gif"; Imguperscripto = new Image(); Imguperscripto.src = " <?=IMG_RICH_PATH?>/superscript_over.gif"; Imgubscript = new Image(); Imgubscript.src = " <?=IMG_RICH_PATH?>/subscript.gif"; Imgubscripto = new Image(); Imgubscripto.src = " <?=IMG_RICH_PATH?>/subscript_over.gif"; Imgtrikethrough = new Image(); Imgtrikethrough.src = " <?=IMG_RICH_PATH?>/strikethrough.gif"; Imgtrikethrougho = new Image(); Imgtrikethrougho.src = " <?=IMG_RICH_PATH?>/strikethrough_over.gif"; imageleftjustify = new Image(); imageleftjustify.src = " <?=IMG_RICH_PATH?>/leftjustify.gif"; imageleftjustifyo = new Image(); imageleftjustifyo.src = " <?=IMG_RICH_PATH?>/leftjustify_over.gif"; imagecenterjustify = new Image(); imagecenterjustify.src = " <?=IMG_RICH_PATH?>/centerjustify.gif"; imagecenterjustifyo = new Image(); imagecenterjustifyo.src = " <?=IMG_RICH_PATH?>/centerjustify_over.gif"; imagerightjustify = new Image(); imagerightjustify.src = " <?=IMG_RICH_PATH?>/rightjustify.gif"; imagerightjustifyo = new Image(); imagerightjustifyo.src = " <?=IMG_RICH_PATH?>/rightjustify_over.gif"; imagenumberedlist = new Image(); imagenumberedlist.src = " <?=IMG_RICH_PATH?>/numberedlist.gif"; imagenumberedlisto = new Image(); imagenumberedlisto.src = " <?=IMG_RICH_PATH?>/numberedlist_over.gif"; imagebullets = new Image(); imagebullets.src = " <?=IMG_RICH_PATH?>/bullets.gif"; imagebulletso = new Image(); imagebulletso.src = " <?=IMG_RICH_PATH?>/bullets_over.gif"; imageoutdent = new Image(); imageoutdent.src = " <?=IMG_RICH_PATH?>/outdent.gif"; imageoutdento = new Image(); imageoutdento.src = " <?=IMG_RICH_PATH?>/outdent_over.gif"; imageindent = new Image(); imageindent.src = " <?=IMG_RICH_PATH?>/indent.gif"; imageindento = new Image(); imageindento.src = " <?=IMG_RICH_PATH?>/indent_over.gif"; imagecut = new Image(); imagecut.src = " <?=IMG_RICH_PATH?>/cut.gif"; imagecuto = new Image(); imagecuto.src = " <?=IMG_RICH_PATH?>/cut_over.gif"; imagecopy = new Image(); imagecopy.src = " <?=IMG_RICH_PATH?>/copy.gif"; imagecopyo = new Image(); imagecopyo.src = " <?=IMG_RICH_PATH?>/copy_over.gif"; imagepaste = new Image(); imagepaste.src = " <?=IMG_RICH_PATH?>/paste.gif"; imagepasteo = new Image(); imagepasteo.src = " <?=IMG_RICH_PATH?>/paste_over.gif"; imageundo = new Image(); imageundo.src = " <?=IMG_RICH_PATH?>/undo.gif"; imageundoo = new Image(); imageundoo.src = " <?=IMG_RICH_PATH?>/undo_over.gif"; imageredo = new Image(); imageredo.src = " <?=IMG_RICH_PATH?>/redo.gif"; imageredoo = new Image(); imageredoo.src = " <?=IMG_RICH_PATH?>/redo_over.gif"; imageinsertimage = new Image(); imageinsertimage.src = " <?=IMG_RICH_PATH?>/insertimage.gif"; imageinsertimageo = new Image(); imageinsertimageo.src = " <?=IMG_RICH_PATH?>/insertimage_over.gif"; imageinsertrule = new Image(); imageinsertrule.src = " <?=IMG_RICH_PATH?>/insertrule.gif"; imageinsertruleo = new Image(); imageinsertruleo.src = " <?=IMG_RICH_PATH?>/insertrule_over.gif"; imageprint = new Image(); imageprint.src = " <?=IMG_RICH_PATH?>/print.gif"; imageprinto = new Image(); imageprinto.src = " <?=IMG_RICH_PATH?>/print_over.gif"; imagehelp = new Image(); imagehelp.src = " <?=IMG_RICH_PATH?>/help.gif"; imagehelpo = new Image(); imagehelpo.src = " <?=IMG_RICH_PATH?>/help_over.gif"; Imgave = new Image(); Imgave.src = " <?=IMG_RICH_PATH?>/save.gif"; Imgaveo = new Image(); Imgaveo.src = " <?=IMG_RICH_PATH?>/save_over.gif"; imageinserttable = new Image(); imageinserttable.src = " <?=IMG_RICH_PATH?>/inserttable.gif"; imageinserttableo = new Image(); imageinserttableo.src = " <?=IMG_RICH_PATH?>/inserttable_over.gif"; imageinsertrow = new Image(); imageinsertrow.src = " <?=IMG_RICH_PATH?>/insertrow.gif"; imageinsertrowo = new Image(); imageinsertrowo.src = " <?=IMG_RICH_PATH?>/insertrow_over.gif"; imageinsertcolumn = new Image(); imageinsertcolumn.src = " <?=IMG_RICH_PATH?>/insertcolumn.gif"; imageinsertcolumno = new Image(); imageinsertcolumno.src = " <?=IMG_RICH_PATH?>/insertcolumn_over.gif"; imagedeleterow = new Image(); imagedeleterow.src = " <?=IMG_RICH_PATH?>/deleterow.gif"; imagedeleterowo = new Image(); imagedeleterowo.src = " <?=IMG_RICH_PATH?>/deleterow_over.gif"; imagedeletecolumn = new Image(); imagedeletecolumn.src = " <?=IMG_RICH_PATH?>/deletecolumn.gif"; imagedeletecolumno = new Image(); imagedeletecolumno.src = " <?=IMG_RICH_PATH?>/deletecolumn_over.gif"; RTB_Setup(richtext1_x5,document.getElementById('richtext1'));richtext1_x5.focus();richtext1_x5.document.onkeydown = RTB_onKeyDown_richtext1;document.getElementById('richtext1_x5').document.body.onload = richtext1_SetChk;
</script> <input name="hidden" type="hidden"  id="richtext1_ShowHTML"  onClick="RTB_ChangeMode(richtext1_x5,'richtext1_toolbar',this.checked,true);richtext1_x5.focus();">
    </td>
  </tr>
</TABLE>
