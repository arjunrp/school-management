<?php	
	include ($_SERVER["DOCUMENT_ROOT"]."/ibis_kiosk/includes/config.inc.php");
	$sid = $_REQUEST["sid"];
		
	if (!loggedUser($sid)) { 
		redirect (HOST_URL ."/admin/include/messages.php?msg=relogin");
		exit();
	}

	$LIB_CLASSES_FOLDER = $_SERVER['DOCUMENT_ROOT']."/sharjah_court/lib/classes/";
	include($LIB_CLASSES_FOLDER."Paging.php");
	
	/*foreach($_REQUEST as $key => $value){
		$$key = $value;
	}*/ 

	$q = stripslashes($q);	
	
	/* UPDATE RECORD */
	if($_REQUEST['ssubmit']=="insertColumns") {
		$qry = "UPDATE ".TBL_HOME_PAGE_IMAGES." SET is_active = 'N', images_order = '1000' WHERE 1";
		//echo $qry."<br><br>";
		update($qry);
		
		for($i=0; $i<count($images_order); $i++) {
			//echo $images_order[$i]."<br>";
			$qry = "UPDATE ".TBL_HOME_PAGE_IMAGES." SET is_active = 'Y', images_order = ".($i+1)." WHERE tbl_home_page_images_id='".$images_order[$i]."'";
			//echo $qry."<br><br>";
			update($qry);
			$MSG = "Image order information has been updated successfully.";
		}
		//print_r($images_order);
		$mid=1;
	}

?>
<html>
    <title>Admin (Image Configuration)</title>
    <style type="text/css">
    .chkbox1 {float:left; padding-left:10px}
.chkbox1 {float:left; padding-left:10px}
    </style>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="<?=HOST_URL?>/admin/css/interface.css" type=text/css rel=stylesheet>
    
    <link rel="stylesheet" href="<?=JS_UI?>/themes/base/jquery.ui.all.css">
	<script src="<?=JS_UI?>/jquery-1.8.0.js"></script>
	<script src="<?=JS_UI?>/ui/jquery.ui.core.js"></script>
	<script src="<?=JS_UI?>/ui/jquery.ui.widget.js"></script>

	<script src="<?=JS_UI?>/ui/jquery.ui.mouse.js"></script>
	<script src="<?=JS_UI?>/ui/jquery.ui.sortable.js"></script>
	<link rel="stylesheet" href="<?=JS_UI?>/demos/demos.css">
	<style>
	#sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
	#sortable li { margin: 0 5px 5px 5px; padding: 5px; height: 115px !important; text-align:left;}
	html>body #sortable li { height: 1.5em; line-height: 1.2em; }
	.ui-state-highlight { height: 1.5em; line-height: 1.2em; }
	.col_txt {margin:0 auto;}
	/* Custom CSS */
	.chkbox {float:left; padding-left:10px}
	.img_frame {background-color:#FFF; padding:2px; border:1px solid #CCC;}
	</style>
	<script>
	$(function() {
		$( "#sortable" ).sortable({
			placeholder: "ui-state-highlight"
		});
		$( "#sortable" ).disableSelection();
	});
	function Sort() {
		var result = $('#sortable').sortable('toArray');
		for(i=0; i<result.length; i++) {
			alert(result[i]);
		}
	}
	</script>
</head>



<?php if (!$offset || $offset<0)  { $offset =0;} ?>
<?php if (!$LIKE)  { $LIKE = "LIKE";} ?>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<!--	COLUMN ORDER MANAGEMENT	-->
<?php if($mid=="1"){ ?>
<?php	
	$qry = "SELECT * FROM ".TBL_HOME_PAGE_IMAGES." WHERE 1 ORDER BY images_order ASC";
	$data = SelectMultiRecords($qry);
	//echo $qry;
?>
<table width="100%" height="570" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr>
    <td height="30" valign="center" class="adminDetailHeading">&nbsp;Home Page Images Order Management </td>
  </tr>
  <tr align="center" >
    <td height="538" align="center" valign="top" >
      <br><br><br>
      <div class="top_heading"> Home Page Images Order Management</div>
      <span class="msgColor"><?php echo $MSG;?></span>
      <form name="frmRecord" id="frmRecord" method="post" action=""  onSubmit="return validateForm();">
        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >
          <tr >
            <td height="22" align="left" valign="middle"><span class="msgColor">Note: Drag panels to arrange them. </span></td>
          </tr>
          <tr >
            <td height="22" align="left" valign="middle"  class="adminDetailHeading" ><strong>&nbsp;Arrange Home Page Images In Order</strong></td>
          </tr>
          <tr >
            <td align="center" valign="top" class="bordLightBlue">
            <div class="demo">
                <ul id="sortable">
                	<?php	for($i=0; $i<count($data); $i++) {
							$tbl_home_page_images_id =$data[$i]["tbl_home_page_images_id"];
							$image_text =$data[$i]["image_text"];
							$file_name_uploaded =$data[$i]["file_name_uploaded"];
							$images_order =$data[$i]["images_order"];
							$added_date =$data[$i]["added_date"];				
							$is_active =$data[$i]["is_active"];
							$chk = "";
							if ($is_active == "Y") {
								$chk = 'checked="checked"';
							}
							if (trim($module_name_ar) == "") {
								$module_name_ar = "-- No Arabic data --";
							}
							if (trim($module_name_ur) == "") {
								$module_name_ur ="-- No Urdu data --";
							}
							
					?>
                    <li id="<?=$images_order?>" class="ui-state-default" style="background-image:none; background-color:#E9E9E9">
                      <div class="col_txt">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="7%" align="left" valign="middle"><div class="chkbox1">
                              <input type="checkbox" name="images_order[]" value="<?=$tbl_home_page_images_id?>" <?=$chk?>>
                            </div></td>
                            <td width="24%" align="center" valign="middle">
                            	<img src="<?=HTTP_HOME_PAGE_IMAGES_PATH?>/<?=$file_name_uploaded?>" title="" style="padding:3px; float:left" width="170" height="108">
                            </td>
                            <td width="1%" align="left" valign="middle">&nbsp;</td>
                            <td align="left" valign="middle"><?=$image_text?></td>
                          </tr>
                        </table>
</span>
                    </li>
                    <?php	}	?>
                </ul>
            </div>
            </td>
          </tr>
        </table>
        <!--<input type="button" name="btn" value="Button" onClick="Sort()">-->
        <br>
        <input type="reset" name="Reset" value="Reset" class="flat">
        <input name="save" type="submit" class="flat" id="ssubmit"  value="Submit">
        <input name="ssubmit" type="hidden" id="ssubmit" value="insertColumns">
        <input name="sid" type="hidden" value="<?=$sid?>">
        <input name="mid" type="hidden" value="1">
        <br>
      </form>
      <br>
      <br>
      </td>
  </tr>
</table>
<?php }  ?>
</body>
</html>