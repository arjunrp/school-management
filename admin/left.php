<?php include ($_SERVER["DOCUMENT_ROOT"] . "/aqdar/includes/config.inc.php");
if (!loggedUser($sid)) { 
	redirect (HOST_URL ."/admin/include/messages.php?msg=relogin");
	exit();
}
$user_id = loggedUser($sid);
$contents = selectFrom("SELECT id,user_id FROM " . TBL_ADMIN_USERS . " WHERE user_id='$user_id'");
$user_type = getUserType($user_id);
?>
<html>
<title>News Admin Menu</title>
<head>
<link rel="stylesheet" href="<?=ADMIN_CSS_PATH?>interface.css" type="text/css">
<script language="JavaScript">
	top.mainFrame.document.location.href= "report.php?sid=<?=$sid;?>&mid=1";
</script>
</head>
<body class="adminDetailHeading">
<table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC">
  <tr> 
    <td align="center" valign="top" > 
    
	<table width="95%" align="center" cellpadding="0" cellspacing="0" class="LeftBorder">
        <br>
        <tr> 
          <td height="17" align="center" valign="middle" class="leftMenuBorder">User(s) 
            Administration</td>
        </tr>
        <tr> 
          <td valign="top" class="AdminMenuBackground"><table width="100%" border="0" cellpadding="0" cellspacing="2">
             
              <tr> 
                <td width="17" height="17"  align="center" class="Adminwhiteborder"><span class="leftArrowColor">&raquo;</span></td>
                <td height="17" class="Adminwhiteborder" ><a href="user_mgmt.php?sid=<?=$sid?>&mid=1" target="mainFrame" title="Users Management">Users Management 
                  </a></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <br>
    <table width="95%" align="center" cellpadding="0" cellspacing="0" class="LeftBorder">
        <br>
        <tr> 
          <td height="17" align="center" valign="middle" class="leftMenuBorder">Car Make(s) Admin</td>
        </tr>
        <tr> 
          <td valign="top" class="AdminMenuBackground"><table width="100%" border="0" cellpadding="0" cellspacing="2">
             
              <tr> 
                <td width="17" height="17"  align="center" class="Adminwhiteborder"><span class="leftArrowColor">&raquo;</span></td>
                <td height="17" class="Adminwhiteborder" ><a href="make_model_management/make_management.php?sid=<?=$sid?>&mid=1" target="mainFrame" title="Car Make(s) Management">Make(s) Management 
                  </a></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <br>
    <table width="95%" align="center" cellpadding="0" cellspacing="0" class="LeftBorder">
        <br>
        <tr> 
          <td height="17" align="center" valign="middle" class="leftMenuBorder">Car Model(s) Admin</td>
        </tr>
        <tr> 
          <td valign="top" class="AdminMenuBackground"><table width="100%" border="0" cellpadding="0" cellspacing="2">
             
              <tr> 
                <td width="17" height="17"  align="center" class="Adminwhiteborder"><span class="leftArrowColor">&raquo;</span></td>
                <td height="17" class="Adminwhiteborder" ><a href="make_model_management/model_management.php?sid=<?=$sid?>&mid=1" target="mainFrame" title="Car Model(s) Management">Model(s) Management 
                  </a></td>
              </tr>
            </table></td>
        </tr>
      </table>
      	<br>
      <table width="95%" align="center" cellpadding="0" cellspacing="0" class="LeftBorder">
        <br>
        <tr> 
          <td height="17" align="center" valign="middle" class="leftMenuBorder">User Ad(s) Management</td>
        </tr>
        <tr> 
          <td valign="top" class="AdminMenuBackground"><table width="100%" border="0" cellpadding="0" cellspacing="2">
             
              <tr> 
                <td width="17" height="17"  align="center" class="Adminwhiteborder"><span class="leftArrowColor">&raquo;</span></td>
                <td height="17" class="Adminwhiteborder" ><a href="user_ads/user_ads.php?sid=<?=$sid?>&mid=1" target="mainFrame" title="Spam/Abuse Management">User Ad(s)Management
                  </a></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <br>
    <table width="95%" align="center" cellpadding="0" cellspacing="0" class="LeftBorder">
        <br>
        <tr> 
          <td height="17" align="center" valign="middle" class="leftMenuBorder">Spam/Abuse Admin</td>
        </tr>
        <tr> 
          <td valign="top" class="AdminMenuBackground"><table width="100%" border="0" cellpadding="0" cellspacing="2">
             
              <tr> 
                <td width="17" height="17"  align="center" class="Adminwhiteborder"><span class="leftArrowColor">&raquo;</span></td>
                <td height="17" class="Adminwhiteborder" ><a href="spam_abuse/spam_abuse.php?sid=<?=$sid?>&mid=1" target="mainFrame" title="Spam/Abuse Management">Spam/Abuse Management 
                  </a></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <br>
    <table width="95%" align="center" cellpadding="0" cellspacing="0" class="LeftBorder">
        <br>
        <tr> 
          <td height="17" align="center" valign="middle" class="leftMenuBorder">News Admin</td>
        </tr>
        <tr> 
          <td valign="top" class="AdminMenuBackground"><table width="100%" border="0" cellpadding="0" cellspacing="2">
             
              <tr> 
                <td width="17" height="17"  align="center" class="Adminwhiteborder"><span class="leftArrowColor">&raquo;</span></td>
                <td height="17" class="Adminwhiteborder" ><a href="news/news_management.php?sid=<?=$sid?>&mid=1" target="mainFrame" title="News Management">News Management</a></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <br>
    <table width="95%" align="center" cellpadding="0" cellspacing="0" class="LeftBorder">
        <br>
        <tr> 
          <td height="17" align="center" valign="middle" class="leftMenuBorder">Home Page Ad(s) Admin</td>
        </tr>
        <tr> 
          <td valign="top" class="AdminMenuBackground"><table width="100%" border="0" cellpadding="0" cellspacing="2">
             
              <tr> 
                <td width="17" height="17"  align="center" class="Adminwhiteborder"><span class="leftArrowColor">&raquo;</span></td>
                <td height="17" class="Adminwhiteborder" ><a href="home_page_ads/home_page_ads.php?sid=<?=$sid?>&mid=1" target="mainFrame" title="Home Page Ad(s)">Home Page Ad(s) 
                  </a></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <br>
    <table width="95%" align="center" cellpadding="0" cellspacing="0" class="LeftBorder">
        <br>
        <tr> 
          <td height="17" align="center" valign="middle" class="leftMenuBorder">Poll(s) Admin</td>
        </tr>
        <tr> 
          <td valign="top" class="AdminMenuBackground"><table width="100%" border="0" cellpadding="0" cellspacing="2">
             
              <tr> 
                <td width="17" height="17"  align="center" class="Adminwhiteborder"><span class="leftArrowColor">&raquo;</span></td>
                <td height="17" class="Adminwhiteborder" ><a href="polls_management/pollsmanagement.php?sid=<?=$sid?>&mid=1" target="mainFrame" title="Poll(s) Management">Poll(s) Management
                  </a></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <br>
<table width="95%" align="center" cellpadding="0" cellspacing="0" class="LeftBorder">
        <br>
        <tr> 
          <td height="17" align="center" valign="middle" class="leftMenuBorder">Ask the Expert(s) Admin</td>
        </tr>
        <tr> 
          <td valign="top" class="AdminMenuBackground"><table width="100%" border="0" cellpadding="0" cellspacing="2">
             
              <tr> 
                <td width="17" height="17"  align="center" class="Adminwhiteborder"><span class="leftArrowColor">&raquo;</span></td>
                <td height="17" class="Adminwhiteborder" ><a href="ask_the_experts/category_management.php?sid=<?=$sid?>&mid=1" target="mainFrame" title="Category Management">Category Management
                  </a></td>
              </tr>
            </table></td>
        </tr>
        <tr> 
          <td valign="top" class="AdminMenuBackground"><table width="100%" border="0" cellpadding="0" cellspacing="2">
             
              <tr> 
                <td width="17" height="17"  align="center" class="Adminwhiteborder"><span class="leftArrowColor">&raquo;</span></td>
                <td height="17" class="Adminwhiteborder" ><a href="ask_the_experts/answers_management.php?sid=<?=$sid?>&mid=1" target="mainFrame" title="Ask the Expert(s) Management">Ask the Expert(s)
                  </a></td>
              </tr>
            </table></td>
        </tr>
      </table>
	  </td>
  </tr>
  <tr>
    <td align="center" valign="top" >&nbsp;</td>
  </tr>
</table>
</body>
</html>