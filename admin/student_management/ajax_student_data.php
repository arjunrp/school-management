<?php include ($_SERVER["DOCUMENT_ROOT"]."/aqdar/includes/config.inc.php"); ?>
<?php
	mysql_query("SET NAMES 'utf8'");
	mysql_query('SET CHARACTER SET utf8'); 

	$tbl_class_id = $_POST['tbl_class_id'];	
	//$tbl_class_id = "d149c08d38f7f06";

	$qry = "SELECT * FROM ".TBL_STUDENT." WHERE tbl_class_id='$tbl_class_id' AND is_active='Y' ";
	$data = SelectMultiRecords($qry);

if (count($data) >0 ) {
?>
<table width="100%" border="0" cellspacing="0" cellpadding="2">
      <tr>
        <td style="border-bottom:2px solid #CCC; border-top:2px solid #CCC; font-weight:bold" width="5%">&nbsp;</td>
        <td style="border-bottom:2px solid #CCC; border-top:2px solid #CCC; font-weight:bold" width="5%">&nbsp;</td>
        <td style="border-bottom:2px solid #CCC; border-top:2px solid #CCC; font-weight:bold" width="5%">&nbsp;</td>
        <td width="25%" align="center" valign="middle" style="border-bottom:2px solid #CCC; border-top:2px solid #CCC; font-weight:bold">Name</td>
        <td width="30%" align="center" valign="middle" style="border-bottom:2px solid #CCC; border-top:2px solid #CCC; font-weight:bold">Email</td>
        <td align="center" valign="middle" style="border-bottom:2px solid #CCC; border-top:2px solid #CCC; font-weight:bold">Date of Birth [mm-dd-yyyy]</td>
      </tr>
    <?php
        for ($i=0; $i<count($data); $i++) {
    
            $id = $data[$i]["id"];
            $tbl_student_id = $data[$i]["tbl_student_id"];
            $first_name = $data[$i]["first_name"];
            $first_name_ar = $data[$i]["first_name_ar"];
            $last_name = $data[$i]["last_name"];
            $last_name_ar = $data[$i]["last_name_ar"];
            $mobile = $data[$i]["mobile"];
            $dob_month = $data[$i]["dob_month"];
            $dob_day = $data[$i]["dob_day"];
            $dob_year = $data[$i]["dob_year"];
            $gender = $data[$i]["gender"];
            $email = $data[$i]["email"]; if (trim($email) == "") { $email = "Not Specified";}
            $added_date = $data[$i]["added_date"];
            $is_active = $data[$i]["is_active"];
            
            $dob = sprintf('%02s',$dob_month)."-".sprintf('%02s',$dob_day)."-".$dob_year;
			
			$bgcol = "#CCC";
			
			if ($i%2 == 0) {
				$bgcol = "#FFF";
			}
    ?>
      <tr bgcolor="<?=$bgcol?>">
        <td width="5%" align="left" valign="top">&nbsp;</td>
        <td width="5%" align="left" valign="top"><?php echo $i+1?>. 	</td>
        <td width="5%" align="left" valign="top"><input name="all_students[]" id="all_students[]" type="checkbox" value="<?=$tbl_student_id?>" onclick="javascript:selected_students()" /></td>
        <td>
            <div class="txt_en"><?=$first_name?>  <?=$last_name?></div>
            <div class="txt_ar"><?=$first_name_ar?>  <?=$last_name_ar?></div>
        </td>
        <td align="center"><?=$email?></td>
        <td align="center" valign="middle"><?=$dob?></td>
      </tr>
    <?php	
        }
    ?>
</table>
<?php
}//if (count($data) >0 ) {
?>
