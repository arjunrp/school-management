<?php	
	include ($_SERVER["DOCUMENT_ROOT"]."/dc_nibras/includes/config.inc.php");
	$sid = $_REQUEST["sid"];
	if (!loggedUser($sid)) { 
		redirect (HOST_URL ."/admin/include/messages.php?msg=relogin");
		exit();
	}
	
	$LIB_CLASSES_FOLDER = $_SERVER['DOCUMENT_ROOT']."/dc_nibras/lib/classes/";
	include($LIB_CLASSES_FOLDER."Paging.php");
	
	foreach($_REQUEST as $key => $value){
		$$key = $value;
	} 

	$q = stripslashes($q);	
	/*	ACTIVATE/INACTIVATE RECORD	*/
	if($ssubmit=="recordActiveInactive") {
		if(isset($show)){
				for ($j=0; $j<count($EditBox); $j++){ 
					$Querry = "UPDATE ". TBL_SUGGESTIONS ." SET is_active='Y' WHERE tbl_suggestions_id='$EditBox[$j]'";			
					update($Querry);
					$mid=1;	
					$MSG = "Selected record(s) have been activated successfully.";
				}
		}
		if(isset($hide)){
				for ($j=0; $j<count($EditBox); $j++){ 
					$Querry = "UPDATE ". TBL_SUGGESTIONS ." SET is_active='N' WHERE tbl_suggestions_id='$EditBox[$j]'";			
					update($Querry);
					$mid=1;	
					$MSG = "Selected record(s) have been deactivated successfully.";
				}
			}
		if(isset($delete)){$mid = 4;}
	}
	
	/* DELETE RECORD */
	if($ssubmit == "recordDel"){
		for($i=0;$i<count($EditBox);$i++){
			$tbl_suggestions_id = $EditBox[$i];
			$qry = "DELETE FROM ".TBL_SUGGESTIONS." WHERE `id` = '$EditBox[$i]'";
			//echo $qry;
			deleteFrom($qry);
			$mid = 1;
			$MSG = "Selected record(s) has been deleted successfully.";
		}
	}

	if ($by == "") {
		$by = "ASC";
	}
?>
<html>
<title>Admin (Designation Management)</title>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<?=HOST_URL?>/admin/css/interface.css" type=text/css rel=stylesheet>
<style type="text/css">
	select {
		width:200px;
	}

	.col_head {
		color:#005B90;
	}
	
	.input_text {
		border:1px solid #CCC;
		width: 220px;
		height:22px;
	}
</style>
<script  src="<?=JS_PATH?>/jquery-1.3.2.min.js"></script>
<script language="JavaScript">
	
	$(document).ready(function(){
	});
	
	/* CUSTOM JS - MODIFY AS PER NEED */
	function valueCheckedForUsersManagement(){
		var ml = document.frmDirectory;
		var len = ml.elements.length;
		for (var i = 0; i < len; i++){
			if (document.frmDirectory.elements[i].checked){
				return true;
			}
		}
		 alert ("Select at least one record.");
		 return false;
	}
	
	function CheckAll(){
		var ml = document.frmDirectory;
		var len = ml.elements.length;
		if (document.frmDirectory.AC.checked==true) {
			 for (var i = 0; i < len; i++) {
				document.frmDirectory.elements[i].checked=true;
			 }
		} else {
			  for (var i = 0; i < len; i++)  {
				document.frmDirectory.elements[i].checked=false;
			  }
		}
	}
	
	function UnCheckAll() {
		var ml = document.frmDirectory;
		var len = ml.elements.length;
		var count=0; var checked=0;
			for (var i = 0; i < len; i++) {	       
				if ((document.frmDirectory.elements[i].type=='checkbox') && (document.frmDirectory.elements[i].name != "AC")) {
					count = count + 1;
					if (document.frmDirectory.elements[i].checked == true){
						checked = checked + 1;
					}
				}
			 }
			 
		if (checked == count) {
			 document.frmDirectory.AC.checked = true;
		} else {
			document.frmDirectory.AC.checked = false;
		}
	}
	
	function validateForm() {
		return true;
		if(isFirstName() && isLastName() && isUserType() && isEmail() && isUserID()
				&& isPassword() && isRetypePassword() && isPasswordSame()) {
				return true;
		} else {
			return false;
		}
	}
	
	function isFirstName() {
		var str = document.frmRecord.file_name_updated.value;
		if (str == "") {
			alert("\nThe First name field is blank. Please write your First name.");
			document.frmRecord.file_name_updated.value="";
			document.frmRecord.file_name_updated.focus();
			return false;
		}
		if (!isNaN(str)) {
			alert("\nPlease write your First name.");
			document.frmRecord.file_name_updated.value="";
			document.frmRecord.file_name_updated.select();
			document.frmRecord.file_name_updated.focus();
			return false;
		}
		for (var i = 0; i < str.length; i++) {
			var ch = str.substring(i, i + 1);
			if  ((ch <"A" || ch > "z" ) && (ch !=" ")){
				alert("\n Please enter valid First name.");
				document.frmRecord.file_name_updated.select();
				document.frmRecord.file_name_updated.focus();
				return false;
			}
		}
		return true;
	}
	
	function isLastName() {
		var str = document.frmRecord.file_name_updated.value;
		if (str == "") {
			alert("\nThe Last name field is blank. Please write your Last name.");
			document.frmRecord.file_name_updated.value="";
			document.frmRecord.file_name_updated.focus();
			return false;
		}
		if (!isNaN(str)) {
			alert("\nPlease write your Last name.");
			document.frmRecord.file_name_updated.value="";
			document.frmRecord.file_name_updated.select();
			document.frmRecord.file_name_updated.focus();
			return false;
		}
		for (var i = 0; i < str.length; i++) {
			var ch = str.substring(i, i + 1);
			if  ((ch <"A" || ch > "z" ) && (ch !=" ")){
				alert("\n Please enter valid Last name.");
				document.frmRecord.file_name_updated.select();
				document.frmRecord.file_name_updated.focus();
				return false;
			}
		}
		return true;
	}
	
	
	function isUserType() {
		var str = document.frmRecord.user_type.value;
		if (str == "") {
			alert("\nPlease select user type.");
			document.frmRecord.user_type.focus();
			return false;
		}
		return true;
	}
	
	function isEmail() {
		var str = document.frmRecord.email.value;
		if (str == "") {
			alert("\nThe Email field is blank.Please enter your valid Email.");
			document.frmRecord.email.focus();
			return false;
		}
		if (!isNaN(str)) {
			alert("\nPlease write your correct Email address");
			document.frmRecord.email.select();
			document.frmRecord.email.focus();
			return false;
		}
		if(str.indexOf('@', 0) == -1) {
			alert("\nIt seems that your email address is not valid.");
			document.frmRecord.email.select();
			document.frmRecord.email.focus();
			return false;
		}
		return true;
	}
	
	function isUserID() {
		var str = document.frmRecord.tbl_suggestions_id.value;
		if ( str=="" ) {
			alert("\nThe User-ID is blank. Please write your User-ID.");
			document.frmRecord.tbl_suggestions_id.focus();
			return false;
		}
		if (str.length < 7 ) {
			alert("\nThe User-ID should be greater than 5 Characters.");
			document.frmRecord.tbl_suggestions_id.focus();
			document.frmRecord.tbl_suggestions_id.select();
			return false;
		}
			
		if (!isNaN(str)) {
			alert("\nThe User-ID have only letters & digits, Please re-enter your User-ID");
			document.frmRecord.tbl_suggestions_id.select();
			document.frmRecord.tbl_suggestions_id.focus();
			return false;
		}
	
		for (var i = 0; i < str.length; i++) {
			var ch = str.substring(i, i + 1);
			if  ((ch < "a" || ch > "z") && (ch < "0" || "9" < ch) ) {
				alert("\nThe User-ID have only letters in lower case & digits, Please re-enter your User-ID");
				document.frmRecord.tbl_suggestions_id.select();
				document.frmRecord.tbl_suggestions_id.focus();
				return false;
			}
		}
		return true;
	}
	
	function isPassword() {
		var str = document.frmRecord.password.value;
		if (str == "") {
			alert("\nThe Password field is blank. Please enter Password.");
			document.frmRecord.password.focus();
			return false;
		}
			if (str.length < 7) {
			alert("\nThe Password should be greater than 6 Characters.");
			document.frmRecord.password.focus();
			document.frmRecord.password.select();
			return false;
		}
		return true;
	}
	
	function isRetypePassword() {
		var str = document.frmRecord.confirm_password.value;
		if (str == "") {
			alert("\nThe Confirm Password field is blank. Please retype password.");
			document.frmRecord.confirm_password.focus();
			return false;
		}
		return true;
	}
	
	function isPasswordSame() {
		var str1 = document.frmRecord.password.value;
		var str2 = document.frmRecord.confirm_password.value;
		if (str1 != str2) {
			alert("\nPassword mismatch, Please retype same passwords in both fields.");
			document.frmRecord.confirm_password.focus();
			return false;
		}
		return true;
	}
</script>
</head>
<?php if (!$offset || $offset<0)  { $offset =0;} ?>
<?php if (!$LIKE)  { $LIKE = "LIKE";} ?>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<!--	DEFAULT FIRST SCREEN	-->
<?php if($mid=="1"){ ?>
<table width="100%" height="570" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr>
    <td height="30" valign="center" class="adminDetailHeading">&nbsp;Suggestions Management </td>
  </tr>
  <tr align="center" >
    <td height="538" align="center" valign="top" >
    
   
    
    
    <form name="form1" method="post" action="">
        <br>
        <table width="80%" border="0" align="center" cellpadding="0" cellspacing="1" >
          <tr >
            <td height="22" align="left" valign="middle" class="adminDetailHeading">&nbsp;Search 
              Criteria</td>
          </tr>
          <tr >
            <td align="center" valign="top" class="bordLightBlue"><table width="100%" border="0" cellspacing="3" cellpadding="2">
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td width="32%">&nbsp;</td>
                  <td width="48%"><a href="<?=HOST_URL?>/admin/suggestions_management/suggestions_management.php?mid=<?=$mid?>&sid=<?=$sid?>&tbl_kiosk_templates_id_outer=<?=$tbl_kiosk_templates_id_outer?>&tbl_kiosk_templates_id_inner=<?=$tbl_kiosk_templates_id_inner?>"><span class="detailLinkColor">All 
                    Records</span></a>: or&nbsp;</td>
                  <td width="20%"><i></i></td>
                </tr>
                <tr>
                  <td colspan="3"><hr width="80%" size="1"></td>
                </tr>
                <tr>
                  <td align="right" valign="top" ><span class="searchText">Field:&nbsp;</span></td>
                  <td align="left" valign="top"><select name="field" id="field">
                      <option value="suggestion_name" <?PHP if ($field == "suggestion_name"){echo "selected";}?>> Name </option>
                      <option value="suggestion_email" <?PHP if ($field == "suggestion_email"){echo "selected";}?>> Email </option>
                      <option value="suggestion_comments" <?PHP if ($field == "suggestion_comments"){echo "selected";}?>> Comments </option>
                      <option value="is_active"<?PHP if($field == "is_active"){echo "selected";}?>>Status 
                      e.g Y=Activate , N=Deactivate</option>
                      <option value="added_date" <?PHP if ($field == "added_date"){echo "selected";}?>> Date e.g [YYYY-MM-DD] </option>
                    </select></td>
                  <td><i></i></td>
                </tr>
                <tr>
                  <td align="right" valign="top"><span class="searchText">Condition:&nbsp;</span></td>
                  <td align="left" valign="top"><?php
						if (!$LIKE) {
							$LIKE = "LIKE";
						}
					?>
                    <select name="LIKE" id="LIKE">
                      <option value="LIKE" <?PHP if ($LIKE == "LIKE"){echo "selected";}?>>LIKE</option>
                      <option value="=" <?PHP if (($LIKE == "=") || ($LIKE == "")){echo "selected";}?>>Equal 
                      To</option>
                      <option value="!=" <?PHP if ($LIKE == "!="){echo "selected";}?>>Not 
                      Equal To</option>
                      <option value="<" <?PHP if ($LIKE == "<"){echo "selected";}?>>Less 
                      Than</option>
                      <option value=">" <?PHP if ($LIKE == ">"){echo "selected";}?>>Greater 
                      Than</option>
                      <option value="<=" <?PHP if ($LIKE == "<="){echo "selected";}?>>Less 
                      Than or Equal To</option>
                      <option value=">=" <?PHP if ($LIKE == ">="){echo "selected";}?>>Greater 
                      Than or Equal To</option>
                    </select></td>
                  <td><i></i></td>
                </tr>
                <tr>
                  <td align="right" valign="top"><span class="searchText">Search 
                    Name:&nbsp;</span></td>
                  <td align="left" valign="top"><input name="q" type="text" id="q" value="<?php echo htmlspecialchars($q);?>" size="40"></td>
                  <td><i></i></td>
                </tr>
                <tr>
                  <td align="right" valign="top"><span class="searchText">Order 
                    By:&nbsp;</span></td>
                  <td align="left" valign="top"><select name="by" id="by">
                      <option value="ASC" <?PHP if (($by == "ASC") || ($by == "")) { echo "selected";}?>>ASCENDING</option>
                      <option value="DESC" <?PHP if ($by == "DESC"){ echo "selected";}?>>DESCENDING</option>
                    </select>
                    &nbsp;</td>
                  <td><i></i></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td align="center"><input name="mid" type="hidden" value="1">
                    <input name="offset" type="hidden" value="0">
                    <input name="Search" type="submit" id="Search" value="Search" class="flat">
                    <input name="ssubmit" type="hidden" id="ssubmit" value="" class="flat">
                    &nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
              </table></td>
          </tr>
        </table>
      </form>
      <br>
      <span class="msgColor"><?php echo $MSG;?></span><br>
      <?php 		
		$q = addslashes($q);
		$CountRec = "SELECT * FROM ".TBL_SUGGESTIONS." WHERE 1 ";		
		$Query = "SELECT * FROM ".TBL_SUGGESTIONS." WHERE 1 ";
		
		if($field && !empty($q)){	    	
			if($LIKE=="LIKE"){
					$CountRec .= " AND $field $LIKE '%$q%' ";
					$Query .= " AND  $field $LIKE '%$q%' ";
			}else{
					$CountRec .= " AND  $field $LIKE '$q' ";	
					$Query .= " AND  $field $LIKE '$q' ";
			}
		}	
		 
		 //echo $Query;
		 $Query .= " ORDER BY suggestion_name $by ";
		 $Query .=" LIMIT $offset, ".TBL_SUGGESTIONS_PAGING;
   	    // echo $Query;
		 $q = stripslashes($q);
		 $total_record = CountRecords($CountRec);
		 $data = SelectMultiRecords($Query);
		  		 if ($total_record =="")
					   echo '<span class="msgColor">'.MSG_NO_RECORD_FOUND."</span>";
					else{	
					    echo '<span class="msgColor">';
					    echo " <b> ". $total_record ." </b> Record(s) found. Showing <b>";
						if ($total_record>$offset){
							echo $offset+1;
							echo " </b>to<b> ";
							if ($offset >= $total_record - TBL_SUGGESTIONS_PAGING)	{ 
								  echo $total_record; 
							}else { 
							   	echo $offset + TBL_SUGGESTIONS_PAGING ;
							}
						}else{ 
							echo $offset+1;
							echo "</b> - ". $total_record;
							echo " to ". $total_record ." Record(s) ";		
						}
						echo "</b>.</span>";	
					}
	   ?>
      <?php  if ($total_record !=""){?>
      <table width="98%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="18"></td>
        </tr>
        <tr>
          <td align="center" valign="top" class="adminDetailHeading"><table width="100%" border="0" align="center"  cellpadding="2" cellspacing="1">
              <form name="frmDirectory" onSubmit="return valueCheckedForUsersManagement()">
                <tr align="center" >
                  <td width="15%" align="center" class="adminDetailHeading"> Name</td>
                  <td width="15%" align="center" class="adminDetailHeading">Email</td>
                  <td width="38%" align="center" class="adminDetailHeading">Suggestions / Comments</td>
                  <td width="10%" height="24" align="center" class="adminDetailHeading">Date</td>
                  <td width="6%" height="24" align="center" class="adminDetailHeading"><input name="AC" type="checkbox" onClick="CheckAll();"></td>
                </tr>
                <?php
				 for($i=0;$i<count($data);$i++){
						$id = $data[$i]["id"];
						$tbl_suggestions_id = $data[$i]["tbl_suggestions_id"];
						$suggestion_name = $data[$i]["suggestion_name"];
						$suggestion_email = $data[$i]["suggestion_email"];
						$suggestion_comments = $data[$i]["suggestion_comments"];
						$added_date = $data[$i]["added_date"];				
						$is_active = $data[$i]["is_active"];
						
						$suggestion_name = stripslashes($suggestion_name);
						$suggestion_email = stripslashes($suggestion_email);
						$suggestion_comments = stripslashes($suggestion_comments);
				?>
                <tr valign="middle" style="background-color:#FFF">
                  <td align="left" >&nbsp;<span style="margin:5px"><?=$suggestion_name?>
                  </span></td>
                  <td align="center" ><span style="margin:5px">
                    <?=$suggestion_email?>
                  </span></td>
                  <td align="left" ><span style="margin:5px">
                    <?=$suggestion_comments?>
                  </span></td>
                  <td height="20" align="center" ><?php echo date("d M, Y H:i:s",strtotime($added_date));?></td>
                  <td align="center"><input name="EditBox[]" type="checkbox" value="<?=$id?>"></td>
                </tr>
                <?php }?>
                <tr>
                  <td height="20" colspan="6" align="center" valign="middle" bgcolor="#FFFFFF"><input name="reset" type="reset" class="flat"  value="Reset">
<!--                    <input name="show" type="submit" class="flat" id="ssubmit" value="Active">
                    <input name="hide" type="submit" class="flat"  value="Deactive">
-->                    <input name="delete" type="submit" class="flat"  value="Delete">
                    <input name="ssubmit" type="hidden" id="ssubmit" value="recordActiveInactive">
                    <input name="mid" type="hidden" id="mid2" value="1">
                    <input name="sid" type="hidden" id="sid" value="<?=$sid?>">
                    <input name="offset" type="hidden" id="offset" value="<?=$offset?>">
                    <input name="q" type="hidden" id="q" value="<? echo htmlspecialchars($q);?>">
                    <input name="tbl_suggestions_id" type="hidden" value="<?=$tbl_suggestions_id?>">
                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>">
                    <input name="by" type="hidden" id="by" value="<?=$by?>">
                    <input name="field" type="hidden" id="field" value="<?=$field?>">
                    <input name="LIKE" type="hidden" value="<?=$LIKE?>"></td>
                </tr>
                <tr align="right">
                  <td height="20" colspan="6" valign="middle" bgcolor="#FFFFFF"><?php 
					   if ($total_record != "" && $total_record>TBL_SUGGESTIONS_PAGING) {
							$url = "suggestions_management.php?mid=1&sid=$sid&field=$field&q=$q&LIKE=$LIKE&by=$by";
							$Paging_object = new Paging();
							$Paging_object->paging_new_style_final($url,$offset,$clicked_link,$total_record,TBL_SUGGESTIONS_PAGING);
						}
					?></td>
                </tr>
              </form>
            </table></td>
        </tr>
      </table>
      <br>
      <?php } ?>
      </td>
  </tr>
</table>
<!--	RECORD DELETE SCREEN	-->
<?php }if($mid=="4"){ ?>
<table width="100%" height="380" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr>
    <td height="30" valign="center" class="adminDetailHeading">&nbsp;Suggestion Management 
    [Del Confirmation]</td>
  </tr>
  <tr align="center" >
    <td valign="top" ><br>
      <span class="msgColor"> Are you sure you want to delete the selected record(s) </span> <br>
      <table width="98%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center" valign="top" class="adminDetailHeading"><table width="100%" border="0" align="center"  cellpadding="2" cellspacing="1" >
              <form name="frmDirectory" onSubmit="return valueCheckedForUsersManagement();">
                <tr >
                  <td width="15%" align="center"  class="adminDetailHeading"> Name</td>
                  <td width="15%" align="center"  class="adminDetailHeading">Email</td>
                  <td width="30%" align="center"  class="adminDetailHeading">Suggestions / Comments</td>
                  <td width="10%" align="center"  class="adminDetailHeading">Date</td>
                  <td width="10%" align="center"  class="adminDetailHeading">Status</td>
                  <td width="4%" height="24" align="center"  class="adminDetailHeading"><input name="AC" type="checkbox" id="AC" onClick="CheckAll();" checked></td>
                </tr>
                <?php 
			for($i=0;$i<count($EditBox);$i++){
				$qry ="SELECT * FROM ".TBL_SUGGESTIONS." WHERE id='$EditBox[$i]'";
				$data = selectFrom($qry);
				$id = $data["id"];
				$suggestion_name = $data["suggestion_name"];
				$suggestion_email = $data["suggestion_email"];
				$suggestion_comments = $data["suggestion_comments"];
				$added_date = $data["added_date"];				
				$is_active = $data["is_active"];
				
				$suggestion_name = stripslashes($suggestion_name);
				$suggestion_email = stripslashes($suggestion_email);
				$suggestion_comments = stripslashes($suggestion_comments);
				?>
                <tr valign="middle" bgcolor="#FFFFFF">
                  <td align="left">&nbsp;<span style="margin:5px">
                    <?=$suggestion_name?>
                  </span></td>
                  <td align="center"><span style="margin:5px">
                    <?=$suggestion_email?>
                  </span></td>
                  <td align="left"><span style="margin:5px">
                    <?=$suggestion_comments?>
                  </span></td>
                  <td height="20" align="center"><?php echo date("d M, Y H:i:s",strtotime($added_date));?></td>
                  <td align="center"><?php 
		  			if($is_active == 'Y'){?>
                    <img src="<?=IMG_PATH?>/yes.gif">
                    <?php } else {?>
                    <img src="<?=IMG_PATH?>/no.gif">
                    <?php }  ?></td>
                  <td height="20" align="center"><input name="EditBox[]" type="checkbox" id="EditBox[]" onClick="UnCheckAll();" value="<?=$id?>" checked></td>
                </tr>
                <?php }?>
                <tr>
                  <td height="20" colspan="7" align="center" valign="middle" bgcolor="#FFFFFF" ><input name="Button" type="button" class="flat" id="hide" value="I am not sure" onClick="javascript:history.back();">
                    <input name="show" type="submit" class="flat" id="show" value="Yes I am sure">
                    <input name="sid" type="hidden" id="sid" value="<?=$sid;?>">
                    <input name="offset" type="hidden" id="offset" value="<?=$offset;?>">
                    <input name="mid" type="hidden" id="mid" value="1">
                    <input name="ssubmit" type="hidden" id="show" value="recordDel">
                    <input name="q" type="hidden" id="q" value="<?php echo htmlspecialchars($q);?>">
                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>">
                    <input name="by" type="hidden" id="by" value="<?=$by?>">
                    <input name="field" type="hidden" id="field" value="<?=$field?>">
                    <input name="LIKE" type="hidden" value="<?=$LIKE?>"></td>
                </tr>
              </form>
            </table></td>
        </tr>
      </table>
  </tr>
</table>
<?php } ?>

<script language="javascript">

	var host = '<?=HOST_URL?>';
	var lan = 'ar';
	
	function get_room_names_ajax(court_name) {
		var xmlHttp, rnd, url;
		rnd = Math.floor(Math.random()*11);
		
		try{		
			xmlHttp = new XMLHttpRequest(); 
		}catch(e) {
			try{
				xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
			}catch(e) {
				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
		}
		
		//AJAX response
		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
				//alert(xmlHttp.responseText);
				$("#court_name_container").html(xmlHttp.responseText);
			}
		}
			
		//Sending AJAX request
		url = host + "/"+lan+"/court/get_room_names_ajax/"+court_name;
		xmlHttp.open("POST",url,true);
		xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlHttp.send("rnd="+rnd);
		
	}//function
</script>
</body>
</html>