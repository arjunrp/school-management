<?php	

    include("../header.php"); 

	$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];

	

	$tbl_school_id_sess = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];

	$LIB_CLASSES_FOLDER = $_SERVER['DOCUMENT_ROOT']."/aqdar/lib/classes/";

	include($LIB_CLASSES_FOLDER."Paging.php");



	$q = stripslashes($q);	

    

	// Image dimensions

	$l_max_x = 60;

	$l_max_y = 60;

	

	/*	INSERT RECORD	*/

	if($_REQUEST['ssubmit']=="insert_point_category") {

        $qry = "SELECT * FROM ".TBL_BEHAVIOUR_POINTS." WHERE point_name_en = '".trim($point_name_en)."' AND tbl_school_id= '".$tbl_school_id_sess."' ";

		if (isExist($qry)) {

			$MSG="Point information is already exists!";

		} else {

	

			$point_name_en = addslashes($point_name_en);

			$tbl_point_category_id = getMD5_ID();

			$qry = "

				INSERT INTO ".TBL_BEHAVIOUR_POINTS." (

				`tbl_point_category_id` ,

				`point_name_en` ,

				`point_name_ar` ,

				`behaviour_point` ,

				`is_active` ,

				`added_date` ,

				`tbl_school_id`

				)

				VALUES (

				'$tbl_point_category_id', '$point_name_en', '$point_name_ar','$behaviour_point', 'Y', NOW(), '$tbl_school_id'

				)";

			//echo $qry."<br><br>"; exit;

			$MSG = "Point information has been saved successfully.";

			insertInto($qry);

		}

		

	}

	

	/*	EDIT RECORD	*/

	if($ssubmit=="editRecord") {

		

		$qry = "SELECT * FROM ".TBL_BEHAVIOUR_POINTS." WHERE tbl_point_category_id != '".trim($_REQUEST['tbl_point_category_id'])."' AND point_name_en = '".$point_name_en."' AND tbl_school_id= '".$tbl_school_id_sess."' ";

		if (isExist($qry)) {

			$MSG="Point information is already exists!";

			$mid = 3;

		} else {

			$qry = "UPDATE ".TBL_BEHAVIOUR_POINTS." SET point_name_en='$point_name_en', point_name_ar='$point_name_ar', behaviour_point='$behaviour_point', is_active='$is_active' WHERE tbl_point_category_id='$tbl_point_category_id'";

			//echo $qry;

			update($qry);

			$mid = 1;

		}

	}

	

	/*	ACTIVATE/INACTIVATE RECORD	*/

	if($ssubmit=="recordActiveInactive") {

		if(isset($show)){

				for ($j=0; $j<count($EditBox); $j++){ 

					$Querry = "UPDATE ". TBL_BEHAVIOUR_POINTS ." SET is_active='Y' WHERE tbl_point_category_id='$EditBox[$j]'";			

					//echo $Querry;

					update($Querry);

					$mid=1;	

					$MSG = "Selected record(s) have been activated successfully.";

				}

		}

		if(isset($hide)){

				for ($j=0; $j<count($EditBox); $j++){ 

					$Querry = "UPDATE ". TBL_BEHAVIOUR_POINTS ." SET is_active='N' WHERE tbl_point_category_id='$EditBox[$j]'";			

					update($Querry);

					//echo $Querry;

					$mid=1;	

					$MSG = "Selected record(s) have been deactivated successfully.";

				}

			}

		if(isset($delete)){$mid = 4;}

	}

	

	/* DELETE RECORD */

	if($ssubmit == "recordDelInformation"){

		for($i=0;$i<count($EditBox);$i++){

			$qry = "SELECT * FROM ".TBL_BEHAVIOUR_POINTS." WHERE `tbl_point_category_id` = '$EditBox[$i]'";

			//echo $qry."<br>";

			$data = selectFrom($qry);

			

			deleteFrom("DELETE FROM ".TBL_BEHAVIOUR_POINTS." WHERE `tbl_point_category_id` = '$EditBox[$i]'");

			$mid = 1;

			$MSG = "Selected record(s) has been deleted successfully.";

		}

	}



	if ($by == "") {

		$by = "ASC";

	}

?>

<title>Admin (Points Management)</title>

<style type="text/css">

	select {

		width:200px;

	}

	.col_head {

		color:#005B90;

	}

	.input_text {

		border:1px solid #CCC;

		width: 160px;

		height:22px;

	}

	.input_text_ar {

		font-size:13px;

		text-align:right;

		direction:rtl;

		font-family:Tahoma, Geneva, sans-serif;

	}

</style>



<script  src="<?=JS_PATH?>/jquery-1.3.2.min.js"></script>

<script language="JavaScript">

	

	$(document).ready(function(){

	});

	

	/* CUSTOM JS - MODIFY AS PER NEED */

	function valueCheckedForUsersManagement(){

		var ml = document.frmDirectory;

		var len = ml.elements.length;

		for (var i = 0; i < len; i++){

			if (document.frmDirectory.elements[i].checked){

				return true;

			}

		}

		 alert ("Select at least one record.");

		 return false;

	}

	

	function CheckAll(){

		var ml = document.frmDirectory;

		var len = ml.elements.length;

		if (document.frmDirectory.AC.checked==true) {

			 for (var i = 0; i < len; i++) {

				document.frmDirectory.elements[i].checked=true;

			 }

		} else {

			  for (var i = 0; i < len; i++)  {

				document.frmDirectory.elements[i].checked=false;

			  }

		}

	}

	

	function UnCheckAll() {

		var ml = document.frmDirectory;

		var len = ml.elements.length;

		var count=0; var checked=0;

			for (var i = 0; i < len; i++) {	       

				if ((document.frmDirectory.elements[i].type=='checkbox') && (document.frmDirectory.elements[i].name != "AC")) {

					count = count + 1;

					if (document.frmDirectory.elements[i].checked == true){

						checked = checked + 1;

					}

				}

			 }

			 

		if (checked == count) {

			 document.frmDirectory.AC.checked = true;

		} else {

			document.frmDirectory.AC.checked = false;

		}

	}

	

	function validateForm() {

		if(isPointInfo() && isPointInfoArabic() && isBehaviourPoints()) {

				return true;

		} else {

			return false;

		}

	}

	

	function isPointInfo() {

		var str = document.frmRecord.point_name_en.value;

		if (str == "") {

			alert("\nPoint information field is blank. Please write point information.");

			document.frmRecord.point_name_en.value="";

			document.frmRecord.point_name_en.focus();

			return false;

		}

		if (!isNaN(str)) {

			alert("\nPlease write your Point Information.");

			document.frmRecord.point_name_en.value="";

			document.frmRecord.point_name_en.select();

			document.frmRecord.point_name_en.focus();

			return false;

		}

		for (var i = 0; i < str.length; i++) {

			var ch = str.substring(i, i + 1);

			if  ((ch <"A" || ch > "z" ) && (ch !=" ")){

				alert("\n Please enter valid Point Inforamtion.");

				document.frmRecord.point_name_en.select();

				document.frmRecord.point_name_en.focus();

				return false;

			}

		}

		return true;

	}

	

function isPointInfoArabic() {

		var str = document.frmRecord.point_name_ar.value;

		if (str == "") {

			alert("\n Point Information in arabic field is blank. Please write card Point Information in arabic field.");

			document.frmRecord.point_name_ar.value="";

			document.frmRecord.point_name_ar.focus();

			return false;

		}

		if (!isNaN(str)) {

			alert("\nPlease write your Point Information in arabic field.");

			document.frmRecord.point_name_ar.value="";

			document.frmRecord.point_name_ar.select();

			document.frmRecord.point_name_ar.focus();

			return false;

		}

		

		return true;

	}

	

	function isBehaviourPoints() {

		var str = document.frmRecord.behaviour_point.value;

		if (str == "") {

			alert("\nPoints field is blank. Please enter points.");

			document.frmRecord.behaviour_point.value="";

			document.frmRecord.behaviour_point.focus();

			return false;

		}

		

		return true;

	}

</script>

<?php if (!$offset || $offset<0)  { $offset =0;} ?>

<?php if (!$LIKE)  { $LIKE = "LIKE";} ?>

<script type="text/javascript" src="<?=JS_COLOR_PATH?>/jscolor.js"></script>

<!--	DEFAULT FIRST SCREEN	-->

<?php if($mid=="1"){ ?>

<table width="100%" height="570" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">

  <tr>

    <td height="30" valign="center" class="adminDetailHeading">&nbsp;Points Management </td>

  </tr>

  <tr align="center" >

    <td height="538" align="center" valign="top"  >

    <br /><br />

     <form name="frmRecord" id="frmRecord" method="post" action=""  onSubmit="return validateForm();" enctype="multipart/form-data">

        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >

          <tr >

            <td height="22" align="left" valign="middle"  class="adminDetailHeading" ><strong>&nbsp;Add  Points</strong></td>

          </tr>

          <tr >

            <td align="center" valign="top" class="bordLightBlue">           

            <table width="90%" border="0" align="center" cellpadding="2" cellspacing="2" >

                 <tr align="left" valign="top" >

                  <td colspan="3"><span class="msgColor">Fields with * are mandatory.</span></td>

                </tr>

                <tr align="left" valign="top" >

                  <td align="left" width="35%" >Point Information [En]<span class="msgColor">*</span><br/><input name="point_name_en" type="text" class="input_text" id="point_name_en" size="40"></td>

                  <td align="left" width="35%" >Point Information [Ar]<span class="msgColor">*</span><br/><input name="point_name_ar" type="text" class="input_text input_text_ar" id="point_name_ar" size="40"></td>

                  <td width="30%" >Points<span class="msgColor">*</span>&nbsp;( + or - sign with point) <br /><input name="behaviour_point" type="text" class="input_text" id="behaviour_point" size="40"></td>

                </tr>

               

              

                <tr valign="middle" >

                

                  <td align="left" width="35%"  colspan="3"><input type="hidden" name="tbl_point_category_id" value="<?=$tbl_point_category_id?>">

                    <input type="reset" name="Reset" value="Reset" class="flat">

                    <input name="save" type="submit" class="flat" id="ssubmit"  value="Submit">

                    <input name="ssubmit" type="hidden" id="ssubmit" value="insert_point_category">

                    <input name="sid" type="hidden" value="<?=$sid?>">

                    <input name="mid" type="hidden" value="1">

                    <input name="offset" type="hidden" id="offset" value="<?=$offset;?>">

                    <input name="q" type="hidden" id="q" value="<?php echo htmlspecialchars($q);?>">

                    <input name="by" type="hidden" id="by" value="<?=$by?>">

                    <input name="field" type="hidden" id="fiel3" value="<?=$field?>">

                    <input name="LIKE" type="hidden" value="<?=$LIKE?>"></td>

                </tr>

              

                <tr>

                  <td>&nbsp;</td>

                  <td align="left">&nbsp;</td>

                </tr>

                </table>

            </td>

          </tr>

        </table>

        <br>

      </form>

    

    <form name="form1" method="post" action="">



        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >

          <tr >

            <td height="22" align="left" valign="middle" class="adminDetailHeading">&nbsp;Search 

              Criteria</td>

          </tr>

          <tr >

            <td align="center" valign="top" class="bordLightBlue">

            <table width="100%" border="0" cellspacing="3" cellpadding="2">

                <tr>

                  <td><span class="searchText">Field</span>:<br /><select name="field" id="field">

                      <option value="point_name_en" <?PHP if ($field == "point_name_en"){echo "selected";}?>>Point Information </option>

                      <option value="is_active"<?PHP if($field == "is_active"){echo "selected";}?>>Status 

                      e.g Y=Activate , N=Deactivate</option>

                    </select></td>

                   <td ><span class="searchText">Condition</span><br /><?php

						if (!$LIKE) {

							$LIKE = "LIKE";

						}

					?>

                    <select name="LIKE" id="LIKE">

                      <option value="LIKE" <?PHP if ($LIKE == "LIKE"){echo "selected";}?>>LIKE</option>

                      <option value="=" <?PHP if (($LIKE == "=") || ($LIKE == "")){echo "selected";}?>>Equal 

                      To</option>

                      <option value="!=" <?PHP if ($LIKE == "!="){echo "selected";}?>>Not 

                      Equal To</option>

                      <option value="<" <?PHP if ($LIKE == "<"){echo "selected";}?>>Less 

                      Than</option>

                      <option value=">" <?PHP if ($LIKE == ">"){echo "selected";}?>>Greater 

                      Than</option>

                      <option value="<=" <?PHP if ($LIKE == "<="){echo "selected";}?>>Less 

                      Than or Equal To</option>

                      <option value=">=" <?PHP if ($LIKE == ">="){echo "selected";}?>>Greater 

                      Than or Equal To</option>

                    </select></td>

                 <td ><span class="searchText">Search 

                    Name&nbsp;</span><br /> <input name="q" type="text" id="q" value="<?php echo htmlspecialchars($q);?>" size="20"></td>  

                    

                   <td ><span class="searchText">Order 

                    By&nbsp;</span><br /><select name="by" id="by">

                      <option value="ASC" <?PHP if (($by == "ASC") || ($by == "")) { echo "selected";}?>>ASCENDING</option>

                      <option value="DESC" <?PHP if ($by == "DESC"){ echo "selected";}?>>DESCENDING</option>

                    </select></td>

                     <td  valign="bottom">

                     <input name="mid" type="hidden" value="1">

                    <input name="offset" type="hidden" value="0">

                    <input name="Search" type="submit" id="Search" value="Search" class="flat">

                    <input name="ssubmit" type="hidden" id="ssubmit" value="" class="flat">

                    </td>

                </tr>

                <tr>

                  <td height="30" colspan="5" align="center" valign="middle" ><a href="<?=HOST_URL?>/admin/school_management/points_management.php?mid=<?=$mid?>&sid=<?=$sid?>&tab=user&subtab=points"><span class="detailLinkColor">All 

                  Records</span></a></td>

                </tr>

              

              </table></td>

          </tr>

        </table>

      </form>

      <br>

      <span class="msgColor"><?php echo $MSG;?></span><br>

      <?php 		

		$q = addslashes($q);

		

		//WHERE tbl_school_id='$tbl_school_id_sess'  now add crad to move govt section. cards common for all schools

		$CountRec = "SELECT * FROM ".TBL_BEHAVIOUR_POINTS." WHERE  tbl_school_id='".$tbl_school_id."' ";		

		//WHERE tbl_school_id='$tbl_school_id_sess'  now add crad to move govt section. cards common for all schools

		$Query = "SELECT * FROM ".TBL_BEHAVIOUR_POINTS." WHERE  tbl_school_id='".$tbl_school_id."' ";

		

		if($field && !empty($q)){	    	

			if($LIKE=="LIKE"){

					$CountRec .= " AND $field $LIKE '%$q%' ";

					$Query .= " AND $field $LIKE '%$q%' ";

			}else{

					$CountRec .= " AND $field $LIKE '$q' ";	

					$Query .= " AND $field $LIKE '$q' ";

			}

		}	



		

		$Query .= " ORDER BY point_category_order $by ";

		$Query .=" LIMIT $offset, ".TBL_CARD_CATEGORY_PAGING;

		//echo $Query; exit;

		$q = stripslashes($q);

		$total_record = CountRecords($CountRec);

		$data = SelectMultiRecords($Query);

		  		 if ($total_record =="")

					   echo '<span class="msgColor">'.MSG_NO_RECORD_FOUND."</span>";

					else{	

					    echo '<span class="msgColor">';

					    echo " <b> ". $total_record ." </b> Record(s) found. Showing <b>";

						if ($total_record>$offset){

							echo $offset+1;

							echo " </b>to<b> ";

							if ($offset >=$total_record - TBL_CARD_CATEGORY_PAGING)	{ 

								  echo $total_record; 

							}else { 

							   	echo $offset + TBL_CARD_CATEGORY_PAGING ;

							}

						}else{ 

							echo $offset+1;

							echo "</b> - ". $total_record;

							echo " to ". $total_record ." Record(s) ";		

						}

						echo "</b>.</span>";	

					}

	   ?>

      <?php  if ($total_record !=""){?>

      <table width="98%" border="0" cellspacing="0" cellpadding="0">

       

        <tr>

          <td align="center" valign="top" class="adminDetailHeading"><table width="100%" border="0" align="center"  cellpadding="2" cellspacing="1">

              <form name="frmDirectory" onSubmit="return valueCheckedForUsersManagement()">

                <tr align="center" >

                  

                  <td width="20%" align="center" class="adminDetailHeading">Point Information</td>

                  <td width="10%" height="24" align="center" class="adminDetailHeading">Points</td>

                  <td width="8%" align="center" class="adminDetailHeading">Status&nbsp;</td>

                  <td width="6%" align="center" class="adminDetailHeading">Action</td>

                  <td width="6%" height="24" align="center" class="adminDetailHeading"><input name="AC" type="checkbox" onClick="CheckAll();"></td>

                </tr>

                <?php

				 for($i=0;$i<count($data);$i++){

						$id =$data[$i]["id"];

						$tbl_point_category_id =$data[$i]["tbl_point_category_id"];

						$point_name_en 		=	$data[$i]["point_name_en"];

						$point_name_ar 		=	$data[$i]["point_name_ar"];

						$behaviour_point 	=	$data[$i]["behaviour_point"];

						$point_category_order =	$data[$i]["point_category_order"];

						$added_date 			=	$data[$i]["added_date"];				

						$is_active 			=	$data[$i]["is_active"];

						

				?>

                <tr valign="middle" style="background-color:#FFF">

                  <td align="left" ><a href="<?=HOST_URL?>/admin/school_management/points_management.php?mid=2&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&by=<?=$by?>&tbl_point_category_id=<?=$tbl_point_category_id?>&tab=user&subtab=points" title="Click to view detail" class="detailLinkColor">

                        <div style="padding:5px"><?php echo $point_name_en;?></div>

                        <div style="padding:5px" class="lan_r_ar"><?php echo $point_name_ar;?></div></a>

                  </td>                  

                  <td height="20" align="center" ><?=$behaviour_point?></td>

                  <td align="center"><?php 

		 			 if($is_active == 'Y'){?>

                    <img src="<?=IMG_PATH?>/yes.gif">

                    <?php } else {?>

                    <img src="<?=IMG_PATH?>/no.gif">

                    <?php }  ?></td>

                  <td align="center"><a href="?mid=3&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&by=<?=$by?>&tbl_point_category_id=<?=$tbl_point_category_id?>&tab=user&subtab=points" class="detailLinkColor">Edit</a></td>

                  <td align="center"><input name="EditBox[]" type="checkbox" value="<?=$tbl_point_category_id?>"></td>

                </tr>

                <?php }?>

                <tr>

                  <td height="20" colspan="5" align="center" valign="middle" bgcolor="#FFFFFF"><input name="reset" type="reset" class="input_text"  value="Reset">

                    <input name="show" type="submit" class="input_text" id="ssubmit" value="Active">

                    <input name="hide" type="submit" class="input_text"  value="Deactive">

                    <input name="delete" type="submit" class="input_text"  value="Delete">

                    <input name="ssubmit" type="hidden" id="ssubmit" value="recordActiveInactive">

                    <input name="mid" type="hidden" id="mid2" value="1">

                    <input name="sid" type="hidden" id="sid" value="<?=$sid?>">

                    <input name="offset" type="hidden" id="offset" value="<?=$offset?>">

                    <input name="q" type="hidden" id="q" value="<? echo htmlspecialchars($q);?>">

                    <input name="tbl_gallery_category_id" type="hidden" value="<?=$tbl_point_category_id?>">

                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>">

                    <input name="by" type="hidden" id="by" value="<?=$by?>">

                    <input name="field" type="hidden" id="field" value="<?=$field?>">

                    <input name="LIKE" type="hidden" value="<?=$LIKE?>"></td>

                </tr>

                <tr align="right">

                  <td height="20" colspan="5" valign="middle" bgcolor="#FFFFFF"><?php 

					   if ($total_record != "" && $total_record>TBL_CARD_CATEGORY_PAGING) {

							$url = "points_management.php?mid=1&sid=$sid&field=$field&q=$q&LIKE=$LIKE&by=$by&tab=user&subtab=points";

							$Paging_object = new Paging();

							$Paging_object->paging_new_style_final($url,$offset,$clicked_link,$total_record,TBL_CARD_CATEGORY_PAGING);

						}

					?></td>

                </tr>

              </form>

            </table></td>

        </tr>

      </table>

      <br>

      <?php } ?>

      <br>

      <br>

     

     

     

     </td>

  </tr>

</table>

<!--	RECORD DETAILS SCREEN	-->

<?php } if($mid=="2") { 

		$qry = "SELECT * FROM ".TBL_BEHAVIOUR_POINTS." WHERE tbl_point_category_id='$tbl_point_category_id' and tbl_school_id='$tbl_school_id_sess'";

		$data = selectFrom($qry);

		$id =$data["id"];

		$tbl_card_category_id =$data["tbl_point_category_id"];

		$category_name_en =$data["point_name_en"];

		$category_name_ar =$data["point_name_ar"];

		$card_points =$data["behaviour_point"];

		$gallery_category_order =$data["point_category_order"];

		$added_date =$data["added_date"];				

		$is_active =$data["is_active"];

		

?>

<table width="98%" border="0" align="center" cellpadding="0" cellspacing="1" >

  <tr >

    <td height="22" align="left" valign="middle"  class="adminDetailHeading" >&nbsp;Point Infrmation</td>

  </tr>

  <tr >

    <td align="center" valign="top" class="bordLightBlue"><form name="frmRegister" id="frmRegister" method="post" action=""  onSubmit="return validateForm();">

        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >

         

          <tr >

            <td align="center" valign="top"><table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" >

                <tr align="center" valign="top" >

                  <td colspan="2"></td>

                </tr>

                <tr valign="middle" >

                  <td width="20%" align="right">&nbsp;</td>

                  <td width="78%">&nbsp;</td>

                </tr>

                <!--<tr valign="middle" >

                  <td align="right">Cateogory Name:</td>

                  <td><?=$category_name_en?>&nbsp;</td>

                </tr>-->

                <tr valign="middle" >

                  <td align="right">Point Information:</td>

                  <td align="left" valign="top"><?php echo $category_name_en; ?></td>

                </tr>

                <tr valign="middle" >

                  <td align="right">Point Information Arabic:</td>

                  <td align="left" valign="top"><?php echo $category_name_ar; ?></td>

                </tr>

                 <tr valign="middle" >

                  <td align="right">Points:</td>

                  <td align="left" valign="top"><?php echo $card_points; ?></td>

                </tr>

                <tr valign="middle" >

                  <td align="right">Status:&nbsp;</td>

                  <td align="left" valign="top"><?php echo $is_active; ?></td>

                </tr>

                <tr valign="middle" >

                  <td align="right">&nbsp;</td>

                  <td align="left" valign="top">&nbsp;</td>

                </tr>

                <tr valign="middle" >

                  <td colspan="2" align="center" height="10"><a href="points_management.php?mid=1&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&sort=<?=$sort?>&by=<?=$by?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&tab=user&subtab=points" class="detailLinkColor">Back </a>

                </tr>

                <tr>

                  <td>&nbsp;</td>

                  <td align="left" valign="middle">&nbsp;</td>

                </tr>

              </table></td>

          </tr>

        </table>

      </form></td>

  </tr>

</table>

<!--	RECORD EDIT SCREEN	-->

<?php }if($mid=="3"){ 

		

		$qry = "SELECT * FROM ".TBL_BEHAVIOUR_POINTS." WHERE tbl_point_category_id='$tbl_point_category_id'";

		$data = selectFrom($qry);

		$id = $data["id"];

		$id =$data["id"];

		$tbl_point_category_id =$data["tbl_point_category_id"];

		$point_name_en 		=	$data["point_name_en"];

		$point_name_ar 		=	$data["point_name_ar"];

	    $behaviour_point 	=	$data["behaviour_point"];

		$point_category_order =	$data["point_category_order"];

		$added_date =$data["added_date"];				

		$is_active =$data["is_active"];

?>

<table width="100%" height="451" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">

  <tr>

    <td height="30" valign="center" class="adminDetailHeading">&nbsp;Points Management 

    [Edit] </td>

  </tr>

  <tr>

    <td align="center" valign="top"><br>

      <span class="msgColor"><?php echo $MSG;?></span> <br>

      <table width="98%" border="0" align="center" cellpadding="0" cellspacing="1" >

        <tr >

          <td align="center" valign="top"><form name="frmRecord" id="frmRecord" method="post" action=""  onSubmit="return validateForm();" enctype="multipart/form-data">

              <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >

                <tr >

                  <td height="22" align="left" valign="middle"  class="adminDetailHeading" ><strong>&nbsp;Edit Point Details</strong></td>

                </tr>

                <tr >

                  <td align="center" valign="top" class="bordLightBlue"><table width="90%" border="0" align="center" cellpadding="2" cellspacing="2" >

                      <tr align="center" valign="top" >

                        <td colspan="2"></td>

                      </tr>

                      <tr align="left" valign="top" >

                        <td colspan="2"><span class="msgColor">Fields with * are 

                          mandatory.</span></td>

                      </tr>

                     

                      <tr valign="middle" >

                        <td align="right" valign="top">&nbsp;</td>

                        <td align="left" valign="top">&nbsp;</td>

                      </tr>

                      <tr valign="middle" >

                        <td align="right" valign="top">

                        </td>

                      </tr>

                      <tr valign="middle" >

                        <td align="left" width="35%" valign="top">Point Information [En]<span class="msgColor">*</span><br/><input name="point_name_en" type="text" class="input_text" id="point_name_en" size="40" value="<?=$point_name_en?>"></td>

                        <td align="left" width="35%" valign="top">Point Information [Ar]<span class="msgColor">*</span><br/><input name="point_name_ar" type="text"  class="input_text input_text_ar" id="point_name_ar" size="40" value="<?=$point_name_ar?>"></td>

                        <td align="left" width="30%" valign="top">Points<span class="msgColor">*</span><br/>

                      <input name="behaviour_point" type="text" class="input_text" id="behaviour_point" size="40" value="<?=$behaviour_point?>"></td>

                      </tr>

                     

                    <tr valign="middle" >

                      <td align="left" width="35%" valign="top" colspan="3">Status<br/>

                        <input type="radio" name="is_active" value="Y" <?php if($is_active == 'Y'){?>checked="checked"<? } ?>>

                          Yes

                          <input type="radio" name="is_active" value="N" <?php if($is_active == 'N'){?>checked="checked"<? } ?>>

                          No 

                          </td>

                    

                    </tr>

                       <tr>

                        <td>&nbsp;</td>

                        <td align="left" valign="middle" colspan="3">

                      </tr>

                      <tr>

                      <td>&nbsp;</td>

                       <td align="left" width="100%" valign="top" colspan="2"><a href="points_management.php?mid=1&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&sort=<?=$sort?>&by=<?=$by?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&tab=user&subtab=points" class="detailLinkColor">Back</a>&nbsp;&nbsp;

                          <input type="hidden" name="tbl_point_category_id" value="<?=$tbl_point_category_id?>">

                          <input type="reset" name="Reset" value="Reset" class="flat">

                          <input type="submit" name="Save" value="Save" class="flat">

                          <input name="ssubmit" type="hidden" id="ssubmit" value="editRecord">

                          <input name="sid" type="hidden" value="<?=$sid?>">

                          <input name="mid" type="hidden" value="1">

                          <input name="offset" type="hidden" id="offset" value="<?=$offset;?>">

                          <input name="q" type="hidden" id="q" value="<?php echo htmlspecialchars($q);?>">

                          <input name="sort" type="hidden" id="sort" value="<?=$sort?>">

                          <input name="by" type="hidden" id="by" value="<?=$by?>">

                          <input name="field" type="hidden" id="field" value="<?=$field?>">

                          <input name="LIKE2" type="hidden" value="<?=$LIKE?>"></td>

                      </tr>

                   

                    </table></td>

                </tr>

              </table>

            </form></td>

        </tr>

      </table>

      <p><br>

      </p></td>

  </tr>

</table>

<!--	RECORD DELETE SCREEN	-->

<?php }if($mid=="4"){ ?>

<table width="100%" height="380" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">

  <tr>

    <td height="30" valign="center" class="adminDetailHeading">Point Management 

    [Del Confirmation]</td>

  </tr>

  <tr align="center" >

    <td valign="top" ><br>

      <span class="msgColor"> Are you sure you want to delete the selected record(s) </span> <br>

      <table width="98%" border="0" cellspacing="0" cellpadding="0">

        <tr>

          <td align="center" valign="top" class="adminDetailHeading"><table width="100%" border="0" align="center"  cellpadding="2" cellspacing="1" >

              <form name="frmDirectory" onSubmit="return valueCheckedForUsersManagement();">

                <tr >

                 

                  <td width="20%" align="center" class="adminDetailHeading">Point Information</td> 

                  <td width="10%" align="center"  class="adminDetailHeading">Date</td>

                  <td width="10%" align="center"  class="adminDetailHeading">Status</td>

                  <td width="4%" height="24" align="center"  class="adminDetailHeading"><input name="AC" type="checkbox" id="AC" onClick="CheckAll();" checked></td>

                </tr>

                <?php 

			for($i=0;$i<count($EditBox);$i++){

				$qry ="SELECT * FROM ".TBL_CARD_CATEGORY." WHERE tbl_card_category_id='$EditBox[$i]'";

				$data = selectFrom($qry);

				$id =$data["id"];

				$tbl_card_category_id =$data["tbl_card_category_id"];

				$category_name_en =$data["category_name_en"];

				$file_name_updated =$data["card_logo"];

				$card_category_order =$data["card_category_order"];

				$added_date =$data["added_date"];				

				$is_active =$data["is_active"];

				



				?>

                <tr valign="middle" bgcolor="#FFFFFF">

                  <td align="left" ><div style="padding:5px"><?php echo $category_name_en;?></div>

                    <div style="padding:5px" class="lan_r_ar"><?php echo $category_name_ar;?></div></td>

                  <td height="20" align="center"><?php echo date("d M, Y H:i:s",strtotime($added_date));?></td>

                  <td align="center"><?php 

		  			if($is_active == 'Y'){?>

                    <img src="<?=IMG_PATH?>/yes.gif">

                    <?php } else {?>

                    <img src="<?=IMG_PATH?>/no.gif">

                    <?php }  ?></td>

                  <td height="20" align="center"><input name="EditBox[]" type="checkbox" id="EditBox[]" onClick="UnCheckAll();" value="<?=$tbl_gallery_category_id?>" checked></td>

                </tr>

                <?php }?>

                <tr>

                  <td height="20" colspan="5" align="center" valign="middle" bgcolor="#FFFFFF" ><input name="Button" type="button" class="input_text" id="hide" value="I am not sure" onClick="javascript:history.back();">

                    <input name="show" type="submit" class="input_text" id="show" value="Yes I am sure">

                    <input name="sid" type="hidden" id="sid" value="<?=$sid;?>">

                    <input name="offset" type="hidden" id="offset" value="<?=$offset;?>">

                    <input name="mid" type="hidden" id="mid" value="1">

                    <input name="ssubmit" type="hidden" id="show" value="recordDelInformation">

                    <input name="q" type="hidden" id="q" value="<?php echo htmlspecialchars($q);?>">

                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>">

                    <input name="by" type="hidden" id="by" value="<?=$by?>">

                    <input name="field" type="hidden" id="field" value="<?=$field?>">

                    <input name="LIKE" type="hidden" value="<?=$LIKE?>"></td>

                </tr>

              </form>

            </table></td>

        </tr>

      </table>

  </tr>

</table>

<?php } ?>



<script language="javascript">



	var host = '<?=HOST_URL?>';

	var lan = 'ar';

	

	function get_room_names_ajax(court_name) {

		var xmlHttp, rnd, url;

		rnd = Math.floor(Math.random()*11);

		

		try{		

			xmlHttp = new XMLHttpRequest(); 

		}catch(e) {

			try{

				xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");

			}catch(e) {

				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");

			}

		}

		

		//AJAX response

		xmlHttp.onreadystatechange = function() {

			if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {

				//alert(xmlHttp.responseText);

				$("#court_name_container").html(xmlHttp.responseText);

			}

		}

			

		//Sending AJAX request

		url = host + "/"+lan+"/court/get_room_names_ajax/"+court_name;

		xmlHttp.open("POST",url,true);

		xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");

		xmlHttp.send("rnd="+rnd);

		

	}//function

</script>

<?php include("../footer.php");  ?>