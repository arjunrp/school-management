<!doctype html>

<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script>
<script src="http://malsup.github.com/jquery.form.js"></script>
<style>
form {
	display: block;
	margin: 20px auto;
	background: #eee;
	border-radius: 10px;
	padding: 15px
}
#progress, #progress2 {
	position: relative;
	width: 400px;
	border: 1px solid #ddd;
	padding: 1px;
	border-radius: 3px;
}
#bar, #bar2 {
	background-color: #B4F5B4;
	width: 0%;
	height: 20px;
	border-radius: 3px;
}
#percent, #percent2 {
	position: absolute;
	display: inline-block;
	top: 3px;
	left: 48%;
}
</style>
</head>
<body>
<h1>Ajax File Upload Demo</h1>
<form id="myForm" action="upload.php" method="post" enctype="multipart/form-data">
  <input type="file" size="60" name="myfile">
  <input type="submit" value="Ajax File Upload">
</form>
<div id="progress">
  <div id="bar"></div>
  <div id="percent">0%</div >
</div>
<br/>
<div id="message"></div>


<form id="myForm2" action="upload.php" method="post" enctype="multipart/form-data">
  <input type="file" size="60" name="myfile">
  <input type="submit" value="Ajax File Upload">
</form>
<div id="progress2">
  <div id="bar2"></div>
  <div id="percent2">0%</div >
</div>
<br/>
<div id="message2"></div>

<script>
$(document).ready(function()
{
 
	var optionss = {
		beforeSend: function()
		{
			$("#progress").show();
			//clear everything
			$("#bar").width('0%');
			$("#message").html("");
			$("#percent").html("0%");
		},
		uploadProgress: function(event, position, total, percentComplete)
		{
			$("#bar").width(percentComplete+'%');
			$("#percent").html(percentComplete+'%');
	 
		},
		success: function()
		{
			$("#bar").width('100%');
			$("#percent").html('100%');
	 
		},
		complete: function(response)
		{
			$("#message").html("<font color='green'>"+response.responseText+"</font>");
		},
		error: function()
		{
			$("#message").html("<font color='red'> ERROR: unable to upload files</font>");
	 
		}
	 
	};
    $("#myForm").ajaxForm(optionss);
	
	
	var options2 = {
		beforeSend: function()
		{
			$("#progress2").show();
			//clear everything
			$("#bar2").width('0%');
			$("#message2").html("");
			$("#percent2").html("0%");
		},
		uploadProgress: function(event, position, total, percentComplete)
		{
			$("#bar2").width(percentComplete+'%');
			$("#percent2").html(percentComplete+'%');
	 
		},
		success: function()
		{
			$("#bar2").width('100%');
			$("#percent2").html('100%');
	 
		},
		complete: function(response)
		{
			$("#message2").html("<font color='green'>"+response.responseText+"</font>");
		},
		error: function()
		{
			$("#message2").html("<font color='red'> ERROR: unable to upload files</font>");
	 
		}
	 
	};
    $("#myForm2").ajaxForm(options2);
 
});


</script>
</body>
</html>