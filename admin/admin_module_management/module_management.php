<?php include ($_SERVER["DOCUMENT_ROOT"]."/aqdar/includes/config.inc.php"); ?>
<?php
$sid = $_REQUEST["sid"];
if (!loggedUser($sid)) { 
	redirect (HOST_URL_ADMIN ."/include/messages.php?msg=relogin");
exit();
}
?>
<?php
$LIB_CLASSES_FOLDER = INC_ADMIN_PATH."/lib/classes/";
								


include($LIB_CLASSES_FOLDER."Paging.php");
$user_id = loggedUser($sid);

$q = stripslashes($q);	

if($ssubmit=="insertModule"){
		$query = "SELECT * FROM ".TBL_MODULES." WHERE tbl_module_cat_id='$tbl_module_cat_id' AND module_name='$module_name' ";
		if (isExist($query)) {
			$MSG = "<b>[$module_name]</b> Already exists against this category</b>";		
		} else {

		$maxRes=getMaxId(TBL_MODULES,"id");
		$id=$maxRes[0]+1;
		
		$tbl_module_id = md5(uniqid(rand()));	
		$tbl_module_id = substr($tbl_module_id,0,10);
		$is_active  = $_REQUEST['is_active'];;
	
		if($is_active=='Y'){
			$approved_by      = $user_id;
			$approved_date	  = date("Y-m-d H:i:s");
		}else{
			$approved_by      = "N/A";
			$approved_date	  = "N/A";
		}					

		$qry = "INSERT INTO ".TBL_MODULES." (`id`, `tbl_module_id`, `tbl_module_cat_id` ,`module_name` ,`module_desc` , `module_source_url`,`added_by`, `added_date`, `approved_by`, `approved_date`, is_active) VALUES ('$id', '$tbl_module_id', '$tbl_module_cat_id', '$module_name', '$module_desc', '$module_source_url',  '$user_id', NOW(), '$approved_by',  '$approved_date', '".$is_active."')";

			//echo $qry;
			insertInto($qry);
			
			$module_name = "";
			$module_desc = "";
			$MSG = "Module created successfully!";
		}
	$mid=1;
}

if($ssubmit=="editModule"){
		$query = "SELECT * FROM ".TBL_MODULES." WHERE tbl_module_cat_id='$tbl_module_cat_id' AND module_name='$module_name' AND tbl_module_id<>$tbl_module_id";
		//echo $query;
		if (isExist($query)) {
			$MSG = "<b>[$module_name]</b> Already exists</b>";		
			$mid = 3;
		} else {
			$qry = "UPDATE ".TBL_MODULES." SET 
					`tbl_module_cat_id` = '$tbl_module_cat_id',
					`module_name` = '$module_name',
					`module_source_url` = '$module_source_url',
					`module_desc` = '$module_desc',
					`last_modified_by` = '$user_id',
					`last_modified_date` = NOW(),
					`is_active` = '$is_active' WHERE tbl_module_id='$tbl_module_id'";
			update($qry);
			$mid = 1;
				
			$module_name = "";
			$module_desc = "";
			$MSG = "Module updated successfully!";
		}
}

if($ssubmit=="moduleDetail"){
		if(isset($show)){
				for ($j=0; $j<count($EditBox); $j++){ 
					$Querry = "UPDATE ". TBL_MODULES ." SET approved_by='$user_id', approved_date = NOW(), last_modified_by='$user_id', last_modified_date = NOW(), is_active='Y' WHERE tbl_module_id='$EditBox[$j]'";			
					//echo $Querry;
					update($Querry);
					$mid=1;	
					$MSG = "Selected Module(s) have been activated successfully!";
				}
		}
		if(isset($hide)){
				for ($j=0; $j<count($EditBox); $j++){ 
					$Querry1 = "UPDATE ". TBL_MODULES ." SET approved_by='N/A', approved_date = '0000-00-00 00:00:00', last_modified_by='$user_id', last_modified_date = NOW(), is_active='N' WHERE tbl_module_id='$EditBox[$j]'";			
					update($Querry1);
					$mid=1;	
					$MSG = "Selected Module(s) have been deactivated successfully!";
				}
			}
		if(isset($delete)){$mid = 4;}
	}

if($ssubmit == "moduleDelConfirmation"){
		for($i=0;$i<count($EditBox);$i++){
				deleteFrom("DELETE FROM ".TBL_MODULES." WHERE `tbl_module_id` = '$EditBox[$i]'");
				$mid = 1;
				$MSG = "Selected Module(s) have been deleted successfully!";
			
		}//for
}	


?>
<html>
<head>
<title>Admin (Module Management)</title>
<link href="<?=HOST_URL?>/admin/css/interface.css" type=text/css rel=stylesheet>
<!--//included files for tigra calendar-->
<script language="JavaScript" src="../include/tigra_calendar/calendar_us.js"></script>
<link rel="stylesheet" href="../include/tigra_calendar/calendar.css">
<script language="javascript" src="<?=HOST_URL?>/scripts/jquery-1.2.3.min.js"></script>
<script src="<?=ADMIN_SCRIPT_PATH?>/validation.js">CopyDatarichtext1();</script>

<script language="JavaScript">
function valueCheckedForModuleManagement(){
	var ml = document.frmDirectory;
	var len = ml.elements.length;
	for (var i = 0; i < len; i++){
	   	if (document.frmDirectory.elements[i].checked){
			return true;
		}
	}
	 alert ("Select at least one Module!");
	 return false;
}

function CheckAll(){
	var ml = document.frmDirectory;
	var len = ml.elements.length;
	if (document.frmDirectory.AC.checked==true) {
		 for (var i = 0; i < len; i++) {
			document.frmDirectory.elements[i].checked=true;
		 }
	} else {
		  for (var i = 0; i < len; i++)  {
			document.frmDirectory.elements[i].checked=false;
		  }
	}
}
function UnCheckAll() {
	var ml = document.frmDirectory;
	var len = ml.elements.length;
	var count=0; var checked=0;
	    for (var i = 0; i < len; i++) {	       
			if ((document.frmDirectory.elements[i].type=='checkbox') && (document.frmDirectory.elements[i].name != "AC")) {
				count = count + 1;
	  		 	if (document.frmDirectory.elements[i].checked == true){
					checked = checked + 1;
				}
			}
	     }
		 
	if (checked == count) {
		 document.frmDirectory.AC.checked = true;
	} else {
		document.frmDirectory.AC.checked = false;
	}
}
function frmNewValidation() {
	if ( validate_module_name() == false) {
		return false;
	}
return true;	
}

function validate_module_name() {
	var regExp = / /g;
	var str = document.frmRegister.module_name.value;
	str = str.replace(regExp,'');
	if (str.length <= 0) {
		alert("\nThe Module Name is blank. Please write Module Name.")
		document.frmRegister.module_name.value = "";
		document.frmRegister.module_name.focus();
	return false;
	}
}

function validate_module_page_name() {
	var regExp = / /g;
	var str = document.frmRegister.module_page_name.value;
	str = str.replace(regExp,'');
	if (str.length <= 0) {
		alert("\nThe Module Page Name is blank. Please write Module Page Name.")
		document.frmRegister.module_page_name.value = "";
		document.frmRegister.module_page_name.focus();
	return false;
	}
}

function validate_module_tooltip() {
	var regExp = / /g;
	var str = document.frmRegister.module_tooltip.value;
	str = str.replace(regExp,'');
	if (str.length <= 0) {
		alert("\nThe Module Tooltip is blank. Please write Module Tooltip.")
		document.frmRegister.module_tooltip.value = "";
		document.frmRegister.module_tooltip.focus();
	return false;
	}
}
</script>
<script type="text/javascript">
var xmlhttp;
function setJuryServices(){

	var jury_id = getDropDownListValue("tbl_jury_idd");
	
//	alert("setJuryServices tbl_jury_id: "+tbl_jury_id+"text: "+getDropDownListText("tbl_jury_id"));
	var url = "ajax_cat_module.php?tbl_jury_id="+jury_id;
	//alert(url);
	//return;
//	document.frmRegister.module_name.value = url;
	xmlhttp=null;
	if (window.XMLHttpRequest) {// code for Firefox, Opera, IE7, etc.
		xmlhttp=new XMLHttpRequest();
	} else if (window.ActiveXObject) {// code for IE6, IE5
	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	if (xmlhttp!=null) {
		xmlhttp.onreadystatechange=function () { 

			state_Change(1);
        }
		xmlhttp.open("GET",url,true);
		xmlhttp.send(null);
	} else {
		alert("Your browser does not support XMLHTTP.");	
	}
}

function setModuleCat(){
	var tbl_jury_service_id = getDropDownListValue("tbl_jury_service_id");
//	alert("setModuleCat tbl_jury_service_id: "+tbl_jury_service_id+" text: "+getDropDownListText("tbl_jury_service_id"));
	var url = "ajax_cat_module.php?tbl_jury_service_id="+tbl_jury_service_id;
//	alert(url)
//	document.frmRegister.module_name.value = url;

	xmlhttp=null;
	if (window.XMLHttpRequest) {// code for Firefox, Opera, IE7, etc.
		xmlhttp=new XMLHttpRequest();
	} else if (window.ActiveXObject) {// code for IE6, IE5
	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	if (xmlhttp!=null) {
		xmlhttp.onreadystatechange=function () { 
                                    state_Change(2);
                                    }
		xmlhttp.open("GET",url,true);
		xmlhttp.send(null);
	} else {
		alert("Your browser does not support XMLHTTP.");	
	}
}

function state_Change(flag) {	
	if (xmlhttp.readyState==4) {// 4 = "loaded"
		if (xmlhttp.status==200) {// 200 = "OK"				
				//alert(xmlhttp.responseText);
				document.getElementById("div_sub_cat").innerHTML=xmlhttp.responseText;
		} else {
			alert("Problem retrieving data:" + xmlhttp.statusText);	
		}
	}
}

function getDropDownListValue(idd){
	return document.getElementById(idd).value;

}
</script>

</head>
<?php if (!$offset || $offset<0)  { $offset =0;} ?>
<?php if (!$LIKE)  { $LIKE = "LIKE";} ?>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<?php if($mid=="1"){ ?>
<table width="100%" height="570" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr> 
    <td  valign="center" height='30'  class="adminDetailHeading">&nbsp;Module Management 
    </td>
  </tr>
  <tr align="center" > 
    <td height="538" align="center" valign="top" > <form name="form1" method="post" action="">
        <br>
        <table width="80%" border="0" align="center" cellpadding="0" cellspacing="1" >
          <tr > 
            <td height="22" align="left" valign="middle" class="adminDetailHeading">&nbsp;Search 
              Criteria</td>
          </tr>
          <tr > 
            <td align="center" valign="top" class="bordLightBlue"><table width="100%" border="0" cellspacing="3" cellpadding="2">
                <tr> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr> 
                  <td width="32%">&nbsp;</td>
                  <td width="48%"> <a href="module_management.php?mid=<?=$mid?>&sid=<?=$sid?>&tbl_jury_id=<?=$tbl_jury_id?>"><span class="detailLinkColor">All 
                    Records</span></a>: or&nbsp;</td>
                  <td width="20%"><i></i></td>
                </tr>
                <tr> 
                  <td colspan="3"><hr width="80%" size="1"></td>
                </tr>
                <tr> 
                  <td align="right" valign="top" ><span class="searchText">Field:&nbsp;</span></td>
                  <td align="left" valign="top"> <select name="field" id="field">
                      <option value="module_name" <?PHP if ($field == "module_name"){echo "selected";}?>>Module Name</option>
                      <option value="module_desc" <?PHP if ($field == "module_desc"){echo "selected";}?>>Description</option>
                      <option value="added_date" <?PHP if ($field == "added_date"){echo "selected";}?>>Date e.g [YYYY-MM-DD]</option>
                      <option value="is_active" <?PHP if($field == "is_active"){echo "selected";}?>>Status e.g Y=Activate , N=Deactivate</option>
                    </select></td>
					<input name="tbl_jury_id" type="hidden" id="sid" value="<?=$tbl_jury_id?>">
                  <td><i></i></td>
                </tr>
                <tr> 
                  <td align="right" valign="top"><span class="searchText">Condition:&nbsp;</span></td>
                  <td align="left" valign="top"> 
                    <?php
							if (!$LIKE) {
								$LIKE = "LIKE";
							}
						?>
                    <select name="LIKE" id="LIKE">
                      <option value="LIKE" <?PHP if ($LIKE == "LIKE"){echo "selected";}?>>LIKE</option>
                      <option value="=" <?PHP if (($LIKE == "=") || ($LIKE == "")){echo "selected";}?>>Equal 
                      To</option>
                      <option value="!=" <?PHP if ($LIKE == "!="){echo "selected";}?>>Not 
                      Equal To</option>
                      <option value="<" <?PHP if ($LIKE == "<"){echo "selected";}?>>Less 
                      Than</option>
                      <option value=">" <?PHP if ($LIKE == ">"){echo "selected";}?>>Greater 
                      Than</option>
                      <option value="<=" <?PHP if ($LIKE == "<="){echo "selected";}?>>Less 
                      Than or Equal To</option>
                      <option value=">=" <?PHP if ($LIKE == ">="){echo "selected";}?>>Greater 
                      Than or Equal To</option>
                    </select></td>
                  <td><i></i></td>
                </tr>
                <tr> 
                  <td align="right" valign="top"><span class="searchText">Search 
                    Name:&nbsp;</span></td>
                  <td align="left" valign="top"> <input name="q" type="text" id="q" value="<?php echo htmlspecialchars($q);?>" size="40"> 
                  </td>
                  <td><i></i></td>
                </tr>
                <tr> 
                  <td align="right" valign="top"><span class="searchText">Order 
                    By:&nbsp;</span></td>
                  <td align="left" valign="top"> <select name="by" id="by">
                      <option value="ASC" <?PHP if (($by == "ASC") || ($by == "")) { echo "selected";}?>>ASCENDING</option>
                      <option value="DESC" <?PHP if ($by == "DESC"){ echo "selected";}?>>DESCENDING</option>
                    </select> &nbsp;</td>
                  <td><i></i></td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                  <td align="left"> <input name="mid" type="hidden" value="1"> 
                    <input name="offset" type="hidden" value="0"> <input name="Search" type="submit" id="Search" value="Search" class="flat"> 
                    &nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
              </table></td>
          </tr>
        </table>
      </form>
      <br> <span class="msgColor"><?php echo $MSG;?></span> <br> 
      <?php 
		$q = addslashes($q);
		$CountRec .= "SELECT * FROM ".TBL_MODULES." WHERE 1 ";
	
		$Query .= "SELECT * FROM ".TBL_MODULES." WHERE 1";

		if(!$by){$by="ASC";}
		
		if($field && !empty($q)){	    	
			
			if($LIKE=="LIKE"){
					$CountRec .= " AND `$field` $LIKE '%$q%' ";
					$Query .= " AND `$field` $LIKE '%$q%' ";
			}else{
					$CountRec .= " AND `$field` $LIKE '$q' ";	
					$Query .= " AND `$field` $LIKE '$q' ";
			}
		}	
		 $Query .= " ORDER BY tbl_modules.tbl_module_cat_id $by ";
		 $Query .=" LIMIT $offset, ".TBL_MODULE_PAGING;
		 $q = stripslashes($q);
		 //echo $Query;
		 //exit();
		 $total_record = CountRecords($CountRec);
		 $data = SelectMultiRecords($Query);
		  		 if ($total_record =="")
					   echo '<span class="msgColor">'.NO_RECORD_FOUND."</span>";
					else{	
					    echo '<span class="msgColor">';
					    echo " <b> ". $total_record ." </b> Module(s) found. Showing <b>";
						if ($total_record>$offset){
							echo $offset+1;
							echo " </b>to<b> ";
							if ($offset >= $total_record - TBL_MODULE_PAGING)	{ 
								  echo $total_record; 
							}else { 
							   	echo $offset + TBL_MODULE_PAGING;
							}
						}else{ 
							echo $offset+1;
							echo "</b> - ". $total_record;
							echo " to ". $total_record ." Module ";		
						}
						echo "</b>.</span>";	
					}
	   ?>
      <?php  if ($total_record !=""){?>
      <table width="98%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td height="18"><span class="msgColor">&nbsp;&nbsp;Note: To view detail 
            click a Module Name.</span></td>
        </tr>
        <tr> 
          <td align="center" valign="top" class="adminDetailHeading"> <table width="100%" border="0" align="center"  cellpadding="2" cellspacing="1">
              <form name="frmDirectory" onSubmit="return valueCheckedForModuleManagement()">
                <tr align="center" > 
                  <td width="34%" height="24" align="center" class="adminDetailHeading">Module 
                    Name </td>
                  <td width="32%" align="center" class="adminDetailHeading">Module 
                    Category Name</td>
                  <td width="15%" height="24" align="center" class="adminDetailHeading">Date</td>
                  <td width="8%" height="24" align="center" class="adminDetailHeading">Status</td>
                  <td width="6%" height="24" align="center" class="adminDetailHeading">Action</td>
                  <td width="5%" height="24" class="adminDetailHeading"> <input name="AC" type="checkbox" onClick="CheckAll();"></td>
                </tr>
                <?php for($i=0;$i<count($data);$i++){
						$module_id_db = $data[$i]["tbl_module_id"];
						$module_cat_id_db = $data[$i]["tbl_module_cat_id"];
						$module_name_db = $data[$i]["module_name"];
						$description_db = $data[$i]["module_desc"];
						$added_date_db = $data[$i]["added_date"];
						$is_active_db = $data[$i]["is_active"];
						$qry = "SELECT * FROM " .TBL_MODULE_CATEGORIES." WHERE tbl_module_cat_id = '".$module_cat_id_db."'";
						$data_mod_cat = selectFrom($qry);
						$module_cat_name = $data_mod_cat['module_cat_name'];
						
				?>
                <tr valign="middle" bgcolor="#FFFFFF"> 
                  <td height="20" align="left" >&nbsp; <a href="?mid=2&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&by=<?=$by?>&tbl_module_id=<?=$module_id_db?>" title="Click to view detail" class="detailLinkColor"> 
                    <?=$module_name_db?>
                    </a> </td>
                  <td align="center"><?=$module_cat_name?></td>
                  <td height="20" align="center" ><? echo dateFormat($added_date_db);?></td>
                  <td align="center"> 
                    <?php
					//echo IMG_PATH."/yes.gif";
		  if($is_active_db == 'Y'){?>
                    <img src="<?=IMG_PATH?>/yes.gif"> 
                    <?php } else {?>
                    <img src="<?=IMG_PATH?>/no.gif"> 
                    <?php }  ?>
                  </td>
                  <td align="center"> <a href="?mid=3&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&by=<?=$by?>&tbl_module_id=<?=$module_id_db?>" class="detailLinkColor">Edit</a> 
                  </td>
                  <td align="center" > <input name="EditBox[]" type="checkbox" onClick="UnCheckAll();" value="<?=$module_id_db?>"> 
                  </td>
                </tr>
                <?php }?>
                <tr> 
                  <td height="20" colspan="7" align="center" valign="middle" bgcolor="#FFFFFF"> 
                    <input name="reset" type="reset" class="flat"  value="Reset"> 
                    <input name="show" type="submit" class="flat" id="ssubmit" value="Active"> 
                    <input name="hide" type="submit" class="flat"  value="Deactive"> 
                    <input name="delete" type="submit" class="flat"  value="Delete"> 
                    <input name="ssubmit" type="hidden" id="ssubmit" value="moduleDetail"> 
                    <input name="mid" type="hidden" id="mid2" value="1"> <input name="sid" type="hidden" id="sid" value="<?=$sid?>"> 
                    <input name="offset" type="hidden" id="offset" value="<?=$offset?>"> 
                    <input name="q" type="hidden" id="q" value="<? echo htmlspecialchars($q);?>"> 
                    <input name="id" type="hidden" value="<?=$id?>"> <input name="sort" type="hidden" id="sort" value="<?=$sort?>"> 
                    <input name="by" type="hidden" id="by" value="<?=$by?>"> <input name="field" type="hidden" id="field" value="<?=$field?>"> 
                    <input name="LIKE" type="hidden" value="<?=$LIKE?>"></td>
                </tr>
                <tr align="right"> 
                  <td height="20" colspan="7" valign="middle" bgcolor="#FFFFFF"> 
                    <?php
	 
					   if ($total_record != "" ) {
							 $url = "module_management.php?mid=1&sid=$sid&field=$field&q=$q&LIKE=$LIKE&by=$by&tbl_jury_id=$tbl_jury_id";
				
							$Paging_object = new Paging();
							$Paging_object->paging_new_style_final($url,$offset,$clicked_link,$total_record,TBL_MODULE_PAGING);
						}
		 
		 ?>
                  </td>
                </tr>
              </form>
            </table></td>
        </tr>
      </table>
      <br> 
      <?php } ?>
      <br> <br> <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >
        <tr > 
          <td height="22" align="center" valign="middle"  class="adminDetailHeading" > 
            <strong>&nbsp;Add Module </strong></td>
        </tr>
        <tr > 
          <td align="center" valign="top" class="bordLightBlue"> <form name="frmRegister" method="post" action=""  onSubmit="return frmNewValidation();">
              <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
                <tr align="center" valign="top" > 
                  <td colspan="2"></td>
                </tr>
                <tr align="left" valign="top" > 
                  <td colspan="2"><span class="msgColor">Fields with * are mandatory</span> 
                  </td>
                </tr>
                <tr valign="middle" > 
                  <td align="right"><span class="msgColor">*</span>Module Category 
                    Name: </td>
                  <td> 
                   <?
					$qry_m = "SELECT * FROM ".TBL_MODULE_CATEGORIES." WHERE is_active='Y'";
					$rs_qry_mod_cat = SelectMultiRecords($qry_m);
                   ?>
                    <div id="div_sub_cat"> 
                      <select name="tbl_module_cat_id" class="flat" style="width:225px;">
                        <? for ($y=0; $y<count($rs_qry_mod_cat); $y++) { 							
							$tbl_module_cat_id = $rs_qry_mod_cat[$y]['tbl_module_cat_id'];
							$module_cat_name = $rs_qry_mod_cat[$y]['module_cat_name'];
				  ?>
                        <option value="<?=$tbl_module_cat_id?>" <? if ($y==0){echo 'selected';} ?>> 
                        <?=$module_cat_name?>
                        </option>
                        <? } ?>
                      </select>
                    </div></td>
                </tr>
                <tr valign="middle" > 
                  <td width="28%" align="right"><span class="msgColor">*</span>Module 
                    Name: </td>
                  <td> <input name="module_name" type="text" class="flat" id="module_name" value="<? echo htmlspecialchars($module_name);?>" size="35"> 
                  </td>
                </tr>
                <tr valign="middle" > 
                  <td align="right" valign="top">Description: </td>
                  <td><textarea name="module_desc" class="flat" cols="40" rows="5"><?=$module_desc?></textarea></td>
                </tr>
                <tr valign="middle" > 
                  <td align="right" valign="top"><span class="msgColor">*</span>Source 
                    Page URL:</td>
                  <td>
						<input name="module_source_url" type="text" class="flat" id="module_source_url"  size="80"></td>
                </tr>
                <tr valign="middle" > 
                  <td align="right">Active:</td>
                  <td> <input name="is_active" type="radio" value="Y" checked <?php if($is_active=='Y'){echo "checked";}?>>
                    Yes 
                    <input type="radio" name="is_active" value="N" <?php if($is_active=='N'){echo "checked";}?>>
                    No </td>
                </tr>
                <tr valign="middle">
				  <td>&nbsp;</td> 
                  <td align="left"> <input type="hidden" name="tbl_modules_id" value="<?=$tbl_modules_id?>" class="flat"> 
                    <input type="reset" name="Reset" value="Reset" class="flat"> 
                    <input name="ssubmit" type="submit" class="flat" id="ssubmit"  value="Submit" onClick="return changeStatus(1);"> 
                    <input name="ssubmit" type="hidden" id="ssubmit" value="insertModule"> 
                    <input name="sid" type="hidden" value="<?=$sid?>"> 
					<input name="mid" type="hidden" value="1"> 
                    <input name="offset" type="hidden" id="offset" value="<?=$offset;?>"> 
                    <input name="q" type="hidden" id="q" value="<?php echo htmlspecialchars($q);?>"> 
                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>"> 
                    <input name="by" type="hidden" id="by" value="<?=$by?>"> 
					<input name="field" type="hidden" id="field" value="<?=$field?>"> 
                    <input name="LIKE" type="hidden" value="<?=$LIKE?>"> </td>
                </tr>
                <tr valign="middle" > 
                  <td colspan="2" align="center"> </tr>
              </table>
            </form></td>
        </tr>
      </table></td>
  </tr>
</table>
<?php }if($mid=="2"){ 
	$data = selectFrom("SELECT * FROM ".TBL_MODULES." WHERE tbl_module_id='$tbl_module_id'");
	
	$tbl_module_cat_id =  $data["tbl_module_cat_id"];
	$module_name =  $data["module_name"];
	$module_desc =  $data["module_desc"];
	$module_source_url =  $data["module_source_url"];	
	$added_by				= $data['added_by'];
	$added_date				= $data['added_date'];
	$approved_by			= $data['approved_by'];
	$approved_date			= $data['approved_date'];
	$last_modified_by		= $data['last_modified_by'];
	$last_modified_date		= $data['last_modified_date'];
	$is_active 				= $data["is_active"];
	
	$qry = "SELECT module_cat_name FROM ".TBL_MODULE_CATEGORIES." WHERE tbl_module_cat_id='".$tbl_module_cat_id."'";
	$data_cn = selectFrom($qry);
	$module_cat_name = $data_cn["module_cat_name"];
	
?>
<table width="100%" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr> 
    <td  valign="center" height='30'  class="adminDetailHeading">&nbsp;Module Management 
      [Detail]</td>
  </tr>
  <tr> 
    <td height="200" align="center" valign="top"> <br> <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >
        <tr > 
          <td height="22" align="center" valign="middle"  class="adminDetailHeading">&nbsp;Module 
            Detail</td>
        </tr>
        <tr > 
          <td align="center" valign="top" class="bordLightBlue"><table width="100%" border="0" align="center" cellpadding="3" cellspacing="0" >
              <tr align="center" valign="top" > 
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr valign="top" > 
                <td align="right"><strong>Module Category Name:</strong></td>
                <td><? echo $module_cat_name;	?>&nbsp;</td>
              </tr>
              <tr valign="top" > 
                <td width="31%" align="right"><strong>Module Name:</strong></td>
                <td width="69%"> 
                  <?=$module_name?>                </td>
              </tr>
              <tr valign="top" > 
                <td align="right"><strong> Description:</strong></td>
                <td> 
                  <?=$module_desc?>                </td>
              </tr>
              <tr valign="top" > 
                <td align="right"><strong> Source Page URL:</strong></td>
                <td> 
                  <?=$module_source_url?>                </td>
              </tr>
		      <tr valign="top" > 
                <td width="31%" align="right"><strong>Added By :</strong></td>
                <td width="69%"> <?=$added_by?></td>
              </tr>
              <tr valign="top" > 
                <td align="right"><strong>Added Date :</strong></td>
                <td><?=$added_date?></td>
              </tr>
              <tr valign="top" > 
                <td align="right"><strong>Approved By :</strong></td>
                <td> <?=$approved_by?></td>
              </tr>
              <tr valign="top" > 
                <td align="right"><strong>Approved Date :</strong></td>
                <td><?php echo $approved_date!='0000-00-00 00:00:00' ? $approved_date : "N/A";?></td>
              </tr>
              <tr valign="top" > 
                <td align="right"><strong>Last Modified By :</strong></td>
                <td> <?php echo $last_modified_by!='' ? $last_modified_by : "N/A";?></td>
              </tr>
              <tr valign="top" > 
                <td align="right"><strong>Last Modified Date :</strong></td>
                <td><?php echo $last_modified_date!='0000-00-00 00:00:00' ? $last_modified_date : "N/A";?></td>
              </tr>

              <tr valign="top" > 
                <td align="right"><strong>Status:</strong></td>
                <td><?php  if($is_active=='Y'){echo "Active";}else{echo "Deactive";}?></td>
              </tr>
              <tr> 
                <td colspan="2" height="10"></td>
              </tr>
              <tr valign="middle" > 
                <td align="right">&nbsp;</td>
                <td align="left"> 
                  <?php $tbl_module_cat_id=$data["tbl_module_cat_id"];?>
                  <a href="module_management.php?mid=1&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&sort=<?=$sort?>&by=<?=$by?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&tbl_jury_id=<?=$tbl_jury_id?>" class="detailLinkColor"> 
                  Back </a> </td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
<?php }if($mid=="3"){ 


	$data = selectFrom("SELECT * FROM ".TBL_MODULES." WHERE tbl_module_id='$tbl_module_id'");
		$tbl_module_cat_id = $data["tbl_module_cat_id"];
		$module_name = $data["module_name"];
		$module_desc = $data["module_desc"];
		$added_date = $data["added_date"];
		$is_active = $data["is_active"];
		$module_source_url = $data["module_source_url"];
				
?>
<table width="100%" height="451" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr> 
    <td  valign="center" height='30'  class="adminDetailHeading">&nbsp;Module Management 
      [Edit] </td>
  </tr>
  <tr> 
    <td align="center" valign="top"> <br> <span class="msgColor"><?php echo $MSG;?></span> 
      <br> <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >
        <tr > 
          <td height="22" align="center" valign="middle"  class="adminDetailHeading" >&nbsp;Edit 
            Module </td>
        </tr>
        <tr > 
          <td align="left" valign="top" class="bordLightBlue"> 
		  <form name="frmRegister" method="post" action=""  onSubmit="return frmNewValidation();">
              <table width="100%" border="0" cellpadding="3" cellspacing="0" >
                <tr align="center" valign="top" > 
                  <td colspan="2"></td>
                </tr>
                <tr align="left" valign="top" > 
                  <td colspan="2">&nbsp; </td>
                </tr>
                <tr align="left" valign="top" > 
                  <td colspan="2"><span class="msgColor">Fields with * are mandatory</span>                  </td>
                </tr>
                <tr align="center" valign="top" > 
                  <td colspan="2">`</td>
                </tr>
                <tr valign="middle" > 
                  <td align="right"><span class="msgColor">*</span>Module Category Name: </td>
                  <td> 
                    <?
					$qry_mod_cat = "SELECT * FROM ".TBL_MODULE_CATEGORIES." WHERE is_active='Y'";
					
					//echo $qry_mod_cat;
					$rs_qry_mod_cat = SelectMultiRecords($qry_mod_cat);
				  ?>
                    <div id="div_sub_cat"> 
                      <select name="tbl_module_cat_id" class="flat">
                        <? for ($y=0; $y<count($rs_qry_mod_cat); $y++) { 
				   			$db_module_cat_id = $rs_qry_mod_cat[$y]["tbl_module_cat_id"];
				   			$module_cat_name = $rs_qry_mod_cat[$y]["module_cat_name"];
				  		?>
                        <option value="<?=$db_module_cat_id?>" 
						<? if ($db_module_cat_id==$tbl_module_cat_id){?> selected="selected" <? }?> > 
                        <?=$module_cat_name?>
                        </option>
                        <? } ?>
                      </select>
                    </div></td>
                </tr>
                <tr valign="middle" > 
                  <td width="28%" align="right"><span class="msgColor">*</span>Module 
                    Name: </td>
                  <td> <input name="module_name" type="text" class="flat" id="module_name" value="<?=$module_name?>" size="35">                  </td>
                </tr>
                <tr valign="middle" > 
                  <td align="right" valign="top">Description: </td>
                  <td valign="top"> <textarea name="module_desc" class="flat" cols="40" rows="5"><?=$module_desc?></textarea>                  </td>
                </tr>
                <tr valign="middle" > 
                  <td align="right" valign="top"><span class="msgColor">*</span>Source 
                    Page URL:</td>
                  <td> <input name="module_source_url" type="text" class="flat" id="module_source_url" value="<?=$module_source_url?>"  size="80"></td>
                </tr>
                <tr valign="middle" > 
                  <td align="right">Active:</td>
                  <td> <input name="is_active" type="radio" value="Y" checked  <?php if($is_active=='Y'){echo "checked";}?>>
                    Yes 
                    <input type="radio" name="is_active" value="N" <?php if($is_active=='N'){echo "checked";}?>>
                    No </td>
                </tr>
                <tr valign="middle">
				  <td></td> 
                  <td align="left"> <a href="module_management.php?mid=1&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&sort=<?=$sort?>&by=<?=$by?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>" class="detailLinkColor"> 
                    Back</a>&nbsp; <input name="Reset" type="reset" class="flat" id="Reset" value="Reset Values"> 
                    <input name="ssubmit" type="submit" class="flat" id="ssubmit"  value="Save Changes"> 
                    <input name="ssubmit" type="hidden" id="ssubmit" value="editModule"> 
                    <input name="sid" type="hidden" id="sid" value="<?=$sid?>"> 
                    <input name="tbl_module_id" type="hidden" id="sid" value="<?=$tbl_module_id?>"> 
                    <input name="mid" type="hidden" id="mid" value="1"> <input name="offset" type="hidden" id="offset" value="<?=$offset;?>"> 
                    <input name="q" type="hidden" id="q" value="<? echo htmlspecialchars($q);?>"> 
                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>"> 
                    <input name="field" type="hidden" id="field" value="<?=$field?>"> 
                    <input name="LIKE" type="hidden" id="LIKE" value="<?=$LIKE?>">                  
					</td>
                </tr>
                <tr valign="middle" > 
                  <td colspan="2" align="center"> </tr>
              </table>
            </form></td>
        </tr>
      </table>
      <p><br>
      </p></td>
  </tr>
</table>
<?php }if($mid=="4"){ ?>
<table width="100%" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr> 
    <td  valign="center" height='30'  class="adminDetailHeading">&nbsp;Module Management 
      [Del Confirmation]</td>
  </tr>
  <tr align="center" > 
    <td valign="top" > <br>
      <span class="msgColor"> </span><br>
      <span class="msgColor"> Are you sure you want to delete the selected Module 
      (s)?</span> <br> 
      <table width="80%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="center" valign="top" class="adminDetailHeading"> <table width="100%" border="0" align="center"  cellpadding="2" cellspacing="1" >
              <form name="frmDirectory" onSubmit="return valueCheckedForModuleManagement();">
                <tr > 
                  <td width="20%" height="24" align="center"  class="adminDetailHeading">Module 
                    Name</td>
                  <td width="15%" align="center"  class="adminDetailHeading">Date</td>
                  <td width="7%" align="center"  class="adminDetailHeading">Status</td>
                  <td width="3%" height="24" align="center"  class="adminDetailHeading"> 
                    <input name="AC" type="checkbox" id="AC" onClick="CheckAll();" checked> 
                  </td>
                </tr>
                <?php for($i=0;$i<count($EditBox);$i++){
			//echo $EditBox[$i]."<br>";
			$Query ="SELECT * FROM ".TBL_MODULES." WHERE tbl_module_id='$EditBox[$i]'";
			//echo $Query;
			$rows = selectFrom($Query);

				$tbl_module_id = $rows["tbl_module_id"];
				$tbl_module_cat_id = $rows["tbl_module_cat_id"];
				$module_name = $rows["module_name"];
				$module_desc = $rows["module_desc"];
				$added_date = $rows["added_date"];
				$is_active = $rows["is_active"];
		?>
                <tr valign="middle" bgcolor="#FFFFFF"> 
                  <td height="20" align="center"> &nbsp; 
                    <?=$module_name?>
                  </td>
                  <td height="20" align="center"><? echo dateFormat($added_date);?></td>
                  <td height="20" align="center"> 
                    <?php 	
		  if($is_active == 'Y'){?>
                    <img src="<?=IMG_PATH?>/yes.gif"> 
                    <?php } else {?>
                    <img src="<?=IMG_PATH?>/no.gif"> 
                    <?php }	 ?>
                  </td>
                  <td height="20" align="center"> <input name="EditBox[]" type="checkbox" id="EditBox[]" onClick="UnCheckAll();" value="<?=$tbl_module_id?>" checked> 
                  </td>
                </tr>
                <?php }?>
                <tr> 
                  <td height="20" colspan="6" align="center" valign="middle" bgcolor="#FFFFFF" > 
                    <input name="Button" type="button" class="flat" id="hide" value="I am not sure" onClick="javascript:history.back();"> 
                    <input name="show" type="submit" class="flat" id="show" value="Yes I am sure"> 
                    <input name="sid" type="hidden" id="sid" value="<?=$sid;?>"> 
                    <input name="offset" type="hidden" id="offset" value="<?=$offset;?>"> 
                    <input name="mid" type="hidden" id="mid" value="1"> <input name="ssubmit" type="hidden" id="show" value="moduleDelConfirmation"> 
                    <input name="q" type="hidden" id="q" value="<?php echo htmlspecialchars($q);?>"> 
                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>"> 
                    <input name="by" type="hidden" id="by" value="<?=$by?>"> <input name="field" type="hidden" id="field" value="<?=$field?>"> 
                    <input name="LIKE" type="hidden" value="<?=$LIKE?>"> </td>
                </tr>
              </form>
            </table></td>
        </tr>
      </table></tr>
</table>
<?php } ?>
</body>
</html>
