<?php include ($_SERVER["DOCUMENT_ROOT"]."/aqdar/includes/config.inc.php"); ?>
<?php
$sid = $_REQUEST["sid"];
if (!loggedUser($sid)) { 
	redirect (HOST_URL_ADMIN ."/include/messages.php?msg=relogin");
exit();
}
?>
<?php
$LIB_CLASSES_FOLDER = INC_ADMIN_PATH."/lib/classes/";
								


include($LIB_CLASSES_FOLDER."Paging.php");

$user_id = loggedUser($sid);

$mid = $_REQUEST["mid"];
$field = $_REQUEST["field"];
$q = $_REQUEST["q"];
$offset = $_REQUEST["offset"];
$LIKE = $_REQUEST["LIKE"];
$by = $_REQUEST["by"];
$ssubmit = $_REQUEST["ssubmit"];
$show = $_REQUEST["show"];
$hide = $_REQUEST["hide"];

$AC = $_REQUEST["AC"];
$id = $_REQUEST["id"];
$is_active = $_REQUEST["is_active"];
$module_cat_name = $_REQUEST["module_cat_name"];
$module_cat_desc = $_REQUEST["module_cat_desc"];



$q = stripslashes($q);	
if($ssubmit=="insertModuleCat"){
		$query = "SELECT * FROM ".TBL_MODULE_CATEGORIES." WHERE module_cat_name='$module_cat_name' ";
		if (isExist($query)) {
			$MSG = "<b>[$module_cat_name]</b> already exists</b>";		
		} else {
		
		$maxRes=getMaxId(TBL_MODULE_CATEGORIES,"id");
		$id=$maxRes[0]+1;
		
		$tbl_module_cat_id = md5(uniqid(rand()));	
		$tbl_module_cat_id = substr($tbl_module_cat_id,0,10);
		$is_active  = $_REQUEST['is_active'];;
	
		if($is_active=='Y'){
			$approved_by      = $user_id;
			$approved_date	  = date("Y-m-d H:i:s");
		}else{
			$approved_by      = "N/A";
			$approved_date	  = "N/A";
		}					


		
			$qry = "INSERT INTO ".TBL_MODULE_CATEGORIES." (`id`, `tbl_module_cat_id` ,`module_cat_name`, `module_cat_desc`, `added_by`, `added_date`, `approved_by`, `approved_date`, is_active) VALUES ('$id', '$tbl_module_cat_id', '$module_cat_name', '$module_cat_desc', '$user_id', NOW(), '$approved_by',  '$approved_date', '".$is_active."')";

			//echo $qry;
			insertInto($qry);
			
			$module_cat_name = "";
			$module_cat_desc = "";
			$MSG = "Module Category created successfully!";
		}
	$mid=1;
}

if($ssubmit=="editModuleCat"){
		$query = "SELECT * FROM ".TBL_MODULE_CATEGORIES." WHERE module_cat_name='$module_cat_name' AND tbl_module_cat_id<>$tbl_module_cat_id ";
		//echo $query;
		if (isExist($query)) {
			$MSG = "<b>[$module_cat_name]</b> already exists</b>";		
			$mid = 3;
		} else {

			$qry = "UPDATE ".TBL_MODULE_CATEGORIES." SET module_cat_name='$module_cat_name', module_cat_desc='$module_cat_desc', `last_modified_by` = '$user_id', `last_modified_date` = NOW(), is_active='$is_active' WHERE tbl_module_cat_id='$tbl_module_cat_id'";  
			update($qry);
			//echo $qry;			
			$mid = 1;
				
			$module_cat_name = "";
			$MSG = "Module Category updated successfully!";
		}
}

if($ssubmit=="moduleDetail"){
		if(isset($show)){
				for ($j=0; $j<count($EditBox); $j++){ 
					$Querry = "UPDATE ". TBL_MODULE_CATEGORIES ." SET approved_by='$user_id', approved_date = NOW(), last_modified_by='$user_id', last_modified_date = NOW(), is_active='Y' WHERE tbl_module_cat_id='$EditBox[$j]'";			
					//echo $Querry;
					update($Querry);
					$mid=1;	
					$MSG = "Selected Module Category(s) have been activated successfully!";
				}
		}
		if(isset($hide)){
				for ($j=0; $j<count($EditBox); $j++){ 
					$Querry = "UPDATE ". TBL_MODULE_CATEGORIES ." SET approved_by='N/A', approved_date = '0000-00-00 00:00:00', last_modified_by='$user_id', last_modified_date = NOW(), is_active='N' WHERE tbl_module_cat_id='$EditBox[$j]'";			
					//echo $Querry;
					update($Querry);
					$mid=1;	
					$MSG = "Selected Module Category(s) have been deactivated successfully!";
				}
			}
		if(isset($delete)){$mid = 4;}
	}

if($ssubmit == "moduleDelConfirmation"){
		
		$EditBox=$_REQUEST['EditBox'];
		for($i=0;$i<count($EditBox);$i++){

				deleteFrom("DELETE FROM ".TBL_MODULES." WHERE tbl_module_cat_id ='".$EditBox[$i]."'");
				deleteFrom("DELETE FROM ".TBL_MODULE_CATEGORIES." WHERE tbl_module_cat_id ='".$EditBox[$i]."'");

				$mid = 1;
				$MSG = "Selected Module Category(s) have been deleted successfully!";
			
		}//for
}	

?>
<html>
<head>
<title>Admin (Module Category Management)</title>
<link href="<?=HOST_URL?>/admin/css/interface.css" type=text/css rel=stylesheet>
<script src="<?=ADMIN_SCRIPT_PATH?>/validation.js">CopyDatarichtext1();</script>
<script src="<?=SCRIPTS_FOLDER?>/commonfunction.js"></script>
<script language="JavaScript">
function valueCheckedForModuleCatManagement(){
	var ml = document.frmDirectory;
	var len = ml.elements.length;
	for (var i = 0; i < len; i++){
	   	if (document.frmDirectory.elements[i].checked){
			return true;
		}
	}
	 alert ("Select at least one Module Category!");
	 return false;
}

function CheckAll(){
	var ml = document.frmDirectory;
	var len = ml.elements.length;
	if (document.frmDirectory.AC.checked==true) {
		 for (var i = 0; i < len; i++) {
			document.frmDirectory.elements[i].checked=true;
		 }
	} else {
		  for (var i = 0; i < len; i++)  {
			document.frmDirectory.elements[i].checked=false;
		  }
	}
}
function UnCheckAll() {
	var ml = document.frmDirectory;
	var len = ml.elements.length;
	var count=0; var checked=0;
	    for (var i = 0; i < len; i++) {	       
			if ((document.frmDirectory.elements[i].type=='checkbox') && (document.frmDirectory.elements[i].name != "AC")) {
				count = count + 1;
	  		 	if (document.frmDirectory.elements[i].checked == true){
					checked = checked + 1;
				}
			}
	     }
		 
	if (checked == count) {
		 document.frmDirectory.AC.checked = true;
	} else {
		document.frmDirectory.AC.checked = false;
	}
}
function frmNewValidation() {

	if (checkModuleCatName())
		return true;
	else 
	 return false;
}
function checkModuleCatName () {
	var regExp = / /g;
		var str = document.frmRegister.module_cat_name.value;
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			alert("\nThe Module Category Name is blank. Please write Module Category Name.")
			document.frmRegister.module_cat_name.value = "";
			document.frmRegister.module_cat_name.focus();
		return false;
		}
	return true;
}

var xmlhttp;
function setJuryServices(){

	var tbl_jury_id = getDropDownListValue("tbl_jury_id");
//	alert("setJuryServices tbl_jury_id: "+tbl_jury_id+"text: "+getDropDownListText("tbl_jury_id"));
	var url = "ajax_jury_services.php?tbl_jury_id="+tbl_jury_id;
//	alert(url)
//	document.frmRegister.module_cat_name.value = url;
	xmlhttp=null;
	if (window.XMLHttpRequest) {// code for Firefox, Opera, IE7, etc.
		xmlhttp=new XMLHttpRequest();
	} else if (window.ActiveXObject) {// code for IE6, IE5
	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	if (xmlhttp!=null) {
		xmlhttp.onreadystatechange= state_Change
                                    
		xmlhttp.open("GET",url,true);
		xmlhttp.send(null);
	} else {
		alert("Your browser does not support XMLHTTP.");	
	}
}
function state_Change() {
	if (xmlhttp.readyState==4) {// 4 = "loaded"
		if (xmlhttp.status==200) {// 200 = "OK"
				document.getElementById("div_jury_services").innerHTML=xmlhttp.responseText;
		} else {
			alert("Problem retrieving data:" + xmlhttp.statusText);	
		}
	}
}

</script>
</head>
<?php if (!$offset || $offset<0)  { $offset =0;} ?>
<?php if (!$LIKE)  { $LIKE = "LIKE";} ?>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<STYLE type=text/css>#dek {
	Z-INDEX: 200; VISIBILITY: hidden; POSITION: absolute
}
</STYLE>
<?php if($mid=="1"){ ?>
<table width="100%" height="570" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr> 
    <td  valign="center" height='30'  class="adminDetailHeading">&nbsp;Module Category 
      Management</td>
  </tr>
  <tr align="center" > 
    <td height="538" align="center" valign="top" > <form name="form1" method="post" action="">
        <br>
        <table width="80%" border="0" align="center" cellpadding="0" cellspacing="1" >
          <tr > 
            <td height="22" align="left" valign="middle" class="adminDetailHeading">&nbsp;Search 
              Criteria</td>
          </tr>
          <tr > 
            <td align="center" valign="top" class="bordLightBlue"><table width="100%" border="0" cellspacing="3" cellpadding="2">
                <tr> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr> 
                  <td width="32%">&nbsp;</td>
                  <td width="48%"> <a href="module_cats_management.php?mid=<?=$mid?>&sid=<?=$sid?>&tbl_jury_id=<?=$tbl_jury_id?>"><span class="detailLinkColor">All 
                    Records</span></a>: or&nbsp;</td>
                  <td width="20%"><i></i></td>
                </tr>
                <tr> 
                  <td colspan="3"><hr width="80%" size="1"></td>
                </tr>
                <tr> 
                  <td align="right" valign="top" ><span class="searchText">Field:&nbsp;</span></td>
                  <td align="left" valign="top"> <select name="field" id="field">
                      <option value="module_cat_name" <?PHP if ($field == "module_cat_name"){echo "selected";}?>>Module Category Name</option>
                      <option value="module_cat_desc" <?PHP if ($field == "module_cat_desc"){echo "selected";}?>>Description</option>
                      <option value="added_date" <?PHP if ($field == "added_date"){echo "selected";}?>>Date e.g [YYYY-MM-DD]</option>
                      <option value="is_active"<?PHP if($field == "is_active"){echo "selected";}?>>Status e.g Y=Activate , N=Deactivate</option>
                    </select></td>
                  <td><i></i></td>
                </tr>
                <tr> 
                  <td align="right" valign="top"><span class="searchText">Condition:&nbsp;</span></td>
                  <td align="left" valign="top"> 
                    <?php
							if (!$LIKE) {
								$LIKE = "LIKE";
							}
						?>
                    <select name="LIKE" id="LIKE">
                      <option value="LIKE" <?PHP if ($LIKE == "LIKE"){echo "selected";}?>>LIKE</option>
                      <option value="=" <?PHP if (($LIKE == "=") || ($LIKE == "")){echo "selected";}?>>Equal 
                      To</option>
                      <option value="!=" <?PHP if ($LIKE == "!="){echo "selected";}?>>Not 
                      Equal To</option>
                      <option value="<" <?PHP if ($LIKE == "<"){echo "selected";}?>>Less 
                      Than</option>
                      <option value=">" <?PHP if ($LIKE == ">"){echo "selected";}?>>Greater 
                      Than</option>
                      <option value="<=" <?PHP if ($LIKE == "<="){echo "selected";}?>>Less 
                      Than or Equal To</option>
                      <option value=">=" <?PHP if ($LIKE == ">="){echo "selected";}?>>Greater 
                      Than or Equal To</option>
                    </select></td>
                  <td><i></i></td>
                </tr>
                <tr> 
                  <td align="right" valign="top"><span class="searchText">Search 
                    Name:&nbsp;</span></td>
                  <td align="left" valign="top"> <input name="q" type="text" id="q" value="<?php echo htmlspecialchars($q);?>" size="40"> 
                  </td>
                  <td><i></i></td>
                </tr>
                <tr> 
                  <td align="right" valign="top"><span class="searchText">Order 
                    By:&nbsp;</span></td>
                  <td align="left" valign="top"> <select name="by" id="by">
                      <option value="ASC" <?PHP if (($by == "ASC") || ($by == "")) { echo "selected";}?>>ASCENDING</option>
                      <option value="DESC" <?PHP if ($by == "DESC"){ echo "selected";}?>>DESCENDING</option>
                    </select> &nbsp;</td>
                  <td><i></i></td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                  <td align="left"> <input name="mid" type="hidden" value="1">
				  <input name="tbl_jury_id" type="hidden" id="sid" value="<?=$tbl_jury_id?>"> 
                    <input name="offset" type="hidden" value="0"> <input name="Search" type="submit" id="Search" value="Search" class="flat"> 
                    &nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
              </table></td>
          </tr>
        </table>
      </form>
      <br> <span class="msgColor"><?php echo $MSG;?></span> <br> 
      <?php 
		$q = addslashes($q);
		$CountRec .= "SELECT * FROM ".TBL_MODULE_CATEGORIES."";	
			
		$Query .= "SELECT * FROM ".TBL_MODULE_CATEGORIES."";	
		if(!$by){$by="ASC";}
		
		if($field && !empty($q)){	    	
			
			if($LIKE=="LIKE"){
					$CountRec .= " AND `$field` $LIKE '%$q%' ";
					$Query .= " AND `$field` $LIKE '%$q%' ";
			}else{
					$CountRec .= " AND `$field` $LIKE '$q' ";	
					$Query .= " AND `$field` $LIKE '$q' ";
			}
		}	
		$Query .= " ORDER BY tbl_module_cat_id $by ";
		$Query .=" LIMIT $offset, ".TBL_MODULE_CATEGORY_PAGING;
		
		$q = stripslashes($q);
		//echo $Query;
		$total_record = CountRecords($CountRec);
		//exit();
		$data = SelectMultiRecords($Query);
		  		 if ($total_record =="")
					   echo '<span class="msgColor">'.NO_RECORD_FOUND."</span>";
					else{	
					    echo '<span class="msgColor">';
					    echo " <b> ". $total_record ." </b> Module Category(s) found. Showing <b>";
						if ($total_record>$offset){
							echo $offset+1;
							echo " </b>to<b> ";
							if ($offset >= $total_record - TBL_MODULE_CATEGORY_PAGING)	{ 
								  echo $total_record; 
							}else { 
							   	echo $offset + TBL_MODULE_CATEGORY_PAGING;
							}
						}else{ 
							echo $offset+1;
							echo "</b> - ". $total_record;
							echo " to ". $total_record ." Module Category ";		
						}
						echo "</b>.</span>";	
					}
	   ?>
      <?php  if ($total_record !=""){?>
      <table width="98%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td height="18"><span class="msgColor">&nbsp;&nbsp;Note: To view detail 
            click a Module Category Name.</span></td>
        </tr>
        <tr> 
          <td align="center" valign="top" class="adminDetailHeading"> <table width="100%" border="0" align="center"  cellpadding="2" cellspacing="1">
              <form name="frmDirectory" onSubmit="return valueCheckedForModuleCatManagement()">
                <tr align="center" > 
                  <td width="54%" align="center" class="adminDetailHeading">Module Category Name </td>
                  <td width="16%" height="24" align="center" class="adminDetailHeading">Date</td>
                  <td width="14%" height="24" align="center" class="adminDetailHeading">Status</td>
                  <td width="10%" height="24" align="center" class="adminDetailHeading">Action</td>
                  <td width="6%" height="24" class="adminDetailHeading"> <input name="AC" type="checkbox" onClick="CheckAll();"></td>
                </tr>
                <?php for($i=0;$i<count($data);$i++){
						$tbl_module_cat_id = $data[$i]["tbl_module_cat_id"];
						$module_cat_name_db = $data[$i]["module_cat_name"];
						$module_cat_desc_db = $data[$i]["module_cat_desc"];
						$added_date = $data[$i]["added_date"];
						$is_active = $data[$i]["is_active"];
						
				?>
                <tr valign="middle" bgcolor="#FFFFFF"> 
                  <td height="20" align="left" >&nbsp; <a href="?mid=2&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&by=<?=$by?>&tbl_module_cat_id=<?=$tbl_module_cat_id?>&tbl_jury_id=<?=$tbl_jury_id?>" title="Click to view detail" class="detailLinkColor"> 
                    <?=$module_cat_name_db?>
                    </a> </td>
                  <td height="20" align="center" ><? echo dateFormat($added_date);?></td>
                  <td align="center"> 
                    <?php 
		  if($is_active == 'Y'){?>
                    <img src="<?=IMG_PATH?>/yes.gif"> 
                    <?php } else {?>
                    <img src="<?=IMG_PATH?>/no.gif"> 
                    <?php }  ?>
                  </td>
                  <td align="center"> <a href="?mid=3&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&by=<?=$by?>&tbl_module_cat_id=<?=$tbl_module_cat_id?>" class="detailLinkColor">Edit</a> 
                  </td>
                  <td align="center" > <input name="EditBox[]" type="checkbox" onClick="UnCheckAll();" value="<?=$tbl_module_cat_id?>"> 
                  </td>
                </tr>
                <?php }?>
                <tr> 
                  <td height="20" colspan="8" align="center" valign="middle" bgcolor="#FFFFFF"> 
                    <input name="reset" type="reset" class="flat"  value="Reset"> 
                    <input name="show" type="submit" class="flat" id="ssubmit" value="Active"> 
                    <input name="hide" type="submit" class="flat"  value="Deactive"> 
                    <input name="delete" type="submit" class="flat"  value="Delete"> 
                    <input name="ssubmit" type="hidden" id="ssubmit" value="moduleDetail"> 
                    <input name="mid" type="hidden" id="mid2" value="1"> <input name="sid" type="hidden" id="sid" value="<?=$sid?>"> 
                    <input name="offset" type="hidden" id="offset" value="<?=$offset?>"> 
                    <input name="q" type="hidden" id="q" value="<? echo htmlspecialchars($q);?>"> 
                    <input name="id" type="hidden" value="<?=$tbl_module_cat_id?>"> <input name="sort" type="hidden" id="sort" value="<?=$sort?>"> 
                    <input name="by" type="hidden" id="by" value="<?=$by?>"> <input name="field" type="hidden" id="field" value="<?=$field?>"> 
                    <input name="LIKE" type="hidden" value="<?=$LIKE?>"></td>
                </tr>
                <tr align="right"> 
                  <td height="20" colspan="8" valign="middle" bgcolor="#FFFFFF"> 
                    <?php 
					   if ($total_record != "" ) {
							 $url = "module_cats_management.php?mid=1&sid=$sid&field=$field&q=$q&LIKE=$LIKE&by=$by&tbl_jury_id=$tbl_jury_id";
							$Paging_object = new Paging();
							$Paging_object->paging_new_style_final($url,$offset,$clicked_link,$total_record,TBL_MODULE_CATEGORY_PAGING);
						}
					?>
                  </td>
                </tr>
              </form>
            </table></td>
        </tr>
      </table>
      <br> 
      <?php }  ?>
      <br> <br> <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >
        <tr > 
          <td height="22" align="center" valign="middle"  class="adminDetailHeading" > 
            <strong>&nbsp;Add Module Category</strong></td>
        </tr>
        <tr > 
          <td align="center" valign="top" class="bordLightBlue">
           <form name="frmRegister" method="post" action=""  onSubmit="return frmNewValidation();">
              <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" >
                <tr align="center" valign="top" > 
                  <td colspan="2"></td>
                </tr>
                <tr align="left" valign="top" > 
                  <td colspan="2"><span class="msgColor">Fields with * are mandatory</span>                  </td>
                </tr>
                <tr valign="middle" > 
                  <td width="28%" align="right"><span class="msgColor">*</span>Module 
                    Category Name: </td>
                  <td> 
                    <input name="module_cat_name" type="text" class="flat" id="module_cat_name" value="<? echo htmlspecialchars($module_cat_name);?>" size="35">                  </td>
                </tr>
                <tr valign="middle" > 
                  <td align="right" valign="top">Description: </td>
                  <td><textarea name="module_cat_desc" class="flat" cols="40" rows="5"><?=$module_cat_desc?></textarea></td>
                </tr>
                <tr valign="middle" > 
                  <td align="right">Active:</td>
                  <td> <input name="is_active" type="radio" value="Y" checked <?php if($is_active=='Y'){echo "checked";}?>>
                    Yes 
                    <input type="radio" name="is_active" value="N" <?php if($is_active=='N'){echo "checked";}?>>
                    No </td>
                </tr>
                <tr valign="middle" > 
				  <td>&nbsp;</td>
                  <td align="left"> 
				    <input type="hidden" name="tbl_module_categories_id" value="<?=$tbl_module_categories_id?>" class="flat"> 
                    <input type="reset" name="Reset" value="Reset" class="flat">
					<input name="tbl_jury_id" type="hidden" id="sid" value="<?=$tbl_jury_id?>"> 
                    <input name="ssubmit" type="submit" class="flat" id="ssubmit"  value="Submit"> 
                    <input name="ssubmit" type="hidden" id="ssubmit" value="insertModuleCat"> 
                    <input name="sid" type="hidden" value="<?=$sid?>"> <input name="mid" type="hidden" value="1"> 
                    <input name="offset" type="hidden" id="offset" value="<?=$offset;?>"> 
                    <input name="q" type="hidden" id="q" value="<?php echo htmlspecialchars($q);?>"> 
                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>"> 
                    <input name="by" type="hidden" id="by" value="<?=$by?>"> <input name="field" type="hidden" id="field" value="<?=$field?>"> 
                    <input name="LIKE" type="hidden" value="<?=$LIKE?>">
                    
                  </td>
                </tr>
                <tr valign="middle" > 
                  <td colspan="2" align="center"> </tr>
              </table>
            </form>
           
            </td>
        </tr>
      </table> <br></td>
  </tr>
</table>
<?php }if($mid=="2"){ 
	//echo "SELECT * FROM ".TBL_MODULE_CATEGORIES." WHERE id=$id";
	$data = selectFrom("SELECT * FROM ".TBL_MODULE_CATEGORIES." WHERE tbl_module_cat_id='".$_REQUEST['tbl_module_cat_id']."'");
	
	$tbl_module_cat_id = $data["tbl_module_cat_id"];	 
	$module_cat_name = $data["module_cat_name"];	 
	$module_cat_desc = $data["module_cat_desc"];	 
	$added_by				= $data['added_by'];
	$added_date				= $data['added_date'];
	$approved_by			= $data['approved_by'];
	$approved_date			= $data['approved_date'];
	$last_modified_by		= $data['last_modified_by'];
	$last_modified_date		= $data['last_modified_date'];
	$is_active 				= $data["is_active"];

?>
<table width="100%" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr> 
    <td  valign="center" height='30'  class="adminDetailHeading">&nbsp;Module Category 
      Management [Detail]</td>
  </tr>
  <tr> 
    <td height="200" align="center" valign="top"> <br> <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >
        <tr > 
          <td height="22" align="center" valign="middle"  class="adminDetailHeading">&nbsp;Module 
            Category Detail</td>
        </tr>
        <tr > 
          <td align="center" valign="top" class="bordLightBlue"><table width="100%" border="0" align="center" cellpadding="3" cellspacing="0" >
              <tr align="center" valign="top" > 
                <td colspan="2">&nbsp;</td>
              </tr>
              <tr valign="top" > 
                <td width="30%" align="right"><strong>Module Category Name:</strong></td>
                <td width="70%"> 
                  <?=$module_cat_name?>                </td>
              </tr>
              <tr valign="top" > 
                <td align="right"><strong> Description:</strong></td>
                <td> 
                  <?=$module_cat_desc?>
                </td>
              </tr>
		      <tr valign="top" > 
                <td width="30%" align="right"><strong>Added By :</strong></td>
                <td width="70%"> <?=$added_by?></td>
              </tr>
              <tr valign="top" > 
                <td align="right"><strong>Added Date :</strong></td>
                <td><?=$added_date?></td>
              </tr>
              <tr valign="top" > 
                <td align="right"><strong>Approved By :</strong></td>
                <td> <?=$approved_by?></td>
              </tr>
              <tr valign="top" > 
                <td align="right"><strong>Approved Date :</strong></td>
                <td><?php echo $approved_date!='0000-00-00 00:00:00' ? $approved_date : "N/A";?></td>
              </tr>
              <tr valign="top" > 
                <td align="right"><strong>Last Modified By :</strong></td>
                <td> <?php echo $last_modified_by!='' ? $last_modified_by : "N/A";?></td>
              </tr>
              <tr valign="top" > 
                <td align="right"><strong>Last Modified Date :</strong></td>
                <td><?php echo $last_modified_date!='0000-00-00 00:00:00' ? $last_modified_date : "N/A";?></td>
              </tr>

              <tr valign="top" > 
                <td align="right"><strong>Status:</strong></td>
                <td><?php  if($is_active=='Y'){echo "Active";}else{echo "Deactive";}?></td>
              </tr>
              <tr align="center" valign="top" > 
                <td align="right">&nbsp;</td>
                <td align="left">&nbsp;</td>
              </tr>
              <tr valign="middle">
			    <td>&nbsp;</td> 
                <td align="left"> 
                  <?php $tbl_module_cat_id=$data["tbl_module_cat_id"];?>
                  <a href="module_cats_management.php?mid=1&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&sort=<?=$sort?>&by=<?=$by?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>&tbl_jury_id=<?=$tbl_jury_id?>" class="detailLinkColor"> 
                  Back </a> </td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
<?php }if($mid=="3"){ 
	$data = selectFrom("SELECT * FROM ".TBL_MODULE_CATEGORIES." WHERE tbl_module_cat_id='$tbl_module_cat_id'");
		$tbl_module_cat_id = $data["tbl_module_cat_id"];
		$module_cat_name = $data["module_cat_name"];
		$module_cat_desc = $data["module_cat_desc"];
		$added_date = $data["added_date"];
		$is_active = $data["is_active"];
?>
<table width="100%" height="451" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr> 
    <td  valign="center" height='30'  class="adminDetailHeading">&nbsp;Module Category 
      Management [Edit] </td>
  </tr>
  <tr> 
    <td align="center" valign="top"> <br> <span class="msgColor"><?php echo $MSG;?></span> 
      <br> <table width="90%" border="0" align="center" cellpadding="0" cellspacing="1" >
        <tr > 
          <td height="22" align="center" valign="middle"  class="adminDetailHeading" >&nbsp;Edit 
            Module Category</td>
        </tr>
        <tr > 
          <td align="left" valign="top" class="bordLightBlue"> <form name="frmRegister" method="post" action=""  onSubmit="return frmNewValidation();">
              <table width="100%" border="0" cellpadding="3" cellspacing="0" >
                <tr align="center" valign="top" > 
                  <td colspan="2"></td>
                </tr>
                <tr align="left" valign="top" > 
                  <td colspan="2">&nbsp; </td>
                </tr>
                <tr align="left" valign="top" > 
                  <td colspan="2"><span class="msgColor">Fields with * are mandatory</span> 
                  </td>
                </tr>
                <tr align="center" valign="top" > 
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr valign="middle" > 
                  <td width="28%" align="right"><span class="msgColor">*</span>Module 
                    Category Name: </td>
                  <td> <input name="module_cat_name" type="text" class="flat" id="module_cat_name" value="<?=$module_cat_name?>" size="35"> 
                  </td>
                </tr>
                <tr valign="middle" > 
                  <td align="right" valign="top">Description: </td>
                  <td valign="top"> <textarea name="module_cat_desc" class="flat" cols="40" rows="5"><?=$module_cat_desc?></textarea> 
                  </td>
                </tr>
                <tr valign="middle" > 
                  <td align="right">Active:</td>
                  <td> <input name="is_active" type="radio" value="Y" checked  <?php if($is_active=='Y'){echo "checked";}?>>
                    Yes 
                    <input type="radio" name="is_active" value="N" <?php if($is_active=='N'){echo "checked";}?>>
                    No </td>
                </tr>
                <tr valign="middle"> 
				  <td>&nbsp;</td>
                  <td align="left"> <a href="module_cats_management.php?mid=1&sid=<?=$sid?>&offset=<?=$offset?>&field=<?=$field?>&sort=<?=$sort?>&by=<?=$by?>&q=<? echo rawurlencode($q)?>&LIKE=<?=$LIKE?>" class="detailLinkColor"> 
                    Back</a>&nbsp; <input name="Reset" type="reset" class="flat" id="Reset" value="Reset Values"> 
                    <input name="ssubmit" type="submit" class="flat" id="ssubmit"  value="Save Changes"  > 
                    <input name="ssubmit" type="hidden" id="ssubmit" value="editModuleCat"> 
                    <input name="sid" type="hidden" id="sid" value="<?=$sid?>"> 
                    <input name="tbl_module_cat_id" type="hidden" id="sid" value="<?=$tbl_module_cat_id?>"> 
                    <input name="mid" type="hidden" id="mid" value="1"> 
					<input name="offset" type="hidden" id="offset" value="<?=$offset;?>"> 
                    <input name="q" type="hidden" id="q" value="<? echo htmlspecialchars($q);?>"> 
                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>"> 
                    <input name="field" type="hidden" id="field" value="<?=$field?>"> 
                    <input name="LIKE" type="hidden" id="LIKE" value="<?=$LIKE?>"> 
                    <input name="isRecordSave"  type="hidden" id="isRecordSave" value="No"> 
                  </td>
                </tr>
                <tr valign="middle" > 
                  <td colspan="2" align="center"> </tr>
              </table>
            </form></td>
        </tr>
      </table>
      <p><br>
      </p></td>
  </tr>
</table>
<?php }if($mid=="4"){ ?>
<table width="100%" border="0" align="center"  cellpadding="0" cellspacing="0" class="bordPanelMainAdmin" id="Active_Deactive">
  <tr> 
    <td  valign="center" height='30'  class="adminDetailHeading">&nbsp;Module Category 
      Management [Del Confirmation]</td>
  </tr>
  <tr align="center" > 
    <td valign="top" > <br>
      <span class="msgColor"> </span><br>
      <span class="msgColor"> Are you sure you want to delete the selected Module 
      Category(s)?</span> <br> 
      <table width="80%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="center" valign="top" class="adminDetailHeading"> <table width="100%" border="0" align="center"  cellpadding="2" cellspacing="1" >
              <form name="frmDirectory" onSubmit="return valueCheckedForModuleCatManagement();">
                <tr > 
                  <td width="46%" height="24" align="left" class="adminDetailHeading" style="padding-left:10px;">Module 
                    Category Name</td>
                  <td width="24%" align="center"  class="adminDetailHeading">Date</td>
                  <td width="21%" align="center"  class="adminDetailHeading">Status</td>
                  <td width="9%" height="24" align="center"  class="adminDetailHeading"> 
                    <input name="AC" type="checkbox" id="AC" onClick="CheckAll();" checked> 
                  </td>
                </tr>
                <?php for($i=0;$i<count($EditBox);$i++){
			//echo $EditBox[$i]."<br>";
			$Query ="SELECT * FROM ".TBL_MODULE_CATEGORIES." WHERE tbl_module_cat_id='$EditBox[$i]'";
			//echo $Query;
			$rows = selectFrom($Query);

				$tbl_module_cat_id = $rows["tbl_module_cat_id"];
				$module_cat_name = $rows["module_cat_name"];
				$module_cat_desc = $rows["module_cat_desc"];
				$added_date = $rows["added_date"];
				$is_active = $rows["is_active"];
		?>
                <tr valign="middle" bgcolor="#FFFFFF"> 
                  <td height="20" align="left"> &nbsp; 
                    <?=$module_cat_name?>
                  </td>
                  <td height="20" align="center"><? echo dateFormat($added_date);?></td>
                  <td height="20" align="center"> 
                    <?php 	
		  if($is_active == 'Y'){?>
                    <img src="<?=IMG_PATH?>/yes.gif"> 
                    <?php } else {?>
                    <img src="<?=IMG_PATH?>/no.gif"> 
                    <?php }	 ?>
                  </td>
                  <td height="20" align="center"> <input name="EditBox[]" type="checkbox" id="EditBox[]" onClick="UnCheckAll();" value="<?=$tbl_module_cat_id?>" checked> 
                  </td>
                </tr>
                <?php }?>
                <tr> 
                  <td height="20" colspan="6" align="center" valign="middle" bgcolor="#FFFFFF" > 
                    <input name="Button" type="button" class="flat" id="hide" value="I am not sure" onClick="javascript:history.back();"> 
                    <input name="show" type="submit" class="flat" id="show" value="Yes I am sure"> 
                    <input name="sid" type="hidden" id="sid" value="<?=$sid;?>"> 
                    <input name="offset" type="hidden" id="offset" value="<?=$offset;?>"> 
                    <input name="mid" type="hidden" id="mid" value="1"> 
					<input name="ssubmit" type="hidden" id="show" value="moduleDelConfirmation"> 
                    <input name="q" type="hidden" id="q" value="<?php echo htmlspecialchars($q);?>"> 
                    <input name="sort" type="hidden" id="sort" value="<?=$sort?>"> 
                    <input name="by" type="hidden" id="by" value="<?=$by?>"> <input name="field" type="hidden" id="field" value="<?=$field?>"> 
                    <input name="LIKE" type="hidden" value="<?=$LIKE?>"> </td>
                </tr>
              </form>
            </table></td>
        </tr>
      </table></tr>
</table>
<?php } ?>
</body>
</html>
