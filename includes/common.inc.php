<?php
/*TITLE*/
define("TITLE",":: Administration - Oasis Living Magazine | All About Life &amp; Living in Al Ain");
define("TITLE_USER_SIDE","Oasis Living Magazine | All About Life &amp; Living in Al Ain");


/*PATHS*/
//=>paths user side
define("HOST_URL", "http://" .$_SERVER["HTTP_HOST"]);
define("SCRIPTS_FOLDER", "http://" .$_SERVER["HTTP_HOST"]. "/js");
define("USERS_FOLDER_PATH", "http://" .$_SERVER["HTTP_HOST"]. "/users");
define("NEWS_FOLDER_PATH", "http://" .$_SERVER["HTTP_HOST"]. "/news");
define("INC_PATH", $_SERVER["DOCUMENT_ROOT"] . "/include/");
define("ADMIN_PATH", "http://" .$_SERVER["HTTP_HOST"]. "/admin/include/");
define("CSS_PATH", "http://" .$_SERVER["HTTP_HOST"]. "/css/");
define("IMG_PATH", "http://" .$_SERVER["HTTP_HOST"] . "/images");
//=>paths admin side
define("ADMIN_FOLDERS_PATH", "http://" .$_SERVER["HTTP_HOST"] . "/admin/");
define("ADMIN_CSS_PATH", "http://" .$_SERVER["HTTP_HOST"] . "/admin/css/");
define("ADMIN_IMG_PATH", "http://" .$_SERVER["HTTP_HOST"] . "/admin/images/");
define("IMG_RICH_PATH", "http://" .$_SERVER["HTTP_HOST"] . "/admin/images/Img");
define("ADMIN_SCRIPT_PATH", "http://" .$_SERVER["HTTP_HOST"] . "/admin/scripts");
define("INC_ADMIN_PATH", $_SERVER["DOCUMENT_ROOT"] . "/admin");
define("ADMIN_USERS_PATH", "http://" .$_SERVER["HTTP_HOST"]. "/admin/admin_users_management/");
define("ADMIN_SITES_PATH", "http://" .$_SERVER["HTTP_HOST"]. "/admin/admin_sites_management/");


/*TABLES*/
//=>tables user credentials
define("TBL_ADMIN_USERS", DBASE_MAIN . ".tbladmin_users");
define("TBL_USER_REG", DBASE_MAIN . ".tblusers_regis");
define("TBL_SESSIONS", DBASE_MAIN . ".tblsession");
define("TBL_CATEGORIES", DBASE_MAIN . ".tblcategories");
define("TBL_RIGHTS", DBASE_MAIN . ".tblrights");
define("TBL_SESSIONS_USERS", DBASE_MAIN . ".tblsession_user");
//=>tables others
define("TBL_CAT", DBASE_MAIN . ".tbl_outdoor_categories");
define("TBL_NEWS", DBASE_MAIN . ".tbl_news");
define("TBL_USER", DBASE_MAIN . ".tbl_user");
define("TBL_USER_CARD", DBASE_MAIN . ".tbl_user_card");


/*PAGING*/
define("SITE_PAGING", "2");
define("TBL_NEWS_PAGING", "2");
define("TBL_USER_PAGING", "10");


/*COLORS*/
define("BG_COLOR","#6c9a5d");//set the text in site 
define("BG_LITE_COLOR","#CCDCC7");//set the text in site 
define("TEXT_COLOR","white");//set the text in site 


/*EMAILS*/
define("INFO_SITE_NAME","info@myoasisliving.com");//set the text in site 


/*MISC*/
define("GENERIC_ERROR","MYOASISLIVING");
define("MAIN_SITE_NAME","MYOASISLIVING");//set the text in site 
define("TXT_SITE_NAME","MYOASISLIVING");//set the text in site 
define("TXT_SITE_DOMAIN","www.myoasisliving.com");//set the domain name in site
define("SEC_NAME","<a href='" . HOST_URL ."'><font color='000000'>MYOASISLIVING</font></a>");
define("FONTS_PATH",  $_SERVER["DOCUMENT_ROOT"] . "/fonts");


/*ERROR MESSAGES*/
define("NO_RECORD_FOUND","No Search results found");
define("NO_AUDIO_FOUND","No Record found.");
define("ERR1"," Sorry, the User-ID already exists.");
define("ERR2"," Your User-ID and Password is not correct.");
define("ERR3"," Login first to proceed.");
define("ERR5"," Sorry, you are not allowed to view this page directly.");
define("MSG3","Your Password has been changed successfully.");
define("MSG5","Your Password has been sent to your given e-mail address. Check your mail and then login again.");
define("MSG7","Sorry, incorrect User-ID.");
define("MSG18","Your old password is not correct, Please try again.");
define("MSG20","Your secret answer does not match, Please try again.");
define("NO_RECORD_FOUND","No record found.");
?>