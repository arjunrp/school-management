<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> School Gallery<small> &nbsp;</small> </h1>
    <!--/HEADING--> 
    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Image Galleries</li>
    </ol>
    <!--/BREADCRUMB--> 
     <div class="box-tools" style="float:right;">
        <a href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/my_school_gallery_category/child_id_enc/<?=$tbl_student_id?>/tbl_school_id/<?=$tbl_school_id?>/offset/0"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
    </div> 
    
    <div style="clear:both"></div>
  </section>
  <div style="clear:both;"></div>
   <!-- Syntax Highlighter -->
  <link href="<?=HOST_URL?>/slider/css/shCore.css" rel="stylesheet" type="text/css" />
  <link href="<?=HOST_URL?>/slider/css/shThemeDefault.css" rel="stylesheet" type="text/css" />
  <!-- Demo CSS -->
  <!--<link rel="stylesheet" href="<?=HOST_URL?>/slider/css/demo.css" type="text/css" media="screen" />-->
  <link rel="stylesheet" href="<?=HOST_URL?>/slider/css/flexslider.css" type="text/css" media="screen" />

  <!-- Modernizr -->
  <script src="<?=HOST_URL?>/slider/js/modernizr.js"></script>
  <div id="container" class="cf">
	<div id="main" role="main">
      <section class="slider">
        <div id="slider" class="flexslider">
          <ul class="slides">
           
           <?php for($m=0;$m<count($data_gallery_images);$m++){
			   
			     $file_name_updated_thumb    = $data_gallery_images[$m]['file_name_updated'];
				if($file_name_updated_thumb<>"")
				{
					$file_url = IMG_GALLERY_PATH."/".$file_name_updated_thumb;
				}
			   ?>
           
                <li>
  	    	    <img src="<?=$file_url?>" />
  	    		</li>
         <?php } ?>
          </ul>
        </div>
        <div id="carousel" class="flexslider">
          <ul class="slides">
            
             <?php for($n=0;$n<count($data_gallery_images);$n++){
			   
			     $file_name_updated_thumb    = $data_gallery_images[$n]['file_name_updated_thumb'];
				if($file_name_updated_thumb<>"")
				{
					$file_url = IMG_GALLERY_PATH."/".$file_name_updated_thumb;
				}
			   ?>
                <li>
  	    	    <img src="<?=$file_url?>" style="border:1px solid #fff;" />
  	    		</li>
  	    		<?php } ?>
          </ul>
        </div>
      </section>
    </div>
 </div>
 
  <!-- jQuery -->
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.min.js">\x3C/script>')</script>

  <!-- FlexSlider -->
  <script defer src="<?=HOST_URL?>/slider/js/jquery.flexslider.js"></script>

  <script type="text/javascript">
    $(function(){
      SyntaxHighlighter.all();
    });
    $(window).load(function(){
      $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 210,
        itemMargin: 5,
        asNavFor: '#slider'
      });

      $('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel",
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
  </script>


  <!-- Syntax Highlighter -->
  <script type="text/javascript" src="<?=HOST_URL?>/slider/js/shCore.js"></script>
  <script type="text/javascript" src="<?=HOST_URL?>/slider/js/shBrushXml.js"></script>
  <script type="text/javascript" src="<?=HOST_URL?>/slider/js/shBrushJScript.js"></script>

  <!-- Optional FlexSlider Additions -->
  <script src="<?=HOST_URL?>/slider/js/jquery.easing.js"></script>
  <script src="<?=HOST_URL?>/slider/js/jquery.mousewheel.js"></script>
  <script defer src="<?=HOST_URL?>/slider/js/demo.js"></script>
  <div style="clear:both;"></div> 
 </div>
