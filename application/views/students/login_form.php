<?php $css = " login-page";
    include(ROOT_ADMIN_PATH."/students/include/header.php");
?>

<script language="javascript">
	function validateForm() {
		if ( validate_username() == false || validate_password() == false) {
			return false;
		} else {
			ajax_validate_and_login_user();
		}
	}//function validateForm

/*	function validate_email() {
		var regExp = / /g;
		var str = $('#email').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Email is blank. Please enter email.");
		return false;
		}
	
		if (!isNaN(str)) {
			my_alert("Invalid Email.");
			return false;
		}
	
		if(str.indexOf('@', 0) == -1) {
			my_alert("Invalid Email.");
			return false;
		}
	}
	*/
	
	function validate_username() {
		var regExp = / /g;
		var str = $('#username').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Username is blank. Please enter username.");
		return false;
		}
	}

	function validate_password() {
		var regExp = / /g;
		var str = $('#password').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Password is blank. Please enter password.");
		return false;
		}
	}
	
	function ajax_validate_and_login_user() {
		$('#my_button').addClass("fa fa-spin fa-refresh");//For spinner
		
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/students/student_user/ajax_validate_and_login_user";
		
		$.ajax({
		type: "POST",
		url: url,
		dataType: "html",
		data: {
			username: $('#username').val(),
			password: $('#password').val(),
			remember_me: $('#remember_me').val(),
			is_ajax: true
			
		},
		success: function(data) {
			var temp = new String();
			temp = data;
			temp = temp.trim();
			
			if (temp=='*N*') {
				my_alert("Invalid Username or Password");
			} else if (temp=='*Y*') {
			window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/students/home/welcome/";
			}
		},
		error: function() {
		}, 
		complete: function() {
		}
		});
	}
</script>
<style>
ul.tab {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #3c8dbc;
}

/* Float the list items side by side */
ul.tab li {float: left; text-align:center; width:100%; border: 1px solid #fff;}

/* Style the links inside the list items */
ul.tab li a {
    display: inline-block;
    color: #fff;
    text-align: center;
    padding: 14px 0;
    text-decoration: none;
    transition: 0.3s;
    font-size: 17px;
	width:100% !important;
	
}

/* Change background color of links on hover */
/*ul.tab li a:hover {width:100% !important; background-color: #ddd; color: #000; }
*/
/* Create an active/current tablink class */
/*ul.tab li a:focus, .active {width:100% !important; background-color:#3c8dbc; color:#fff !important; } /*#c3a336*/
*/
/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}

.btnLogin{
	background-color:#3c8dbc;/*#ba3a39*/
	color:#fff;
}
</style>


<div class="login-box">
  <div class="login-logo">
  
  <img src="<?=HOST_URL?>/admin/images/inner_header_logo.png">
  
	<!--Logo goes here--> 
    <?php	
		$user_mode = "student";
		$user_type = "ST";
		 if(LAN_SEL=="en"){
				$loginHead = '<b>Students</b> Login';
		 }else{
			 $loginHead = '<b>الطلاب الدخول</b>';
		 }
	 
	 echo $loginHead;
	?>
  </div>
  
  <ul class="tab">
      <?php if(LAN_SEL=="en"){?>
      <li><a href="javascript:void(0)" class="tablinks"  >STUDENTS</a></li>
      <?php } else { ?>
      <li><a href="javascript:void(0)" class="tablinks"  >الطلاب</a></li>
      <?php } ?>
      
    </ul>
  
  
  <!-- /.login-logo -->
  <div class="login-box-body">
    
    <form name="login" method="post">
      <div class="form-group has-feedback">
      
        <?php
		 if(LAN_SEL=="en"){ 
		 	$placeHolderUser = 'placeholder="Username"'; 
			$placeHolderPass =  'placeholder="Password"'; 
			$submitLink = 'Sign In';
			$submitStyle= ' style="margin-right:5px" ';
			$submitDivAr = '';
		 }else {    
		 	$placeHolderUser = ' placeholder="معرف المستخدم" dir="rtl" ';
			$placeHolderPass = ' placeholder="كلمه السر" dir="rtl" ';
			$submitLink = 'تسجيل الدخول ';
			$submitStyle = '';
			$submitDivAr = 'style="padding-right: 0 !important;"';
		 } ?>
         
        <input id="username" name="username" type="text" class="form-control"  <?=$placeHolderUser?> >
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input id="password" name="password" type="password" class="form-control" <?=$placeHolderPass?> >
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
     <div class="row">
        <!-- <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input id="remember_me" name="remember_me" type="checkbox"> Remember Me
            </label>
          </div>
        </div>-->
        <!-- /.col -->
        <div class="col-xs-4" <?=$submitDivAr?> >
          <button type="button" class="btn btnLogin btn-block btn-flat" onClick="validateForm()">
          <i id="my_button"  <?=$submitStyle?> ></i>
          	<?=$submitLink?>
            </li>
          </button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- iCheck -->
<script src="<?=HOST_URL?>/assets/admin/plugins/iCheck/icheck.min.js"></script>

<script>
  $(function () {
	$('input').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		radioClass: 'iradio_square-blue',
		increaseArea: '20%' // optional
	});
	//show_modal();
  });
</script>

<script language="javascript">
function show_modal() {
	$('#alert_box').modal('show');
}


</script>

<?php
include(ROOT_ADMIN_PATH."/students/include/footer.php");
?>