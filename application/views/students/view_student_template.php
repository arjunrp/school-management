<?php include(ROOT_ADMIN_PATH."/students/include/header.php");

?>
<!--WRAPPER-->
<div class="wrapper"> 
  
      <!--TOP BAR-->
      <?php include(ROOT_ADMIN_PATH."/students/include/header_top_bar.php");?>
      <!--/TOP BAR--> 
      
      <!--LEFT-->
      <?php include(ROOT_ADMIN_PATH."/students/include/left.php");?>
      <!--/LEFT--> 
      
      
      <!--INC FILE-->
      <?php
		//echo $page;
		//exit();
      	switch($page) {
			case("login_form"): {
				include(ROOT_ADMIN_PATH."/students/login_form.php");
			break;	
			}
			case("view_child_details"): {
				include(ROOT_ADMIN_PATH."/students/view_child_details.php");
			break;	
			}	
			case("view_school_records"): {
				include(ROOT_ADMIN_PATH."/students/view_school_records.php");
			break;	
			}
			case("view_books_list"): {
				include(ROOT_ADMIN_PATH."/students/view_books_list.php");
			break;	
			}
			  
			case("view_my_progress_report"): {
				include(ROOT_ADMIN_PATH."/students/view_my_progress_report.php");
			break;	
			}
	
			
			
		}
	  ?>
      <!--/INC FILE--> 
      
      
      <!--FOOTER COPYRIGHT-->
      <?php include(ROOT_ADMIN_PATH."/students/include/footer_copyright.php");?>
      <!--/FOOTER COPYRIGHT--> 
      
      <!--MENU RIGHT-->
      <?php include(ROOT_ADMIN_PATH."/students/include/menu_slide_right.php");?>
      <!--/MENU RIGHT--> 
  
</div>
<!--/WRAPPER-->
<?php include(ROOT_ADMIN_PATH."/students/include/footer.php");?>