
<?php
/*$colorCode = array("#f56954","#00a65a","#f39c12","#00c0ef","#3c8dbc","#d2d6de","#f56954","#00a65a","#f39c12","#00c0ef","#3c8dbc","#d2d6de");

$pieHistory = "[ ";
for($b=0;$b<count($list_category); $b++)
{
	$list_category[$b]['cntEnquiry'];
	if(LAN_SEL=="ar")
	    $category_name  = $list_category[$b]['parent_category_name_ar']." - ".$list_category[$b]['category_name_ar'];
	else
		$category_name  = $list_category[$b]['parent_category_name_en']." - ".$list_category[$b]['category_name_en'];

    $pieHistory .= "{
        value: '".$list_category[$b]['cntEnquiry']."',
        color: '".$colorCode[$b]."',
        highlight: '".$colorCode[$b]."',
        label: '".$category_name."'      }, ";
}
$pieHistory .= " ]; ";


$pieHistoryCourt = "[ ";
for($d=0;$d<count($list_court); $d++)
{
	$list_court[$d]['cntEnquiry'];
	if(LAN_SEL=="ar")
		$court_name = $list_court[$d]['court_name_ar'];
	else
		$court_name = $list_court[$d]['court_name_en'];
	

    $pieHistoryCourt .= "{
        value: '".$list_court[$d]['cntEnquiry']."',
        color: '".$colorCode[$d]."',
        highlight: '".$colorCode[$d]."',
        label: '".$court_name."'      }, ";
}
$pieHistoryCourt .= " ]; ";



$pieNationality = "[ ";
for($b=0;$b<count($list_enq_nationality); $b++)
{
	$list_enq_nationality[$b]['cntEnquiry'];
	$nationality  = $list_enq_nationality[$b]['country_name'];

    $pieNationality .= "{
        value: '".$list_enq_nationality[$b]['cntEnquiry']."',
        color: '".$colorCode[$b]."',
        highlight: '".$colorCode[$b]."',
        label: '".$nationality."'      }, ";
}
$pieNationality .= " ]; ";


$pieAge = "[ ";
for($g=0;$g<count($list_age_group_enq); $g++)
{
	$list_age_group_enq[$g]['cntEnquiry'];
	$age_group  = $list_age_group_enq[$g]['age_from']." - ".$list_age_group_enq[$g]['age_to'] ;

    $pieAge .= "{
        value: '".$list_age_group_enq[$g]['cntEnquiry']."',
        color: '".$colorCode[$g]."',
        highlight: '".$colorCode[$g]."',
        label: ' Age Group(".$age_group.")'      }, ";
}
$pieAge .= " ]; ";


$pieGender = "[ ";
for($e=0;$e<count($list_enq_gender); $e++)
{
	$list_enq_gender[$e]['cntEnquiry'];
	$gender  = $list_enq_gender[$e]['gender'];

    $pieGender .= "{
        value: '".$list_enq_gender[$e]['cntEnquiry']."',
        color: '".$colorCode[$e]."',
        highlight: '".$colorCode[$e]."',
        label: '".$gender."'      }, ";
}
$pieGender .= " ]; ";



$pieReligion = "[ ";
for($f=0;$f<count($list_enq_religion); $f++)
{
	$list_enq_religion[$f]['cntEnquiry'];
	$religion  = $list_enq_religion[$f]['religion'];

    $pieReligion .= "{
        value: '".$list_enq_religion[$f]['cntEnquiry']."',
        color: '".$colorCode[$f]."',
        highlight: '".$colorCode[$f]."',
        label: '".$religion."'      }, ";
}
$pieReligion .= " ]; ";




*/
?>
<?php if(LAN_SEL=="ar"){ 
		$enquiryLabel  = "الإستفسارات";
		$enquiryOpen   = "فتح";
		$enquiryClosed = "مغلق";
		$enquiryPerformance = "أداء";
		$positionBreadCrumb = 'float:right;';
}else{
		$enquiryLabel  = "Enquiries";
		$enquiryOpen   = "Open";
		$enquiryClosed = "Closed";
		$enquiryPerformance = "Performance";
		$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <?php if(LAN_SEL=="ar"){?> 
       <h1>مرحبا بكم في الأقدار تطبيق المدرسة  </h1>
    <?php }else{?>
    <h1> Welcome to <?=APP_HEAD_SMALL?><small> School Application</small></h1>
    <?php } ?>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="#" target=""><i class="fa fa-home"></i><?php if(LAN_SEL=="ar"){?>الرئيسية<?php }else{?>Home<?php } ?></a></li>
      <li><?php if(LAN_SEL=="ar"){?>مرحبا<?php }else{?>Welcome<?php } ?></li>
    </ol>
    <!--/BREADCRUMB--> 
    <div style="clear:both"></div>
  </section>
  
  <?php if($_SESSION['aqdar_smartcare']['user_type_sess'] == "admin") {?>
  
  
  <section class="content" style="min-height:181px;"> 
    <!--WORKING AREA-->	
                <div class="row">
                  <div class="col-lg-3 col-xs-6"> 
                    <div class="small-box bg-yellow">
                      <div class="inner">
                        <h3><?=$total_student_school?></h3>
                        <?php if(LAN_SEL=="ar"){?> 
                        <p> مجموع الطلاب</p>
                        <?php }else{ ?>
                        <p>Total Students</p>
                        <?php } ?>
                      </div>
                      <div class="icon"> <i class="ion ion-person-add"></i> </div>
                      <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/all_students" class="small-box-footer"> <?php if(LAN_SEL=="ar"){?> للمزيد من المعلومات  <?php }else{ ?> More info <?php } ?><i class="fa fa-arrow-circle-right"></i></a> </div>
                  </div>
                  <div class="col-lg-3 col-xs-6"> 
                    <div class="small-box bg-red">
                      <div class="inner">
                        <h3><?=$total_teacher_school?></h3>
                         <?php if(LAN_SEL=="ar"){?> 
                        <p> مجموع المعلمين</p>
                        <?php }else{ ?>
                        <p>Total Teachers</p>
                        <?php } ?>
                      </div>
                      <div class="icon"> <i class="ion ion-person-add"></i> </div>
                      <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/all_teachers/" class="small-box-footer"><?php if(LAN_SEL=="ar"){?> للمزيد من المعلومات  <?php }else{ ?> More info <?php } ?><i class="fa fa-arrow-circle-right"></i></a> </div>
                  </div>
                  
                 <?php /*?> <div class="col-lg-3 col-xs-6"> 
                    <div class="small-box bg-green"><!--bg-aqua-->
                      <div class="inner">
                        <h3><?=$total_enquiries?></h3>
                         <?php if(LAN_SEL=="ar"){?> 
                        <p> مجموع الإستفسارات</p>
                        <?php }else{ ?>
                        <p>Total Enquiries</p>
                        <?php } ?>
                      </div>
                      <div class="icon"> <i class="fa fa-envelope"></i> </div>
                      <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/enquiry/all_enquiries" class="small-box-footer"><?php if(LAN_SEL=="ar"){?> للمزيد من المعلومات  <?php }else{ ?> More info <?php } ?><i class="fa fa-arrow-circle-right"></i></a> </div>
                  </div><?php */?>
                  <!--<div class="col-lg-3 col-xs-6"> 
                    <div class="small-box bg-green">
                      <div class="inner">
                        <h3>Android <sup style="font-size: 20px">65%</sup> IOS <sup style="font-size: 20px">35%</sup></h3>
                        <p>Closed </p>
                      </div>
                      <div class="icon"> <i class="ion ion-stats-bars"></i> </div>
                      <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> </div>
                  </div>-->
                 <!-- <div class="col-lg-3 col-xs-6"> 
                    <div class="small-box bg-aqua">
                      <div class="inner">
                        <h3><?=$total_judges?></h3>
                         <?php if(LAN_SEL=="ar"){?> 
                        <p> استشاريون الأسرة</p>
                        <?php }else{ ?>
                        <p>Family Consultants</p>
                        <?php } ?>
                        
                      </div>
                      <div class="icon"> <i class="fa fa-gavel"></i> </div>
                      <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/judge/all_judges" class="small-box-footer"><?php if(LAN_SEL=="ar"){?> للمزيد من المعلومات  <?php }else{ ?> More info <?php } ?><i class="fa fa-arrow-circle-right"></i></a> </div>
                  </div>-->
                </div>
        
    <!--/WORKING AREA--> 
  </section>
  
  <?php }else if($_SESSION['aqdar_smartcare']['user_type_sess'] == "judge") {?>
  
  
  <section class="content" style="min-height:181px;"> 
    <!--WORKING AREA-->	
                <div class="row">
                  
                  <div class="col-lg-3 col-xs-6"> 
                    <div class="small-box bg-red">
                      <div class="inner">
                        <h3><?=$total_open_enquiries?></h3>
                         <?php if(LAN_SEL=="ar"){?> 
                        <p> فتح الإستفسارات</p>
                        <?php }else{ ?>
                        <p>Open Enquiries</p>
                        <?php } ?>
                      </div>
                      <div class="icon"> <i class="fa fa-envelope-o"></i> </div>
                      <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/enquiry/all_enquiries/is_not_replied/Y/" class="small-box-footer"><?php if(LAN_SEL=="ar"){?> للمزيد من المعلومات  <?php }else{ ?> More info <?php } ?><i class="fa fa-arrow-circle-right"></i></a> </div>
                  </div>
                  
                  <div class="col-lg-3 col-xs-6"> 
                    <div class="small-box bg-green">
                      <div class="inner">
                       <h3><?=$total_enquiries?></h3>
                         <?php if(LAN_SEL=="ar"){?> 
                        <p> مجموع الإستفسارات</p>
                        <?php }else{ ?>
                        <p>Total Enquiries</p>
                        <?php } ?>
                      </div>
                      <div class="icon"> <i class="fa fa-envelope"></i> </div>
                      <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/enquiry/all_enquiries" class="small-box-footer"><?php if(LAN_SEL=="ar"){?> للمزيد من المعلومات  <?php }else{ ?> More info <?php } ?><i class="fa fa-arrow-circle-right"></i></a> </div>
                  </div>
                  <!--<div class="col-lg-3 col-xs-6"> 
                    <div class="small-box bg-green">
                      <div class="inner">
                        <h3>Android <sup style="font-size: 20px">65%</sup> IOS <sup style="font-size: 20px">35%</sup></h3>
                        <p>Closed </p>
                      </div>
                      <div class="icon"> <i class="ion ion-stats-bars"></i> </div>
                      <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> </div>
                  </div>-->
                
                </div>
        
    <!--/WORKING AREA--> 
  </section>
  
  <?php } ?>
   
   
     <!-- start --->
  

  
  
  <section class="content" style="display:none;"> 
    <!--WORKING AREA-->	

    <div class="row">
        <div class="col-md-6">
          <!-- AREA CHART -->
          <div class="box box-primary">
            <div class="box-header with-border">
               <?php if(LAN_SEL=="ar"){?> 
                    <h3 class="box-title">  الإستفسارات والردود </h3>
               <?php }else{ ?>
                     <h3 class="box-title">Area Chart - Enquiries vs Replied</h3>
              <?php } ?>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
                <canvas id="areaChart" style="height:250px"></canvas>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- DONUT CHART -->
          <div class="box box-danger">
            <div class="box-header with-border">
               <?php if(LAN_SEL=="ar"){?> 
                    <h3 class="box-title">  الإستفسارات بناء على تصنيفات مختلفة </h3>
               <?php }else{ ?>
                     <h3 class="box-title">Enquiries Based On Different Categories </h3>
              <?php } ?>
              
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>
              </div>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <canvas id="pieChart" style="height:250px"></canvas>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          
          
         

          <!-- DONUT CHART -->
          <div class="box box-info">
            <div class="box-header with-border">
               <?php if(LAN_SEL=="ar"){?> 
                    <h3 class="box-title">  الإستفسارات بناء على تصنيفات مختلفة </h3>
               <?php }else{ ?>
                     <h3 class="box-title">Enquiries Based On Gender </h3>
              <?php } ?>
              
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>
              </div>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
               <div class="chart">
              <canvas id="genderChart" style="height:250px"></canvas>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
           <!-- /.box -->
           
           
           <div class="box box-info">
            <div class="box-header with-border">
               <?php if(LAN_SEL=="ar"){?> 
                    <h3 class="box-title">  الاستفسارات بناء على الدين </h3>
               <?php }else{ ?>
                     <h3 class="box-title">Enquiries Based On Religion </h3>
              <?php } ?>
              
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>
              </div>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
               <div class="chart">
              <canvas id="religionChart" style="height:250px"></canvas>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
           
           
           
           
        </div>
        <!-- /.col (LEFT) -->
        <div class="col-md-6">
          <!-- LINE CHART -->
         <?php
		if(isset($_SESSION['aqdar_smartcare']['tbl_judge_id_sess']))
		{
			if(LAN_SEL=="ar")
				$titleBarChart = "الإستفسارات ضد أداء استشاريون الأسرة" ;
			else
			   $titleBarChart = "Enquiries vs Family Consultants Performance";
		}else{
			if(LAN_SEL=="ar")
				$titleBarChart = "الإستفسارات ضد استشاريون الأسرة";
			else
				$titleBarChart = "Enquiries vs Family Consultants";
		}
	
		 ?>
           <!-- BAR CHART -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title"><?=$titleBarChart?></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>
              </div>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
                <canvas id="barChart" style="height:250px"></canvas>
            </div>
            <!-- /.box-body -->
          </div>
          
          
          
           <!-- DONUT CHART -->
          <div class="box box-info">
            <div class="box-header with-border">
               <?php if(LAN_SEL=="ar"){?> 
                    <h3 class="box-title">  الإستفسارات بناء على تصنيفات مختلفة </h3>
               <?php }else{ ?>
                     <h3 class="box-title">Enquiries Based On Nationality </h3>
              <?php } ?>
              
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>
              </div>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <canvas id="nationalityChart" style="height:250px"></canvas>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          
           <!-- DONUT CHART -->
          <div class="box box-danger">
            <div class="box-header with-border">
               <?php if(LAN_SEL=="ar"){?> 
                    <h3 class="box-title">  الإستفسارات بناء على تصنيفات مختلفة </h3>
               <?php }else{ ?>
                     <h3 class="box-title">Enquiries Based On Age Group </h3>
              <?php } ?>
              
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>
              </div>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
               <div class="chart">
                  <canvas id="ageChart" style="height:250px"></canvas>
               </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          

        </div>
        <!-- /.col (RIGHT) -->
      </div>
    <!--/WORKING AREA--> 
    
  </section>

<script>
   

  $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */
	 
	 
	 // bar chart
	  // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var areaChart = new Chart(areaChartCanvas);

    var barChartData = {
      labels: ["<?=$judge_perfo['6']['judge_name']?>", "<?=$judge_perfo['5']['judge_name']?>", "<?=$judge_perfo['4']['judge_name']?>", "<?=$judge_perfo['3']['judge_name']?>", "<?=$judge_perfo['2']['judge_name']?>", "<?=$judge_perfo['1']['judge_name']?>", "<?=$judge_perfo['0']['judge_name']?>"],
      datasets: [
        {
          label: "<?=$enquiryLabel?>",
          fillColor: "rgba(0, 192, 239, 1)",
          strokeColor: "rgba(0, 192, 239, 1)",
          pointColor: "rgba(0, 192, 239, 1)",
          pointStrokeColor: "#00c0ef",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(0, 192, 239, 1)",
          data: [<?=$judge_perfo['6']['total_enquiries_judge']?>, <?=$judge_perfo['5']['total_enquiries_judge']?>, <?=$judge_perfo['4']['total_enquiries_judge']?>, <?=$judge_perfo['3']['total_enquiries_judge']?>, <?=$judge_perfo['2']['total_enquiries_judge']?>, <?=$judge_perfo['1']['total_enquiries_judge']?>, <?=$judge_perfo['0']['total_enquiries_judge']?>]
        }
		
	   <?php	if(!isset($_SESSION['aqdar_smartcare']['tbl_judge_id_sess']))
		{ ?>
		
		,
        {
          label: "<?=$enquiryOpen?>",
          fillColor: "rgba(245,105,84,0.9)",
          strokeColor: "rgba(245,105,84,0.9)",
          pointColor: "#f56954",
          pointStrokeColor: "rgba(245,105,84,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(245,105,84,1)",
          data: [<?=$judge_perfo['6']['total_open_enquiries_judge']?>, <?=$judge_perfo['5']['total_open_enquiries_judge']?>, <?=$judge_perfo['4']['total_open_enquiries_judge']?>, <?=$judge_perfo['3']['total_open_enquiries_judge']?>, <?=$judge_perfo['2']['total_open_enquiries_judge']?>, <?=$judge_perfo['1']['total_open_enquiries_judge']?>, <?=$judge_perfo['0']['total_open_enquiries_judge']?>]
        },
		{
          label: "<?=$enquiryClosed?>",
          fillColor: "rgba(0,166,90,0.9)",
          strokeColor: "rgba(0,166,90,0.8)",
          pointColor: "#00a65a",
          pointStrokeColor: "rgba(0,166,90,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(0,166,90,1)",
          data: [<?=$judge_perfo['6']['total_closed_enquiries_judge']?>, <?=$judge_perfo['5']['total_closed_enquiries_judge']?>, <?=$judge_perfo['4']['total_closed_enquiries_judge']?>, <?=$judge_perfo['3']['total_closed_enquiries_judge']?>, <?=$judge_perfo['2']['total_closed_enquiries_judge']?>, <?=$judge_perfo['1']['total_closed_enquiries_judge']?>, <?=$judge_perfo['0']['total_closed_enquiries_judge']?>]
        }
		
		<?php }else{  ?>,
		
		{
          label: " <?=$enquiryPerformance?> (%)",
          fillColor: "rgba(0,166,90,0.9)",
          strokeColor: "rgba(0,166,90,0.8)",
          pointColor: "#00a65a",
          pointStrokeColor: "rgba(0,166,90,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(0,166,90,1)",
          data: [<?=$judge_perfo['6']['performance']?>, <?=$judge_perfo['5']['performance']?>, <?=$judge_perfo['4']['performance']?>, <?=$judge_perfo['3']['performance']?>, <?=$judge_perfo['2']['performance']?>, <?=$judge_perfo['1']['performance']?>, <?=$judge_perfo['0']['performance']?>]
        }
		
		<?php } ?>
		
      ]
    };

    var barChartOptions = {
      //Boolean - If we should show the scale at all
      showScale: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: false,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - Whether the line is curved between points
      bezierCurve: true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension: 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot: false,
      //Number - Radius of each point dot in pixels
      pointDotRadius: 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth: 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius: 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke: true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth: 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true
    };

    //Create the line chart
   // areaChart.Line(areaChartData, areaChartOptions);

	 //end bar chart
	 
	 
	 
	 
	 

    //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var areaChart = new Chart(areaChartCanvas);

    var areaChartData = {
      labels: ["<?=$month['6']?>", "<?=$month['5']?>", "<?=$month['4']?>", "<?=$month['3']?>", "<?=$month['2']?>", "<?=$month['1']?>", "<?=$month['0']?>"],
      datasets: [
        {
          label: "<?=$enquiryLabel?>",
          fillColor: "rgba(0, 192, 239, 1)",
          strokeColor: "rgba(0, 192, 239, 1)",
          pointColor: "rgba(0, 192, 239, 1)",
          pointStrokeColor: "#00c0ef",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(0, 192, 239, 1)",
          data: [<?=$total_enquiries_chart['6']?>, <?=$total_enquiries_chart['5']?>, <?=$total_enquiries_chart['4']?>, <?=$total_enquiries_chart['3']?>, <?=$total_enquiries_chart['2']?>, <?=$total_enquiries_chart['1']?>, <?=$total_enquiries_chart['0']?>]
        },
        {
          label: "<?=$enquiryOpen?>",
          fillColor: "rgba(245,105,84,0.9)",
          strokeColor: "rgba(245,105,84,0.9)",
          pointColor: "#f56954",
          pointStrokeColor: "rgba(245,105,84,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(245,105,84,1)",
          data: [<?=$total_open_enquiries_chart['6']?>, <?=$total_open_enquiries_chart['5']?>, <?=$total_open_enquiries_chart['4']?>, <?=$total_open_enquiries_chart['3']?>, <?=$total_open_enquiries_chart['2']?>, <?=$total_open_enquiries_chart['1']?>, <?=$total_open_enquiries_chart['0']?>]
        },
		{
          label: "<?=$enquiryClosed?>",
          fillColor: "rgba(0,166,90,0.9)",
          strokeColor: "rgba(0,166,90,0.8)",
          pointColor: "#00a65a",
          pointStrokeColor: "rgba(0,166,90,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(0,166,90,1)",
          data: [<?=$total_closed_enquiries_chart['6']?>, <?=$total_closed_enquiries_chart['5']?>, <?=$total_closed_enquiries_chart['4']?>, <?=$total_closed_enquiries_chart['3']?>, <?=$total_closed_enquiries_chart['2']?>, <?=$total_closed_enquiries_chart['1']?>, <?=$total_closed_enquiries_chart['0']?>]
        }
      ]
    };

    var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: false,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - Whether the line is curved between points
      bezierCurve: true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension: 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot: false,
      //Number - Radius of each point dot in pixels
      pointDotRadius: 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth: 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius: 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke: true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth: 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true
    };

    //Create the line chart
    areaChart.Line(areaChartData, areaChartOptions);

    //-------------
    //- LINE CHART -
    //--------------
    //var lineChartCanvas = $("#lineChart").get(0).getContext("2d");
//    var lineChart = new Chart(lineChartCanvas);
//
//    var lineChartOptions = areaChartOptions;
//    lineChartOptions.datasetFill = false;
//    lineChart.Line(areaChartData, lineChartOptions);
	
	
	
	  //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $("#nationalityChart").get(0).getContext("2d");
    var pieChart = new Chart(pieChartCanvas);
    var PieData = <?=$pieNationality?>
    var pieOptions = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke: true,
      //String - The colour of each segment stroke
      segmentStrokeColor: "#fff",
      //Number - The width of each segment stroke
      segmentStrokeWidth: 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps: 100,
      //String - Animation easing effect
      animationEasing: "easeOutBounce",
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate: true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale: false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    };
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions);
	
	
	
		  //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $("#ageChart").get(0).getContext("2d");
    var pieChart = new Chart(pieChartCanvas);
    var PieData = <?=$pieAge?>
    var pieOptions = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke: true,
      //String - The colour of each segment stroke
      segmentStrokeColor: "#fff",
      //Number - The width of each segment stroke
      segmentStrokeWidth: 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps: 100,
      //String - Animation easing effect
      animationEasing: "easeOutBounce",
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate: true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale: false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    };
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions);

	

   	
		  //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $("#genderChart").get(0).getContext("2d");
    var pieChart = new Chart(pieChartCanvas);
    var PieData = <?=$pieGender?>
    var pieOptions = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke: true,
      //String - The colour of each segment stroke
      segmentStrokeColor: "#fff",
      //Number - The width of each segment stroke
      segmentStrokeWidth: 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps: 100,
      //String - Animation easing effect
      animationEasing: "easeOutBounce",
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate: true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale: false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    };
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions);
	
	
	//religion chart
		  //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $("#religionChart").get(0).getContext("2d");
    var pieChart = new Chart(pieChartCanvas);
    var PieData = <?=$pieReligion?>
    var pieOptions = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke: true,
      //String - The colour of each segment stroke
      segmentStrokeColor: "#fff",
      //Number - The width of each segment stroke
      segmentStrokeWidth: 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps: 100,
      //String - Animation easing effect
      animationEasing: "easeOutBounce",
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate: true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale: false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    };
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions);


    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
    var pieChart = new Chart(pieChartCanvas);
    var PieData = <?=$pieHistory?>
    var pieOptions = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke: true,
      //String - The colour of each segment stroke
      segmentStrokeColor: "#fff",
      //Number - The width of each segment stroke
      segmentStrokeWidth: 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps: 100,
      //String - Animation easing effect
      animationEasing: "easeOutBounce",
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate: true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale: false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    };
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions);


   

    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $("#barChart").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas);
    var barChartData = barChartData;
    barChartData.datasets[1].fillColor = "#f56954";
    barChartData.datasets[1].strokeColor = "#f56954";
    barChartData.datasets[1].pointColor = "#f56954";
    var barChartOptions = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: true,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - If there is a stroke on each bar
      barShowStroke: true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth: 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing: 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing: 1,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to make the chart responsive
      responsive: true,
      maintainAspectRatio: true
    };

    barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions);
  });
</script>

  
  <!-- end start --->
   
   
   
   

  
  <div style="clear:both"></div>
</div>

