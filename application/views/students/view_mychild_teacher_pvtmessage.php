<?php
//Init Parameters
$message_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}
?>
 
<style>
.txt_en {
	text-align:left;
	padding-left:2px;
}
.txt_ar {
	text-align:right;
	padding-right:2px;	
	direction:rtl;		
}
</style>
<script>
var item_id = "<?=$teacher_id_enc?>";
</script>

<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1_list').hide(500);
		$('#mid2').show(500);
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/mychild_teacher_messages/tbl_teacher_id/<?=$tbl_sel_teacher_id?>/child_id_enc/<?=$tbl_sel_student_id?>/tbl_school_id/<?=$tbl_sel_school_id?>/offset/0";
			}
		})
	});
	
	
	function ajax_create() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/sendPrivateMessageToTeacher",
			data: {
				message_from   : $('#message_from').val(),
				message_to     : $('#message_to').val(),
				tbl_student_id : $('#student_id_enc').val(),
				message_type   : 'text',
				tbl_message    : $('#message').val(),
				tbl_school_id : $('#school_id_enc').val(),
			    tbl_item_id   : '',
				message_dir    : 'PT',
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
				refresh_page = "Y";
				    my_alert("Message sent successfully.", 'green');
				    $("#pre-loader").hide();
				}else{
					refresh_page = "N";
					my_alert("Message sent failed, Please try again.", 'red');
					$("#pre-loader").hide();
				}
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}


</script>
<script language="javascript">
   //add student
   /* || validate_picture() == false*/
	function ajax_validate() {
		if (validate_message() == false ) 
		{
			return false;
		} 
		ajax_create();
	}
	
  /************************************* START MESSAGE VALIDATION *******************************/
   function validate_message() {
		var regExp = / /g;
		var str = $("#message").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Message is blank. Please write message.")
			$("#message").val('');
			$("#message").focus();
		return false;
		}
	}

</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Private Messages<small></small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Messages</li>
    </ol>
     <div class="box-tools" style="float:right;">
     <a href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/mychild_teachers/child_id_enc/<?=$tbl_sel_student_id?>/tbl_school_id/<?=$tbl_sel_school_id?>/offset/0"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
     </div>
    <!--/BREADCRUMB-->
    <div style="clear:both"></div>
  </section>
      <link href="<?=HOST_URL?>/assets/admin/dist/css/jquery-ui.css" rel="stylesheet">
      <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-1.11.1.js"></script>
      <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-ui.js"></script>
      <link href="<?=HOST_URL?>/assets/admin/dist/css/uploadfile.min.css" rel="stylesheet">
 
  <script>
   function show_roles()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/school_role/";
		window.location.href = url;
	}     
  </script>
  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {
	?>
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/teacher/all_teachers";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
    
  
 <link href="<?=HOST_URL?>/assets/admin/dist/css/jquery-ui.css" rel="stylesheet">
 <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-1.11.1.js"></script>
 <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-ui.js"></script>
  <script>
  $( function() {
		   $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
		
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	
				
				
				
				
				
			}
	  } );
  
  } );
  </script> 
  
  
  
  <!--File Upload START-->
<link href="<?=HOST_URL?>/assets/admin/dist/css/uploadfile.min.css" rel="stylesheet">
<script>
 $( function() {
    $( "#tabs" ).tabs();
  } );
  
 
</script>
<style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url('<?=IMG_PATH?>/delete.jpg');
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
		clear:both;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
	#overlay_container {
		position:relative;
	}
	#overloading {
		background-image:url('<?=IMG_PATH?>/preloader/preloader_2.gif');
		background-repeat:no-repeat;
		background-position:center;
		background-color:#CCC;
		position:absolute;
		left:0px;
		top:0px;
		opacity: 0.3;
		z-index: 10000;
	}
	#div_listing_container {
		display:none;	
	}
	.d_d_text {
		color:#745156;
		font-size:20px;
			
	}
	.ajax-upload-dragdrop {
		margin:auto;
		margin-bottom:10px;
		width:700px !important;
	}
	.ajax-file-upload-statusbar {
		margin:auto;
		margin-top:10px;
	}
	.ajax-file-upload {
		height:31px;
	}
	
	
	 #tabs-1{  
	    overflow-y:scroll; overflow-x:none;
	}

    #tabs-2{
		overflow-y:scroll; overflow-x:none;
	}
				  
  .ui-tabs-active{
		border-color:#efca86  !important;
   }
					 
	.ui-tabs .ui-tabs-nav li {
		float:left;
		font-size: 16px;
        font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif;
  }
  label{
	  display: inline-block;
      font-weight: 700;
  }
  
  .ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button {
    font-family:"Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
    font-size: 14px;
}
  
  .ui-widget{
	 font-size: 16px;
     font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
  }
  .form-control{
	 font-size: 14px; 
  }
</style>
 

  
          
    
            <!--Listing-->
                    <div id="mid1_list" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Message to Teacher - &nbsp;<strong><?=$child_info[0]['first_name']." ".$child_info[0]['last_name']?>&nbsp;&nbsp;/&nbsp;&nbsp;<?=$child_info[0]['first_name_ar']." ".$child_info[0]['last_name_ar']?></strong> 
                          - &nbsp;<strong><?=$teacher_info[0]['first_name']." ".$teacher_info[0]['last_name']?>&nbsp;&nbsp;/&nbsp;&nbsp;<?=$teacher_info[0]['first_name_ar']." ".$teacher_info[0]['last_name_ar']?></strong>
                          </h3>
                          <div class="box-tools">
                            <?php if (count($rs_all_messages)>0) { echo $paging_string;}?>	
                            <button class="btn bg-orange fa fa-plus" type="button" title="Send Message" onclick="show_create_form()"></button>
                          </div>
                        </div>
                        
                        <div class="box-body">
                     <!--   <div style="color:#030; font-weight:bold;">You can sort students by using drag and drop of rows </div>-->
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="100%" align="center" valign="middle">Messages</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
							   
                                for ($i=0; $i<count($rs_all_messages); $i++) { 
                                    $tbl_teacher_id     = $rs_all_messages[$i]['teacher_id'];
                                    $name               = ucfirst($rs_all_messages[$i]['name']);
									$message            = $rs_all_messages[$i]['message'];
									$message_audio      = isset($rs_all_messages[$i]['message_audio'])? $rs_all_messages[$i]['message_audio']:'';
									$message_img        = isset($rs_all_messages[$i]['message_img'])? $rs_all_messages[$i]['message_img']:'';
									$date               = $rs_all_messages[$i]['date'];
									$message_dir        = $rs_all_messages[$i]['message_dir'];
									
									if($message_audio<>"")
									{
										$message_file = '<video width="320" height="60" controls><source src="'.$message_audio.'" type="video/mp4"></video>';
									}
									
									if($message_img<>"")
									{
										$message_file = '<img  style="max-width:200px; max-height:200px;" src="'.$message_img.'" />';	
									}
								
									/*if($pic<>"") 
										$pic_path           =   '<img width="100" height="80" src="'.IMG_PATH_TEACHER.'/'.$pic.'"  />';
									else
										$pic_path           =   '<img width="100" height="80" src="'.IMG_PATH_STUDENT.'/no_img.png"  
										style="border-color:1px solid #7C858C !important;" />';*/
                                    
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_teacher_id?>" >
                              <td align="left" valign="middle">
                              <?php if($message_dir=="TP"){?>
                              <?php if($message_file<>""){ ?>
                              		<div class="txt_ar"><?=$message_file?></div>
                              <?php } ?>
                              <div class="txt_ar"><?=$message?></div>
                              <div class="txt_ar"><?=$name?></div>
                              <div class="txt_ar"><?=$date?></div><br />
                               <?php }else{?>
                               <?php if($message_file<>""){ ?>
                              		<div class="txt_en"><?=$message_file?></div>
                              <?php } ?>
                                <div class="txt_en"><?=$message?></div>
                                <div class="txt_en"><?=$name?></div>
                                <div class="txt_en"><?=$date?></div><br />
                               <?php } ?>
                              </td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="10" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_all_messages)<=0) {
                            ?>
                            <tr>
                              <td colspan="10" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no messages available. Click on the + button to send message.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
    
            <!--Add or Create-->
            <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Send Message For &nbsp;<strong><?=$child_info[0]['first_name']." ".$child_info[0]['last_name']?>&nbsp;&nbsp;/&nbsp;&nbsp;<?=$child_info[0]['first_name_ar']." ".$child_info[0]['last_name_ar']?></strong> 
                  To &nbsp;<strong><?=$teacher_info[0]['first_name']." ".$teacher_info[0]['last_name']?>&nbsp;&nbsp;/&nbsp;&nbsp;<?=$teacher_info[0]['first_name_ar']." ".$teacher_info[0]['last_name_ar']?></strong>
                  </h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
               <?php $start_date                = date("m/d/Y"); ?>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                     <div class="form-group">
                         <label class="col-sm-2 control-label" for="description">Message</label>
                         <div class="col-sm-10">
                          <textarea tabindex="1" dir="ltr" id="message" placeholder="Enter your message" name="message" class="form-control" ></textarea>
                         </div>
                     </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate()">Submit</button>
                    <input type="hidden" name="student_id_enc" id="student_id_enc" value="<?=$tbl_sel_student_id?>" />
                    <input type="hidden" name="message_from" id="message_from" value="<?=$tbl_sel_student_id?>" />
                    <input type="hidden" name="message_to" id="message_to" value="<?=$tbl_sel_teacher_id?>" />
                    <input type="hidden" name="school_id_enc" id="school_id_enc" value="<?=$tbl_sel_school_id?>" />
                    
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
            <!--/Add or Create-->
                
  
            <!--/Add or Create-->
                
        <!--/Admin Category Management-->

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>


<script src="<?=HOST_URL?>/assets/admin/dist/js/jquery.uploadfile.min.js"></script>
<script language="javascript">

//Primary Key for a Form. 

function set_item_id(obj) {
	item_id = obj.value;
	get_files();	
}

$(document).ready(function() {
	var uploadObj = $("#advancedUpload").uploadFile({
		url:"<?=HOST_URL?>/file_mgmt/upload_the_file",
		multiple:true,
		autoSubmit:true,
		maxFileSize:130000,
		fileName:"myfile",
		formData: {"module_name":"teacher"},
		dynamicFormData: function() {
			var data = { item_id:item_id}
			return data;
		},
		showStatusAfterSuccess:false,
		dragDropStr: "<span class='d_d_text'>Optionally Drag and Drop the File to Upload.</span>",
		abortStr:"Abourt",
		cancelStr:"Cancel",
		doneStr:"Done",
		multiDragErrorStr: "Multi Drag Error.",
		extErrorStr:"Extention Error:",
		sizeErrorStr:"Max Size Error:",
		uploadErrorStr:"Upload Error",
		onSelect:function(files) {
 		},
		onSubmit:function(files) {
 		},
		onSuccess:function(files, data, xhr) {
			if (data == "error") {
				alert("Error uploading file. Please try again.");
				return;
			}
			var obj = JSON.parse(data);
			var tbl_uploads_id = obj.tbl_uploads_id;
			var file_name_updated = obj.file_name_updated;
			
			//alert("tbl_uploads_id: "+tbl_uploads_id)
			//alert("file_name_updated: "+file_name_updated)
			add_uploaded_item(tbl_uploads_id, file_name_updated);
		},
		afterUploadAll:function() {
 		},
		onError: function(files, status, errMsg) {
 		}
	});

	$("#startUpload").click(function() {
		uploadObj.startUpload();
	});
	
	try { 
		$('input[type=file]').click();
	} catch(e) {
		alert(e)
	}
});

//Function called when file is uploaded
function add_uploaded_item(tbl_uploads_id, file_name_updated) {
	var str = "<div id='"+tbl_uploads_id+"' class='box-header with-border'> <div class='box-title'><img src='<?=IMG_PATH_TEACHER?>/"+file_name_updated+"' /></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_img_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
		
	$("#div_listing_container").show();
	$("#div_listing_container").append(str);
	$(".ajax-upload-dragdrop").hide();//Hide the upload button
return;
}

function confirm_delete_img_popup(tbl_uploads_id) {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		$('#'+tbl_uploads_id).hide();	
		$(".ajax-upload-dragdrop").show();
		
		var url_str = "<?=HOST_URL?>/file_mgmt/delete_file";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					tbl_uploads_id: tbl_uploads_id
				},
			success: function(data) {
				$("#pre-loader").hide();
			}
		});	
	} else {
		$("#pre-loader").hide();		
	}
}

function get_files() {
	var url_str = "<?=HOST_URL?>/misc/get_files.php";
	
	$.ajax({
		type: "POST",
		url: url_str,
		data: {
				module_name: "teacher",
				show_del: "Y",
				item_id: item_id//global variable
			},
		success: function(data) {
			$('#div_listing_container').show();
			$('#div_listing_container').html(data)
			
		}
	});	
}
</script>
<!--File Upload END-->
        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >
function search_data() {
		var tbl_class_search_id = $("#tbl_class_search_id").val();
		var q = $.trim($("#q").val()); 
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/all_teachers/";
		if(tbl_class_search_id !='')
			url += "tbl_class_search_id/"+tbl_class_search_id+"/";
		if(q !='')
			url += "q/"+q+"/";
		
			url += "offset/0/";
		window.location.href = url;
		<?php /*?>window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/enquiry/all_enquiries/is_not_replied/"+is_not_replied+"/tbl_court_id/"+tbl_court_id+"/tbl_category_id/"+tbl_category_id;<?php */?>
	}
function generate_teacher_id(offset) {
		var tbl_class_search_id = $("#tbl_class_search_id").val();
		var q = $("#q").val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/generate_teacher_id/";
		if(tbl_class_search_id !='')
			url += "tbl_class_id/"+tbl_class_search_id+"/";
		if(q !='')
			url += "q/"+q+"/";
		
			url += "offset/"+offset+"/";
		window.open(url);
	}


function reset_data() {
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/all_teachers/";
		url += "offset/0/";
		window.location.href = url;
	}
</script>