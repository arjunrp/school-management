    <link href="<?=HOST_URL?>/css/gallery/jquery.bsPhotoGallery.css" rel="stylesheet">
    <script src="<?=HOST_URL?>/js/gallery/jquery.bsPhotoGallery.js"></script>
    <script>
      $(document).ready(function(){
        $('ul.first').bsPhotoGallery({
          "classes" : "col-lg-2 col-md-4 col-sm-3 col-xs-4 col-xxs-12",
          "hasModal" : true,
          // "fullHeight" : false
        });
      });
    </script>
    <style>

.btn-01 {
   /* background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #489eed 0%, #347abb 100%) repeat scroll 0 0;*/
    border: 1px solid #2b649b;
    border-radius: 5px;
    box-shadow: 0 1px 0 rgba(255, 255, 255, 0.4) inset;
    color: #3c8dbc;
    /*font-weight: 600;*/
	font-size:13px;
    height: 30px;
    /*padding: 0 20px;*/
    /*text-shadow: 0 1px 2px rgba(0, 0, 0, 0.5);*/
}
    </style>
    <ul class="row first" style=" padding:0 0 0 0; margin:0 0 40px 0;" >
<?php

	$CI =& get_instance();
	$CI->load->model("model_student");
	for ($i=0; $i<count($data_rs); $i++) {
		$tbl_student_id = $data_rs[$i]['tbl_student_id'];
		if ($module_id == "m_give_points") {
			$total_points = $CI->model_student->get_total_points($tbl_student_id);
		}
		//if (trim($lan) == "ar") {
			$name_ar = $data_rs[$i]['first_name_ar']." ".$data_rs[$i]['last_name_ar'];
		//} else {
			$name = $data_rs[$i]['first_name']." ".$data_rs[$i]['last_name'];
		//}
		$file_name_updated = $data_rs[$i]['file_name_updated'];
		
		$arr_students[$i]["student_id"] = $tbl_student_id;
		$arr_students[$i]["title"] = $name;
	
		if($file_name_updated==""){
			$arr_students[$i]["url"] = IMG_PATH_DEFAULT."/student.png";
		}else{
			$arr_students[$i]["url"] = IMG_PATH_STUDENT."/".$file_name_updated;
		}
		if ($module_id == "m_give_points") {
			$arr_students[$i]["points"] = $total_points;
		}
		
		if ($module_id == "m_give_points") {
	?>		
             <li class="col-lg-2 col-md-4 col-sm-3 col-xs-4 col-xxs-12 bspHasModal" style="border:1px solid #CCC; background-color:#f9fafc; cursor:pointer; border-radius:15px; list-style:none; margin:10px 10px 0px 10px; text-align:center;  width: 18%; height:180px !important; ">
                <input type="checkbox"  style="float:right; position:absolute; right:10px; top:0;" name="option" id="option<?=$i?>" value="<?=$tbl_student_id?>" />
                <div style="float:left; width:100%; padding-top:10px;">
                	<img  class="img-circle" alt="<?=$name?>&nbsp;[<?=$name_ar?>]"  src="<?=$arr_students[$i]["url"]?>" width="60" height="60" style="color:#778070;" title="Click On To Message" alt="Click On To Message" /><br />
                	<div style="float:left; width:100%; font-size:15px;"><?=$name_ar?></div>
                	<div style="clear:both;"></div>
                	<div style="float:left; width:100%; font-size:15px;"><?=$name?></div>
                </div>
                <div style="bottom:8px;float:left;position: absolute;width:100%; left:0;">
                 <div style="width:65%; float:left;"> <button class="btn-01" type="button" style="cursor:pointer" onclick="get_behaviour_points('<?=$tbl_student_id?>', '<?=$name?>')">Add Behaviour Points</button></div>
                 <div style="width:35%; float:left;"><span>Points</span><div style="border: 1px solid #2b649b; border-radius: 50%; height:30px; width:30px; float:right; margin-right:15px; box-shadow: 0 1px 0 rgba(255, 255, 255, 0.4) inset; color:#fff; background:#3c8dbc;"><?=$total_points?></div></div>
                </div>
            </li>    
            
            
<?php		
		} else if ($module_id == "m_contact_parent" && $contact_parents == "Y") {
?>		   
         
            <li class="col-lg-2 col-md-4 col-sm-3 col-xs-4 col-xxs-12 bspHasModal" style="border:1px solid #CCC; background-color:#f9fafc; cursor:pointer; border-radius:15px; list-style:none; margin:10px 10px 0px 10px; text-align:center;  width: 18%; height:180px !important; ">
                <input type="checkbox"  style="float:right; position:absolute; right:10px; top:0;" name="option" id="option<?=$i?>" value="<?=$tbl_student_id?>" />
                <div style="float:left; width:100%; padding-top:10px;" onclick="popup_option('<?=$tbl_student_id?>', '<?=$name?>', '<?=$i?>')">
                <img  class="img-circle" alt="<?=$name?>&nbsp;[<?=$name_ar?>]"  src="<?=$arr_students[$i]["url"]?>" width="60" height="60" style="color:#778070;" title="Click On To Message" alt="Click On To Message"><br />
                <div style="float:left; width:100%; font-size:15px;"><?=$name_ar?></div>
                <div style="clear:both;"></div>
                <div style="float:left; width:100%; font-size:15px;"><?=$name?></div>
                </div>
              
                <div style="bottom:8px;float:left;position: absolute;width:100%; left:0;">
                 <div style="width:50%; float:left;"> <button class="btn-01" type="button" style="cursor:pointer" onclick="get_all_conversation('<?=$tbl_student_id?>', '<?=$name?>', '<?=$i?>')">Group Conversation</button></div>
                 <div style="width:50%; float:left;"><button class="btn-01" type="button" style="cursor:pointer" onclick="get_all_private_msg('<?=$tbl_student_id?>', '<?=$name?>', '<?=$i?>')" >Private Conversation</button></div>
                </div>
            </li>    
            
            
<?php		
		} else if ($module_id == "m_contact_parent" && $issue_cards == "Y") {
?>		
            <li class="col-lg-2 col-md-4 col-sm-3 col-xs-4 col-xxs-12 bspHasModal" style="border:1px solid #CCC; background-color:#f9fafc; cursor:pointer; border-radius:15px; list-style:none; margin:10px 10px 0px 10px; text-align:center;  width: 18%; height:180px !important; ">
                <input type="checkbox"  style="float:right; position:absolute; right:10px; top:0;" name="option" id="option<?=$i?>" value="<?=$tbl_student_id?>" />
                <div style="float:left; width:100%; padding-top:10px;" onclick="popup_option('<?=$tbl_student_id?>', '<?=$name?>', '<?=$i?>')">
                <img  class="img-circle" alt="<?=$name?>&nbsp;[<?=$name_ar?>]"  src="<?=$arr_students[$i]["url"]?>" width="60" height="60" style="color:#778070;" title="Click On To Message" alt="Click On To Message"><br />
                <div style="float:left; width:100%; font-size:15px;"><?=$name_ar?></div>
                <div style="clear:both;"></div>
                <div style="float:left; width:100%; font-size:15px;"><?=$name?></div>
                </div>
              
                <div style="bottom:8px;float:left;position: absolute;width:100%; left:0;">
                     <div style="width:45%; float:left;"> <button class="btn-01" type="button" style="cursor:pointer" onclick="get_issue_cards_form('<?=$tbl_student_id?>', '<?=$name?>')">Issue Cards</button></div>
                     <div style="width:55%; float:left;">
                         <div style="border: 1px solid #fa2307; border-radius: 50%; height:30px; width:30px; float:right; margin-right:15px; color:#000;"><?=$total_red?></div>
                         <div style="border: 1px solid #faf707; border-radius: 50%; height:30px; width:30px; float:right; margin-right:15px; color:#000;"><?=$total_yellow?></div> 
                         <div style="border: 1px solid #0a7429; border-radius: 50%; height:30px; width:30px; float:right; margin-right:15px; color:#000;"><?=$total_green?></div>
                         
                    </div>
                </div>
                
            </li>    
            
 <?php		
		} else if ($module_id == "m_contact_parent" && $performance_calc == "Y") {
?>		
               <li class="col-lg-2 col-md-4 col-sm-3 col-xs-4 col-xxs-12 bspHasModal" style="border:1px solid #CCC; background-color:#f9fafc; cursor:pointer; border-radius:15px; list-style:none; margin:10px 10px 0px 10px; text-align:center !important;  width: 18%; height:180px !important; ">
                <input type="checkbox"  style="float:right; position:absolute; right:10px; top:0;" name="option" id="option<?=$i?>" value="<?=$tbl_student_id?>" />
                <div style="float:left; width:100%; padding-top:10px;">
                <img  class="img-circle" alt="<?=$name?>&nbsp;[<?=$name_ar?>]"  src="<?=$arr_students[$i]["url"]?>" width="60" height="60" style="color:#778070;"><br />
                <div style="float:left; width:100%; font-size:15px;"><?=$name_ar?></div>
                <div style="clear:both;"></div>
                <div style="float:left; width:100%; font-size:15px;"><?=$name?></div>
                </div>
              
                <div style="bottom:8px;float:left;position: absolute;width:85%; center:0;">
                 <div style="width:100%; float:left;"> <button class="btn-01" type="button" style="cursor:pointer" onclick="get_performance_monitor_form('<?=$tbl_student_id?>', '<?=$name?>')">Add Performance</button></div>
                </div>
            </li>     
            
            
            
            
<?php		
		} else if ($module_id == "m_contact_parent" && $contact_parents != "Y") {
?>		
            <tr>
              <td align="left" valign="middle"><img src="<?=$arr_students[$i]["url"]?>" height="100" /></td>
              <td align="left" valign="middle">
                <a href="#" onclick="get_behaviour_points('<?=$tbl_student_id?>', '<?=$name?>')">
                <?=$name?>
                </a>
              </td>
            </tr>
<?php		
		} }	
?>
		<input type="hidden" name="total_count" id="total_count" value="<?=count($data_rs)?>" />

</ul>









