<?php
//Init Parameters
$student_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}
?>
<style>
.txt_en {
	text-align:left;
	padding-left:2px;
}
.txt_ar {
	text-align:right;
	padding-right:2px;	
	direction:rtl;		
}
</style>
<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#divPromo').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
			$('#divPromo').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/all_students";
			}
		})
	});
	



	
	
	
	function ajax_create() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/add_student",
			data: {
				student_id_enc: "<?=$student_id_enc?>",
				first_name    : $('#first_name').val(),
				last_name     : $('#last_name').val(),
				first_name_ar : $('#first_name_ar').val(),
				last_name_ar  : $('#last_name_ar').val(),
				dob_month     : $('#dob_month').val(),
				dob_day       : $('#dob_day').val(),
				dob_year      : $('#dob_year').val(),
				gender        : $('input[name=gender]:checked').val(),
				mobile        : $('#mobile').val(),
				email         : $('#email').val(),
				tbl_emirates_id : $('#tbl_emirates_id').val(),
				country       : $('#country').val(),
				emirates_id_father : $('#emirates_id_father').val(),
				emirates_id_mother : $('#emirates_id_mother').val(),
				tbl_academic_year_id : $('#tbl_academic_year_id').val(),
				tbl_class_id         : $('#tbl_class_id').val(),
				
				first_name_parent      : $('#first_name_parent').val(),
				first_name_parent_ar   : $('#first_name_parent_ar').val(),
				last_name_parent       : $('#last_name_parent').val(),
				last_name_parent_ar    : $('#last_name_parent_ar').val(),
				dob_month_parent       : $('#dob_month_parent').val(),
				dob_day_parent         : $('#dob_day_parent').val(),
				dob_year_parent        : $('#dob_year_parent').val(),
				gender_parent          : $('input[name=gender_parent]:checked').val(), 
				mobile_parent          : $('#mobile_parent').val(),
				email_parent           : $('#email_parent').val(),
				emirates_id_parent     : $('#emirates_id_parent').val(),
				parent_user_id         : $('#parent_user_id').val(),
				password               : $('#password').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
				refresh_page = "Y";
				    my_alert("Student added successfully.", 'green');
				    $("#pre-loader").hide();
				}else{
					refresh_page = "N";
					my_alert("Student added failed, Please try again.", 'red');
					$("#pre-loader").hide();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

</script>
<script language="javascript">
   //add student
   /* || validate_picture() == false*/
	function ajax_validate() {
		if (validate_first_name() == false || validate_last_name() == false || validate_emirates_id() == false || validate_dob() == false || isMobile() == false ||  validate_email() == false || validate_country() == false || validate_emirates_id_father() == false || validate_emirates_id_mother() == false || validate_emirates_id_parent_same()== false || validate_emirates_id_parent() == false || validate_class() == false || validate_academic_year() == false || validate_exist_student()==false ) 
		{
			return false;
		} 
		else {
		
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/is_exist_parent_info",
			dataType: "json",
			data: {
				emirates_id_father: $('#emirates_id_father').val(),
				emirates_id_mother: $('#emirates_id_mother').val(),
				is_ajax: true
			},
			success: function(data) {
			   $("#first_name_parent").val(data[0]['first_name']);
			   $("#first_name_parent_ar").val(data[0]['first_name_ar']);
			   $("#last_name_parent").val(data[0]['last_name']);
			   $("#last_name_parent_ar").val(data[0]['last_name_ar']);
			   $("#dob_month_parent").val(data[0]['dob_month']);
			   $("#dob_day_parent").val(data[0]['dob_day']);
			   $("#dob_year_parent").val(data[0]['dob_year']);
			   $("#gender_parent").val(data[0]['gender']);
			   $("#email_parent").val(data[0]['email']);
			   $("#parent_user_id").val(data[0]['user_id']);
			   if(data[0]['emirates_id']!='')
			    	$("#emirates_id_parent").val(data[0]['emirates_id']);
			   else
			   		$("#emirates_id_parent").val($('#emirates_id_father').val());
			   
			   
			   if(data[0]['mobile']!='')
			   {
				   var str = data[0]['mobile'];
				   str = str.replace("+971", "");
			   }
			   $("#mobile_parent").val($.trim(str));
			   $("#password").val('123456');
			   $("#confirm_password").val('123456');
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
			
			
			
			
		}
	}
	
	//Validate Parent
	
	function ajax_validate_parent() {
		if (validate_first_name_parent() == false || validate_last_name_parent() == false || validate_dob_parent() == false || isMobile_parent() == false ||  validate_email_parent() == false || validate_parent_emirates_id() == false || validate_user_id() == false || validate_password() == false || validate_confirm_password() == false || isPasswordSame()==false || validate_exist_parent()==false   ) 
		{
			return false;
		} 
		
	}	
	
	
  function validate_exist_parent()
   {
	   $.ajax({
				type: "POST",
				url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/parents/is_exist_parent_user_id",
				dataType: "text",
				data: {
					 parent_id_enc  : '<?=$parent_id_enc?>',
					 parent_user_id : $("#parent_user_id").val(),
					is_ajax: true
				},
				success: function(data) {
					    var temp = new String();
						temp = data;
						temp = temp.trim();
						if (temp=='Y') {
							my_alert("User Id is already exist. please try another one.", 'red');
							$("#pre-loader").hide();
							return false;
						}else{
						   is_exist();
						}
					
					},
					error: function() {
						$("#pre-loader").hide();
					}, 
					complete: function() {
						$("#pre-loader").hide();
					}
				});
   }
	
	
	
	
	function is_exist() {
		$("#pre-loader").show();
		
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/is_exist_student",
			data: {
				student_id_enc: "<?=$student_id_enc?>",
				first_name: $('#first_name').val(),
				last_name: $('#last_name').val(),
				tbl_class_id: $('#tbl_class_id').val(),
				tbl_emirates_id: $('#tbl_emirates_id').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
					refresh_page = "N";
					my_alert("Student is already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_create();
				}
			
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	

   function validate_first_name() {
		var regExp = / /g;
		var str = $("#first_name").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("The First Name [En] is blank. Please write First Name [En].")
			$("#first_name").val('');
			$("#first_name").focus();
		return false;
		}
		var regExp = / /g;
		var str = $("#first_name_ar").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("The First Name [Ar] is blank. Please write First Name [Ar].")
			$("#first_name_ar").val('');
			$("#first_name_ar").focus();
		return false;
		}
	}
	
	function validate_last_name() {
		var regExp = / /g;
		var str = $("#last_name").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("The Last Name [En] is blank. Please write Last Name [En].")
			$("#last_name").val('');
			$("#last_name").focus();
		return false;
		}
		var regExp = / /g;
		var str = $("#last_name_ar").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("The Last Name [Ar] is blank. Please write Last Name [Ar].")
			$("#last_name_ar").val('');
			$("#last_name_ar").focus();
		return false;
		}
	}
	
	
	
	function isMobile() {
		var strPhone = $.trim($("#mobile").val());
		if($.trim(strPhone)== "") {
			return true;
		}else{
				if (strPhone.length < 8 || strPhone.length > 10) {
					my_alert("Please enter valid mobile number.");
					$("#mobile").focus();
					return false;
			    }

				for (var i = 0; i < strPhone.length; i++) {
				var ch = strPhone.substring(i, i + 1);
					if  (ch < "0" || "9" < ch) {
						my_alert("The mobile number in digits only, Please re-enter your valid mobile number");
						$("#mobile").focus();
						return false;
					}
				}
		}
	 return true;
	}

</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>
<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> My Child<small> &nbsp;</small> </h1>
    <!--/HEADING--> 
    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>My Child</li>
    </ol>
    <div class="box-tools" style="float:right;">
     <a href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/mychild_teachers/child_id_enc/<?=$tbl_sel_student_id?>/tbl_school_id/<?=$tbl_sel_school_id?>/offset/0"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
     </div>
    <div style="clear:both"></div>
  </section>
 
  <section class="content"> 
    <!--WORKING AREA-->	

 <link href="<?=HOST_URL?>/assets/admin/dist/css/jquery-ui.css" rel="stylesheet">
 <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-1.11.1.js"></script>
 <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-ui.js"></script>
  <script>
  $( function() {
		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
				
				/* var order = $("#tabledivbody").sortable("serialize");
				
				alert(order);
				
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/your/url/here'
				});*/
				
				
				
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	
				
				
				
				
				
			}
	  } );
  
  } );
  </script> 
  
  
  
  <!--File Upload START-->
<link href="<?=HOST_URL?>/assets/admin/dist/css/uploadfile.min.css" rel="stylesheet">
<script>
 $( function() {
    $( "#tabs" ).tabs();
  } );
  
 
</script>
<style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url('<?=IMG_PATH?>/delete.jpg');
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
		clear:both;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
	#overlay_container {
		position:relative;
	}
	#overloading {
		background-image:url('<?=IMG_PATH?>/preloader/preloader_2.gif');
		background-repeat:no-repeat;
		background-position:center;
		background-color:#CCC;
		position:absolute;
		left:0px;
		top:0px;
		opacity: 0.3;
		z-index: 10000;
	}
	#div_listing_container {
		display:none;	
	}
	.d_d_text {
		color:#745156;
		font-size:20px;
			
	}
	.ajax-upload-dragdrop {
		margin:auto;
		margin-bottom:10px;
		width:700px !important;
	}
	.ajax-file-upload-statusbar {
		margin:auto;
		margin-top:10px;
	}
	.ajax-file-upload {
		height:31px;
	}
	
	
	 #tabs-1{  
	    overflow-y:scroll; overflow-x:none;
	}

    #tabs-2{
		overflow-y:scroll; overflow-x:none;
	}
				  
  .ui-tabs-active{
		border-color:#efca86  !important;
   }
					 
	.ui-tabs .ui-tabs-nav li {
		float:left;
		font-size: 16px;
        font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif;
  }
  label{
	  display: inline-block;
      font-weight: 700;
  }
  
  .ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button {
    font-family:"Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
    font-size: 14px;
}
  
  .ui-widget{
	 font-size: 16px;
     font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
  }
  .form-control{
	 font-size: 14px; 
  }
</style>
                  
    
                    <?php /*?><div id="mid1" class="box box-success">
                        <div class="box-header">
                          <div class="col-sm-1" >
                          <h3 class="box-title">SEARCH</h3>
                          </div>
                          <div class="col-sm-11"> 
                              <div class="col-sm-3"> 
                             
                              <select name="tbl_class_search_id" id="tbl_class_search_id" class="form-control">
                              <option value="">--Select Class --</option>
							  
							  <?php
                                    for ($u=0; $u<count($classes_list); $u++) { 
                                        $tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
                                        $class_name             = $classes_list[$u]['class_name'];
                                        $class_name_ar          = $classes_list[$u]['class_name_ar'];
										$section_name           = $classes_list[$u]['section_name'];
                                        $section_name_ar        = $classes_list[$u]['section_name_ar'];
                                        if($tbl_class_search_id == $tbl_class_id_u)
                                           $selClass = "selected";
                                         else
                                           $selClass = "";
                                  ?>
                                      <option value="<?=$tbl_class_id_u?>"  <?=$selClass?>  >
                                      <?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;
                                    <?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>   
                                 
                                 
                                 
                               </div>
                               <div class="col-sm-6"><input name="q" id="q" value="<?=urldecode($q)?>" type="text" class="form-control" placeholder="Search By Name, Email, Mobile, Country, Gender, Emirates Id, Parent Name etc."   > </div>
                               <div class="col-sm-2"><button class="btn btn-success" type="button" onclick="search_data()">Search</button>&nbsp;<button class="btn btn-success" type="button" 
                               onclick="reset_data();">Reset</button>
                               </div>
                           
                          </div>
                        </div>  
                     </div><?php */?>  
                     
                     

            <!--Listing-->
                    <div id="mid1_list" class="box">
                        <div class="box-header">
                          <h3 class="box-title">My Child</h3>
                         <?php /*?> <div class="box-tools">
                            <?php if (count($rs_all_students)>0) { echo $paging_string;}?>	
                            <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
                            <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>
                             <button class="btn bg-green fa fa-print" type="button" title="Generate Student ID"  onclick="generate_student_id(<?=$offset?>)" ></button>
                             <button class="btn bg-green fa fa-print" type="button" title="Generate Student ID Card"  onclick="generate_student_id_card(<?=$offset?>)" ></button>
                          </div><?php */?>
                        </div> 
                        
                        <div class="box-body">
                     <!--   <div style="color:#030; font-weight:bold;">You can sort students by using drag and drop of rows </div>-->
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="35%" align="center" valign="middle">Name</th>
                              <th width="10%" align="center" valign="middle">Picture</th>
                              <th width="55%" align="center" valign="middle">Action</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                                for ($i=0; $i<count($rs_all_students); $i++) { 
                                    $id = $rs_all_students[$i]['id'];
                                    $tbl_student_id     = $rs_all_students[$i]['tbl_student_id'];
									$tbl_school_id      = $rs_all_students[$i]['tbl_school_id'];
                                    $name_en            = ucfirst(trim($rs_all_students[$i]['first_name']))." ".ucfirst(trim($rs_all_students[$i]['last_name'])); 
									$name_ar            = $rs_all_students[$i]['first_name_ar']." ".$rs_all_students[$i]['last_name_ar'];
									$parent_name_en     = ucfirst($rs_all_students[$i]['parent_first_name'])." ".ucfirst($rs_all_students[$i]['parent_last_name']);
									$parent_name_ar     = $rs_all_students[$i]['parent_first_name_ar']." ".$rs_all_students[$i]['parent_last_name_ar'];
                                    $mobile             = $rs_all_students[$i]['mobile'];
									$pic                = $rs_all_students[$i]['file_name_updated'];
									$email              = $rs_all_students[$i]['email'];
									$gender             = ucfirst($rs_all_students[$i]['gender']);
                                    $added_date         = $rs_all_students[$i]['added_date'];
                                    $is_active          = $rs_all_students[$i]['is_active'];
									$class_name         = $rs_all_students[$i]['class_name'];
                                    $class_name_ar      = $rs_all_students[$i]['class_name_ar'];
									$section_name       = $rs_all_students[$i]['section_name'];
									$section_name_ar    = $rs_all_students[$i]['section_name_ar'];
                                    $school_type        = $rs_all_students[$i]['school_type'];
									$school_type_ar     = $rs_all_students[$i]['school_type_ar'];
									if($pic<>"") // class="img-circle"
										$pic_path           =   '<img width="100" height="80" src="'.IMG_PATH_STUDENT.'/'.$pic.'"  />';
									else
										$pic_path           =   '<img width="100" height="80" src="'.IMG_PATH_STUDENT.'/no_img.png"  
										style="border-color:1px solid #7C858C !important;" />';
                                    
                                    
                                    $added_date = date('m-d-Y',strtotime($added_date));
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_student_id?>" >
                             <?php /*?> <td align="left" valign="middle">
                              <span style="float:left;">
                              <input id="student_id_enc" name="student_id_enc" class="checkbox" type="checkbox" value="<?=$tbl_student_id?>" />
                              </span>
                           
                              </td><?php */?>
                             <!-- <td align="left" valign="middle"><?=$offset+$i+1?></td>-->
                              <td align="left" valign="middle">
                              <div class="txt_en"><?=$name_en?></div><div class="txt_ar"><?=$name_ar?></div><br />
                              <div class="txt_en"><?=$class_name?>&nbsp;<?=$section_name?></div><div class="txt_ar"><?=$class_name_ar?>&nbsp;<?=$section_name_ar?></div>
                             <?php if($gender=="Female"){?>
                              <i class="fa fa-female" aria-hidden="true"></i>
                             <?php } else { ?>
                              <i class="fa fa-male" aria-hidden="true"></i>
                             <?php } ?>
                              </td> 
                              <td align="left" style="vertical-align:middle; text-align:center;"><?=$pic_path?></td>
                              <td align="left"  style="vertical-align:middle;"> 
                              <span onclick="ajax_teachers_list('<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/mychild_teachers/child_id_enc/<?=$tbl_student_id?>/tbl_school_id/<?=$tbl_school_id?>/tbl_parent_id/<?=$tbl_sel_parent_id?>/offset/0')"  class="label label-success" style="cursor:pointer; font-size:100% !important; font-weight:normal; margin-right:10px; ">Message To Teachers</span>&nbsp;&nbsp;
                              <span onclick="popup_message_to_school('<?=$tbl_student_id?>','<?=$tbl_school_id?>','<?=$tbl_sel_parent_id?>')"   class="label label-success" style="cursor:pointer; font-size:100% !important; font-weight:normal; margin-right:10px;">Message To School</span>&nbsp;&nbsp;
                              <span onclick="popup_message_to_govt('<?=$tbl_student_id?>','<?=$tbl_school_id?>','<?=$tbl_sel_parent_id?>')"  class="label label-success" style="cursor:pointer; font-size:100% !important; font-weight:normal; margin-right:10px;">Message To Government</span>&nbsp;&nbsp;
                              <span onclick="popup_message_to_timeline('<?=$tbl_student_id?>','<?=$tbl_school_id?>','<?=$tbl_sel_parent_id?>')"  class="label label-success" style="cursor:pointer; font-size:100% !important; font-weight:normal; margin-right:10px;">Message To Timeline Technology</span>
                              </td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="10" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_all_students)<=0) {
                            ?>
                            <tr>
                              <td colspan="10" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no child available. 
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->

<script src="<?=HOST_URL?>/assets/admin/dist/js/jquery.uploadfile.min.js"></script>

<script language="javascript">

// Button Actions
	function ajax_teachers_list(url) {
        window.location.href = url; 
	}
	
	function popup_message_to_school(tbl_student_id,tbl_school_id,tbl_parent_id) {
		$("#txt_sch_student_id").val(tbl_student_id);
		$("#txt_sch_school_id").val(tbl_school_id);
		$("#txt_sch_parent_id").val(tbl_parent_id);
		$('#myModal_school').modal('show');
	}
	
	function popup_message_to_govt(tbl_student_id,tbl_school_id,tbl_parent_id) {
		$("#txt_govt_student_id").val(tbl_student_id);
		$("#txt_govt_school_id").val(tbl_school_id);
		$("#txt_govt_parent_id").val(tbl_parent_id);
        $('#myModal_govt').modal('show');
	}
	
	function popup_message_to_timeline(tbl_student_id,tbl_school_id,tbl_parent_id) {
	   $("#txt_timeline_student_id").val(tbl_student_id);
	   $("#txt_timeline_school_id").val(tbl_school_id);
	   $("#txt_timeline_parent_id").val(tbl_parent_id);
       $('#myModal_timeline').modal('show');
	}




var item_id = "<?=$student_id_enc?>";//Primary Key for a Form. 
if(item_id=="")
  var item_id = $("#student_id_enc").val();

function set_item_id(obj) {
	item_id = obj.value;
	get_files();	
}

$(document).ready(function() {
	var uploadObj = $("#advancedUpload").uploadFile({
		url:"<?=HOST_URL?>/file_mgmt/upload_the_file",
		multiple:true,
		autoSubmit:true,
		maxFileSize:130000,
		fileName:"myfile",
		formData: {"module_name":"student"},
		dynamicFormData: function() {
			var data = { item_id:item_id}
			return data;
		},
		showStatusAfterSuccess:false,
		dragDropStr: "<span class='d_d_text'>Optionally Drag and Drop the File to Upload.</span>",
		abortStr:"Abourt",
		cancelStr:"Cancel",
		doneStr:"Done",
		multiDragErrorStr: "Multi Drag Error.",
		extErrorStr:"Extention Error:",
		sizeErrorStr:"Max Size Error:",
		uploadErrorStr:"Upload Error",
		onSelect:function(files) {
 		},
		onSubmit:function(files) {
 		},
		onSuccess:function(files, data, xhr) {
			if (data == "error") {
				alert("Error uploading file. Please try again.");
				return;
			}
			var obj = JSON.parse(data);
			var tbl_uploads_id = obj.tbl_uploads_id;
			var file_name_updated = obj.file_name_updated;
			
			//alert("tbl_uploads_id: "+tbl_uploads_id)
			//alert("file_name_updated: "+file_name_updated)
			add_uploaded_item(tbl_uploads_id, file_name_updated);
		},
		afterUploadAll:function() {
 		},
		onError: function(files, status, errMsg) {
 		}
	});

	$("#startUpload").click(function() {
		uploadObj.startUpload();
	});
	
	try { 
		$('input[type=file]').click();
	} catch(e) {
		alert(e)
	}
});

//Function called when file is uploaded
function add_uploaded_item(tbl_uploads_id, file_name_updated) {
	var str = "<div id='"+tbl_uploads_id+"' class='box-header with-border'> <div class='box-title'><img src='<?=IMG_PATH_STUDENT?>/"+file_name_updated+"' /></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_img_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
		
	$("#div_listing_container").show();
	$("#div_listing_container").append(str);
	$(".ajax-upload-dragdrop").hide();//Hide the upload button
return;
}

function confirm_delete_img_popup(tbl_uploads_id) {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		$('#'+tbl_uploads_id).hide();	
		$(".ajax-upload-dragdrop").show();
		
		var url_str = "<?=HOST_URL?>/file_mgmt/delete_file";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					tbl_uploads_id: tbl_uploads_id
				},
			success: function(data) {
				$("#pre-loader").hide();
			}
		});	
	} else {
		$("#pre-loader").hide();		
	}
}

function get_files() {
	var url_str = "<?=HOST_URL?>/misc/get_files.php";
	
	$.ajax({
		type: "POST",
		url: url_str,
		data: {
				module_name: "student",
				show_del: "Y",
				item_id: item_id//global variable
			},
		success: function(data) {
			$('#div_listing_container').show();
			$('#div_listing_container').html(data)
			
		}
	});	
}
	
</script>
<!--File Upload END-->
     <!-- myModal_school -->
     <div id="myModal_school" class="modal fade" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Send Message To School</h4>
              </div>
              
              <div class="box-body">
              <label id="message_school_success" style="display:none; color: #060; font-size:13px !important;">&nbsp;Message Sent successfully.</label>
              
              <div class="form-group">
                     <label class="col-sm-2 control-label" for="description">Message</label>
                     <div class="col-sm-10">
                              <textarea tabindex="1" dir="ltr" id="txt_message_school" placeholder="Write your message" name="txt_message_school" class="form-control" rows="5"></textarea>
                              <label id="message_school_error" style="display:none; color:#F00; font-size:13px !important;">&nbsp;Please write your message</label>
                     </div>
               </div>
               </div>
               <div class="modal-footer" >
                <button type="button" class="btn btn-default" onclick="send_message_school();" >Send Message</button>
                <input id="txt_sch_student_id" name="txt_sch_student_id" value="" type="hidden" />
                <input id="txt_sch_school_id" name="txt_sch_school_id" value="" type="hidden" />
                <input id="txt_sch_parent_id" name="txt_sch_parent_id" value="" type="hidden" />
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->  
        
        
         <!-- myModal_school -->
     <div id="myModal_govt" class="modal fade" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Send Message To Government</h4>
              </div>
              <div class="modal-body" id="modal-body" >
                	<!--Upload Files START-->
                     <div class="box-body">
                        <div class="form-group">
                         <label class="col-sm-2 control-label" for="txt_subject_govt">Subject</label>
                         <div class="col-sm-10">
                          <input tabindex="1" dir="ltr" id="txt_subject_govt" placeholder="Write your subject" name="txt_subject_govt" class="form-control" />
                          <label id="subject_govt_error" style="display:none; color:#F00; font-size:13px !important;">&nbsp;Please write your subject</label>
                         </div>
                        </div>
                        <br />
                        <div class="form-group">
                         <label class="col-sm-2 control-label" for="description">Message</label>
                         <div class="col-sm-10">
                          <textarea tabindex="2" dir="ltr" id="txt_message_govt" placeholder="Write your message" name="txt_message_govt" class="form-control" rows="5"></textarea>
                          <label id="message_govt_error" style="display:none; color:#F00; font-size:13px !important;">&nbsp;Please write your message</label>
                         </div>
                       </div>
                     </div>
                    
                    <!--Upload Files END--> 
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="send_message_govt();" >Send Message</button>
                 <input id="txt_govt_student_id" name="txt_govt_student_id" value="" type="hidden" />
                <input id="txt_govt_school_id" name="txt_govt_school_id" value="" type="hidden" />
                <input id="txt_govt_parent_id" name="txt_govt_parent_id" value="" type="hidden" />
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->       
        
         <!-- myModal_school -->
     <div id="myModal_timeline" class="modal fade" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Send Message To Timeline Technology Solution</h4>
              </div>
              <div class="modal-body" id="modal-body" >
                	 <div class="box-body">
                        <div class="form-group">
                         <label class="col-sm-2 control-label" for="txt_subject_timeline">Subject</label>
                         <div class="col-sm-10">
                          <input tabindex="1" dir="ltr" id="txt_subject_timeline" placeholder="Write your subject" name="txt_subject_timeline" class="form-control" />
                          <label id="subject_timeline_error" style="display:none; color:#F00; font-size:13px !important;">&nbsp;Please write your subject</label>
                         </div>
                        </div>
                        <br />
                        <div class="form-group">
                         <label class="col-sm-2 control-label" for="description">Message</label>
                         <div class="col-sm-10">
                         <textarea tabindex="2" dir="ltr" id="txt_message_timeline" placeholder="Write your message" name="txt_message_timeline" class="form-control" rows="5"></textarea>
                         <label id="message_timeline_error" style="display:none; color:#F00; font-size:13px !important;">&nbsp;Please write your message</label>
                         </div>
                       </div>
                     </div> 
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="send_message_timeline();" >Send Message</button>
                <input id="txt_timeline_student_id" name="txt_timeline_student_id" value="" type="hidden" />
                <input id="txt_timeline_school_id" name="txt_timeline_school_id" value="" type="hidden" />
                <input id="txt_timeline_parent_id" name="txt_timeline_parent_id" value="" type="hidden" />
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->      
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >

    function send_message_school()
	{
		if($("#txt_message_school").val()=='')
		{
			$("#message_school_error").show();
			return false;
		}else{
			$("#message_school_error").hide();
		
			$.ajax({
				type: "POST",
				url: "<?=HOST_URL?>/en/parent/parent_user/sendParentMessageToSchool",
				data: {
					message: $("#txt_message_school").val(),
					tbl_student_id: $("#txt_sch_student_id").val(),
					tbl_school_id: $("#txt_sch_school_id").val(),
					tbl_parent_id: $("#txt_sch_parent_id").val(),
					is_ajax: true
				},
				success: function(data) {
					$('#myModal_school').modal('hide');
					my_alert("Message sent successfully.", "green");
					setTimeout(function(){window.location.reload();},1000);
				},
				error: function() {
					$('#pre_loader').css('display','none');	
				}, 
				complete: function() {
					$('#pre_loader').css('display','none');	
				}
			});	
		}
	}
	
	function send_message_govt()
	{
		if($("#txt_subject_govt").val()=='')
		{
			$("#subject_govt_error").show();
			return false;
		}else{
			$("#subject_govt_error").hide();
		}
		
		
		if($("#txt_message_govt").val()=='')
		{
			$("#message_govt_error").show();
			return false;
		}else{
			$("#message_govt_error").hide();
		
			$.ajax({
				type: "POST",
				url: "<?=HOST_URL?>/en/parent/parent_user/sendParentMessageToGovt",
				data: {
					subject: $("#txt_subject_govt").val(),
					message: $("#txt_message_govt").val(),
					tbl_student_id: $("#txt_govt_student_id").val(),
					tbl_school_id: $("#txt_govt_school_id").val(),
					tbl_parent_id: $("#txt_govt_parent_id").val(),
					is_ajax: true
				},
				success: function(data) {
					$('#myModal_govt').modal('hide');
					my_alert("Message sent successfully.", "green");
					setTimeout(function(){window.location.reload();},1000);
				},
				error: function() {
					$('#pre_loader').css('display','none');	
				}, 
				complete: function() {
					$('#pre_loader').css('display','none');	
				}
			});	
		}
	}
	
	function send_message_timeline()
	{
		if($("#txt_subject_timeline").val()=='')
		{
			$("#subject_timeline_error").show();
			return false;
		}else{
			$("#subject_timeline_error").hide();
		}
		
		
		if($("#txt_message_timeline").val()=='')
		{
			$("#message_timeline_error").show();
			return false;
		}else{
			$("#message_timeline_error").hide();
		
			$.ajax({
				type: "POST",
				url: "<?=HOST_URL?>/en/parent/parent_user/sendParentMessageToTimeline",
				data: {
					subject: $("#txt_subject_timeline").val(),
					message: $("#txt_message_timeline").val(),
					tbl_student_id: $("#txt_timeline_student_id").val(),
					tbl_school_id: $("#txt_timeline_school_id").val(),
					tbl_parent_id: $("#txt_timeline_parent_id").val(),
					is_ajax: true
				},
				success: function(data) {
					$('#myModal_timeline').modal('hide');
					my_alert("Message sent successfully.", "green");
					setTimeout(function(){window.location.reload();},1000);
				},
				error: function() {
					$('#pre_loader').css('display','none');	
				}, 
				complete: function() {
					$('#pre_loader').css('display','none');	
				}
			});	
		}
	}
 

function search_data() {
		var tbl_class_search_id = $("#tbl_class_search_id").val();
		var q = $.trim($("#q").val());
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/all_students/";
		
		if(tbl_class_search_id !='')
			url += "tbl_class_search_id/"+tbl_class_search_id+"/";
		
		if(q !='')
			url += "q/"+q+"/";
		
			url += "offset/0/";
		window.location.href = url;
		<?php /*?>window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/enquiry/all_enquiries/is_not_replied/"+is_not_replied+"/tbl_court_id/"+tbl_court_id+"/tbl_category_id/"+tbl_category_id;<?php */?>
	}

    function reset_data() {
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/all_students/";
		url += "offset/0/";
		window.location.href = url;
	}

  
	// Button Actions
	function ajax_teachers_list(url) {
        window.location.href = url; 
	}
	
	function popup_message_to_school(tbl_student_id,tbl_school_id,tbl_parent_id) {
		$("#txt_sch_student_id").val(tbl_student_id);
		$("#txt_sch_school_id").val(tbl_school_id);
		$("#txt_sch_parent_id").val(tbl_parent_id);
		$('#myModal_school').modal('show');
	}
	
	function popup_message_to_govt(tbl_student_id,tbl_school_id,tbl_parent_id) {
		$("#txt_govt_student_id").val(tbl_student_id);
		$("#txt_govt_school_id").val(tbl_school_id);
		$("#txt_govt_parent_id").val(tbl_parent_id);
        $('#myModal_govt').modal('show');
	}
	
	function popup_message_to_timeline(tbl_student_id,tbl_school_id,tbl_parent_id) {
	   $("#txt_timeline_student_id").val(tbl_student_id);
	   $("#txt_timeline_school_id").val(tbl_school_id);
	   $("#txt_timeline_parent_id").val(tbl_parent_id);
       $('#myModal_timeline').modal('show');
	}

	
 </script>                       
