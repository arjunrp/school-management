
<?php
$primary_id_enc = md5(uniqid(rand()));
if (trim($mid) == "") {
	$mid = "1";	
}
if ($module_id == "m_give_points") {
	$module_text = "Assign Points";
	$module_text_big = "Assign";
	$module_text_small = "Points";
	$module_short = "Points";
} else if ($module_id == "m_contact_parent" && $issue_cards == "Y") {
	$module_text = "Issue Cards";
	$module_text_big = "Issue";
	$module_text_small = "Cards";
	$module_short = "Issue Cards";
} else if ($module_id == "m_contact_parent" && $contact_parents == "Y") {
	$module_text = "Contact Parents";
	$module_text_big = "Contact";
	$module_text_small = "Parents";
	$module_short = "Contact Parents";
} else if ($module_id == "m_contact_parent" && $performance_calc == "Y") {
	$module_text = "Performance Monitor";
	$module_text_big = "Performance";
	$module_text_small = "Monitor";
	$module_short = "Performance Monitor";
} else if ($module_id == "cdrpt") {
	$module_text = "Send Reports";
	$module_text_big = "Send";
	$module_text_small = "Reports";
	$module_short = "Send Reports";
} else if ($module_id == "m_teacher_attendance") {
	$module_text = "Attendance Register";
	$module_text_big = "Attendance";
	$module_text_small = "Register";
	$module_short = "Attendance Register";
}
?>
<script language="javascript">
	var host = "<?=HOST_URL?>";
	var lan = "<?=LAN_SEL?>";
	var user_id = "<?=$_SESSION["tbl_teacher_id_sess"]?>";
	var school_id = "<?=$_SESSION["tbl_school_id_sess"]?>";
	var module_id = "<?=$module_id?>";
	var issue_cards = '<?=$issue_cards?>';
	var contact_parents = '<?=$contact_parents?>';
	var performance_calc = '<?=$performance_calc?>';
	
	var class_name_g = "";
	var section_name_g = "";
	var tbl_class_id_g = "";
	var tbl_student_id_g = "";
	var student_name_g = "";
	var index_g = "";
	
	$("#pre-loader").show();
	// Get class list on page load
	$(document).ready(function(e) {
		get_teacher_all_classes();
    });
	
	function get_teacher_all_classes() {
		$("#pre-loader").show();
		var url_str = host+"/classes/class_list?user_id="+user_id+"&role=T&lan=en&device=none&device_uid=none&school_id="+school_id+"&module_id="+module_id;
		//alert(url_str);
        $.ajax({
			type: "POST",
			url: url_str,
			data: {
				is_admin: "Y"
			},
			success: function(data) {
				$("#class_list").html(data);
				$("#pre-loader").hide();
				
				hide_all_panels();
				show_panel("mid1");
			}
		});
	}
		
	function get_teacher_all_students(tbl_class_id, class_name, section_name) {
		$("#pre-loader").show();
		
		var url_str;
		
		if (module_id == "m_teacher_attendance") {
			url_str = host+"/teacher_attendance/attendance_page?user_id="+user_id+"&role=T&lan=en&device=none&device_uid=none&school_id="+school_id+"&module_id="+module_id+"&tbl_class_id="+tbl_class_id;
		} else if (issue_cards == "Y") {
			url_str = host+"/student/student_list?user_id="+user_id+"&role=T&lan=en&device=none&device_uid=none&school_id="+school_id+"&module_id="+module_id+"&tbl_class_id="+tbl_class_id+"&issue_cards=Y";
		} else if (contact_parents == "Y") {
			url_str = host+"/student/student_list?user_id="+user_id+"&role=T&lan=en&device=none&device_uid=none&school_id="+school_id+"&module_id="+module_id+"&tbl_class_id="+tbl_class_id+"&contact_parents=Y";
		} else if (performance_calc == "Y") {
			url_str = host+"/student/student_list?user_id="+user_id+"&role=T&lan=en&device=none&device_uid=none&school_id="+school_id+"&module_id="+module_id+"&tbl_class_id="+tbl_class_id+"&performance_calc=Y";
		}  else {
			url_str = host+"/student/student_list?user_id="+user_id+"&role=T&lan=en&device=none&device_uid=none&school_id="+school_id+"&module_id="+module_id+"&tbl_class_id="+tbl_class_id;
		}
		
		//alert(url_str);
        $.ajax({
			type: "POST",
			url: url_str,
			data: {
				is_admin: "Y"
			},
			success: function(data) {
				var temp_str = new String(data);
				if (temp_str.indexOf(':"200"')>0) {
					$("#pre-loader").hide();
					alert("There is no data available");
					return;
				}
				tbl_class_id_g = tbl_class_id;
				class_name_g = class_name;
				section_name_g = section_name;

				$("#class_name").html(class_name+"-"+section_name);
				$("#student_list").html(data);
				
				if (module_id == "m_teacher_attendance") {
					get_class_sessions();
				}
				
				hide_all_panels();
				show_panel("mid2");
				$("#pre-loader").hide();
			}
		});
	}	
	
	function get_behaviour_points(tbl_student_id, student_name) {
		$("#pre-loader").show();
		
		var url_str = host+"/student/get_behaviour_points?user_id="+user_id+"&role=T&lan=en&device=none&device_uid=none&tbl_school_id="+school_id;
		//alert(url_str);
		
		$.ajax({
			type: "POST",
			url: url_str,
			data: {
				is_admin: "Y",
				student_name: student_name
			},
			success: function(data) {
				var temp_str = new String(data);
				if (temp_str.indexOf(':"200"')>0) {
					$("#pre-loader").hide();
					alert("There is no data available");
					return;
				}

				tbl_student_id_g = tbl_student_id;
				$("#student_name").html(student_name);
				$("#points_list").html(data);
				
				hide_all_panels();
				show_panel("mid3");
				
				$("#pre-loader").hide();
			}
		});
	}
	
	function save_points() {
		var points_student = $("#points_student").val();
		var points_message = $("#points_student").find(':selected').attr('data-pointsmessage');
		//alert(points_text);
		var url_str = host+"/student/save_points?user_id="+user_id+"&role=T&lan=en&device=none&device_uid=none&school_id="+school_id+"&tbl_class_id="+tbl_class_id_g+"&tbl_student_id="+tbl_student_id_g+"&points_student="+points_student+"&message="+points_message;
		//alert(url_str);
		
		if (confirm("Are you sure you want to assign points?")) {
			$.ajax({
			type: "POST",
			url: url_str,
			data: {
				is_admin: "Y"
			},
			success: function(data) {
				var temp_str = new String(data);
				if (temp_str.indexOf(':"200"')>0) {
					$("#pre-loader").hide();
					alert("Points have been assigned successfully");
					return;
				}
				$("#pre-loader").hide();
			}
		});
		}
	}
	
	
	
	function get_issue_cards_form(tbl_student_id, student_name) {
		$("#pre-loader").show();
		
		var url_str = host+"/teachers/issue_cards_form?user_id="+user_id+"&role=T&lan=en&device=none&device_uid=none&school_id="+school_id+"&tbl_class_id="+tbl_class_id_g+"&tbl_student_id="+tbl_student_id;
		//alert(url_str);
		
		$.ajax({
			type: "POST",
			url: url_str,
			data: {
				is_admin: "Y",
				student_name: student_name
			},
			success: function(data) {
				var temp_str = new String(data);
				if (temp_str.indexOf(':"200"')>0) {
					$("#pre-loader").hide();
					alert("There is no data available");
					return;
				}

				tbl_student_id_g = tbl_student_id;
				student_name_g = student_name;
				$("#student_name_cards").html(student_name);
				$("#class_name_cards").html(class_name_g+" "+section_name_g);
				
				$("#cards_list").html(data);
				
				hide_all_panels();
				show_panel("mid4");
				
				$("#pre-loader").hide();
			}
		});
	}
	
	function get_issue_cards_form_refresh() {
		get_issue_cards_form(tbl_student_id_g, student_name_g);
	}

	function get_card_list_by_type(card_type) {
		$("#pre-loader").show();
		
		var url_str = host+"/student/get_card_list_by_type?user_id="+user_id+"&role=T&lan=en&device=none&device_uid=none&school_id="+school_id+"&tbl_student_id="+tbl_student_id_g+"&card_type="+card_type;
		//alert(url_str);
		
		$.ajax({
			type: "POST",
			url: url_str,
			data: {
				is_admin: "Y",
				student_name: student_name
			},
			success: function(data) {
				var temp_str = new String(data);
				if (temp_str.indexOf(':"200"')>0) {
					$("#pre-loader").hide();
					alert("There is no data available");
					return;
				}

				tbl_student_id_g = tbl_student_id;
				$("#student_name_cards").html(student_name);
				$("#card_list_by_type").html(data);
				
				hide_all_panels();
				show_panel("mid5");
				
				$("#pre-loader").hide();
			}
		});
	}
	
	function issue_card(card_type, card_issue_type, message) {
		$("#pre-loader").show();
		
		var url_str = host+"/student/issue_card?user_id="+user_id+"&role=T&lan=en&device=none&device_uid=none&school_id="+school_id+"&tbl_class_id="+tbl_class_id_g+"&tbl_student_id="+tbl_student_id_g+"&card_type="+card_type+"&message="+message+"&card_issue_type="+card_issue_type;
		if (!confirm("Do you want to issue card?")) {
			$("#pre-loader").hide();
			return;
		}
		
		$.ajax({
			type: "POST",
			url: url_str,
			data: {
				is_admin: "Y"
			},
			success: function(data) {
				var temp_str = new String(data);
				if (temp_str.indexOf(':"200"')>0) {
					$("#pre-loader").hide();
					alert("Card has been issued successfully.");
					get_issue_cards_form_refresh();

					return;
				}
				$("#pre-loader").hide();
			}
		});
	}
	
	function submit_daily_report() {
		var total_count = $("#total_count").val();
		var report_str = new String();
		var tbl_student_id;
		var option1, option2, option3;
		//alert(total_count);
		
		for(i=0; i<total_count; i++) {
			tbl_student_id = $("#option"+i+"0").attr("data-studentid");
			option1 =  get_boolean($("#option"+i+"0").prop("checked"));
			option2 =  get_boolean($("#option"+i+"1").prop("checked"));
			option3 =  get_boolean($("#option"+i+"2").prop("checked"));
			report_str = report_str + tbl_student_id + ":" + option1 + "," + option2 + "," + option3 + ";";
		}
		
		var url_str = host+"/student/submit_daily_report?user_id="+user_id+"&role=T&lan=en&device=none&device_uid=none&school_id="+school_id+"&report_str="+report_str;
		//alert(url_str);
		//return;
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: url_str,
			data: {
				is_admin: "Y"
			},
			success: function(data) {
				var temp_str = new String(data);
				if (temp_str.indexOf(':"200"')>0) {
					$("#pre-loader").hide();
					alert("Report has been submitted successfully.");
					return;
				}
				$("#pre-loader").hide();
			}
		});
		
	}
	
	function get_boolean(boolean_val) {
		if (boolean_val == true) {
			return "Y";
		} else {
			return "N";
		}
	}
	
	function get_class_sessions() {
		var url_str = host+"/class_sessions/get_class_sessions/?user_id="+user_id+"&role=T&lan=en&device=none&device_uid=none&school_id="+school_id+"&tbl_class_id="+tbl_class_id_g;
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: url_str,
			data: {
				is_admin: "Y"
			},
			success: function(data) {
				var temp_str = new String(data);
				if (temp_str.indexOf(':"200"')>0) {
					$("#pre-loader").hide();
					//alert("Report has been submitted successfully.");
					return;
				}
				$("#tbl_class_sessions_id").html(data);
				$("#pre-loader").hide();
			}
		});
	
	}
	
	function submit_attendance() {
		var total_count = $("#total_count").val();
		var att_str = new String();
		var tbl_student_id;
		var option;
		var tbl_class_sessions_id = $("#tbl_class_sessions_id").val();
		if (tbl_class_sessions_id == "" || tbl_class_sessions_id == null) {
			alert("Please select a Class Session from the drop down list.");
			return;
		}
		
		var attendance_date = '<?=date('Y-m-d')?>';
	    /*for(i=0; i<total_count; i++) {
			tbl_student_id = $("#option"+i).attr("data-studentid");
			option = get_boolean($("#option"+i).prop("checked"));
			att_str = att_str + tbl_student_id + ":" + option + ",";
		}*/
		
		for(i=0; i<total_count; i++) {
			dataVal = $("#option"+i).val();
			var attendanceVal  = dataVal.split('_');
			var attendance     = attendanceVal[0];			
			var tbl_student_id = attendanceVal[1];
			var att_str = att_str + tbl_student_id + ":" + attendance + ",";
		}
		
		var url_str = host+"/teacher_attendance/submit_attendance?user_id="+user_id+"&role=T&lan=en&device=none&device_uid=none&school_id="+school_id+"&att_str="+att_str+"&attendance_date="+attendance_date+"&tbl_class_id="+tbl_class_id_g+"&tbl_class_sessions_id="+tbl_class_sessions_id;
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: url_str,
			data: {
				is_admin: "Y"
			},
			success: function(data) {
				var temp_str = new String(data);
				if (temp_str.indexOf(':"200"')>0) {
					$("#pre-loader").hide();
					get_class_sessions();
					alert("Attendance has been submitted successfully.");
					return;
				}
				$("#pre-loader").hide();
			}
		});
	}
	
	function select_unselect_all_students() {
		var total_count = $("#total_count").val();
		var option;
		
		for(i=0; i<total_count; i++) {
			if ($("#option"+i).prop("checked")) {
				$("#option"+i).prop("checked", false);
			} else {
				$("#option"+i).prop("checked", true);
			}
		}
	}
	
	function save_message() {
		
		var total_count = $("#total_count").val();
		var student_str = new String();
		var url_str;
		for(i=0; i<total_count; i++) {
			if ($("#option"+i).prop("checked") == false) {
				continue;
			}
			tbl_student_id = $("#option"+i).val();
			student_str = student_str + tbl_student_id + ":";
		}
		
		if (student_str == "") {
			alert("Select at least one student to proceed.");
			return;
		}
		
		// Send Text Message		
		if($("#tab1").attr("aria-expanded") == true || $("#tab1").attr("aria-expanded") == "true") {
			
			if ($("#message").val().trim() == ""){
				alert("Please provide text for the message.");
				return;
			}
			
			var message_type = "message";
			//alert($('#is_audio').prop('checked'));
		
			url_str = host+"/message/save_message?user_id="+user_id+"&role=T&lan=en&device=none&device_uid=none&school_id="+school_id+"&tbl_class_id="+tbl_class_id_g+"&tbl_student_id="+student_str+"&message_type="+message_type+"&message="+$("#message").val().trim()+"&rnd=<?=md5(uniqid(rand()))?>";
			//alert(url_str);
			//return;
			$("#pre-loader").show();
			$.ajax({
				type: "POST",
				url: url_str,
				data: {
					is_admin: "Y"
				},
				success: function(data) {
					//alert(data);
					var temp_str = new String(data);
					if (temp_str.indexOf(':"200"')>0) {
						$("#pre-loader").hide();
						$("#message").val("");
						alert("Message has been sent successfully.");
						get_teacher_all_students_g();
						return;
					}
					$("#pre-loader").hide();
				}
			});
			
		// Send Audio Message	
		} else if($("#tab2").attr("aria-expanded") == true || $("#tab2").attr("aria-expanded") == "true") {
			/*alert(school_id);
			alert(tbl_class_id_g);
			alert(student_str);
			alert($("#message_picture").val().trim());
			alert(tbl_item_id_str);*/
			if (is_file_uploaded_flg == 0) {
				alert("Upload Picture/Audio file to proceed.");
				return;
			}
			
			var message_type = "message";
			alert($('#is_audio').prop('checked'));
			if($('#is_audio').prop('checked')) {
				message_type = "audio";
			} else if($('#is_picture').prop('checked')) {
				message_type = "picture";				
			} else {
				message_type = "message";
			}
			
			
			url_str = host+"/message/save_message?user_id="+user_id+"&role=T&lan=en&device=none&device_uid=none&school_id="+school_id+"&tbl_class_id="+tbl_class_id_g+"&tbl_student_id="+tbl_student_id_g+"&message_type="+message_type+"&message="+$("#message").val().trim()+"&rnd=<?=md5(uniqid(rand()))?>";
			//alert(url_str);
			//alert($("#picture_caption").val());
			$("#pre-loader").show();
			$.ajax({
				type: "POST",
				url: url_str,
				data: {
					is_admin: "Y",
					file_name_original:file_name_original_g,
					file_name_updated:file_name_updated_g,
					file_name_updated_thumb:file_name_updated_thumb_g,
					file_type:file_type_g,
					file_size:file_size_g,
					picture_caption: $("#picture_caption").val()
				},
				success: function(data) {
					//alert(data);
					var temp_str = new String(data);
					if (temp_str.indexOf(':"200"')>0) {
						$("#pre-loader").hide();
						$("#message").val("");
						alert("Message has been sent successfully.");
						get_teacher_all_students_g();
						return;
					}
					$("#pre-loader").hide();
				}
			});
		}
	}
	
	function get_teacher_all_students_g() {
		get_teacher_all_students(tbl_class_id_g, class_name_g, section_name_g);
	}
	
	function hide_all_panels() {
		if (document.getElementById("mid1") !="") {	$('#mid1').hide();	}
		if (document.getElementById("mid2") !="") {	$('#mid2').hide();	}
		if (document.getElementById("mid3") !="") {	$('#mid3').hide();	}
		if (document.getElementById("mid4") !="") {	$('#mid4').hide();	}
		if (document.getElementById("mid5") !="") {	$('#mid5').hide();	}
		if (document.getElementById("mid6") !="") {	$('#mid6').hide();	}
		if (document.getElementById("mid7") !="") {	$('#mid7').hide();	}
		if (document.getElementById("mid8") !="") {	$('#mid8').hide();	}
		if (document.getElementById("mid9") !="") {	$('#mid9').hide();	}
		if (document.getElementById("mid20") !="") { $('#mid20').hide();	}
	}
		
	function show_panel(panel_id) {
		$("#"+panel_id).show(500);
	}
	
	function popup_option(tbl_student_id, name, index) {
		//alert(tbl_student_id);
		$("#popupid"+index).toggle();
	}
	
	function get_all_conversation(tbl_student_id, name, index) {
		$("#pre-loader").show();
		
		var url_str;
		
		url_str = host+"/message/get_message_list/?user_id="+user_id+"&role=T&lan=en&device=none&device_uid=none&school_id="+school_id+"&tbl_student_id="+tbl_student_id+"&module_id=m_contact_parent&rnd=<?=md5(uniqid(rand()))?>";
		
		//alert(url_str);
        $.ajax({
			type: "POST",
			url: url_str,
			data: {
				is_admin: "Y"
			},
			success: function(data) {
				var temp_str = new String(data);
				if (temp_str.indexOf(':"200"')>0) {
					$("#pre-loader").hide();
					alert("There is no data available");
					return;
				}
				
				tbl_student_id_g = tbl_student_id;
				student_name_g = name;
				index_g = index;
				/*
				var class_name_g = "";
				var section_name_g = "";
				var tbl_class_id_g = "";
				var tbl_student_id_g = "";
				var student_name_g = "";
*/				
				$("#student_name_all_c").html(name);
				$("#all_conversation").html(data);
				
				
				hide_all_panels();
				show_panel("mid6");
				$("#pre-loader").hide();
			}
		});
	}
	
	function get_all_private_msg(tbl_student_id, name, index) {
		$("#pre-loader").show();
		
		var url_str;
		
		url_str = host+"/message/get_private_message_list/?teacher_id="+user_id+"&role=T&lan=en&device=none&device_uid=none&tbl_school_id="+school_id+"&tbl_student_id="+tbl_student_id+"&rnd=<?=md5(uniqid(rand()))?>";
		
		//alert(url_str);
        $.ajax({
			type: "POST",
			url: url_str,
			data: {
				is_admin: "Y"
			},
			success: function(data) {
				var temp_str = new String(data);
				if (temp_str.indexOf(':"200"')>0) {
					$("#pre-loader").hide();
					alert("There is no data available");
					return;
				}
				
				tbl_student_id_g = tbl_student_id;
				student_name_g = name;
				index_g = index;
				/*
				var class_name_g = "";
				var section_name_g = "";
				var tbl_class_id_g = "";
				var tbl_student_id_g = "";
				var student_name_g = "";
*/				
				$("#student_name_all_cp").html(name);
				$("#all_private_msg").html(data);
				
				
				hide_all_panels();
				show_panel("mid7");
				$("#pre-loader").hide();
			}
		});
	}
</script>

<!--File Upload START-->
<link href="http://hayageek.github.io/jQuery-Upload-File/uploadfile.min.css" rel="stylesheet">

<style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url('<?=IMG_PATH?>/delete.jpg');
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
		clear:both;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
	#overlay_container {
		position:relative;
	}
	#overloading {
		background-image:url('<?=IMG_PATH?>/loading.gif');
		background-repeat:no-repeat;
		background-position:center;
		background-color:#CCC;
		position:absolute;
		left:0px;
		top:0px;
		opacity: 0.3;
		z-index: 10000;
	}
	#div_listing_container {
		display:none;	
	}
	.d_d_text 
	{
		color:#745156;
		font-size:20px;
			
	}
	.ajax-upload-dragdrop {
		margin:auto;
		padding-bottom:5px;
	}
	.ajax-file-upload-statusbar {
		margin:auto;
		margin-top:10px;
	}
	.d_d_text {font-size:13px !important;}
	.ajax-upload-dragdrop {width:400px !important;}
	.max_width {max-width:300px;}
	#picture_upload_div {display:none;}
	#picture_upload2_div {display:none;}
	.delete_item {cursor:pointer;}
</style>

<script src="http://hayageek.github.io/jQuery-Upload-File/jquery.uploadfile.min.js"></script>

<script language="javascript">
	var is_file_uploaded_flg = 0;
	$(document).ready(function(e) {
        $(document).on('change', '.is_picture_audio', function() {
			$("#picture_caption").val("");
			if (this.id == "is_picture") {
				$("#picture_caption").show();
			} else {
				$("#picture_caption").hide();
			}
		});
    });
	
	
	var file_name_original_g;	
	var file_name_updated_g;
	var file_name_updated_thumb_g;
	var file_type_g;
	var file_size_g;
	
	function random_str() {
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	
		for( var i=0; i < 15; i++ )
			text += possible.charAt(Math.floor(Math.random() * possible.length));
	
		return text;
	}
	
	function set_item_id(obj) {
		item_id = obj.value;
		get_files();	
	}
	
	$(document).ready(function() {
		//Initialize file upload object
		var uploadObj = $("#picture_upload_component").uploadFile({
			url:"<?=HOST_URL?>/admin/upload_aqdar.php",
			multiple:false,
			autoSubmit:true,
			fileName:"myfile",
			showStatusAfterSuccess:false,
			dragDropStr: "<span class='d_d_text'>Optionally Drag and Drop the File to Upload.</span>",
			abortStr:"Abourt",
			cancelStr:"Cancel",
			doneStr:"Done",
			multiDragErrorStr: "Multi Drag Error.",
			extErrorStr:"Extention Error:",
			sizeErrorStr:"Max Size Error:",
			uploadErrorStr:"Upload Error",
			
			dynamicFormData: function() {
				var upload_type;
				if($('#is_audio').prop('checked')) {
					upload_type = "audio";
				} else {
					upload_type = "picture";
				}
				//alert(upload_type);
				var data = {upload_type:upload_type}
				return data;
			},

			onSelect:function(files) {
			},
			onSubmit:function(files) {
				
			},
			
			onSuccess:function(files, data, xhr) {
				var obj = JSON.parse(data);
				if (obj.error == "1") {
					alert(obj.error_msg);
					return;
				}
				file_name_original_g = obj.file_name_original;
				file_name_updated_g = obj.file_name_updated;
				file_name_updated_thumb_g = obj.file_name_updated_thumb;
				file_type_g = obj.file_type;
				file_size_g = obj.file_size;
				if (data == "error") {
					alert("Error uploading file. Please try again.");
					return;
				}
				
				var temp = new String();
				temp = data;
				if (temp.indexOf("FATAL") >0 ) {
					alert("Fatal Error. Please try another image.");
					return
				}
				
				if($('#is_audio').prop('checked')) {
					$("#picture_upload").attr('src', '<?=HOST_URL?>/images/audio.png');
				} else {
					$("#picture_upload").attr('src', '<?=HOST_URL?>/admin/uploads/'+file_name_updated_g);
				}
				
				$(".ajax-upload-dragdrop").hide();
				$("#picture_upload_div").show();
				$("#pic_aud").hide();
				//$("#picture_caption").val("");
				is_file_uploaded_flg = 1;
				//$("#item_image").val(item_image);
			},
			afterUploadAll:function() {
			},
			onError: function(files, status, errMsg) {
			}
		});
	
		$("#startUpload").click(function() {
			uploadObj.startUpload();
		});
		
		try { 
			$('input[type=file]').click();
		} catch(e) {
			alert(e)
		}
	});
	
	function delete_item(tbl_uploads_id) {
		if (confirm("Are you sure you want to delete?")) {
			
			//Remove the element from database
			var url_str = "<?=HOST_URL?>/admin/delete_aqdar.php";
			//alert(file_name_updated_g);
			$.ajax({
				type: "POST",
				url: url_str,
				data: {
						file_name_updated: file_name_updated_g//global variable
					},
				success: function(data) {
					//alert(data);
					$("#picture_upload").attr('src', '');
					$(".ajax-upload-dragdrop").show();
					$("#picture_upload_div").hide();
					$("#pic_aud").show();
					is_file_uploaded_flg = 0;
				}
			});	
		}
	return;
	}
	
	function get_files() {
		var url_str = "<?=HOST_URL?>/misc/get_item_picture.php";
		
		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					module_name: "create_item",
					item_id: item_id//global variable
				},
			success: function(data) {
				$('#div_listing_container').show();
				$('#div_listing_container').html(data)
				
			}
		});	
	}
</script>
<!--File Upload END-->



<script language="javascript">

function save_message2() {
		
		var url_str1;
		
			$("#picture_upload2_div").hide();
		
		// Send Text Message		
		if($("#tab11").attr("aria-expanded") == true || $("#tab11").attr("aria-expanded") == "true") {
			if ($("#message2").val().trim() == ""){
				alert("Please provide text for the message.");
				return;
			}
			
			var message_type = "message";
			
			url_str1 = host+"/message/save_message?user_id="+user_id+"&role=T&lan=en&device=none&device_uid=none&school_id="+school_id+"&tbl_class_id="+tbl_class_id_g+"&tbl_student_id="+tbl_student_id_g+"&message_type="+message_type+"&message="+$("#message2").val().trim()+"&rnd=<?=md5(uniqid(rand()))?>";
			//alert(url_str1);
			//return;
			$("#pre-loader").show();
			$.ajax({
				type: "POST",
				url: url_str1,
				data: {
					is_admin: "Y"
				},
				success: function(data) {
					//alert(data);
					var temp_str = new String(data);
					if (temp_str.indexOf(':"200"')>0) {
						$("#pre-loader").hide();
						$("#message2").val("");
						alert("Message has been sent successfully.");
						//get_teacher_all_students_g();
						get_all_conversation(tbl_student_id_g, student_name_g, index_g);
						return;
					}
					$("#pre-loader").hide();
				}
			});
			
		// Send Audio Message	
		} else if($("#tab22").attr("aria-expanded") == true || $("#tab22").attr("aria-expanded") == "true") {
			//alert("tab22");
			if (is_file_uploaded_flg2 == 0) {
				alert("Upload Picture/Audio file to proceed.");
				return;
			}
			
			var message_type = "message";
			//alert($('#is_audio2').prop('checked'));
			if($('#is_audio2').prop('checked')) {
				message_type = "audio";
			} else if($('#is_picture2').prop('checked')) {
				message_type = "picture";				
			} else {
				message_type = "message";
			}
			
			url_str1 = host+"/message/save_message?user_id="+user_id+"&role=T&lan=en&device=none&device_uid=none&school_id="+school_id+"&tbl_class_id="+tbl_class_id_g+"&tbl_student_id="+tbl_student_id_g+"&message_type="+message_type+"&message="+$("#picture_caption2").val().trim()+"&rnd=<?=md5(uniqid(rand()))?>";
			//alert(url_str1);
			//alert($("#picture_caption").val());
			$("#pre-loader").show();
			$.ajax({
				type: "POST",
				url: url_str1,
				data: {
					is_admin: "Y",
					file_name_original:file_name_original_g2,
					file_name_updated:file_name_updated_g2,
					file_name_updated_thumb:file_name_updated_thumb_g2,
					file_type:file_type_g2,
					file_size:file_size_g2,
					picture_caption: $("#picture_caption2").val()
				},
				success: function(data) {
					//alert(data);
					var temp_str = new String(data);
					if (temp_str.indexOf(':"200"')>0) {
						$("#pre-loader").hide();
						$("#message2").val("");
						alert("Message has been sent successfully.");
						$("#picture_upload2").attr('src', '');
						$(".ajax-upload-dragdrop").show();
						$("#picture_upload2_div").hide();
						$("#pic_aud2").show();
						$("#picture_caption2").val("");
						is_file_uploaded_flg2 = 0;
						get_all_conversation(tbl_student_id_g, student_name_g, index_g);
						return;
					}
					$("#pre-loader").hide();
				}
			});
		}
	}

	var is_file_uploaded_flg2 = 0;
	$(document).ready(function(e) {
        $(document).on('change', '.is_picture2_audio', function() {
			$("#picture_caption2").val("");
			if (this.id == "is_picture2") {
				$("#picture_caption2").show();
			} else {
				$("#picture_caption2").hide();
			}
		});
    });
	
	var file_name_original_g2;	
	var file_name_updated_g2;
	var file_name_updated_thumb_g2;
	var file_type_g2;
	var file_size_g2;
	
	function random_str2() {
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	
		for( var i=0; i < 15; i++ )
			text += possible.charAt(Math.floor(Math.random() * possible.length));
	
		return text;
	}
	
	function set_item_id2(obj) {
		item_id = obj.value;
		get_files2();	
	}
	
	$(document).ready(function() {
		//Initialize file upload object
		var uploadObj = $("#picture_upload2_component2").uploadFile({
			url:"<?=HOST_URL?>/admin/upload_aqdar.php",
			multiple:false,
			autoSubmit:true,
			fileName:"myfile",
			showStatusAfterSuccess:false,
			dragDropStr: "<span class='d_d_text'>Optionally Drag and Drop the File to Upload.</span>",
			abortStr:"Abourt",
			cancelStr:"Cancel",
			doneStr:"Done",
			multiDragErrorStr: "Multi Drag Error.",
			extErrorStr:"Extention Error:",
			sizeErrorStr:"Max Size Error:",
			uploadErrorStr:"Upload Error",
			
			dynamicFormData: function() {
				var upload_type;
				if($('#is_audio2').prop('checked')) {
					upload_type = "audio";
				} else {
					upload_type = "picture";
				}
				//alert(upload_type);
				var data = {upload_type:upload_type}
				return data;
			},

			onSelect:function(files) {
			},
			
			onSubmit:function(files) {
			},
			
			onSuccess:function(files, data, xhr) {
				var obj = JSON.parse(data);
				if (obj.error == "1") {
					alert(obj.error_msg);
					return;
				}
				file_name_original_g2 = obj.file_name_original;
				file_name_updated_g2 = obj.file_name_updated;
				file_name_updated_thumb_g2 = obj.file_name_updated_thumb;
				file_type_g2 = obj.file_type;
				file_size_g2 = obj.file_size;
				if (data == "error") {
					alert("Error uploading file. Please try again.");
					return;
				}
				
				var temp = new String();
				temp = data;
				if (temp.indexOf("FATAL") >0 ) {
					alert("Fatal Error. Please try another image.");
					return
				}
				
				if($('#is_audio2').prop('checked')) {
					$("#picture_upload2").attr('src', '<?=HOST_URL?>/images/audio.png');
				} else {
					$("#picture_upload2").attr('src', '<?=HOST_URL?>/admin/uploads/'+file_name_updated_g2);
				}
				
				$(".ajax-upload-dragdrop").hide();
				$("#picture_upload2_div").show();
				$("#pic_aud2").hide();
				//$("#picture_caption2").val("");
				is_file_uploaded_flg2 = 1;
				//$("#item_image").val(item_image);
			},
			
			afterUploadAll:function() {
			},
			
			onError: function(files, status, errMsg) {
			}
		});
	
		$("#startUpload").click(function() {
			uploadObj.startUpload();
		});
		
		try { 
			$('input[type=file]').click();
		} catch(e) {
			alert(e)
		}
	});
	
	function delete_item2(tbl_uploads_id) {
		if (confirm("Are you sure you want to delete?")) {
			
			//Remove the element from database
			var url_str = "<?=HOST_URL?>/admin/delete_aqdar.php";
			//alert(file_name_updated_g2);
			$.ajax({
				type: "POST",
				url: url_str,
				data: {
						file_name_updated: file_name_updated_g2//global variable
					},
				success: function(data) {
					//alert(data);
					$("#picture_upload2").attr('src', '');
					$(".ajax-upload-dragdrop").show();
					$("#picture_upload2_div").hide();
					$("#pic_aud2").show();
					is_file_uploaded_flg2 = 0;
				}
			});	
		}
	return;
	}
	
	function get_files2() {
		var url_str = "<?=HOST_URL?>/misc/get_item_picture.php";
		
		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					module_name: "create_item",
					item_id: item_id//global variable
				},
			success: function(data) {
				$('#div_listing_container2').show();
				$('#div_listing_container2').html(data)
				
			}
		});	
	}
</script>
<!--File Upload END-->


<script language="javascript">

function save_message4() {
		
		var url_str2;
		
			$("#picture_upload4_div").hide();
		
		// Send Text Message		
		if($("#tab111").attr("aria-expanded") == true || $("#tab111").attr("aria-expanded") == "true") {
			if ($("#message4").val().trim() == ""){
				alert("Please provide text for the message.");
				return;
			}
			
			var message_type = "message";
			
			url_str2 = host+"/message/savePrivateMessage?message_from="+user_id+"&message_to="+tbl_student_id_g+"&role=T&lan=en&device=none&device_uid=none&message_dir=TP&tbl_school_id="+school_id+"&tbl_student_id="+tbl_student_id_g+"&message_type=text&tbl_message="+$("#message4").val().trim()+"&rnd=<?=md5(uniqid(rand()))?>";
			//alert(url_str2);
			//return;
			$("#pre-loader").show();
			$.ajax({
				type: "POST",
				url: url_str2,
				data: {
					is_admin: "Y"
				},
				success: function(data) {
					//alert(data);
					var temp_str = new String(data);
					if (temp_str.indexOf('sent successfully')>0) {
						$("#pre-loader").hide();
						$("#message4").val("");
						alert("Message has been sent successfully.");
						//get_teacher_all_students_g();
						get_all_private_msg(tbl_student_id_g, student_name_g, index_g);
						return;
					}
					$("#pre-loader").hide();
				}
			});
			
		// Send Audio Message	
		} else if($("#tab222").attr("aria-expanded") == true || $("#tab222").attr("aria-expanded") == "true") {
			//alert("tab222");
			if (is_file_uploaded_flg2 == 0) {
				alert("Upload Picture/Audio file to proceed.");
				return;
			}
			var message_type_url = "";
			var message_type = "message";
			//alert($('#is_audio4').prop('checked'));
			if($('#is_audio4').prop('checked')) {
				message_type = "audio";
				message_type_url = "audio";
			} else if($('#is_picture4').prop('checked')) {
				message_type = "picture";
				message_type_url = "image";
			} else {
				message_type = "message";
			}
			
			url_str2 = host+"/message/savePrivateMessage?message_from="+user_id+"&message_to="+tbl_student_id_g+"&role=T&lan=en&device=none&device_uid=none&message_dir=TP&tbl_school_id="+school_id+"&tbl_student_id="+tbl_student_id_g+"&message_type="+message_type_url+"&tbl_message="+$("#picture_caption4").val().trim()+"&tbl_item_id=<?=md5(uniqid(rand()))?>";
			//alert(url_str2);
			//alert($("#picture_caption").val());
			$("#pre-loader").show();
			$.ajax({
				type: "POST",
				url: url_str2,
				data: {
					is_admin: "Y",
					file_name_original:file_name_original_g4,
					file_name_updated:file_name_updated_g4,
					file_name_updated_thumb:file_name_updated_thumb_g4,
					file_type:file_type_g4,
					file_size:file_size_g4,
					picture_caption: $("#picture_caption4").val()
				},
				success: function(data) {
					//alert(data);
					var temp_str = new String(data);
					if (temp_str.indexOf('sent successfully')>0) {
						$("#pre-loader").hide();
						$("#message4").val("");
						alert("Message has been sent successfully.");
						$("#picture_upload4").attr('src', '');
						$(".ajax-upload-dragdrop").show();
						$("#picture_upload4_div").hide();
						$("#pic_aud4").show();
						$("#picture_caption4").val("");
						is_file_uploaded_flg2 = 0;
						get_all_private_msg(tbl_student_id_g, student_name_g, index_g);
						return;
					}
					$("#pre-loader").hide();
				}
			});
		}
	}

	var is_file_uploaded_flg2 = 0;
	$(document).ready(function(e) {
        $(document).on('change', '.is_picture4_audio', function() {
			$("#picture_caption4").val("");
			if (this.id == "is_picture4") {
				$("#picture_caption4").show();
			} else {
				$("#picture_caption4").hide();
			}
		});
    });
	
	var file_name_original_g4;	
	var file_name_updated_g4;
	var file_name_updated_thumb_g4;
	var file_type_g4;
	var file_size_g4;
	
	function random_str4() {
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	
		for( var i=0; i < 15; i++ )
			text += possible.charAt(Math.floor(Math.random() * possible.length));
	
		return text;
	}
	
	function set_item_id4(obj) {
		item_id = obj.value;
		get_files4();	
	}
	
	$(document).ready(function() {
		//Initialize file upload object
		var uploadObj = $("#picture_upload4_component4").uploadFile({
			url:"<?=HOST_URL?>/admin/upload_aqdar.php",
			multiple:false,
			autoSubmit:true,
			fileName:"myfile",
			showStatusAfterSuccess:false,
			dragDropStr: "<span class='d_d_text'>Optionally Drag and Drop the File to Upload.</span>",
			abortStr:"Abourt",
			cancelStr:"Cancel",
			doneStr:"Done",
			multiDragErrorStr: "Multi Drag Error.",
			extErrorStr:"Extention Error:",
			sizeErrorStr:"Max Size Error:",
			uploadErrorStr:"Upload Error",
			
			dynamicFormData: function() {
				var upload_type;
				if($('#is_audio4').prop('checked')) {
					upload_type = "audio";
				} else {
					upload_type = "picture";
				}
				//alert("upload_type"+upload_type);
				var data = {upload_type:upload_type}
				return data;
			},

			onSelect:function(files) {
			},
			
			onSubmit:function(files) {
			},
			
			onSuccess:function(files, data, xhr) {
				var obj = JSON.parse(data);
				if (obj.error == "1") {
					//alert(obj.error_msg);
					return;
				}
				file_name_original_g4 = obj.file_name_original;
				file_name_updated_g4 = obj.file_name_updated;
				file_name_updated_thumb_g4 = obj.file_name_updated_thumb;
				file_type_g4 = obj.file_type;
				file_size_g4 = obj.file_size;
				if (data == "error") {
					alert("Error uploading file. Please try again.");
					return;
				}
				
				var temp = new String();
				temp = data;
				if (temp.indexOf("FATAL") >0 ) {
					alert("Fatal Error. Please try another image.");
					return
				}
				
				if($('#is_audio4').prop('checked')) {
					$("#picture_upload4").attr('src', '<?=HOST_URL?>/images/audio.png');
				} else {
					$("#picture_upload4").attr('src', '<?=HOST_URL?>/admin/uploads/'+file_name_updated_g4);
				}
				
				$(".ajax-upload-dragdrop").hide();
				$("#picture_upload4_div").show();
				$("#pic_aud4").hide();
				//$("#picture_caption4").val("");
				is_file_uploaded_flg2 = 1;
				//$("#item_image").val(item_image);
			},
			
			afterUploadAll:function() {
			},
			
			onError: function(files, status, errMsg) {
			}
		});
	
		$("#startUpload").click(function() {
			uploadObj.startUpload();
		});
		
		try { 
			$('input[type=file]').click();
		} catch(e) {
			alert(e)
		}
	});
	
	function delete_item4(tbl_uploads_id) {
		if (confirm("Are you sure you want to delete?")) {
			
			//Remove the element from database
			var url_str = "<?=HOST_URL?>/admin/delete_aqdar.php";
			//alert(file_name_updated_g4);
			$.ajax({
				type: "POST",
				url: url_str,
				data: {
						file_name_updated: file_name_updated_g4//global variable
					},
				success: function(data) {
					//alert(data);
					$("#picture_upload4").attr('src', '');
					$(".ajax-upload-dragdrop").show();
					$("#picture_upload4_div").hide();
					$("#pic_aud4").show();
					is_file_uploaded_flg2 = 0;
				}
			});	
		}
	return;
	}
	
	function get_files4() {
		var url_str = "<?=HOST_URL?>/misc/get_item_picture.php";
		
		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					module_name: "create_item",
					item_id: item_id//global variable
				},
			success: function(data) {
				$('#div_listing_container4').show();
				$('#div_listing_container4').html(data)
				
			}
		});	
	}
</script>
<!--File Upload END-->
<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1>
      <?=$module_text_big?>
      <small>
      <?=$module_text_small?>
      </small> </h1>
    <!--/HEADING--> 
    
    <!--BREADCRUMB-->
    <ol class="breadcrumb" style="float:left; position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home/teacher" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>
        <?=$module_text?>
      </li>
    </ol>
    <!--/BREADCRUMB-->
    <div style="clear:both"></div>
  </section>
  <section class="content">
   
    <!--WORKING AREA-->

    <!--Class List-->
    <div id="mid1" class="box">
      <div class="box-header">
        <h3 class="box-title">Class List</h3>
        <div class="box-tools">
          <button class="btn bg-orange fa fa-refresh" type="button" title="Add" onclick="get_teacher_all_classes()"></button>
        </div>
      </div>
      <div class="box-body">
        <table width="100%" class="table table-bordered table-striped">
          <thead>
            <tr>
                <th width="30%" align="center" valign="middle">Section</th>
                <th width="30%" align="center" valign="middle">Class</th>
                <th width="" align="center" valign="middle">Status</th> 
            </tr> 
          </thead>
          <tbody id="class_list">
          </tbody>
          <tfoot>
          </tfoot>
        </table>
      </div>
    </div>
    
    
    <!--Student List-->
    <div id="mid2" class="box" style="display:none;">
      <div class="box-header">
        <h3 class="box-title">
        	<span><?=$module_short;?></span> / <span id="class_name"></span>
        </h3>
        <div class="box-tools">
          <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="get_teacher_all_classes()"></button>
        </div>
      </div>
      <div class="box-body">
	      <div class="box box-primary">
      
      	<?php	if ($module_id == "m_give_points") {	?>
            <table width="100%" class="table table-bordered table-striped">
              <thead>
                <tr>
                    <th width="30%" align="center" valign="middle">Picture</th>
                    <th width="30%" align="center" valign="middle">Student</th>
                    <th width="" align="center" valign="middle">Points</th> 
                </tr> 
              </thead>
              <tbody id="student_list">
              </tbody>
              <tfoot>
              </tfoot>
            </table>
        <?php	} else if ($module_id == "m_contact_parent" && $contact_parents != "Y") {	?>
            <table width="100%" class="table table-bordered table-striped">
              <thead>
                <tr>
                    <th width="20%" align="center" valign="middle">Picture</th>
                    <th width="" align="center" valign="middle">Student</th>
                </tr> 
              </thead>
              <tbody id="student_list">
              </tbody>
              <tfoot>
              </tfoot>
            </table>
            
		 <?php	} else if ($module_id == "m_contact_parent" && $contact_parents == "Y") {	?>
            <table width="100%" class="table table-bordered table-striped">
              <thead>
                <tr>
                    <th width="20%" align="center" valign="middle">Picture</th>
                    <th width="" align="center" valign="middle">Student</th>
                    <th width="" align="center" valign="middle"><input type="checkbox" name="select_unselect_all_students" id="select_unselect_all_students" onclick="select_unselect_all_students(this)" />&nbsp;Action</th>
                </tr> 
              </thead>
              <tbody id="student_list">
              </tbody>
              <tfoot>
              </tfoot>
            </table>
                        
        <?php	} else if ($module_id == "cdrpt" || $module_id == "m_teacher_attendance") {	?>
        
            <div id="student_list"></div>
            
        <?php	}	?>
      </div>
      	<?php	if ($module_id == "cdrpt") {	?>
            <!-- /.box-body -->
            <div class="box-footer">
              <button class="btn btn-primary" type="button" onclick="submit_daily_report()">Send</button>
              <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>--> 
            </div>
            <!-- /.box-footer -->
    	<?php	}	?>
        <?php	if ($module_id == "m_teacher_attendance") {	?>
            <!-- /.box-body -->
            <div class="box-footer">
              <button class="btn btn-primary" type="button" onclick="submit_attendance()">Send</button>
              <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>--> 
            </div>
            <!-- /.box-footer -->
    	<?php	}	?>
        
        
        	<?php	if ($module_id == "m_contact_parent" && $contact_parents == "Y") {	?>
                <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Message</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                  <div class="box-body">
                    <div class="form-group">
                      		<div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                  <li class="active"><a href="#tab_1" id="tab1" data-toggle="tab" aria-expanded="true">Text Message</a></li>
                                  <li class=""><a href="#tab_2" id="tab2" data-toggle="tab" aria-expanded="false">Picture/Audio Message</a></li>
                                </ul>
                                <div class="tab-content">
                                  <div class="tab-pane active" id="tab_1">
                                  	<textarea class="form-control" name="message" id="message" rows="3" cols="20" placeholder="Enter ..."></textarea>  	
                                  </div>
                                  <!-- /.tab-pane -->
                                  <div class="tab-pane" id="tab_2">
                                  	<!--File Upload START-->
									<style>
										#picture_upload_component {
											padding-bottom:5px;
										}
										.listing_container {
											width:80%;
											margin:auto;
											border:1px solid #CCC;
											overflow:auto;
											padding:5px;	
										}
										#file_name {
											font-size:18px;
											color:#745156;
											float:left;
											text-align:left;
											width:80%;
										}
										#del_file {
											float:left;
											width:20%;
										}
                                    </style>
                                    <div>
                                        <div id="pic_aud">
                                            <input name="is_picture_audio" class="is_picture_audio" id="is_audio" type="radio" value="Y">Audio
                                            <input name="is_picture_audio" class="is_picture_audio" id="is_picture" type="radio" checked="checked" value="Y">Picture
                                        </div>
                                        <div style="padding:10px 0px">
                                            <input type="text" name="picture_caption" id="picture_caption" value="" placeholder="Picture Caption" />
                                        </div>
                                    </div>
                                    
                                    <div id="picture_upload_component">Upload Files</div>
                                    <div id="picture_upload_div">
                                        
                                        
                                        <img id="picture_upload" class="max_width" src="" />
                                        <br>
                                        
                                        <div class="delete_item" style="display:none" onclick="delete_item()">Delete</div>
                                    </div>
                                	<!--File Upload END-->                                    	
                                  </div>
                                  
                                  <!-- /.tab-pane -->
                                </div>
                                <!-- /.tab-content -->
                              </div>
                    </div>
                  </div>
                  <!-- /.box-body -->
                </form>
                
                    <div class="box-footer">
                      <button class="btn btn-primary" type="button" onclick="save_message()">Send</button>
                      <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>--> 
                    </div>
                </div>
            <?php	}	?>       
        
        </div> 
                  
    </div>
    <input type="hidden" name="message_picture" id="message_picture" value="" />
    
    <div id="mid3" class="box" style="display:none;">
      <div class="box-header">
        <h3 class="box-title">Assign Points: <span id="student_name"></span></h3>
        <div class="box-tools">
          <button class="btn bg-red fa fa-home" type="button" title="Home" onclick="get_teacher_all_classes()"></button>
          <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="get_teacher_all_students_g()"></button>
        </div>
      </div>
      <div class="box-body">
        <table width="100%" class="table table-bordered table-striped">
          <thead>
            <tr>
                <th width="30%" align="center" valign="middle">Student</th>
                <th width="30%" align="center" valign="middle">Points</th>
            </tr> 
          </thead>
          <tbody id="points_list">
          </tbody>
          <tfoot>
          </tfoot>
        </table>
      </div>
      <!-- /.box-body -->
        <div class="box-footer">
          <button class="btn btn-primary" type="button" onclick="save_points()">Submit</button>
          <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>--> 
        </div>
        <!-- /.box-footer -->
    </div>
    
    
    <div id="mid4" class="box" style="display:none;">
      <div class="box-header">
        <h3 class="box-title"><span id="class_name_cards"></span> / <span id="student_name_cards"></span></h3>
        <div class="box-tools">
        	
          <button class="btn bg-red fa fa-home" type="button" title="Home" onclick="get_teacher_all_classes()"></button>
          <button class="btn bg-orange fa fa-refresh" type="button" title="Refresh" onclick="get_issue_cards_form_refresh()"></button>
          <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="get_teacher_all_students_g()"></button>
        </div>
      </div>
      <div class="box-body">
        <table width="100%" class="table table-bordered table-striped">
          <thead>
            <tr>
                <th width="30%" align="center" valign="middle">Total</th>
                <th width="30%" align="center" valign="middle">Cards</th>
                <th width="30%" align="center" valign="middle">Action</th>
            </tr> 
          </thead>
          <tbody id="cards_list">
          </tbody>
          <tfoot>
          </tfoot>
        </table>
      </div>      
    </div>
    
    <div id="mid20" class="box" style="display:none;">
      <div class="box-header">
        <h3 class="box-title"><span id="class_name_topics"></span> / <span id="student_name_topics"></span></h3>
        <div class="box-tools">
        	
          <button class="btn bg-red fa fa-home" type="button" title="Home" onclick="get_teacher_all_classes()"></button>
          <button class="btn bg-orange fa fa-edit" type="button" title="Update"  onclick="get_performance_topic_form_refresh()"></button>
          <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="get_teacher_all_students_g()"></button>
        </div>
      </div>
      <div class="box-body" id="performance_list">
         
      </div>      
    </div>
    
    
    <div id="mid5" class="box" style="display:none;">
      <div class="box-header">
        <h3 class="box-title">Card Details</h3>
        <div class="box-tools">
          <button class="btn bg-red fa fa-home" type="button" title="Home" onclick="get_teacher_all_classes()"></button>
          <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="get_teacher_all_students_g()"></button>
        </div>
      </div>
      <div class="box-body">
        <table width="100%" class="table table-bordered table-striped">
          <thead>
            <tr>
                <th width="30%" align="center" valign="middle">Card</th>
                <th width="30%" align="center" valign="middle">Date</th>
            </tr> 
          </thead>
          <tbody id="cards_list_by_type">
          </tbody>
          <tfoot>
          </tfoot>
        </table>
      </div>      
    </div>
    
    <?php /*?>$("#student_name_all_c").html(student_name_g);
				$("#all_conversation").html(data);<?php */?>
                
    <div id="mid6" class="box" style="display:none;">
      <div class="box-header">
        <h3 class="box-title">All Conversation (<span id="student_name_all_c"></span>)</h3>
        <div class="box-tools">
          <button class="btn bg-red fa fa-home" type="button" title="Home" onclick="get_teacher_all_classes()"></button>
          <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="get_teacher_all_students_g()"></button>
        </div>
      </div>
      <div class="box-body">
        <table width="100%" class="table table-bordered table-striped">
          <thead>
            <tr>
                <th width="10%" align="center" valign="middle">Picture</th>
                <th width="15%" align="center" valign="middle">Name</th>
                <th width="30%" align="center" valign="middle">Message</th>
                <th width="20%" align="center" valign="middle">Picture/Audio</th>
                <th width="10%" align="center" valign="middle">Date</th>
            </tr> 
          </thead>
          <tbody id="all_conversation">
          </tbody>
          <tfoot>
          </tfoot>
        </table>
      </div>
  		
        <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Message</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                  <div class="box-body">
                    <div class="form-group">
                      		<div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                  <li class="active"><a href="#tab_11" id="tab11" data-toggle="tab" aria-expanded="true">Text Message</a></li>
                                  <li class=""><a href="#tab_22" id="tab22" data-toggle="tab" aria-expanded="false">Picture/Audio Message</a></li>
                                </ul>
                                <div class="tab-content">
                                  <div class="tab-pane active" id="tab_11">
                                  	<textarea class="form-control" name="message2" id="message2" rows="3" cols="20" placeholder="Enter ..."></textarea>  	
                                  </div>
                                  <!-- /.tab-pane -->
                                  <div class="tab-pane" id="tab_22">
                                  	<!--File Upload START-->
									<style>
										#picture_upload2_component2 {
											padding-bottom:5px;
										}
										.listing_container {
											width:80%;
											margin:auto;
											border:1px solid #CCC;
											overflow:auto;
											padding:5px;	
										}
										#file_name1 {
											font-size:18px;
											color:#745156;
											float:left;
											text-align:left;
											width:80%;
										}
										#del_file1 {
											float:left;
											width:20%;
										}
                                    </style>
                                    <div>
                                        <div id="pic_aud2">
                                            <input name="is_picture2_audio" class="is_picture2_audio" id="is_audio2" type="radio" value="Y">Audio
                                            <input name="is_picture2_audio" class="is_picture2_audio" id="is_picture2" type="radio" checked="checked" value="Y">Picture
                                        </div>
                                        <div style="padding:10px 0px">
                                            <input type="text" name="picture_caption2" id="picture_caption2" value="" placeholder="Picture Caption" />
                                        </div>
                                    </div>
                                    
                                    <div id="picture_upload2_component2">Upload Files</div>
                                    <div id="picture_upload2_div">
                                        <img id="picture_upload2" class="max_width" src="" />
                                        <br>
                                        <div class="delete_item" onclick="delete_item2()">Delete</div>
                                    </div>
                                	<!--File Upload END-->                                    	
                                  </div>
                                  
                                  <!-- /.tab-pane -->
                                </div>
                                <!-- /.tab-content -->
                              </div>
                    </div>
                  </div>
                  <!-- /.box-body -->
                </form>
                
                    <div class="box-footer">
                      <button class="btn btn-primary" type="button" onclick="save_message2()">Send</button>
                      <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>--> 
                    </div>
                </div>

    </div>

    <div id="mid7" class="box" style="display:none;">
      <div class="box-header">
        <h3 class="box-title">Private Message (<span id="student_name_all_cp"></span>)</h3>
        <div class="box-tools">
          <button class="btn bg-red fa fa-home" type="button" title="Home" onclick="get_teacher_all_classes()"></button>
          <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="get_teacher_all_students_g()"></button>
        </div>
      </div>
      <div class="box-body">
        <table width="100%" class="table table-bordered table-striped">
          <thead>
            <tr>
                <th width="10%" align="center" valign="middle">Picture</th>
                <th width="15%" align="center" valign="middle">Name</th>
                <th width="30%" align="center" valign="middle">Message</th>
                <th width="20%" align="center" valign="middle">Picture/Audio</th>
                <th width="10%" align="center" valign="middle">Date</th>
            </tr> 
          </thead>
          <tbody id="all_private_msg">
          </tbody>
          <tfoot>
          </tfoot>
        </table>
      </div>
  		
        <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Message</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                  <div class="box-body">
                    <div class="form-group">
                      		<div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                  <li class="active"><a href="#tab_111" id="tab111" data-toggle="tab" aria-expanded="true">Text Message</a></li>
                                  <li class=""><a href="#tab_222" id="tab222" data-toggle="tab" aria-expanded="false">Picture/Audio Message</a></li>
                                </ul>
                                <div class="tab-content">
                                  <div class="tab-pane active" id="tab_111">
                                  	<textarea class="form-control" name="message4" id="message4" rows="3" cols="20" placeholder="Enter ..."></textarea>  	
                                  </div>
                                  <!-- /.tab-pane -->
                                  <div class="tab-pane" id="tab_222">
                                  	<!--File Upload START-->
									<style>
										#picture_upload4_component4 {
											padding-bottom:5px;
										}
										.listing_container {
											width:80%;
											margin:auto;
											border:1px solid #CCC;
											overflow:auto;
											padding:5px;	
										}
										#file_name1 {
											font-size:18px;
											color:#745156;
											float:left;
											text-align:left;
											width:80%;
										}
										#del_file1 {
											float:left;
											width:20%;
										}
                                    </style>
                                    <div>
                                        <div id="pic_aud4">
                                            <input name="is_picture4_audio" class="is_picture4_audio" id="is_audio4" type="radio" value="Y">Audio
                                            <input name="is_picture4_audio" class="is_picture4_audio" id="is_picture4" type="radio" checked="checked" value="Y">Picture
                                        </div>
                                        <div style="padding:10px 0px">
                                            <input type="text" name="picture_caption4" id="picture_caption4" value="" placeholder="Picture Caption" />
                                        </div>
                                    </div>
                                    
                                    <div id="picture_upload4_component4">Upload Files</div>
                                    <div id="picture_upload4_div">
                                        <img id="picture_upload4" class="max_width" src="" />
                                        <br>
                                        <div class="delete_item" onclick="delete_item4()">Delete</div>
                                    </div>
                                	<!--File Upload END-->                                    	
                                  </div>
                                  
                                  <!-- /.tab-pane -->
                                </div>
                                <!-- /.tab-content -->
                              </div>
                    </div>
                  </div>
                  <!-- /.box-body -->
                </form>
                
                    <div class="box-footer">
                      <button class="btn btn-primary" type="button" onclick="save_message4()">Send</button>
                      <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>--> 
                    </div>
                </div>

    </div>
    <!--/Listing--> 
    
    <!--Add or Create-->
    <div id="mid44" class="box box-primary" style="display:none">
      <div class="box-header with-border">
        <h3 class="box-title">Create Dictionary</h3>
        <div class="box-tools">
          <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
        </div>
      </div>
      <!-- /.box-header --> 
      <!-- form start -->
      <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
        <div class="box-body">
          <div class="box-body">
            <table class="table table-bordered table-striped" id="example1" width="100%">
              <thead>
                <tr>
                  <th valign="middle" align="center">Language</th>
                  <th width="25%" valign="middle" align="center">Dictionary Text</th>
                  <th width="25%" valign="middle" align="center"> Speech Text
                    <div class="fa"></div>
                  </th>
                  <th width="30%" valign="middle" align="center">Comments
                    <div class="fa"></div>
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php	for($i=0; $i<count($data_trans_rs); $i++){	
							$tbl_translation_language_idd = $data_trans_rs[$i]["tbl_translation_language_id"];
							$translation_language = $data_trans_rs[$i]["translation_language"];
				?>
                <tr>
                  <td valign="middle" align="left"><?=$translation_language?></td>
                  <td valign="middle" align="left"><input type="text" name="translation_word_arr[]" value="" /></td>
                  <td valign="middle" align="left"><input type="text" name="speech_text_arr[]" value="" /></td>
                  <td valign="middle" align="left"><textarea name="description_arr[]" rows="3" cols="30"></textarea></td>
                </tr>
              <input type="hidden" name="tbl_translation_language_id_arr[]" value="<?=$tbl_translation_language_idd?>" />
              <?php	}	?>
              <tr>
                <td colspan="4" valign="middle" align="right"></td>
              </tr>
                </tbody>
              
              <tfoot>
              </tfoot>
            </table>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <button class="btn btn-primary" type="button" onclick="ajax_validate()">Create Dictionary</button>
          <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>--> 
        </div>
        <!-- /.box-footer -->
      </form>
    </div>
    <!--/Add or Create--> 
    
    <!--/WORKING AREA--> 
  </section>
</div>
<script>

function get_performance_monitor_form(tbl_student_id, student_name) {
		$("#pre-loader").show();
		var url_str = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/performance_monitor_form?user_id="+user_id+"&role=T&lan=en&device=none&device_uid=none&school_id="+school_id+"&tbl_class_id="+tbl_class_id_g+"&tbl_student_id="+tbl_student_id;
		$.ajax({
			type: "POST",
			url: url_str,
			data: {
				is_admin: "Y",
				student_name: student_name
			},
			success: function(data) {
				var temp_str = new String(data);
				if (temp_str.indexOf(':"200"')>0) {
					$("#pre-loader").hide();
					alert("There is no data available");
					return;
				}
				
				tbl_student_id_g = tbl_student_id;
				student_name_g = student_name;
				$("#student_name_topics").html(student_name);
				$("#class_name_topics").html(class_name_g+" "+section_name_g);
				
				$("#performance_list").html(data);
				hide_all_panels();
				show_panel("mid20");
				$("#pre-loader").hide();	 
				
			}
		});
	
	}
	
	function get_performance_topic_form_refresh() {
		get_performance_monitor_form(tbl_student_id_g, student_name_g);
	}

</script>