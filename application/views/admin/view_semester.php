<?php
//Init Parameters
$semester_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}


?>
<link rel="stylesheet" media="all" type="text/css" href="<?=JS_PATH?>/date_time_picker/css/jquery-ui-1.8.6.custom.css" />
<style type="text/css">
pre {
	padding: 20px;
	background-color: #ffffcc;
	border: solid 1px #fff;
}
.example-container {
	background-color: #f4f4f4;
	border-bottom: solid 2px #777777;
	margin: 0 0 40px 0;
	padding: 20px;
}

.example-container p {
	font-weight: bold;
}

.example-container dt {
	font-weight: bold;
	height: 20px;
}

.example-container dd {
	margin: -20px 0 10px 100px;
	border-bottom: solid 1px #fff;
}

.example-container input {
	width: 150px;
}

.clear {
	clear: both;
}

#ui-datepicker-div {
}

.ui-timepicker-div .ui-widget-header {
	margin-bottom: 8px;
}

.ui-timepicker-div dl {
	text-align: left;
}

.ui-timepicker-div dl dt {
	height: 25px;
}

.ui-timepicker-div dl dd {
	margin: -25px 0 10px 65px;
}

.ui-timepicker-div td {
	font-size: 90%;
}
</style>

<script type="text/javascript" src="<?=JS_PATH?>/date_time_picker/js/jquery-ui-1.8.6.custom.min.js"></script>
<script type="text/javascript" src="<?=JS_PATH?>/date_time_picker/js/jquery-ui-timepicker-addon.js"></script>
<script language="javascript">
	$(document).ready(function(){
		$('#start_date').datepicker({
			dateFormat: '',
			timeFormat: 'hh:mm tt',
			timeOnly: true   
		});


		$('#end_date').datepicker({
			dateFormat: '',
			timeFormat: 'hh:mm tt',
			timeOnly: true   
		});
	});
</script>
<script type="text/javascript" src="<?=JS_COLOR_PATH?>/jscolor.js"></script>

<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/semester_conf";
			}
		})
	});
	
function confirm_delete_popup() {
		var len = $("input[id='semester_id_enc']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more semester(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/deleteSemester",
			data: {
				semester_id_enc: $("input[id='semester_id_enc']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Semester(s) deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}	
	function ajax_activate(semester_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/activateSemester",
			data: {
				semester_id_enc: semester_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Semester activated successfully.", 'green')

				$('#act_deact_'+semester_id_enc).html('<span style="cursor:pointer" onClick="ajax_deactivate(\''+semester_id_enc+'\')" onMouseOver="deactivate_me(this)" onMouseOut="reset_activate(this)" class="label label-success">Active</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_deactivate(semester_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/deactivateSemester",
			data: {
				semester_id_enc: semester_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Semester de-activated successfully.", 'green')
				
				$('#act_deact_'+semester_id_enc).html('<span style="cursor:pointer" onClick="ajax_activate(\''+semester_id_enc+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/is_exist_semester",
			data: {
			    tbl_semester_id: "",
				title: $('#title').val(),
				title_ar: "",
				start_date: $('#start_date').val(),
				end_date: $('#end_date').val(),
				is_ajax: true
			},
			success: function(data) {
				
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
					refresh_page = "N";
					my_alert("Semester already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_create();
				}
			
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist_edit() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/is_exist_semester",
			data: {
				tbl_semester_id: $('#semester_id_enc').val(), 
				title: $('#title').val(),
				title_ar: "",
				start_date: $('#start_date').val(),
				end_date: $('#end_date').val(),
				is_ajax: true
			},
			success: function(data) {
				
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='T') {
					refresh_page = "N";
					my_alert("Semester duration already exists.", 'red');
					$("#pre-loader").hide();
				}else if (temp=='Y') {
					refresh_page = "N";
					my_alert("Semester duration already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_save_changes();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_create() {
		 
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/create_semester",
			data: {
				tbl_semester_id: "<?=$semester_id_enc?>",
				title: $('#title').val(),
				title_ar: $('#title_ar').val(),
				tbl_academic_year_id: $('#tbl_academic_year_id').val(),
				start_date: $('#start_date').val(),
				end_date: $('#end_date').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp == 'Y') {
				refresh_page = "Y";
				    my_alert("Semester is added successfully.", 'green');
				    $("#pre-loader").hide();
				} else {
					refresh_page = "N";
					my_alert("Semester added failed, Please try again.", 'red');
					$("#pre-loader").hide();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_save_changes() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/save_semester_changes",
			data: {
				tbl_semester_id: $('#semester_id_enc').val(), 
			    title: $('#title').val(),
				title_ar: $('#title_ar').val(),
				tbl_academic_year_id: $('#tbl_academic_year_id').val(),
				start_date: $('#start_date').val(),
				end_date: $('#end_date').val(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Changes saved successfully.", 'green');
				
				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
</script>
<script language="javascript">
	function ajax_validate() {
		if (validate_semester_name() == false || validate_semester_name_ar() == false || validate_academic_year() == false || validate_start_date() == false || validate_end_date() == false ) {
			return false;
		} else {
			is_exist();
		}
	} 

	function ajax_validate_edit() {
		if ( validate_semester_name() == false || validate_semester_name_ar() == false || validate_academic_year() == false || validate_start_date() == false || validate_end_date() == false) {
			return false;
		} else {
			is_exist_edit();
		}
	} 

    

	function validate_semester_name() {
		var regExp = / /g;
		var str = $('#title').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Semester Title [En] is blank. Please enter Semester Title [En].");
		return false;
		}
	}

	function validate_semester_name_ar() {
		var regExp = / /g;
		var str = $('#title_ar').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Semester Title [Ar] is blank. Please enter Semester Title [Ar].");
		return false;
		}
	}
	
	 function validate_academic_year() {
		var regExp = / /g;
		var str = $('#tbl_academic_year_id').val();
		if (str.length <= 0) {
			my_alert("Academic Year is not selected. Please select Academic Year.");
		return false;
		}
	}
	
	
	 function validate_start_date() {
		var regExp = / /g;
		var str = $('#start_date').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Start Date is not selected. Please select Start Date.");
		return false;
		}
	}
	
	 function validate_end_date() {
		var regExp = / /g;
		var str = $('#end_date').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("End Date is not selected. Please select End Date.");
		return false;
		}
		
		
		var D1 = $('#start_date').val();
		var D2 = $('#end_date').val();
		
		D1 = new Date(D1).getTime();
		D2 = new Date(D2).getTime();
		alert(D1);
		alert(D2);
		
		if ((new Date(D2).getTime()) <= (new Date(D1).getTime())) {		
			my_alert("Start Date should be smaller than End Date.","red");
		return false;	
		}
		
	}

</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Semesters Configuration <small> Management</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Semesters</li>
    </ol>
     <div style=" float:right; padding-right:10px;"> <button onclick="show_academic_year()" title="Academic Years" type="button" class="btn btn-primary">Academic Years</button></div> 
    <!--/BREADCRUMB--> 
    <div style="clear:both"></div>
  </section>
  <script>
   function show_academic_year()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/academic_year_conf/";
		window.location.href = url;
	}
  </script>
  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {
			$tbl_sel_semester_id     = $class_semester_obj['tbl_semester_id'];
			$title                   = $class_semester_obj['title'];
			$title_ar                = $class_semester_obj['title_ar'];
			$tbl_academic_year_id    = $class_semester_obj['tbl_academic_year_id'];
			$start_date              = date("m/d/Y", strtotime($class_semester_obj['start_date']));
			$end_date                = date("m/d/Y", strtotime($class_semester_obj['end_date']));
	?>
        <!--Edit-->
        
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Semester</h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/semester_conf"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_edit" id="frm_listing" class="form-horizontal" method="post">
                    <div class="box-body">
                   
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="title">Semester Title [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Semester Title [En]" id="title" name="title" class="form-control" value="<?=$title?>">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="title_ar">Semester Title [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Semester Title [Ar]" id="title_ar" name="title_ar" class="form-control" dir="rtl" value="<?=$title_ar?>" >
                      </div>
                    </div>
                  
                    <div class="form-group">
                     <label class="col-sm-2 control-label" for="tbl_academic_year_id">Academic Year<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-10">
                                 <select name="tbl_academic_year_id" id="tbl_academic_year_id" class="form-control">
                                 <option value="">Select Year</option>
                              <?php
                                    for ($u=0; $u<count($rs_academic_year); $u++) { 
                                        $tbl_academic_year_id_u = $rs_academic_year[$u]['tbl_academic_year_id'];
                                        $academic_start = $rs_academic_year[$u]['academic_start'];
                                        $academic_end = $rs_academic_year[$u]['academic_end'];
										
                                        if($tbl_academic_year_id   == $tbl_academic_year_id_u)
                                           $selClass = "selected";
                                         else
                                           $selClass = "";
                                  ?>
                                      <option value="<?=$tbl_academic_year_id_u?>"  <?=$selClass?> >
                                      <?=$academic_start?>&nbsp;-&nbsp;<?=$academic_end?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>
                   </div>
                   </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="start_time">Start Date<span style="color:#F30; padding-left:2px;">*</span></label>
                      <div class="col-sm-10">
                        <input type="text" placeholder="Start Date" id="start_date" name="start_date" class="form-control"  value="<?=$start_date?>" >
                        
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="end_time">End Date<span style="color:#F30; padding-left:2px;">*</span></label>
                      <div class="col-sm-10">
                        <input type="text" placeholder="End Date" id="end_date" name="end_date" class="form-control"  value="<?=$end_date?>" >
                      </div>
                    </div>
                   
                    
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate_edit()">Save Changes</button>
                    <input type="hidden" name="semester_id_enc" id="semester_id_enc" value="<?=$tbl_sel_semester_id?>" />
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
		        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/classes/semester_conf";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
    
  
 <link href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" rel="stylesheet">
 <script src="http://code.jquery.com/jquery-1.11.1.js"></script>
 <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
  <script>
  $( function() {
		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
				
				/* var order = $("#tabledivbody").sortable("serialize");
				
				alert(order);
				
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/your/url/here'
				});*/
				
				
				
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	

				
				
				
				
				
			}
	  } );
  
  } );
  
  
  
  </script> 
                  <div id="mid1" class="box box-success">
                        <div class="box-header">
                          <div class="col-sm-1" >
                          <h3 class="box-title">SEARCH</h3>
                          </div>
                          <div class="col-sm-11"> 
                             
                               <div class="col-sm-6"><input name="q" id="q" value="<?=urldecode($q)?>" type="text" class="form-control" placeholder="Search By Title."   > </div>
                               <div class="col-sm-2"><button class="btn btn-success" type="button" onclick="search_data()">Search</button>&nbsp;<button class="btn btn-success" type="button" 
                               onclick="reset_data();">Reset</button>
                               </div>
                           
                          </div>
                        </div>  
                     </div>     
    
            <!--Listing-->
                    <div id="mid1_list" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Semesters</h3>
                          <div class="box-tools">
                            <?php if (count($rs_all_semesters)>0) { echo $paging_string;}?>	
                            <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
                            <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>
                          </div>
                        </div>
                        
                        <div class="box-body">
                     <!--   <div style="color:#030; font-weight:bold;">You can sort categories by using drag and drop of rows </div>-->
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle"><input id="select_all" type="checkbox" value="" /></th>
                              <!--<th width="10%" align="center" valign="middle">Sl No.</th>-->
                              <th width="15%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y">Semester Title [En] <?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?><div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>
                              <th width="15%" align="center" valign="middle">Semester Title [Ar]</th>
                              <th width="13%" align="center" valign="middle">Academic Year</th>
                              <th width="12%" align="center" valign="middle">Start Date</th>
                              <th width="9%" align="center" valign="middle">End Date</th>
                              <th width="7%" align="center" valign="middle">Date</th>
                              <th width="7%" align="center" valign="middle">Status</th>
                              <th width="7%" align="center" valign="middle">Action</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                                for ($i=0; $i<count($rs_all_semesters); $i++) { 
                                    $id                        = $rs_all_semesters[$i]['id'];
                                    $tbl_semester_id           = $rs_all_semesters[$i]['tbl_semester_id'];
                                    $title                     = $rs_all_semesters[$i]['title'];
                                    $title_ar                  = $rs_all_semesters[$i]['title_ar'];
									 
									$academic_year             = $rs_all_semesters[$i]['academic_start']." - ".$rs_all_semesters[$i]['academic_end'];
									$start_date                = $rs_all_semesters[$i]['start_date'];
                                    $end_date                  = $rs_all_semesters[$i]['end_date'];
									
                                    $added_date                = $rs_all_semesters[$i]['added_date'];
                                    $is_active                 = $rs_all_semesters[$i]['is_active'];
                                    
                                    $title       = ucfirst($title);
									$start_date                = date('m-d-Y',strtotime($start_date));
									$end_date                  = date('m-d-Y',strtotime($end_date));
                                    $added_date                = date('m-d-Y',strtotime($added_date));
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_semester_id?>" >
                              <td align="left" valign="middle">
                              <span style="float:left;">
                              <input id="semester_id_enc" name="semester_id_enc" class="checkbox" type="checkbox" value="<?=$tbl_semester_id?>" />
                              </span>
                              
                             <?php /*?> <span style="float:left;">&nbsp;
                              <?php if($i<>0){ ?> <i class="fa fa-arrow-up"  style="color:#3c8dbc; cursor:pointer;"  aria-hidden="true" title="Sorting - Drag & Drop To Up"></i> &nbsp; <?php } ?>
                               <?php if($i<> count($rs_all_categories)-1){ ?> <i class="fa fa-arrow-down" style="color:#3c8dbc;cursor:pointer;" aria-hidden="true" title="Sorting - Drag & Drop To Down"></i> <?php } ?>
                              </span><?php */?>
                              </td>
                             <!-- <td align="left" valign="middle"><?=$offset+$i+1?></td>-->
                              <td align="left" valign="middle"><?=$title?></td>
                              <td align="left" valign="middle"><?=$title_ar?></td>
                              <td align="left" valign="middle"><?=$academic_year?></td>
                              <td align="left" valign="middle"><?=$start_date?></td>
                              <td align="left" valign="middle"><?=$end_date?></td>
                              <td align="left" valign="middle"><?=$added_date?></td>
                              <td align="left" valign="middle">
                                <div id="act_deact_<?=$tbl_semester_id?>">
                                <?php if (trim($is_active) == "Y") { ?>
                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_semester_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                <?php } else { ?>
                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_semester_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                <?php } ?>
                                </div>
                              </td>
                              <td align="left" valign="middle">
                                <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/edit_semester/semester_id_enc/<?=$tbl_semester_id?>"><button class="btn bg-purple fa fa-pencil" type="button" title="Edit"></button></a>
                              </td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="9" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_all_semesters)<=0) {
                            ?>
                            <tr>
                              <td colspan="9" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no semesters available. Click on the + button to create one.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
    
            <!--Add or Create-->
              <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Semester</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                  
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="title">Semester Title [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Semester Title [En]" id="title" name="title" class="form-control">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="title_ar">Semester Title [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Semester Title [Ar]" id="title_ar" name="title_ar" class="form-control" dir="rtl">
                      </div>
                    </div>
                  
                  
                    <div class="form-group">
                     <label class="col-sm-2 control-label" for="tbl_academic_year_id">Academic Year<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-10">
                                 <select name="tbl_academic_year_id" id="tbl_academic_year_id" class="form-control">
                                 <option value="">Select Year</option>
                              <?php
                                    for ($u=0; $u<count($rs_academic_year); $u++) { 
                                        $tbl_academic_year_id_u = $rs_academic_year[$u]['tbl_academic_year_id'];
                                        $academic_start         = $rs_academic_year[$u]['academic_start'];
                                        $academic_end           = $rs_academic_year[$u]['academic_end'];
										
                                        if($tbl_sel_academic_year_id == $tbl_academic_year_id_u)
                                           $selClass = "selected";
                                         else
                                           $selClass = "";
                                  ?>
                                      <option value="<?=$tbl_academic_year_id_u?>"  <?=$selClass?> >
                                      <?=$academic_start?>&nbsp;-&nbsp;<?=$academic_end?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>
                   </div>
                   </div>
                   
                    
                      <div class="form-group">
                      <label class="col-sm-2 control-label" for="start_time">Start Date<span style="color:#F30; padding-left:2px;">*</span></label>
                      <div class="col-sm-10">
                        <input type="text" placeholder="Start Date" id="start_date" name="start_date" class="form-control" >
                        
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="end_time">End Date<span style="color:#F30; padding-left:2px;">*</span></label>
                      <div class="col-sm-10">
                        <input type="text" placeholder="End Date" id="end_date" name="end_date" class="form-control" >
                      </div>
                    </div>
                    
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate()">Submit</button>
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
            <!--/Add or Create-->
                
        <!--/Admin Category Management-->
        

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>

        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >
function search_data() {
		var q = $("#q").val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/semester_conf/";
		
		if(q !='')
			url += "q/"+q+"/";
		
			url += "offset/0/";
		window.location.href = url;
	}

function reset_data() {
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/semester_conf/";
		url += "offset/0/";
		window.location.href = url;
	}


</script>