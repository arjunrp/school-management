<?php include(ROOT_ADMIN_PATH."/admin/include/ministry_header.php");

?>
<!--WRAPPER-->
<div class="wrapper"> 
  
      <!--TOP BAR-->
      <?php include(ROOT_ADMIN_PATH."/admin/include/ministry_header_top_bar.php");?>
      <!--/TOP BAR--> 
      
      <!--LEFT-->
      <?php include(ROOT_ADMIN_PATH."/admin/include/ministry_left.php");?>
      <!--/LEFT--> 
      
      
      <!--INC FILE-->
      <?php
		//echo $page;
		//exit();
      	switch($page) {
			case("login_form"): {
				include(ROOT_ADMIN_PATH."/admin/login_form.php");
			break;	
			}	
			case("welcome_ministry"): {
				include(ROOT_ADMIN_PATH."/admin/welcome_ministry.php");
			break;	
			}	
			case("view_ministry_admin_user"): {
				include(ROOT_ADMIN_PATH."/admin/view_ministry_admin_user.php");
			break;	
			}
			case("view_ministry_user_rights"): {
				include(ROOT_ADMIN_PATH."/admin/view_ministry_user_rights.php");
			break;	
			}
			case("view_ministry_message_to_teachers"): {
				include(ROOT_ADMIN_PATH."/admin/view_ministry_message_to_teachers.php");
			break;	
			}
			case("view_ministry_card"): {
				include(ROOT_ADMIN_PATH."/admin/view_ministry_card.php");
			break;	
			}
			case("view_ministry_parenting_category"): {
				include(ROOT_ADMIN_PATH."/admin/view_ministry_parenting_category.php");
			break;	
			}
			case("view_ministry_parenting"): {
				include(ROOT_ADMIN_PATH."/admin/view_ministry_parenting.php");
			break;	
			}
			case("view_student_points_conf_ministry"): {
				include(ROOT_ADMIN_PATH."/admin/view_student_points_conf_ministry.php");
			break;	
			}
		}
	  ?>
      <!--/INC FILE--> 
      
      
      <!--FOOTER COPYRIGHT-->
      <?php include(ROOT_ADMIN_PATH."/admin/include/footer_copyright.php");?>
      <!--/FOOTER COPYRIGHT--> 
      
      <!--MENU RIGHT-->
      <?php include(ROOT_ADMIN_PATH."/admin/include/menu_slide_right.php");?>
      <!--/MENU RIGHT--> 
  
</div>
<!--/WRAPPER-->
<?php include(ROOT_ADMIN_PATH."/admin/include/footer.php");?>