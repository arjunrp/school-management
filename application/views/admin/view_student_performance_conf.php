<?php
//Init Parameters
$performance_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}
?>
 

<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/student_performance_conf";
			}
		})
	});
	
function confirm_delete_popup() {
		var len = $("input[id='performance_id_enc']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more topic(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/deleteStudentTopic",
			data: {
				performance_id_enc: $("input[id='performance_id_enc']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Student topic(s) deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}	
	function ajax_activate(performance_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/activateStudentTopic",
			data: {
				performance_id_enc: performance_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Student topic is activated successfully.", 'green')

				$('#act_deact_'+performance_id_enc).html('<span style="cursor:pointer" onClick="ajax_deactivate(\''+performance_id_enc+'\')" onMouseOver="deactivate_me(this)" onMouseOut="reset_activate(this)" class="label label-success">Active</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_deactivate(performance_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/deactivateStudentTopic",
			data: {
				performance_id_enc: performance_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Student topic is de-activated successfully.", 'green')
				
				$('#act_deact_'+performance_id_enc).html('<span style="cursor:pointer" onClick="ajax_activate(\''+performance_id_enc+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/is_exist_student_topic",
			data: {
				performance_id_enc: "<?=$performance_id_enc?>",
				topic_name_en: $('#topic_name_en').val(),
				topic_name_ar: "",
				is_ajax: true
			},
			success: function(data) {
				
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
					refresh_page = "N";
					my_alert("Student topic is already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_create();
				}
			
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist_edit() {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/is_exist_student_topic",
			data: {
				performance_id_enc:$('#performance_id_enc').val(),
				topic_name_en: $('#topic_name_en').val(),
				topic_name_ar: "",
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
					refresh_page = "N";
					my_alert("Student topic is already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_save_changes();
				}
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_create() {
	    var selectedCategory = $('#tbl_performance_category_id').val();
		
		var selectednumbers = "";
        $('.checkboxClass:checked').each(function(i){
          selectednumbers += $(this).val()+"&";
        });	
	
		
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/add_student_topic",
			data: {
				performance_id_enc: "<?=$performance_id_enc?>",
				topic_name_en: $('#topic_name_en').val(),
				topic_name_ar: $('#topic_name_ar').val(),
				tbl_class_id:selectednumbers,
				tbl_performance_category_id:selectedCategory,
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
				refresh_page = "Y";
				    my_alert("Student topic is added successfully.", 'green');
				    $("#pre-loader").hide();
				}else{
					refresh_page = "N";
					my_alert("Student topic is added failed, Please try again.", 'red');
					$("#pre-loader").hide();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_save_changes() {
		
	    var selectedCategory = $('#tbl_performance_category_id').val();
		
		var selectednumbers = "";
        $('.checkboxClass:checked').each(function(i){
          selectednumbers += $(this).val()+"&";
        });		
		
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/save_student_topic_changes",
			data: {
				performance_id_enc:$('#performance_id_enc').val(),
				topic_name_en: $('#topic_name_en').val(),
				topic_name_ar: $('#topic_name_ar').val(),
				tbl_class_id:selectednumbers,
				tbl_performance_category_id:selectedCategory,
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
				refresh_page = "Y";
				    my_alert("Student topic is updated successfully.", 'green');
				    $("#pre-loader").hide();
				}else{
					refresh_page = "N";
					my_alert("Student topic updation failed, Please try again.", 'red');
					$("#pre-loader").hide();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
</script>
<script language="javascript">
	function ajax_validate() {
		if (validate_category() == false || validate_topic_name() == false || validate_topic_ar() == false ) {
			return false;
		} else {
		   is_exist();
		}
	} 

	function ajax_validate_edit() {
		if (validate_category() == false || validate_topic_name() == false || validate_topic_ar() == false ) {
			return false;
		} else {
			is_exist_edit();
		}
	} 


	function validate_topic_name() {
		var regExp = / /g;
		var str = $('#topic_name_en').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Topic name [En] is blank. Please enter topic name [En].");
		return false;
		}
	}

	function validate_topic_ar() {
		var regExp = / /g;
		var str = $('#topic_name_ar').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Topic name [Ar] is blank. Please enter topic name [Ar].");
		return false;
		}
	}
	
	function validate_category() {
		var selectedCategory = $('#tbl_performance_category_id').val();
		if (selectedCategory=='') {
			my_alert("Category is not selected. Please select category.");
		   return false;
		}
		return true;
	}
	
	

</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Students Performance <small> Management</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Students Performance Configuration</li>
    </ol>
    <!--/BREADCRUMB--> 
<?php /*?>     <div style=" float:right; padding-right:10px;"> <button onclick="show_card_points()" title="Card Points" type="button" class="btn btn-primary">Cards Point Settings</button></div> 
<?php */?>   
 <div style="clear:both"></div>
  </section>
  
  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {

			$tbl_performance_id            		= $student_topic_obj[0]['tbl_performance_id'];
			$topic_name_en           			= $student_topic_obj[0]['topic_en'];
			$topic_name_ar           			= $student_topic_obj[0]['topic_ar'];
			$tbl_sel_performance_category_id    = $student_topic_obj[0]['tbl_performance_category_id'];
			$is_active                  		= $student_topic_obj[0]['is_active'];
	?>
        <!--Edit-->
        
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Student Topic</h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/student_performance_conf"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_edit" id="frm_listing" class="form-horizontal" method="post">
                 <div class="box-body">
                  
                     <div class="form-group">
                     <label class="col-sm-2 control-label" for="tbl_performance_category_id">Category<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-10">
                                 <select name="tbl_performance_category_id" id="tbl_performance_category_id" class="form-control">
                                 <option value="">Select Category</option>
                              <?php
                                    for ($u=0; $u<count($topic_categories); $u++) { 
                                        $tbl_performance_category_id_u   = $topic_categories[$u]['tbl_performance_category_id'];
                                        $performance_category_en         = $topic_categories[$u]['performance_category_en'];
                                        $performance_category_ar         = $topic_categories[$u]['performance_category_ar'];
										
                                        if($tbl_sel_performance_category_id == $tbl_performance_category_id_u)
                                           $selClass = "selected";
                                         else
                                           $selClass = "";
                                  ?>
                                      <option value="<?=$tbl_performance_category_id_u?>"  <?=$selClass?> >
                                      <?=$performance_category_en?>&nbsp;-&nbsp;<?=$performance_category_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>
                   </div>
                   </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="topic_name_en">Topic [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Topic Name [En]" id="topic_name_en" name="topic_name_en" class="form-control" value="<?=$topic_name_en?>">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="topic_name_ar">Topic [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Topic Name [Ar]" id="topic_name_ar" name="topic_name_ar" class="form-control" dir="rtl" value="<?=$topic_name_ar?>">
                      </div>
                    </div>
                    
                      <div class="form-group" id="all_student">
                        <label class="col-sm-2 control-label" for="tbl_class_id">Class(s)<!--<span style="color:#F30; padding-left:2px;">*</span>--></label>
        
                          <div class="col-sm-10" id="divClass" style="height:200px; overflow-y:scroll;">
                         
                        <?php  if(count($classes_list)>0){ ?>
                           <div style="padding-bottom:10px;background-color:#e8eaeb;" class="col-sm-12"> 
                           <input type="checkbox" value="" id="select_all_class" class="checkboxClass" >&nbsp;Select All</div>
                        <?php  } ?>
                        <?php
						   
                            for ($u=0; $u<count($classes_list); $u++) { 
								$tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
								$class_name             = $classes_list[$u]['class_name'];
								$class_name_ar          = $classes_list[$u]['class_name_ar'];
								$section_name           = $classes_list[$u]['section_name'];
								$section_name_ar        = $classes_list[$u]['section_name_ar'];
							
						    
								if(in_array($tbl_class_id_u, $class_array, true))
								{
                                    $selClass = "checked";
								}
                                else
								{
                                    $selClass = "";
								}
								
                             ?>
                             <div class="col-sm-4" style="padding-top:10px; padding-bottom:10px; border:1px solid #CCC;"> 
								  <input id="tbl_class_id_<?=$u?>" name="tbl_class_id[]" class="checkboxClass" type="checkbox" value="<?=$tbl_class_id_u?>"   <?=$selClass?> />&nbsp;
								   <?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;<?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                             </div>
							<?php  
	                         }
							 ?>
	                        </div> 
                    </div>    
               
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate_edit()">Save Changes</button>
                    <input type="hidden" name="performance_id_enc" id="performance_id_enc" value="<?=$tbl_performance_id?>" />
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
		        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/student/student_performance_conf";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
    
  
 <link href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" rel="stylesheet">
 <script src="http://code.jquery.com/jquery-1.11.1.js"></script>
 <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
  <script>
  $( function() {
		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
				
				/* var order = $("#tabledivbody").sortable("serialize");
				
				alert(order);
				
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/your/url/here'
				});*/
				
				
				
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	
				
				
				
				
				
			}
	  } );
  
  } );
  
   function show_card_points()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/student_cards_points_conf/";
		window.location.href = url;
	}
	
  </script> 
 
    
                    <div id="mid1" class="box box-success">
                        <div class="box-header">
                          <div class="col-sm-1" >
                          <h3 class="box-title">SEARCH</h3>
                          </div>
                          <div class="col-sm-11"> 
                             
                               <div class="col-sm-6"><input name="q" id="q" value="<?=urldecode($q)?>" type="text" class="form-control" placeholder="Search By topic, category."   > </div>
                               <div class="col-sm-2"><button class="btn btn-success" type="button" onclick="search_data()">Search</button>&nbsp;<button class="btn btn-success" type="button" 
                               onclick="reset_data();">Reset</button>
                               </div>
                           
                          </div>
                        </div>  
                     </div>     
    
            <!--Listing-->
                    <div id="mid1_list" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Student Performance Configuration</h3>
                          <div class="box-tools">
                            <?php if (count($rs_student_topics)>0) { echo $paging_string;}
							
						
							?>	
                            <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
                            <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>
                          </div>
                        </div>
                        
                        <div class="box-body">
                     <!--   <div style="color:#030; font-weight:bold;">You can sort categories by using drag and drop of rows </div>-->
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle"><input id="select_all" type="checkbox" value="" /></th>
                              <!--<th width="10%" align="center" valign="middle">Sl No.</th>-->
                              <th width="25%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y">Topic [En] <?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?>
	                              <div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>
                              <th width="25%" align="center" valign="middle">Topic [Ar]</th>
                              <th width="15%" align="center" valign="middle"><a href="<?=$sort_url?>/sort_name/B/sort_by/<?=$sort_by?>/sort_by_click/Y">Category<?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "B" && $sort_by == "ASC") { ?>
	                              <div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a></th>
                              <th width="10%" align="center" valign="middle">Status</th>
                              <th width="10%" align="center" valign="middle">Action</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                                for ($i=0; $i<count($rs_student_topics); $i++) { 
                                    $id                    		= $rs_student_topics[$i]['id'];
                                    $tbl_performance_id    		= $rs_student_topics[$i]['tbl_performance_id'];
                                    $topic_name_en         		= $rs_student_topics[$i]['topic_en'];
                                    $topic_name_ar         		= $rs_student_topics[$i]['topic_ar'];
									$performance_category_en    = $rs_student_topics[$i]['performance_category_en'];
									$performance_category_ar    = $rs_student_topics[$i]['performance_category_ar'];
                                    $is_active                  = $rs_student_topics[$i]['is_active'];
                                    $topic_name_en              = ucfirst($topic_name_en);
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_performance_id?>" >
                              <td align="left" valign="middle">
                              <span style="float:left;">
                              <input id="performance_id_enc" name="performance_id_enc" class="checkbox" type="checkbox" value="<?=$tbl_performance_id?>" />
                              </span>
                              
                             <?php /*?> <span style="float:left;">&nbsp;
                              <?php if($i<>0){ ?> <i class="fa fa-arrow-up"  style="color:#3c8dbc; cursor:pointer;"  aria-hidden="true" title="Sorting - Drag & Drop To Up"></i> &nbsp; <?php } ?>
                               <?php if($i<> count($rs_all_categories)-1){ ?> <i class="fa fa-arrow-down" style="color:#3c8dbc;cursor:pointer;" aria-hidden="true" title="Sorting - Drag & Drop To Down"></i> <?php } ?>
                              </span><?php */?>
                              </td>
                             <!-- <td align="left" valign="middle"><?=$offset+$i+1?></td>-->
                              <td align="left" valign="middle"><?=$topic_name_en?></td>
                              <td align="left" valign="middle"><?=$topic_name_ar?></td>
                              <td align="left" valign="middle"><?=$performance_category_en?><span style="float:right;"><?=$performance_category_ar?></span></td>
                              <td align="left" valign="middle">
                                <div id="act_deact_<?=$tbl_performance_id?>">
                                <?php if (trim($is_active) == "Y") { ?>
                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_performance_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                <?php } else { ?>
                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_performance_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                <?php } ?>
                                </div>
                              </td>
                              <td align="left" valign="middle">
                                <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/edit_student_topic/performance_id_enc/<?=$tbl_performance_id?>"><button class="btn bg-purple fa fa-pencil" type="button" title="Edit"></button></a>
                              </td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="8" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_student_topics)<=0) {
                            ?>
                            <tr>
                              <td colspan="8" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no topic available. Click on the + button to create one.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
    
            <!--Add or Create-->
              <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Student Topic / Activity</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                  
                     <div class="form-group">
                     <label class="col-sm-2 control-label" for="tbl_performance_category_id">Category<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-10">
                                 <select name="tbl_performance_category_id" id="tbl_performance_category_id" class="form-control">
                                 <option value="">Select Category</option>
                              <?php
                                    for ($u=0; $u<count($topic_categories); $u++) { 
                                        $tbl_performance_category_id_u   = $topic_categories[$u]['tbl_performance_category_id'];
                                        $performance_category_en         = $topic_categories[$u]['performance_category_en'];
                                        $performance_category_ar         = $topic_categories[$u]['performance_category_ar'];
										
                                        if($tbl_sel_performance_category_id == $tbl_performance_category_id_u)
                                           $selClass = "selected";
                                         else
                                           $selClass = "";
                                  ?>
                                      <option value="<?=$tbl_performance_category_id_u?>"  <?=$selClass?> >
                                      <?=$performance_category_en?>&nbsp;-&nbsp;<?=$performance_category_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>
                   </div>
                   </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="topic_name_en">Topic Name [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Topic Name [En]" id="topic_name_en" name="topic_name_en" class="form-control">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="topic_name_ar">Topic Name [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Topic Name [Ar]" id="topic_name_ar" name="topic_name_ar" class="form-control" dir="rtl">
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-group" id="all_student">
                        <label class="col-sm-2 control-label" for="tbl_class_id">Class(s)<!--<span style="color:#F30; padding-left:2px;">*</span>--></label>
        
                          <div class="col-sm-10" id="divClass" style="height:200px; overflow-y:scroll;">
                         
                        <?php  if(count($classes_list)>0){ ?>
                           <div style="padding-bottom:10px;background-color:#e8eaeb;" class="col-sm-12"> 
                           <input type="checkbox" value="" id="select_all_class" class="checkboxClass" >&nbsp;Select All</div>
                        <?php  } ?>
                        <?php
                            for ($u=0; $u<count($classes_list); $u++) { 
								$tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
								$class_name             = $classes_list[$u]['class_name'];
								$class_name_ar          = $classes_list[$u]['class_name_ar'];
								$section_name           = $classes_list[$u]['section_name'];
								$section_name_ar        = $classes_list[$u]['section_name_ar'];
								if(in_array($tbl_class_id_u, $class_array, true))
                                    $selClass = "checked";
                                else
                                    $selClass = "";
								
                             ?>
                             <div class="col-sm-4" style="padding-top:10px; padding-bottom:10px; border:1px solid #CCC;"> 
								  <input id="tbl_class_id_<?=$u?>" name="tbl_class_id[]" class="checkboxClass" type="checkbox" value="<?=$tbl_class_id_u?>"   <?=$selClass?> />&nbsp;
								   <?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;<?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                             </div>
							<?php  
	                         }
							 ?>
	                        </div> 
                    </div>    
                  
                  
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate()">Submit</button>
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
            <!--/Add or Create-->
                
        <!--/Admin Category Management-->

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>

        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >
 $('#select_all_class').on('click',function(){
	if(this.checked){
		$('.checkboxClass').each(function(){
			this.checked = true;
		});
		
	}else{
		 $('.checkboxClass').each(function(){
			this.checked = false;
		});
	}
 });

function search_data() {
		var q = $("#q").val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/student_performance_conf/";
		
		if(q !='')
			url += "q/"+q+"/";
		
			url += "offset/0/";
		window.location.href = url;
	}

function reset_data() {
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/student_performance_conf/";
		url += "offset/0/";
		window.location.href = url;
	}


</script>