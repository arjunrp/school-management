<?php

	$CI =& get_instance();
	$CI->load->model("model_student");
	for ($i=0; $i<count($data_rs); $i++) {
		$tbl_student_id = $data_rs[$i]['tbl_student_id'];
		if ($module_id == "m_give_points") {
			$total_points = $CI->model_student->get_total_points($tbl_student_id);
		}
		if (trim($lan) == "ar") {
			$name = $data_rs[$i]['first_name_ar']." ".$data_rs[$i]['last_name_ar'];
		} else {
			$name = $data_rs[$i]['first_name']." ".$data_rs[$i]['last_name'];
		}
		$file_name_updated = $data_rs[$i]['file_name_updated'];
		
		$arr_students[$i]["student_id"] = $tbl_student_id;
		$arr_students[$i]["title"] = $name;
	
		if($file_name_updated==""){
			$arr_students[$i]["url"] = IMG_PATH_STUDENT."/avatar_default.png";
		}else{
			$arr_students[$i]["url"] = IMG_PATH_STUDENT."/".$file_name_updated;
		}
		if ($module_id == "m_give_points") {
			$arr_students[$i]["points"] = $total_points;
		}
		
		if ($module_id == "m_give_points") {
	?>		
            <tr>
              <td align="left" valign="middle"><img src="<?=$arr_students[$i]["url"]?>" height="100" /></td>
              <td align="left" valign="middle">
                <a href="#" onclick="get_behaviour_points('<?=$tbl_student_id?>', '<?=$name?>')">
                <?=$name?>
                </a>
              </td>
              <td align="center" valign="middle"><?=$total_points?></td>
            </tr>
<?php		
		} else if ($module_id == "m_contact_parent" && $contact_parents == "Y") {
?>		
            <tr>
              <td align="left" valign="middle"><img src="<?=$arr_students[$i]["url"]?>" height="100" /></td>
              <td align="left" valign="middle" style="position:relative;">
                <a href="#" onclick="popup_option('<?=$tbl_student_id?>', '<?=$name?>', '<?=$i?>')">
                <?=$name?>
                </a>
                <div id="popupid<?=$i?>" style="position:absolute; width:auto; height:auto; left:0px; top:30px; background-color:#FFFFFF; border:1px solid #CCC; padding:10px; display:none;">
                	<div style="cursor:pointer" onclick="get_all_conversation('<?=$tbl_student_id?>', '<?=$name?>', '<?=$i?>')">All Conversation</div>
                    <div style="cursor:pointer" onclick="get_all_private_msg('<?=$tbl_student_id?>', '<?=$name?>', '<?=$i?>')">Private Message</div>
                    <div onclick="popup_option('<?=$tbl_student_id?>', '<?=$name?>', '<?=$i?>')" style="cursor:pointer">Cancel</div>
                </div>
              </td>
              <td align="left" valign="middle"><input type="checkbox" name="option" id="option<?=$i?>" value="<?=$tbl_student_id?>"  /></td>
            </tr>
            
            
            
            
            
            
<?php		
		} else if ($module_id == "m_contact_parent" && $issue_cards == "Y") {
?>		
            <tr>
              <td align="left" valign="middle"><img src="<?=$arr_students[$i]["url"]?>" height="100" /></td>
              <td align="left" valign="middle">
                <a href="#" onclick="get_issue_cards_form('<?=$tbl_student_id?>', '<?=$name?>')">
                <?=$name?>
                </a>
              </td>
            </tr>
 <?php		
		} else if ($module_id == "m_contact_parent" && $performance_calc == "Y") {
?>		
            <tr>
              <td align="left" valign="middle"><img src="<?=$arr_students[$i]["url"]?>" height="100" /></td>
              <td align="left" valign="middle">
                <a href="#" onclick="get_performance_monitor_form('<?=$tbl_student_id?>', '<?=$name?>')">
                <?=$name?>
                </a>
              </td>
            </tr>
<?php		
		} else if ($module_id == "m_contact_parent" && $contact_parents != "Y") {
?>		
            <tr>
              <td align="left" valign="middle"><img src="<?=$arr_students[$i]["url"]?>" height="100" /></td>
              <td align="left" valign="middle">
                <a href="#" onclick="get_behaviour_points('<?=$tbl_student_id?>', '<?=$name?>')">
                <?=$name?>
                </a>
              </td>
            </tr>
<?php		
		} }	
?>
		<input type="hidden" name="total_count" id="total_count" value="<?=count($data_rs)?>" />











