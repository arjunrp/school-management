<?php
//Init Parameters
$student_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}
?>
 
<style>
.txt_en {
	text-align:left;
	padding-left:2px;
}
.txt_ar {
	text-align:right;
	padding-right:2px;	
	direction:rtl;		
}
</style>
<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/all_students";
			}
		})
	});
	
function confirm_delete_popup() {
		var len = $("input[id='student_id_enc']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more student(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/deleteStudent",
			data: {
				student_id_enc: $("input[id='student_id_enc']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Student(s) deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}	
	function ajax_activate(student_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/activateStudent",
			data: {
				student_id_enc: student_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Student activated successfully.", 'green')

				$('#act_deact_'+student_id_enc).html('<span style="cursor:pointer" onClick="ajax_deactivate(\''+student_id_enc+'\')" onMouseOver="deactivate_me(this)" onMouseOut="reset_activate(this)" class="label label-success">Active</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_deactivate(student_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/deactivateStudent",
			data: {
				student_id_enc: student_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Student de-activated successfully.", 'green')
				
				$('#act_deact_'+student_id_enc).html('<span style="cursor:pointer" onClick="ajax_activate(\''+student_id_enc+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/is_exist_student",
			data: {
				student_id_enc: "<?=$student_id_enc?>",
				first_name: $('#first_name').val(),
				last_name: $('#last_name').val(),
				tbl_class_id: $('#tbl_class_id').val(),
				is_ajax: true
			},
			success: function(data) {
				
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
					refresh_page = "N";
					my_alert("Student is already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_create();
				}
			
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist_edit() {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/is_exist_student",
			data: {
				student_id_enc: "<?=$student_id_enc?>",
				first_name: $('#first_name').val(),
				last_name: $('#last_name').val(),
				tbl_class_id: $('#tbl_class_id').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String(data);
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
					refresh_page = "N";
					my_alert("Student is already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_save_changes();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_create() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/add_student",
			data: {
				student_id_enc: "<?=$student_id_enc?>",
				first_name: $('#first_name').val(),
				last_name: $('#last_name').val(),
				tbl_class_id: $('#tbl_class_id').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
				refresh_page = "Y";
				    my_alert("School Type created successfully.", 'green');
				    $("#pre-loader").hide();
				}else{
					refresh_page = "N";
					my_alert("School type added failed, Please try again.", 'red');
					$("#pre-loader").hide();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_save_changes() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/save_type_changes",
			data: {
				school_type_id_enc:$('#school_type_id_enc').val(),
				school_type: $('#school_type').val(),
				school_type_ar: $('#school_type_ar').val(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Changes saved successfully.", 'green');
				
				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
</script>
<script language="javascript">
	function ajax_validate() {
		if (validate_school_type() == false || validate_school_type_ar() == false ) {
			return false;
		} else {
			is_exist();
		}
	} 

	function ajax_validate_edit() {
		if ( validate_school_type() == false || validate_school_type_ar() == false ) {
			return false;
		} else {
			is_exist_edit();
		}
	} 


	function validate_school_type() {
		var regExp = / /g;
		var str = $('#school_type').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Class Type [En] is blank. Please enter Class Type[En].");
		return false;
		}
	}

	function validate_school_type_ar() {
		var regExp = / /g;
		var str = $('#school_type_ar').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Class Type[Ar] is blank. Please enter Class Type[Ar].");
		return false;
		}
	}

</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Students <small> Management</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Students</li>
    </ol>
    <!--/BREADCRUMB--> 
    <div style="clear:both"></div>
  </section>
  
  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {

			$tbl_school_type_id         = $school_type_obj['tbl_school_type_id'];
			$school_type                = $school_type_obj['school_type'];
			$school_type_ar             = $school_type_obj['school_type_ar'];
			$added_date                 = $school_type_obj['added_date'];
			$is_active                  = $school_type_obj['is_active'];
	?>
        <!--Edit-->
        
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Category</h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/school_types"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_edit" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                  
                   
                  
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="school_type">Class Type [En]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Class Type [En]" id="school_type" name="school_type" class="form-control" value="<?=$school_type?>">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="school_type_ar">Class Type [Ar]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Class Type [Ar]" id="school_type_ar" name="school_type_ar" class="form-control" value="<?=$school_type_ar?>" dir="rtl">
                      </div>
                    </div>
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate_edit()">Save Changes</button>
                    <input type="hidden" name="school_type_id_enc" id="school_type_id_enc" value="<?=$tbl_school_type_id?>" />
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
		        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/student/all_students";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
    
  
 <link href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" rel="stylesheet">
 <script src="http://code.jquery.com/jquery-1.11.1.js"></script>
 <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
  <script>
  $( function() {
		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
				
				/* var order = $("#tabledivbody").sortable("serialize");
				
				alert(order);
				
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/your/url/here'
				});*/
				
				
				
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	
				
				
				
				
				
			}
	  } );
  
  } );
  </script> 
  
  
  
  <!--File Upload START-->
<link href="http://hayageek.github.io/jQuery-Upload-File/uploadfile.min.css" rel="stylesheet">

<style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url('<?=IMG_PATH?>/delete.jpg');
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
		clear:both;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
	#overlay_container {
		position:relative;
	}
	#overloading {
		background-image:url('<?=IMG_PATH?>/preloader/preloader_2.gif');
		background-repeat:no-repeat;
		background-position:center;
		background-color:#CCC;
		position:absolute;
		left:0px;
		top:0px;
		opacity: 0.3;
		z-index: 10000;
	}
	#div_listing_container {
		display:none;	
	}
	.d_d_text {
		color:#745156;
		font-size:20px;
			
	}
	.ajax-upload-dragdrop {
		margin:auto;
		margin-bottom:10px;
		width:700px !important;
	}
	.ajax-file-upload-statusbar {
		margin:auto;
		margin-top:10px;
	}
	.ajax-file-upload {
		height:31px;
	}
</style>
 
<script src="http://hayageek.github.io/jQuery-Upload-File/jquery.uploadfile.min.js"></script>

<script language="javascript">

var item_id = "<?=$tbl_admin_user_id?>";//Primary Key for a Form. 

function set_item_id(obj) {
	item_id = obj.value;
	get_files();	
}

$(document).ready(function() {
	var uploadObj = $("#advancedUpload").uploadFile({
		url:"<?=HOST_URL?>/file_mgmt/upload_the_file",
		multiple:true,
		autoSubmit:true,
		maxFileSize:130000,
		fileName:"myfile",
		formData: {"module_name":"student"},
		dynamicFormData: function() {
			var data = { item_id:item_id}
			return data;
		},
		showStatusAfterSuccess:false,
		dragDropStr: "<span class='d_d_text'>Optionally Drag and Drop the File to Upload.</span>",
		abortStr:"Abourt",
		cancelStr:"Cancel",
		doneStr:"Done",
		multiDragErrorStr: "Multi Drag Error.",
		extErrorStr:"Extention Error:",
		sizeErrorStr:"Max Size Error:",
		uploadErrorStr:"Upload Error",
		onSelect:function(files) {
 		},
		onSubmit:function(files) {
 		},
		onSuccess:function(files, data, xhr) {
			if (data == "error") {
				alert("Error uploading file. Please try again.");
				return;
			}
			var obj = JSON.parse(data);
			var tbl_uploads_id = obj.tbl_uploads_id;
			var file_name_updated = obj.file_name_updated;
			
			//alert("tbl_uploads_id: "+tbl_uploads_id)
			//alert("file_name_updated: "+file_name_updated)
			add_uploaded_item(tbl_uploads_id, file_name_updated);
		},
		afterUploadAll:function() {
 		},
		onError: function(files, status, errMsg) {
 		}
	});

	$("#startUpload").click(function() {
		uploadObj.startUpload();
	});
	
	try { 
		$('input[type=file]').click();
	} catch(e) {
		alert(e)
	}
});

//Function called when file is uploaded
function add_uploaded_item(tbl_uploads_id, file_name_updated) {
	var str = "<div id='"+tbl_uploads_id+"' class='box-header with-border'> <div class='box-title'><img src='<?=IMG_PATH_STUDENT?>/"+file_name_updated+"' /></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_img_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
		
	$("#div_listing_container").show();
	$("#div_listing_container").append(str);
	$(".ajax-upload-dragdrop").hide();//Hide the upload button
return;
}

function confirm_delete_img_popup(tbl_uploads_id) {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		$('#'+tbl_uploads_id).hide();	
		$(".ajax-upload-dragdrop").show();
		
		var url_str = "<?=HOST_URL?>/file_mgmt/delete_file";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					tbl_uploads_id: tbl_uploads_id
				},
			success: function(data) {
				$("#pre-loader").hide();
			}
		});	
	} else {
		$("#pre-loader").hide();		
	}
}

function get_files() {
	var url_str = "<?=HOST_URL?>/misc/get_files.php";
	
	$.ajax({
		type: "POST",
		url: url_str,
		data: {
				module_name: "admin_user",
				show_del: "Y",
				item_id: item_id//global variable
			},
		success: function(data) {
			$('#div_listing_container').show();
			$('#div_listing_container').html(data)
			
		}
	});	
}
</script>
<!--File Upload END-->
  
    
                    <div id="mid1" class="box box-success">
                        <div class="box-header">
                          <div style="width:20%; float:left;">
                          <h3 class="box-title">SEARCH</h3>
                          </div>
                          <div style="width:60%; float:left;"> 
                    	     <div class="col-sm-5">
                             <?php //echo $category_parent_list ?>
                   		      </div>
                             
                          </div>
                        </div>  
                     </div>     
    
            <!--Listing-->
                    <div id="mid1_list" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Students</h3>
                          <div class="box-tools">
                            <?php if (count($rs_all_students)>0) { echo $paging_string;}?>	
                            <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
                            <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>
                          </div>
                        </div>
                        
                        <div class="box-body">
                     <!--   <div style="color:#030; font-weight:bold;">You can sort categories by using drag and drop of rows </div>-->
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle"><input id="select_all" type="checkbox" value="" /></th>
                              <!--<th width="10%" align="center" valign="middle">Sl No.</th>-->
                              <th width="25%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y">Name <?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?><div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>
                              <th width="20%" align="center" valign="middle">Class</th>
                              <th width="5%" align="center" valign="middle">Gender</th>
                              <th width="10%" align="center" valign="middle">Mobile</th>
                              <th width="15%" align="center" valign="middle">Email</th>
                              <th width="10%" align="center" valign="middle">Date</th>
                              <th width="5%" align="center" valign="middle">Status</th>
                              <th width="5%" align="center" valign="middle">Action</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                                for ($i=0; $i<count($rs_all_students); $i++) { 
                                    $id = $rs_all_students[$i]['id'];
                                    $tbl_student_id     = $rs_all_students[$i]['tbl_student_id'];
                                    $name_en            = ucfirst($rs_all_students[$i]['first_name'])." ".ucfirst($rs_all_students[$i]['last_name']);
									$name_ar            = $rs_all_students[$i]['first_name_ar']." ".$rs_all_students[$i]['last_name_ar'];
                                    $mobile             = $rs_all_students[$i]['mobile'];
									$email              = $rs_all_students[$i]['email'];
									$gender             = ucfirst($rs_all_students[$i]['gender']);
                                    $added_date         = $rs_all_students[$i]['added_date'];
                                    $is_active          = $rs_all_students[$i]['is_active'];
									$class_name         = $rs_all_students[$i]['class_name'];
                                    $class_name_ar      = $rs_all_students[$i]['class_name_ar'];
									$section_name       = $rs_all_students[$i]['section_name'];
									$section_name_ar    = $rs_all_students[$i]['section_name_ar'];
                                    $school_type        = $rs_all_students[$i]['school_type'];
									$school_type_ar     = $rs_all_students[$i]['school_type_ar'];
                                    
                                    
                                    $added_date = date('m-d-Y',strtotime($added_date));
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_student_id?>" >
                              <td align="left" valign="middle">
                              <span style="float:left;">
                              <input id="student_id_enc" name="student_id_enc" class="checkbox" type="checkbox" value="<?=$tbl_student_id?>" />
                              </span>
                              
                             <?php /*?> <span style="float:left;">&nbsp;
                              <?php if($i<>0){ ?> <i class="fa fa-arrow-up"  style="color:#3c8dbc; cursor:pointer;"  aria-hidden="true" title="Sorting - Drag & Drop To Up"></i> &nbsp; <?php } ?>
                               <?php if($i<> count($rs_all_categories)-1){ ?> <i class="fa fa-arrow-down" style="color:#3c8dbc;cursor:pointer;" aria-hidden="true" title="Sorting - Drag & Drop To Down"></i> <?php } ?>
                              </span><?php */?>
                              </td>
                             <!-- <td align="left" valign="middle"><?=$offset+$i+1?></td>-->
                              <td align="left" valign="middle">
                              <div class="txt_en"><?=$name_en?></div>
                              <div class="txt_ar"><?=$name_ar?></div></td>
                              <td align="left" valign="middle"> 
                              <div class="txt_en"><?=$class_name?>&nbsp;<?=$section_name?></div>
                              <div class="txt_ar"><?=$class_name_ar?>&nbsp;<?=$section_name_ar?></div></td>
                              <td align="left" valign="middle"><?=$gender?></td>
                              <td align="left" valign="middle"><?=$mobile?></td>
                              <td align="left" valign="middle"><?=$email?></td>
                              <td align="left" valign="middle"><?=$added_date?></td>
                              <td align="left" valign="middle">
                                <div id="act_deact_<?=$tbl_student_id?>">
                                <?php if (trim($is_active) == "Y") { ?>
                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_student_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                <?php } else { ?>
                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_student_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                <?php } ?>
                                </div>
                              </td>
                              <td align="left" valign="middle">
                                <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/edit_student/student_id_enc/<?=$tbl_student_id?>"><button class="btn bg-purple fa fa-pencil" type="button" title="Edit"></button></a>
                              </td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="10" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_all_students)<=0) {
                            ?>
                            <tr>
                              <td colspan="10" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no categories available. Click on the + button to create one.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
    
            <!--Add or Create-->
              <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Student</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
                
                
                
                
                
                
                
                
                
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                  
                   
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="first_name">First Name [En]</label>
                      <div class="col-sm-10">
                        <input type="text" placeholder="First Name[En]" id="first_name" name="first_name" class="form-control">
                      </div>
                      
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="first_name_ar">First Name [Ar]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="First Name[Ar]" id="first_name_ar" name="first_name_ar" class="form-control" dir="rtl">
                      </div>
                    </div>
                    
                      <div class="form-group">
                      <label class="col-sm-2 control-label" for="last_name">Last Name [En]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Last Name[En]" id="last_name" name="last_name" class="form-control">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="last_name_ar">Last Name [Ar]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Last Name[Ar]" id="last_name_ar" name="last_name_ar" class="form-control" dir="rtl">
                      </div>
                    </div>
                    
                 
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="category_name_ar">DOB</label>
    
                      <div class="col-sm-2">
                        <select name="dob_month" id="dob_month"  class="form-control" tabindex="6">
                                      	<option value="">--Month--</option>
                                      	<?php for ($m=1; $m<=12; $m++) { ?>
                                      	<option value="<?=$m?>" <?php if ($dob_month == $m) {echo "selected";}?> ><?=$m?></option>
                                        <?php } ?>
                        </select>
                     </div>
                     <div class="col-sm-2">
                                      <?php if (!isset($dob_day) || trim($dob_day) == "") {$dob_day = '';}?>
                                      <select name="dob_day" id="dob_day" tabindex="7" class="form-control">
                                        <option value="">--Day--</option>
                                      	<?php for ($d=1; $d<=31; $d++) { ?>
                                      	<option value="<?=$d?>" <?php if ($dob_day == $d) {echo "selected";}?> ><?=$d?></option>
                                        <?php } ?>
                    </select>
                    </div>
                    <div class="col-sm-2">
                                      <?php if (!isset($dob_year) || trim($dob_year) == "") {$dob_year = '';}?>
                                      <select name="dob_year" id="dob_year" tabindex="8" class="form-control">
                                        <option value="">--Year--</option>

                                      	<?php for ($y=1950; $y<=date('Y'); $y++) { ?>
                                      	<option value="<?=$y?>" <?php if ($dob_year == $y) {echo "selected";}?> ><?=$y?></option>
                                        <?php } ?>
                                      </select>              
                    </div>  
                    </div>
                    
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="gender">Gender</label>
                       <div class="col-sm-10">
                        <label>
                          <input type="radio" id="gender" name="gender" value="male" class="minimal" <?php if (trim($gender) == "male") {echo "checked";}?> >
                          Male
                        </label>
                        &nbsp;
                        <label>
                          <input type="radio" id="gender" name="gender" value="female" class="minimal" <?php if (trim($gender) == "female") {echo "checked";}?> >
                          Female
                        </label>
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="mobile">Parent's Mobile</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Parent's Mobile" id="mobile" name="mobile" class="form-control">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="email">Email</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Email" id="email" name="email" class="form-control">
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="country">Nationality</label>
    
                      <div class="col-sm-10">
                      
                                      <select name="country" id="country" tabindex="8" class="form-control">
                                      	<?php for ($c=0; $c<count($countries_list); $c++) { ?>
                                      	<option value="<?=$countries_list[$c]['tbl_country_id']?>"><?=$countries_list[$c]['country_name']?></option>
                                        <?php } ?>
                                      </select>         
                      </div>
                    </div>
                    
                    
                      <div class="form-group">
                      <label class="col-sm-2 control-label" for="emirates_id_father">Emirates Id [Father]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Emirates Id [Father]" id="emirates_id_father" name="emirates_id_father" class="form-control">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="emirates_id_mother">Emirates Id [Mother]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Emirates Id [Mother]" id="emirates_id_mother" name="emirates_id_mother" class="form-control" >
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="tbl_academic_year_id">Academic Year</label>
    
                      <div class="col-sm-10">
                       <select name="tbl_academic_year_id" id="tbl_academic_year_id" class="form-control" >
                              <?php
                                    for ($a=0; $a<count($academic_list); $a++) { 
                                        $tbl_academic_year_id    = $academic_list[$a]['tbl_academic_year_id'];
                                        $academic_start          = $academic_list[$a]['academic_start'];
                                        $academic_end            = $academic_list[$a]['academic_end'];
									
                                  ?>
                                      <option value="<?=$tbl_academic_year_id?>">
                                      <?=$academic_start?>&nbsp;-&nbsp;<?=$academic_end?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>
                      </div>
                    </div>
                    
                 
                    
                    <div class="form-group">
                     <label class="col-sm-2 control-label" for="tbl_class_id">Class</label>
                     <div class="col-sm-10">
                                 <select name="tbl_class_id" id="tbl_class_id" class="form-control">
                              <option value="">--Select Class --</option>
							  
							  <?php
                                    for ($u=0; $u<count($classes_list); $u++) { 
                                        $tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
                                        $class_name             = $classes_list[$u]['class_name'];
                                        $class_name_ar          = $classes_list[$u]['class_name_ar'];
										$section_name           = $classes_list[$u]['section_name'];
                                        $section_name_ar        = $classes_list[$u]['section_name_ar'];
                                        if($tbl_sel_class_id == $tbl_class_id_u)
                                           $selClass = "selected";
                                         else
                                           $selClass = "";
                                  ?>
                                      <option value="<?=$tbl_class_id_u?>"  <?=$selClass?> >
                                      <?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;
                                    <?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>
                   </div>
                   </div>
                    
                  
                       <div class="form-group">
                      <label class="col-sm-2 control-label" for="picture">Picture</label>
    
                      <div class="col-sm-10">
                        <!--File Upload START-->
                            <style>
                            #advancedUpload {
                                padding-bottom:0px;
                            }
                            </style>
                                 
                            <div id="advancedUpload">Upload File</div>
                            
                            <div id="uploaded_items" >
                                <div id="div_listing_container" class="listing_container" style="display:block">	            
										<?php
                                            if (trim($img_url) != "") {
                                        ?>
                                                    <div id='<?=$tbl_uploads_id?>' class='box-header with-border'>
                                                      <div class='box-title'><img src='<?=$img_url?>' /></div>
                                                      <div class='box-tools'> <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick="confirm_delete_img_popup('<?=$tbl_uploads_id?>')">
                                                        </button>
                                                      </div>
                                                    </div>
											<style>
												.ajax-upload-dragdrop {
													display:none;	
												}
                                            </style>        
                                        <?php		
                                            }
                                        ?>
                                </div>        
                            </div>
                        <!--File Upload END-->
                      </div>
                   
                    
                    </div>
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate()">Submit</button>
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
            <!--/Add or Create-->
                
        <!--/Admin Category Management-->

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>

        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >
function search_data() {
		var tbl_category_id = $("#tbl_category_id").val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/all_categories/";
		if(tbl_category_id !='')
			url += "tbl_category_id/"+tbl_category_id+"/";
		
			url += "offset/0/";
		window.location.href = url;
		<?php /*?>window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/enquiry/all_enquiries/is_not_replied/"+is_not_replied+"/tbl_court_id/"+tbl_court_id+"/tbl_category_id/"+tbl_category_id;<?php */?>
	}
</script>