<?php
//Init Parameters
$image_gallery_id_enc = md5(uniqid(rand()));
$allowed_files_display = "File: *.mp4, *.jpg, *.jpeg, *.png, *.pdf";
if (trim($mid) == "") {
	$mid = "1";	
}
?>
 
<style>
.txt_en {
	text-align:left;
	padding-left:2px;
}
.txt_ar {
	text-align:right;
	padding-right:2px;	
	direction:rtl;		
}
textarea {
    height: 170px;
    padding-bottom: 6px;
    padding-top: 6px;
    width: 95%;
	font-size: 14px;
	border: 1px solid #ddd;
 }
 select {
   font-size: 14px;
   border: 1px solid #ddd;
 }
</style>
<?php /*?><script type="text/javascript" src="<?=JS_PATH?>/nice_edit/nicEdit.php"></script>
<script language="javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script><?php */?>
<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/all_galleries";
			}
		})
	});
	
function confirm_delete_popup() {
		var len = $("input[id='tbl_image_gallery_id']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more Image(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/deleteGallery",
			data: {
				tbl_image_gallery_id: $("input[id='tbl_image_gallery_id']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Image(s) deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}	
	function ajax_activate(tbl_image_gallery_id) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/activateGallery",
			data: {
				tbl_image_gallery_id: tbl_image_gallery_id,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Image activated successfully.", 'green')

				$('#act_deact_'+tbl_image_gallery_id).html('<span style="cursor:pointer" onClick="ajax_deactivate(\''+tbl_image_gallery_id+'\')" onMouseOver="deactivate_me(this)" onMouseOut="reset_activate(this)" class="label label-success">Active</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_deactivate(tbl_image_gallery_id) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/deactivateGallery",
			data: {
				tbl_image_gallery_id: tbl_image_gallery_id,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Image de-activated successfully.", 'green')
				
				$('#act_deact_'+tbl_image_gallery_id).html('<span style="cursor:pointer" onClick="ajax_activate(\''+tbl_image_gallery_id+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	
	function ajax_add_gallery() {
		
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/add_school_gallery",
			data: {
				tbl_image_gallery_id: "<?=$image_gallery_id_enc?>",
				tbl_gallery_category_id : $('#tbl_gallery_category_id').val(),
				is_active         : $("#activeDiv input[type='radio']:checked").val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='N') {
					refresh_page = "N";
					my_alert("Gallery uploaded failed, Please try again.", 'red');
					$("#pre-loader").hide();
				   
				}else{
					 refresh_page = "Y";
				    my_alert("Gallery uploaded successfully.", 'green');
				    $("#pre-loader").hide();
				}
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_update_gallery() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/update_school_gallery",
			data: {
				tbl_image_gallery_id : $('#tbl_image_gallery_id').val(),
				tbl_gallery_category_id : $('#tbl_gallery_category_id').val(),
				is_active         : $("#activeDiv input[type='radio']:checked").val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='N') {
					refresh_page = "N";
					my_alert("Gallery uploaded failed, Please try again.", 'red');
					$("#pre-loader").hide();
				   
				}else{
					 refresh_page = "Y";
				    my_alert("Gallery uploaded successfully.", 'green');
				    $("#pre-loader").hide();
				}
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	
	function exist_gallery() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/exist_gallery",
			data: {
			    tbl_image_gallery_id: "<?=$image_gallery_id_enc?>",
				is_ajax: true
			},
			success: function(data) {
				
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='T') {
					refresh_page = "N";
					my_alert("Image(s) not uploaded. Please upload Images(s)", 'red');
					$("#pre-loader").hide();
				}else if (temp=='Y') {
					refresh_page = "N";
					my_alert("Image(s) not uploaded. Please upload Images(s)", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_add_gallery();
				}
			
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function exist_gallery_edit() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/exist_gallery",
			data: {
				tbl_image_gallery_id: $('#image_gallery_id_enc').val(), 
				is_ajax: true
			},
			success: function(data) {
				
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='T') {
					refresh_page = "N";
					my_alert("Image(s) not uploaded. Please upload Images(s).", 'red');
					$("#pre-loader").hide();
				}else if (temp=='Y') {
					refresh_page = "N";
					my_alert("Image(s) not uploaded. Please upload Images(s)", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_update_gallery();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
</script>
<script language="javascript">
   //add student
   /* || validate_picture() == false*/
	function ajax_validate() {
		if (validate_category() == false ) 
		{
			return false;
		}
		else{
		 	exist_gallery();
		}
	}
	
    //edit student
	function ajax_validate_edit() {
		if (validate_category() == false ) 
		{
			return false;
		} 
		else{
			exist_gallery_edit();
		}
	} 
	
  /************************************* START MESSAGE VALIDATION *******************************/

   function validate_category() {
		var regExp = / /g;
		var str = $("#tbl_gallery_category_id").val();
		if (str.length <= 0) {
			my_alert("Gallery category is not selected. Please select gallery category")
			$("#tbl_gallery_category_id").focus();
		return false;
		}


		var is_image = $.trim($('#div_listing_container').html());		
		
		if (is_image == "") {
			my_alert("Image is mandatory. Please upload image.")
		return false;	
		}
		
		return true;
	}
	
</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Image Gallery <small> Management</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Image Gallery</li>
    </ol>
    <!--/BREADCRUMB--> 
 <div style=" float:right; padding-right:10px;"> <button onclick="show_categories()" title="Categories" type="button" class="btn btn-primary">Categories</button></div>

    <div style="clear:both"></div>
  </section>
      <link href="<?=HOST_URL?>/assets/admin/dist/css/jquery-ui.css" rel="stylesheet">
      <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-1.11.1.js"></script>
      <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-ui.js"></script>
      <link href="<?=HOST_URL?>/assets/admin/dist/css/uploadfile.min.css" rel="stylesheet">
                      

  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {
	?>
        <!--Edit-->
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Record</h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/school_records"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

            
     <style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url('<?=IMG_PATH?>/delete.jpg');
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
		clear:both;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
	#overlay_container {
		position:relative;
	}
	#overloading {
		background-image:url('<?=IMG_PATH?>/preloader/preloader_2.gif');
		background-repeat:no-repeat;
		background-position:center;
		background-color:#CCC;
		position:absolute;
		left:0px;
		top:0px;
		opacity: 0.3;
		z-index: 10000;
	}
	#div_listing_container {
		display:none;	
	}
	.d_d_text {
		color:#745156;
		font-size:16px;
			
	}
	.ajax-upload-dragdrop {
		margin:auto;
		margin-bottom:10px;
		width:300px !important;
	}
	.ajax-file-upload-statusbar {
		margin:auto;
		margin-top:10px;
	}
	.ajax-file-upload {
		height:31px;
	}
	
	
	 #tabs-1{  
	    overflow-y:scroll; overflow-x:none;
	}

    #tabs-2{
		overflow-y:scroll; overflow-x:none;
	}
				  
  .ui-tabs-active{
		border-color:#efca86  !important;
   }
					 
	.ui-tabs .ui-tabs-nav li {
		float:left;
		font-size: 16px;
        font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif;
  }
  label{
	  display: inline-block;
      font-weight: 700;
  }
  
  .ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button {
    font-family:"Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
    font-size: 14px;
}
  
  .ui-widget{
	 font-size: 16px;
     font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
  }
  .form-control{
	 font-size: 14px; 
  }
</style>         
         <?php
		 	$tbl_parenting_school_id             = $school_record[0]['tbl_parenting_school_id'];		
			$tbl_parenting_school_cat_id         = $school_record[0]['tbl_parenting_school_cat_id'];
			$parenting_title_en                  = $school_record[0]['parenting_title_en'];		
			$parenting_title_ar                  = $school_record[0]['parenting_title_ar'];
			$parenting_type                      = $school_record[0]['parenting_type'];		
			$parenting_logo                      = $school_record[0]['parenting_logo'];
			$parenting_text_en                   = $school_record[0]['parenting_text_en'];	
			$parenting_text_ar                   = $school_record[0]['parenting_text_ar'];	
			$parenting_url                       = $school_record[0]['parenting_url'];
			$is_active                           = $school_record[0]['is_active'];
			$file                                = $school_record[0]['file_name_updated'];
			$tbl_uploads_id                      = $school_record[0]['tbl_uploads_id'];
			if($parenting_logo<>""){
			  $img_url                           = IMG_UPLOAD_PATH."/".$parenting_logo;
			}
			if($file<>""){
			  $file_url                          = IMG_UPLOAD_PATH."/".$file;
			}
			
	
		 ?>   
         <div class="box-body">
                    <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
    
                        
                     <div class="form-group">
                     <label class="col-sm-2 control-label" for="tbl_parenting_school_cat_id">Records Category<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-10">
                                 <select name="tbl_parenting_school_cat_id" id="tbl_parenting_school_cat_id" class="form-control"  >
                                 <option value="">Select Category</option>
                              <?php
                                    for ($u=0; $u<count($category_list); $u++) { 
                                        $tbl_parenting_school_cat_id_u   = $category_list[$u]['tbl_parenting_school_cat_id'];
                                        $title_en         = $category_list[$u]['title_en'];
                                        $title_ar         = $category_list[$u]['title_ar'];
                                        if($tbl_parenting_school_cat_id == $tbl_parenting_school_cat_id_u)
                                           $selType = "selected";
                                         else
                                           $selType = "";
                                  ?>
                                      <option value="<?=$tbl_parenting_school_cat_id_u?>"  <?=$selType?> >
                                      <?=$title_en?>&nbsp;[::]&nbsp;
                                    <?=$title_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>
                    </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="parenting_title_en">Title [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Title [En]" id="parenting_title_en" name="parenting_title_en" class="form-control"  value="<?=$parenting_title_en?>" >
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="parenting_title_ar">Title [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Title [Ar]" id="parenting_title_ar" name="parenting_title_ar" class="form-control" dir="rtl" value="<?=$parenting_title_ar?>" >
                      </div>
                    </div>
                    
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="thumb_image">Thumb Image</label>
    
                      <div class="col-sm-10" >
                      
                       
                        <!--File Upload START-->
                           <div id="divThumb"  <?php if (trim($img_url) <> "") {?> style="display:none;" <?php } ?> >
                            <style>
                            #advancedNewUpload {
                                padding-bottom:0px;
                            }
                            </style>
                                 
                               <div id="advancedNewUpload">Upload File</div>
                            
                            </div>
                            <div id="uploaded_items" >
                                <div id="div_listing_container_new" class="listing_container" style="display:block">	            
										<?php
                                            if (trim($img_url) != "") {
                                        ?>
                                                    <div id='<?=$id?>' class='box-header with-border'>
                                                      <div class='box-title'><img src='<?=$img_url?>' /></div>
                                                      <div class='box-tools'> <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick="confirm_delete_img()">
                                                        </button>
                                                      </div>
                                                    </div>
											<style>
												.ajax-upload-dragdrop-new {
													display:block;	
												}
                                            </style>        
                                        <?php		
                                            }
                                        ?>
                                </div>        
                            </div>
                        <!--File Upload END-->
                      </div>
                   
                    
                    </div>
                    
                    
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="parenting_type">Record Type</label>
    
                      <div class="col-sm-10" id="typeDiv" >
                         <input type="radio" name="parenting_type" value="d" onClick="show_text()" <?php if($parenting_type=='d') { ?> checked="checked" <?php } ?> > Text&nbsp;&nbsp;&nbsp;&nbsp;
                         <input type="radio"  name="parenting_type" value="u" onClick="show_url()" <?php if($parenting_type=='u') { ?> checked="checked" <?php } ?> > URL&nbsp;&nbsp;&nbsp;&nbsp;
                         <input name="parenting_type" type="radio" value="v" onClick="show_video()" <?php if($parenting_type=='v') { ?> checked="checked" <?php } ?> > <?=$allowed_files_display?>
                      </div>
                    </div>
                          
                 
                <div id="parenting_text" <?php if($parenting_type<>'d') { ?> style="display:none;" <?php } ?> >
                       <div class="form-group" >
                       <label class="col-sm-2 control-label" for="parenting_text_en">Parenting Text [En]</label>
    
                          <div class="col-sm-10">
                            <textarea name="parenting_text_en" id="parenting_text_en" cols="80" rows="10" class="form-control" ><?=$parenting_text_en?></textarea>
                          </div>
                        </div>
                      
                        <div class="form-group">
                       <label class="col-sm-2 control-label" for="parenting_text_ar">Parenting Text [Ar]</label>
    
                          <div class="col-sm-10">
                            <textarea name="parenting_text_ar" id="parenting_text_ar" cols="80" rows="10" class="form-control" dir="rtl"><?=$parenting_text_ar?></textarea>
                          </div>
                        </div>
                    
                 
                 </div>
                <div id="parenting_video"  <?php if($parenting_type<>'v') { ?> style="display:none;" <?php } ?> >
                 
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="thumb_image">Record File</label>
    
                      <div class="col-sm-10" >
                         <div id="divFile">
                        <!--File Upload START-->
                            <style>
                            #advancedFileUpload {
                                padding-bottom:0px;
                            }
                            </style>
                                 
                            <div id="advancedFileUpload">Upload File</div>
                        </div>
                            
                            <div id="uploaded_items_file" >
                                <div id="div_listing_container" class="listing_container" style="display:block">	            
										<?php
                                            if (trim($file_url) != "") {
												
												$file_type = explode(".",$file);
												
												
                                        ?>
                                                    <div id='<?=$tbl_uploads_id?>' class='box-header with-border'>
                                                      <div class='box-title'>
                                                      <?php if($file_type=="png" || $file_type=="jpg") { ?>
                                                               <img src='<?=$file_url?>' />
                                                      <?php }else{ ?>
                                                      <a href='<?=$file_url?>' target='_blank' ><?=$file?></a>
                                                      <?php } ?>
                                                      
                                                      </div>
                                                      <div class='box-tools'> <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick="confirm_delete_file('<?=$tbl_uploads_id?>')">
                                                        </button>
                                                      </div>
                                                    </div>
											<style>
												.ajax-upload-dragdrop {
													display:none;	
												}
                                            </style>        
                                        <?php		
                                            }
                                        ?>
                                </div>        
                            </div>
                        <!--File Upload END-->
                      </div>
                   
                    
                    </div>
                 </div>  
                          
                          
                 <div id="parenting_url" <?php if($parenting_type<>'u') { ?> style="display:none;" <?php } ?> >
                        <div class="form-group">
                         <label class="col-sm-2 control-label" for="parenting_url">URL</label>
    
                          <div class="col-sm-10">
                           <input type="text" name="url" id="url" value="<?=$parenting_url?>" size="50" class="form-control" maxlength="50">
                          </div>
                        </div>
                 </div>
                 
                 <div class="form-group">
                         <label class="col-sm-2 control-label" for="parenting_status">Status</label>
    
                          <div class="col-sm-10" id="activeDiv">
                           <input name="is_active" type="radio" value="Y" <?php if($is_active=="Y"){?> checked="checked" <?php } ?> > Yes
                           <input type="radio" name="is_active" value="N" <?php if($is_active=="N"){?> checked="checked" <?php } ?> > No
                          </div>
                  </div>
              
                          
                     
                        <!-- /.box-body -->
                      <div class="box-footer">
                        <button class="btn btn-primary" type="button" onclick="ajax_validate_edit()">Submit</button>
                        <input type="hidden" name="tbl_parenting_school_id" id="tbl_parenting_school_id" value="<?=$tbl_parenting_school_id?>" />
                        <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                      </div>
                      <!-- /.box-footer -->  
                 <script>
                      var item_id = $("#tbl_parenting_school_id").val();
                 </script>    
            
           </form>
                </div>    
                
    </div>
		        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/gallery/all_galleries";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
  
 <link href="<?=HOST_URL?>/assets/admin/dist/css/jquery-ui.css" rel="stylesheet">
 <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-1.11.1.js"></script>
 <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-ui.js"></script>
  <script>
  $( function() {
		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
	
			var order = $("#tabledivbody").sortable("serialize");
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	
				
			}
	  } );
  
  } );
  </script> 
  
 <link href="http://hayageek.github.io/jQuery-Upload-File/uploadfile.min.css" rel="stylesheet">

<style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url(<?=IMG_PATH?>/delete.jpg);
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
</style>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="http://hayageek.github.io/jQuery-Upload-File/jquery.uploadfile.min.js"></script>
<script type="text/javascript" src="<?=JS_PATH?>/jquery.timer.js"></script>
<script>

$(document).ready(function()
{
	var tbl_item_id = '<?=$image_gallery_id_enc?>';
	$("#advancedUpload").uploadFile({
		allowedTypes: "mp4,jpg,jpeg,png,pdf",
		maxFileCount:1,
		url:"<?=HOST_URL?>/admin/upload.php",
		fileName:"myfile",
		formData: {"module_name":"parenting","tbl_item_id":tbl_item_id},
		onSubmit:function(files)
		{
			//if ($("#upload_content").val().trim() != "") {alert("The file is already uploaded."); return false;}
			//$("#eventsmessage").html($("#eventsmessage").html()+"<br/>Submitting:"+JSON.stringify(files));
		},
		onSuccess:function(files,data,xhr)
		{
			if (data == "error") {
				alert("Error uploading file. Please try again.");
				return;
			}
			add_uploaded_item(files, data);
		}
	});
});

// tbl_item_id = tbl_uploads_id
		/*function add_uploaded_item(item_name, tbl_item_id) {
			//alert(item_name+"-"+ tbl_item_id);
			var tbl_item_id = "tmp001"+Math.random();
			tbl_item_id = tbl_item_id.replace(".","_");
			//alert(tbl_item_id)
			var row_item = document.createElement('div');
			$(row_item).addClass("row_item")
				.attr("id",tbl_item_id)
				.appendTo($("#uploaded_items"))
				
			var upload_del = document.createElement('div');
			$(upload_del).addClass("upload_del")
				.appendTo($("#"+tbl_item_id)) //main div
				.click(function(){
					delete_file(tbl_item_id);
				})
		
			var upload_content = document.createElement('div');
			$(upload_content).addClass("upload_content")
				.html(item_name)
				.appendTo($("#"+tbl_item_id)) //main div
			//$("#parenting_video").css("display","none");
			$("#vid_text").css("display","block");
		return;
		}
*/
function delete_file(tbl_uploads_id) {
	if (confirm("Are you sure you want to delete?")) {
		delete_file_ajax(tbl_uploads_id)
	}
	return;
}



var connectivity_msg = "Connection timed out. Please try again.";
var connectivity_timeout_time = 10000;
var host = '<?=HOST_URL?>';
/* Function to load a navigation page */
function delete_file_ajax(tbl_uploads_id) {
	//show_loading();
	if ($("#parenting_type").val() == "d") {
		alert("Please choose Type to be 'File' in order to proceed.");
		return;
	}

	var xmlHttp, rnd, url, search_param, ajax_timer;
	rnd = Math.floor(Math.random()*11);
	try{		
		xmlHttp = new XMLHttpRequest(); 
	}catch(e) {
		try{
			xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		}catch(e) {
			xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
			hide_loading();
		}
	}

	//AJAX response
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
			ajax_timer.stop();
			var d = document.getElementById("uploaded_items");
			var d_nested = document.getElementById(tbl_uploads_id);
			var throwawayNode = d.removeChild(d_nested);
			if ($("#parenting_type").html().trim() == "v") {
				$("#parenting_video").css("display","block");
				$("#vid_text").css("display","none");
			}
			alert("The file have been deleted successfully.");
		}
	}

	ajax_timer = $.timer(function() {
		xmlHttp.abort();
		alert(connectivity_msg);
		ajax_timer.stop();
	},connectivity_timeout_time,true);

	//Sending AJAX request
	url = host + "/admin/delete_upload.php?tbl_uploads_id="+tbl_uploads_id+"&rnd="+rnd;
	//alert(url);
	xmlHttp.open("POST",url,true);
	xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlHttp.send("rnd="+rnd);
}

	
	
	function show_categories()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/gallery_categories_list/";
		window.location.href = url;
	}
	

	
</script>
                   <div id="mid1" class="box box-success">
                        <div class="box-header">
                          <div class="col-sm-1" >
                          <h3 class="box-title">SEARCH</h3>
                          </div>
                          <div class="col-sm-11"> 
                             
                               <div class="col-sm-6"><input name="q" id="q" value="<?=urldecode($q)?>" type="text" class="form-control" placeholder="Search By Category "   > </div>
                               <div class="col-sm-2"><button class="btn btn-success" type="button" onclick="search_data()">Search</button>&nbsp;<button class="btn btn-success" type="button" 
                               onclick="reset_data();">Reset</button>
                               </div>
                           
                          </div>
                        </div>  
                     </div>     
    
            <!--Listing-->
                    <div id="mid1_list" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Image Galleries</h3>
                          <div class="box-tools">
                            <?php if (count($rs_all_gallery)>0) { echo $paging_string;}?>	
                           <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
                            <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>
                          </div>
                        </div>
                        
                        <div class="box-body">
                     <!--   <div style="color:#030; font-weight:bold;">You can sort students by using drag and drop of rows </div>-->
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle"><input id="select_all" type="checkbox" value="" /></th>
                              <!--<th width="10%" align="center" valign="middle">Sl No.</th>-->
                             <!-- <th width="25%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y">Title <?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?><div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>-->
                             <!-- <th width="20%" align="center" valign="middle">Class</th>-->
                             
                              <th width="35%" align="center" valign="middle">Category</th> 
                              <th width="20%" align="center" valign="middle">Picture</th>
                              <!--<th width="10%" align="center" valign="middle">Data</th>-->
                              <th width="10%" align="center" valign="middle">Date</th>
                              <th width="5%" align="center" valign="middle">Status</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                                
								
								for ($i=0; $i<count($rs_all_gallery); $i++) { 
										$id                         = $rs_all_gallery[$i]['id'];
										$tbl_image_gallery_id       = $rs_all_gallery[$i]['tbl_image_gallery_id'];
										$image_text_en              = $rs_all_gallery[$i]['image_text_en'];
										$image_text_ar              = $rs_all_gallery[$i]['image_text_ar'];
										$file_name_updated_thumb    = $rs_all_gallery[$i]['file_name_updated_thumb'];
										$added_date                 = $rs_all_gallery[$i]['added_date'];
										$is_active                  = $rs_all_gallery[$i]['is_active'];
										$category_name_en           = $rs_all_gallery[$i]['category_name_en'];
										$category_name_ar           = $rs_all_gallery[$i]['category_name_ar'];
										$cntGallery                 = $rs_all_gallery[$i]['cntGallery'];
										$tbl_gallery_category_id    = $rs_all_gallery[$i]['tbl_gallery_category_id'];
										/*if($file_name_updated_thumb<>"")
										{
											$file_url = IMG_GALLERY_PATH."/".$file_name_updated_thumb;
										}*/
									    $category_image           = $rs_all_gallery[$i]['category_image'];
										if($category_image<>"")
										{
											$file_url = IMG_GALLERY_PATH."/".$category_image;
										}
										
										$category_image           = $rs_all_gallery[$i]['category_image'];
									
                                    $added_date = date('m-d-Y',strtotime($added_date));
									
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_image_gallery_id?>" >
                              <td align="left" valign="middle">
                              <span style="float:left;">
                              <input id="tbl_image_gallery_id" name="tbl_image_gallery_id" class="checkbox" type="checkbox" value="<?=$tbl_image_gallery_id?>" />
                              </span>
                              
                             <?php /*?> <span style="float:left;">&nbsp;
                              <?php if($i<>0){ ?> <i class="fa fa-arrow-up"  style="color:#3c8dbc; cursor:pointer;"  aria-hidden="true" title="Sorting - Drag & Drop To Up"></i> &nbsp; <?php } ?>
                               <?php if($i<> count($rs_all_categories)-1){ ?> <i class="fa fa-arrow-down" style="color:#3c8dbc;cursor:pointer;" aria-hidden="true" title="Sorting - Drag & Drop To Down"></i> <?php } ?>
                              </span><?php */?>
                              </td>
                             <!-- <td align="left" valign="middle"><?=$offset+$i+1?></td>-->
                              <!--<td align="left" valign="middle">
                             <span style="float:left;"><?=$image_text_en?></span><span style="float:right;"><?=$image_text_ar?></span></td>-->
                             <?php /*?> <td align="left" valign="middle"> 
                              <div class="txt_en"><?=$class_name?>&nbsp;<?=$section_name?></div>
                              <div class="txt_ar"><?=$class_name_ar?>&nbsp;<?=$section_name_ar?></div></td><?php */?>
                              <td align="left" valign="middle">
                              <span style="float:left;"><a style="cursor:pointer;" onclick="show_pictures('<?=$tbl_gallery_category_id?>','<?=$tbl_school_id?>','');"><?=$category_name_en?></a></span>
                              <span style="float:right;"><a style="cursor:pointer;" onclick="show_pictures('<?=$tbl_gallery_category_id?>','<?=$tbl_school_id?>','');"><?=$category_name_ar?></a></span></td>
                              <td align="center" valign="middle"><a style="text-decoration:underline;" onclick="show_pictures('<?=$tbl_gallery_category_id?>','<?=$tbl_school_id?>','');"><img src="<?=$file_url?>" title="" style="width:100px; height:100px; margin-right:5px; cursor:pointer;"><?=$cntGallery?> Pictures</a></td>
                              <?php /*?><td align="left" valign="middle"><?=$parentingData?></td><?php */?>
                              <td align="left" valign="middle"><?=$added_date?></td>
                              <td align="left" valign="middle">
                               <div id="act_deact_<?=$tbl_image_gallery_id?>">
                                <?php if (trim($is_active) == "Y") { ?>
                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_image_gallery_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                <?php } else { ?>
                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_image_gallery_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                <?php } ?>
                                </div>
                              </td>
                           
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="10" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_all_gallery)<=0) {
                            ?>
                            <tr>
                              <td colspan="10" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no pictures available. Click on the + button to add pictures.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>
            <script>
			function show_pictures(tbl_gallery_category_id,tbl_school_id,tbl_student_id) {
				var url=  "<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/view_school_gallery_list/tbl_gallery_category_id/"+tbl_gallery_category_id+"/tbl_school_id/"+tbl_school_id+"/tbl_student_id/"+tbl_student_id;
				window.location.href=url;
			}
			</script>       
            <!--/Listing-->
    
            <!--Add or Create-->
            <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Images</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
   	            <div class="box-body">
                    <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
    
                        
                     <div class="form-group">
                     <label class="col-sm-2 control-label" for="tbl_gallery_category_id">Gallery Category<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-10">
                                 <select name="tbl_gallery_category_id" id="tbl_gallery_category_id" class="form-control"  >
                                 <option value="">Select Category</option>
                              <?php
                                    for ($u=0; $u<count($category_list); $u++) { 
                                        $tbl_gallery_category_id_u   = $category_list[$u]['tbl_gallery_category_id'];
                                        $category_name_en            = $category_list[$u]['category_name_en'];
                                        $category_name_ar            = $category_list[$u]['category_name_ar'];
                                        if($tbl_sel_gallery_category_id_u == $tbl_gallery_category_id_u)
                                           $selType = "selected";
                                         else
                                           $selType = "";
                                  ?>
                                      <option value="<?=$tbl_gallery_category_id_u?>"  <?=$selType?> >
                                      <?=$category_name_en?>&nbsp;[::]&nbsp;
                                    <?=$category_name_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>
                    </div>
                    </div>
                   
                    <style>
						.ajax-upload-dragdrop {
							width:380px !important;	
						}
                    </style>  
                    
                  
                     <div class="form-group" >
                      <label class="col-sm-2 control-label" for="image">Image<span style="color:#F30; padding-left:2px;">*</span></label>
                      
                      <div class="col-sm-10" >
                      <div id="uploaded_items_file" >
                                <div id="div_listing_container" class="listing_container" style="display:block">	            
										<?php
                                            if (trim($img_url) != "") {
                                        ?>
                                                    <div id='<?=$tbl_uploads_id?>' class='box-header with-border'>
                                                      <div class='box-title'><img src='<?=$img_url?>' /></div>
                                                      <div class='box-tools'> <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick="confirm_delete_file_popup('<?=$tbl_uploads_id?>')">
                                                        </button>
                                                      </div>
                                                    </div>
											<style>
												.ajax-upload-dragdrop {
													display:none;	
												}
                                            </style>        
                                        <?php		
                                            }
                                        ?>
                                </div>        
                          </div>
                         <div id="divFile">
                        <!--File Upload START-->
                            <style>
                            #advancedFileUpload {
                                padding-bottom:0px;
                            }
							
                            </style>
                                 
                            <div id="advancedFileUpload">Upload File</div>
                        </div>
                            
                            
                        <!--File Upload END-->
                      </div>
                    </div>
                   
                 <div class="form-group">
                         <label class="col-sm-2 control-label" for="parenting_status">Status</label>
    
                          <div class="col-sm-10" id="activeDiv">
                           <input name="is_active" type="radio" value="Y" checked> Yes
                           <input type="radio" name="is_active" value="N"> No
                          </div>
                  </div>
                     
                        <!-- /.box-body -->
                      <div class="box-footer">
                        <button class="btn btn-primary" type="button" onclick="ajax_validate()">Submit</button>
                        <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                      </div>
                      <!-- /.box-footer -->  
                      
            
           </form>
                </div>
           </div>
           <script>
           var item_id = "<?=$image_gallery_id_enc?>";  

			
	
	
<?php /*?>	$('#myModal').modal('show');
	$.ajax({
		type: "POST",
		url: "<?=HOST_URL?>/en/parent/parent_user/view_school_gallery_details",
		data: {
			tbl_gallery_category_id: tbl_gallery_category_id,
			tbl_school_id: tbl_school_id,
			is_ajax: true
		},
		success: function(data) {
			$("#modal-body").html(data);
 
		},
		error: function() {
			$('#pre_loader').css('display','none');	
		}, 
		complete: function() {
			$('#pre_loader').css('display','none');	
		}
	});	<?php */?>
}
</script>
		  </script>   
  
            <!--/Add or Create-->
                
        <!--/Admin Category Management-->

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>


<script src="<?=HOST_URL?>/assets/admin/dist/js/jquery.uploadfile.min.js"></script>
<script language="javascript">
//Primary Key for a Form. 

function set_item_id(obj) {
	item_id = obj.value;
	get_files();	
}

$(document).ready(function() {
	var item_id = '<?=$image_gallery_id_enc?>';
	/*
	var uploadObj = $("#advancedNewUpload").uploadFile({
		url:"<?=HOST_URL?>/file_mgmt/upload_the_file",
		multiple:true,
		autoSubmit:true,
		maxFileSize:130000,
		allowedTypes: "jpg,jpeg,png,gif",
		fileName:"myfile",
		formData: {"module_name":"school_gallery_thumb"},
		dynamicFormData: function() {
			var data = { item_id:item_id}
			return data;
		},
		showStatusAfterSuccess:false,
		dragDropStr: "<span class='d_d_text'>Drag and Drop the File to Upload.</span>",
		abortStr:"Abourt",
		cancelStr:"Cancel",
		doneStr:"Done",
		multiDragErrorStr: "Multi Drag Error.",
		extErrorStr:"Extention Error:",
		sizeErrorStr:"Max Size Error:",
		uploadErrorStr:"Upload Error",
		onSelect:function(files) {
 		},
		onSubmit:function(files) {
 		},
		onSuccess:function(files, data, xhr) {
			if (data == "error") {
				alert("Error uploading file. Please try again.");
				return;
			}
			var obj = JSON.parse(data);
			var tbl_uploads_id = obj.tbl_uploads_id;
			var file_name_updated = obj.file_name_updated;
			$("#divThumb").hide();
			//alert("tbl_uploads_id: "+tbl_uploads_id)
			//alert("file_name_updated: "+file_name_updated)
			add_new_uploaded_item(tbl_uploads_id, file_name_updated);
		},
		afterUploadAll:function() {
 		},
		onError: function(files, status, errMsg) {
 		}
	});
	*/
	
	
	var uploadFileObj = $("#advancedFileUpload").uploadFile({
		url:"<?=HOST_URL?>/file_mgmt/upload_the_file",
		multiple:true,
		autoSubmit:true,
		maxFileSize:500000000,
		allowedTypes: "jpg,jpeg,png,gif",
		fileName:"myfile",
		formData: {"module_name":"school_gallery_large"},
		dynamicFormData: function() {
			var data = { item_id:item_id}
			return data;
		},
		showStatusAfterSuccess:false,
		dragDropStr: "<span class='d_d_text'>Drag and Drop the File to Upload.</span>",
		abortStr:"Abourt",
		cancelStr:"Cancel",
		doneStr:"Done",
		multiDragErrorStr: "Multi Drag Error.",
		extErrorStr:"Extention Error:",
		sizeErrorStr:"Max Size Error:",
		uploadErrorStr:"Upload Error",
		onSelect:function(files) {
 		},
		onSubmit:function(files) {
 		},
		onSuccess:function(files, data, xhr) {
			if (data == "error") {
				alert("Error uploading file. Please try again.");
				return;
			}
			var obj = JSON.parse(data);
			var tbl_uploads_id = obj.tbl_uploads_id;
			var file_name_updated = obj.file_name_updated;
			//$("#divFile").hide();
			
			//alert("tbl_uploads_id: "+tbl_uploads_id)
			//alert("file_name_updated: "+file_name_updated)
			add_new_uploaded_file(tbl_uploads_id, file_name_updated);
		},
		afterUploadAll:function() {
 		},
		onError: function(files, status, errMsg) {
 		}
	});
	
	

	$("#startUpload").click(function() {
		uploadObj.startUpload();
	});
	
	$("#startUpload").click(function() {
		uploadFileObj.startUpload();
	});
	
	try { 
		$('input[type=file]').click();
	} catch(e) {
		alert(e)
	}
});

//Function called when file is uploaded
function add_new_uploaded_item(tbl_uploads_id, file_name_updated) {
	var str = "<div id='"+tbl_uploads_id+"' class='box-header with-border'> <div class='box-title'><img src='<?=IMG_GALLERY_PATH?>/"+file_name_updated+"'  width='108' height='130'/></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_img_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
		
	$("#div_listing_container_new").show();
	$("#div_listing_container_new").append(str);
	
	//$(".ajax-upload-dragdrop-new").hide();//Hide the upload button
return;
}

function add_new_uploaded_file(tbl_uploads_id, file_name_updated) {
	var result      = $(file_name_updated).text().split('.');
    var type_file   =  result[1];
	var str = "<div id='"+tbl_uploads_id+"' class='box-header with-border'> <div class='box-title'><img src='<?=IMG_GALLERY_PATH?>/"+file_name_updated+"' width='108' height='130' /></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_img_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
	/*if(type_file=='png' || type_file=='jpg')
	{
		var str = "<div id='"+tbl_uploads_id+"' class='box-header with-border'> <div class='box-title'><img src='<?=IMG_GALLERY_PATH?>/"+file_name_updated+"' /></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_file_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
	}else{
		var str = "<div id='"+tbl_uploads_id+"' class='box-header with-border'> <div class='box-title'><a target='_blank' href='<?=IMG_GALLERY_PATH?>/"+file_name_updated+"' >"+file_name_updated+"</a></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_file_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
	}*/
		
	$("#div_listing_container").show();
	$("#div_listing_container").append(str);
	//$(".ajax-upload-dragdrop").hide();//Hide the upload button
return;
}


function confirm_delete_img_popup(tbl_uploads_id) {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		$('#'+tbl_uploads_id).hide();	
		//$(".ajax-upload-dragdrop-new").show();
		$("#divThumb").show();
		var url_str = "<?=HOST_URL?>/file_mgmt/delete_file";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					tbl_uploads_id: tbl_uploads_id
				},
			success: function(data) {
				$("#pre-loader").hide();
			}
		});	
	} else {
		$("#pre-loader").hide();		
	}
}


function confirm_delete_img() {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		$("#divThumb .ajax-upload-dragdrop").css({"display":"block"});
		$("#divThumb").show();
		$("#div_listing_container_new").html('');
		$("#pre-loader").hide();	
	} 
}

function confirm_delete_file(tbl_uploads_id) {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		$('#'+tbl_uploads_id).hide();	
		//$(".ajax-upload-dragdrop").show();
		$("#divFile .ajax-upload-dragdrop").css({"display":"block"});
		$("#divFile").show();
		
		var url_str = "<?=HOST_URL?>/file_mgmt/delete_file";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					tbl_uploads_id: tbl_uploads_id
				},
			success: function(data) {
				$("#pre-loader").hide();
			}
		});	
	} else {
		$("#pre-loader").hide();		
	}
}






function confirm_delete_file_popup(tbl_uploads_id) {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		$('#'+tbl_uploads_id).hide();	
		//$(".ajax-upload-dragdrop").show();
		$("#divFile").show();
		
		var url_str = "<?=HOST_URL?>/file_mgmt/delete_file";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					tbl_uploads_id: tbl_uploads_id
				},
			success: function(data) {
				$("#pre-loader").hide();
			}
		});	
	} else {
		$("#pre-loader").hide();		
	}
}


function get_files() {
	var url_str = "<?=HOST_URL?>/misc/get_files.php";
	
	$.ajax({
		type: "POST",
		url: url_str,
		data: {
				module_name: "student",
				show_del: "Y",
				item_id: item_id//global variable
			},
		success: function(data) {
			$('#div_listing_container_new').show();
			$('#div_listing_container_new').html(data)
			
		}
	});	
}


</script>

<!--File Upload END-->
        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >
function search_data() {
		var q = $("#q").val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/all_galleries/";
		
		if(q !='')
			url += "q/"+q+"/";
		
			url += "offset/0/";
		window.location.href = url;
	}

function reset_data() {
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/all_galleries/";
		url += "offset/0/";
		window.location.href = url;
	}
</script>