
<?php
/*$colorCode = array("#f56954","#00a65a","#f39c12","#00c0ef","#3c8dbc","#d2d6de","#f56954","#00a65a","#f39c12","#00c0ef","#3c8dbc","#d2d6de");

$pieHistory = "[ ";
for($b=0;$b<count($list_category); $b++)
{
	$list_category[$b]['cntEnquiry'];
	if(LAN_SEL=="ar")
	    $category_name  = $list_category[$b]['parent_category_name_ar']." - ".$list_category[$b]['category_name_ar'];
	else
		$category_name  = $list_category[$b]['parent_category_name_en']." - ".$list_category[$b]['category_name_en'];

    $pieHistory .= "{
        value: '".$list_category[$b]['cntEnquiry']."',
        color: '".$colorCode[$b]."',
        highlight: '".$colorCode[$b]."',
        label: '".$category_name."'      }, ";
}
$pieHistory .= " ]; ";


$pieHistoryCourt = "[ ";
for($d=0;$d<count($list_court); $d++)
{
	$list_court[$d]['cntEnquiry'];
	if(LAN_SEL=="ar")
		$court_name = $list_court[$d]['court_name_ar'];
	else
		$court_name = $list_court[$d]['court_name_en'];
	

    $pieHistoryCourt .= "{
        value: '".$list_court[$d]['cntEnquiry']."',
        color: '".$colorCode[$d]."',
        highlight: '".$colorCode[$d]."',
        label: '".$court_name."'      }, ";
}
$pieHistoryCourt .= " ]; ";



$pieNationality = "[ ";
for($b=0;$b<count($list_enq_nationality); $b++)
{
	$list_enq_nationality[$b]['cntEnquiry'];
	$nationality  = $list_enq_nationality[$b]['country_name'];

    $pieNationality .= "{
        value: '".$list_enq_nationality[$b]['cntEnquiry']."',
        color: '".$colorCode[$b]."',
        highlight: '".$colorCode[$b]."',
        label: '".$nationality."'      }, ";
}
$pieNationality .= " ]; ";


$pieAge = "[ ";
for($g=0;$g<count($list_age_group_enq); $g++)
{
	$list_age_group_enq[$g]['cntEnquiry'];
	$age_group  = $list_age_group_enq[$g]['age_from']." - ".$list_age_group_enq[$g]['age_to'] ;

    $pieAge .= "{
        value: '".$list_age_group_enq[$g]['cntEnquiry']."',
        color: '".$colorCode[$g]."',
        highlight: '".$colorCode[$g]."',
        label: ' Age Group(".$age_group.")'      }, ";
}
$pieAge .= " ]; ";


$pieGender = "[ ";
for($e=0;$e<count($list_enq_gender); $e++)
{
	$list_enq_gender[$e]['cntEnquiry'];
	$gender  = $list_enq_gender[$e]['gender'];

    $pieGender .= "{
        value: '".$list_enq_gender[$e]['cntEnquiry']."',
        color: '".$colorCode[$e]."',
        highlight: '".$colorCode[$e]."',
        label: '".$gender."'      }, ";
}
$pieGender .= " ]; ";



$pieReligion = "[ ";
for($f=0;$f<count($list_enq_religion); $f++)
{
	$list_enq_religion[$f]['cntEnquiry'];
	$religion  = $list_enq_religion[$f]['religion'];

    $pieReligion .= "{
        value: '".$list_enq_religion[$f]['cntEnquiry']."',
        color: '".$colorCode[$f]."',
        highlight: '".$colorCode[$f]."',
        label: '".$religion."'      }, ";
}
$pieReligion .= " ]; ";




*/
?>
<?php if(LAN_SEL=="ar"){ 
		$enquiryLabel  = "الإستفسارات";
		$enquiryOpen   = "فتح";
		$enquiryClosed = "مغلق";
		$enquiryPerformance = "أداء";
		$positionBreadCrumb = 'float:right;';
}else{
		$enquiryLabel  = "Enquiries";
		$enquiryOpen   = "Open";
		$enquiryClosed = "Closed";
		$enquiryPerformance = "Performance";
		$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <?php if(LAN_SEL=="ar"){?> 
       <h1>مرحبا بكم في الأقدار تطبيق المدرسة  </h1>
    <?php }else{?>
    <h1> Welcome to <?=APP_HEAD_SMALL?><small> School Application</small> </h1>
    <?php } ?>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="#" target=""><i class="fa fa-home"></i><?php if(LAN_SEL=="ar"){?>الرئيسية<?php }else{?>Home<?php } ?></a></li>
      <li><?php if(LAN_SEL=="ar"){?>مرحبا<?php }else{?>Welcome<?php } ?></li>
    </ol>
    <!--/BREADCRUMB--> 
    <div style="clear:both"></div>
  </section>
  
  
   
     <!-- start --->
  

  
  
  <section class="content"> 
    <!--WORKING AREA-->	

    <?php /*?><div class="row">
        <div class="col-md-6">
          <!-- AREA CHART -->
          <div class="box box-primary">
            <div class="box-header with-border">
               <?php if(LAN_SEL=="ar"){?> 
                    <h3 class="box-title">  الإستفسارات والردود </h3>
               <?php }else{ ?>
                     <h3 class="box-title">Area Chart - Enquiries vs Replied</h3>
              <?php } ?>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
                <canvas id="areaChart" style="height:250px"></canvas>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- DONUT CHART -->
          <div class="box box-danger">
            <div class="box-header with-border">
               <?php if(LAN_SEL=="ar"){?> 
                    <h3 class="box-title">  الإستفسارات بناء على تصنيفات مختلفة </h3>
               <?php }else{ ?>
                     <h3 class="box-title">Enquiries Based On Different Categories </h3>
              <?php } ?>
              
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>
              </div>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <canvas id="pieChart" style="height:250px"></canvas>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          
          
         

          <!-- DONUT CHART -->
          <div class="box box-info">
            <div class="box-header with-border">
               <?php if(LAN_SEL=="ar"){?> 
                    <h3 class="box-title">  الإستفسارات بناء على تصنيفات مختلفة </h3>
               <?php }else{ ?>
                     <h3 class="box-title">Enquiries Based On Gender </h3>
              <?php } ?>
              
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>
              </div>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
               <div class="chart">
              <canvas id="genderChart" style="height:250px"></canvas>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
           <!-- /.box -->
           
           
           <div class="box box-info">
            <div class="box-header with-border">
               <?php if(LAN_SEL=="ar"){?> 
                    <h3 class="box-title">  الاستفسارات بناء على الدين </h3>
               <?php }else{ ?>
                     <h3 class="box-title">Enquiries Based On Religion </h3>
              <?php } ?>
              
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>
              </div>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
               <div class="chart">
              <canvas id="religionChart" style="height:250px"></canvas>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
           
           
           
           
        </div>
        <!-- /.col (LEFT) -->
        <div class="col-md-6">
          <!-- LINE CHART -->
         <?php
		if(isset($_SESSION['aqdar_smartcare']['tbl_judge_id_sess']))
		{
			if(LAN_SEL=="ar")
				$titleBarChart = "الإستفسارات ضد أداء استشاريون الأسرة" ;
			else
			   $titleBarChart = "Enquiries vs Family Consultants Performance";
		}else{
			if(LAN_SEL=="ar")
				$titleBarChart = "الإستفسارات ضد استشاريون الأسرة";
			else
				$titleBarChart = "Enquiries vs Family Consultants";
		}
	
		 ?>
           <!-- BAR CHART -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title"><?=$titleBarChart?></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>
              </div>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
                <canvas id="barChart" style="height:250px"></canvas>
            </div>
            <!-- /.box-body -->
          </div>
          
          
          
           <!-- DONUT CHART -->
          <div class="box box-info">
            <div class="box-header with-border">
               <?php if(LAN_SEL=="ar"){?> 
                    <h3 class="box-title">  الإستفسارات بناء على تصنيفات مختلفة </h3>
               <?php }else{ ?>
                     <h3 class="box-title">Enquiries Based On Nationality </h3>
              <?php } ?>
              
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>
              </div>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <canvas id="nationalityChart" style="height:250px"></canvas>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          
           <!-- DONUT CHART -->
          <div class="box box-danger">
            <div class="box-header with-border">
               <?php if(LAN_SEL=="ar"){?> 
                    <h3 class="box-title">  الإستفسارات بناء على تصنيفات مختلفة </h3>
               <?php }else{ ?>
                     <h3 class="box-title">Enquiries Based On Age Group </h3>
              <?php } ?>
              
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i> </button>
              </div>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
               <div class="chart">
                  <canvas id="ageChart" style="height:250px"></canvas>
               </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          

        </div>
        <!-- /.col (RIGHT) -->
      </div><?php */?>
    <!--/WORKING AREA--> 
    
  </section>
  <!-- end start --->
  
  <div style="clear:both"></div>
</div>

