<?php
//Init Parameters
$student_group_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}
?>
 
<style>
.txt_en {
	text-align:left;
	padding-left:2px;
}
.txt_ar {
	text-align:right;
	padding-right:2px;	
	direction:rtl;		
}
textarea {
    height: 170px;
    padding-bottom: 6px;
    padding-top: 6px;
    width: 95%;
	font-size: 14px;
	border: 1px solid #ddd;
 }
 select {
   font-size: 14px;
   border: 1px solid #ddd;
 }
</style>
<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/students_group";
			}
		})
	});
	
function confirm_delete_popup() {
		var len = $("input[id='tbl_student_group_id']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more group(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/deleteStudentGroup",
			data: {
				tbl_student_group_id: $("input[id='tbl_student_group_id']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Group(s) deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}	
	function ajax_activate(tbl_student_group_id) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/activateStudentGroup",
			data: {
				tbl_student_group_id: tbl_student_group_id,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Group activated successfully.", 'green')

				$('#act_deact_'+tbl_student_group_id).html('<span style="cursor:pointer" onClick="ajax_deactivate(\''+tbl_student_group_id+'\')" onMouseOver="deactivate_me(this)" onMouseOut="reset_activate(this)" class="label label-success">Active</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_deactivate(tbl_student_group_id) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/deactivateStudentGroup",
			data: {
				tbl_student_group_id: tbl_student_group_id,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Group de-activated successfully.", 'green')
				
				$('#act_deact_'+tbl_student_group_id).html('<span style="cursor:pointer" onClick="ajax_activate(\''+tbl_student_group_id+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	
	function ajax_submit() {
	
		var selectednumbers = "";
        $('.checkboxModule:checked').each(function(i){
          selectednumbers += $(this).val()+"&";
        });		
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/save_student_group",
			data: {
				group_name_en            : $('#group_name_en').val(),
				group_name_ar            : $('#group_name_ar').val(),
				group_for                : $('#group_for').val(),
				tbl_student_id           : selectednumbers,
				tbl_student_group_id     : '<?=$student_group_id_enc?>',
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='X') {
					refresh_page = "N";
					my_alert("Group is already exist, Please try another name.", 'red');
					$("#pre-loader").hide();
				} else if (temp=='N') {
					refresh_page = "N";
					my_alert("Group added failed, Please try again.", 'red');
					$("#pre-loader").hide();
				   
				}else{
					 refresh_page = "Y";
				    my_alert("Group added successfully.", 'green');
				    $("#pre-loader").hide();
				}
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_update() {
		
		var selectednumbers = "";
        $('.checkboxModule:checked').each(function(i){
          selectednumbers += $(this).val()+"&";
        });	
			
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/update_student_group",
			data: {
				group_name_en            : $('#group_name_en').val(),
				group_name_ar            : $('#group_name_ar').val(),
				group_for                : $('#group_for').val(),
				tbl_student_id            : selectednumbers,
				tbl_student_group_id      : $('#student_group_id_enc').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='N') {
					refresh_page = "N";
					my_alert("Group updation failed, Please try again.", 'red');
					$("#pre-loader").hide();
				   
				}else{
					 refresh_page = "Y";
				    my_alert("Group updated successfully.", 'green');
				    $("#pre-loader").hide();
				}
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
</script>
<script language="javascript">
   //add student
   /* || validate_picture() == false*/
	function ajax_validate() {
		if (validate_group_name() == false || validate_students() == false ) 
		{
			return false;
		}
		else{
			ajax_submit();
		}
	}
	
    //edit student
	function ajax_validate_edit() {
		if (validate_group_name() == false || validate_students() == false) 
		{
			return false;
		} 
		else{
			ajax_update();
		}
	} 
	
  /************************************* START MESSAGE VALIDATION *******************************/

   function validate_group_name() {
		var regExp = / /g;
		var str = $("#group_name_en").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Group Name [En] is blank. Please enter group name in English")
			$("#group_name_en").val('');
			$("#group_name_en").focus();
		return false;
		}
		var regExp = / /g;
		var str = $("#group_name_ar").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Group Name [Ar] is blank. Please enter group name in Arabic")
			$("#group_name_ar").val('');
			$("#group_name_ar").focus();
		return false;
		}
		return true;
	
	}
	
	function validate_students() {
		var tbl_class_id = "";
        $('.checkboxClass:checked').each(function(i){
          tbl_class_id += $(this).val()+",";
        });
		
		if (tbl_class_id=='' ) {
			my_alert("Please select Student(s)");
		return false;
		}
	  return true;
	}
	
</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Students Group <small> Management</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Students Group</li>
    </ol>
    <!--/BREADCRUMB--> 
 <!-- <div style=" float:right; padding-right:10px;"> <button onclick="show_topic()" title="Records" type="button" class="btn btn-primary">Forum Topics</button></div> -->
    <div style="clear:both"></div>
  </section>
    <script>
	   function show_topic()
		{
			var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/forum/all_student_forum_topics/";
			window.location.href = url;
		}
    </script>
      <link href="<?=HOST_URL?>/assets/admin/dist/css/jquery-ui.css" rel="stylesheet">
      <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-1.11.1.js"></script>
      <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-ui.js"></script>
      <link href="<?=HOST_URL?>/assets/admin/dist/css/uploadfile.min.css" rel="stylesheet">

  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {
	?>
        <!--Edit-->
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Students Group</h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/students_group"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

            
     <style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url('<?=IMG_PATH?>/delete.jpg');
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
		clear:both;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
	#overlay_container {
		position:relative;
	}
	#overloading {
		background-image:url('<?=IMG_PATH?>/preloader/preloader_2.gif');
		background-repeat:no-repeat;
		background-position:center;
		background-color:#CCC;
		position:absolute;
		left:0px;
		top:0px;
		opacity: 0.3;
		z-index: 10000;
	}
	#div_listing_container {
		display:none;	
	}
	.d_d_text {
		color:#745156;
		font-size:20px;
			
	}
	.ajax-upload-dragdrop {
		margin:auto;
		margin-bottom:10px;
		width:700px !important;
	}
	.ajax-file-upload-statusbar {
		margin:auto;
		margin-top:10px;
	}
	.ajax-file-upload {
		height:31px;
	}
	
	
	 #tabs-1{  
	    overflow-y:scroll; overflow-x:none;
	}

    #tabs-2{
		overflow-y:scroll; overflow-x:none;
	}
				  
  .ui-tabs-active{
		border-color:#efca86  !important;
   }
					 
	.ui-tabs .ui-tabs-nav li {
		float:left;
		font-size: 16px;
        font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif;
  }
  label{
	  display: inline-block;
      font-weight: 700;
  }
  
  .ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button {
    font-family:"Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
    font-size: 14px;
}
  
  .ui-widget{
	 font-size: 16px;
     font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
  }
  .form-control{
	 font-size: 14px; 
  }
</style>         
         <?php
		 	$tbl_student_group_id              = $group_info[0]['tbl_student_group_id'];		
			$group_name_en             	       = $group_info[0]['group_name_en'];
			$group_name_ar             	       = $group_info[0]['group_name_ar'];
			$group_for             	           = $group_info[0]['group_for'];
			$class_array = array(); 
			for($y=0;$y<count($selected_class_list);$y++)
			{
				$class_array[$y] = $selected_class_list[$y]['tbl_class_id'];
			} 
		 ?>       
                
             <div class="box-body">
                    <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
                         <div class="form-group">
                          <label class="col-sm-2 control-label" for="group_name_en">Group Name [En]<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10">
                            <input type="text" placeholder="Group Name [En]" id="group_name_en" name="group_name_en" class="form-control" value="<?=$group_name_en?>">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="group_name_ar">Group Name [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10">
                            <input type="text" placeholder="Group Name [Ar]" id="group_name_ar" name="group_name_ar" class="form-control" value="<?=$group_name_ar?>" dir="rtl">
                          </div>
                        </div>
                        
                         <div class="form-group">
                          <label class="col-sm-2 control-label" for="group_for">Group For<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10">
                           <select name="group_for" id="group_for" class="form-control">
                                     <option value="A" <?php if($group_for=='A'){?> selected="selected" <?php } ?> >All</option>
                                     <option value="F" <?php if($group_for=='F'){?> selected="selected" <?php } ?> >Forum</option>
                                     <option value="M" <?php if($group_for=='M'){?> selected="selected" <?php } ?> >Message</option>
                           </select>
                          </div>
                        </div>
             
                     
                         <div class="form-group" id="all_student">
                        <label class="col-sm-2 control-label" for="tbl_class_id">Class(s)<!--<span style="color:#F30; padding-left:2px;">*</span>--></label>
        
                          <div class="col-sm-10" id="divClass" style="height:200px; overflow-y:scroll;">
                         
                        <?php  if(count($classes_list)>0){ ?>
                           <div style="padding-bottom:10px;background-color:#e8eaeb;" class="col-sm-12"> 
                           <input type="checkbox" value="" id="select_all_class" class="checkboxClass" >&nbsp;Select All</div>
                        <?php  } ?>
                        <?php
                            for ($u=0; $u<count($classes_list); $u++) { 
								$tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
								$class_name             = $classes_list[$u]['class_name'];
								$class_name_ar          = $classes_list[$u]['class_name_ar'];
								$section_name           = $classes_list[$u]['section_name'];
								$section_name_ar        = $classes_list[$u]['section_name_ar'];
								if(in_array($tbl_class_id_u, $class_array, true))
                                    $selClass = "checked";
                                else
                                    $selClass = "";
								
                             ?>
                             <div class="col-sm-4" style="padding-top:10px; padding-bottom:10px; border:1px solid #CCC;"> 
								  <input id="tbl_class_id_<?=$u?>" name="tbl_class_id[]" class="checkboxClass" type="checkbox" value="<?=$tbl_class_id_u?>"  onChange="get_students_ajax()"  <?=$selClass?> />&nbsp;
								   <?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;<?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                             </div>
							<?php  
	                         }
							 ?>
	                        </div> 
                        </div>    
                      
                      
                       <div class="form-group">
                       <label class="col-sm-2 control-label" for="tbl_student_id">Student(s)<span style="color:#F30; padding-left:2px;">*</span></label>
                      
                          <div class"col-sm-10" style="height:400px; overflow-y:scroll;" id="divStudent" >
						 <?php
                         if(count($all_class_student_list)>0){ ?>
                            <div style="padding-bottom:10px;background-color:#e8eaeb;" class="col-sm-12"> <input type="checkbox" value="" id="select_all_students" class="checkboxStudents" >&nbsp;Select All</div>
                       <?php  } ?>
	
    				  <?php 
						for($i=0; $i<count($all_class_student_list); $i++) { 
							
							$tbl_student_id_u = $all_class_student_list[$i]['tbl_student_id'];
							$first_name      = $all_class_student_list[$i]['first_name'];
							$last_name       = $all_class_student_list[$i]['last_name'];
							$class_name      = $all_class_student_list[$i]['class_name'];
							$section_name    = $all_class_student_list[$i]['section_name'];
					
							$first_name_ar 	     = $all_class_student_list[$i]['first_name_ar'];
							$last_name_ar		 = $all_class_student_list[$i]['last_name_ar'];
							$class_name_ar       = $all_class_student_list[$i]['class_name_ar'];
							$section_name_ar     = $all_class_student_list[$i]['section_name_ar'];
							
							$tbl_class_id        = $all_class_student_list[$i]['tbl_class_id'];
							  
							  if(in_array($tbl_student_id_u, $assign_students_list, true))
									   $selStudent = "checked";
									 else
									   $selStudent = "";
								?>
		
		                      <div class="col-sm-4" style="padding-top:10px; padding-bottom:10px; border:1px solid #CCC;"> 
								  <input id="tbl_student_id" name="tbl_student_id" class="checkboxModule" type="checkbox" value="<?=$tbl_student_id_u?>*<?=$all_class_student_list[$i]['tbl_class_id']?>"  <?=$selStudent?> />&nbsp;
								 <?=$first_name?> <?=$last_name?> :: <?=$first_name_ar?> <?=$last_name_ar?><br><?=$class_name?>&nbsp;<?=$section_name?>
                                 [::]<?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                             </div>
						<?php  } ?>
	                  </div>
                       </div>
 
                   <?php /*?>    <div class="form-group">
                          <label class="col-sm-2 control-label" for="tbl_parent_id">Parent(s)<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10" id="divParent" style="height:400px; overflow-y:scroll;">
                        
                        </div>
                     </div><?php */?>
                     
                     
                     
                        
                    
                     
                        <!-- /.box-body -->
                      <div class="box-footer">
                        <button class="btn btn-primary" type="button" onclick="ajax_validate_edit()">Submit</button>
                         <input type="hidden" name="student_group_id_enc" id="student_group_id_enc" value="<?=$tbl_student_group_id?>" />
                        <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                      </div>
                      <!-- /.box-footer -->  
                      
            
           </form>
                </div>
                
    </div>
		        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/message/students_group";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
    
  
 <link href="<?=HOST_URL?>/assets/admin/dist/css/jquery-ui.css" rel="stylesheet">
 <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-1.11.1.js"></script>
 <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-ui.js"></script>
  <script>
  $( function() {
		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
				
				/* var order = $("#tabledivbody").sortable("serialize");
				
				alert(order);
				
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/your/url/here'
				});*/
				
				
				
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	
				
				
				
				
				
			}
	  } );
  
  } );
  </script> 
  
  
  
  <!--File Upload START-->
<link href="<?=HOST_URL?>/assets/admin/dist/css/uploadfile.min.css" rel="stylesheet">
<script>
 $( function() {
    $( "#tabs" ).tabs();
  } );
  
 
</script>
<style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url('<?=IMG_PATH?>/delete.jpg');
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
		clear:both;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
	#overlay_container {
		position:relative;
	}
	#overloading {
		background-image:url('<?=IMG_PATH?>/preloader/preloader_2.gif');
		background-repeat:no-repeat;
		background-position:center;
		background-color:#CCC;
		position:absolute;
		left:0px;
		top:0px;
		opacity: 0.3;
		z-index: 10000;
	}
	#div_listing_container {
		display:none;	
	}
	.d_d_text {
		color:#745156;
		font-size:20px;
			
	}
	.ajax-upload-dragdrop {
		margin:auto;
		margin-bottom:10px;
		width:700px !important;
	}
	.ajax-file-upload-statusbar {
		margin:auto;
		margin-top:10px;
	}
	.ajax-file-upload {
		height:31px;
	}
	
	
	 #tabs-1{  
	    overflow-y:scroll; overflow-x:none;
	}

    #tabs-2{
		overflow-y:scroll; overflow-x:none;
	}
				  
  .ui-tabs-active{
		border-color:#efca86  !important;
   }
					 
	.ui-tabs .ui-tabs-nav li {
		float:left;
		font-size: 16px;
        font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif;
  }
  label{
	  display: inline-block;
      font-weight: 700;
  }
  
  .ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button {
    font-family:"Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
    font-size: 14px;
}
  
  .ui-widget{
	 font-size: 16px;
     font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
  }
  .form-control{
	 font-size: 14px; 
  }
</style>
 
  
    
                    <div id="mid1" class="box box-success">
                        <div class="box-header">
                          <div style="width:20%; float:left;">
                          <h3 class="box-title">SEARCH</h3>
                          </div>
                          <div style="width:60%; float:left;"> 
                    	     <div class="col-sm-5">
                             <?php //echo $category_parent_list ?>
                   		      </div>
                             
                          </div>
                        </div>  
                     </div>     
    
            <!--Listing-->
                    <div id="mid1_list" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Students Group</h3>
                          <div class="box-tools">
                            <?php if (count($rs_all_groups)>0) { echo $paging_string;}?>	
                            <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
                            <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>
                          </div>
                        </div>
                        
                        <div class="box-body">
                     <!--   <div style="color:#030; font-weight:bold;">You can sort students by using drag and drop of rows </div>-->
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle"><input id="select_all" type="checkbox" value="" /></th>
                              <!--<th width="10%" align="center" valign="middle">Sl No.</th>-->
                              <th width="30%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y">Group Name <?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?><div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>
                              <th width="30%" align="center" valign="middle">Students</th>
                              <th width="15%" align="center" valign="middle">Group For</th>
                              <th width="10%" align="center" valign="middle">Date</th>
                              <th width="5%" align="center" valign="middle">Status</th>
                              <th width="5%" align="center" valign="middle">Action</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                                for ($i=0; $i<count($rs_all_groups); $i++) { 
                                    $id                         = $rs_all_groups[$i]['id'];
                                    $tbl_student_group_id       = $rs_all_groups[$i]['tbl_student_group_id'];
                                    $group_name_en              = $rs_all_groups[$i]['group_name_en'];
									$group_name_ar              = $rs_all_groups[$i]['group_name_ar'];
									$group_for                  = $rs_all_groups[$i]['group_for'];
									$students_list	            = $rs_all_groups[$i]['students_list'];
									$cntStudents	             = $rs_all_groups[$i]['cntStudents'];
                                    $added_date                 = $rs_all_groups[$i]['added_date'];
                                    $is_active                  = $rs_all_groups[$i]['is_active'];
                                    $added_date = date('m-d-Y',strtotime($added_date));
									
									if($group_for=="M")
										$group_for = " Message Group";
									else if($group_for=="M")
										$group_for = " Forum Group";
									else
										$group_for = " All";
									
									
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_student_group_id?>" >
                              <td align="left" valign="middle">
                              <span style="float:left;">
                              <input id="tbl_student_group_id" name="tbl_student_group_id" class="checkbox" type="checkbox" value="<?=$tbl_student_group_id?>" />
                              </span>
                              
                             <?php /*?> <span style="float:left;">&nbsp;
                              <?php if($i<>0){ ?> <i class="fa fa-arrow-up"  style="color:#3c8dbc; cursor:pointer;"  aria-hidden="true" title="Sorting - Drag & Drop To Up"></i> &nbsp; <?php } ?>
                               <?php if($i<> count($rs_all_categories)-1){ ?> <i class="fa fa-arrow-down" style="color:#3c8dbc;cursor:pointer;" aria-hidden="true" title="Sorting - Drag & Drop To Down"></i> <?php } ?>
                              </span><?php */?>
                              </td>
                             <!-- <td align="left" valign="middle"><?=$offset+$i+1?></td>-->
                              <td align="left" valign="middle">
                              <span style='float:left;'><?=$group_name_en?> </span><span style='float:right;'><?=$group_name_ar?></span>
                              </td>
                              <td align="center" valign="middle" style="text-decoration:underline; cursor:pointer;"> <a onclick="openStudentDiv('std_<?=$tbl_student_group_id?>');" ><?=$cntStudents?>&nbsp;Students</a> <br />
                              <div id="std_<?=$tbl_student_group_id?>" style="display:none;">
                              <?php
                              for($t=0;$t<count($students_list);$t++)
									{
						echo "<span style='float:left;'>".$students_list[$t]['first_name']." ".$students_list[$t]['last_name']."</span><span style='float:right;'>".$students_list[$t]['first_name_ar']." ".$students_list[$t]['last_name_ar']."</span><br>";
									}
							  ?>
                              </div>
                              </td>
                              <td align="left" valign="middle"><?=$group_for?></td>
                              <td align="left" valign="middle"><?=$added_date?></td>
                              <td align="left" valign="middle">
                                <div id="act_deact_<?=$tbl_student_group_id?>" >
                                <?php if (trim($is_active) == "Y") { ?>
                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_student_group_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                <?php } else { ?>
                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_student_group_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                <?php } ?>
                                </div>
                              </td>
                              <td align="left" valign="middle">
                                <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/edit_student_group/tbl_student_group_id/<?=$tbl_student_group_id ?>"><button class="btn bg-purple fa fa-pencil" type="button" title="Edit"></button></a>
                              </td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="10" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_all_groups)<=0) {
                            ?>
                            <tr>
                              <td colspan="10" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no group available. Click on the + button to add group.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div> 
                    
            <script>
			function openStudentDiv(divId)
			{
			   if($("#"+divId).is(":visible"))
			   {
				    $("#"+divId).hide();
			   }else{
				   $("#"+divId).show();
			   }
			}
			</script>       
            <!--/Listing-->
    
            <!--Add or Create-->
            <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Students Group</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
   	            <div class="box-body">
                    <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
                            
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="group_name_en">Group Name [En]<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10">
                            <input type="text" placeholder="Group Name [En]" id="group_name_en" name="group_name_en" class="form-control" value="">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="group_name_ar">Group Name [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10">
                            <input type="text" placeholder="Group Name [Ar]" id="group_name_ar" name="group_name_ar" class="form-control" value="" dir="rtl">
                          </div>
                        </div>
                  
                         <div class="form-group">
                          <label class="col-sm-2 control-label" for="group_for">Group For<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10">
                           <select name="group_for" id="group_for" class="form-control">
                                     <option value="A" selected="selected" >All</option>
                                     <option value="F">Forum</option>
                                     <option value="M">Message</option>
                           </select>
                          </div>
                        </div>
                  
                  
                      
                      <div class="form-group" id="all_student">
                        <label class="col-sm-2 control-label" for="tbl_class_id">Class(s)<!--<span style="color:#F30; padding-left:2px;">*</span>--></label>
        
                          <div class="col-sm-10" id="divClass" style="height:200px; overflow-y:scroll;">
                          <div class"col-sm-10" >
	 
                        <?php  if(count($classes_list)>0){ ?>
                           <div style="padding-bottom:10px;background-color:#e8eaeb;" class="col-sm-12"> 
                           <input type="checkbox" value="" id="select_all_class" class="checkboxClass" >&nbsp;Select All</div>
                        <?php  } ?>
                        <?php
                            for ($u=0; $u<count($classes_list); $u++) { 
								$tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
								$class_name             = $classes_list[$u]['class_name'];
								$class_name_ar          = $classes_list[$u]['class_name_ar'];
								$section_name           = $classes_list[$u]['section_name'];
								$section_name_ar        = $classes_list[$u]['section_name_ar'];
                             ?>
                             <div class="col-sm-4" style="padding-top:10px; padding-bottom:10px; border:1px solid #CCC;"> 
								  <input id="tbl_class_id_<?=$u?>" name="tbl_class_id[]" class="checkboxClass" type="checkbox" value="<?=$tbl_class_id_u?>"  onChange="get_students_ajax()" />&nbsp;
								   <?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;<?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                             </div>
							<?php  
	                         }
							 ?>
	                          </div> 
                        </div>
                        </div>    
                      
                      
               
                       <div class="form-group">
                          <label class="col-sm-2 control-label" for="tbl_student_id">Students(s)<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10" id="divStudent" style="height:400px; border:1px solid #999; overflow-y:scroll;">
                        
                        </div>
                     </div>
                     
                     
                     
                        
                      
                     
                        <!-- /.box-body -->
                      <div class="box-footer">
                        <button class="btn btn-primary" type="button" onclick="ajax_validate()">Submit</button>
                        <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                      </div>
                      <!-- /.box-footer -->  
                      
            
           </form>
                </div>
           </div>
                
  
            <!--/Add or Create-->
                
        <!--/Admin Category Management-->

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>


<script src="<?=HOST_URL?>/assets/admin/dist/js/jquery.uploadfile.min.js"></script>
<script language="javascript">

var item_id = "<?=$student_id_enc?>";//Primary Key for a Form. 

function set_item_id(obj) {
	item_id = obj.value;
	get_files();	
}

$(document).ready(function() {
	var uploadObj = $("#advancedUpload").uploadFile({
		url:"<?=HOST_URL?>/file_mgmt/upload_the_file",
		multiple:true,
		autoSubmit:true,
		maxFileSize:130000,
		fileName:"myfile",
		formData: {"module_name":"teacher"},
		dynamicFormData: function() {
			var data = { item_id:item_id}
			return data;
		},
		showStatusAfterSuccess:false,
		dragDropStr: "<span class='d_d_text'>Optionally Drag and Drop the File to Upload.</span>",
		abortStr:"Abourt",
		cancelStr:"Cancel",
		doneStr:"Done",
		multiDragErrorStr: "Multi Drag Error.",
		extErrorStr:"Extention Error:",
		sizeErrorStr:"Max Size Error:",
		uploadErrorStr:"Upload Error",
		onSelect:function(files) {
 		},
		onSubmit:function(files) {
 		},
		onSuccess:function(files, data, xhr) {
			if (data == "error") {
				alert("Error uploading file. Please try again.");
				return;
			}
			var obj = JSON.parse(data);
			var tbl_uploads_id = obj.tbl_uploads_id;
			var file_name_updated = obj.file_name_updated;
			
			//alert("tbl_uploads_id: "+tbl_uploads_id)
			//alert("file_name_updated: "+file_name_updated)
			add_uploaded_item(tbl_uploads_id, file_name_updated);
		},
		afterUploadAll:function() {
 		},
		onError: function(files, status, errMsg) {
 		}
	});

	$("#startUpload").click(function() {
		uploadObj.startUpload();
	});
	
	try { 
		$('input[type=file]').click();
	} catch(e) {
		alert(e)
	}
});

//Function called when file is uploaded
function add_uploaded_item(tbl_uploads_id, file_name_updated) {
	var str = "<div id='"+tbl_uploads_id+"' class='box-header with-border'> <div class='box-title'><img src='<?=IMG_PATH_TEACHER?>/"+file_name_updated+"' /></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_img_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
		
	$("#div_listing_container").show();
	$("#div_listing_container").append(str);
	$(".ajax-upload-dragdrop").hide();//Hide the upload button
return;
}

function confirm_delete_img_popup(tbl_uploads_id) {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		$('#'+tbl_uploads_id).hide();	
		$(".ajax-upload-dragdrop").show();
		
		var url_str = "<?=HOST_URL?>/file_mgmt/delete_file";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					tbl_uploads_id: tbl_uploads_id
				},
			success: function(data) {
				$("#pre-loader").hide();
			}
		});	
	} else {
		$("#pre-loader").hide();		
	}
}

function get_files() {
	var url_str = "<?=HOST_URL?>/misc/get_files.php";
	
	$.ajax({
		type: "POST",
		url: url_str,
		data: {
				module_name: "teacher",
				show_del: "Y",
				item_id: item_id//global variable
			},
		success: function(data) {
			$('#div_listing_container').show();
			$('#div_listing_container').html(data)
			
		}
	});	
}
</script>
<script>
/*var connectivity_msg = "Connection timed out. Please try again.";
var connectivity_timeout_time = 10000;*/
var host = '<?=HOST_URL?>';
function get_students_ajax() {
		//show_loading();
		var tbl_class_id = "";
        $('.checkboxClass:checked').each(function(i){
          tbl_class_id += $(this).val()+",";
        });
		//alert(tbl_class_id);
		//return;
		var xmlHttp, rnd, url, search_param, ajax_timer;
		rnd = Math.floor(Math.random()*11);
		try{		
			xmlHttp = new XMLHttpRequest(); 
		}catch(e) {
			try{
				xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
			}catch(e) {
				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
				hide_loading();
			}
		}

		//AJAX response
		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
				//ajax_timer.stop();
				var data = xmlHttp.responseText;
				$("#divStudent").html(data);
				/*$("#tbl_parent_dropdown").multiselect('refresh');*/
				return;
			}
		}

		/*ajax_timer = $.timer(function() {
			xmlHttp.abort();
			alert(connectivity_msg);
			ajax_timer.stop();
		},connectivity_timeout_time,true);*/

		//Sending AJAX request
		url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/students_against_classes/tbl_class_id/"+tbl_class_id+"/rnd/"+rnd
		xmlHttp.open("POST",url,true);
		xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlHttp.send("rnd="+rnd);
	}
	
	
       $('#select_all_class').on('click',function(){
			if(this.checked){
				$('.checkboxClass').each(function(){
					this.checked = true;
				});
				
			}else{
				 $('.checkboxClass').each(function(){
					this.checked = false;
				});
			}
			
			get_students_ajax();
			
			if(this.checked){
				$('.checkboxModule').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkboxModule').each(function(){
					this.checked = false;
				});
			}
			
			
		});
</script>
	


<!--File Upload END-->
        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >
function search_data() {
		var tbl_category_id = $("#tbl_category_id").val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/all_categories/";
		if(tbl_category_id !='')
			url += "tbl_category_id/"+tbl_category_id+"/";
		
			url += "offset/0/";
		window.location.href = url;
		<?php /*?>window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/enquiry/all_enquiries/is_not_replied/"+is_not_replied+"/tbl_court_id/"+tbl_court_id+"/tbl_category_id/"+tbl_category_id;<?php */?>
	}
</script>