 <div id="report_list"  >
      
    <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle"><input id="select_all" type="checkbox" value="" /></th>
                              <!--<th width="10%" align="center" valign="middle">Sl No.</th>-->
                              <th width="30%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y">Report Name[En] <?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?><div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>
                              <th width="30%" align="center" valign="middle">Report Name [Ar]</th>
                              <th width="20%" align="center" valign="middle">Added Date</th>
                              <th width="5%" align="center" valign="middle">Status</th>
                              <th width="5%" align="center" valign="middle">Action</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                                for ($i=0; $i<count($progress_reports_student); $i++) { 
                                    $id = $rs_all_students[$i]['id'];
									$tbl_progress_report_id    = $progress_reports_student[$i]['tbl_progress_report_id'];
                                    $tbl_progress_id    = $progress_reports_student[$i]['tbl_progress_id'];
                                    $added_date         = $progress_reports_student[$i]['reported_date'];
                                    $is_active          = $progress_reports_student[$i]['is_active'];
									$report_name        = $progress_reports_student[$i]['report_name'];
                                    $report_name_ar     = $progress_reports_student[$i]['report_name_ar'];
									$tbl_class_id       = $progress_reports_student[$i]['tbl_class_id'];
									$tbl_student_id     = $progress_reports_student[$i]['tbl_student_id'];
									
                                    $added_date = date('m-d-Y',strtotime($added_date));
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_teacher_id?>" >
                              <td align="left" valign="middle">
                              <span style="float:left;">
                              <input id="tbl_progress_id" name="tbl_progress_id" class="checkbox" type="checkbox" value="<?=$tbl_progress_id?>" />
                              </span>
                              </td>
                             <!-- <td align="left" valign="middle"><?=$offset+$i+1?></td>-->
                              <td align="left" valign="middle">
                              <div class="txt_en"><span style="float:left;"> <a style="cursor:pointer;" onclick="popup_progress_report('<?=$tbl_progress_report_id?>','<?=$tbl_progress_id?>','<?=$tbl_school_id?>','<?=$tbl_student_id?>','<?=$tbl_class_id?>')" ><?=$report_name?></a></span>
                              </div>
                              </td>
                             <?php /*?> <td align="left" valign="middle"> 
                              <div class="txt_en"><?=$class_name?>&nbsp;<?=$section_name?></div>
                              <div class="txt_ar"><?=$class_name_ar?>&nbsp;<?=$section_name_ar?></div></td><?php */?>
                              <td align="left" valign="middle">  <div class="txt_ar"><a style="cursor:pointer;" onclick="popup_progress_report('<?=$tbl_progress_report_id?>','<?=$tbl_progress_id?>','<?=$tbl_school_id?>','<?=$tbl_student_id?>','<?=$tbl_class_id?>')" ><?=$report_name_ar?></a></div></td>
                              <td align="left" valign="middle"><?=$added_date?></td>
                              <td align="left" valign="middle">
                                <div id="act_deact_<?=$tbl_teacher_id?>">
                                <?php if (trim($is_active) == "Y") { ?>
                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_teacher_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                <?php } else { ?>
                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_teacher_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                <?php } ?>
                                </div>
                              </td>
                              <td align="left" valign="middle">
                                <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/edit_teacher/teacher_id_enc/<?=$tbl_teacher_id?>"><button class="btn bg-purple fa fa-pencil" type="button" title="Edit"></button></a>
                              </td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="10" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if ($total_progress_reports_student<=0) {
                            ?>
                            <tr>
                              <td colspan="10" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no progress reports available. Click on the + button to create one.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>   
       
        
  </div>


      <div class="box-body" id="add_report" style="display:none;">
       
       
                  <div class="form-group">
                     <label class="col-sm-2 control-label" for="tbl_parenting_school_cat_id">Report<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-4">
                                 <select name="tbl_progress_report_id" id="tbl_progress_report_id" class="form-control"  >
                                 <option value="">Select Report</option>
                              <?php
                                    for ($u=0; $u<count($rs_progress_report); $u++) { 
                                        $tbl_progress_report_id   	= $rs_progress_report[$u]['tbl_progress_report_id'];
                                        $report_name         		= $rs_progress_report[$u]['report_name'];
                                        $report_name_ar         	= $rs_progress_report[$u]['report_name_ar'];
                                  ?>
                                      <option value="<?=$tbl_progress_report_id?>" >
                                      <?=$report_name?>&nbsp;[::]&nbsp;
                                    <?=$report_name_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>
                    </div>
                     <label class="col-sm-1 control-label" for="tbl_progress_report_id" style="font-size:12px; text-decoration:underline; cursor:pointer; color:#030;" ><span style="padding-left:2px;" onclick="addNewCategory();">+&nbsp;Add New Report</span></label>
                      <div style="clear:both;"></div>
                    </div>
                   
                    
                    <div class="form-group" id="divCategory" style="display:none;" >
                      <label class="col-sm-2 control-label" for="report_name">Report Name [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-4">
                        <input type="text" placeholder="Report Name [En]" id="report_name" name="report_name" class="form-control"  >
                      </div> 
                      
                      <label class="col-sm-1 control-label" for="report_name_ar">Report Name [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-4">
                        <input type="text" placeholder="Report Name [Ar]" id="report_name_ar" name="report_name_ar" class="form-control" dir="rtl"  >
                      </div>
                      
                    </div>
                     
  
        <table width="100%" class="table table-bordered table-striped">
          <thead>
            <tr>
                <th width="50%" align="center" valign="middle">Subject</th>
                <th width="50%" align="center" valign="middle">Mark</th>
                <th width="50%" align="center" valign="middle">Total Mark</th>
            </tr> 
          </thead>
          <tbody>
         

          <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		  <script src="<?=HOST_URL?>/js/jquery-ui.js"></script>  


  
<?php	
    $subject_ids  = "";
	for($i=0; $i<count($rs_subject); $i++) {
		 $id                = $rs_subject[$i]['id'];
		 $tbl_subject_id    = $rs_subject[$i]['tbl_subject_id'];
		 $subject_ids      .=  $tbl_subject_id."||";
?>		
         
         <tr>
		  <td>
          <div><?=$rs_subject[$i]["subject"]?><span style="float:right;"><?=$rs_subject[$i]["subject_ar"]?></span> </div>
         </td>
         <td >
           <input name="mark_<?=$tbl_subject_id?>" id="mark_<?=$tbl_subject_id?>" value="" size="2" maxlength="3" type="text" class="form-control"  style="width:200px;" placeholder="Enter Mark"/>
          </td>
           <td >
           <input name="total_<?=$tbl_subject_id?>" id="total_<?=$tbl_subject_id?>" size="2" maxlength="3" type="text" class="form-control" style="width:200px;" placeholder="Enter Total Mark"  value="100" />
          </td>
         </tr>
<?php
	}
?>

<tr>
<td colspan="3" >
<input name="subject_ids" id="subject_ids" type="hidden" value="<?=$subject_ids?>"/>
<button class="btn btn-primary" type="button" onclick="add_student_progress_report('<?=$tbl_student_id?>', '<?=$tbl_class_id?>',  '<?=$tbl_school_id?>', '<?=$tbl_teacher_id?>' )">SAVE</button>
<button class="btn btn-primary" type="button" onclick="get_progress_report_form_refresh()">CANCEL</button>

</td></tr>
 </tbody>
          <tfoot>
          </tfoot>
        </table>
        
        
  </div>
        
<script>
	 
function addNewCategory()
	{
		// divCategory
		if($('#divCategory').is(':visible'))
		{
			$("#divCategory").hide();
		}else{
			$("#divCategory").show();
		}
		 
	 }
function add_student_progress_report(tbl_student_id,tbl_class_id,tbl_school_id,tbl_teacher_id) {
		
		var tbl_progress_report_id =  $("#tbl_progress_report_id").val();
		var report_name            =  $("#report_name").val();
		var report_name_ar         =  $("#report_name_ar").val();
		var subject_ids 		   =  $("#subject_ids").val();
		var id_data     		=  "";
		var sub_mark_data       =  "";
		var tot_mark_data       =  "";
		var cnt                 =  "";
		
		
		if(report_name== '' && tbl_progress_report_id=="")
		{
			alert("Select report name.");
			return false;
		}
		
		
		var id_array            =  new Array;
		if(subject_ids != '')
		{
			var id_array    = subject_ids.split('||');
			for(k = 0; k < id_array.length; k++)
			{
				var ids          = id_array[k];
				if(ids!='')
				{
					var subMark  	= $("#mark_"+ids).val();
					var totMark  	= $("#total_"+ids).val();
					if(subMark != '' && totMark != '' )
					{
						id_data 		= id_data+ids+"||";
						sub_mark_data 	= sub_mark_data+subMark+"||";
						tot_mark_data 	= tot_mark_data+totMark+"||";
						cnt = 1;
						
					}
				}
			
			}
			
		}
		
		
		if(cnt!= '1')
		{
			alert("Please enter student marks and total marks.");
			return false;
		}else{
			
			$("#pre-loader").show();
			var url_str = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/save_student_progress_report";
			$.ajax({
				type: "POST",
				url: url_str,
				data: {
					is_admin: "Y",
					tbl_progress_report_id: tbl_progress_report_id,
					report_name: report_name,
					report_name_ar: report_name_ar,
					id_data: id_data,
					sub_mark_data: sub_mark_data,
					tot_mark_data: tot_mark_data,
					tbl_student_id: tbl_student_id,
					tbl_class_id: tbl_class_id,
					tbl_teacher_id: tbl_teacher_id,
					tbl_school_id: tbl_school_id
				},
				success: function(result_data) {
					if($.trim(result_data)=="E")
					{
						 alert("Progress report already exist.");
						$("#pre-loader").hide();
					}else if($.trim(result_data)=="N")
					{
						 alert("Progress report added failed, Please try again.");
						$("#pre-loader").hide();
					}else{
						alert("Progress report added successfully.");
						$("#pre-loader").hide();
					}
				}
			});
		}
	}
	
	
	
</script>