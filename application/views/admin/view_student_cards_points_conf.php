<?php
//Init Parameters
$card_point_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}
?>
 

<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/student_cards_points_conf";
			}
		})
	});
	
function confirm_delete_popup() {
		var len = $("input[id='card_point_id_enc']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more card points(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/deleteStudentCardPoint",
			data: {
				card_point_id_enc: $("input[id='card_point_id_enc']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Student card point setting(s) deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}	

	function is_exist() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/is_exist_student_card_point",
			data: {
				card_point_id_enc: "<?=$card_point_id_enc?>",
				range_from: $('#range_from').val(),
				range_to: $('#range_to').val(),
				is_ajax: true
			},
			success: function(data) {
				
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
					refresh_page = "N";
					my_alert("Student card point settings already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_create();
				}
			
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	
	
	function ajax_create() {
	
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/add_student_card_point",
			data: {
				card_point_id_enc: "<?=$card_point_id_enc?>",
				total_points: $('#total_points').val(),
				range_from: $('#range_from').val(),
				range_to: $('#range_to').val(),
				card_point: Math.abs($('#card_point').val()),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
				refresh_page = "Y";
				    my_alert("Student card point settings added successfully.", 'green');
				    $("#pre-loader").hide();
				}else if (temp=='X') {
					refresh_page = "N";
					my_alert("Student card point settings already exist.", 'red');
					$("#pre-loader").hide();
				}else{
					refresh_page = "N";
					my_alert("Student card point settings added failed, Please try again.", 'red');
					$("#pre-loader").hide();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	
</script>
<script language="javascript">
	
	
	function ajax_validate() {
		if(isTotalPoints() == false ||  isRangeFrom()== false  || isRangeTo() == false || isCardPoints() == false) {
				return false;
		} else {
			is_exist();
		}
	}

	function isTotalPoints() {
		var regExp = / /g;
		var str = $('#total_points').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Total points is blank. Please enter total points.");
			$('#total_points').focus();
			return false;
		}
		if (isNaN(str)) {
			my_alert("Total points should be number. Please enter valid total points.");
			$('#total_points').val('');
			$('#total_points').focus();
			return false;
		}
		return true;
	}
     
	 function isRangeFrom() {
		var regExp = / /g;
		var str = $('#range_from').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Range From is blank. Please enter Range From.");
			$('#range_from').focus();
			return false;
		}
		if (isNaN(str)) {
			my_alert("Range From should be number. Please enter valid Range From.");
			$('#range_from').val('');
			$('#range_from').focus();
			return false;
		}
		return true;
	}

	
	 function isRangeTo() {
		var regExp = / /g;
		var str = $('#range_to').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Range To is blank. Please enter Range To.");
			$('#range_to').focus();
			return false;
		}
		if (isNaN(str)) {
			my_alert("Range To should be number. Please enter valid Range To.");
			$('#range_to').val('');
			$('#range_to').focus();
			return false;
		}
		
		if(Math.abs($('#range_to').val())<= Math.abs($('#range_from').val()))
		{
			my_alert("Range To should be greater than Range From.");
			$('#range_to').val('');
			$('#range_to').focus();
			return false;
			
		}
		
		return true;
	}

	 function isCardPoints() {
		var regExp = / /g;
		var str = $('#card_point').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Card Point is blank. Please enter Card Point.");
			$('#card_point').focus();
			return false;
		}
		if (isNaN(str)) {
			my_alert("Card Point should be number. Please enter valid Card Point.");
			$('#card_point').val('');
			$('#card_point').focus();
			return false;
		}
		return true;
	}

</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Students Cards Points <small> Management</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Students Cards Points Configuration</li>
    </ol>
    <!--/BREADCRUMB--> 
     <div style=" float:right; padding-right:10px;"> <button onclick="show_cards()" title="Student Cards" type="button" class="btn btn-primary">Student Cards</button></div> 
    <div style="clear:both"></div>
  </section>
  
  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {

			$tbl_card_category_id       = $card_obj[0]['tbl_card_category_id'];
			$category_name_en           = $card_obj[0]['category_name_en'];
			$category_name_ar           = $card_obj[0]['category_name_ar'];
			$card_point                 = $card_obj[0]['card_point'];
			$school_type_ids            = $card_obj[0]['tbl_school_type_id'];
			$is_active                  = $card_obj[0]['is_active'];
			
			$tbl_school_type_id         = explode("||",$school_type_ids);
			
		/*	
			for($y=0;$y<count($school_type_id);$y++)
			{
				$tbl_school_type_id[$y] = $school_type_id[$y]['tbl_class_id'];
			} 
			
			
			print_r($tbl_school_type_id);*/
			
	?>
        <!--Edit-->
        
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Student Card</h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/student_cards_conf"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_edit" id="frm_listing" class="form-horizontal" method="post">
                 <div class="box-body">
                  
                   
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="category_name_en">Card Name [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Card Name [En]" id="category_name_en" name="category_name_en" class="form-control" value="<?=$category_name_en?>">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="category_name_ar">Card Name [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Card Name [Ar]" id="category_name_ar" name="category_name_ar" class="form-control" dir="rtl" value="<?=$category_name_ar?>">
                      </div>
                    </div>
                     <!--<div class="form-group">
                      <label class="col-sm-2 control-label" for="card_point">Points ( + or - sign with point) </label>
    
                      <div class="col-sm-10">
                        <input type="hidden" placeholder="Points ( + or - sign with point)" id="card_point" name="card_point" class="form-control" value="-1" >
                     </div>
                    </div>-->
                    <div class="form-group">
                     <label class="col-sm-2 control-label" for="tbl_school_type_id">Category<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-10" id="tbl_school_type_id"> 
                         <?php
                                    for ($u=0; $u<count($school_types); $u++) { 
                                        $tbl_school_type_id_u   = $school_types[$u]['tbl_school_type_id'];
                                        $school_type            = $school_types[$u]['school_type'];
                                        $school_type_ar         = $school_types[$u]['school_type_ar'];   
										
										if(in_array($tbl_school_type_id_u, $tbl_school_type_id, true)){
                                               $selClass = "checked";
										}else{
                                               $selClass = "";
										}
										
										
										?>  
                                       <input class="chkBox" type="checkbox" value="<?=$tbl_school_type_id_u?>" name="tbl_school_type_id" <?=$selClass?> >&nbsp;<?=$school_type?>&nbsp;[::]<?=$school_type_ar?>&nbsp;&nbsp;
                       
                        
                       <?php } ?>  
                       
                      <?php if(in_array('general', $tbl_school_type_id, true)){
                                               $selClass = "checked";
										}else{
                                               $selClass = "";
											   
										}  ?>
                       <input class="chkBox" type="checkbox" value="general" name="tbl_school_type_id" <?=$selClass?> >&nbsp;General&nbsp;[::]جنرال لواء&nbsp;&nbsp;       
                   </div>
                   </div>
                    
                    
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate_edit()">Save Changes</button>
                    <input type="hidden" name="card_category_id_enc" id="card_category_id_enc" value="<?=$tbl_card_category_id?>" />
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
		        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/student/student_cards_points_conf";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
    
  
 <link href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" rel="stylesheet">
 <script src="http://code.jquery.com/jquery-1.11.1.js"></script>
 <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
  <script>
  $( function() {
		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
				
				/* var order = $("#tabledivbody").sortable("serialize");
				
				alert(order);
				
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/your/url/here'
				});*/
				
				
				
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	
				
				
				
				
				
			}
	  } );
  
  } );
  
   function show_cards()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/student_cards_conf/";
		window.location.href = url;
	}
	
  </script> 
 
    
                    <div id="mid1" class="box box-success">
                        <div class="box-header">
                          <div style="width:20%; float:left;">
                          <h3 class="box-title">SEARCH</h3>
                          </div>
                          <div style="width:60%; float:left;"> 
                    	     <div class="col-sm-5">
                             <?php //echo $category_parent_list ?>
                   		      </div>
                             
                          </div>
                        </div>  
                     </div>     
    
            <!--Listing-->
                    <div id="mid1_list" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Student Cards Points Configuration</h3>
                          <div class="box-tools">
                            <?php if (count($rs_student_cards_points)>0) { echo $paging_string;}?>	
                            <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
                            <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>
                          </div>
                        </div>
                        
                        <div class="box-body">
                     <!--   <div style="color:#030; font-weight:bold;">You can sort categories by using drag and drop of rows </div>-->
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle"><input id="select_all" type="checkbox" value="" /></th>
                              <!--<th width="10%" align="center" valign="middle">Sl No.</th>-->
                              <th width="25%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y">Range From <?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?><div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>
                              <th width="25%" align="center" valign="middle">Range To</th>
                              <th width="15%" align="center" valign="middle">Point</th>
                             <!-- <th width="10%" align="center" valign="middle">Status</th>
                              <th width="10%" align="center" valign="middle">Action</th>-->
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                                for ($i=0; $i<count($rs_student_cards_points); $i++) { 
                                    $id                    = $rs_student_cards_points[$i]['id'];
                                    $tbl_card_point_id     = $rs_student_cards_points[$i]['tbl_card_point_id'];
                                    $range_from            = $rs_student_cards_points[$i]['range_from'];
                                    $range_to              = $rs_student_cards_points[$i]['range_to'];
                                    $card_point            = $rs_student_cards_points[$i]['card_point'];
									$added_date            = $rs_student_cards_points[$i]['added_date'];
                                    $is_active             = $rs_student_cards_points[$i]['is_active'];
									$classTypes            = $rs_student_cards_points[$i]['classTypes'];
									
                                    
                                    $title_en = ucfirst($title_en);
                                    $added_date = date('m-d-Y',strtotime($added_date));
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_card_point_id?>" >
                              <td align="left" valign="middle">
                              <span style="float:left;">
                              <input id="card_point_id_enc" name="card_point_id_enc" class="checkbox" type="checkbox" value="<?=$tbl_card_point_id?>" />
                              </span>
                              
                             <?php /*?> <span style="float:left;">&nbsp;
                              <?php if($i<>0){ ?> <i class="fa fa-arrow-up"  style="color:#3c8dbc; cursor:pointer;"  aria-hidden="true" title="Sorting - Drag & Drop To Up"></i> &nbsp; <?php } ?>
                               <?php if($i<> count($rs_all_categories)-1){ ?> <i class="fa fa-arrow-down" style="color:#3c8dbc;cursor:pointer;" aria-hidden="true" title="Sorting - Drag & Drop To Down"></i> <?php } ?>
                              </span><?php */?>
                              </td>
                             <!-- <td align="left" valign="middle"><?=$offset+$i+1?></td>-->
                              <td align="left" valign="middle"><?=$range_from?></td>
                              <td align="left" valign="middle"><?=$range_to?></td>
                              <td align="left" valign="middle"><?=$card_point?></td>
                             <?php /*?> <td align="left" valign="middle">
                                <div id="act_deact_<?=$tbl_card_point_id?>">
                                <?php if (trim($is_active) == "Y") { ?>
                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_card_point_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                <?php } else { ?>
                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_card_point_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                <?php } ?>
                                </div>
                              </td>
                              <td align="left" valign="middle">
                                <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/edit_student_card/card_category_id_enc/<?=$tbl_card_point_id?>"><button class="btn bg-purple fa fa-pencil" type="button" title="Edit"></button></a>
                              </td><?php */?>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="8" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_student_cards_points)<=0) {
                            ?>
                            <tr>
                              <td colspan="8" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no card points settings available. Click on the + button to create one.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
             <?php if($totalPoint==""){
					      $textReadonly = "";
				 }else{
					  $textReadonly = "readonly";
				 }

				 ?>

            <!--Add or Create-->
              <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Student Card Points</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                  
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="total_points">Total Points<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Total Points" id="total_points" name="total_points" class="form-control"  value="<?=$totalPoint?>"  <?=$textReadonly?> >
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="range_from">Range From<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Range From" id="range_from" name="range_from" class="form-control">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="range_to">Range To<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Range To" id="range_to" name="range_to" class="form-control">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="card_point">Point (-ve value)<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                       <span style="position:absolute; padding-left:20px; padding-top:5px;"> <strong>-</strong></span> <input type="text" placeholder="" id="card_point" name="card_point" class="form-control" value="" style="padding-left:30px;" >
                    </div>
                    </div>
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate()">Submit</button>
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
            <!--/Add or Create-->
                
        <!--/Admin Category Management-->

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>

        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >

function search_data() {
		var tbl_category_id = $("#tbl_category_id").val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/all_categories/";
		if(tbl_category_id !='')
			url += "tbl_category_id/"+tbl_category_id+"/";
		
			url += "offset/0/";
		window.location.href = url;
		<?php /*?>window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/enquiry/all_enquiries/is_not_replied/"+is_not_replied+"/tbl_court_id/"+tbl_court_id+"/tbl_category_id/"+tbl_category_id;<?php */?>
	}
</script>