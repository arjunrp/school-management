<?php

//Init Parameters
$student_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}
$selDate = date("m/d/Y");
?>
 
<style>
.txt_en {
	text-align:left;
	padding-left:2px;
}
.txt_ar {
	text-align:right;
	padding-right:2px;	
	direction:rtl;		
}
</style>

<script type="text/javascript" src="<?=JS_PATH?>/date_time_picker/js/jquery-ui-1.8.6.custom.min.js"></script>
<script type="text/javascript" src="<?=JS_PATH?>/date_time_picker/js/jquery-ui-timepicker-addon.js"></script>
<script language="javascript">
	$(document).ready(function(){
		$('#attendance_date').datepicker({
			dateFormat: 'yyyy-mm-dd',
			timeFormat: 'hh:mm tt',
			timeOnly: true   
		});


	});
</script>

<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_students_list(tbl_class_id,attendance_date) {
		
		
		var tbl_academic_year = $("#tbl_academic_year").val();
		var tbl_semester_id   = $("#tbl_semester_id").val();
		
		var gender            = $("#gender").val();
		var tbl_country_id    = $("#tbl_country_id").val();
		var tbl_student_id    = $("#tbl_student_id").val();
		var tbl_teacher_id    = $("#tbl_teacher_id").val();
		
		
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/attendance_reports_list/";
		
			
		if(tbl_semester_id !='')
			url += "tbl_semester_id/"+tbl_semester_id+"/";
			
		if(tbl_class_id !='')
			url += "tbl_class_id/"+tbl_class_id+"/";
			
	
		if(tbl_student_id !='')
			url += "tbl_student_id/"+tbl_student_id+"/";
			
		if(attendance_date !='')
			url += "attendance_date/"+attendance_date+"/";
			
		if(tbl_teacher_id !='')
			url += "tbl_teacher_id/"+tbl_teacher_id+"/";

			url += "offset/0/";
		window.location.href = url;	
		
	}
	
	function show_listing() {
		$('#mid2').hide();
		$('#mid1_list').show(500);
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/school_attendance_reports";
			}
		})
	});
	
</script>
	
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper" >
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Students Progress Reports</h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Students</li>
    Progress Report
    </ol>
    <!--/BREADCRUMB--> 
  <?php /*?>  <div style=" float:right; "> <button onclick="show_student_cards()" title="Student Cards" type="button" class="btn btn-primary">Student Cards</button></div>
    <div style=" float:right; padding-right:10px;"> <button onclick="show_student_points()" title="Student Points" type="button" class="btn btn-primary">Student Points</button></div><?php */?>
    <div style="clear:both"></div>
  </section>
      <link href="<?=HOST_URL?>/assets/admin/dist/css/jquery-ui.css" rel="stylesheet">
      <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-1.11.1.js"></script>
      <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-ui.js"></script>
      <link href="<?=HOST_URL?>/assets/admin/dist/css/uploadfile.min.css" rel="stylesheet">
 
      
 <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {
			
	
	?>
        <!--Edit-->
   <script>
 $( function() {
    $( "#tabs" ).tabs();
  } );
  
 
</script>

        
              
		        
        <!--/Edit-->
	<?php							
		} else { ?>
			
			<div id="mid1" class="box box-success">
                        <div class="box-header">
                          <div class="col-sm-11"> 
                          <div class="col-sm-1" >
                          <h3 class="box-title">SEARCH</h3>
                          </div>
                          </div>
                          <div class="col-sm-11"> &nbsp;</div>
                          <div class="col-sm-11"> 
                            
                              <div class="col-sm-3"> 
                             
                              <select name="tbl_class_id" id="tbl_class_id" class="form-control" onChange="get_students_ajax()" >
                              <option value="">--Select Class --</option>
							  
							  <?php
                                    for ($u=0; $u<count($classes_list); $u++) { 
                                        $tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
                                        $class_name             = $classes_list[$u]['class_name'];
                                        $class_name_ar          = $classes_list[$u]['class_name_ar'];
										$section_name           = $classes_list[$u]['section_name'];
                                        $section_name_ar        = $classes_list[$u]['section_name_ar'];
                                        if($tbl_sel_class_id == $tbl_class_id_u)
                                           $selClass = "selected";
                                         else
                                           $selClass = "";
                                  ?>
                                      <option value="<?=$tbl_class_id_u?>"  <?=$selClass?>  >
                                      <?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;
                                    <?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>   
                               </div>
                             
                             
                             <?php 
							 
							 if($tbl_sel_student_id<>"")
							 	$displayStu = 'block';
							 else
							 	$displayStu = 'none';
							 ?>  
                             
                               
                              <div class="col-sm-3" id="divStudent" style="display:<?=$displayStu?>;" > 
                             
                           <select name="tbl_student_id" id="tbl_student_id" class="form-control">
                                <option value="">--Select Student--</option>
                                  <?php
                                    for ($u=0; $u<count($rs_all_students); $u++) { 
                                        $tbl_student_id_u         = $rs_all_students[$u]['tbl_student_id'];
                                        $name                     = $rs_all_students[$u]['first_name']." ".$rs_all_students[$u]['last_name'];
										$name_ar                    = $rs_all_students[$u]['first_name_ar']." ".$rs_all_students[$u]['last_name_ar'];
                                        if($tbl_sel_student_id == $tbl_student_id_u)
                                           $selStudent = "selected";
                                         else
                                           $selStudent = "";
                                  ?>
                                      <option value="<?=$tbl_student_id_u?>"  <?=$selStudent?>  >
                                      <?=$name?>[::]<?=$name_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                                
                               </select>     
                              </div>  
                                <div class="col-sm-3"><button class="btn btn-success" type="button" onClick="search_data()">Search</button>&nbsp;<button class="btn btn-success" type="button" 
                               onclick="reset_data();">Reset</button>
                               </div>
                             
                             
                               </div>
                               
                               <div class="col-sm-11">&nbsp;</div>
                          </div>
                        </div>
                       
                       
                         <?php if($tbl_sel_student_id<>"") { ?>
                        
                       <div id="mid1_list" class="box" style="display:block;" >  
                        
                     
                         <div class="box-header">
                          <h3 class="box-title">Reports - <?=$student_info[0]['first_name']." ".$student_info[0]['last_name']?> &nbsp;[<?=$class_info[0]['class_name']." ".$class_info[0]['section_name']?>]&nbsp;[::]&nbsp; 
                          <?=$student_info[0]['first_name_ar']." ".$student_info[0]['last_name_ar']?> &nbsp;[<?=$class_info[0]['class_name_ar']." ".$class_info[0]['section_name_ar']?>]</h3>
                          <div class="box-tools">
                            <?php if (count($progress_reports_student)>0) { echo $paging_string;}?>	
                            <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
                          </div>
                        </div> 
                        <!-- Progress Report Form -->
                        <div id="report_list"  >
                        
                            <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle"><input id="select_all" type="checkbox" value="" /></th>
                              <!--<th width="10%" align="center" valign="middle">Sl No.</th>-->
                              <th width="30%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y">Report Name[En] <?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?><div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>
                              <th width="30%" align="center" valign="middle">Report Name [Ar]</th>
                              <th width="20%" align="center" valign="middle">Added Date</th>
                             <?php /*?> <th width="5%" align="center" valign="middle">Status</th>
                              <th width="5%" align="center" valign="middle">Action</th><?php */?>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                                for ($i=0; $i<count($progress_reports_student); $i++) { 
                                    $id = $rs_all_students[$i]['id'];
									$tbl_progress_report_id    = $progress_reports_student[$i]['tbl_progress_report_id'];
                                    $tbl_progress_id    = $progress_reports_student[$i]['tbl_progress_id'];
                                    $added_date         = $progress_reports_student[$i]['reported_date'];
                                    $is_active          = $progress_reports_student[$i]['is_active'];
									$report_name        = $progress_reports_student[$i]['report_name'];
                                    $report_name_ar     = $progress_reports_student[$i]['report_name_ar'];
									$tbl_class_id       = $progress_reports_student[$i]['tbl_class_id'];
									$tbl_student_id     = $progress_reports_student[$i]['tbl_student_id'];
									$tbl_school_id      = $progress_reports_student[$i]['tbl_school_id'];
									
                                    $added_date = date('m-d-Y',strtotime($added_date));
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_teacher_id?>" >
                              <td align="left" valign="middle">
                              <span style="float:left;">
                              <input id="tbl_progress_id" name="tbl_progress_id" class="checkbox" type="checkbox" value="<?=$tbl_progress_id?>" />
                              </span>
                              </td>
                             <!-- <td align="left" valign="middle"><?=$offset+$i+1?></td>-->
                              <td align="left" valign="middle">
                              <div class="txt_en"><span style="float:left;"> <a style="cursor:pointer;" onclick="popup_progress_report('<?=$tbl_progress_report_id?>','<?=$tbl_progress_id?>','<?=$tbl_school_id?>','<?=$tbl_student_id?>','<?=$tbl_class_id?>')" ><?=$report_name?></a></span>
                              </div>
                              </td>
                             <?php /*?> <td align="left" valign="middle"> 
                              <div class="txt_en"><?=$class_name?>&nbsp;<?=$section_name?></div>
                              <div class="txt_ar"><?=$class_name_ar?>&nbsp;<?=$section_name_ar?></div></td><?php */?>
                              <td align="left" valign="middle">  <div class="txt_ar"><a style="cursor:pointer;" onclick="popup_progress_report('<?=$tbl_progress_report_id?>','<?=$tbl_progress_id?>','<?=$tbl_school_id?>','<?=$tbl_student_id?>','<?=$tbl_class_id?>')" ><?=$report_name_ar?></a></div></td>
                              <td align="left" valign="middle"><?=$added_date?></td>
                            <?php /*?>  <td align="left" valign="middle">
                                <div id="act_deact_<?=$tbl_teacher_id?>">
                                <?php if (trim($is_active) == "Y") { ?>
                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_teacher_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                <?php } else { ?>
                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_teacher_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                <?php } ?>
                                </div>
                              </td>
                              <td align="left" valign="middle">
                                <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/edit_teacher/teacher_id_enc/<?=$tbl_teacher_id?>"><button class="btn bg-purple fa fa-pencil" type="button" title="Edit"></button></a>
                              </td><?php */?>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="10" align="right" valign="middle">
                              <?php // echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if ($total_progress_reports_student<=0) {
                            ?>
                            <tr>
                              <td colspan="10" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no progress reports available. Click on the + button to create one.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>   
       
        
  </div>
  
                        <!-- End progress report form -->
                       
                     
                       </div>
                 <?php } ?>
                 
                 <div id="mid2" class="box" style="display:none;" >  
                  
                    <div class="box-header">
                          <h3 class="box-title">Reports - <?=$student_info[0]['first_name']." ".$student_info[0]['last_name']?> &nbsp;[<?=$class_info[0]['class_name']." ".$class_info[0]['section_name']?>]&nbsp;[::]&nbsp; 
                          <?=$student_info[0]['first_name_ar']." ".$student_info[0]['last_name_ar']?> &nbsp;[<?=$class_info[0]['class_name_ar']." ".$class_info[0]['section_name_ar']?>]</h3>
                          <div class="box-tools">
							<button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>                          
						</div>
                        </div> 
                  
                   <div class="box-body" id="add_report" style="display:block;">
       
       
                  <div class="form-group">
                     <label class="col-sm-2 control-label" for="tbl_parenting_school_cat_id">Report<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-4">
                                 <select name="tbl_progress_report_id" id="tbl_progress_report_id" class="form-control"  >
                                 <option value="">Select Report</option>
                              <?php
                                    for ($u=0; $u<count($rs_progress_report); $u++) { 
                                        $tbl_progress_report_id   	= $rs_progress_report[$u]['tbl_progress_report_id'];
                                        $report_name         		= $rs_progress_report[$u]['report_name'];
                                        $report_name_ar         	= $rs_progress_report[$u]['report_name_ar'];
                                  ?>
                                      <option value="<?=$tbl_progress_report_id?>" >
                                      <?=$report_name?>&nbsp;[::]&nbsp;
                                    <?=$report_name_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>
                    </div>
                     <label class="col-sm-1 control-label" for="tbl_progress_report_id" style="font-size:12px; text-decoration:underline; cursor:pointer; color:#030;" ><span style="padding-left:2px;" onclick="addNewCategory();">+&nbsp;Add New Report</span></label>
                      <div style="clear:both;"></div>
                    </div>
                   
                    
                  <div class="form-group" id="divCategory" style="display:none;" >
                      <label class="col-sm-2 control-label" for="report_name">Report Name [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-4">
                        <input type="text" placeholder="Report Name [En]" id="report_name" name="report_name" class="form-control"  >
                      </div> 
                      
                      <label class="col-sm-1 control-label" for="report_name_ar">Report Name [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-4">
                        <input type="text" placeholder="Report Name [Ar]" id="report_name_ar" name="report_name_ar" class="form-control" dir="rtl"  >
                      </div>
                      
                    </div>
                     
  
        <table width="100%" class="table table-bordered table-striped">
          <thead>
            <tr>
                <th width="50%" align="center" valign="middle">Subject</th>
                <th width="50%" align="center" valign="middle">Mark</th>
                <th width="50%" align="center" valign="middle">Total Mark</th>
            </tr> 
          </thead>
          <tbody>
         

          <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		  <script src="<?=HOST_URL?>/js/jquery-ui.js"></script>  


  
<?php	

    $subject_ids  = "";
	for($i=0; $i<count($rs_subject); $i++) {
		 $id                = $rs_subject[$i]['id'];
		 $tbl_subject_id    = $rs_subject[$i]['tbl_subject_id'];
		 $tbl_school_id     = $rs_subject[$i]['tbl_school_id'];
		 $subject_ids      .=  $tbl_subject_id."||";
?>		
         
         <tr>
		  <td>
          <div><?=$rs_subject[$i]["subject"]?><span style="float:right;"><?=$rs_subject[$i]["subject_ar"]?></span> </div>
         </td>
         <td >
           <input name="mark_<?=$tbl_subject_id?>" id="mark_<?=$tbl_subject_id?>" value="" size="2" maxlength="3" type="text" class="form-control"  style="width:200px;" placeholder="Enter Mark"/>
          </td>
           <td >
           <input name="total_<?=$tbl_subject_id?>" id="total_<?=$tbl_subject_id?>" size="2" maxlength="3" type="text" class="form-control" style="width:200px;" placeholder="Enter Total Mark"  value="100" />
          </td>
         </tr>
<?php
	}
?>

<tr>
<td colspan="3" >
<input name="subject_ids" id="subject_ids" type="hidden" value="<?=$subject_ids?>"/>
<button class="btn btn-primary" type="button" onclick="add_student_progress_report('<?=$tbl_student_id?>', '<?=$tbl_class_id?>',  '<?=$tbl_school_id?>', '<?=$tbl_teacher_id?>' )">SAVE</button>
<button class="btn btn-primary" type="button" onclick="get_progress_report_form_refresh()">CANCEL</button>

</td></tr>
 </tbody>
          <tfoot>
          </tfoot>
        </table>
        
   </div>     
  </div>
  
  <div id="myModal" class="modal fade" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close"  aria-label="Close" onclick="get_progress_report_close();"><span aria-hidden="true">&times;</span></button>  <!--data-dismiss="modal"-->
                <h4 class="modal-title">Progress Report</h4>
              </div>
              <div class="modal-body" id="modal-body" >
                
                
              </div>
              <div class="modal-footer" style="border:none !important;">
               <!-- <button type="button" class="btn btn-default" onclick="send_message_user()" >Send Message</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div>
        
<script>

function popup_progress_report(tbl_progress_report_id, tbl_progress_id, tbl_school_id, tbl_student_id, tbl_class_id) {
		$('#myModal').modal('show');
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/en/admin/teacher/view_progress_report",
			data: {
				tbl_progress_report_id:tbl_progress_report_id,
				tbl_progress_id: tbl_progress_id,
				tbl_student_id: tbl_student_id,
				tbl_class_id: tbl_class_id,
				tbl_school_id: tbl_school_id,
				is_ajax: true
			},
			success: function(data) {
				$("#modal-body").html(data);
				//$('#txt_message').html(data);
				//$( "#alert_message_sent" ).dialog({ modal: true },{ resizable: false }, { draggable: false });
				//$( "#alert_message_sent" ).dialog({ title: "Message" },{ buttons: [ { text: "Ok", click: function() { window.location.reload();	} } ] });
			//	$('#myModal').modal('hide');
			//	my_alert("Message sent successfully.", "green");
			//	setTimeout(function(){window.location.reload();},1000);
			},
			error: function() {
				$('#pre_loader').css('display','none');	
			}, 
			complete: function() {
				$('#pre_loader').css('display','none');	
			}
		});	
   }

function show_create_form() {
		$('#mid1_list').hide(500);
		$('#mid2').show(500);
	}
	
function show_listing() {
		$('#mid1_list').show(500);
		$('#mid2').hide(500);
}

	 
function addNewCategory()
	{
		// divCategory
		if($('#divCategory').is(':visible'))
		{
			$("#divCategory").hide();
		}else{
			$("#divCategory").show();
		}
		 
	 }
function add_student_progress_report(tbl_student_id,tbl_class_id,tbl_school_id,tbl_teacher_id) {
		
		var tbl_progress_report_id =  $("#tbl_progress_report_id").val();
		var report_name            =  $("#report_name").val();
		var report_name_ar         =  $("#report_name_ar").val();
		var subject_ids 		   =  $("#subject_ids").val();
		var id_data     		=  "";
		var sub_mark_data       =  "";
		var tot_mark_data       =  "";
		var cnt                 =  "";
		
		
		if(report_name== '' && tbl_progress_report_id=="")
		{
			alert("Select report name.");
			return false;
		}
		
		
		var id_array            =  new Array;
		if(subject_ids != '')
		{
			var id_array    = subject_ids.split('||');
			for(k = 0; k < id_array.length; k++)
			{
				var ids          = id_array[k];
				if(ids!='')
				{
					var subMark  	= $("#mark_"+ids).val();
					var totMark  	= $("#total_"+ids).val();
					if(subMark != '' && totMark != '' )
					{
						id_data 		= id_data+ids+"||";
						sub_mark_data 	= sub_mark_data+subMark+"||";
						tot_mark_data 	= tot_mark_data+totMark+"||";
						cnt = 1;
						
					}
				}
			
			}
			
		}
		
		
		if(cnt!= '1')
		{
			alert("Please enter student marks and total marks.");
			return false;
		}else{
			
			$("#pre-loader").show();
			var url_str = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/save_student_progress_report";
			$.ajax({
				type: "POST",
				url: url_str,
				data: {
					is_admin: "Y",
					tbl_progress_report_id: tbl_progress_report_id,
					report_name: report_name,
					report_name_ar: report_name_ar,
					id_data: id_data,
					sub_mark_data: sub_mark_data,
					tot_mark_data: tot_mark_data,
					tbl_student_id: tbl_student_id,
					tbl_class_id: tbl_class_id,
					tbl_teacher_id: tbl_teacher_id,
					tbl_school_id: tbl_school_id
				},
				success: function(result_data) {
					if($.trim(result_data)=="E")
					{
						 alert("Progress report already exist.");
						$("#pre-loader").hide();
					}else if($.trim(result_data)=="N")
					{
						 alert("Progress report added failed, Please try again.");
						$("#pre-loader").hide();
					}else{
						alert("Progress report added successfully.");
						$("#pre-loader").hide();
						search_data();
					}
				}
			});
		}
	}
	
/* function get_progress_report_close()
	{
		jQuery.noConflict(); 
		$('#myModal').modal('hide');
		
	}	
*/	
</script>
                        
                        <!-- End Progress Report Form -->
			
			
		<?php	}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>
  
    <!--/WORKING AREA--> 
  </section>
  <div style="clear:both;"></div>
</div>
   
<script language="javascript" >
function search_data() {
		
		var tbl_academic_year = $("#tbl_academic_year").val();
		var tbl_semester_id   = $("#tbl_semester_id").val();
		var tbl_class_id      = $("#tbl_class_id").val();
		var tbl_student_id    = $("#tbl_student_id").val();
		var tbl_teacher_id    = $("#tbl_teacher_id").val();
		
		
	
		
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/student_progress_report/";
		if(tbl_academic_year !='')
			url += "tbl_academic_year/"+tbl_academic_year+"/";
		if(tbl_semester_id !='')
			url += "tbl_semester_id/"+tbl_semester_id+"/";
		if(tbl_class_id !='')
			url += "tbl_class_id/"+tbl_class_id+"/";
		if(tbl_student_id !='')
			url += "tbl_student_id/"+tbl_student_id+"/";
		if(tbl_teacher_id !='')
			url += "tbl_teacher_id/"+tbl_teacher_id+"/";	
		
			url += "offset/0/";
		window.location.href = url;
		<?php /*?>window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/enquiry/all_enquiries/is_not_replied/"+is_not_replied+"/tbl_court_id/"+tbl_court_id+"/tbl_category_id/"+tbl_category_id;<?php */?>
	}

function reset_data() {
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/student_progress_report/";
		url += "offset/0/";
		window.location.href = url;
	}
	
function get_students_ajax() {
		//show_loading();
		var class_ids = $("#tbl_class_id").val();
		 var tbl_class_id='';
        	$('#tbl_class_id :selected').each(function(i, selected) {
            	tbl_class_id += $(selected).val()+",";
        	});
		//alert(tbl_class_id);
		//return;
		var xmlHttp, rnd, url, search_param, ajax_timer;
		rnd = Math.floor(Math.random()*11);
		try{		
			xmlHttp = new XMLHttpRequest(); 
		}catch(e) {
			try{
				xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
			}catch(e) {
				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
				hide_loading();
			}
		}

		//AJAX response
		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
				//ajax_timer.stop();
				var data = xmlHttp.responseText;
			    $("#divStudent").show();
				$("#divStudent").html(data);
				//$("#tbl_student_dropdown").multiselect('refresh');
				return;
			}
		}

		/*ajax_timer = $.timer(function() {
			xmlHttp.abort();
			alert(connectivity_msg);
			ajax_timer.stop();
		},connectivity_timeout_time,true);*/

		//Sending AJAX request
		url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/students_against_classes/selBox/1/tbl_class_id/"+tbl_class_id+"/rnd/"+rnd;
		xmlHttp.open("POST",url,true);
		xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlHttp.send("rnd="+rnd);
	}
	
	
	function get_semester_ajax() {
		//show_loading();
		var tbl_academic_year = $("#tbl_academic_year").val();
		
		//alert(tbl_class_id);
		//return;

		var xmlHttp, rnd, url, search_param, ajax_timer;
		rnd = Math.floor(Math.random()*11);
		try{		
			xmlHttp = new XMLHttpRequest(); 
		}catch(e) {
			try{
				xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
			}catch(e) {
				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
				hide_loading();
			}
		}

		//AJAX response
		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
				//ajax_timer.stop();
				var data = xmlHttp.responseText;
				$("#divSemester").html(data);
				//$("#tbl_student_dropdown").multiselect('refresh');
				return;
			}
		}

		/*ajax_timer = $.timer(function() {
			xmlHttp.abort();
			alert(connectivity_msg);
			ajax_timer.stop();
		},connectivity_timeout_time,true);*/

		//Sending AJAX request
		url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/semesters_against_academicyear/tbl_academic_year/"+tbl_academic_year+"/rnd/"+rnd;
		xmlHttp.open("POST",url,true);
		xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlHttp.send("rnd="+rnd);
	}
	
	
function get_progress_report_form_refresh()
{
   	search_data();
	
}
</script>


    
    

