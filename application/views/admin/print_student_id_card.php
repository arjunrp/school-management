<?php ob_start();?>
<!DOCTYPE html>
<html>
<style>
  html, body {
    padding:0;
    margin:0;
}
  body {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #000;
}
table {
	border: none;
}
table td {
	font-size: 11px;
	color: #000;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" id="printarea">
<?php
//header('Content-Type: text/html; charset=utf-8');
for ($i=0; $i<count($rs_all_students); $i++) { 

		$id               = $rs_all_students[$i]["id"];
		$tbl_student_id   = $rs_all_students[$i]["tbl_student_id"];
		$first_name       = $rs_all_students[$i]["first_name"];
		$last_name        = $rs_all_students[$i]["last_name"];
		$name_en          = $first_name." ".$last_name;
		$name_en          =  substr($name_en, 0,42);
		$first_name_ar    = $rs_all_students[$i]["first_name_ar"];
		$last_name_ar     = $rs_all_students[$i]["last_name_ar"];
		$name_ar          = $first_name_ar." ".$last_name_ar;
		//$name_ar          = substr($name_ar, 0,42);
		$mobile           = $rs_all_students[$i]["mobile"];
		$dob_month        = $rs_all_students[$i]["dob_month"];
		$dob_day          = $rs_all_students[$i]["dob_day"];
		$dob_year         = $rs_all_students[$i]["dob_year"];
		$gender           = $rs_all_students[$i]["gender"];
		$country          = $rs_all_students[$i]["country"];
		$pic              = $rs_all_students[$i]['file_name_updated'];
		$parent_name_en     = ucfirst($rs_all_students[$i]['parent_first_name'])." ".ucfirst($rs_all_students[$i]['parent_last_name']);
		$parent_name_ar     = $rs_all_students[$i]['parent_first_name_ar']." ".$rs_all_students[$i]['parent_last_name_ar'];
		if($pic=="")
		{
			$pic = "no_img.png";
		}
		
		
		if($pic<>"") // class="img-circle"
			$pic_path           =   '<img width="100" height="80" src="'.IMG_PATH_STUDENT.'/'.$pic.'"  />';
		else
			$pic_path           =   '<img width="100" height="80" src="'.IMG_PATH_STUDENT.'/no_img.png"  
										style="border-color:1px solid #7C858C !important;" />';

		if($gender =="male"){
			$gender  ="Male";
			$gender_ar ="ذكر";
		}else{
			$gender  ="Female";
			$gender_ar ="أنثى";
		}

		$email                  = isset($rs_all_students[$i]["email"])? $rs_all_students[$i]["email"] :"-";
		$file_name_updated      = $rs_all_students[$i]["file_name_updated"];
		$emirates_id_father     = $rs_all_students[$i]["emirates_id_father"];

		if($rs_all_students[$i]["emirates_id_mother"]<>""){ 
		 	$emirates_id_mother = $rs_all_students[$i]["emirates_id_mother"]; 
		}else{ 
			$emirates_id_mother = "Not mentioned"; 
		} 

		$class_name             = $rs_all_students[$i]["class_name"];
		$class_name_ar          = $rs_all_students[$i]["class_name_ar"];
		$section_name           = $rs_all_students[$i]["section_name"];
		$section_name_ar        = $rs_all_students[$i]["section_name_ar"];
		$user_id                = $rs_all_students[$i]["user_id"];
		$password = base64_decode($rs_all_students[$i]["pass_code"]);
		?>
<table width="100%" height="100%"  >
  <tr height="30">
    <td width="28%"  valign="bottom"><img src="<?=HOST_URL?>/assets/admin/images/home_19.png" width="90px"  alt=""></td>
    <td width="46%" align="center" style="font-size:16px;" ><strong><?=$_SESSION['aqdar_smartcare']['school_name_ar']?></strong></td>
    <td width="26%" align="right"><img width="90px" src="<?=HOST_URL?>/assets/admin/images/logos.png" alt="image" /></td>
  </tr>
  <tr height="35">
    <td colspan="3" height="35">
    <table width="100%">
        <tr>
          <td width="50"><img width="50px" src="<?=HOST_URL?>/assets/admin/images/barcode.jpg" alt="image" /></td>
          <td width="130"> Username: <strong><?=$user_id?></strong><br>Password: <strong><?=$password?></strong></td>
          <td width="120" align="right" ><img style="margin-right:5px;" width="50px" height="50px" src="<?=HOST_URL?>/images/student/<?=$pic?>" alt="image" /></td>
        </tr>
      </table></td>
  </tr>
  <tr height="140" >
    <td colspan="3" >
   <table width="100%">
     
        <tr>
          <td height="4" style="line-height:8px; overflow:hidden; text-wrap:none;" width="100%" dir="rtl" align="right" colspan="2">اسم:  <strong><?=$name_ar?></strong></td>
        </tr>
       <tr>
          <td  height="2" colspan="2" align="left" valign="top"  style=" overflow:hidden;">
          <table cellspacing="0" cellpadding="0" height="2" width="320" style="margin-right:-200px; overflow:hidden;" ><tr><td  width="320" style="font-size:11px;" height="2">Name: <strong><?=$name_en?></strong></td></tr></table>
          </td>
        </tr>
        <tr>
          <td height="3" style="line-height:6px;" align="right" colspan="2">الصف: <strong><?=$class_name_ar?> <?=$section_name_ar?></strong></td>
        </tr>
        <tr>
          <td height="3" style="line-height:6px;" colspan="2">Class: <strong><?=$class_name?> <?=$section_name?></strong></td>
        </tr>
         <tr>
          <td height="3" style="line-height:6px; padding-top:6px;"  dir="ltr" align="left" >Nationality: <strong><?=$country?></strong></td> 
          <td height="3" style="line-height:6px; padding-top:6px;" align="right"  dir="rtl">الجنسية: <strong><?=$country?></strong></td>
        </tr>
        <tr>
          <td height="4" style="line-height:6px; padding-top:6px;"  dir="ltr" align="left" colspan="2" >&nbsp;</td>
        </tr>
      </table> 
   
   </td>
  </tr>
   <tr height="20" >
    <td colspan="3">
   <table width="100%" style="border-top:1px solid #000;" >
         <tr>
         <td height="4" style="font-size:9px;"  align="left" valign="bottom" >www.timeline.ae</td>
         <td height="4" style="font-size:9px;"  align="right" valign="bottom"  >www.aqdarsmartcare.com</td>
        </tr> 
      </table> 
   </td>
  </tr>
</table>
<?php } ?>
</body>
</html>
<?php
$output = ob_get_clean( );
$output = htmlentities($output);
?>
<form method="post" name="frmsubmit" id="frmsubmit" action="<?=HOST_URL?>/id_cards_oldversion.php">
  <input type="hidden" name="html" value="<?=$output?>" />
</form>
<script>
  document.frmsubmit.submit();
</script>