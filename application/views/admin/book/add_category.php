<?php include("include/header.php"); 
      $unique_id = substr(md5(rand()),0,15); 
?>
        
        <!-- Page Title -->
        <div class="page-title-container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeIn">
                        <i class="fa fa-book" style="color:#0FF;"></i>
                        <h1>Add Category</h1>
                        <p>You can add book category here.</p>
                         <i class="fa fa-book" style="float:right;color:#0FF;"></i>
                        <h1 dir="rtl" style="float:right; margin-top:0px !important;">&nbsp;اضافة تصنيف  </h1>
                        <p dir="rtl" style="float:right; margin-top:10px !important;">يمكنك إضافة تصنيف الكتاب من هنا .</p>
                    </div>
                </div>
            </div>
        </div>

        <!-- Portfolio -->
        <div class="portfolio-container">
            <div class="contact-us-container" id="div_add_book">
        	<div class="container">
	            <div class="row">
                 <div class="col-sm-12 services-full-width-text wow fadeInLeft">
                   <div style="width:50%; float:left;">
                  <h2 align="left">Add Book Category&nbsp;/&nbsp;اضاف تصنيف الكتاب</h2>  
                  </div>  
                  <div style="width:50%; float:right; padding-top:30px;">
                  <span style="float:right; padding-right:30px;"><a href="<?=HOST_URL?>/books/list_category/page/book" class="big-link-1">List Category&nbsp;/&nbsp;قائمة التصنيفات</a></span>   
                  </div> 
                </div>
                
                <p style="margin-left:10px;text-align:center;">
	                    	<span class="successMsg"><?=$message?></span>
	                    </p> 
                 <form role="form" enctype="multipart/form-data" onsubmit="return validateForm();" action="" method="post" id="frmRecord" name="frmRecord" >
	                <div class="col-sm-6 contact-form wow fadeInLeft">
                          
                            <input name="tbl_book_category_id" id="tbl_book_category_id" value="<?=$unique_id?>"  type="hidden"/>
                          
                           <?php /*?> <div class="form-group">
	                    		<label for="contact-name">Parent Category</label><br>
	                        	<select id="tbl_book_category_parent_id" name="tbl_book_category_parent_id" class="contact-name">
                                <option value="">Parent</option>
                                <?php for($m=0;$m<count($list_category);$m++){?>
                                <option value="<?php echo $list_category[$m]['tbl_book_category_id']; ?>"><?php echo $list_category[$m]['category_name_en']; ?> - <?php echo $list_category[$m]['category_name_ar']; ?></option>
                                <?php } ?>
                                </select>
                                
	                        </div><?php */?>
                            <div class="form-group">
	                    		<label for="contact-name">Category Name [Ar] / اسم التصنيف باللغة العربية<span class="msgColor">*</span></label>
	                        	<input type="text" name="category_name_ar" placeholder="أدخل اسم التصنيف باللغة العربية..." class="contact-name" id="category_name_ar" dir="rtl" tabindex="1">
                                <span class="err_message" id="sel_title_ar" style="display:none;">Please enter category name in Arabic / الرجاء إدخال اسم التصنيف باللغة العربية</span>
	                        </div>
                       </div>
                       <div class="col-sm-6 contact-form wow fadeInLeft">
	                
                            <div class="form-group">
	                    		<label for="contact-name">Category Name [En] / اسم التصنيف باللغة الإنجليزية<span class="msgColor">*</span></label>
	                        	<input type="text" name="category_name_en" placeholder="Enter category name in English..." class="contact-name" id="category_name_en">
                                <span class="err_message" id="sel_title_en" style="display:none;">Please enter category name in English / الرجاء إدخال اسم التصنيف باللغة الإنجليزية</span>
	                        </div>
                           
                            
	                </div>
                    <div class="col-sm-12 wow fadeInLeft">
                    <div class="form-group">  
                             <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                            <tr>
                            <td>Picture:</td>
                            </tr>
                            <tr>
                              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td width="100%" align="right" valign="top"> <iframe id="iframe_category_pic" name="iframe_category_pic" src="<?=HOST_URL?>/books/iframe_show_category_pic/tbl_book_category_id/<?=$unique_id?>" width="100%" height="100" frameborder="0" scrolling="no" ></iframe></td>
                                  </tr>
                                </table></td>
                            </tr>
                            <tr><td>&nbsp;</td></tr>
                          </table>
                          </div>
                     </div>
                   <div class="col-sm-12 contact-form wow fadeInLeft"> 
                    <button type="submit" class="btn">Submit / تقديم</button>
                    </div>
                     </form>
	         
	            </div>
	        </div>
        </div>
        </div>
 

	    <script language="javascript">
		
		function validateForm() {
			
			if ( val_title_ar() == false || val_title_en() == false) {
				return false;
			} else {
				saveBookCategory();
			}	
		}//function validateForm
        
	
		
		//VALIDATE title
		function val_title_en() {
			var strTitleEn = $("#category_name_en").val();	
			if(strTitleEn==""){
				$("#sel_title_en").show('20');
				return false;
			}else{
				$("#sel_title_en").hide('20');
				return true;
			}
		}
		
		function val_title_ar() {
			var strTitleAr = $("#category_name_ar").val();	
			if(strTitleAr==""){
				$("#sel_title_ar").show('20');
				return false;
			}else{
				$("#sel_title_ar").hide('20');
				return true;
			}
		}
		
		//VALIDATE CHECK LOGIN 
		  function saveBookCategory()
		  {
				try { 
					$.ajax({
					type: "POST",
					dataType:"text",
					url: "<?=HOST_URL?>/books/add_category",
					data: {
						tbl_book_category_id: $("#tbl_book_category_id").val(),
						tbl_book_category_parent_id: '',
						category_name_en: $("#category_name_en").val(),
						category_name_ar: $("#category_name_ar").val(),
						is_ajax: true
					},
					success: function(data) {
						
						if(data=="N")
						{
							alert("Book category is already exist");
							return false;
						}else{
							alert("Book category is added successfully");
							window.location.href="<?=HOST_URL?>/books/add_category?page=book";
						}
				
					}
		
					});
				} catch(e) {
					//alert(e)	
				}	
		  }
        </script>

        <!-- Footer -->
      <?php include("include/footer.php"); ?>