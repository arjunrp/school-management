<?php include("include/header.php"); ?>

 <link rel="stylesheet" href="<?=HOST_URL?>/assets/css/Fr.star.css">
 <script src="<?=HOST_URL?>/assets/js/Fr.star.js"></script> 
 <script src="<?=HOST_URL?>/assets/js/rate.js"></script> 

        <!-- Page Title -->
        <div class="page-title-container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeIn">
                        <i class="fa fa-book" style="color:#0FF;"></i>
                        <h1>Book Details </h1>
                        <p>Here you can see all the information about book</p>
                        <i class="fa fa-book" style="float:right;color:#0FF;"></i>
                        <h1 dir="rtl" style="float:right; margin-top:0px !important;">&nbsp;تفاصيل الكتاب</h1>
                        <p dir="rtl" style="float:right; margin-top:10px !important;">هنا يمكنك ان ترى كل المعلومات عن الكتاب</p>
                    </div>
                </div>
            </div>
        </div>
        
        <?php
		$tbl_book_id 			= $getBookInfo[0]['tbl_book_id'];
		$tbl_book_category_id   = $getBookInfo[0]['tbl_book_category_id'];
		$book_language          = $getBookInfo[0]['book_language'];
		$title_en 			   = $getBookInfo[0]['title_en'];
		$title_ar   			   = $getBookInfo[0]['title_ar'];
		$author_name_en         = $getBookInfo[0]['author_name_en'];
		$author_name_ar 		 = $getBookInfo[0]['author_name_ar'];
		$isbn   				   = $getBookInfo[0]['isbn'];
		$description_en         = $getBookInfo[0]['description_en'];
		$description_ar 		 = $getBookInfo[0]['description_ar'];
		$age_group              = explode("|",$getBookInfo[0]['age_group']);
		$cover_pic              = $getBookInfo[0]['cover_pic'];
		$cover_pic_path         = HOST_URL."/uploads/books/".$getBookInfo[0]['cover_pic'];
        $book_file_name         = $getBookInfo[0]['book_file_name'];
		$book_file_path         = HOST_URL."/uploads/books/".$getBookInfo[0]['book_file_name'];
		$book_point 		     = $getBookInfo[0]['book_point'];
		
		
		?>
        

        <!-- Services Full Width Text -->
        <div class="services-full-width-container">
             
        	<div class="container">
	            <div class="row">
                
	                  <div class="col-sm-12 services-full-width-text wow fadeInLeft">
                      <div style="width:50%; float:left;">
                      <h2 align="left">Book Details&nbsp;/&nbsp;تفاصيل الكتاب</h2>  
                      </div>  
                      <div style="width:50%; float:right; padding-top:30px;">
                      <span style="float:right;"><a href="<?=HOST_URL?>/books/index/page/book/offset/<?=$offset?>" class="big-link-1">List Books&nbsp;/&nbsp;قائمة الكتب</a></span>   
                      </div>    
                      </div>
                    
                    <div class="col-sm-6 services-full-width-text wow fadeInLeft">
	                   <?php if($isbn<>""){ ?>
                       Book Key Number :&nbsp;<?=$isbn?> 
                       <?php } ?>
                        <h3> <?=$title_en?></h3>
                        <br>
                         <p>
	                    	Author : <i><strong><?=$author_name_en?></strong></i>
	                	</p>
                       
                        
                        <p><?=$category_name_en?></p>
                        <p>
	                    	<?=$description_en?>
	                	</p>
                        
	                </div>
                    <div class="col-sm-6 services-full-width-text wow fadeInLeft" style="text-align:right !important;" >
                      <h3 dir="rtl"> <?=$title_ar?></h3>
                         <br>
                         <p dir="rtl">
	                    	<i><strong><?=$author_name_ar?></strong></i>
	                	</p>
                        <p dir="rtl"><?=$category_name_ar?></p>
                        <p dir="rtl">
	                    	<?=$description_ar?>
	                	</p>
                       
	                </div>
                    
                    
                    <div class="col-sm-12 services-full-width-text wow fadeInLeft">
                          <div id="divFile" >
                               <span style="float:left;"><a href="<?=$book_file_path?>" target="_blank"> <img src="<?=$cover_pic_path?>" height="100" width="100" style="border:1px solid #CCC;" /></a></span>
                               <span style="float:left; padding-left:10px; text-align:center; ">
                              <a href="<?=$book_file_path?>" target="_blank"><img src="<?=IMG_PATH?>/epub_icon.png" width="100" height="100"/><br>
                              <?=$title_en?>.epub</a></span>
                              <div style="clear:both;"></div>
						  </div>
                          <br>
                          <strong>Viewers <i class="fa fa-user"></i></strong>&nbsp;:&nbsp;<?=$cntViews?>
                          <br><br>
                         <div class="Fr-star size-3"><div class="Fr-star-value" style="width:<?=$ratePercentage?>%"></div><div class="Fr-star-bg"></div></div>     
                              
	                </div>
	            </div>
	        </div>
        </div>

         <!-- Footer -->
      <?php include("include/footer.php"); ?>