<?php include("include/header.php"); ?>
     <script src="<?=HOST_URL?>/assets/js/jquery-1.11.1.min.js"></script>

   <?php
			$unique_id = substr(md5(rand()),0,15);
         ?>    
        <!-- Page Title -->
        <div class="page-title-container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeIn">
                        <i class="fa fa-book" style="color:#0FF;"></i>
                        <h1>Add Books</h1>
                        <p>You can add books here.</p>
                        <i class="fa fa-book" style="float:right;color:#0FF;"></i>
                        <h1 dir="rtl" style="float:right; margin-top:0px !important;">&nbsp;إضافة كتب</h1>
                        <p dir="rtl" style="float:right; margin-top:10px !important;">يمكنك إضافة الكتب هنا .</p>
                    </div>
                </div>
            </div>
        </div>

        <!-- Portfolio -->
        <div class="portfolio-container">
            <div class="contact-us-container" id="div_add_book">
        	<div class="container">
	            <div class="row">
                      <div class="col-sm-12 services-full-width-text wow fadeInLeft">
                      <div style="width:50%; float:left;">
                      <h2 align="left">Add Book&nbsp;/&nbsp;إضافة كتاب</h2>  
                      </div>  
                      <div style="width:50%; float:right; padding-top:30px;">
                      <span style="float:right; padding-right:20px;"><a href="<?=HOST_URL?>/books/index/page/book/" class="big-link-1">List Books&nbsp;/&nbsp;قائمة الكتب</a></span>   
                      </div>    
                      </div>
                
                       <p style="margin-left:10px;text-align:center;">
	                    	<span class="successMsg"><?=$message?></span>
	                    </p> 
                
                
                 <form role="form" enctype="multipart/form-data" onsubmit="return validateForm();" action="" method="post" id="frmRecord" name="frmRecord" >
                        <p style="margin-left:10px;">
	                    	<span class="msgColor">Fields with * are mandatory. /.الحقول ذات علامة * إلزامية</span>
	                    </p> 
	                <div class="col-sm-6 contact-form wow fadeInLeft">
                            <div class="form-group">
                                <input type="hidden" name="tbl_book_id" id="tbl_book_id" value="<?=$unique_id?>" >

	                    		<label for="contact-name">Category / التصنيف <span class="msgColor">*</span></label><br>
	                        	<select id="tbl_book_category_id" name="tbl_book_category_id" class="contact-name" tabindex="1" autofocus>
                                <option value="">--SELECT--</option>
                                <?php for($m=0;$m<count($list_category);$m++){?>
                                <option value="<?php echo $list_category[$m]['tbl_book_category_id']; ?>"><?php echo $list_category[$m]['category_name_en']; ?> - <?php echo $list_category[$m]['category_name_ar']; ?></option>
                                <?php } ?>
                                </select>
                                <span class="err_message" id="sel_category" style="display:none;">Please select category / الرجاء تحديد الفئة</span>
	                        </div>
                            <div class="form-group">
	                    		<label for="contact-name">Language / لغة <span class="msgColor">*</span> </label><br>
	                        	<select id="tbl_language_id" name="tbl_language_id" class="contact-name" tabindex="2" >
                                <option value="">--SELECT--</option>
                                <option value="ar">Arabic</option>
                                <option value="en">English</option>
                                </select>
                                <span class="err_message" id="sel_language" style="display:none;">Please select language / الرجاء تحديد اللغة</span>
	                        </div>
                            
                           
                            <div class="form-group">
	                    		<label for="contact-name">Title [Ar] / عنوان باللغة العربية <span class="msgColor">*</span></label>
	                        	<input type="text" name="title_ar" id="title_ar"  placeholder="أدخل اسم الكتاب باللغة العربية ..." class="contact-name"  dir="rtl" tabindex="3">
                                <span class="err_message" id="sel_title_ar" style="display:none;">Please enter book name in Arabic / الرجاء إدخال اسم الكتاب باللغة العربية</span>
	                        </div> 
                            <div class="form-group">
	                    		<label for="contact-name">Title [En] / العنوان في اللغة الإنجليزية <span class="msgColor">*</span></label>
	                        	<input type="text" name="title_en" id="title_en"  placeholder="Enter book name in English..." class="contact-name" tabindex="4">
                                 <span class="err_message" id="sel_title_en" style="display:none;">Please enter book name in English / الرجاء إدخال اسم الكتاب باللغة الانجليزية</span>
	                        </div>
	                    	
	                    	<div class="form-group">
	                    		<label for="contact-email">Author Name [Ar] / اسم المؤلف باللغة العربية<span class="msgColor">*</span></label>
	                        	<input type="text" name="author_name_ar" id="author_name_ar" placeholder="أدخل اسم المؤلف في العربية ..." class="contact-email" dir="rtl" tabindex="5">
                                 <span class="err_message" id="sel_author_ar" style="display:none;">Please enter author name in Arabic / الرجاء إدخال اسم المؤلف باللغة العربية</span>
	                        </div>
                            
                            <div class="form-group">
	                    		<label for="contact-name">Author Name [En] / اسم الكاتب في اللغة الإنجليزية <span class="msgColor">*</span></label>
	                        	<input type="text" name="author_name_en" id="author_name_en"  placeholder="Enter author name in English..." class="contact-name"  tabindex="6">
                                <span class="err_message" id="sel_author_en" style="display:none;">Please enter author name in English / الرجاء إدخال اسم المؤلف في اللغة الإنجليزية</span>
	                        </div>
                            <div class="form-group" style="display:hidden;">
	                        	<label for="contact-subject">ISBN (International Standard Book Number) / Book Key Number</label>
	                        	<input type="text" name="isbn" placeholder="Enter ISBN / Book Key Number" class="contact-subject" id="isbn">
                                <span class="err_message" id="sel_isbn" style="display:none;">Please enter ISBN/ الرجاء إدخال ISBN</span>
	                        </div>
                            
                            
                              <div class="form-group" style="min-height:100px; padding-top:20px;">
                             
                               <label for="contact-subject">Cover Picture / صورة غلاف <span class="msgColor">*</span>(.png,.jpg, .gif etc.)</label>
                               <iframe id="iframe_book_cover_pic" name="iframe_book_cover_pic" src="<?=HOST_URL?>/books/iframe_show_book_cover_pic/?unique_id=<?=$unique_id?>" width="100%" height="100" frameborder="0" scrolling="no" ></iframe>
                               <span class="err_message" id="sel_cover_pic" style="display:none;">Please upload cover picture / يرجى تحميل صورة الغلاف</span>
	                          <input type="hidden" name="cover_pic" class="contact-subject" id="cover_pic" value=""  tabindex="7">
                             
                             </div>  
                             
                             <div class="form-group"  style="min-height:100px; padding-top:20px;">
                               <label for="contact-subject">Upload Book / تحميل الكتاب (.epub File Only)  <span class="msgColor">*</span></label> 
                               <img src="<?=IMG_PATH?>/epub_icon.png"  height="80" width="80"/>
                               <iframe id="iframe_show_book_file" name="iframe_show_book_file" src="<?=HOST_URL?>/books/iframe_show_book_file/?unique_id=<?=$unique_id?>" width="100%" height="100" frameborder="0" scrolling="no" ></iframe>
                              
                                <span class="err_message" id="sel_book_file" style="display:none;">Please upload book file / يرجى تحميل ملف الكتاب</span>
	                              <input type="hidden" name="book_file" class="contact-subject" id="book_file" value="" tabindex="8" >
                             
                            </div> 
                           
	                </div>
                    
                    <div class="col-sm-6 contact-form wow fadeInLeft"> 
                             <div class="form-group">
	                        	<label for="contact-message">Description [Ar] / وصف باللغة العربية <span class="msgColor">*</span></label>
	                        	<textarea name="description_ar" placeholder="وصف..." class="contact-message" id="description_ar" dir="rtl" tabindex="9"></textarea>
                                 <span class="err_message" id="sel_book_desc_ar" style="display:none;">Please enter description in Arabic / الرجاء إدخال وصف باللغة العربية</span>
	                        </div>
                              
                            <div class="form-group">
	                        	<label for="contact-message">Description [En] / وصف باللغة الإنجليزية <span class="msgColor">*</span> </label>
	                        	<textarea name="description_en" placeholder="Description..." class="contact-message" id="description_en" tabindex="10"></textarea>
                                 <span class="err_message" id="sel_book_desc_en" style="display:none;">Please enter description in English / الرجاء إدخال وصف باللغة الإنجليزية</span>
	                        </div>
                             
                              <div class="form-group" id="div_age_group" >
	                        	<label for="contact-message" style="width:100%">Recommended For / الموصى بها <span class="msgColor">*</span></label>
                                <br>
                                
	                        	<?php
								if(count($list_age_group)>0){ 
								  for($j=0;$j<count($list_age_group);$j++)
								  {?>
									 <div class="checkbox">
  										<label><input type="checkbox" name="age_group"  value="<?php echo $list_age_group[$j]['tbl_age_group_id']?>" onClick="getAgeGroup();" tabindex="11" ><?php echo $list_age_group[$j]['age_from']."&nbsp;-&nbsp;".$list_age_group[$j]['age_to']."&nbsp;(&nbsp;".$list_age_group[$j]['group_name']."&nbsp;)"; ?></label>
									</div> 
								<?php  
								}
							
								 } ?>
                                 <span class="err_message" id="sel_age_group" style="display:none;">Please select age group / الرجاء تحديد الفئة العمرية</span>
                                 <input type="hidden" name="user_age_group" class="contact-name" id="user_age_group" value="" tabindex="12">
                                 
                                 <div style="clear:both;"></div>
	                        </div>
                            
                             <div class="form-group" style="padding-top:13px;">
	                    		<label for="contact-name">Total Page / مجموع الصفحة <span class="msgColor">*</span></label>
	                        	<input type="text" name="total_page" placeholder="Enter total page..." class="contact-name" id="total_page" value="" tabindex="12" maxlength="8" >
                                <span class="err_message" id="sel_total_page" style="display:none;">Please enter total page / الرجاء إدخال إجمالي عدد الصفحات</span>
	                        </div>
                            
                            <div class="form-group" style="padding-top:13px;">
	                    		<label for="contact-name">Point / نقطة <span class="msgColor">*</span></label>
	                        	<input type="text" name="book_point" placeholder="Enter point..." class="contact-name" id="book_point" tabindex="13" maxlength="4" >
                                <span class="err_message" id="sel_book_point" style="display:none;">Please enter point / الرجاء إدخال نقطة</span>
	                        </div>
                            
                             <div class="form-group" style="padding-top:13px;">
	                    		<label for="contact-name">Price / السعر <span class="msgColor">*</span></label>
	                        	<input type="text" name="book_price" placeholder="Enter price..." class="contact-name" id="book_price" tabindex="14" maxlength="6" >
                                <span class="err_message" id="sel_book_price" style="display:none;">Please enter price / الرجاء إدخال سعر</span>
	                        </div>
                          
	                       <button type="submit" class="btn" tabindex="14">Submit / عرض</button>
	                 
	                </div>
                   
                     </form>
	             
	            </div>
	        </div>
        </div>
        
        
        </div>
        
        


	    <script language="javascript">
	
	     function getAgeGroup()
		 {
				var age_criteria = '';
				$('#div_age_group input[type="checkbox"]:checked').each(function() { 
				  age_criteria = age_criteria + $(this).val()+'|';
				  //$('#user_age_group').append($(this).val()+'|');
				});
				$('#user_age_group').val(age_criteria);
		 }
	
	
		
		function validateForm() {
			
			if (val_tbl_book_category_id() == false  ||  val_tbl_language_id() == false   || val_title() == false || val_author() == false ||  val_cover_pic() == false ||  val_book_file() == false ||  val_description() == false || val_age_criteria() == false || val_point() == false ) {
				return false;
			} else {
				saveBook();
			}	
		}//function validateForm
      //  tbl_language_id
	  
	  //val_isbn() == false || 
	  
		//VALIDATE USER ID
        function val_tbl_book_category_id() {
			var strCat = $("#tbl_book_category_id").val();	
			if(strCat==""){
				$("#sel_category").show('20');
				return false;
			}else{
				$("#sel_category").hide('20');
			}
			return true;
		}
		
		//VALIDATE LANGUAGE
        function val_tbl_language_id() {
			var strLan = $("#tbl_language_id").val();	
			if(strLan==""){
				$("#sel_language").show('20');
				return false;
			}else{
				$("#sel_language").hide('20');
				
			}
			return true;
		}
		
		//VALIDATE title
		function val_title() {
			var strTitleEn = $("#title_en").val();
			var strTitleAr = $("#title_ar").val();	
			
		    if(strTitleAr==""){
				$("#sel_title_ar").show('20');
				return false;
			}
			else{
				
				$("#sel_title_ar").hide('20');
			}
			
			if(strTitleEn==""){
				$("#sel_title_en").show('20');
				return false;
			}else{
				$("#sel_title_en").hide('20');
			}
			return true;
		}
		
		function val_author() {
			var strAuthorEn = $("#author_name_en").val();
			var strAuthorAr = $("#author_name_ar").val();	
			
			if(strAuthorAr==""){
				$("#sel_author_ar").show('20');
				return false;
			}else{
				
				$("#sel_author_ar").hide('20');
			}
			
			if(strAuthorEn==""){
				$("#sel_author_en").show('20');
				return false;
			}else{
				$("#sel_author_en").hide('20');
			}
			
			return true;
		}
		
		
		function val_isbn() {
			var strISBN = $("#isbn").val();
			
			if(strISBN==""){
				$("#sel_isbn").show('20');
				return false;
			}else{
				$("#sel_isbn").hide('20');
			}
			
			return true;
		}
		
		
		
		function val_cover_pic() {
			var strCoverPic = $("#cover_pic").val();
			if(strCoverPic == ""){
				$("#sel_cover_pic").show('20');
				return false;
			}else{
				$("#sel_cover_pic").hide('20');
			}
			return true;
		}
		
		function val_book_file() {
			var strCoverPic = $("#book_file").val();
			if(strCoverPic==""){
				$("#sel_book_file").show('20');
				return false;
			}else{
				$("#sel_book_file").hide('20');
			}
			return true;
		}
		
		
		function val_description() {
			var strBookDescEn = $("#description_en").val();
			var strBookDescAr = $("#description_ar").val();	
			
			
			if(strBookDescAr==""){
				$("#sel_book_desc_ar").show('20');
				return false;
			}else{
				
				$("#sel_book_desc_ar").hide('20');
			}
			
			if(strBookDescEn==""){
				$("#sel_book_desc_en").show('20');
				return false;
			}else{
				$("#sel_book_desc_en").hide('20');
				
			}
			return true;
		}
		
		function val_age_criteria() {
			var strAgeGroup = $("#user_age_group").val();	
			if(strAgeGroup==""){
				$("#sel_age_group").show('20');
				return false;
			}else{
				$("#sel_age_group").hide('20');
				
			}
			return true;
		}
		
		
		
		function val_point() {
			var strBookPoint = $("#book_point").val();
			if(strBookPoint==""){
				$("#sel_book_point").show('20');
				return false;
			}else{
				$("#sel_book_point").hide('20');
			}
			return true;
		}
		
		$('#book_point').keypress(function(event){
			   if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
				   event.preventDefault(); //stop character from entering input
			   }
			});
		
		//VALIDATE CHECK LOGIN 
		  function saveBook()
		  {
				
				try { 
					$.ajax({
					type: "POST",
					dataType:"text",
					url: "<?=HOST_URL?>/books/add_book",
					data: {
						tbl_book_id:$("#tbl_book_id").val(),
						tbl_book_category_id  : $("#tbl_book_category_id").val(),
						tbl_language_id: $("#tbl_language_id").val(), 
						title_en: $("#title_en").val(),
						title_ar: $("#title_ar").val(),
						author_name_en: $("#author_name_en").val(),
						author_name_ar: $("#author_name_ar").val(),
						isbn: $("#isbn").val(),
						description_en: $("#description_en").val(),
						description_ar: $("#description_ar").val(),
						user_age_group: $("#user_age_group").val(),
						book_point: $("#book_point").val(),
						total_page: $("#total_page").val(),
						book_price: $("#book_price").val(),
						is_ajax: true
					},
					success: function(data) {
						
						/*if(data=="N")
						{
							alert("Book is already exist");
							return false;
						}else{
							alert("Book is added successfully");
							window.location.href="<?=HOST_URL?>/books/add_book?page=book";
						}*/
					},
					error: function() {
						$('#pre_loader').css('display','none');	
					}, 
					complete: function() {
						$('#pre_loader').css('display','none');	
					}
		
					});
				} catch(e) {
					//alert(e)	
				}	
		  }
        </script>


        <!-- Footer -->
      <?php include("include/footer.php"); ?>