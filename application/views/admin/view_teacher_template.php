<?php include(ROOT_ADMIN_PATH."/admin/include/teacher_header.php"); ?>

<!--WRAPPER-->
<div class="wrapper"> 
  
      <!--TOP BAR-->
      <?php include(ROOT_ADMIN_PATH."/admin/include/teacher_header_top_bar.php");?>
      <!--/TOP BAR--> 
      
      <!--LEFT-->
      <?php include(ROOT_ADMIN_PATH."/admin/include/teacher_left.php");?>
      <!--/LEFT--> 
      
      
      <!--INC FILE-->
      <?php
      	switch($page) {
			case("teacher_welcome"): {
				include(ROOT_ADMIN_PATH."/admin/teacher_welcome.php");
			break;	
			}
			case("view_teacher_all_classes"): {
				include(ROOT_ADMIN_PATH."/admin/view_teacher_all_classes.php");
			break;	
			}	
			case("view_teacher_event_calendar"): {
				include(ROOT_ADMIN_PATH."/admin/view_teacher_event_calendar.php");
			break;	
			}	
			case("view_teacher_leave_applications"): {
				include(ROOT_ADMIN_PATH."/admin/view_teacher_leave_applications.php");
			break;	
			}	
			case("view_teacher_groups"): {
				include(ROOT_ADMIN_PATH."/admin/view_teacher_groups.php");
			break;	
			}	
			case("view_teacher_messages"): {
				include(ROOT_ADMIN_PATH."/admin/view_teacher_messages.php");
			break;	
			}
			
			case("view_cards_report"): {
				include(ROOT_ADMIN_PATH."/admin/view_cards_report.php");
			break;	
			}
			case("view_cards_report_detailed"): {
				include(ROOT_ADMIN_PATH."/admin/view_cards_report_detailed.php");
			break;	
			}
			
			case("view_points_report"): {
				include(ROOT_ADMIN_PATH."/admin/view_points_report.php");
			break;	
			}
			case("view_points_report_detailed"): {
				include(ROOT_ADMIN_PATH."/admin/view_points_report_detailed.php");
			break;	
			}
			
			case("view_attendance_report"): {
				include(ROOT_ADMIN_PATH."/admin/view_attendance_report.php");
			break;	
			}
			case("view_attendance_report_detailed"): {
				include(ROOT_ADMIN_PATH."/admin/view_attendance_report_detailed.php");
			break;	
			}
			
			case("view_performance_report"): {
				include(ROOT_ADMIN_PATH."/admin/view_performance_report.php");
			break;	
			}
			
			case("view_progress_report"): {
				include(ROOT_ADMIN_PATH."/admin/view_progress_report.php");
			break;	
			}	
		}
	  ?>
      <!--/INC FILE--> 
      
      
      <!--FOOTER COPYRIGHT-->
      <?php include(ROOT_ADMIN_PATH."/admin/include/footer_copyright.php");?>
      <!--/FOOTER COPYRIGHT--> 
      
      <!--MENU RIGHT-->
      <?php include(ROOT_ADMIN_PATH."/admin/include/menu_slide_right.php");?>
      <!--/MENU RIGHT--> 
  
</div>
<!--/WRAPPER-->

<?php include(ROOT_ADMIN_PATH."/admin/include/footer.php");?>