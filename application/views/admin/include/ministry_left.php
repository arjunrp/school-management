<aside class="main-sidebar"> 
    
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar"> 
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header">MENU </li>
            <li <?php if($sub_menu == "admin_users") {echo 'class="active"';}?>>
            	<a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/admin_user/all_ministry_users"><i class="fa fa-user-plus"></i> <span>Admin Users</span></a>
            </li>

            <li class="treeview <?php if($sub_menu == "message_to_teachers" || $sub_menu == "ministry_message_to_parents" || $sub_menu == "school_message_parents") {echo " active";}?> "> <a href="#"><i class="fa fa-comments"></i> <span>Messages</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
              <ul class="treeview-menu">
                <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/ministry_message_to_teachers">Message To Teachers</a></li>
                <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/ministry_message_to_parents">Message To Parents</a></li>
                <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/ministry_message_from_parents">Message From Parents</a></li>
              </ul>
            </li>

            <li class="treeview <?php if($menu == "parenting") {echo " active";}?> "> <a href="#"><i class="fa fa-user"></i> <span>Parenting</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
              <ul class="treeview-menu">
                <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/ministry_parenting_category">Category Management</a></li>
                <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/ministry_parenting">Parenting Management</a></li>
              </ul>
            </li>

            <li class="treeview <?php if($menu == "configuration") {echo " active";}?> "> <a href="#"><i class="fa fa-gear"></i> <span>Configuration</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
              <ul class="treeview-menu">
                <li><a <?php if($sub_menu == "student_cards_conf") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/ministry_card/all_ministry_cards">Student Cards</a></li>
                <li><a <?php if($sub_menu == "student_points_conf") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/ministry_point/student_points_conf_ministry">Student Points</a></li>
              </ul>
           </li>
      </ul>
      <!-- /.sidebar-menu --> 
    </section>
    <!-- /.sidebar --> 
    
</aside>