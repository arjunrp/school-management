<!-- REQUIRED JS SCRIPTS -->

<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="<?=HOST_URL?>/assets/admin/bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?=HOST_URL?>/assets/admin/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?=HOST_URL?>/assets/admin/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?=HOST_URL?>/assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?=HOST_URL?>/assets/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?=HOST_URL?>/assets/admin/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?=HOST_URL?>/assets/admin/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?=HOST_URL?>/assets/admin/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap time picker -->
<script src="<?=HOST_URL?>/assets/admin/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?=HOST_URL?>/assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?=HOST_URL?>/assets/admin/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?=HOST_URL?>/assets/admin/plugins/fastclick/fastclick.js"></script>
<!-- Charts -->
<script src="<?=HOST_URL?>/assets/admin/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=HOST_URL?>/assets/admin/dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?=HOST_URL?>/assets/admin/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=HOST_URL?>/assets/admin/dist/js/demo.js"></script>
<!-- fullCalendar 2.2.5 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?=HOST_URL?>/assets/admin/plugins/fullcalendar/fullcalendar.min.js"></script>
<!-- InputMask -->
<script src="<?=HOST_URL?>/assets/admin/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?=HOST_URL?>/assets/admin/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?=HOST_URL?>/assets/admin/plugins/input-mask/jquery.inputmask.extensions.js"></script>


<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. Slimscroll is required when using the
     fixed layout. -->
     
     <script>
     $(function() {
        function _fix() {
            var h = $(window).height();
            var w = $(window).width();
            $("#preview-iframe").css({
                width: w + "px",
                height: (h - 50) + "px"
            });
        }
        _fix();
        $(window).resize(function() {
            _fix();
        });
        $('[data-toggle="tooltip"]').tooltip();

        function iframe_width(width) {
            $("#preview-iframe").animate({width: width}, 500);
        }

        $("#display-full").click(function(e){
            e.preventDefault();
            iframe_width("100%");
        });

        $("#display-940").click(function(e){
            e.preventDefault();
            iframe_width("940px");
        });

        $("#display-480").click(function(e){
            e.preventDefault();
            iframe_width("480px");
        });

        $("#remove-frame").click(function(e){
            e.preventDefault();
            window.location.href = "http://almsaeedstudio.com/themes/AdminLTE/index2.html";
        });

    });

</script>
<script language="javascript">
function deactivate_me(obj) {
	$(obj).removeClass("label-success");	
	$(obj).addClass("label-danger");	
	
	$(obj).html("Deactivate Me");	
}

function activate_me(obj) {
	$(obj).addClass("label-success");	
	$(obj).removeClass("label-danger");	
	
	$(obj).html("Activate Me");	
}

function reset_activate(obj) {
	$(obj).addClass("label-success");	
	$(obj).removeClass("label-danger");	
	
	$(obj).html("Active");	
}

function reset_deactivate(obj) {
	$(obj).addClass("label-danger");	
	$(obj).removeClass("label-success");	
	
	$(obj).html("Inactive");	
}
</script>

<!--MODAL-->
<style>
	.alert-danger, .alert-error {
		border-color: #FFFFFF;
	}
	.alert {
		margin-bottom: 0px;
	}	
</style>
<script language="javascript">



function my_alert(msg, opt_skin) {//opt_skin: red, green [red is default]
	$('#msg_body').html(msg);
	
	if (opt_skin == 'red' || opt_skin == '') {
		$('#my_alert_box').removeClass("alert-success");
		$('#alert_icon').removeClass("icon fa fa-info");

		$('#my_alert_box').addClass("alert-danger");
		$('#alert_icon').addClass("icon fa fa-ban");
	}
	if (opt_skin == 'green') {
		$('#my_alert_box').removeClass("alert-danger");
		$('#alert_icon').removeClass("icon fa fa-ban");

		$('#my_alert_box').addClass("alert-success");
		$('#alert_icon').addClass("icon fa fa-info");
	}
	
	$('#alert_box').modal('show');					
}

function close_popup() {
	$('#alert_box').modal('hide');
	$('#button_confirm').hide();
	
	//Remove spinner if active
	$('#my_button').removeClass("fa fa-spin fa-refresh");//For spinner
}
</script>

<div class="modal fade" id="alert_box" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div id="my_alert_box" class="alert alert-danger alert-dismissible">
        <button aria-hidden="true" class="close" type="button" onclick="close_popup()">×</button>
        <h4><i id="alert_icon" class="icon fa fa-ban"></i><span id="msg_title"> Message</span></h4>
        <div id="msg_body" style="padding:10px 0px 20px 0px"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="close_popup()">Close</button>
        <button id="button_confirm" name="button_confirm" style="display:none" type="button" class="btn btn-default" onclick="ajax_delete()">Confirm</button>
        <button id="button_confirm_assign" name="button_confirm_assign" style="display:none" type="button" class="btn btn-default" onclick="ajax_assign_enquiry()">Confirm</button>
      </div>
    </div>
  </div>
</div>
<!--/MODAL-->



<div id="pre-loader"></div>

</body>
</html>	