<?php
	$pic_show_path = "";
	if (trim($_SESSION['aqdar_smartcare']['teacher_picture_sess']) != "") {
		$pic_show_path = HOST_URL."/images/teacher/".$_SESSION['aqdar_smartcare']['teacher_picture_sess'];
	}
?> 
  <header class="main-header"> 
    
    <!-- Logo --> 
    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home/teacher" class="logo"> 
    <!-- mini logo for sidebar mini 50x50 pixels --> 
    <span class="logo-mini"><b>A</b>dmin</span> 
    <!-- logo for regular state and mobile devices --> 
    <span class="logo-lg"><b>Administration</b></span> </a> 
    
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation"> 
      <!-- Sidebar toggle button--> 
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> </a> 
      
      <span style="line-height:50px; padding-left:60%;"> <a style="color:#fff; font-size:16px; cursor:pointer;" href="<?=HOST_URL?>/<?=LAN_SEL?>/school_web/"> << Back to Website </a></span>

      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- User Account Menu -->
          <li class="dropdown user user-menu"> 
            <!-- Menu Toggle Button --> 
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
            <!-- The user image in the navbar--> 
            <?php
            	if (trim($pic_show_path) != "") { 
			?>
    	        <img src="<?=$pic_show_path?>" class="user-image" alt="User Image"> 
            <?php } else { ?>
	            <img src="<?=HOST_URL?>/assets/admin/images/users/default_avatar.png" class="user-image" alt="User Image"> 
			<?php } ?>
            <!-- hidden-xs hides the username on small devices so only the image appears. --> 
            <span class="hidden-xs"><?=$_SESSION['aqdar_smartcare']['teacher_first_name_sess']?> <?=$_SESSION['aqdar_smartcare']['teacher_last_name_sess']?></span> </a>
            
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header"> 
				<?php
                    if (trim($pic_show_path) != "") { 
                ?>
                  <img src="<?=$pic_show_path?>" class="img-circle" alt="User Image">
				<?php } else { ?>
                    <img src="<?=HOST_URL?>/assets/admin/images/users/default_avatar.png" class="img-circle" alt="User Image">
                <?php } ?>
                  
                <p> <?=$_SESSION['aqdar_smartcare']['teacher_first_name_sess']?> <?=$_SESSION['aqdar_smartcare']['teacher_last_name_sess']?> <small>Member since <?=$_SESSION['aqdar_smartcare']['added_date_sess']?></small> </p>
              </li>
              
              <!-- Menu Body -->
              <li class="user-body">
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left"> <a href="#" class="btn btn-default btn-flat">Profile</a> </div>
                <div class="pull-right"> <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/signout" class="btn btn-default btn-flat">Sign out</a> </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li> <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a> </li>
        </ul>
      </div>
    </nav>
  </header>
