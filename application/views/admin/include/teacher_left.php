<aside class="main-sidebar"> 
    
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar"> 
      
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image"> 
            <?php
            	if (trim($pic_show_path) != "") { 
			?>
        	<img src="<?=$pic_show_path?>" class="img-circle" alt="User Image">
            <?php } else { ?>
	            <img src="<?=HOST_URL?>/assets/admin/images/users/default_avatar.png" class="img-circle" alt="User Image">
			<?php } ?>
        </div>
        <div class="pull-left info">
          <p><?=$_SESSION['aqdar_smartcare']['teacher_first_name_sess']?> <?=$_SESSION['aqdar_smartcare']['teacher_last_name_sess']?></p>
          <!-- Status --> 
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a> </div>
      </div>
      
      <!-- search form (Optional) -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i> </button>
          </span> </div>
      </form>
      <!-- /.search form --> 
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header">MENU </li>
        <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/teacher_all_classes?module_id=m_contact_parent&contact_parents=Y"><i class="fa fa-phone"><!--<img src="<?=HOST_URL?>/images/icon_contact_parents.png" />--></i> <span>Contact Parents</span></a></li>
        <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/teacher_all_classes?module_id=m_give_points">
        	<i class="fa fa-line-chart"><?php /*?><img src="<?=HOST_URL?>/images/icon_assign_points.png" /><?php */?></i> <span>Assign Points</span></a>
        </li>
        <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/teacher_all_classes?module_id=m_contact_parent&issue_cards=Y">
        	<i class="fa fa-clone"><?php /*?><img src="<?=HOST_URL?>/images/icon_issue_cards.png" /><?php */?></i> <span>Issue Cards</span></a>
        </li>
        
         <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/teacher_all_classes?module_id=m_contact_parent&performance_calc=Y">
        	<i class="fa fa-area-chart"><?php /*?><img src="<?=HOST_URL?>/images/icon_issue_cards.png" /><?php */?></i> <span>Performance</span></a>
        </li>
        
         <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/teacher_all_classes?module_id=m_contact_parent&progress_report=Y">
        	<i class="fa fa-bar-chart "><?php /*?><img src="<?=HOST_URL?>/images/icon_issue_cards.png" /><?php */?></i> <span>Progress Report</span></a>
        </li>
        
        
        
        <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/teachers_roles_web/?user_id=<?=$_SESSION['aqdar_smartcare']['tbl_teacher_id_sess']?>&role=T&lan=en&school_id=<?=$_SESSION['aqdar_smartcare']['tbl_school_id_sess']?>&module_id=m_issue_cards"><i class="fa fa-comments"><?php /*?><img src="<?=HOST_URL?>/images/icon_view_messages.png" /><?php */?></i> <span>View Messages</span></a></li>
        <?php /*?><li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/teacher_all_classes?module_id=cdrpt">
        	<i class="fa"><img src="<?=HOST_URL?>/images/icon_send_report.png" /></i> <span>Send Report</span></a>
        </li><?php */?>
        <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/teacher_all_classes?module_id=m_teacher_attendance"><i class="fa fa-hand-paper-o"><?php /*?><img src="<?=HOST_URL?>/images/icon_attendance_register.png" /><?php */?></i> <span>Attendance Register</span></a></li>
        <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/leave/get_leaves_web/?lan=en&user_id=<?=$_SESSION['aqdar_smartcare']['tbl_teacher_id_sess']?>"><i class="fa fa-user-times"><?php /*?><img src="<?=HOST_URL?>/images/icon_view_absentees.png" /><?php */?></i> <span>View Absentees</span></a></li>
        <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/event/event_calendar"><i class="fa fa-calendar"><?php /*?><img src="<?=HOST_URL?>/images/icon_event_calendar.png" /><?php */?></i> <span>Event Calendar</span></a></li>
        
        
           
       
        <li class="treeview <?php if($menu == "reports") {echo " active";}?> "> <a href="#"><i class="fa fa-file-text-o"></i> <span>
        <?php if(LAN_SEL=="ar"){ ?>السجلات<?php }else{?> Reports <?php } ?></span> <span class="pull-right-container"> <i class="fa fa-angle-right pull-right"></i> </span> </a>
          <ul class="treeview-menu">
                
                 <li><a <?php if($sub_menu == "cards_report") {?> style="color:#dd4b39;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/school_reports/">
                <?php if(LAN_SEL=="ar"){ ?>بطاقات<?php }else{?> Cards <?php } ?>
                </a></li>
            
                <li><a <?php if($sub_menu == "points_report") {?> style="color:#dd4b39;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/school_point_reports">
                <?php if(LAN_SEL=="ar"){ ?>نقاط <?php }else{?>  Points <?php } ?>
                </a></li>
            
                 <li ><a <?php if($sub_menu == "attendance_report") {?> style="color:#dd4b39;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/school_attendance_reports/">
                <?php if(LAN_SEL=="ar"){ ?>الحضور<?php }else{?> Attendance <?php } ?>
                </a></li>
            
                 <li ><a <?php if($sub_menu == "student_performance_report") {?> style="color:#dd4b39;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/student_performance_report/">
                <?php if(LAN_SEL=="ar"){ ?>تقرير الأداء<?php }else{?> Performance Report <?php } ?>
                </a></li>
            
                 <li ><a <?php if($sub_menu == "student_progress_report") {?> style="color:#dd4b39;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/student_progress_report/">
                <?php if(LAN_SEL=="ar"){ ?>تقرير الأداء<?php }else{?> Progress Report <?php } ?>
                </a></li>

          </ul>
        </li>   
         
        
        
        <!--<li><a href="#"><i class="fa"><img src="<?=HOST_URL?>/images/icon_help.png" /></i> <span>Help</span></a></li>-->
      </ul>
      <!-- /.sidebar-menu --> 
    </section>
    <!-- /.sidebar --> 
</aside>

