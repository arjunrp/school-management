<?php
	$pic_show_path = "";
	
	if (trim($_SESSION['aqdar_smartcare']['admin_picture_sess']) != "") {
		$pic_show_path = HOST_URL."/assets/uploads/".$_SESSION['aqdar_smartcare']['admin_picture_sess'];
	}else if(trim($_SESSION['aqdar_smartcare']['teacher_picture_sess']) != "") {
		$pic_show_path = HOST_URL."/assets/uploads/".$_SESSION['aqdar_smartcare']['judge_picture_sess'];
	}else{
		$pic_show_path = HOST_URL."/assets/admin/images/users/".$_SESSION['aqdar_smartcare']['admin_picture_sess'];
	}
?> 
  <header class="main-header"> 
    
    <!-- Logo --> 
    <?php if (trim($_SESSION['aqdar_smartcare']['user_type_sess']) == "admin") { ?>
    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/admin_user/all_users" class="logo"> 
    <?php } else { ?>
	    <a href="#, return false" class="logo"> 
	<?php } ?>
    
    <?php if(LAN_SEL=="ar"){ ?>
    
     <!-- mini logo for sidebar mini 50x50 pixels --> 
    <span class="logo-mini"><b>إدارة</b></span> 
    <!-- logo for regular state and mobile devices --> 
    <span class="logo-lg"><b>إدارة</b></span> 
    
    <?php } else { ?>
    <!-- mini logo for sidebar mini 50x50 pixels --> 
    <span class="logo-mini"><b>A</b>dmin</span> 
    <!-- logo for regular state and mobile devices --> 
    <span class="logo-lg"><b>Administration</b></span> 
    <?php } ?>
    
    
    </a>
    <?php 
    if($_SESSION['aqdar_smartcare']['school_logo']<>"")
	{
	   $logo_url= HOST_URL."/images/logo/".$_SESSION['aqdar_smartcare']['school_logo'];	
	}
	if($_SESSION['aqdar_smartcare']['admin_pic']<>"")
	{
		$admin_pic = $_SESSION['aqdar_smartcare']['admin_pic'];
	}
	
    ?>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button--> 
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> </a> 
      <span class="logo-lg"  style="color:#fff; line-height:50px; padding-left:30%; font-size:20px;"><?php if($_SESSION['aqdar_smartcare']['school_logo']<>"")
	{ ?> <img class="img-circle" src="<?=$logo_url?>" height="45" width="45" /> <?php } ?> &nbsp;&nbsp;<b><?=strtoupper($_SESSION['aqdar_smartcare']['school_name'])?></b></span>
    
    
      <span style="line-height:50px; padding-left:20%;"> <a style="color:#fff; font-size:16px; cursor:pointer;" href="<?=HOST_URL?>/<?=LAN_SEL?>/school_web/"> << Back to Website </a></span>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- User Account Menu -->
          <li class="dropdown user user-menu"> 
            <!-- Menu Toggle Button --> 
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
            <!-- The user image in the navbar--> 
            <?php
            	if (trim($admin_pic) != "") { 
			?>
    	        <img src="<?=$admin_pic?>" class="user-image" alt="User Image"> 
            <?php } else { ?>
	            <img src="<?=HOST_URL?>/assets/admin/images/users/default_avatar.png" class="user-image" alt="User Image"> 
			<?php } ?>
            <!-- hidden-xs hides the username on small devices so only the image appears. --> 
            <span class="hidden-xs">
                	<?php if (trim($_SESSION['aqdar_smartcare']['user_type_sess']) == "admin") { ?>
						<?=$_SESSION['aqdar_smartcare']['admin_first_name_sess']?> <?=$_SESSION['aqdar_smartcare']['admin_last_name_sess']?>
                    <?php }  else { ?>
						<?=$_SESSION['aqdar_smartcare']['judge_first_name_en_sess']?> <?=$_SESSION['aqdar_smartcare']['judge_last_name_en_sess']?>
					<?php } ?>
            </span> 
            </a>
            
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header"> 
				<?php
                    if (trim($admin_pic) != "") { 
                ?>
                  <img src="<?=$admin_pic?>" class="img-circle" alt="User Image">
				<?php } else { ?>
                    <img src="<?=HOST_URL?>/assets/admin/images/users/default_avatar.png" class="img-circle" alt="User Image">
                <?php } ?>
                  
                <p> 
                	<?php if (trim($_SESSION['aqdar_smartcare']['user_type_sess']) == "admin") { ?>
						<?=$_SESSION['aqdar_smartcare']['admin_first_name_sess']?> <?=$_SESSION['aqdar_smartcare']['admin_last_name_sess']?> <small>Member since <?=$_SESSION['aqdar_smartcare']['added_date_sess']?></small>
                    <?php }  else { ?>
						<?=$_SESSION['aqdar_smartcare']['judge_first_name_en_sess']?> <?=$_SESSION['aqdar_smartcare']['judge_last_name_en_sess']?> <small>Member since <?=$_SESSION['aqdar_smartcare']['added_date_sess']?></small>
					<?php } ?>
                </p>
              </li>
              
              <!-- Menu Body -->
              <li class="user-body">
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left"> <a href="#" class="btn btn-default btn-flat">Profile</a> </div>
                <div class="pull-right"> <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/signout" class="btn btn-default btn-flat">Sign out</a> </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li> <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a> </li>
        </ul>
      </div>
    </nav>
  </header>
