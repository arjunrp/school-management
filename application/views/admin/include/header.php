<?php
	if (trim($_SESSION['aqdar_smartcare']['tbl_admin_user_id_sess']) == "" && $page!= "login_form") { 
		/*header("Location: ".HOST_URL."/".LAN_SEL."/admin/admin_user/login_form");
		exit();*/
		$url = HOST_URL."/".LAN_SEL."/school_web/";
		echo "<script type='text/javascript'>window.location.href = '".$url."';</script>";
        exit();
	}
	
?>
<!DOCTYPE html>
<html>
<head> 
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?=TITLE_ADMIN?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
   <?php if(LAN_SEL=="ar"){ ?>
  <link rel="stylesheet" href="<?=HOST_URL?>/assets/admin/bootstrap/css/bootstrap.min_ar.css">
  <?php }else{ ?>
  <link rel="stylesheet" href="<?=HOST_URL?>/assets/admin/bootstrap/css/bootstrap.min.css">
  <?php } ?>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <?php if(LAN_SEL=="ar"){ ?>
  	<link rel="stylesheet" href="<?=HOST_URL?>/assets/admin/dist/css/AdminLTE.min_ar.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?=HOST_URL?>/assets/admin/dist/css/skins/_all-skins.min_ar.css">
  <?php }else{ ?>
    <link rel="stylesheet" href="<?=HOST_URL?>/assets/admin/dist/css/AdminLTE.min.css"> 
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?=HOST_URL?>/assets/admin/dist/css/skins/_all-skins.min.css">
  <?php } ?>
 
  <!-- iCheck -->
  <link rel="stylesheet" href="<?=HOST_URL?>/assets/admin/plugins/iCheck/flat/blue.css">
  <link rel="stylesheet" href="<?=HOST_URL?>/assets/admin/plugins/iCheck/square/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?=HOST_URL?>/assets/admin/plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?=HOST_URL?>/assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?=HOST_URL?>/assets/admin/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?=HOST_URL?>/assets/admin/plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?=HOST_URL?>/assets/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?=HOST_URL?>/assets/admin/plugins/timepicker/bootstrap-timepicker.min.css">


  <!-- fullCalendar 2.2.5-->
  <link rel="stylesheet" href="<?=HOST_URL?>/assets/admin/plugins/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="<?=HOST_URL?>/assets/admin/plugins/fullcalendar/fullcalendar.print.css" media="print">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- jQuery 2.2.3 -->
  <script src="<?=HOST_URL?>/assets/admin/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  
 
  <style>
	#pre-loader  {
		display:none;
		width: 200px;
		height: 200px;
		position: absolute;
		left: 50%;
		top: 50%;
		background-image: url('<?=IMG_PATH?>/preloader/preloader_2.gif');
		background-repeat: no-repeat;
		background-position: center;
		margin: -100px 0 0 -100px;
	} 
  </style>
  <?php if(LAN_SEL=="ar"){ ?>
   <style>
    
	.sidebar-menu > li > a > .fa {
	   float:right !important;
	   margin-right:10px;
	}
	
	 .sidebar-menu > li > a{
		 text-align:right;
	 }
	
	/*.sidebar-menu > li > a > span {
		margin-right:40px !important;
		display:block;
	}*/
	</style>
  
  <?php } ?>
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini <?=$css?>">
