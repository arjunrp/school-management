<aside class="main-sidebar"> 
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar"> 
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header">MENU </li>
            <li <?php if($sub_menu == "admin_users") {echo 'class="active"';}?>>
            	<a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/admin_user/all_timeline_users"><i class="fa fa-user-plus"></i> <span>Admin Users</span></a>
            </li>
            
            <li>
            	<a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/website_feedback"><i class="fa fa-comments"></i> <span>Messages</span></a>
            </li>
            
            <li <?php if($sub_menu == "schools") {echo 'class="active"';}?>>
            	<a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/all_schools"><i class="fa fa-university "></i> <span>Schools</span></a>
           </li>
      </ul>
      <!-- /.sidebar-menu --> 
    </section>
    <!-- /.sidebar --> 
    
</aside>