<?php
if($_SESSION['aqdar_smartcare']['admin_pic']<>"")
	{
		$admin_pic = $_SESSION['aqdar_smartcare']['admin_pic'];
	}
?>
<aside class="main-sidebar"> 
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar"> 
      <!-- Sidebar user panel (optional) -->
   <?php /*?>   <div class="user-panel">
        <div class="pull-left image"> 
            <?php
            	if (trim($admin_pic) != "") { 
			?>
        	<img src="<?=$admin_pic?>" height="50" class="img-circle" alt="User Image">
            <?php } else { ?>
	            <img src="<?=HOST_URL?>/assets/admin/images/users/default_avatar.png" class="img-circle" alt="User Image">
			<?php } ?>
        </div>
        <div class="pull-left info">
          <p>
				<?php if (trim($_SESSION['aqdar_smartcare']['user_type_sess']) == "admin") { ?>
                    <?=$_SESSION['aqdar_smartcare']['admin_first_name_sess']?> <?=$_SESSION['aqdar_smartcare']['admin_last_name_sess']?>
                <?php }  else { ?>
                    <?=$_SESSION['aqdar_smartcare']['judge_first_name_en_sess']?> <?=$_SESSION['aqdar_smartcare']['judge_last_name_en_sess']?>
                <?php } ?>
          </p>
          <!-- Status --> 
          <a href="#"><i class="fa fa-circle text-success"></i>  <?php if(LAN_SEL=="ar"){?> عبر الانترنت <?php }else{?> Online <?php } ?></a> 
          </div>
      </div><?php */?>
      
      <!-- search form (Optional) -->
      <!--<form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i> </button>
          </span> </div>
      </form>-->
      <!-- /.search form --> 
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header"> <?php if(LAN_SEL=="ar"){?> قائمة  <?php }else{?> MENU <?php } ?>  </li>
       <?php if (trim($_SESSION['aqdar_smartcare']['user_type_sess']) == "admin" )  { ?>
       
         
            <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_admin']) && trim($_SESSION['aqdar_smartcare']['school_admin']) == "Y" ))  { ?>
             
              <li class="treeview <?php if($menu == "admin") { echo " active"; }?> "> <a href="#"><i class="fa fa-user-plus" ></i> <span>
               <?php if(LAN_SEL=="ar"){ ?>الإداريين<?php }else{?> Admin<?php } ?></span> <span class="pull-right-container"> <i class="fa fa-angle-right pull-right"></i> </span> </a>
              <ul class="treeview-menu">
               
               <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_admin']) && trim($_SESSION['aqdar_smartcare']['school_admin_users']) == "Y" ))  { ?>
                <li>
                <a <?php if($sub_menu == "admin_users") {?> style="color:#fff;" <?php }?>   href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/admin_user/all_users" >
                <?php if(LAN_SEL=="ar"){ ?> الإداريين<?php }else{?> Admin Users <?php } ?>
                </a></li>
                <?php } ?>
               </ul>
              </li>
          
             <?php } ?>
        
        
       
     <?php /*?>  <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_class']) && trim($_SESSION['aqdar_smartcare']['school_class']) == "Y" ))  { ?>
       
        <li class="treeview <?php if($menu == "classes") {echo " active";}?> "> <a href="#"><i class="fa fa-list"></i> <span>
        <?php if(LAN_SEL=="ar"){ ?>الصفوف<?php }else{?> Classes / Grades <?php } ?></span> <span class="pull-right-container"> <i class="fa fa-angle-right pull-right"></i> </span> </a>
          <ul class="treeview-menu">
			 
			 <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_class_types']) && trim($_SESSION['aqdar_smartcare']['school_class_types']) == "Y" ))  { ?>
                <li><a <?php if($sub_menu == "class_types") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/school_types/">
                <?php if(LAN_SEL=="ar"){ ?>ادارة فئة الصف<?php }else{?> Types <?php } ?>
                </a></li>
             <?php } ?>
                
              <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_class_sections']) && trim($_SESSION['aqdar_smartcare']['school_class_sections']) == "Y" ))  { ?>
                 <li><a <?php if($sub_menu == "class_sections") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/class_sections/">
                <?php if(LAN_SEL=="ar"){ ?>ادارة الأقسام<?php }else{?> Divisions <?php } ?>
                </a></li>
              <?php } ?>
            
             <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_class_grades']) && trim($_SESSION['aqdar_smartcare']['school_class_grades']) == "Y" ))  { ?>
                <li><a <?php if($sub_menu == "class_grades") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/class_grades/">
                <?php if(LAN_SEL=="ar"){ ?>الصف<?php }else{?>  Classes / Grades <?php } ?>
                </a></li>
            <?php } ?>
            
          <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_class_sessions']) && trim($_SESSION['aqdar_smartcare']['school_class_sessions']) == "Y" ))  { ?>
                 <li ><a <?php if($sub_menu == "class_sessions") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/class_sessions/">
                <?php if(LAN_SEL=="ar"){ ?>ادارة جلسات العمل<?php }else{?> Periods <?php } ?>
                </a></li>
            <?php } ?>
            
          </ul>
        </li>
        
        <?php } ?><?php */?>
        
        
          <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_class']) && trim($_SESSION['aqdar_smartcare']['school_class']) == "Y" ))  { ?> 
            <li <?php if($menu == "classes") {echo " class='active'";}?>  ><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/class_grades/"><i class="fa fa-list"></i> 
            <span>  <?php if(LAN_SEL=="ar"){ ?>الصفوف<?php }else{?> Classes / Grades <?php } ?></span></a></li>
         <?php } ?>
        
        
        
         <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_members']) && trim($_SESSION['aqdar_smartcare']['school_members']) == "Y" ))  { ?>
        
              <li class="treeview <?php if($menu == "members") {echo " active";}?> "> <a href="#"><i class="fa fa-users"></i> <span>
            <?php if(LAN_SEL=="ar"){ ?>أعضاء<?php }else{?> Members  <?php } ?></span> <span class="pull-right-container"> <i class="fa fa-angle-right pull-right"></i> </span> </a>
              <ul class="treeview-menu">
               
               <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_students']) && trim($_SESSION['aqdar_smartcare']['school_students']) == "Y" ))  { ?>
                    <li><a <?php if($sub_module == "students_pending") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/all_students/sub_module/students_pending">
                    <?php if(LAN_SEL=="ar"){ ?> الطلاب<?php }else{?> Students (Pending Approvals) <?php } ?>
                    </a></li>
                    <li><a <?php if($sub_menu == "students" && $sub_module == "") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/all_students">
                    <?php if(LAN_SEL=="ar"){ ?> الطلاب<?php }else{?> Students <?php } ?>
                    </a></li>
                <?php } ?>
                
                <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_parents']) && trim($_SESSION['aqdar_smartcare']['school_parents']) == "Y" ))  { ?>
                     <li><a <?php if($sub_menu == "parents") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/parents/all_parents">
                    <?php if(LAN_SEL=="ar"){ ?> الوالدين<?php }else{?> Parents <?php } ?>
                    </a></li>
                <?php } ?>
                
                <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_teachers']) && trim($_SESSION['aqdar_smartcare']['school_teachers']) == "Y" ))  { ?>
                     <li><a <?php if($sub_menu == "teachers") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/all_teachers">
                    <?php if(LAN_SEL=="ar"){ ?> المعلمين<?php }else{?>  Teachers <?php } ?>
                    </a></li>
                <?php } ?>
                
              </ul>
            </li>
        
           <?php } ?>
        
        
        <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_messages']) && trim($_SESSION['aqdar_smartcare']['school_messages']) == "Y" ))  { ?>
        
               <li class="treeview <?php if($menu == "messages") {echo " active";}?> "> <a href="#"><i class="fa fa-comments "></i> <span>
            <?php if(LAN_SEL=="ar"){ ?> الرسائل<?php }else{?> Messages <?php } ?></span> <span class="pull-right-container"> <i class="fa fa-angle-right pull-right"></i> </span> </a>
              <ul class="treeview-menu">
                
                <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_message_to_teachers']) && trim($_SESSION['aqdar_smartcare']['school_message_to_teachers']) == "Y" ))  { ?>
                    <li><a <?php if($sub_menu == "message_to_teachers") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/message_to_teachers">
                    <?php if(LAN_SEL=="ar"){ ?>رسالة إلى المعلمين<?php }else{?>  Message To Teachers <?php } ?>
                    </a></li>
                 <?php } ?>
                    
                
                <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_message_to_parents']) && trim($_SESSION['aqdar_smartcare']['school_message_to_parents']) == "Y" ))  { ?>
                    <li><a  <?php if($sub_menu == "message_to_parents") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/message_to_parents">
                    <?php if(LAN_SEL=="ar"){ ?>رسالة إلى أولياء الأمور<?php }else{?>  Message To Parents  <?php } ?>
                    </a></li>
                 <?php } ?>
                    
              <?php /*?>   <li><a  <?php if($sub_menu == "parents_group") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/parents_group">
                <?php if(LAN_SEL=="ar"){ ?>المجموعات المنتدى الرئيسي <?php }else{?>  Parent Forum Groups <?php } ?>
                </a></li><?php */?>
                
                 <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_message_parents']) && trim($_SESSION['aqdar_smartcare']['school_message_parents']) == "Y" ))  { ?>
                    <li><a  <?php if($sub_menu == "message_from_parents") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/message_from_parents">
                    <?php if(LAN_SEL=="ar"){ ?>رسالة من الآباء والأمهات <?php }else{?>  Message From Parents<?php } ?>
                    </a></li>
                 <?php } ?>
              </ul>
            </li>
    <?php } ?>
          
          
    <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_library']) && trim($_SESSION['aqdar_smartcare']['school_library']) == "Y" ))  { ?>
               <li class="treeview <?php if($menu == "school_library") {echo " active";}?> "> <a href="#"><i class="fa fa-book "></i> <span>
            <?php if(LAN_SEL=="ar"){ ?> مكتبة المدرسة <?php }else{?> School Library <?php } ?></span> <span class="pull-right-container"> <i class="fa fa-angle-right pull-right"></i> </span> </a>
              <ul class="treeview-menu">
                
                <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['library_books']) && trim($_SESSION['aqdar_smartcare']['library_books']) == "Y" ))  { ?>
                    <li><a <?php if($sub_menu == "library_books") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/books/index/">
                    <?php if(LAN_SEL=="ar"){ ?>إضافة كتاب<?php }else{?> Upload Books<?php } ?>
                    </a></li>
                 <?php } ?>
                    
              </ul>
            </li>
      <?php } ?>
          
          
          
          
          
          
        
        
        
      <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_parenting']) && trim($_SESSION['aqdar_smartcare']['school_parenting']) == "Y" ))  { ?>  
             <?php /*?>  <li class="treeview <?php if($menu == "records") {echo " active";}?> "> <a href="#"><i class="fa fa-file"></i> <span>
            <?php if(LAN_SEL=="ar"){ ?> السجلات<?php }else{?> Records <?php } ?></span> <span class="pull-right-container"> <i class="fa fa-angle-right pull-right"></i> </span> </a>
              <ul class="treeview-menu">
                 <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_parenting_records']) && trim($_SESSION['aqdar_smartcare']['school_parenting_records']) == "Y" ))  { ?>
                    <li><a <?php if($sub_menu == "school_records") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/school_records">
                    <?php if(LAN_SEL=="ar"){ ?>حقوق الاداري المستخدمين الاداريين <?php }else{?>  School Parenting  <?php } ?>
                    </a></li>
                  <?php } ?>  
              </ul>
            </li>
         <?php } ?><?php */?>
         
       <?php /*?>  
          <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_parenting']) && trim($_SESSION['aqdar_smartcare']['school_parenting']) == "Y" ))  { ?>  
            <li <?php if($menu == "records") {echo " class='active'";}?> ><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/school_records"><i class="fa fa-calendar"></i> 
            <span><?php if(LAN_SEL=="ar"){ ?> السجلات <?php }else{?> Records <?php } ?></span></a></li>
            
             <li class="treeview <?php if($menu == "records") {echo " active";}?> "> <a href="#"><i class="fa fa-files-o"></i> <span>
        <?php if(LAN_SEL=="ar"){ ?>السجلات<?php }else{?> Records <?php } ?></span> <span class="pull-right-container"> <i class="fa fa-angle-right pull-right"></i> </span> </a>
          <ul class="treeview-menu">
			 
			 <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_parenting']) && trim($_SESSION['aqdar_smartcare']['school_parenting']) == "Y" ))  { ?>
                <li><a <?php if($sub_menu == "record_category") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/parenting_categories/">
                <?php if(LAN_SEL=="ar"){ ?>فئة<?php }else{?> Categories <?php } ?>
                </a></li>
             <?php } ?>
                
              <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_parenting']) && trim($_SESSION['aqdar_smartcare']['school_parenting']) == "Y" ))  { ?>
                 <li><a <?php if($sub_menu == "school_records") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/school_records/">
                <?php if(LAN_SEL=="ar"){ ?>السجلات<?php }else{?> Records <?php } ?>
                </a></li>
              <?php } ?>
            
           <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_parenting']) && trim($_SESSION['aqdar_smartcare']['school_parenting']) == "Y" ))  { ?>
                <li><a <?php if($sub_menu == "assign_records") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/list_assign_records/add/1/">
                <?php if(LAN_SEL=="ar"){ ?>تعيين سجل لطالب<?php }else{?> Assign Records To Student <?php } ?>
                </a></li>
            <?php } ?>
          </ul>
        </li><?php */?>
            
            
      
        
        
        
        <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['records']) && trim($_SESSION['aqdar_smartcare']['records']) == "Y" ))  { ?> 
            <li <?php if($menu == "records") {echo " class='active'";}?>  ><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/parenting_categories/"><i class="fa fa-files-o"></i> 
            <span> <?php if(LAN_SEL=="ar"){ ?>السجلات<?php }else{?> Records <?php } ?></span></a></li>
         <?php } ?>
        
          <?php } ?>
        
         
          
        
        
        <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_survey']) && trim($_SESSION['aqdar_smartcare']['school_survey']) == "Y" ))  { ?> 
            <li <?php if($menu == "survey") {echo " class='active'";}?>  ><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/survey/all_survey"><i class="fa fa-check-square-o"></i> 
            <span><?php if(LAN_SEL=="ar"){ ?> الاستبيان  <?php }else{?> Survey <?php } ?></span></a></li>
         <?php } ?>
         
         <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_events']) && trim($_SESSION['aqdar_smartcare']['school_events']) == "Y" ))  { ?>  
            <li <?php if($menu == "events") {echo " class='active'";}?> ><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/event/all_events"><i class="fa fa-calendar"></i> 
            <span><?php if(LAN_SEL=="ar"){ ?> الاحداث <?php }else{?> Events <?php } ?></span></a></li>
        <?php } ?>
         
         <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_parent_forum']) && trim($_SESSION['aqdar_smartcare']['school_parent_forum']) == "Y" ))  { ?> 
            <li <?php if($menu == "forum") {echo " class='active'";}?> ><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/forum/all_topics"><i class="fa fa-object-group"></i> 
            <span><?php if(LAN_SEL=="ar"){ ?> المنتدى <?php }else{?> Forum For Parents<?php } ?></span></a></li>
        <?php } ?>
        
         <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_student_forum']) && trim($_SESSION['aqdar_smartcare']['school_student_forum']) == "Y" ))  { ?> 
            <li <?php if($menu == "forum_student") {echo " class='active'";}?> ><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/forum/all_student_forum_topics"><i class="fa fa-object-group"></i> 
            <span><?php if(LAN_SEL=="ar"){ ?> المنتدى <?php }else{?> Forum For Students<?php } ?></span></a></li>
        <?php } ?>
        
        
        
        <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_gallery']) && trim($_SESSION['aqdar_smartcare']['school_gallery']) == "Y" ))  { ?>  
             <li <?php if($menu == "gallery") {echo " class='active'";}?> ><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/all_galleries"><i class="fa fa-picture-o"></i> 
             <span><?php if(LAN_SEL=="ar"){ ?>  معرض الصور  <?php }else{?> Gallery <?php } ?></span></a></li>
        <?php } ?>
        
       <?php /*?> <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_reports']) && trim($_SESSION['aqdar_smartcare']['school_reports']) == "Y" ))  { ?> 
            <li <?php if($menu == "reports") {echo " class='active'";}?>  ><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/school_reports"><i class="fa fa-file-text-o"></i> 
            <span><?php if(LAN_SEL=="ar"){ ?> السجلات  <?php }else{?> Reports <?php } ?></span></a></li>
         <?php } ?>  <?php */?>
         
      
       <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_reports']) && trim($_SESSION['aqdar_smartcare']['school_reports']) == "Y" ))  { ?>    
       
        <li class="treeview <?php if($menu == "reports") {echo " active";}?> "> <a href="#"><i class="fa fa-file-text-o"></i> <span>
        <?php if(LAN_SEL=="ar"){ ?>السجلات<?php }else{?> Reports <?php } ?></span> <span class="pull-right-container"> <i class="fa fa-angle-right pull-right"></i> </span> </a>
          <ul class="treeview-menu">
			 
			
                
              <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_cards_report']) && trim($_SESSION['aqdar_smartcare']['school_cards_report']) == "Y" ))  { ?>
                 <li><a <?php if($sub_menu == "cards_report") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/school_reports">
                <?php if(LAN_SEL=="ar"){ ?>بطاقات<?php }else{?> Cards <?php } ?>
                </a></li>
              <?php } ?>
            
             <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_points_report']) && trim($_SESSION['aqdar_smartcare']['school_points_report']) == "Y" ))  { ?>
                <li><a <?php if($sub_menu == "points_report") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/school_point_reports">
                <?php if(LAN_SEL=="ar"){ ?>نقاط <?php }else{?>  Points <?php } ?>
                </a></li>
            <?php } ?>
            
             <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_attendance_report']) && trim($_SESSION['aqdar_smartcare']['school_attendance_report']) == "Y" ))  { ?>
                 <li ><a <?php if($sub_menu == "attendance_report") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/attendance_days_reports/"> <!--school_attendance_reports-->
                <?php if(LAN_SEL=="ar"){ ?>الحضور<?php }else{?> Attendance <?php } ?>
                </a></li>
            <?php } ?>
            
             <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['student_performance_report']) && trim($_SESSION['aqdar_smartcare']['student_performance_report']) == "Y" ))  { ?>
                 <li ><a <?php if($sub_menu == "student_performance_report") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/student_performance_report/">
                <?php if(LAN_SEL=="ar"){ ?>تقرير الأداء<?php }else{?> Performance Report <?php } ?>
                </a></li>
            <?php } ?>
            
             <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['student_progress_report']) && trim($_SESSION['aqdar_smartcare']['student_progress_report']) == "Y" ))  { ?>
                 <li ><a <?php if($sub_menu == "student_progress_report") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/student_progress_report/">
                <?php if(LAN_SEL=="ar"){ ?>تقرير الأداء<?php }else{?> Progress Report <?php } ?>
                </a></li>
            <?php } ?>

            
            
          </ul>
        </li>   
         <?php } ?>
         
         
         
        
        
        
       <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_configuration']) && trim($_SESSION['aqdar_smartcare']['school_configuration']) == "Y" ))  { ?>  
            <li class="treeview <?php if($menu == "configuration") {echo " active";}?> "> <a href="#"><i class="fa fa-gear"></i> <span>
            <?php if(LAN_SEL=="ar"){ ?> تكوينات<?php }else{?> Configurations <?php } ?></span> <span class="pull-right-container"> <i class="fa fa-angle-right pull-right"></i> </span> </a>
              <ul class="treeview-menu">
              
              
                
                <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_student_cards']) && trim($_SESSION['aqdar_smartcare']['school_student_cards']) == "Y" ))  { ?>  
                     <li><a <?php if($sub_menu == "student_cards_conf") {?> style="color:#fff;" <?php }?> href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/student_cards_conf">
                    <?php if(LAN_SEL=="ar"){ ?>بطاقات طالب<?php }else{?> Cards  <?php } ?>
                    </a></li>
                 <?php } ?>
                 
                   <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_student_points']) && trim($_SESSION['aqdar_smartcare']['school_student_points']) == "Y" ))  { ?>  
                    <li><a <?php if($sub_menu == "student_points_conf") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/student_points_conf">
                    <?php if(LAN_SEL=="ar"){ ?>نقاط طالب <?php }else{?>  Points  <?php } ?>
                    </a></li>
                 <?php } ?>
                 <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_student_performance']) && trim($_SESSION['aqdar_smartcare']['school_student_performance']) == "Y" ))  { ?>  
                     <li><a <?php if($sub_menu == "student_performance_conf") {?> style="color:#fff;" <?php }?> href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/student_performance_conf">
                    <?php if(LAN_SEL=="ar"){ ?>أداء الطالب<?php }else{?>  Performance Indicator  <?php } ?>
                    </a></li>
                 <?php } ?>
                
                <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_semester']) && trim($_SESSION['aqdar_smartcare']['school_semester']) == "Y" ))  { ?>  
                     <li><a <?php if($sub_menu == "semester_conf") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/semester_conf">
                    <?php if(LAN_SEL=="ar"){ ?>التكوين الفصل الدراسي<?php }else{?>  Semesters  <?php } ?>
                    </a></li>
                 <?php } ?>
                 
                 <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_subject']) && trim($_SESSION['aqdar_smartcare']['school_subject']) == "Y" ))  { ?>  
                     <li><a <?php if($sub_menu == "subject") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/school_subject">
                    <?php if(LAN_SEL=="ar"){ ?>دراسة الموضوعات<?php }else{?>  Subjects  <?php } ?>
                    </a></li>
                 <?php } ?>
                
                <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_contacts']) && trim($_SESSION['aqdar_smartcare']['school_contacts']) == "Y" ))  { ?>  
                     <li><a <?php if($sub_menu == "school_contact") {?> style="color:#fff;" <?php }?> href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/settings/school_contact">
                     <?php if(LAN_SEL=="ar"){ ?>جهات الاتصال بالمدرسة<?php }else{?>  Contact  <?php } ?>
                     </a></li>
                 <?php } ?>
                 
                   <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_configuration']) && trim($_SESSION['aqdar_smartcare']['school_configuration']) == "Y" ))  { ?>  
                     <li><a <?php if($sub_menu == "student_group") {?> style="color:#fff;" <?php }?> href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/students_group">
                    <?php if(LAN_SEL=="ar"){ ?>مجموعة الطلاب<?php }else{?>  Students Group  <?php } ?>
                    </a></li>
                 <?php } ?>
                
                <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_configuration']) && trim($_SESSION['aqdar_smartcare']['school_configuration']) == "Y" ))  { ?>  
                     <li><a <?php if($sub_menu == "parent_group") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/parents_group">
                    <?php if(LAN_SEL=="ar"){ ?>مجموعة أولياء الأمور<?php }else{?>  Parents Group  <?php } ?>
                    </a></li>
                 <?php } ?>
                
                <?php if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" || (isset($_SESSION['aqdar_smartcare']['school_configuration']) && trim($_SESSION['aqdar_smartcare']['school_configuration']) == "Y" ))  { ?>  
                     <li><a <?php if($sub_menu == "teacher_group") {?> style="color:#fff;" <?php }?> href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/teachers_group">
                     <?php if(LAN_SEL=="ar"){ ?>مجموعة المعلمين <?php }else{?>  Teachers Group  <?php } ?>
                     </a></li>
                 <?php } ?>
                 
                
              </ul>
            </li>
         <?php } ?>
         
         
    
		<?php } ?>
      </ul>
      <!-- /.sidebar-menu --> 
    </section>
    <!-- /.sidebar --> 
  </aside>