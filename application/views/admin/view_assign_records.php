<?php
//Init Parameters
$parent_group_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}
?>
 
<style>
.txt_en {
	text-align:left;
	padding-left:2px;
}
.txt_ar {
	text-align:right;
	padding-right:2px;	
	direction:rtl;		
}
textarea {
    height: 170px;
    padding-bottom: 6px;
    padding-top: 6px;
    width: 95%;
	font-size: 14px;
	border: 1px solid #ddd;
 }
 select {
   font-size: 14px;
   border: 1px solid #ddd;
 }
</style>

<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1_list').hide(500);
		$('#mid2').show(500);
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/list_assign_records";
			}
		})
	});
	
function confirm_delete_popup() {
		var len = $("input[id='tbl_parenting_school_assign_id']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more record(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/deleteAssignRecords",
			data: {
				tbl_parenting_school_assign_id: $("input[id='tbl_parenting_school_assign_id']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Assigned record for student(s) deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}	
	function ajax_activate(tbl_parenting_school_assign_id) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/activateAssignRecords",
			data: {
				tbl_parenting_school_assign_id: tbl_parenting_school_assign_id,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Assigned record for student activated successfully.", 'green')

				$('#act_deact_'+tbl_parenting_school_assign_id).html('<span style="cursor:pointer" onClick="ajax_deactivate(\''+tbl_parenting_school_assign_id+'\')" onMouseOver="deactivate_me(this)" onMouseOut="reset_activate(this)" class="label label-success">Active</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_deactivate(tbl_parenting_school_assign_id) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/deactivateAssignRecords",
			data: {
				tbl_parenting_school_assign_id: tbl_parenting_school_assign_id,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Assigned record for student de-activated successfully.", 'green')
				
				$('#act_deact_'+tbl_parenting_school_assign_id).html('<span style="cursor:pointer" onClick="ajax_activate(\''+tbl_parenting_school_assign_id+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	
	function ajax_submit() {
	
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/assign_records",
			data: {
				tbl_parenting_school_id  : $('#tbl_parenting_school_id').val(),
				tbl_student_id           : $("input[id='student_id_enc']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='N') {
					refresh_page = "N";
					my_alert("Record assign to student failed, Please try again.", 'red');
					$("#pre-loader").hide();
				}else{
					// refresh_page = "Y";
				    my_alert("Record assigned to student(s) successfully.", 'green');
				    $("#pre-loader").hide();
					setTimeout("window.location.href='<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/school_records';",4000);
					//window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/school_records";
				}
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_update() {
			
		 var selectednumbers = "";
        $('.checkboxStudent:checked').each(function(i){
          selectednumbers += $(this).val()+"&";
        });		
			
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/update_parent_group",
			data: {
				group_name_en            : $('#group_name_en').val(),
				group_name_ar            : $('#group_name_ar').val(),
				tbl_parent_id            : selectednumbers,
				tbl_parent_group_id      : $('#parent_group_id_enc').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='N') {
					refresh_page = "N";
					my_alert("Group updation failed, Please try again.", 'red');
					$("#pre-loader").hide();
				   
				}else{
					 refresh_page = "Y";
				    my_alert("Group updated successfully.", 'green');
				    $("#pre-loader").hide();
				}
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
</script>
<script language="javascript">
   //add student
   /* || validate_picture() == false*/
	function ajax_validate() {
		if (validate_record() == false || validate_students() == false ) 
		{
			return false;
		}
		else{
			ajax_submit();
		}
	}
	
    //edit student
	function ajax_validate_edit() {
		if (validate_record() == false || validate_students() == false) 
		{
			return false;
		} 
		else{
			ajax_update();
		}
	} 
	
  /************************************* START MESSAGE VALIDATION *******************************/

   function validate_record() {
		var regExp = / /g;
		var str = $("#tbl_parenting_school_id").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Record is blank. Please select a record")
			$("#tbl_parenting_school_id").focus();
		return false;
		}
	
		return true;
	
	}
	

	function validate_students() {
		var regExp = / /g;
		var str = $('#student_id_enc').val();
		if (str==null ) {
			my_alert("Please select Student(s)");
		return false;
		}
	  return true;
	}
	
    function show_categories()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/parenting_categories/";
		window.location.href = url;
	}
	
	function show_records()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/school_records/";
		window.location.href = url;
	}
	
</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Assign Records <small> Management</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Assign Records To Students</li>
    </ol>
    <!--/BREADCRUMB--> 
 
  <div class="box-tools" style="float:right;">
        <a onclick="show_records()" ><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
    </div>

    <div style="clear:both"></div>
  </section>
      <link href="<?=HOST_URL?>/assets/admin/dist/css/jquery-ui.css" rel="stylesheet">
      <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-1.11.1.js"></script>
      <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-ui.js"></script>
      <link href="<?=HOST_URL?>/assets/admin/dist/css/uploadfile.min.css" rel="stylesheet">

  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {
	?>
        <!--Edit-->
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Parents Group</h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/parents_group"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

            
     <style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url('<?=IMG_PATH?>/delete.jpg');
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
		clear:both;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
	#overlay_container {
		position:relative;
	}
	#overloading {
		background-image:url('<?=IMG_PATH?>/preloader/preloader_2.gif');
		background-repeat:no-repeat;
		background-position:center;
		background-color:#CCC;
		position:absolute;
		left:0px;
		top:0px;
		opacity: 0.3;
		z-index: 10000;
	}
	#div_listing_container {
		display:none;	
	}
	.d_d_text {
		color:#745156;
		font-size:20px;
			
	}
	.ajax-upload-dragdrop {
		margin:auto;
		margin-bottom:10px;
		width:700px !important;
	}
	.ajax-file-upload-statusbar {
		margin:auto;
		margin-top:10px;
	}
	.ajax-file-upload {
		height:31px;
	}
	
	
	 #tabs-1{  
	    overflow-y:scroll; overflow-x:none;
	}

    #tabs-2{
		overflow-y:scroll; overflow-x:none;
	}
				  
  .ui-tabs-active{
		border-color:#efca86  !important;
   }
					 
	.ui-tabs .ui-tabs-nav li {
		float:left;
		font-size: 16px;
        font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif;
  }
  label{
	  display: inline-block;
      font-weight: 700;
  }
  
  .ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button {
    font-family:"Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
    font-size: 14px;
}
  
  .ui-widget{
	 font-size: 16px;
     font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
  }
  .form-control{
	 font-size: 14px; 
  }
</style>         
         <?php
		 	$tbl_parent_group_id              = $group_info[0]['tbl_parent_group_id'];		
			$group_name_en             	    = $group_info[0]['group_name_en'];
			$group_name_ar             	    = $group_info[0]['group_name_ar'];
			$class_array = array(); 
			for($y=0;$y<count($selected_class_list);$y++)
			{
				$class_array[$y] = $selected_class_list[$y]['tbl_class_id'];
			} 
		 ?>       
                
             <div class="box-body">
                    <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
                         <div class="form-group">
                          <label class="col-sm-2 control-label" for="group_name_en">Group Name [En]</label>
        
                          <div class="col-sm-10">
                            <input type="text" placeholder="Group Name [En]" id="group_name_en" name="group_name_en" class="form-control" value="<?=$group_name_en?>">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="group_name_ar">Group Name [Ar]</label>
        
                          <div class="col-sm-10">
                            <input type="text" placeholder="Group Name [Ar]" id="group_name_ar" name="group_name_ar" class="form-control" value="<?=$group_name_ar?>" dir="rtl">
                          </div>
                        </div>
                       
                         <div class="form-group">
                         <label class="col-sm-2 control-label" for="tbl_class_id">Class</label>
                         <div class="col-sm-10">
                                     <select name="tbl_class_id" id="tbl_class_id" class="form-control" multiple size="10" style="padding:0px 5px 0px 2px; font-size:14px; " onChange="get_parents_ajax()"  >
                                     <option value="">Select Class</option>
                                  <?php
                                        for ($u=0; $u<count($classes_list); $u++) { 
                                            $tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
                                            $class_name             = $classes_list[$u]['class_name'];
                                            $class_name_ar          = $classes_list[$u]['class_name_ar'];
                                            $section_name           = $classes_list[$u]['section_name'];
                                            $section_name_ar        = $classes_list[$u]['section_name_ar'];
                                            
											if(in_array($tbl_class_id_u, $class_array, true))
                                               $selClass = "selected";
                                             else
                                               $selClass = "";
										
                                      ?>
                                          <option value="<?=$tbl_class_id_u?>"  <?=$selClass?> >
                                          <?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;
                                        <?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                                          </option>
                                          <?php
                                        }
                                    ?>
                                 </select>
                        </div>
                      </div>
                      
                       <div class="form-group" >
                          <label class="col-sm-2 control-label" for="tbl_student_id">Student(s)</label>
        
                          <div class="col-sm-10" >
                          <select id="tbl_student_dropdown" name="tbl_student_id" multiple size="10" style="padding:0px 5px 0px 2px; font-size:14px; width:100%;">
    
                                <?php 
								
								      for($i=0; $i<count($all_class_parent_list); $i++) { 
								      $tbl_parent_id_u = $all_class_parent_list[$i]["tbl_parent_id"];
									  $tbl_class_id_u  = $all_class_parent_list[$i]["tbl_class_id"];
									  if(in_array($tbl_parent_id_u, $assign_parents_list, true))
                                               $selParent = "selected";
                                             else
                                               $selParent = "";
							
								
								?>
    
                                <option value="<?=$tbl_parent_id_u?>*<?=$tbl_class_id_u?>"  <?=$selParent?> ><?=$all_class_parent_list[$i]["first_name"]?> <?=$all_class_parent_list[$i]["last_name"]?> &nbsp;  
                                <?=$all_class_parent_list[$i]["class_name"]?> - <?=$all_class_parent_list[$i]["section_name"]?> [::] <?=$all_class_parent_list[$i]["first_name_ar"]?> <?=$all_class_parent_list[$i]["last_name_ar"]?> &nbsp; <?=$all_class_parent_list[$i]["class_name_ar"]?> - <?=$all_class_parent_list[$i]["section_name_ar"]?> </option>
    
                                <?php	} ?>
    
                        </select> 
                        </div>
                     </div>
                        
                    
                     
                        <!-- /.box-body -->
                      <div class="box-footer">
                        <button class="btn btn-primary" type="button" onclick="ajax_validate_edit()">Submit</button>
                         <input type="hidden" name="parent_group_id_enc" id="parent_group_id_enc" value="<?=$tbl_parent_group_id?>" />
                        <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                      </div>
                      <!-- /.box-footer -->  
                      
            
           </form>
                </div>
                
    </div>
		        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/message/parents_group";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
    
  
 <link href="<?=HOST_URL?>/assets/admin/dist/css/jquery-ui.css" rel="stylesheet">
 <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-1.11.1.js"></script>
 <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-ui.js"></script>
  <script>
  $( function() {
		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
				
				/* var order = $("#tabledivbody").sortable("serialize");
				
				alert(order);
				
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/your/url/here'
				});*/
				
				
				
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	
				
				
				
				
				
			}
	  } );
  
  } );
  </script> 
  
  
  
  <!--File Upload START-->
<link href="<?=HOST_URL?>/assets/admin/dist/css/uploadfile.min.css" rel="stylesheet">
<script>
 $( function() {
    $( "#tabs" ).tabs();
  } );
  
 
</script>
<style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url('<?=IMG_PATH?>/delete.jpg');
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
		clear:both;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
	#overlay_container {
		position:relative;
	}
	#overloading {
		background-image:url('<?=IMG_PATH?>/preloader/preloader_2.gif');
		background-repeat:no-repeat;
		background-position:center;
		background-color:#CCC;
		position:absolute;
		left:0px;
		top:0px;
		opacity: 0.3;
		z-index: 10000;
	}
	#div_listing_container {
		display:none;	
	}
	.d_d_text {
		color:#745156;
		font-size:20px;
			
	}
	.ajax-upload-dragdrop {
		margin:auto;
		margin-bottom:10px;
		width:700px !important;
	}
	.ajax-file-upload-statusbar {
		margin:auto;
		margin-top:10px;
	}
	.ajax-file-upload {
		height:31px;
	}
	
	
	 #tabs-1{  
	    overflow-y:scroll; overflow-x:none;
	}

    #tabs-2{
		overflow-y:scroll; overflow-x:none;
	}
				  
  .ui-tabs-active{
		border-color:#efca86  !important;
   }
					 
	.ui-tabs .ui-tabs-nav li {
		float:left;
		font-size: 16px;
        font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif;
  }
  label{
	  display: inline-block;
      font-weight: 700;
  }
  
  .ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button {
    font-family:"Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
    font-size: 14px;
}
  
  .ui-widget{
	 font-size: 16px;
     font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
  }
  .form-control{
	 font-size: 14px; 
  }
</style>
 
  
    
                   <!-- <div id="mid1" class="box box-success">
                        <div class="box-header">
                          <div style="width:20%; float:left;">
                          <h3 class="box-title">SEARCH</h3>
                          </div>
                          <div style="width:60%; float:left;"> 
                    	     <div class="col-sm-5">
                             <?php //echo $category_parent_list ?>
                   		      </div>
                             
                          </div>
                        </div>  
                     </div>   -->  
    
            <!--Listing-->
                    <div id="mid1_list" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Records Assigned Students</h3>
                          <div class="box-tools">
                            <?php if (count($rs_assigned_students)>0) { echo $paging_string;}?>	
                            <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
                            <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>
                          </div>
                        </div>
                        
                        <div class="box-body">
                     <!--   <div style="color:#030; font-weight:bold;">You can sort students by using drag and drop of rows </div>-->
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle"><input id="select_all" type="checkbox" value="" /></th>
                              <!--<th width="10%" align="center" valign="middle">Sl No.</th>-->
                              <th width="25%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y">Records <?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?><div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>
                              <th width="35%" align="center" valign="middle">Students</th>
                              <th width="20%" align="center" valign="middle">Class</th>
                              <th width="10%" align="center" valign="middle">Date</th>
                              <th width="5%" align="center" valign="middle">Status</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                                for ($i=0; $i<count($rs_assigned_students); $i++) { 
                                    $tbl_parenting_school_assign_id = $rs_assigned_students[$i]['tbl_parenting_school_assign_id'];
									$tbl_parenting_school_id    = $rs_assigned_students[$i]['tbl_parenting_school_id'];
                                    $parenting_title_en         = $rs_assigned_students[$i]['parenting_title_en'];
									$parenting_title_ar         = $rs_assigned_students[$i]['parenting_title_ar'];
									$parenting_type	         = $rs_assigned_students[$i]['parenting_type'];
									$first_name	             = $rs_assigned_students[$i]['first_name'];
									$last_name	              = $rs_assigned_students[$i]['last_name'];
									$first_name_ar	          = $rs_assigned_students[$i]['first_name_ar'];
									$last_name_ar	           = $rs_assigned_students[$i]['last_name_ar'];
									$class_name	             = $rs_assigned_students[$i]['class_name'];
									$class_name_ar	          = $rs_assigned_students[$i]['class_name_ar'];
									$section_name	           = $rs_assigned_students[$i]['section_name'];
									$section_name_ar	        = $rs_assigned_students[$i]['section_name_ar'];
                                    $added_date                 = $rs_assigned_students[$i]['assigned_date'];
                                    $is_active                  = $rs_assigned_students[$i]['is_active'];
                                    $added_date = date('m-d-Y',strtotime($added_date));
									
									
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_parenting_school_assign_id?>" >
                              <td align="left" valign="middle">
                              <span style="float:left;">
                              <input id="tbl_parenting_school_assign_id" name="tbl_parenting_school_assign_id" class="checkbox" type="checkbox" value="<?=$tbl_parenting_school_assign_id?>" />
                              </span>
                              
                             <?php /*?> <span style="float:left;">&nbsp;
                              <?php if($i<>0){ ?> <i class="fa fa-arrow-up"  style="color:#3c8dbc; cursor:pointer;"  aria-hidden="true" title="Sorting - Drag & Drop To Up"></i> &nbsp; <?php } ?>
                               <?php if($i<> count($rs_all_categories)-1){ ?> <i class="fa fa-arrow-down" style="color:#3c8dbc;cursor:pointer;" aria-hidden="true" title="Sorting - Drag & Drop To Down"></i> <?php } ?>
                              </span><?php */?>
                              </td>
                             <!-- <td align="left" valign="middle"><?=$offset+$i+1?></td>-->
                              <td align="left" valign="middle">
                              <span style='float:left;'><?=$parenting_title_en?> </span><span style='float:right;'><?=$parenting_title_ar?></span>
                              </td>
                              <td align="left" valign="middle"> <span style='float:left;'><?=$first_name?>&nbsp;<?=$last_name?></span><span style='float:right;'>
							  <?=$first_name_ar?>&nbsp;<?=$last_name_ar?></span></td>
                              <td align="left" valign="middle"><span style='float:left;'><?=$class_name?>&nbsp;<?=$section_name?></span><span style='float:right;'>
							  <?=$class_name_ar?>&nbsp;<?=$section_name_ar?></span></td>
                              <td align="left" valign="middle"><?=$added_date?></td>
                              <td align="left" valign="middle">
                                <div id="act_deact_<?=$tbl_parenting_school_assign_id?>">
                                <?php if (trim($is_active) == "Y") { ?>
                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_parenting_school_assign_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                <?php } else { ?>
                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_parenting_school_assign_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                <?php } ?>
                                </div>
                              </td>
                            
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="10" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_assigned_students)<=0) {
                            ?>
                            <tr>
                              <td colspan="10" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no assigned records available. Click on the + button to assign records to student.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
    
            <!--Add or Create-->
            <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Assign Record To Student(s)</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
   	            <div class="box-body">
                    <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
                            
                         <div class="form-group">
                         <label class="col-sm-2 control-label" for="tbl_parenting_school_id">Records<span style="color:#F30; padding-left:2px;">*</span></label>
                         <div class="col-sm-10">
                                     <select name="tbl_parenting_school_id" id="tbl_parenting_school_id" class="form-control"  >
                                     <option value="">Select Record</option>
                                  <?php
                                        for ($u=0; $u<count($recordsObj); $u++) { 
                                            $tbl_parenting_school_id_u   = $recordsObj[$u]['tbl_parenting_school_id'];
                                            $parenting_title_en          = $recordsObj[$u]['parenting_title_en'];
                                            $parenting_title_ar          = $recordsObj[$u]['parenting_title_ar'];
                                            if($tbl_parenting_school_id   == $tbl_parenting_school_id_u)
                                               $selType = "selected";
                                             else
                                               $selType = "";
                                      ?>
                                          <option value="<?=$tbl_parenting_school_id_u?>"  <?=$selType?> >
                                          <?=$parenting_title_en?>&nbsp;[::]&nbsp;
                                        <?=$parenting_title_ar?>
                                          </option>
                                          <?php
                                        }
                                    ?>
                                 </select>
                       </div>
                       </div>
                        
                     <?php /*?>  <div class="form-group">
                         <label class="col-sm-2 control-label" for="tbl_class_id">Class<span style="color:#F30; padding-left:2px;">*</span></label>
                         <div class="col-sm-10">
                                     <select name="tbl_class_id" id="tbl_class_id" class="form-control" multiple size="10" 
                                     style="padding:0px 5px 0px 2px; font-size:14px; " onChange="get_students_ajax()"  >
                                     <option value="">Select Class</option>
                                  <?php
                                        for ($u=0; $u<count($classes_list); $u++) { 
                                            $tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
                                            $class_name             = $classes_list[$u]['class_name'];
                                            $class_name_ar          = $classes_list[$u]['class_name_ar'];
                                            $section_name           = $classes_list[$u]['section_name'];
                                            $section_name_ar        = $classes_list[$u]['section_name_ar'];
                                            if($tbl_sel_class_id == $tbl_class_id_u)
                                               $selClass = "selected";
                                             else
                                               $selClass = "";
                                      ?>
                                          <option value="<?=$tbl_class_id_u?>"  <?=$selClass?> >
                                          <?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;
                                        <?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                                          </option>
                                          <?php
                                        }
                                    ?>
                                 </select>
                        </div>
                      </div><?php */?>
                      
                      
                       <div class="form-group" id="all_student">
                        <label class="col-sm-2 control-label" for="tbl_class_id">Class(s)<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10" id="divClass" style="height:200px; overflow-y:scroll;">
                          <div class"col-sm-10" >
	 
                        <?php  if(count($classes_list)>0){ ?>
                           <div style="padding-bottom:10px;background-color:#e8eaeb;" class="col-sm-12"> 
                           <input type="checkbox" value="" id="select_all_class" class="checkboxClass" >&nbsp;Select All</div>
                        <?php  } ?>
                        <?php
                            for ($u=0; $u<count($classes_list); $u++) { 
								$tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
								$class_name             = $classes_list[$u]['class_name'];
								$class_name_ar          = $classes_list[$u]['class_name_ar'];
								$section_name           = $classes_list[$u]['section_name'];
								$section_name_ar        = $classes_list[$u]['section_name_ar'];
                             ?>
                             <div class="col-sm-4" style="padding-top:10px; padding-bottom:10px; border:1px solid #CCC;"> 
								  <input id="tbl_class_id_<?=$u?>" name="tbl_class_id[]" class="checkboxClass" type="checkbox" value="<?=$tbl_class_id_u?>"  onChange="get_students_ajax()" />&nbsp;
								   <?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;<?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                             </div>
							<?php  
	                         }
							 ?>
	                          </div> 
                        </div>
                        </div>    
                      
                      
                      
                      
                      
                      
                      
                      
                       <div class="form-group">
                          <label class="col-sm-2 control-label" for="tbl_student_id">Student(s)<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10" id="divStudent" style="height:400px; overflow-y:scroll;">
                          <select id="tbl_student_dropdown" name="tbl_student_id" multiple size="10" style="padding:0px 5px 0px 2px; font-size:14px; width:100%;">
    
                                <?php for($i=0; $i<count($data_rs); $i++) { ?>
    
                                <option value="<?=$data_rs[$i]["tbl_student_id"]?>"><?=$data_rs[$i]["first_name"]?> <?=$data_rs[$i]["last_name"]?> :: <?=$data_rs[$i]["first_name_ar"]?> <?=$data_rs[$i]["last_name_ar"]?> </option>
    
                                <?php	} ?>
    
                        </select> 
                        </div>
                     </div>
                        
                      
                     
                        <!-- /.box-body -->
                      <div class="box-footer">
                        <button class="btn btn-primary" type="button" onclick="ajax_validate()">Submit</button>
                        <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                      </div>
                      <!-- /.box-footer -->  
                      
            
           </form>
                </div>
           </div>
                
  
            <!--/Add or Create-->
                
        <!--/Admin Category Management-->

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>
<?php
if($add=="1")
{ ?>        <script>
	        $('#mid1').hide();
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
			</script>
<?php } ?>

<script type="text/javascript" src="<?=JS_PATH?>/jquery.timer.js"></script>
<script>
var host = '<?=HOST_URL?>';
function get_students_ajax() {
		//show_loading();
	
		//show_loading();
		var tbl_class_id = "";
        $('.checkboxClass:checked').each(function(i){
          tbl_class_id += $(this).val()+",";
        });
		//alert(tbl_class_id);
		//return;
		var xmlHttp, rnd, url, search_param, ajax_timer;
		rnd = Math.floor(Math.random()*11);
		try{		
			xmlHttp = new XMLHttpRequest(); 
		}catch(e) {
			try{
				xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
			}catch(e) {
				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
				hide_loading();
			}
		}

		//AJAX response
		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
				//ajax_timer.stop();
				var data = xmlHttp.responseText;
				$("#divStudent").html(data);
				//$("#tbl_student_dropdown").multiselect('refresh');
				return;
			}
		}

		/*ajax_timer = $.timer(function() {
			xmlHttp.abort();
			alert(connectivity_msg);
			ajax_timer.stop();
		},connectivity_timeout_time,true);*/

		//Sending AJAX request
		url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/students_against_classes/tbl_class_id/"+tbl_class_id+"/rnd/"+rnd;
		xmlHttp.open("POST",url,true);
		xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlHttp.send("rnd="+rnd);
	}
	
       $('#select_all_class').on('click',function(){
			if(this.checked){
				$('.checkboxClass').each(function(){
					this.checked = true;
				});
				
			}else{
				 $('.checkboxClass').each(function(){
					this.checked = false;
				});
			}
			
			get_students_ajax();
			
			if(this.checked){
				$('.checkboxModule').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkboxModule').each(function(){
					this.checked = false;
				});
			}
			
			
		});
</script>
<!--File Upload END-->
        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >
function search_data() {
		var tbl_category_id = $("#tbl_category_id").val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/all_categories/";
		if(tbl_category_id !='')
			url += "tbl_category_id/"+tbl_category_id+"/";
		
			url += "offset/0/";
		window.location.href = url;
		<?php /*?>window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/enquiry/all_enquiries/is_not_replied/"+is_not_replied+"/tbl_court_id/"+tbl_court_id+"/tbl_category_id/"+tbl_category_id;<?php */?>
	}
</script>


