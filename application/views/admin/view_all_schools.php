<?php
//Init Parameters
$school_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}
?>
 
<style>
.txt_en {
	text-align:left;
	padding-left:2px;
}
.txt_ar {
	text-align:right;
	padding-right:2px;	
	direction:rtl;		
}
</style>
<script>
var item_id = "<?=$school_id_enc?>";
</script>

<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/all_schools";
			}
		})
	});
	
function confirm_delete_popup() {
		var len = $("input[id='school_id_enc']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more school(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/deleteSchool",
			data: {
				school_id_enc: $("input[id='school_id_enc']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("School(s) deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}	
	function ajax_activate(school_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/activateSchool",
			data: {
				school_id_enc: school_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("School activated successfully.", 'green')

				$('#act_deact_'+school_id_enc).html('<span style="cursor:pointer" onClick="ajax_deactivate(\''+school_id_enc+'\')" onMouseOver="deactivate_me(this)" onMouseOut="reset_activate(this)" class="label label-success">Active</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_deactivate(school_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/deactivateSchool",
			data: {
				school_id_enc: school_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("School de-activated successfully.", 'green')
				
				$('#act_deact_'+school_id_enc).html('<span style="cursor:pointer" onClick="ajax_activate(\''+school_id_enc+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	
	
	
	function ajax_create() {
	
		
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/add_school",
			data: {
				school_id_enc: "<?=$school_id_enc?>",
				school_name     : $.trim($('#school_name').val()),
				school_name_ar  : $.trim($('#school_name_ar').val()),
				school_type     :  $('input[name=school_type]:checked').val(),
				email           : $('#email').val(),
				contact_person  : $('#contact_person').val(),
				phone           : $('#phone').val(),
				mobile          : $('#mobile').val(),
				fax             : $('#fax').val(),
				address         : $('#address').val(),
				color_code      : $('#color_code').val(),
				is_active       :  $('input[name=is_active]:checked').val(),
				
			
				first_name      : $('#first_name').val(),
				last_name       : $('#last_name').val(),
				user_id       : $('#user_id').val(),
				password      : $('#password').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
				refresh_page = "Y";
				    my_alert("School added successfully.", 'green');
				    $("#pre-loader").hide();
				}else{
					refresh_page = "N";
					my_alert("School added failed, Please try again.", 'red');
					$("#pre-loader").hide();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_save_changes() {
		 
		
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/save_school_changes",
			data: {
				school_id_enc   : $('#school_id_enc').val(),
				admin_id_enc    : $('#admin_id_enc').val(),
				school_name     : $.trim($('#school_name').val()),
				school_name_ar  : $.trim($('#school_name_ar').val()),
				school_type     : $('input[name=school_type]:checked').val(),
				email           : $('#email').val(),
				contact_person  : $('#contact_person').val(),
				phone           : $('#phone').val(),
				mobile          : $('#mobile').val(),
				fax             : $('#fax').val(),
				address         : $('#address').val(),
				color_code      : $('#color_code').val(),
				is_active       : $('input[name=is_active]:checked').val(),
				first_name      : $('#first_name').val(),
				last_name       : $('#last_name').val(),
				user_id         : $('#user_id').val(),
				password        : $('#password').val(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Changes saved successfully.", 'green');
				
				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
</script>
<script language="javascript">
   //add student
   /* || validate_picture() == false*/
	function ajax_validate() {
		if (validate_school_name() == false  || validate_email() == false || isPhone() == false || isMobile() == false || isFax() == false || 
		validate_first_name() == false || validate_last_name() == false || validate_user_id() == false || validate_password() == false 
		|| validate_confirm_password() == false  || isPasswordSame()==false || validate_exist_school()==false  || validate_exist_school_user_id()==false) 
		{
			return false;
		} 
	}
	

	
    //edit student
	function ajax_validate_edit() {
		if (validate_school_name() == false || validate_email() == false || isPhone() == false || isMobile() == false || isFax() == false || 
		validate_first_name() == false || validate_last_name() == false || validate_user_id() == false || validate_edit_password() == false || 
		validate_edit_confirm_password() == false || isEditPasswordSame()==false  || validate_exist_edit_school()==false || validate_exist_edit_school_user_id()==false ) 
		{
			return false;
		} 
	} 
	

	
  /************************************* START TEACHER VALIDATION *******************************/
   function validate_exist_school()
   {
	  
	   $.ajax({
				type: "POST",
				url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/is_exist_school",
				dataType: "text",
				data: {
					 school_id_enc: '<?=$school_id_enc?>',
					 school_name   : $('#school_name').val(),
					is_ajax: true
				},
				success: function(data) {
					    var temp = new String();
						temp = data;
						temp = temp.trim();
						if (temp=='Y') {
							my_alert("School is already exist.", 'red');
							$("#pre-loader").hide();
							return false;
						}else{
							return true;
						}
					},
					error: function() {
						$("#pre-loader").hide();
					}, 
					complete: function() {
						$("#pre-loader").hide();
					}
				});
   }
   
    function validate_exist_school_user_id()
   {
	   $.ajax({
				type: "POST",
				url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/is_exist_school_user_id",
				dataType: "text",
				data: {
					 school_id_enc: '<?=$school_id_enc?>',
					 user_id   : $('#user_id').val(),
					is_ajax: true
				},
				success: function(data) {
					    var temp = new String();
						temp = data;
						temp = temp.trim();
						if (temp=='Y') {
							my_alert("Username is already exist. please use another Username", 'red');
							$("#pre-loader").hide();
							return false;
						}else{
						   ajax_create();
						}
					
					},
					error: function() {
						$("#pre-loader").hide();
					}, 
					complete: function() {
						$("#pre-loader").hide();
					}
				});
   }
   
   
  //edit validation case
  
    function validate_exist_edit_school()
   {
	   $.ajax({
				type: "POST",
				url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/is_exist_edit_school",
				dataType: "text",
				data: {
					 school_id_enc: $('#school_id_enc').val(),
					 school_name   : $('#school_name').val(),
					is_ajax: true
				},
				success: function(data) {
					    var temp = new String();
						temp = data;
						temp = temp.trim();
						if (temp=='Y') {
							my_alert("School is already exist.", 'red');
							$("#pre-loader").hide();
							return false;
						}else{
							return true;
						}
					},
					error: function() {
						$("#pre-loader").hide();
					}, 
					complete: function() {
						$("#pre-loader").hide();
					}
				});
   }
   
    function validate_exist_edit_school_user_id()
   {
	   $.ajax({
				type: "POST",
				url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/is_exist_edit_school_user_id",
				dataType: "text",
				data: {
					 school_id_enc: $('#school_id_enc').val(),
					 user_id   : $('#user_id').val(),
					is_ajax: true
				},
				success: function(data) {
					    var temp = new String();
						temp = data;
						temp = temp.trim();
						if (temp=='Y') {
							my_alert("Username is already exist. please use another Username", 'red');
							$("#pre-loader").hide();
							return false;
						}else{
						    ajax_save_changes();
						}
					
					},
					error: function() {
						$("#pre-loader").hide();
					}, 
					complete: function() {
						$("#pre-loader").hide();
					}
				});
   }


   function validate_school_name() {
		var regExp = / /g;
		var str = $("#school_name").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("The School Name [En] is blank. Please write School Name [En].")
			$("#school_name").val('');
			$("#school_name").focus();
		return false;
		}
		var regExp = / /g;
		var str = $("#school_name_ar").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("The School Name [Ar] is blank. Please write School Name [Ar].")
			$("#school_name_ar").val('');
			$("#school_name_ar").focus();
		return false;
		}
	}


   function validate_first_name() {
		var regExp = / /g;
		var str = $("#first_name").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("The First Name is blank. Please write First Name [En].")
			$("#first_name").val('');
			$("#first_name").focus();
		return false;
		}
	}
	
	function validate_last_name() {
		var regExp = / /g;
		var str = $("#last_name").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("The Last Name [En] is blank. Please write Last Name [En].")
			$("#last_name").val('');
			$("#last_name").focus();
		return false;
		}
	}
	
	
	
	function isPhone() {
		var strPhone = $("#phone").val();
		if($.trim(strPhone)== "") {
				   /* my_alert("Please enter mobile number.");
					$("#mobile").focus();
					return false;*/
					return true;
		}else{
				if (strPhone.length < 10 || strPhone.length > 13) {
					my_alert("Please enter valid land phone number.");
					$("#phone").focus();
					return false;
			    }

				for (var i = 0; i < strPhone.length; i++) {
				var ch = strPhone.substring(i, i + 1);
					if  (ch < "0" || "9" < ch) {
						my_alert("The land phone number in digits only, Please re-enter your valid land phone number");
						$("#phone").focus();
						return false;
					}
				}
		}
	 return true;
	}
	
	function isMobile() {
		var strPhone = $("#mobile").val();
		if($.trim(strPhone)== "") {
				   /* my_alert("Please enter mobile number.");
					$("#mobile").focus();
					return false;*/
					return true;
		}else{
				if (strPhone.length < 10 || strPhone.length > 13) {
					my_alert("Please enter valid mobile number.");
					$("#mobile").focus();
					return false;
			    }

				for (var i = 0; i < strPhone.length; i++) {
				var ch = strPhone.substring(i, i + 1);
					if  (ch < "0" || "9" < ch) {
						my_alert("The mobile number in digits only, Please re-enter your valid mobile number");
						$("#mobile").focus();
						return false;
					}
				}
		}
	 return true;
	}
	
	
	function isFax() {
		var strPhone = $("#fax").val();
		if($.trim(strPhone)== "") {
				   /* my_alert("Please enter mobile number.");
					$("#mobile").focus();
					return false;*/
					return true;
		}else{
				if (strPhone.length < 10 || strPhone.length > 13) {
					my_alert("Please enter valid fax number.");
					$("#fax").focus();
					return false;
			    }

				for (var i = 0; i < strPhone.length; i++) {
				var ch = strPhone.substring(i, i + 1);
					if  (ch < "0" || "9" < ch) {
						my_alert("The fax number in digits only, Please re-enter your valid fax number");
						$("#fax").focus();
						return false;
					}
				}
		}
	 return true;
	}
	
		
	function validate_email() {
		var regExp = / /g;
		var str = $("#email").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
		     return true;	
		}
		if (!isNaN(str)) {
			my_alert("Invalid Email Id.");
			$("#email").focus();
			$("#email").select();
			return false;
		}

		if(str.indexOf('@', 0) == -1) {
			my_alert("Invalid Email Id.");
			$("#email").focus();
			$("#email").select();
			return false;
		}

	 return true;
	}

	

	
	function validate_user_id() {
		var str =  $("#user_id").val();
		if ( str=="" ) {
			my_alert("The Username is blank. Please write Username.");
			$("#user_id").focus();
			return false;
		}
		if (str.length < 6 ) {
			my_alert("The Username should be greater than 5 characters.");
			$("#user_id").focus();
			return false;
		}
		if (!isNaN(str)) {
			my_alert("The Username have only letters & digits, Please re-enter Username");
			$("#user_id").focus();
			return false;
		}
	   return true;
	 }
	 
    /* Password */
	function validate_password() {
		var str = $('#password').val(); 
		if (str == "") {
			my_alert("The Password field is blank. Please enter Password.");
			$('#password').focus();
			return false;
		}
		if($('#password').val()!="") {
			var str = $('#password').val();
			var regExp = / /g;
			var tmp = $('#password').val();
			tmp = tmp.replace(regExp,'');
			if (tmp.length <= 0) {
				my_alert("Enter valid Password.");
				$('#password').focus();
			return false;
			}	
		}
		if (str.length < 6) {
			my_alert("The Password should be greater than 5 Characters.");
			$('#password').focus();
			$('#password').select();
			return false;
		}

	return true;
	}


	/* Retype Password */
	function validate_confirm_password() {
		var str = $('#confirm_password').val();
		if (str == "") {
			my_alert("The Confirm Password field is blank. Please Retype Password.");
			$('#confirm_password').focus();
			return false;
		}
		if($('#confirm_password').val()!="") {
				var str = $('#confirm_password').val();
				var regExp = / /g;
				var tmp = $('#confirm_password').val();
				tmp = tmp.replace(regExp,'');
				if (tmp.length <= 0) {
					my_alert("Enter valid Confirm Password.");
					$('#confirm_password').focus();
					return false;
				}	
			
		}
	return true;
	}

	/* Check both password */
	function isPasswordSame() {
		var str1 = $('#password').val();
		var str2 = $('#confirm_password').val();
		if (str1 != str2) {
			my_alert("Password mismatch, Please Retype same Passwords in both fields.");
			$('#confirm_password').focus();
			return false;
		}
	return true;
	}




      /* Password */
	function validate_edit_password() {
		var str = $('#password').val(); 
		if ($.trim(str) == "") {
			return true;
		}
		if($('#password').val()!="") {
			var str = $('#password').val();
			var regExp = / /g;
			var tmp = $('#password').val();
			tmp = tmp.replace(regExp,'');
			if (tmp.length <= 0) {
				my_alert("Enter valid Password.");
				$('#password').focus();
			return false;
			}	
		}
		if (str.length < 6) {
			my_alert("The Password should be greater than 5 Characters.");
			$('#password').focus();
			$('#password').select();
			return false;
		}

	return true;
	}


	/* Retype Password */
	function validate_edit_confirm_password() {
		var strPass = $('#password').val(); 
		if ($.trim(strPass) == "") {
			return true;
		}else{
			var str = $('#confirm_password').val();
				if (str == "") {
					my_alert("The Confirm Password field is blank. Please Retype Password.");
					$('#confirm_password').focus();
					return false;
				}
		}
		if($('#confirm_password').val()!="") {
				var str = $('#confirm_password').val();
				var regExp = / /g;
				var tmp = $('#confirm_password').val();
				tmp = tmp.replace(regExp,'');
				if (tmp.length <= 0) {
					my_alert("Enter valid Confirm Password.");
					$('#confirm_password').focus();
					return false;
				}	
		}
	return true;
	}
	
	function isEditPasswordSame() {
		var str1 = $('#password').val();
		var strPass = $('#password').val(); 
		if ($.trim(strPass) == "") {
			return true;
		}else{
				var str2 = $('#confirm_password').val();
				if (str1 != str2) {
					my_alert("Password mismatch, Please Retype same Passwords in both fields.");
					$('#confirm_password').focus();
					return false;
				}
		}
	return true;
	}



</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Schools <small> Management</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Schools</li>
    </ol>
    <!--/BREADCRUMB-->
    <div style="clear:both"></div>
  </section>
      <link href="<?=HOST_URL?>/assets/admin/dist/css/jquery-ui.css" rel="stylesheet">
      <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-1.11.1.js"></script>
      <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-ui.js"></script>
      <link href="<?=HOST_URL?>/assets/admin/dist/css/uploadfile.min.css" rel="stylesheet">
      <script type="text/javascript" src="<?=JS_COLOR_PATH?>/jscolor.js"></script>
 
 
  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {
			
	
	?>
        <!--Edit-->

        
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit School</h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/all_schools"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

            
     <style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url('<?=IMG_PATH?>/delete.jpg');
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
		clear:both;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
	#overlay_container {
		position:relative;
	}
	#overloading {
		background-image:url('<?=IMG_PATH?>/preloader/preloader_2.gif');
		background-repeat:no-repeat;
		background-position:center;
		background-color:#CCC;
		position:absolute;
		left:0px;
		top:0px;
		opacity: 0.3;
		z-index: 10000;
	}
	#div_listing_container {
		display:none;	
	}
	.d_d_text {
		color:#745156;
		font-size:20px;
			
	}
	.ajax-upload-dragdrop {
		margin:auto;
		margin-bottom:10px;
		width:700px !important;
	}
	.ajax-file-upload-statusbar {
		margin:auto;
		margin-top:10px;
	}
	.ajax-file-upload {
		height:31px;
	}
	
	
	 #tabs-1{  
	    overflow-y:scroll; overflow-x:none;
	}

    #tabs-2{
		overflow-y:scroll; overflow-x:none;
	}
				  
  .ui-tabs-active{
		border-color:#efca86  !important;
   }
					 
	.ui-tabs .ui-tabs-nav li {
		float:left;
		font-size: 16px;
        font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif;
  }
  label{
	  display: inline-block;
      font-weight: 700;
  }
  
  .ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button {
    font-family:"Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
    font-size: 14px;
}
  
  .ui-widget{
	 font-size: 16px;
     font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
  }
  .form-control{
	 font-size: 14px; 
  }
</style>         
         <?php
		 	$tbl_school_id             = $school_obj[0]['tbl_school_id'];
			$school_name               = $school_obj[0]['school_name'];
			$school_name_ar            = $school_obj[0]['school_name_ar'];
			$school_type               = $school_obj[0]['school_type'];
			$email                     = $school_obj[0]['email'];
			$contact_person            = $school_obj[0]['contact_person'];		
			$phone                     = $school_obj[0]['phone'];
			$mobile                    = $school_obj[0]['mobile'];
			$fax                       = $school_obj[0]['fax'];
			$address                   = $school_obj[0]['address'];
			$color_code                = $school_obj[0]['color_code'];	
			$picture                   = $school_obj[0]['logo'];
			$is_active                 = $school_obj[0]['is_active'];
		
					
			$first_name                = $school_admin_obj[0]['first_name'];
			$last_name                 = $school_admin_obj[0]['last_name'];
			$username                  = $school_admin_obj[0]['user_id'];
			$tbl_admin_id              = $school_admin_obj[0]['tbl_admin_id'];
		
			
			if($picture<>"") // class="img-circle"
				$pic_path           =   '<img width="100" height="80" src="'.IMG_SHOW_PATH_LOGO.'/'.$picture.'"  />';
			else
				$pic_path           =   '';
			
		 ?>       
    <div class="box-body">
                    <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="school_name">School Name <span style="color:#F30; padding-left:2px;">*</span></label>
                          <div class="col-sm-5">
                            <input type="text" placeholder="School Name[En]" id="school_name" name="school_name" class="form-control" value="<?=$school_name?>" >
                          </div>
                           <div class="col-sm-5">
                            <input type="text" placeholder="School Name[Ar]" id="school_name_ar" name="school_name_ar" class="form-control" dir="rtl" value="<?=$school_name_ar?>">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="school_type">School Type<span style="color:#F30; padding-left:2px;">*</span></label>
                           <div class="col-sm-10">
                            <label>
                              <input type="radio" id="school_type1" name="school_type" value="public" class="minimal" <?php if($school_type=="public"){?> checked="checked" <?php } ?> >
                              Public
                            </label>
                            &nbsp;
                            <label>
                              <input type="radio" id="school_type2" name="school_type" value="private" class="minimal"  <?php if($school_type=="private"){?> checked="checked" <?php } ?>  >
                              Private
                            </label>
                          </div>
                        </div>
                        
                         <div class="form-group">
                          <label class="col-sm-2 control-label" for="email">Email</label>
        
                          <div class="col-sm-5">
                            <input type="text" placeholder="Email" id="email" name="email" class="form-control" maxlength="50" value="<?=$email?>" >
                          </div>
                           <div class="col-sm-5">
                            <input type="text" placeholder="Contact Person" id="contact_person" name="contact_person" class="form-control" maxlength="50" value="<?=$contact_person?>" >
                          </div>
                        </div>
                        
                         <div class="form-group">
                          <label class="col-sm-2 control-label" for="phone">Contact Number</label>
        
                          <div class="col-sm-3">
                           <!--<span style="position:absolute; padding-left:20px; padding-top:5px; color: #999; font-size:14px;">Phone +971</span>--><input type="text" placeholder="Land Phone" id="phone" name="phone" class="form-control"  maxlength="20" value="<?=$phone?>" ><!--style="padding-left:60px;"-->
                          </div> 
                          
                          <div class="col-sm-3">
                          <!-- <span style="position:absolute; padding-left:20px; padding-top:5px; color:#999; font-size:14px;">Mobile 971</span>--><input type="text" placeholder="Mobile" id="mobile" name="mobile" class="form-control"  maxlength="20" value="<?=$mobile?>" ><!--style="padding-left:60px;"-->
                          </div>
                          
                           <div class="col-sm-4">
                          <!-- <span style="position:absolute; padding-left:20px; padding-top:5px; color:#999; font-size:14px;">Fax 971</span>--><input type="text" placeholder="Fax" id="fax" name="fax" class="form-control"  maxlength="20" value="<?=$fax?>" ><!--style="padding-left:60px;"-->
                          </div>
                        </div>
                    
                        
                     <div class="form-group">
                         <label class="col-sm-2 control-label" for="description">Address </label>
                         <div class="col-sm-10">
                                  <textarea  dir="ltr" id="address" placeholder="Address" name="address" class="form-control" maxlength="250" > <?=$address?></textarea>
                         </div>
                        
                        <?php /*?> <div class="col-sm-5"> <label class="col-sm-2 control-label" for="description">Description </label>
                                  <textarea  dir="ltr" id="description" placeholder="Description " name="description" class="form-control" ></textarea>
                         </div><?php */?>
                     </div>
                        
                       
                      
                       <div class="form-group">
                          <label class="col-sm-2 control-label" for="last_name">Color Code</label>
        
                          <div class="col-sm-10">
                            <input type="text" placeholder="Color Code" id="color_code" name="color_code" class="form-control color"  value="<?=$color_code?>">
                          </div>
                        </div>
                      
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="school_type">Status<span style="color:#F30; padding-left:2px;">*</span></label>
                           <div class="col-sm-10">
                            <label>
                              <input type="radio" id="is_active1" name="is_active" value="Y" class="minimal" <?php if($is_active=="Y"){?> checked="checked" <?php } ?>  >
                              Active
                            </label>
                            &nbsp;
                            <label>
                              <input type="radio" id="is_active2" name="is_active" value="N" class="minimal" <?php if($is_active=="N"){?> checked="checked" <?php } ?> >
                              Inactive
                            </label>
                          </div>
                        </div>
                       
                        
                      
                           <div class="form-group">
                          <label class="col-sm-2 control-label" for="picture">Picture</label>
        
                          <div class="col-sm-10">
                            <!--File Upload START-->
                                <style>
                                #advancedUpload {
                                    padding-bottom:0px;
                                }
                                </style>
                                     
                                <div id="advancedUpload">Upload File</div>
                                
                                <div id="uploaded_items" >
                                    <div id="div_listing_container" class="listing_container" style="display:block">	            
                                            <?php
                                                if (trim($pic_path) != "") {
                                            ?>
                                                        <div id='<?=$tbl_uploads_id?>' class='box-header with-border'>
                                                          <div class='box-title'><?=$pic_path?></div>
                                                          <div class='box-tools'> <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick="confirm_delete_img_popup('<?=$tbl_uploads_id?>')">
                                                            </button>
                                                          </div>
                                                        </div>
                                                <style>
                                                    .ajax-upload-dragdrop {
                                                        display:none;	
                                                    }
                                                </style>        
                                            <?php		
                                                }
                                            ?>
                                    </div>        
                                </div>
                            <!--File Upload END-->
                          </div>
                        </div>
                        
                   
                       <!--   Super Admin Section -->
                        <hr />
                         <div class="form-group">
                          <label class="col-sm-3 control-label" for="" style="text-decoration:underline;">School Super Administrator</label>
                        </div>
                       
                         <div class="form-group">
                          <label class="col-sm-2 control-label" for="first_name">Name <span style="color:#F30; padding-left:2px;">*</span></label>
                          <div class="col-sm-5">
                            <input type="text" placeholder="First Name" id="first_name" name="first_name" class="form-control" maxlength="50" value="<?=$first_name?>" >
                          </div>
                           <div class="col-sm-5">
                            <input type="text" placeholder="Last Name" id="last_name" name="last_name" class="form-control" dir="ltr" maxlength="50" value="<?=$last_name?>" >
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="user_id">User Id<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10">
                            <input type="text" placeholder="User Id " id="user_id" name="user_id" class="form-control" value="<?=$username?>" readonly="readonly" >
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="password">Password</label>
        
                          <div class="col-sm-10">
                            <input type="password" placeholder="Password" id="password" name="password" class="form-control">
                          </div>
                        </div>
                        
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="confirm_password">Confirm Password </label>
        
                          <div class="col-sm-10">
                            <input type="password" placeholder="Confirm Password" id="confirm_password" name="confirm_password" class="form-control">
                            Note: Password will only change if entered.
                          </div>
                        </div>
                     
                        <!-- /.box-body -->
                      <div class="box-footer">
                        <button class="btn btn-primary" type="button" onclick="ajax_validate_edit()">Submit</button>
                         <input type="hidden" name="school_id_enc" id="school_id_enc" value="<?=$tbl_school_id?>" />
                         <input type="hidden" name="admin_id_enc" id="admin_id_enc" value="<?=$tbl_admin_id?>" />
                         
                        <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                      </div>
                      <!-- /.box-footer -->  
                      <script>
						var item_id = "<?=$tbl_school_id?>";
                      </script>
            
           </form>
                </div>
                
    </div>
		        
        <!--/Edit-->
         <?php							
		} else if (trim($mid) == "4" || trim($mid) == 4) { 
        
           	$tbl_school_id             = $school_obj[0]['tbl_school_id'];
			$school_name               = $school_obj[0]['school_name'];
			$school_name_ar            = $school_obj[0]['school_name_ar'];
			$school_type               = $school_obj[0]['school_type'];
			$email                     = $school_obj[0]['email'];
			$contact_person            = $school_obj[0]['contact_person'];		
			$phone                     = isset($school_obj[0]['phone'])? $school_obj[0]['phone']:'-' ;
			$mobile                    = isset($school_obj[0]['mobile'])? $school_obj[0]['mobile']:'-' ;
			$fax                       = isset($school_obj[0]['fax'])? $school_obj[0]['fax']:'-' ;
			$address                   = $school_obj[0]['address'];
			$color_code                = $school_obj[0]['color_code'];	
			$picture                   = $school_obj[0]['logo'];
			$is_active                 = $school_obj[0]['is_active'];
		
					
			$first_name                = $school_admin_obj[0]['first_name'];
			$last_name                 = $school_admin_obj[0]['last_name'];
			$username                  = $school_admin_obj[0]['user_id'];
			$tbl_admin_id              = $school_admin_obj[0]['tbl_admin_id'];
		
			
			if($picture<>"") // class="img-circle"
				$picture_path           =   IMG_SHOW_PATH_LOGO.'/'.$picture.'"';
			else
				$picture_path           =   '';
			
		    if($is_active=="Y")
				$is_active = "Active";
		    else
			    $is_active = "Not Active";
        
        ?>
       <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">School Details</h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/all_schools"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_edit" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                  
                    
                    <?php
                    if (trim($picture)=="") {
					?>
    	               <img class="profile-user-img img-responsive" src="<?=$picture_path?>" alt="User profile picture">
                    <?php }  else { ?>
	                   <a href="<?=$picture_path?>" target="_blank"><img class="profile-user-img img-responsive" src="<?=$picture_path?>" alt="User profile picture"></a>
				    <?php } ?>
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label">School Name [En]</label>
    
                      <div class="col-sm-10 control-label" style="text-align:left">
                        <?=$school_name?></span>
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label">School Name [Ar]</label>
    
                      <div class="col-sm-10 control-label" style="text-align:left">
                        <?=$school_name_ar?></span>
                      </div>
                    </div>
                    
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label">School Type</label>
    
                      <div class="col-sm-10 control-label" style="text-align:left">
                        <?=$school_type?></span>
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label">Email</label>
    
                      <div class="col-sm-10 control-label" style="text-align:left">
                        <?=$email?></span>
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label">Contact Person</label>
    
                      <div class="col-sm-10 control-label" style="text-align:left">
                        <?=$contact_person?></span>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label">Land Phone</label>
    
                      <div class="col-sm-2 control-label" style="text-align:left">
                      <?=$phone?></span>
                      </div>
                      
                      <div class="col-sm-2 control-label" style="text-align:left">
                       <strong>Mobile</strong>:  <?=$mobile?></span>
                      </div>
                      
                      <div class="col-sm-2 control-label" style="text-align:left">
                       <strong>Fax</strong>: <?=$fax?></span>
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label">Address</label>
    
                      <div class="col-sm-10 control-label" style="text-align:left">
                        <?=$address?></span>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label">Status</label>
    
                      <div class="col-sm-10 control-label" style="text-align:left">
                        <?=$is_active?></span>
                      </div>
                    </div>
                    
                    <hr />
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label">Super Admin Details</label>
    
                      <div class="col-sm-10 control-label" style="text-align:left">
                        <?=$first_name?>&nbsp;<?=$last_name?></span>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label">Username</label>
    
                      <div class="col-sm-10 control-label" style="text-align:left">
                        <?=$username?></span>
                      </div>
                    </div>
                    
                
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                  </div>
                  <!-- /.box-footer -->
                </form>
        	</div>
        
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/school/all_schools";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
    
  
 <link href="<?=HOST_URL?>/assets/admin/dist/css/jquery-ui.css" rel="stylesheet">
 <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-1.11.1.js"></script>
 <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-ui.js"></script>
  <script>
  $( function() {
		   $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
		
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	
				
				
				
				
				
			}
	  } );
  
  } );
  </script> 
  
  
  
  <!--File Upload START-->
<link href="<?=HOST_URL?>/assets/admin/dist/css/uploadfile.min.css" rel="stylesheet">
<script>
 $( function() {
    $( "#tabs" ).tabs();
  } );
  
 
</script>
<style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url('<?=IMG_PATH?>/delete.jpg');
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
		clear:both;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
	#overlay_container {
		position:relative;
	}
	#overloading {
		background-image:url('<?=IMG_PATH?>/preloader/preloader_2.gif');
		background-repeat:no-repeat;
		background-position:center;
		background-color:#CCC;
		position:absolute;
		left:0px;
		top:0px;
		opacity: 0.3;
		z-index: 10000;
	}
	#div_listing_container {
		display:none;	
	}
	.d_d_text {
		color:#745156;
		font-size:20px;
			
	}
	.ajax-upload-dragdrop {
		margin:auto;
		margin-bottom:10px;
		width:700px !important;
	}
	.ajax-file-upload-statusbar {
		margin:auto;
		margin-top:10px;
	}
	.ajax-file-upload {
		height:31px;
	}
	
	
	 #tabs-1{  
	    overflow-y:scroll; overflow-x:none;
	}

    #tabs-2{
		overflow-y:scroll; overflow-x:none;
	}
				  
  .ui-tabs-active{
		border-color:#efca86  !important;
   }
					 
	.ui-tabs .ui-tabs-nav li {
		float:left;
		font-size: 16px;
        font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif;
  }
  label{
	  display: inline-block;
      font-weight: 700;
  }
  
  .ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button {
    font-family:"Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
    font-size: 14px;
}
  
  .ui-widget{
	 font-size: 16px;
     font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
  }
  .form-control{
	 font-size: 14px; 
  }
</style>
 

  
     <div id="mid1" class="box box-success">
                        <div class="box-header">
                          <div class="col-sm-1" >
                          <h3 class="box-title">SEARCH</h3>
                          </div>
                          <div class="col-sm-11"> 
                              
                               <div class="col-sm-6"><input name="q" id="q" value="<?=urldecode($q)?>" type="text" class="form-control" placeholder="Search By Keyword"   > </div>
                               <div class="col-sm-2"><button class="btn btn-success" type="button" onclick="search_data()">Search</button>&nbsp;<button class="btn btn-success" type="button" 
                               onclick="reset_data();">Reset</button>
                               </div>
                           
                          </div>
                        </div>  
                     </div>     
    
            <!--Listing-->
                    <div id="mid1_list" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Schools</h3>
                          <div class="box-tools">
                            <?php if (count($rs_all_schools)>0) { echo $paging_string;}?>	
                            <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
                            <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>
                          </div>
                        </div>
                        
                        <div class="box-body">
                     <!--   <div style="color:#030; font-weight:bold;">You can sort students by using drag and drop of rows </div>-->
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle"><input id="select_all" type="checkbox" value="" /></th>
                              <!--<th width="10%" align="center" valign="middle">Sl No.</th>-->
                              <th width="25%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y">School Name <?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?><div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>
                             <!-- <th width="20%" align="center" valign="middle">Class</th>-->
                             
                              <th width="15%" align="center" valign="middle">Type</th>
                              <th width="10%" align="center" valign="middle">Picture</th>
                              <th width="5%" align="center" valign="middle">Status</th>
                              <th width="5%" align="center" valign="middle">Action</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                                for ($i=0; $i<count($rs_all_schools); $i++) { 
                                    $id 				= $rs_all_schools[$i]['id'];
                                    $tbl_school_id      = $rs_all_schools[$i]['tbl_school_id'];
                                    $name_en            = ucfirst($rs_all_schools[$i]['school_name']);
									$name_ar            = $rs_all_schools[$i]['school_name_ar'];
									$pic                = $rs_all_schools[$i]['logo'];
                                    $added_date         = $rs_all_schools[$i]['added_date'];
                                    $is_active          = $rs_all_schools[$i]['is_active'];
                                    $school_type        = ucfirst($rs_all_schools[$i]['school_type']);
									if($pic<>"") // class="img-circle"
										$pic_path           =   '<img width="100" height="80" src="'.IMG_SHOW_PATH_LOGO.'/'.$pic.'"  />';
									else
										$pic_path           =   '<img width="100" height="80" src="'.IMG_PATH_STUDENT.'/no_img.png"  
										style="border-color:1px solid #7C858C !important;" />';
                                    
                                    
                                    $added_date = date('m-d-Y',strtotime($added_date));
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_school_id?>" >
                              <td align="left" valign="middle">
                              <span style="float:left;">
                              <input id="school_id_enc" name="school_id_enc" class="checkbox" type="checkbox" value="<?=$tbl_school_id?>" />
                              </span>
                              
                             <?php /*?> <span style="float:left;">&nbsp;
                              <?php if($i<>0){ ?> <i class="fa fa-arrow-up"  style="color:#3c8dbc; cursor:pointer;"  aria-hidden="true" title="Sorting - Drag & Drop To Up"></i> &nbsp; <?php } ?>
                               <?php if($i<> count($rs_all_categories)-1){ ?> <i class="fa fa-arrow-down" style="color:#3c8dbc;cursor:pointer;" aria-hidden="true" title="Sorting - Drag & Drop To Down"></i> <?php } ?>
                              </span><?php */?>
                              </td>
                             <!-- <td align="left" valign="middle"><?=$offset+$i+1?></td>-->
                              <td align="left" valign="middle">
                              <div class="txt_en"><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/school_details/school_id_enc/<?=$tbl_school_id?>"><?=$name_en?></a></div>
                              <div class="txt_ar"><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/school_details/school_id_enc/<?=$tbl_school_id?>"><?=$name_ar?></a></div><br />
                          
                              </td>
                             <?php /*?> <td align="left" valign="middle"> 
                              <div class="txt_en"><?=$class_name?>&nbsp;<?=$section_name?></div>
                              <div class="txt_ar"><?=$class_name_ar?>&nbsp;<?=$section_name_ar?></div></td><?php */?>
                              <td align="left" valign="middle"><?=$school_type?></td>
                              <td align="left" valign="middle"><?=$pic_path?></td>
                              <td align="left" valign="middle">
                                <div id="act_deact_<?=$tbl_school_id?>">
                                <?php if (trim($is_active) == "Y") { ?>
                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_school_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                <?php } else { ?>
                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_school_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                <?php } ?>
                                </div>
                              </td>
                              <td align="left" valign="middle">
                                <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/edit_school/school_id_enc/<?=$tbl_school_id?>"><button class="btn bg-purple fa fa-pencil" type="button" title="Edit"></button></a>
                              </td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="10" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_all_schools)<=0) {
                            ?>
                            <tr>
                              <td colspan="10" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no schools available. Click on the + button to create one.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
    
            <!--Add or Create-->
            <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Add School</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
   	            <div class="box-body">
                    <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
    
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="school_name">School Name <span style="color:#F30; padding-left:2px;">*</span></label>
                          <div class="col-sm-5">
                            <input type="text" placeholder="School Name[En]" id="school_name" name="school_name" class="form-control" >
                          </div>
                           <div class="col-sm-5">
                            <input type="text" placeholder="School Name[Ar]" id="school_name_ar" name="school_name_ar" class="form-control" dir="rtl">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="school_type">School Type<span style="color:#F30; padding-left:2px;">*</span></label>
                           <div class="col-sm-10">
                            <label>
                              <input type="radio" id="school_type1" name="school_type" value="public" class="minimal"  checked="checked"  >
                              Public
                            </label>
                            &nbsp;
                            <label>
                              <input type="radio" id="school_type2" name="school_type" value="private" class="minimal"  >
                              Private
                            </label>
                          </div>
                        </div>
                        
                         <div class="form-group">
                          <label class="col-sm-2 control-label" for="email">Email</label>
        
                          <div class="col-sm-5">
                            <input type="text" placeholder="Email" id="email" name="email" class="form-control" maxlength="50">
                          </div>
                           <div class="col-sm-5">
                            <input type="text" placeholder="Contact Person" id="contact_person" name="contact_person" class="form-control" maxlength="50">
                          </div>
                        </div>
                        
                         <div class="form-group">
                          <label class="col-sm-2 control-label" for="phone">Contact Number</label>
        
                          <div class="col-sm-3">
                           <!--<span style="position:absolute; padding-left:20px; padding-top:5px; color: #999; font-size:14px;">Phone +971</span>--><input type="text" placeholder="Land Phone" id="phone" name="phone" class="form-control"  maxlength="20"><!--style="padding-left:60px;"-->
                          </div> 
                          
                          <div class="col-sm-3">
                          <!-- <span style="position:absolute; padding-left:20px; padding-top:5px; color:#999; font-size:14px;">Mobile 971</span>--><input type="text" placeholder="Mobile" id="mobile" name="mobile" class="form-control"  maxlength="20"><!--style="padding-left:60px;"-->
                          </div>
                          
                           <div class="col-sm-4">
                          <!-- <span style="position:absolute; padding-left:20px; padding-top:5px; color:#999; font-size:14px;">Fax 971</span>--><input type="text" placeholder="Fax" id="fax" name="fax" class="form-control"  maxlength="20"><!--style="padding-left:60px;"-->
                          </div>
                        </div>
                    
                        
                     <div class="form-group">
                         <label class="col-sm-2 control-label" for="description">Address </label>
                         <div class="col-sm-10">
                                  <textarea  dir="ltr" id="address" placeholder="Address" name="address" class="form-control" maxlength="250" ></textarea>
                         </div>
                        
                        <?php /*?> <div class="col-sm-5"> <label class="col-sm-2 control-label" for="description">Description </label>
                                  <textarea  dir="ltr" id="description" placeholder="Description " name="description" class="form-control" ></textarea>
                         </div><?php */?>
                     </div>
                        
                       
                      
                       <div class="form-group">
                          <label class="col-sm-2 control-label" for="last_name">Color Code</label>
        
                          <div class="col-sm-10">
                            <input type="text" placeholder="Color Code" id="color_code" name="color_code" class="form-control color">
                          </div>
                        </div>
                      
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="school_type">Status<span style="color:#F30; padding-left:2px;">*</span></label>
                           <div class="col-sm-10">
                            <label>
                              <input type="radio" id="is_active1" name="is_active" value="Y" class="minimal"  checked="checked"  >
                              Active
                            </label>
                            &nbsp;
                            <label>
                              <input type="radio" id="is_active2" name="is_active" value="N" class="minimal"  >
                              Inactive
                            </label>
                          </div>
                        </div>
                   
                      
                          <div class="form-group">
                          <label class="col-sm-2 control-label" for="picture">Picture</label>
        
                          <div class="col-sm-10">
                            <!--File Upload START-->
                                <style>
                                #advancedUpload {
                                    padding-bottom:0px;
                                }
                                </style>
                                     
                                <div id="advancedUpload">Upload File</div>
                                
                                <div id="uploaded_items" >
                                    <div id="div_listing_container" class="listing_container" style="display:block">	            
                                            <?php
                                                if (trim($img_url) != "") {
                                            ?>
                                                        <div id='<?=$tbl_uploads_id?>' class='box-header with-border'>
                                                          <div class='box-title'><img src='<?=$img_url?>' /></div>
                                                          <div class='box-tools'> <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick="confirm_delete_img_popup('<?=$tbl_uploads_id?>')">
                                                            </button>
                                                          </div>
                                                        </div>
                                                <style>
                                                    .ajax-upload-dragdrop {
                                                        display:none;	
                                                    }
                                                </style>        
                                            <?php		
                                                }
                                            ?>
                                    </div>        
                                </div>
                            <!--File Upload END-->
                          </div>
                        </div>
                        
                   <!--   Super Admin Section -->
                        <hr />
                         <div class="form-group">
                          <label class="col-sm-3 control-label" for="" style="text-decoration:underline;">School Super Administrator</label>
                        </div>
                       
                         <div class="form-group">
                          <label class="col-sm-2 control-label" for="first_name">Name <span style="color:#F30; padding-left:2px;">*</span></label>
                          <div class="col-sm-5">
                            <input type="text" placeholder="First Name" id="first_name" name="first_name" class="form-control" maxlength="50">
                          </div>
                           <div class="col-sm-5">
                            <input type="text" placeholder="Last Name" id="last_name" name="last_name" class="form-control" dir="ltr" maxlength="50">
                          </div>
                        </div>
                      
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="user_id">Username<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10">
                            <input type="text" placeholder="Username" id="user_id" name="user_id" class="form-control"  maxlength="32">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="password" >Password<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-5">
                            <input type="password" placeholder="Password" id="password" name="password" class="form-control" maxlength="16">
                          </div> 
                          <div class="col-sm-5">
                            <input type="password" placeholder="Confirm Password" id="confirm_password" name="confirm_password" class="form-control" maxlength="16">
                          </div>
                        </div>
                    
                     
                        <!-- /.box-body -->
                      <div class="box-footer">
                        <button class="btn btn-primary" type="button" onclick="ajax_validate()">Submit</button>
                        <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                      </div>
                      <!-- /.box-footer -->  
                      
            
           </form>
                </div>
           </div>
                
  
            <!--/Add or Create-->
                
        <!--/Admin Category Management-->

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>


<script src="<?=HOST_URL?>/assets/admin/dist/js/jquery.uploadfile.min.js"></script>
<script language="javascript">

//Primary Key for a Form. 

function set_item_id(obj) {
	item_id = obj.value;
	get_files();	
}

$(document).ready(function() {
	var uploadObj = $("#advancedUpload").uploadFile({
		url:"<?=HOST_URL?>/file_mgmt/upload_the_file",
		multiple:true,
		autoSubmit:true,
		maxFileSize:130000,
		fileName:"myfile",
		formData: {"module_name":"school"},
		dynamicFormData: function() {
			var data = { item_id:item_id}
			return data;
		},
		showStatusAfterSuccess:false,
		dragDropStr: "<span class='d_d_text'>Optionally Drag and Drop the File to Upload.</span>",
		abortStr:"Abourt",
		cancelStr:"Cancel",
		doneStr:"Done",
		multiDragErrorStr: "Multi Drag Error.",
		extErrorStr:"Extention Error:",
		sizeErrorStr:"Max Size Error:",
		uploadErrorStr:"Upload Error",
		onSelect:function(files) {
 		},
		onSubmit:function(files) {
 		},
		onSuccess:function(files, data, xhr) {
			if (data == "error") {
				alert("Error uploading file. Please try again.");
				return;
			}
			var obj = JSON.parse(data);
			var tbl_uploads_id = obj.tbl_uploads_id;
			var file_name_updated = obj.file_name_updated;
			
			//alert("tbl_uploads_id: "+tbl_uploads_id)
			//alert("file_name_updated: "+file_name_updated)
			add_uploaded_item(tbl_uploads_id, file_name_updated);
		},
		afterUploadAll:function() {
 		},
		onError: function(files, status, errMsg) {
 		}
	});

	$("#startUpload").click(function() {
		uploadObj.startUpload();
	});
	
	try { 
		$('input[type=file]').click();
	} catch(e) {
		alert(e)
	}
});

//Function called when file is uploaded
function add_uploaded_item(tbl_uploads_id, file_name_updated) {
	var str = "<div id='"+tbl_uploads_id+"' class='box-header with-border'> <div class='box-title'><img src='<?=IMG_SHOW_PATH_LOGO?>/"+file_name_updated+"' /></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_img_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
		
	$("#div_listing_container").show();
	$("#div_listing_container").append(str);
	$(".ajax-upload-dragdrop").hide();//Hide the upload button
return;
}

function confirm_delete_img_popup(tbl_uploads_id) {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		$('#'+tbl_uploads_id).hide();	
		$(".ajax-upload-dragdrop").show();
		
		var url_str = "<?=HOST_URL?>/file_mgmt/delete_file";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					tbl_uploads_id: tbl_uploads_id
				},
			success: function(data) {
				$("#pre-loader").hide();
			}
		});	
	} else {
		$("#pre-loader").hide();		
	}
}

function get_files() {
	var url_str = "<?=HOST_URL?>/misc/get_files.php";
	
	$.ajax({
		type: "POST",
		url: url_str,
		data: {
				module_name: "teacher",
				show_del: "Y",
				item_id: item_id//global variable
			},
		success: function(data) {
			$('#div_listing_container').show();
			$('#div_listing_container').html(data)
			
		}
	});	
}
</script>
<!--File Upload END-->
        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >
function search_data() {
		var q = $.trim($("#q").val()); 
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/all_schools/";
		
		if(q !='')
			url += "q/"+q+"/";
		
			url += "offset/0/";
		window.location.href = url;
	}

function reset_data() {
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/all_schools/";
		url += "offset/0/";
		window.location.href = url;
	}
</script>