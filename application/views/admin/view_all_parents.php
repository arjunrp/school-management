<?php
//Init Parameters
$parent_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}
?>
 
<style>
.txt_en {
	text-align:left;
	padding-left:2px;
}
.txt_ar {
	text-align:right;
	padding-right:2px;	
	direction:rtl;		
}
</style>
<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/parents/all_parents";
			}
		})
	});
	
function confirm_delete_popup() {
		var len = $("input[id='parent_id_enc']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more parent(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/parents/deleteParent",
			data: {
				parent_id_enc: $("input[id='parent_id_enc']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Parent(s) deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}	
	function ajax_activate(parent_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/parents/activateParent",
			data: {
				parent_id_enc: parent_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Parent activated successfully.", 'green')

				$('#act_deact_'+parent_id_enc).html('<span style="cursor:pointer" onClick="ajax_deactivate(\''+parent_id_enc+'\')" onMouseOver="deactivate_me(this)" onMouseOut="reset_activate(this)" class="label label-success">Active</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_deactivate(parent_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/parents/deactivateParent",
			data: {
				parent_id_enc: parent_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Parent de-activated successfully.", 'green')
				
				$('#act_deact_'+parent_id_enc).html('<span style="cursor:pointer" onClick="ajax_activate(\''+parent_id_enc+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/parents/is_exist_student",
			data: {
				parent_id_enc: "<?=$parent_id_enc?>",
				first_name: $('#first_name').val(),
				last_name: $('#last_name').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
					refresh_page = "N";
					my_alert("Parent is already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_create();
				}
			
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist_edit() {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/parents/is_exist_parent_details",
			data: {
				parent_id_enc: $('#parent_id_enc').val(),
				emirates_id: $('#emirates_id').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String(data);
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
					refresh_page = "N";
					my_alert("Parent is already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_save_changes();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_create() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/parents/add_parent",
			data: {
				parent_id_enc: "<?=$parent_id_enc?>",
				first_name_parent      : $('#first_name_parent').val(),
				first_name_parent_ar   : $('#first_name_parent_ar').val(),
				last_name_parent       : $('#last_name_parent').val(),
				last_name_parent_ar    : $('#last_name_parent_ar').val(),
				dob_month_parent       : $('#dob_month_parent').val(),
				dob_day_parent         : $('#dob_day_parent').val(),
				dob_year_parent        : $('#dob_year_parent').val(),
				gender_parent          : $('#gender_parent').val(),
				mobile_parent          : $('#mobile_parent').val(),
				email_parent           : $('#email_parent').val(),
				emirates_id_parent     : $('#emirates_id_parent').val(),
				parent_user_id         : $('#parent_user_id').val(),
				password               : $('#password').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
				refresh_page = "Y";
				    my_alert("Parent added successfully.", 'green');
				    $("#pre-loader").hide();
				}else{
					refresh_page = "N";
					my_alert("Parent added failed, Please try again.", 'red');
					$("#pre-loader").hide();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_save_changes() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/parents/save_parent_changes",
			data: {
				tbl_parent_id   : $('#parent_id_enc').val(),
				first_name      : $('#first_name').val(),
				first_name_ar   : $('#first_name_ar').val(),
				last_name       : $('#last_name').val(),
				last_name_ar    : $('#last_name_ar').val(),
				dob_month       : $('#dob_month').val(),
				dob_day         : $('#dob_day').val(),
				dob_year        : $('#dob_year').val(),
				gender          : $("input[name='gender']:checked").val(),
				mobile          : $('#mobile').val(),
				email           : $('#email').val(),
				emirates_id     : $('#emirates_id').val(),
				user_id         : $('#user_id').val(),
				password        : $('#password').val(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Changes saved successfully.", 'green');
				
				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
</script>
<script language="javascript">
	
	function ajax_validate_parent_edit() {
		if (validate_first_name_parent() == false || validate_last_name_parent() == false || validate_dob_parent() == false || isMobile_parent() == false ||  validate_email_parent() == false || validate_parent_emirates_id() == false || validate_password() == false || validate_confirm_password() == false || isPasswordSame() == false ) 
		{
			return false;
		} 
		else{
			is_exist_edit();
			
		}
	}	
	
	
  /*********************************** PARENT VALIDATION SECTION **************************/
	
	 function validate_first_name_parent() {
		var regExp = / /g;
		var str = $("#first_name").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("The First Name [En] is blank. Please write First Name [En].")
			$("#first_name").val('');
			$("#first_name").focus();
		return false;
		}
		var regExp = / /g;
		var str = $("#first_name_ar").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("The First Name [Ar] is blank. Please write First Name [Ar].")
			$("#first_name_ar").val('');
			$("#first_name_ar").focus();
		return false;
		}
	}
	
	function validate_last_name_parent() {
		var regExp = / /g;
		var str = $("#last_name").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("The Last Name [En] is blank. Please write Last Name [En].")
			$("#last_name").val('');
			$("#last_name").focus();
		return false;
		}
		var regExp = / /g;
		var str = $("#last_name_ar").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("The Last Name [Ar] is blank. Please write Last Name [Ar].")
			$("#last_name_ar").val('');
			$("#last_name_ar").focus();
		return false;
		}
	}
	
	function validate_dob_parent() {
		var month_index 	= $("#dob_month").val();
		var day_index	  = $("#dob_day").val();
		var year_index 	 = $("#dob_year").val();
		if (month_index == 0 && day_index == 0 && year_index ==0) {
			return true;
		}else if(month_index == 0 || day_index == 0 || year_index ==0) {
			my_alert("Please select Date of Birth.")
			return false		
		}
	}
	
	function isMobile_parent() {
		var strPhone = $("#mobile").val();
		if(strPhone== "") {
			return true;
		}else{
				if (strPhone.length < 12 || strPhone.length > 12) {
					my_alert("Please enter valid mobile number.");
					$("#mobile").focus();
					return false;
			    }

				for (var i = 0; i < strPhone.length; i++) {
				var ch = strPhone.substring(i, i + 1);
					if  (ch < "0" || "9" < ch) {
						my_alert("The mobile number in digits only, Please re-enter your valid mobile number");
						$("#mobile").focus();
						return false;
					}
				}
		}
	 return true;
	}
		
	function validate_email_parent() {
		var regExp = / /g;
		var str = $("#email").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
		     return true;	
		}
		if (!isNaN(str)) {
			my_alert("Invalid Email Id.");
			$("#email").focus();
			$("#email").select();
			return false;
		}

		if(str.indexOf('@', 0) == -1) {
			my_alert("Invalid Email Id.");
			$("#email").focus();
			$("#email").select();
			return false;
		}

	 return true;
	}

   function validate_parent_emirates_id() {
	    var regExp = / /g;
		var str = $("#emirates_id").val();  
		str = str.replace(regExp,'');
		if (str.length <= 0) {
		    my_alert("The Emirates ID for Parent is mandatory. Please write Parent’s Emirates ID.")
			$("#emirates_id").focus();
			return false;
		}

		var emrno = /^\(?([0-9]{3})\)?[-]?([0-9]{4})[-]?([0-9]{7})[-]?([0-9]{1})$/;  
		if(str.match(emrno))  
		{  
			return true;    
		}  
		else  
		{  
			my_alert("Please write correct Parent’s Emirates ID"); 
			return false;  
		}  	
	}

	
	 
    /* Password */
	function validate_password() {
		var str = $('#password').val(); 
		if (str == "") {
			return true;
		}
		if($('#password').val()!="") {
			var str = $('#password').val();
			var regExp = / /g;
			var tmp = $('#password').val();
			tmp = tmp.replace(regExp,'');
			if (tmp.length <= 0) {
				my_alert("Enter valid Password.");
				$('#password').focus();
			return false;
			}	
		}
		if (str.length < 6) {
			my_alert("The Password should be greater than 5 Characters.");
			$('#password').focus();
			$('#password').select();
			return false;
		}

	return true;
	}


	/* Retype Password */
	function validate_confirm_password() {
		var str = $('#confirm_password').val();
		if (str == "") {
			
			var strPass = $('#password').val(); 
			if (strPass == "") {
			  return true;
			}else{
			
			   my_alert("The Confirm Password field is blank. Please Retype Password.");
			   $('#confirm_password').focus();
			   return false;
			}
		}
		if($('#confirm_password').val()!="") {
			    var strPass = $('#password').val(); 
				if (strPass == "") {
					my_alert("The Password field is blank. Please enter Password.");
					$('#password').focus();
					return false;
				}
			
				var str = $('#confirm_password').val();
				var regExp = / /g;
				var tmp = $('#confirm_password').val();
				tmp = tmp.replace(regExp,'');
				if (tmp.length <= 0) {
					my_alert("Enter valid Confirm Password.");
					$('#confirm_password').focus();
					return false;
				}
		   
		}
		
		
	return true;
	}

	/* Check both password */
	function isPasswordSame() {
		var str1 = $('#password').val();
		var str2 = $('#confirm_password').val();
		if (str1 != str2) {
			my_alert("Password mismatch, Please Retype same Passwords in both fields.");
			$('#confirm_password').focus();
			return false;
		}
	return true;
	}


	function ajax_approve(parent_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/parents/approve_parent",
			data: {
				parent_id_enc: parent_id_enc,
				is_ajax: true
			},
			success: function(data) {
				//alert(data);
				//return;
				refresh_page = "Y";
				my_alert("Parent/Student details have been approved successfully.", 'green')
				//setTimeout(function(){window.location.href = window.location.href;},1500);
				
				//$('#act_deact_'+student_id_enc).html('<span style="cursor:pointer" onClick="ajax_activate(\''+student_id_enc+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}


</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Parents <small> Management</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Parents</li>
    </ol>
    <!--/BREADCRUMB--> 
    <div style="clear:both"></div>
  </section>
  
  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {

			$tbl_parent_id              = $parent_obj[0]['tbl_parent_id'];
			$first_name                 = $parent_obj[0]['first_name'];
			$first_name_ar              = $parent_obj[0]['first_name_ar'];
			$last_name                  = $parent_obj[0]['last_name'];
			$last_name_ar               = $parent_obj[0]['last_name_ar'];
			$mobile                     = $parent_obj[0]['mobile'];
			$dob_month                  = $parent_obj[0]['dob_month'];
			$dob_day                    = $parent_obj[0]['dob_day'];
			$dob_year                   = $parent_obj[0]['dob_year'];
			$gender                     = $parent_obj[0]['gender'];
			$email                      = $parent_obj[0]['email'];
		    $user_id                    = $parent_obj[0]['user_id'];
			$password                   = $parent_obj[0]['password'];
			$emirates_id                = $parent_obj[0]['emirates_id'];
			$added_date                 = $parent_obj[0]['added_date'];
			$is_active                  = $parent_obj[0]['is_active'];
			$mobile                     = str_replace("+",'',$mobile);
			
			//echo $gender;
	?>
        <!--Edit-->
        
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Parent</h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/parents/all_parents"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_edit" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                  
                   
                  
                   <div class="form-group">
                      
                      <label class="col-sm-2 control-label" for="first_name">First Name [En]<span style="color:#F30; padding-left:2px;">*</span></label>
                      <div class="col-sm-4">
                        <input type="text" placeholder="First Name[En]" id="first_name" name="first_name" class="form-control" value="<?=$first_name?>" >
                      </div>
                       
                       <label class="col-sm-2 control-label" for="first_name_ar">First Name [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-4">
                        <input type="text" placeholder="First Name[Ar]" id="first_name_ar" name="first_name_ar" class="form-control" dir="rtl" value="<?=$first_name_ar?>" >
                      </div>
                    </div>
                    
                      <div class="form-group">
                      <label class="col-sm-2 control-label" for="last_name">Last Name [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-4">
                        <input type="text" placeholder="Last Name[En]" id="last_name" name="last_name" class="form-control" value="<?=$last_name?>">
                      </div> 
                      
                      <label class="col-sm-2 control-label" for="last_name_ar">Last Name [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-4">
                        <input type="text" placeholder="Last Name[Ar]" id="last_name_ar" name="last_name_ar" class="form-control" dir="rtl" value="<?=$last_name_ar?>">
                      </div>
                    </div>
                    
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="dob_month"> DOB</label>
    
                      <div class="col-sm-1">
                        <select name="dob_month" id="dob_month"  class="form-control" tabindex="6">
                                      	<option value="">--Month--</option>
                                      	<?php for ($m=1; $m<=12; $m++) { ?>
                                      	<option value="<?=$m?>" <?php if ($dob_month == $m) {echo "selected";}?> ><?=$m?></option>
                                        <?php } ?>
                        </select>
                     </div>
                     <div class="col-sm-1">
                                      <?php if (!isset($dob_day) || trim($dob_day) == "") {$dob_day = '';}?>
                                      <select name="dob_day" id="dob_day" tabindex="7" class="form-control">
                                        <option value="">--Day--</option>
                                      	<?php for ($d=1; $d<=31; $d++) { ?>
                                      	<option value="<?=$d?>" <?php if ($dob_day == $d) {echo "selected";}?> ><?=$d?></option>
                                        <?php } ?>
                    </select>
                    </div>
                    <div class="col-sm-1">
                                      <?php if (!isset($dob_year) || trim($dob_year) == "") {$dob_year = '';}?>
                                      <select name="dob_year" id="dob_year" tabindex="8" class="form-control">
                                        <option value="">--Year--</option>

                                      	<?php for ($y=1950; $y<=date('Y'); $y++) { ?>
                                      	<option value="<?=$y?>" <?php if ($dob_year == $y) {echo "selected";}?> ><?=$y?></option>
                                        <?php } ?>
                                      </select>              
                    </div>  
                    
                      <label class="col-sm-3 control-label" for="gender">Relationship<span style="color:#F30; padding-left:2px;">*</span></label>
                       <div class="col-sm-4">
                        <label>
                          <input type="radio" id="gender" name="gender" value="male" class="minimal" <?php if($gender=="male"){?> checked="checked" <?php } ?> >
                          Father
                        </label>
                        &nbsp;
                        <label>
                          <input type="radio" id="gender" name="gender" value="female" class="minimal" <?php if($gender=="female"){?> checked="checked" <?php } ?>  >
                          Mother
                        </label>
                         &nbsp;
                        <label>
                          <input type="radio" id="gender" name="gender" value="other" class="minimal" <?php if($gender=="other"){?> checked="checked" <?php } ?>  >
                          Other
                        </label>
                      </div>
                    
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="mobile">Mobile</label>
    
                      <div class="col-sm-4">
                       <span style="position:absolute; padding-left:20px; padding-top:5px;"> +</span> <input type="text" placeholder="Mobile" id="mobile" name="mobile" class="form-control" style="padding-left:30px;" value="<?=$mobile?>">
                      </div> 
                      
                      <label class="col-sm-2 control-label" for="email">Email</label>
    
                      <div class="col-sm-4">
                        <input type="text" placeholder="Email" id="email" name="email" class="form-control" value="<?=$email?>" >
                      </div>
                    </div>
                    
                      <div class="form-group">
                      <label class="col-sm-2 control-label" for="emirates_id">Emirates Id<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-4">
                        <input type="text" placeholder="Emirates Id" id="emirates_id" name="emirates_id_parent" class="form-control" value="<?=$emirates_id?>" >
                      </div>
                    </div>
                    
                   
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="user_id">User Id<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-4">
                        <input type="text" placeholder="User Id " id="user_id" name="user_id" class="form-control" value="<?=$user_id?>" readonly="readonly">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="password">Password<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-4">
                        <input type="password" placeholder="Password" id="password" name="password" class="form-control">
                      </div> 
                      
                      <label class="col-sm-2 control-label" for="confirm_password">Confirm Password<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-4">
                        <input type="password" placeholder="Confirm Password" id="confirm_password" name="confirm_password" class="form-control">
                      </div>
                    </div>
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate_parent_edit()">Save Changes</button>
                    <input type="hidden" name="parent_id_enc" id="parent_id_enc" value="<?=$tbl_parent_id?>" />
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
	 <script src="<?=JS_PATH?>/mask_input/jquery.maskedinput.min.js"></script>
     <script>
      $(function() {
        $.mask.definitions['~'] = "[+-]";
		$("#mobile").mask("999999999999");
		$("#emirates_id").mask("999-9999-9999999-9");
    });
	</script>	                 
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/parents/all_parents";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
    
    
 <link href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" rel="stylesheet">
 <script src="http://code.jquery.com/jquery-1.11.1.js"></script>
 <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
 
  
  <script>
  $( function() {
		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
				
				/* var order = $("#tabledivbody").sortable("serialize");
				
				alert(order);
				
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/your/url/here'
				});*/
				
				
				
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	
				
				
				
				
				
			}
	  } );
  
  } );
  </script> 
  
  
  
  <!--File Upload START-->
<link href="http://hayageek.github.io/jQuery-Upload-File/uploadfile.min.css" rel="stylesheet">
<script>
 $( function() {
    $( "#tabs" ).tabs();
  } );
  
 
</script>
<style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url('<?=IMG_PATH?>/delete.jpg');
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
		clear:both;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
	#overlay_container {
		position:relative;
	}
	#overloading {
		background-image:url('<?=IMG_PATH?>/preloader/preloader_2.gif');
		background-repeat:no-repeat;
		background-position:center;
		background-color:#CCC;
		position:absolute;
		left:0px;
		top:0px;
		opacity: 0.3;
		z-index: 10000;
	}
	#div_listing_container {
		display:none;	
	}
	.d_d_text {
		color:#745156;
		font-size:20px;
			
	}
	.ajax-upload-dragdrop {
		margin:auto;
		margin-bottom:10px;
		width:700px !important;
	}
	.ajax-file-upload-statusbar {
		margin:auto;
		margin-top:10px;
	}
	.ajax-file-upload {
		height:31px;
	}
	
	
	 #tabs-1{  
	    overflow-y:scroll; overflow-x:none;
	}

    #tabs-2{
		overflow-y:scroll; overflow-x:none;
	}
				  
  .ui-tabs-active{
		border-color:#efca86  !important;
   }
					 
	.ui-tabs .ui-tabs-nav li {
		float:left;
		font-size: 16px;
        font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif;
  }
  label{
	  display: inline-block;
      font-weight: 700;
  }
  
  .ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button {
    font-family:"Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
    font-size: 14px;
}
  
  .ui-widget{
	 font-size: 16px;
     font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
  }
  .form-control{
	 font-size: 14px; 
  }
</style>
 
<script src="http://hayageek.github.io/jQuery-Upload-File/jquery.uploadfile.min.js"></script>

<script language="javascript">

var item_id = "<?=$parent_id_enc?>";//Primary Key for a Form. 

function set_item_id(obj) {
	item_id = obj.value;
	get_files();	
}

$(document).ready(function() {
	var uploadObj = $("#advancedUpload").uploadFile({
		url:"<?=HOST_URL?>/file_mgmt/upload_the_file",
		multiple:true,
		autoSubmit:true,
		maxFileSize:130000,
		fileName:"myfile",
		formData: {"module_name":"student"},
		dynamicFormData: function() {
			var data = { item_id:item_id}
			return data;
		},
		showStatusAfterSuccess:false,
		dragDropStr: "<span class='d_d_text'>Optionally Drag and Drop the File to Upload.</span>",
		abortStr:"Abourt",
		cancelStr:"Cancel",
		doneStr:"Done",
		multiDragErrorStr: "Multi Drag Error.",
		extErrorStr:"Extention Error:",
		sizeErrorStr:"Max Size Error:",
		uploadErrorStr:"Upload Error",
		onSelect:function(files) {
 		},
		onSubmit:function(files) {
 		},
		onSuccess:function(files, data, xhr) {
			if (data == "error") {
				alert("Error uploading file. Please try again.");
				return;
			}
			var obj = JSON.parse(data);
			var tbl_uploads_id = obj.tbl_uploads_id;
			var file_name_updated = obj.file_name_updated;
			
			//alert("tbl_uploads_id: "+tbl_uploads_id)
			//alert("file_name_updated: "+file_name_updated)
			add_uploaded_item(tbl_uploads_id, file_name_updated);
		},
		afterUploadAll:function() {
 		},
		onError: function(files, status, errMsg) {
 		}
	});

	$("#startUpload").click(function() {
		uploadObj.startUpload();
	});
	
	try { 
		$('input[type=file]').click();
	} catch(e) {
		alert(e)
	}
});

//Function called when file is uploaded
function add_uploaded_item(tbl_uploads_id, file_name_updated) {
	var str = "<div id='"+tbl_uploads_id+"' class='box-header with-border'> <div class='box-title'><img src='<?=IMG_PATH_STUDENT?>/"+file_name_updated+"' /></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_img_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
		
	$("#div_listing_container").show();
	$("#div_listing_container").append(str);
	$(".ajax-upload-dragdrop").hide();//Hide the upload button
return;
}

function confirm_delete_img_popup(tbl_uploads_id) {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		$('#'+tbl_uploads_id).hide();	
		$(".ajax-upload-dragdrop").show();
		
		var url_str = "<?=HOST_URL?>/file_mgmt/delete_file";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					tbl_uploads_id: tbl_uploads_id
				},
			success: function(data) {
				$("#pre-loader").hide();
			}
		});	
	} else {
		$("#pre-loader").hide();		
	}
}

function get_files() {
	var url_str = "<?=HOST_URL?>/misc/get_files.php";
	
	$.ajax({
		type: "POST",
		url: url_str,
		data: {
				module_name: "admin_user",
				show_del: "Y",
				item_id: item_id//global variable
			},
		success: function(data) {
			$('#div_listing_container').show();
			$('#div_listing_container').html(data)
			
		}
	});	
}
</script>
<!--File Upload END-->
  
    
                   <div id="mid1" class="box box-success">
                        <div class="box-header">
                          <div class="col-sm-1" >
                          <h3 class="box-title">SEARCH</h3>
                          </div>
                          <div class="col-sm-11"> 
                              <div class="col-sm-3"> 
                             
                              <select name="tbl_class_id" id="tbl_class_id" class="form-control">
                              <option value="">--Select Class --</option>
							  
							  <?php
                                    for ($u=0; $u<count($classes_list); $u++) { 
                                        $tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
                                        $class_name             = $classes_list[$u]['class_name'];
                                        $class_name_ar          = $classes_list[$u]['class_name_ar'];
										$section_name           = $classes_list[$u]['section_name'];
                                        $section_name_ar        = $classes_list[$u]['section_name_ar'];
                                        if($tbl_sel_class_id == $tbl_class_id_u)
                                           $selClass = "selected";
                                         else
                                           $selClass = "";
                                  ?>
                                      <option value="<?=$tbl_class_id_u?>"  <?=$selClass?>  >
                                      <?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;
                                    <?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>   
                                 
                                 
                                 
                               </div>
                               <div class="col-sm-3"><input name="q" id="q" value="<?=urldecode($q)?>" type="text" class="form-control" placeholder="Search By Name, Email, Mobile, Gender, Emirates Id, Student Name etc."   >    </div>
                               <div class="col-sm-3"><label><input name="is_approved" type="radio" value="Y" <?php if (trim($is_approved) == "Y") {echo "checked";} ?> />Approved</label>&nbsp; <!--<label><input name="is_approved" type="radio" value="R" <?php if (trim($is_approved) == "R") {echo "checked";} ?> />Rejected</label>&nbsp; --><label><input name="is_approved" type="radio" value="N" <?php if (trim($is_approved) == "N") {echo "checked";} ?> />Pending</label></div>
                               <div class="col-sm-2"><button class="btn btn-success" type="button" onclick="search_data()">Search</button>&nbsp;<button class="btn btn-success" type="button" onclick="reset_data();">Reset</button>
                               </div>
                           
                          </div>
                          
                       
                        </div>  
                     </div>     
    
    
            <!--Listing-->
                    <div id="mid1_list" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Parents</h3>
                          <div class="box-tools">
                            <?php if (count($rs_all_parents)>0) { echo $paging_string;}?>	
                           <!-- <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>-->
                            <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>
                            <button class="btn bg-green fa fa-print" type="button" title="Generate ID Card"  onclick="generate_parent_id(<?=$offset?>)" ></button>
                          </div>
                        </div>
                        
                        <div class="box-body">
                     <!--   <div style="color:#030; font-weight:bold;">You can sort categories by using drag and drop of rows </div>-->
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle"><input id="select_all" type="checkbox" value="" /></th>
                              <!--<th width="10%" align="center" valign="middle">Sl No.</th>-->
                              <th width="27%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y">Name <?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?><div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>
                               <th width="27%" align="center" valign="middle">Students</th>
                              <th width="15%" align="center" valign="middle">Class</th>
                              <th width="15%" align="center" valign="middle">Contact</th>
                              <th width="5%" align="center" valign="middle">Status</th>
                              <th width="5%" align="center" valign="middle">Action</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                              //  print_r($rs_all_parents);
								
								for ($i=0; $i<count($rs_all_parents); $i++) { 
                                    $id = $rs_all_students[$i]['id'];
                                    $tbl_parent_id      = $rs_all_parents[$i]['tbl_parent_id'];
                                    $parent_name_en     = ucfirst($rs_all_parents[$i]['first_name'])." ".ucfirst($rs_all_parents[$i]['last_name']);
									$parent_name_ar     = $rs_all_parents[$i]['first_name_ar']." ".$rs_all_parents[$i]['last_name_ar'];
                                    $mobile             = $rs_all_parents[$i]['mobile'];
									$email              = $rs_all_parents[$i]['email'];
									$emirates_id        = $rs_all_parents[$i]['emirates_id'];
									$gender             = ucfirst($rs_all_parents[$i]['gender']);
                                    $added_date         = $rs_all_parents[$i]['added_date'];
                                    $is_active          = $rs_all_parents[$i]['is_active'];
                                    $is_approved          = $rs_all_parents[$i]['is_approved'];
                                    $added_date = date('m-d-Y',strtotime($added_date));
									
									$children_list =  $rs_all_parents[$i]['rs_all_children'];
									$stud_det_list = "";
									$class_det_list = "";
									  for($n=0;$n<count($children_list);$n++)
									  {
										  if($n<>0)
										  {
											   $class_det_list .='<br><div style="border:1px dotted #444;"></div><br>';
											   $stud_det_list .='<br><div style="border:1px dotted #444;"></div><br>';
										  }
										  $name     = $children_list[$n]['first_name']." ".$children_list[$n]['last_name'];
										  $name_ar  = $children_list[$n]['first_name_ar']." ".$children_list[$n]['last_name_ar'];
										  $class    = $children_list[$n]['class_name']." ".$children_list[$n]['section_name'];
										  $class_ar = $children_list[$n]['class_name_ar']." ".$children_list[$n]['section_name_ar'];
										  $stud_det_list .= '<div class="txt_en">'.$name.'</div><div class="txt_ar">'.$name_ar.'</div>';
										  $class_det_list .= '<div class="txt_en">'.$class.'</div><div class="txt_ar">'.$class_ar.'</div>';
									  }
									
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_parent_id?>" >
                              <td align="left" valign="middle">
                              <span style="float:left;">
                              <input id="parent_id_enc" name="parent_id_enc" class="checkbox" type="checkbox" value="<?=$tbl_parent_id?>" />
                              </span>
                              
                             <?php /*?> <span style="float:left;">&nbsp;
                              <?php if($i<>0){ ?> <i class="fa fa-arrow-up"  style="color:#3c8dbc; cursor:pointer;"  aria-hidden="true" title="Sorting - Drag & Drop To Up"></i> &nbsp; <?php } ?>
                               <?php if($i<> count($rs_all_categories)-1){ ?> <i class="fa fa-arrow-down" style="color:#3c8dbc;cursor:pointer;" aria-hidden="true" title="Sorting - Drag & Drop To Down"></i> <?php } ?>
                              </span><?php */?>
                              </td>
                             <!-- <td align="left" valign="middle"><?=$offset+$i+1?></td>-->
                              <td align="left" valign="middle">
                              <div class="txt_en"><?=$parent_name_en?></div>
                              <div class="txt_ar"><?=$parent_name_ar?></div> 
							  <?php if($gender=="Female"){?>
                              <i class="fa fa-female" aria-hidden="true"></i><span style="float:right;">Emirates Id:&nbsp;<?=$emirates_id?></span>
                             <?php } else { ?>
                              <i class="fa fa-male" aria-hidden="true"></i><span style="float:right;">Emirates Id:&nbsp;<?=$emirates_id?></span>
                             <?php } ?></td>
                             
                              <td align="left" valign="middle"><?=$stud_det_list?></td>
                              <td align="left" valign="middle"><?=$class_det_list?></td>
                              <td align="left" valign="middle"><?=$mobile?><br /><?=$email?></td>
                              <td align="left" valign="middle">
                                <div id="act_deact_<?=$tbl_parent_id?>">
                                    <?php if (trim($is_approved) == "Y") { ?>                              
                                                <?php if (trim($is_active) == "Y") { ?>
                                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_parent_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                                <?php } else { ?>
                                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_parent_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                                <?php } ?>
                                    <?php	} else {	?>
                                            <span style="cursor:pointer" onclick="ajax_approve('<?=$tbl_parent_id?>')" onmouseover="approve_me(this)" onmouseout="reset_approve(this)" class="label label-danger">Approve</span>
                                    <?php 	} ?>
                                </div>
                              </td>
                              <td align="left" valign="middle">
                                <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/parents/edit_parent/parent_id_enc/<?=$tbl_parent_id?>"><button class="btn bg-purple fa fa-pencil" type="button" title="Edit"></button></a>
                              </td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="10" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_all_parents)<=0) {
                            ?>
                            <tr>
                              <td colspan="10" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no parents available. Click on the + button to create one.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
    
            <!--Add or Create-->
              <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Student</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
     <style>
	 .disabled {
		pointer-events:none; //This makes it not clickable
		opacity:0.6;         //This grays it out to look disabled
	}
	 </style>           
                
                
                
   	  <div id="tabs" >
          <ul>
            <li id="li-tab1" ><a href="#tabs-1" id="tab1">STUDENT INFORMATION</a></li>
           <li id="li-tab2" class="disabled" ><a  href="#tabs-2" id="tab2">PARENT INFORMATION</a></li> <!---->
          </ul>     
          <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">

                <div id="tabs-1">
                <!-- /.box-header -->
                <!-- form start -->
                  <div class="box-body">
                  
                   
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="first_name">First Name [En]</label>
                      <div class="col-sm-10">
                        <input type="text" placeholder="First Name[En]" id="first_name" name="first_name" class="form-control">
                      </div>
                      
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="first_name_ar">First Name [Ar]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="First Name[Ar]" id="first_name_ar" name="first_name_ar" class="form-control" dir="rtl">
                      </div>
                    </div>
                    
                      <div class="form-group">
                      <label class="col-sm-2 control-label" for="last_name">Last Name [En]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Last Name[En]" id="last_name" name="last_name" class="form-control">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="last_name_ar">Last Name [Ar]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Last Name[Ar]" id="last_name_ar" name="last_name_ar" class="form-control" dir="rtl">
                      </div>
                    </div>
                    
                 
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="category_name_ar">DOB</label>
    
                      <div class="col-sm-2">
                        <select name="dob_month" id="dob_month"  class="form-control" tabindex="6">
                                      	<option value="">--Month--</option>
                                      	<?php for ($m=1; $m<=12; $m++) { ?>
                                      	<option value="<?=$m?>" <?php if ($dob_month == $m) {echo "selected";}?> ><?=$m?></option>
                                        <?php } ?>
                        </select>
                     </div>
                     <div class="col-sm-2">
                                      <?php if (!isset($dob_day) || trim($dob_day) == "") {$dob_day = '';}?>
                                      <select name="dob_day" id="dob_day" tabindex="7" class="form-control">
                                        <option value="">--Day--</option>
                                      	<?php for ($d=1; $d<=31; $d++) { ?>
                                      	<option value="<?=$d?>" <?php if ($dob_day == $d) {echo "selected";}?> ><?=$d?></option>
                                        <?php } ?>
                    </select>
                    </div>
                    <div class="col-sm-2">
                                      <?php if (!isset($dob_year) || trim($dob_year) == "") {$dob_year = '';}?>
                                      <select name="dob_year" id="dob_year" tabindex="8" class="form-control">
                                        <option value="">--Year--</option>

                                      	<?php for ($y=1950; $y<=date('Y'); $y++) { ?>
                                      	<option value="<?=$y?>" <?php if ($dob_year == $y) {echo "selected";}?> ><?=$y?></option>
                                        <?php } ?>
                                      </select>              
                    </div>  
                    </div>
                    
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="gender">Relationship</label>
                       <div class="col-sm-10">
                        <label>
                          <input type="radio" id="gender" name="gender" value="male" class="minimal"  checked="checked"  >
                          Father
                        </label>
                        &nbsp;
                        <label>
                          <input type="radio" id="gender" name="gender" value="female" class="minimal"  >
                          Mother
                        </label>
                         &nbsp;
                        <label>
                          <input type="radio" id="gender" name="gender" value="other" class="minimal"  >
                          Mother
                        </label>
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="mobile">Parent's Mobile</label>
    
                      <div class="col-sm-10">
                       <span style="position:absolute; padding-left:20px; padding-top:5px;"> +971</span><input type="text" placeholder="" id="mobile" name="mobile" class="form-control" style="padding-left:60px;">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="email">Email</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Email" id="email" name="email" class="form-control">
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="country">Nationality</label>
    
                      <div class="col-sm-10">
                      
                                      <select name="country" id="country" tabindex="8" class="form-control">
                                      	<?php for ($c=0; $c<count($countries_list); $c++) { ?>
                                      	<option value="<?=$countries_list[$c]['country_id']?>"><?=$countries_list[$c]['country_name']?></option>
                                        <?php } ?>
                                      </select>         
                      </div>
                    </div>
                    
                    
                      <div class="form-group">
                      <label class="col-sm-2 control-label" for="emirates_id_father">Emirates Id [Father]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Emirates Id [Father]" id="emirates_id_father" name="emirates_id_father" class="form-control">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="emirates_id_mother">Emirates Id [Mother]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Emirates Id [Mother]" id="emirates_id_mother" name="emirates_id_mother" class="form-control" >
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="tbl_academic_year_id">Academic Year</label>
    
                      <div class="col-sm-10">
                       <select name="tbl_academic_year_id" id="tbl_academic_year_id" class="form-control" >
                              <?php
                                    for ($a=0; $a<count($academic_list); $a++) { 
                                        $tbl_academic_year_id    = $academic_list[$a]['tbl_academic_year_id'];
                                        $academic_start          = $academic_list[$a]['academic_start'];
                                        $academic_end            = $academic_list[$a]['academic_end'];
									
                                  ?>
                                      <option value="<?=$tbl_academic_year_id?>">
                                      <?=$academic_start?>&nbsp;-&nbsp;<?=$academic_end?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>
                      </div>
                    </div>
                    
                 
                    
                    <div class="form-group">
                     <label class="col-sm-2 control-label" for="tbl_class_id">Class</label>
                     <div class="col-sm-10">
                                 <select name="tbl_class_id" id="tbl_class_id" class="form-control">
                              <option value="">--Select Class --</option>
							  
							  <?php
                                    for ($u=0; $u<count($classes_list); $u++) { 
                                        $tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
                                        $class_name             = $classes_list[$u]['class_name'];
                                        $class_name_ar          = $classes_list[$u]['class_name_ar'];
										$section_name           = $classes_list[$u]['section_name'];
                                        $section_name_ar        = $classes_list[$u]['section_name_ar'];
                                        if($tbl_sel_class_id == $tbl_class_id_u)
                                           $selClass = "selected";
                                         else
                                           $selClass = "";
                                  ?>
                                      <option value="<?=$tbl_class_id_u?>"  <?=$selClass?> >
                                      <?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;
                                    <?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>
                   </div>
                   </div>
                    
                  
                       <div class="form-group">
                      <label class="col-sm-2 control-label" for="picture">Picture</label>
    
                      <div class="col-sm-10">
                        <!--File Upload START-->
                            <style>
                            #advancedUpload {
                                padding-bottom:0px;
                            }
                            </style>
                                 
                            <div id="advancedUpload">Upload File</div>
                            
                            <div id="uploaded_items" >
                                <div id="div_listing_container" class="listing_container" style="display:block">	            
										<?php
                                            if (trim($img_url) != "") {
                                        ?>
                                                    <div id='<?=$tbl_uploads_id?>' class='box-header with-border'>
                                                      <div class='box-title'><img src='<?=$img_url?>' /></div>
                                                      <div class='box-tools'> <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick="confirm_delete_img_popup('<?=$tbl_uploads_id?>')">
                                                        </button>
                                                      </div>
                                                    </div>
											<style>
												.ajax-upload-dragdrop {
													display:none;	
												}
                                            </style>        
                                        <?php		
                                            }
                                        ?>
                                </div>        
                            </div>
                        <!--File Upload END-->
                      </div>
                   
                    
                    </div>
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate()">Continue</button>
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
               
                </div>
      
      
               <div id="tabs-2">
                <!-- /.box-header -->
                <!-- form start -->
                  <div class="box-body">
                  
                    <p  style="padding-left:10px;"><strong>Parent Details</strong></p>
                    <br />
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="first_name_parent">Parent First Name [En]</label>
                      <div class="col-sm-10">
                        <input type="text" placeholder="Parent First Name[En]" id="first_name_parent" name="first_name_parent" class="form-control">
                      </div>
                      
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="first_name_parent_ar">Parent First Name [Ar]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Parent First Name[Ar]" id="first_name_parent_ar" name="first_name_parent_ar" class="form-control" dir="rtl">
                      </div>
                    </div>
                    
                      <div class="form-group">
                      <label class="col-sm-2 control-label" for="last_name_parent">Parent Last Name [En]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Parent Last Name[En]" id="last_name_parent" name="last_name_parent" class="form-control">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="last_name_parent_ar">Parent Last Name [Ar]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Parent Last Name[Ar]" id="last_name_parent_ar" name="last_name_parent_ar" class="form-control" dir="rtl">
                      </div>
                    </div>
                    
                 
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="dob_month_parent"> DOB</label>
    
                      <div class="col-sm-2">
                        <select name="dob_month_parent" id="dob_month_parent"  class="form-control" tabindex="6">
                                      	<option value="">--Month--</option>
                                      	<?php for ($m=1; $m<=12; $m++) { ?>
                                      	<option value="<?=$m?>" <?php if ($dob_month_parent == $m) {echo "selected";}?> ><?=$m?></option>
                                        <?php } ?>
                        </select>
                     </div>
                     <div class="col-sm-2">
                                      <?php if (!isset($dob_day_parent) || trim($dob_day_parent) == "") {$dob_day = '';}?>
                                      <select name="dob_day_parent" id="dob_day_parent" tabindex="7" class="form-control">
                                        <option value="">--Day--</option>
                                      	<?php for ($d=1; $d<=31; $d++) { ?>
                                      	<option value="<?=$d?>" <?php if ($dob_day_parent == $d) {echo "selected";}?> ><?=$d?></option>
                                        <?php } ?>
                    </select>
                    </div>
                    <div class="col-sm-2">
                                      <?php if (!isset($dob_year_parent) || trim($dob_year_parent) == "") {$dob_year = '';}?>
                                      <select name="dob_year_parent" id="dob_year_parent" tabindex="8" class="form-control">
                                        <option value="">--Year--</option>

                                      	<?php for ($y=1950; $y<=date('Y'); $y++) { ?>
                                      	<option value="<?=$y?>" <?php if ($dob_year_parent == $y) {echo "selected";}?> ><?=$y?></option>
                                        <?php } ?>
                                      </select>              
                    </div>  
                    </div>
                    
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="gender_parent">Relationship</label>
                       <div class="col-sm-10">
                        <label>
                          <input type="radio" id="gender_parent" name="gender_parent" value="male" class="minimal" checked="checked"  >
                          Father
                        </label>
                        &nbsp;
                        <label>
                          <input type="radio" id="gender_parent" name="gender_parent" value="female" class="minimal"  >
                          Mother
                        </label>
                         &nbsp;
                        <label>
                          <input type="radio" id="gender" name="gender" value="other" class="minimal"  >
                          Other
                        </label>
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="mobile_parent">Parent's Mobile</label>
    
                      <div class="col-sm-10">
                       <span style="position:absolute; padding-left:20px; padding-top:5px;"> +971</span> <input type="text" placeholder="Parent's Mobile" id="mobile_parent" name="mobile_parent" class="form-control" style="padding-left:60px;">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="email_parent">Paren'st Email</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Parent's Email" id="email_parent" name="email_parent" class="form-control">
                      </div>
                    </div>
                    
                  
                    
                    
                      <div class="form-group">
                      <label class="col-sm-2 control-label" for="emirates_id_parent">Parent Emirates Id </label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Parent Emirates Id" id="emirates_id_parent" name="emirates_id_parent" class="form-control">
                      </div>
                    </div>
                    
                   
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="parent_user_id">User Id</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="User Id " id="parent_user_id" name="parent_user_id" class="form-control">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="password">Password</label>
    
                      <div class="col-sm-10">
                        <input type="password" placeholder="Password" id="password" name="password" class="form-control">
                      </div>
                    </div>
                    
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="confirm_password">Confirm Password </label>
    
                      <div class="col-sm-10">
                        <input type="password" placeholder="Confirm Password" id="confirm_password" name="confirm_password" class="form-control">
                      </div>
                    </div>
                    
                    </div>
                    
                    <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate_parent()">Submit</button>
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->  
                  
               </div>
                
               
            </div>
       </form>
      </div>
                
  
            <!--/Add or Create-->
                
        <!--/Admin Category Management-->

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>

        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >
function search_data() {
		var tbl_class_id = $("#tbl_class_id").val();
		var q = $.trim($("#q").val());
		var status_is_checked = $("input[name='is_approved']").is(":checked");

		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/parents/all_parents/";

		if(tbl_class_id !='') {
			url += "tbl_class_id/"+tbl_class_id+"/";
		}

		if (status_is_checked) {
			var checked_val = $("input[name='is_approved']:checked").val();	
			url += "is_approved/"+checked_val+"/";
		}
		
		if(q !='') {
			url += "q/"+q+"/";
		}
		
		url += "offset/0/";
		window.location.href = url;
		<?php /*?>window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/enquiry/all_enquiries/is_not_replied/"+is_not_replied+"/tbl_court_id/"+tbl_court_id+"/tbl_category_id/"+tbl_category_id;<?php */?>
	}
function generate_parent_id(offset) {
		var tbl_class_id = $("#tbl_class_id").val();
		var q = $("#q").val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/parents/generate_parent_id/";
		if(tbl_class_id !='')
			url += "tbl_class_id/"+tbl_class_id+"/";
		if(q !='')
			url += "q/"+q+"/";
		
			url += "offset/"+offset+"/";
		window.open(url);
	}

function reset_data() {
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/parents/all_parents/";
		url += "offset/0/";
		window.location.href = url;
	}
</script>