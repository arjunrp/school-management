<?php ob_start(); ?>
<!DOCTYPE html>
<html>
<!--<html>
<link rel="stylesheet" href="<?=ADMIN_CSS_PATH?>print_id_card.css" type="text/css">
<link rel="stylesheet" href="<?=ADMIN_CSS_PATH?>id_card_style.css" type="text/css">-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" id="printarea" >
<style>
  html, body {
    padding:0;
    margin:0;
}
  body{
		font-family:Arial, Helvetica, sans-serif;
		font-size:11px;
		color:#000;		}
	table{
		border:none;
		}
	table th{
		font-size:38px;
		color: #F63;	
		line-height:20px;
		}
	table td{
		font-size:28px;
		color:#000;	
		line-height:25px;
		}

</style>

<!-- Save for Web Slices (Untitled-1 - Slices: 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, 14, 15, 16) -->
<table id="Table_01" width="2481" height="1151" border="0" cellpadding="0" cellspacing="0" dir="ltr" style="margin-bottom:50px; ">
  <tr>
    <td colspan="5" width="2481" height="200" style="text-align:center; color:#933; margin-top:30px; font-size:32px; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;"><h1>Cards Reports</h1></td>
  </tr>
  <?php
  
  if($tbl_sel_student_id <>""){
  		$student_name_en   = $rs_all_card_details[0]['first_name']." ".$rs_all_card_details[0]['last_name'];
  		$student_name_ar   = $rs_all_card_details[0]['first_name_ar']." ".$rs_all_card_details[0]['last_name_ar'];
  		$class_name_en     = $rs_all_card_details[0]['class_name']." ".$rs_all_card_details[0]['section_name'];
  		$class_name_ar     = $rs_all_card_details[0]['class_name_ar']." ".$rs_all_card_details[0]['section_name_ar']; 
  
  ?>
   
    <tr>
    <td  width="2400" height="100" style="text-align:center; color:#933; margin-top:30px; font-size:42px; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;">
    <table id="Table_01" width="2481" height="1151" cellpadding="0" cellspacing="0" dir="ltr" style="margin-bottom:50px; ">
       <tr>
        <td  width="200" height="100" style="font-size:42px; font-weight:bold;">Name:</td>
        <td  width="890" height="100" style="font-size:42px; font-weight:bold;color: #F63; text-align:left;"><?=$student_name_en?></td>
        <td  width="342" height="100" style="font-size:42px; font-weight:bold;"></td>
        <td  width="750" height="100" style="font-size:42px; font-weight:bold;color: #F63; text-align:right;"><?=$student_name_ar?></td>
        <td  width="218" height="100" style="font-size:42px; font-weight:bold; ">:الاسم</td>
      </tr>
       <tr>
        <td  width="200" height="100" style="font-size:42px; font-weight:bold;  ">Class:</td>
        <td  width="890" height="100" style="font-size:42px; font-weight:bold;color: #F63; text-align:left;"><?=$class_name_en?></td>
        <td  width="342" height="100" style="font-size:42px; font-weight:bold;"></td>
        <td  width="750" height="100" style="font-size:42px; font-weight:bold;color: #F63; text-align:right;"><?=$class_name_ar?></td>
        <td  width="218" height="100" style="font-size:42px; font-weight:bold;">:الصف</td>
      </tr>
    </table>
    </td>
    </tr>
   
  
  <?php } ?>
  
  
   <tr>
    <td  width="2400" height="100" style="text-align:center; color:#933; margin-top:30px; font-size:32px; font-family:'Trebuchet MS', Arial, Helvetica, sans-serif;">
    <table id="Table_01" width="2481" height="1151" border="1" cellpadding="0" cellspacing="0" dir="ltr" style="margin-bottom:50px; ">
       <tr>
        <?php  if($tbl_sel_student_id <>""){ ?>
        <td  width="890" height="100" style="font-size:32px; font-weight:bold;">Cards</td>
        <?php }else{ ?>
        <td  width="445" height="100" style="font-size:32px; font-weight:bold;">Cards</td>
        <td  width="445" height="100" style="font-size:32px; font-weight:bold;">Issued To</td>
       <?php  }  ?>
        <td  width="200" height="100" style="font-size:32px; font-weight:bold;">Point</td>
        <td  width="342" height="100" style="font-size:32px; font-weight:bold;">Type</td>
        <td  width="750" height="100" style="font-size:32px; font-weight:bold;">Teacher</td>
        <td  width="218" height="100" style="font-size:32px; font-weight:bold;">Date</td>
      </tr>
     
      <?php
      for ($i=0; $i<count($rs_all_card_details); $i++) { 

		   $student_name_en   = $rs_all_card_details[$i]['first_name']." ".$rs_all_card_details[$i]['last_name'];
		   $student_name_ar   = $rs_all_card_details[$i]['first_name_ar']." ".$rs_all_card_details[$i]['last_name_ar'];
		   $class_name_en     = $rs_all_card_details[$i]['class_name']." ".$rs_all_card_details[$i]['section_name'];
		   $class_name_ar     = $rs_all_card_details[$i]['class_name_ar']." ".$rs_all_card_details[$i]['section_name_ar'];
		   $category_name_en  = $rs_all_card_details[$i]['category_name_en'];
		   $category_name_ar  = $rs_all_card_details[$i]['category_name_ar'];
		   $card_point        = $rs_all_card_details[$i]['card_point'];
		   $card_issue_type   = $rs_all_card_details[$i]['card_issue_type'];
		   $teacher_name_en   = $rs_all_card_details[$i]['teacher_first_name']." ".$rs_all_card_details[$i]['teacher_last_name'];
		   $teacher_name_ar   = $rs_all_card_details[$i]['teacher_first_name_ar']." ".$rs_all_card_details[$i]['teacher_last_name_ar'];
		   $added_date        = $rs_all_card_details[$i]['added_date'];
		   $semester_en       = $rs_all_card_details[$i]['title'];
		   $semester_ar       = $rs_all_card_details[$i]['title_ar'];
         
		 ?>  
      
      
       <tr>
        <?php  if($tbl_sel_student_id <>""){ ?>
        <td  width="890" height="200"><?=$category_name_en?>&nbsp;/&nbsp;<?=$category_name_ar?></td>
        <?php }else{ ?>
        <td  width="445" height="200" ><?=$category_name_en?>&nbsp;/&nbsp;<?=$category_name_ar?></td>
        <td  width="445" height="200" ><p><?=$student_name_en?>[::]<?=$student_name_ar?></p><span style="color:#C63;"><?=$class_name_en?>[::]<?=$class_name_ar?></span></td>
       <?php  }  ?>
       
        <td  width="200" height="200"><?=$card_point?></td>
        <td  width="342" height="200"><?=$card_issue_type?></td>
        <td  width="750" height="200"><?=$teacher_name_en?>&nbsp;/&nbsp;<?=$teacher_name_ar?></td>
        <td  width="218" height="200"><?=$added_date?></td>
      </tr>
    <?php } ?>
      
    </table>
    </td>
  </tr>
  
</table>
<!-- End Save for Web Slices -->
</body>
</html>
<?php
$output = ob_get_clean( );
$output .= "<div style='page-break-after:always'>" . $output . "</div>";
$output = htmlentities($output);
?>
<form method="post" name="frmsubmit" id="frmsubmit" action="<?=HOST_URL?>/id_cards.php">
  <input type="hidden" name="html" value="<?=$output?>"/>
</form>
<script>
  document.frmsubmit.submit();
</script>