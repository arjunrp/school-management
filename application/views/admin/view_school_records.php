<?php
//Init Parameters
$parenting_school_id_enc = md5(uniqid(rand()));
$allowed_files_display = "File: *.mp4, *.jpg, *.jpeg, *.png, *.pdf";
if (trim($mid) == "") {
	$mid = "1";	
}


?>
 
<style>
.txt_en {
	text-align:left;
	padding-left:2px;
}
.txt_ar {
	text-align:right;
	padding-right:2px;	
	direction:rtl;		
}
textarea {
    height: 170px;
    padding-bottom: 6px;
    padding-top: 6px;
    width: 95%;
	font-size: 14px;
	border: 1px solid #ddd;
 }
 select {
   font-size: 14px;
   border: 1px solid #ddd;
 }
</style>
<?php /*?><script type="text/javascript" src="<?=JS_PATH?>/nice_edit/nicEdit.php"></script>
<script language="javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script><?php */?>

<script language="javascript">
	
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/school_records";
			}
		})
	});
	
function confirm_delete_popup() {
		var len = $("input[id='tbl_parenting_school_id']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more record(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/deleteSchoolRecord",
			data: {
				tbl_parenting_school_id: $("input[id='tbl_parenting_school_id']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Record(s) deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}	
	function ajax_activate(tbl_parenting_school_id) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/activateSchoolRecord",
			data: {
				tbl_parenting_school_id: tbl_parenting_school_id,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Record activated successfully.", 'green')

				$('#act_deact_'+tbl_parenting_school_id).html('<span style="cursor:pointer" onClick="ajax_deactivate(\''+tbl_parenting_school_id+'\')" onMouseOver="deactivate_me(this)" onMouseOut="reset_activate(this)" class="label label-success">Active</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_deactivate(tbl_parenting_school_id) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/deactivateSchoolRecord",
			data: {
				tbl_parenting_school_id: tbl_parenting_school_id,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Record de-activated successfully.", 'green')
				
				$('#act_deact_'+tbl_parenting_school_id).html('<span style="cursor:pointer" onClick="ajax_activate(\''+tbl_parenting_school_id+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	
	function ajax_add_record() {
		
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/add_school_record",
			data: {
				tbl_parenting_school_id : '<?=$parenting_school_id_enc?>',
				tbl_parenting_school_cat_id : $('#tbl_parenting_school_cat_id').val(),
				category_en: $('#category_en').val(),
				category_ar: $('#category_ar').val(),
				parenting_title_en: $('#parenting_title_en').val(),
				parenting_title_ar: $('#parenting_title_ar').val(),
				parenting_type    : $("#typeDiv input[type='radio']:checked").val(),
				parenting_text_en : $('#parenting_text_en').val(),
				parenting_text_ar : $('#parenting_text_ar').val(),
				parenting_url     : $('#url').val(),
				is_active         : $("#activeDiv input[type='radio']:checked").val(),
				tbl_student_id    : $("input[id='student_id_enc']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='N') {
					refresh_page = "N";
					my_alert("Record added failed, Please try again.", 'red');
					$("#pre-loader").hide();
				   
				}else{
					 refresh_page = "Y";
				    my_alert("Record added successfully.", 'green');
				    $("#pre-loader").hide();
				}
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_update_record() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/update_school_record",
			data: {
				tbl_parenting_school_id 	: $('#tbl_parenting_school_id').val(),
				tbl_parenting_school_cat_id : $('#tbl_parenting_school_cat_id').val(),
				category_en			: $('#category_en').val(),
				category_ar			: $('#category_ar').val(),
				parenting_title_en	: $('#parenting_title_en').val(),
				parenting_title_ar	: $('#parenting_title_ar').val(),
				parenting_type    	: $("#typeDiv input[type='radio']:checked").val(),
				parenting_text_en 	: $('#parenting_text_en').val(),
				parenting_text_ar 	: $('#parenting_text_ar').val(),
				parenting_url     	: $('#url').val(),
				is_active         	: $("#activeDiv input[type='radio']:checked").val(),
				is_ajax				: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='N') {
					refresh_page = "N";
					my_alert("Record updation failed, Please try again.", 'red');
					$("#pre-loader").hide();
				   
				}else{
					 refresh_page = "Y";
				    my_alert("Record updated successfully.", 'green');
				    $("#pre-loader").hide();
				}
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
</script>
<script language="javascript">
   //add student
   /* || validate_picture() == false*/
	function ajax_validate() {
		if (validate_category() == false || validate_title() == false ||  validate_parenting_type() == false || validate_record() == false ) 
		{
			return false;
		}
		else{
		 	var tbl_parenting_school_cat_id = $('#tbl_parenting_school_cat_id').val();
			if(tbl_parenting_school_cat_id!='')
			{
				ajax_add_record();
			}else{
				var category_en =  $('#category_en').val();
				var category_ar =  $('#category_ar').val();
            	if(category_en !='' || category_ar !='')
				{
					is_exist_record_category();
					
				}
			}
			
		}
	}
	
	function is_exist_record_category() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/is_exist_records_category",
			data: {
				parenting_school_cat_id_enc: "<?=$parenting_school_cat_id_enc?>",
				title_en: $('#category_en').val(),
				title_ar: "",
				is_ajax: true
			},
			success: function(data) {
				
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
					refresh_page = "N";
					my_alert("Records category already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_add_record();
				}
			
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist_edit_record_category() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/is_exist_records_category",
			data: {
				parenting_school_cat_id_enc: "<?=$parenting_school_cat_id_enc?>",
				title_en: $('#category_en').val(),
				title_ar: "",
				is_ajax: true
			},
			success: function(data) {
				
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
					refresh_page = "N";
					my_alert("Records category already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_update_record();
				}
			
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	
    //edit student
	function ajax_validate_edit() {
		if (validate_category() == false || validate_title() == false ||  validate_parenting_type() == false ||  validate_record() == false ) 
		{
			return false;
		} 
		else{
			
			var tbl_parenting_school_cat_id = $('#tbl_parenting_school_cat_id').val();
			if(tbl_parenting_school_cat_id!='')
			{
				ajax_update_record();
			}else{
				var category_en =  $('#category_en').val();
				var category_ar =  $('#category_ar').val();
            	if(category_en !='' || category_ar !='')
				{
					is_exist_record_category();
					
				}
			}
			
			
			
		}
	} 
	
  /************************************* START MESSAGE VALIDATION *******************************/

   function validate_category() {
		var strCatEn = $("#category_en").val();
		var strCatAr = $("#category_ar").val();
		if(strCatEn == "" && strCatAr =="")
		{
			var regExp = / /g;
			var str = $("#tbl_parenting_school_cat_id").val();
			if (str.length <= 0) {
				my_alert("Records category is not selected. Please select records category")
				$("#tbl_parenting_school_cat_id").focus();
			return false;
			}
		}
		return true;
	
	}
	
	function validate_title() {
	    var regExp = / /g;
		var str = $('#parenting_title_en').val();
		if (str.length <= 0) {
			my_alert("Please select title [En]");
			return false;
		}
		
		var regExp = / /g;
		var strAr = $('#parenting_title_ar').val();
		if (strAr.length <= 0) {
			my_alert("Please select title [Ar]");
			return false;
		}
	  return true;
	}
	
	
	 function validate_parenting_type() {
		var regExp = / /g;
		var str = $("#typeDiv input[type='radio']:checked"); // $("#parenting_type").val();
		if (str.length <=0 ) {
			my_alert("Record type is not selected. Please select record type")
			$("#parenting_type").focus();
		return false;
		}
		return true;
	}
	
	
	function validate_record() {
		var strType = $("#typeDiv input[type='radio']:checked").val();
		if(strType=="d")
		{
			var regExp = / /g;
			var str = $('#parenting_text_en').val();
			if (str.length <= 0) {
				my_alert("Please enter parenting text [En]");
				return false;
			}
			
			var regExp = / /g;
			var strAr = $('#parenting_text_ar').val();
			if (strAr.length <= 0) {
				my_alert("Please enter parenting text [Ar]");
				return false;
			}
		}else if(strType=="u")
		{
			var regExp = / /g;
			var strAr = $('#url').val();
			if (strAr.length <= 0) {
				my_alert("Please add url");
				return false;
			}
			
		}
	  return true;
	}
	
	
</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Records <small> Management</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Records From Schools</li>
    </ol>
    <!--/BREADCRUMB--> 
    <div class="box-tools" style="float:right;">
        <a onclick="show_categories()" ><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
    </div>
    <div style="clear:both"></div>
  </section>
      <link href="<?=HOST_URL?>/assets/admin/dist/css/jquery-ui.css" rel="stylesheet">
      <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-1.11.1.js"></script>
      <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-ui.js"></script>
      <link href="<?=HOST_URL?>/assets/admin/dist/css/uploadfile.min.css" rel="stylesheet">
                      

  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {
	?>
        <!--Edit-->
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Record</h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/school_records"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

            
     <style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url('<?=IMG_PATH?>/delete.jpg');
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
		clear:both;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
	#overlay_container {
		position:relative;
	}
	#overloading {
		background-image:url('<?=IMG_PATH?>/preloader/preloader_2.gif');
		background-repeat:no-repeat;
		background-position:center;
		background-color:#CCC;
		position:absolute;
		left:0px;
		top:0px;
		opacity: 0.3;
		z-index: 10000;
	}
	#div_listing_container {
		display:none;	
	}
	.d_d_text {
		color:#745156;
		font-size:20px;
			
	}
	.ajax-upload-dragdrop {
		margin:auto;
		margin-bottom:10px;
		width:700px !important;
	}
	.ajax-file-upload-statusbar {
		margin:auto;
		margin-top:10px;
	}
	.ajax-file-upload {
		height:31px;
	}
	
	
	 #tabs-1{  
	    overflow-y:scroll; overflow-x:none;
	}

    #tabs-2{
		overflow-y:scroll; overflow-x:none;
	}
				  
  .ui-tabs-active{
		border-color:#efca86  !important;
   }
					 
	.ui-tabs .ui-tabs-nav li {
		float:left;
		font-size: 16px;
        font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif;
  }
  label{
	  display: inline-block;
      font-weight: 700;
  }
  
  .ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button {
    font-family:"Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
    font-size: 14px;
}
  
  .ui-widget{
	 font-size: 16px;
     font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
  }
  .form-control{
	 font-size: 14px; 
  }
</style>         
         <?php
		 	$tbl_parenting_school_id             = $school_record[0]['tbl_parenting_school_id'];		
			$tbl_parenting_school_cat_id         = $school_record[0]['tbl_parenting_school_cat_id'];
			$parenting_title_en                  = $school_record[0]['parenting_title_en'];		
			$parenting_title_ar                  = $school_record[0]['parenting_title_ar'];
			$parenting_type                      = $school_record[0]['parenting_type'];		
			$parenting_logo                      = $school_record[0]['parenting_logo'];
			$parenting_text_en                   = $school_record[0]['parenting_text_en'];	
			$parenting_text_ar                   = $school_record[0]['parenting_text_ar'];	
			$parenting_url                       = $school_record[0]['parenting_url'];
			$is_active                           = $school_record[0]['is_active'];
			$file                                = $school_record[0]['file_name_updated'];
			$tbl_uploads_id                      = $school_record[0]['tbl_uploads_id'];
			if($parenting_logo<>""){
			  $img_url                           = IMG_UPLOAD_PATH."/".$parenting_logo;
			}
			if($file<>""){
			  $file_url                          = IMG_UPLOAD_PATH."/".$file;
			}
			
	
		 ?>   
         <div class="box-body">
                    <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
    
                        
                     <div class="form-group">
                     <label class="col-sm-2 control-label" for="tbl_parenting_school_cat_id">Records Category<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-4">
                                 <select name="tbl_parenting_school_cat_id" id="tbl_parenting_school_cat_id" class="form-control"  >
                                 <option value="">Select Category</option>
                              <?php
                                    for ($u=0; $u<count($category_list); $u++) { 
                                        $tbl_parenting_school_cat_id_u   = $category_list[$u]['tbl_parenting_school_cat_id'];
                                        $title_en         = $category_list[$u]['title_en'];
                                        $title_ar         = $category_list[$u]['title_ar'];
                                        if($tbl_parenting_school_cat_id == $tbl_parenting_school_cat_id_u)
                                           $selType = "selected";
                                         else
                                           $selType = "";
                                  ?>
                                      <option value="<?=$tbl_parenting_school_cat_id_u?>"  <?=$selType?> >
                                      <?=$title_en?>&nbsp;[::]&nbsp;
                                    <?=$title_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>
                    </div>
                     <label class="col-sm-1 control-label" for="tbl_parenting_school_cat_id" style="font-size:12px; text-decoration:underline; cursor:pointer; color:#030;" ><span style="padding-left:2px;" onclick="addNewCategory();">+&nbsp;Add New Category</span></label>
                    
                    </div>
                    
                    <div class="form-group" id="divCategory" style="display:none;" >
                      <label class="col-sm-2 control-label" for="category_en">Category [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-4">
                        <input type="text" placeholder="Category [En]" id="category_en" name="category_en" class="form-control"  value="<?=$category_en?>" >
                      </div> 
                      
                      <label class="col-sm-1 control-label" for="category_ar">Category [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-4">
                        <input type="text" placeholder="Category [Ar]" id="category_ar" name="category_ar" class="form-control" dir="rtl" value="<?=$category_ar?>" >
                      </div>
                      
                    </div>
                    
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="parenting_title_en">Title [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-4">
                        <input type="text" placeholder="Title [En]" id="parenting_title_en" name="parenting_title_en" class="form-control"  value="<?=$parenting_title_en?>" >
                      </div> 
                      
                      <label class="col-sm-1 control-label" for="parenting_title_ar">Title [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-4">
                        <input type="text" placeholder="Title [Ar]" id="parenting_title_ar" name="parenting_title_ar" class="form-control" dir="rtl" value="<?=$parenting_title_ar?>" >
                      </div>
                      
                    </div>
                  
                  
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="thumb_image">Thumb Image</label>
    
                      <div class="col-sm-4" >
                      
                       
                        <!--File Upload START-->
                           <div id="divThumb"  <?php if (trim($img_url) <> "") {?> style="display:none;" <?php } ?> >
                            <style>
                            #advancedNewUpload {
                                padding-bottom:0px;
                            }
                            </style>
                                 
                               <div id="advancedNewUpload">Upload File</div>
                            
                            </div>
                            <div id="uploaded_items" >
                                <div id="div_listing_container_new" class="listing_container" style="display:block">	            
										<?php
                                            if (trim($img_url) != "") {
                                        ?>
                                                    <div id='<?=$id?>' class='box-header'>
                                                      <div class='box-title'><img src='<?=$img_url?>' /></div>
                                                      <div class='box-tools'> <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick="confirm_delete_img()">
                                                        </button>
                                                      </div>
                                                    </div>
											<style>
												.ajax-upload-dragdrop-new {
													display:block;	
												}
                                            </style>        
                                        <?php		
                                            }
                                        ?>
                                </div>        
                            </div>
                        <!--File Upload END-->
                      </div>
                   
                    
                    </div>
                    
                    
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="parenting_type">Record Type</label>
    
                      <div class="col-sm-10" id="typeDiv" >
                         <input type="radio" name="parenting_type" value="d" onClick="show_text()" <?php if($parenting_type=='d') { ?> checked="checked" <?php } ?> > Text&nbsp;&nbsp;&nbsp;&nbsp;
                         <input type="radio"  name="parenting_type" value="u" onClick="show_url()" <?php if($parenting_type=='u') { ?> checked="checked" <?php } ?> > URL&nbsp;&nbsp;&nbsp;&nbsp;
                         <input name="parenting_type" type="radio" value="v" onClick="show_video()" <?php if($parenting_type=='v') { ?> checked="checked" <?php } ?> > <?=$allowed_files_display?>
                      </div>
                    </div>
                          
                 
                <div id="parenting_text" <?php if($parenting_type<>'d') { ?> style="display:none;" <?php } ?> >
                       <div class="form-group" >
                       <label class="col-sm-2 control-label" for="parenting_text_en">Parenting Text [En]</label>
    
                          <div class="col-sm-4">
                            <textarea name="parenting_text_en" id="parenting_text_en" cols="80" rows="10" class="form-control" ><?=$parenting_text_en?></textarea>
                          </div> 
                          
                          <label class="col-sm-2 control-label" for="parenting_text_ar">Parenting Text [Ar]</label>
    
                          <div class="col-sm-4">
                            <textarea name="parenting_text_ar" id="parenting_text_ar" cols="80" rows="10" class="form-control" dir="rtl"><?=$parenting_text_ar?></textarea>
                          </div>
                        </div>
                      
                   
                 
                 </div>
                <div id="parenting_video"  <?php if($parenting_type<>'v') { ?> style="display:none;" <?php } ?> >
                 
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="thumb_image">Record File</label>
    
                      <div class="col-sm-4" >
                         <div id="divFile">
                        <!--File Upload START-->
                            <style>
                            #advancedFileUpload {
                                padding-bottom:0px;
                            }
                            </style>
                                 
                            <div id="advancedFileUpload">Upload File</div>
                        </div>
                            
                            <div id="uploaded_items_file" >
                                <div id="div_listing_container" class="listing_container" style="display:block">	            
										<?php
                                            if (trim($file_url) != "") {
												
												$file_type = explode(".",$file);
												
												
                                        ?>
                                                    <div id='<?=$tbl_uploads_id?>' class='box-header'>
                                                      <div class='box-title'>
                                                      <?php if($file_type=="png" || $file_type=="jpg") { ?>
                                                               <img src='<?=$file_url?>' />
                                                      <?php }else{ ?>
                                                      <a href='<?=$file_url?>' target='_blank' ><?=$file?></a>
                                                      <?php } ?>
                                                      
                                                      </div>
                                                      <div class='box-tools'> <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick="confirm_delete_file('<?=$tbl_uploads_id?>')">
                                                        </button>
                                                      </div>
                                                    </div>
											<style>
												.ajax-upload-dragdrop {
													display:none;	
												}
                                            </style>        
                                        <?php		
                                            }
                                        ?>
                                </div>        
                            </div>
                        <!--File Upload END-->
                      </div>
                   
                    
                    </div>
                 </div>  
                          
                          
                 <div id="parenting_url" <?php if($parenting_type<>'u') { ?> style="display:none;" <?php } ?> >
                        <div class="form-group">
                         <label class="col-sm-2 control-label" for="parenting_url">URL</label>
    
                          <div class="col-sm-4">
                           <input type="text" name="url" id="url" value="<?=$parenting_url?>" size="50" class="form-control" maxlength="50">
                          </div>
                        </div>
                 </div>
                 
                 <div class="form-group">
                         <label class="col-sm-2 control-label" for="parenting_status">Status</label>
    
                          <div class="col-sm-10" id="activeDiv">
                           <input name="is_active" type="radio" value="Y" <?php if($is_active=="Y"){?> checked="checked" <?php } ?> > Yes
                           <input type="radio" name="is_active" value="N" <?php if($is_active=="N"){?> checked="checked" <?php } ?> > No
                          </div>
                  </div>
              
                          
                     
                        <!-- /.box-body -->
                      <div class="box-footer">
                        <button class="btn btn-primary" type="button" onclick="ajax_validate_edit()">Submit</button>
                        <input type="hidden" name="tbl_parenting_school_id" id="tbl_parenting_school_id" value="<?=$tbl_parenting_school_id?>" />
                        <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                      </div>
                      <!-- /.box-footer -->  
                 <script>
                      var item_id = $("#tbl_parenting_school_id").val();
					  
					 function addNewCategory()
					 {
						// divCategory
						if($('#divCategory').is(':visible'))
						{
							$("#divCategory").hide();
						}else{
							$("#divCategory").show();
						}
						 
					 }
                 </script>    
            
           </form>
                </div>    
                
    </div>
		        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/records/school_records";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
  
 <link href="<?=HOST_URL?>/assets/admin/dist/css/jquery-ui.css" rel="stylesheet">
 <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-1.11.1.js"></script>
 <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-ui.js"></script>
  <script>
  $( function() {
		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
	
			var order = $("#tabledivbody").sortable("serialize");
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	
				
			}
	  } );
  
  } );
  </script> 
  
 <link href="http://hayageek.github.io/jQuery-Upload-File/uploadfile.min.css" rel="stylesheet">

<style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url(<?=IMG_PATH?>/delete.jpg);
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
</style>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="http://hayageek.github.io/jQuery-Upload-File/jquery.uploadfile.min.js"></script>
<script type="text/javascript" src="<?=JS_PATH?>/jquery.timer.js"></script>
<script>

$(document).ready(function()
{
	var tbl_item_id = '<?=$parenting_school_id_enc?>';
	$("#advancedUpload").uploadFile({
		allowedTypes: "mp4,jpg,jpeg,png,pdf",
		maxFileCount:1,
		url:"<?=HOST_URL?>/admin/upload.php",
		fileName:"myfile",
		formData: {"module_name":"parenting","tbl_item_id":tbl_item_id},
		onSubmit:function(files)
		{
			//if ($("#upload_content").val().trim() != "") {alert("The file is already uploaded."); return false;}
			//$("#eventsmessage").html($("#eventsmessage").html()+"<br/>Submitting:"+JSON.stringify(files));
		},
		onSuccess:function(files,data,xhr)
		{
			if (data == "error") {
				alert("Error uploading file. Please try again.");
				return;
			}
			add_uploaded_item(files, data);
		}
	});
});

// tbl_item_id = tbl_uploads_id
		/*function add_uploaded_item(item_name, tbl_item_id) {
			//alert(item_name+"-"+ tbl_item_id);
			var tbl_item_id = "tmp001"+Math.random();
			tbl_item_id = tbl_item_id.replace(".","_");
			//alert(tbl_item_id)
			var row_item = document.createElement('div');
			$(row_item).addClass("row_item")
				.attr("id",tbl_item_id)
				.appendTo($("#uploaded_items"))
				
			var upload_del = document.createElement('div');
			$(upload_del).addClass("upload_del")
				.appendTo($("#"+tbl_item_id)) //main div
				.click(function(){
					delete_file(tbl_item_id);
				})
		
			var upload_content = document.createElement('div');
			$(upload_content).addClass("upload_content")
				.html(item_name)
				.appendTo($("#"+tbl_item_id)) //main div
			//$("#parenting_video").css("display","none");
			$("#vid_text").css("display","block");
		return;
		}
*/
function delete_file(tbl_uploads_id) {
	if (confirm("Are you sure you want to delete?")) {
		delete_file_ajax(tbl_uploads_id)
	}
	return;
}



var connectivity_msg = "Connection timed out. Please try again.";
var connectivity_timeout_time = 10000;
var host = '<?=HOST_URL?>';
/* Function to load a navigation page */
function delete_file_ajax(tbl_uploads_id) {
	//show_loading();
	if ($("#parenting_type").val() == "d") {
		alert("Please choose Type to be 'File' in order to proceed.");
		return;
	}

	var xmlHttp, rnd, url, search_param, ajax_timer;
	rnd = Math.floor(Math.random()*11);
	try{		
		xmlHttp = new XMLHttpRequest(); 
	}catch(e) {
		try{
			xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		}catch(e) {
			xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
			hide_loading();
		}
	}

	//AJAX response
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
			ajax_timer.stop();
			var d = document.getElementById("uploaded_items");
			var d_nested = document.getElementById(tbl_uploads_id);
			var throwawayNode = d.removeChild(d_nested);
			if ($("#parenting_type").html().trim() == "v") {
				$("#parenting_video").css("display","block");
				$("#vid_text").css("display","none");
			}
			alert("The file have been deleted successfully.");
		}
	}

	ajax_timer = $.timer(function() {
		xmlHttp.abort();
		alert(connectivity_msg);
		ajax_timer.stop();
	},connectivity_timeout_time,true);

	//Sending AJAX request
	url = host + "/admin/delete_upload.php?tbl_uploads_id="+tbl_uploads_id+"&rnd="+rnd;
	//alert(url);
	xmlHttp.open("POST",url,true);
	xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlHttp.send("rnd="+rnd);
}

function show_video() {
		$("#parenting_video").slideDown();
		$("#parenting_text").slideUp();
		$("#parenting_url").slideUp();

		/*var uploaded_items = $("#uploaded_items").html().trim();
		if (uploaded_items != "") {
			$("#parenting_video").hide();
		}*/
		$("#parenting_type").html("v");
	}

	function show_text() {
		$("#parenting_text").slideDown();
		$("#parenting_video").slideUp();
		$("#parenting_url").slideUp();
		$("#parenting_type").html("d");
	}

	function show_url() {
		$("#parenting_text").slideUp();
		$("#parenting_video").slideUp();
		$("#parenting_url").slideDown();
		$("#parenting_type").html("u");
	}

	$(document).ready(function(e) {
		var parenting_type;
		if (document.getElementById("parenting_type") != null) {
			parenting_type = $("#parenting_type").html().trim();
		}

		//alert(parenting_type);
		if (parenting_type == "v" || parenting_type == "V") {
			$("#parenting_url").slideUp();
			$("#parenting_text").slideUp();
			$("#parenting_video").slideDown();
		} else if (parenting_type == "d" || parenting_type == "D") {
			$("#parenting_url").slideUp();
			$("#parenting_text").slideDown();
			$("#parenting_video").slideUp();	
		} else if (parenting_type == "u" || parenting_type == "U") {
			$("#parenting_text").slideUp();
			$("#parenting_video").slideUp();
			$("#parenting_url").slideDown();	
		} else {
			$("#parenting_text").slideUp();
			$("#parenting_video").slideUp();	
			$("#parenting_url").slideUp();	
		}

		if (document.getElementById("uploaded_items") != null) {
			var uploaded_items = $("#uploaded_items").html().trim();
			if (uploaded_items != "") {
				$("#parenting_video").hide();
			}
		}
    });
	
	function show_categories()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/parenting_categories/";
		window.location.href = url;
	}
	
	function show_assign_records()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/list_assign_records/";
		window.location.href = url;
	}
	
</script>
                    <div id="mid1" class="box box-success">
                        <div class="box-header">
                          <div class="col-sm-1" >
                          <h3 class="box-title">SEARCH</h3>
                          </div>
                          <div class="col-sm-11"> 
                             
                               <div class="col-sm-6"><input name="q" id="q" value="<?=urldecode($q)?>" type="text" class="form-control" placeholder="Search By Title, Category etc."   > </div>
                               <div class="col-sm-2"><button class="btn btn-success" type="button" onclick="search_data()">Search</button>&nbsp;<button class="btn btn-success" type="button" 
                               onclick="reset_data();">Reset</button>
                               </div>
                           
                          </div>
                        </div>  
                     </div>     
    
            <!--Listing-->
                    <div id="mid1_list" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Records From School</h3>
                          <div class="box-tools">
                            <?php if (count($rs_all_records)>0) { echo $paging_string;}?>	
                           <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
                            <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>
                          </div>
                        </div>
                        
                        <div class="box-body">
                     <!--   <div style="color:#030; font-weight:bold;">You can sort students by using drag and drop of rows </div>-->
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle"><input id="select_all" type="checkbox" value="" /></th>
                              <!--<th width="10%" align="center" valign="middle">Sl No.</th>-->
                              <th width="25%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y">Title <?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?><div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>
                             <!-- <th width="20%" align="center" valign="middle">Class</th>-->
                             
                              <th width="20%" align="center" valign="middle">Category</th> 
                              <th width="10%" align="center" valign="middle">Type</th>
                              <th width="10%" align="center" valign="middle">Assigned Students</th>
                              <th width="10%" align="center" valign="middle">Date</th>
                              <th width="5%" align="center" valign="middle">Status</th>
                              <th width="5%" align="center" valign="middle">Action</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                                for ($i=0; $i<count($rs_all_records); $i++) { 
                                    $id = $rs_all_messages[$i]['id'];
                                    $tbl_parenting_school_id    = $rs_all_records[$i]['tbl_parenting_school_id'];
                                    $parenting_title_en         = $rs_all_records[$i]['parenting_title_en'];
									$parenting_title_ar         = $rs_all_records[$i]['parenting_title_ar'];
									$parenting_type             = $rs_all_records[$i]['parenting_type'];
									$parenting_text_en          = $rs_all_records[$i]['parenting_text_en'];
									$parenting_text_ar          = $rs_all_records[$i]['parenting_text_ar'];
									$parenting_url              = $rs_all_records[$i]['parenting_url'];
									$parenting_logo             = $rs_all_records[$i]['parenting_logo'];
                                    $added_date                 = $rs_all_records[$i]['added_date'];
                                    $is_active                  = $rs_all_records[$i]['is_active'];
									$category_name_en           = $rs_all_records[$i]['title_en'];
									$category_name_ar           = $rs_all_records[$i]['title_ar'];
									$total_assigned_users       = $rs_all_records[$i]['total_assigned_users'];
									if($parenting_type=="v"){
										$record_type   = "Video / Image";
										$parentingData = $parenting_logo;
									}else if($parenting_type=="d"){
										$record_type   = "Text";
										$parentingData = '<span style="float:left;">'.$parenting_title_en.'</span><span style="float:right;">'.$parenting_title_ar.'</span>';
									} else if($parenting_type=="u"){
										$record_type   = "Web Url";
										$parentingData = $parenting_url;
									}
									
									$tbl_parenting_school_cat_id= $rs_all_records[$i]['tbl_parenting_school_cat_id'];
                                    $added_date = date('m-d-Y',strtotime($added_date));
									
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_parenting_school_id?>" >
                              <td align="left" valign="middle">
                              <span style="float:left;">
                              <input id="tbl_parenting_school_id" name="tbl_parenting_school_id" class="checkbox" type="checkbox" value="<?=$tbl_parenting_school_id?>" />
                              </span>
                              
                             <?php /*?> <span style="float:left;">&nbsp;
                              <?php if($i<>0){ ?> <i class="fa fa-arrow-up"  style="color:#3c8dbc; cursor:pointer;"  aria-hidden="true" title="Sorting - Drag & Drop To Up"></i> &nbsp; <?php } ?>
                               <?php if($i<> count($rs_all_categories)-1){ ?> <i class="fa fa-arrow-down" style="color:#3c8dbc;cursor:pointer;" aria-hidden="true" title="Sorting - Drag & Drop To Down"></i> <?php } ?>
                              </span><?php */?>
                              </td>
                             <!-- <td align="left" valign="middle"><?=$offset+$i+1?></td>-->
                              <td align="left" valign="middle">
                               <span style="float:left;"><a style="cursor:pointer;" onclick="popup_record('<?=$tbl_parenting_school_id?>')" ><?=$parenting_title_en?></a></span><span style="float:right;"><a style="cursor:pointer;" onclick="popup_record('<?=$tbl_parenting_school_id?>')" ><?=$parenting_title_ar?></a></span></td>
                             <?php /*?> <td align="left" valign="middle"> 
                              <div class="txt_en"><?=$class_name?>&nbsp;<?=$section_name?></div>
                              <div class="txt_ar"><?=$class_name_ar?>&nbsp;<?=$section_name_ar?></div></td><?php */?>
                              <td align="left" valign="middle"><span style="float:left;"><?=$category_name_en?></span><span style="float:right;"><?=$category_name_ar?></span></td>
                              <td align="left" valign="middle"><?=$record_type?></td>
                              <td align="left" valign="middle"><a style="text-decoration:underline;" href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/list_assign_records/tbl_parenting_school_id/<?=$tbl_parenting_school_id ?>"><?=$total_assigned_users?></a></td>
                              <td align="left" valign="middle"><?=$added_date?></td>
                              <td align="left" valign="middle">
                               <div id="act_deact_<?=$tbl_parenting_school_id?>">
                                <?php if (trim($is_active) == "Y") { ?>
                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_parenting_school_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                <?php } else { ?>
                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_parenting_school_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                <?php } ?>
                                </div>
                              </td>
                            <td align="left" valign="middle">
                                <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/edit_school_record/tbl_parenting_school_id/<?=$tbl_parenting_school_id ?>"><button class="btn bg-purple fa fa-pencil" type="button" title="Edit"></button></a>
                              </td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="10" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_all_records)<=0) {
                            ?>
                            <tr>
                              <td colspan="10" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no records available. Click on the + button to add records.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
          <div id="myModal" class="modal fade" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document" style="width:80% !important; height:80%;">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close"  data-dismiss="modal"  aria-label="Close" onclick="get_record_popup_close();"><span aria-hidden="true">&times;</span></button>  <!--data-dismiss="modal"-->
                <h4 class="modal-title">Record Details</h4>
              </div>
              <div class="modal-body" id="modal-body" >
                
                
              </div>
              <div class="modal-footer" style="border:none !important;">
               <!-- <button type="button" class="btn btn-default" onclick="send_message_user()" >Send Message</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div>
            <script>

			function popup_record(tbl_parenting_school_id) {
					$('#myModal').modal('show');
					$.ajax({
						type: "POST",
						url: "<?=HOST_URL?>/en/admin/records/popup_school_record",
						data: {
							tbl_parenting_school_id:tbl_parenting_school_id,
							is_ajax: true
						},
						success: function(data) {
							$("#modal-body").html(data);
							//$('#txt_message').html(data);
							//$( "#alert_message_sent" ).dialog({ modal: true },{ resizable: false }, { draggable: false });
							//$( "#alert_message_sent" ).dialog({ title: "Message" },{ buttons: [ { text: "Ok", click: function() { window.location.reload();	} } ] });
						//	$('#myModal').modal('hide');
						//	my_alert("Message sent successfully.", "green");
						//	setTimeout(function(){window.location.reload();},1000);
						},
						error: function() {
							$('#pre_loader').css('display','none');	
						}, 
						complete: function() {
							$('#pre_loader').css('display','none');	
						}
					});	
			   }
    
	         </script>
            <!--Add or Create-->
            <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Records</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
   	            <div class="box-body">
                    <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
    
                        
                     <div class="form-group">
                     <label class="col-sm-2 control-label" for="tbl_parenting_school_cat_id">Records Category<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-4">
                                 <select name="tbl_parenting_school_cat_id" id="tbl_parenting_school_cat_id" class="form-control" onclick="removeCategory();"  >
                                 <option value="">Select Category</option>
                              <?php
                                    for ($u=0; $u<count($category_list); $u++) { 
                                        $tbl_parenting_school_cat_id_u   = $category_list[$u]['tbl_parenting_school_cat_id'];
                                        $title_en         = $category_list[$u]['title_en'];
                                        $title_ar         = $category_list[$u]['title_ar'];
                                        if($tbl_sel_parenting_school_cat_id == $tbl_parenting_school_cat_id_u)
                                           $selType = "selected";
                                         else
                                           $selType = "";
                                  ?>
                                      <option value="<?=$tbl_parenting_school_cat_id_u?>"  <?=$selType?> >
                                      <?=$title_en?>&nbsp;[::]&nbsp;
                                    <?=$title_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>
                    </div>
                    <label class="col-sm-1 control-label" for="labl_add_cat" style="font-size:12px; text-decoration:underline; cursor:pointer; color:#030;" ><span style="padding-left:2px;" onclick="addNewCategory();" id="tbl_add_cat_id">+&nbsp;Add New Category</span></label>
                    
                    </div>
                    
                    
                    
                    <div class="form-group" id="divCategory" style="display:none;" >
                      <label class="col-sm-2 control-label" for="category_en">Category [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-4">
                        <input type="text" placeholder="Category [En]" id="category_en" name="category_en" class="form-control"  value="<?=$category_en?>" >
                      </div> 
                      
                      <label class="col-sm-1 control-label" for="category_ar">Category [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-4">
                        <input type="text" placeholder="Category [Ar]" id="category_ar" name="category_ar" class="form-control" dir="rtl" value="<?=$category_ar?>" >
                      </div>
                      
                    </div>
                    
                    
                    
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="parenting_title_en">Title [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-4">
                        <input type="text" placeholder="Title [En]" id="parenting_title_en" name="parenting_title_en" class="form-control">
                      </div> 
                      
                      <label class="col-sm-1 control-label" for="parenting_title_ar">Title [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-4">
                        <input type="text" placeholder="Title [Ar]" id="parenting_title_ar" name="parenting_title_ar" class="form-control" dir="rtl">
                      </div>
                    </div>
                    
               <div id="record_upload" style="display:block;">
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="thumb_image">Thumb Image</label>
    
                      <div class="col-sm-4" >
                        <!--File Upload START-->
                           <div id="divThumb">
                            <style>
                            #advancedNewUpload {
                                padding-bottom:0px;
                            }
                            </style>
                                 
                               <div id="advancedNewUpload">Upload File</div>
                            
                            </div>
                            <div id="uploaded_items" >
                                <div id="div_listing_container_new" class="listing_container" style="display:block">	            
										<?php
                                            if (trim($img_url) != "") {
                                        ?>
                                                    <div id='<?=$tbl_uploads_id?>' class='box-header with-border'>
                                                      <div class='box-title'><img src='<?=$img_url?>' /></div>
                                                      <div class='box-tools'> <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' 
                                                      onclick="confirm_delete_img_popup('<?=$tbl_uploads_id?>')">
                                                        </button>
                                                      </div>
                                                    </div>
											<style>
												.ajax-upload-dragdrop-new {
													display:none;	
												}
                                            </style>        
                                        <?php		
                                            }
                                        ?>
                                </div>        
                            </div>
                        <!--File Upload END-->
                      </div>
                   
                    
                    </div>
                    
                    
                    
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="parenting_type">Record Type</label>
    
                      <div class="col-sm-4" id="typeDiv" >
                         <input type="radio" name="parenting_type" value="d" onClick="show_text()"> Text&nbsp;&nbsp;&nbsp;&nbsp;
                         <input type="radio"  name="parenting_type" value="u" onClick="show_url()"> URL&nbsp;&nbsp;&nbsp;&nbsp;
                         <input name="parenting_type" type="radio" value="v" onClick="show_video()"> <?=$allowed_files_display?>
                      </div>
                    </div>
                          
                 
                <div id="parenting_text">
                       <div class="form-group">
                         <label class="col-sm-2 control-label" for="parenting_text_en">Parenting Text [En]</label>
    
                          <div class="col-sm-4">
                            <textarea name="parenting_text_en" id="parenting_text_en" cols="80" rows="10" class="form-control" ></textarea>
                          </div>
                          
                          <label class="col-sm-2 control-label" for="parenting_text_en">Parenting Text [Ar]</label>
    
                          <div class="col-sm-4">
                            <textarea name="parenting_text_ar" id="parenting_text_ar" cols="80" rows="10" class="form-control" dir="rtl"></textarea>
                          </div>
                          
                        </div>
                     
                 </div>
                 
               <!-- <div id="parenting_video">
                       <div class="form-group">
                       <label class="col-sm-2 control-label" for="parenting_video">Video</label>
    
                          <div class="col-sm-10">
                           <div id="advancedUpload">Upload</div>
                          </div>
                        </div>
                 </div>   -->  
                          
                          
                 <div id="parenting_url">
                        <div class="form-group">
                         <label class="col-sm-2 control-label" for="parenting_url">URL</label>
    
                          <div class="col-sm-4">
                           <input type="text" name="url" id="url" value="<?=$url?>" size="50" class="form-control" maxlength="50">
                          </div>
                        </div>
                 </div>
                 <div id="parenting_video">
                 
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="thumb_image">Record File</label>
    
                      <div class="col-sm-4" >
                         <div id="divFile">
                        <!--File Upload START-->
                            <style>
                            #advancedFileUpload {
                                padding-bottom:0px;
                            }
                            </style>
                                 
                            <div id="advancedFileUpload">Upload File</div>
                        </div>
                            
                            <div id="uploaded_items_file" >
                                <div id="div_listing_container" class="listing_container" style="display:block">	            
										<?php
                                            if (trim($img_url) != "") {
                                        ?>
                                                    <div id='<?=$tbl_uploads_id?>' class='box-header with-border'>
                                                      <div class='box-title'><img src='<?=$img_url?>' /></div>
                                                      <div class='box-tools'> <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick="confirm_delete_file_popup('<?=$tbl_uploads_id?>')">
                                                        </button>
                                                      </div>
                                                    </div>
											<style>
												.ajax-upload-dragdrop {
													display:none;	
												}
                                            </style>        
                                        <?php		
                                            }
                                        ?>
                                </div>        
                            </div>
                        <!--File Upload END-->
                      </div>
                   
                    
                    </div>
                 </div>  
          
                 <div class="form-group">
                         <label class="col-sm-2 control-label" for="parenting_status">Status</label>
    
                          <div class="col-sm-10" id="activeDiv">
                           <input name="is_active" type="radio" value="Y" checked> Yes
                           <input type="radio" name="is_active" value="N"> No
                          </div>
                  </div>
            
            </div>  
                  
                  
                   <div class="form-group">
                     <label class="col-sm-2 control-label" for="assigned_list"></label>
                     <div class="col-sm-10">
                        <div id="divClassList" onclick="openClassList()" style=" display:block; color:#360; font-weight:bold; text-decoration:underline; cursor:pointer;">Show Assigned Class List</div> 
                     </div>
                 </div>  
                 
                 <div id="class_list" style="display:none;">
                    
                    <div class="form-group" id="all_student">
                        <label class="col-sm-2 control-label" for="tbl_class_id">Class(s)<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10" id="divClass" style="height:200px; overflow-y:scroll;">
                          <div class"col-sm-10" >
	 
                        <?php  if(count($classes_list)>0){ ?>
                           <div style="padding-bottom:10px;background-color:#e8eaeb;" class="col-sm-12"> 
                           <input type="checkbox" value="" id="select_all_class" class="checkboxClass" >&nbsp;Select All</div>
                        <?php  } ?>
                        <?php
                            for ($u=0; $u<count($classes_list); $u++) { 
								$tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
								$class_name             = $classes_list[$u]['class_name'];
								$class_name_ar          = $classes_list[$u]['class_name_ar'];
								$section_name           = $classes_list[$u]['section_name'];
								$section_name_ar        = $classes_list[$u]['section_name_ar'];
                             ?>
                             <div class="col-sm-4" style="padding-top:10px; padding-bottom:10px; border:1px solid #CCC;"> 
								  <input id="tbl_class_id_<?=$u?>" name="tbl_class_id[]" class="checkboxClass" type="checkbox" value="<?=$tbl_class_id_u?>"  onChange="get_students_ajax()" />&nbsp;
								   <?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;<?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                             </div>
							<?php  
	                         }
							 ?>
	                          </div> 
                        </div>
                        </div>    
                   
                    
                  
                 <div class="form-group">
                          <label class="col-sm-2 control-label" for="tbl_student_id">Student(s)<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10" id="divStudent" style="height:400px; overflow-y:scroll;">
                          <select id="tbl_student_dropdown" name="tbl_student_id" multiple size="10" style="padding:0px 5px 0px 2px; font-size:14px; width:100%;">
    
                                <?php for($i=0; $i<count($data_rs); $i++) { ?>
    
                                <option value="<?=$data_rs[$i]["tbl_student_id"]?>"><?=$data_rs[$i]["first_name"]?> <?=$data_rs[$i]["last_name"]?> :: <?=$data_rs[$i]["first_name_ar"]?> <?=$data_rs[$i]["last_name_ar"]?> </option>
    
                                <?php	} ?>
    
                        </select> 
                        </div>
                     </div> 
                  
                  </div>         
                     
                        <!-- /.box-body -->
                      <div class="box-footer">
                        <button class="btn btn-primary" type="button" onclick="ajax_validate()">Submit</button>
                        <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                      </div>
                      <!-- /.box-footer -->  
                      
            
           </form>
                </div>
           </div>
           <script>
           var item_id = "<?=$parenting_school_id_enc?>";  
		     
		//tbl_parenting_school_cat_id
		  function removeCategory()
		 {
			var str = $("#tbl_parenting_school_cat_id").val();
			if (str.length <= 0) {
				$("#tbl_add_cat_id").show();
			}else{
				$("#divCategory").hide();
				$("#category_en").val('');
				$("#category_ar").val('');
				$("#tbl_add_cat_id").hide();
			}
		 }
		
		 function addNewCategory()
		 {
			// divCategory
			if($('#divCategory').is(':visible'))
			{
				$("#divCategory").hide();
			}else{
				$("#divCategory").show();
				$("#tbl_add_cat_id").hide();
			}
			 
		 }
		  </script>   
          
          <script>
		   function openClassList()
		   {
			 if($('#class_list').is(':visible')){ 
				$('#class_list').hide();
				$('#record_upload').show();
				$('#divClassList').html("Show Assigned Class List");
			 }else{
				$('#class_list').show();
				$('#divClassList').html("Hide Assigned Class List");
				$('#record_upload').hide();
			 }
		   }
	   </script>
       <script>
var host = '<?=HOST_URL?>';
function get_students_ajax() {
		//show_loading();
	
		//show_loading();
		var tbl_class_id = "";
        $('.checkboxClass:checked').each(function(i){
          tbl_class_id += $(this).val()+",";
        });
		//alert(tbl_class_id);
		//return;
		var xmlHttp, rnd, url, search_param, ajax_timer;
		rnd = Math.floor(Math.random()*11);
		try{		
			xmlHttp = new XMLHttpRequest(); 
		}catch(e) {
			try{
				xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
			}catch(e) {
				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
				hide_loading();
			}
		}

		//AJAX response
		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
				//ajax_timer.stop();
				var data = xmlHttp.responseText;
				$("#divStudent").html(data);
				//$("#tbl_student_dropdown").multiselect('refresh');
				return;
			}
		}

		/*ajax_timer = $.timer(function() {
			xmlHttp.abort();
			alert(connectivity_msg);
			ajax_timer.stop();
		},connectivity_timeout_time,true);*/

		//Sending AJAX request
		url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/students_against_classes/tbl_class_id/"+tbl_class_id+"/rnd/"+rnd;
		xmlHttp.open("POST",url,true);
		xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlHttp.send("rnd="+rnd);
	}
	
       $('#select_all_class').on('click',function(){
			if(this.checked){
				$('.checkboxClass').each(function(){
					this.checked = true;
				});
				
			}else{
				 $('.checkboxClass').each(function(){
					this.checked = false;
				});
			}
			
			get_students_ajax();
			
			if(this.checked){
				$('.checkboxModule').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkboxModule').each(function(){
					this.checked = false;
				});
			}
			
			
		});
      </script>

            <!--/Add or Create-->
                
        <!--/Admin Category Management-->

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>


<script src="<?=HOST_URL?>/assets/admin/dist/js/jquery.uploadfile.min.js"></script>
<script language="javascript">
//Primary Key for a Form. 

function set_item_id(obj) {
	item_id = obj.value;
	get_files();	
}

$(document).ready(function() {
	
	var uploadObj = $("#advancedNewUpload").uploadFile({
		url:"<?=HOST_URL?>/file_mgmt/upload_the_file",
		multiple:true,
		autoSubmit:true,
		maxFileSize:130000,
		fileName:"myfile",
		formData: {"module_name":"parenting_logo"},
		dynamicFormData: function() {
			var data = { item_id:item_id}
			return data;
		},
		showStatusAfterSuccess:false,
		dragDropStr: "<span class='d_d_text'>Optionally Drag and Drop the File to Upload.</span>",
		abortStr:"Abourt",
		cancelStr:"Cancel",
		doneStr:"Done",
		multiDragErrorStr: "Multi Drag Error.",
		extErrorStr:"Extention Error:",
		sizeErrorStr:"Max Size Error:",
		uploadErrorStr:"Upload Error",
		onSelect:function(files) {
 		},
		onSubmit:function(files) {
 		},
		onSuccess:function(files, data, xhr) {
			if (data == "error") {
				alert("Error uploading file. Please try again.");
				return;
			}
			var obj = JSON.parse(data);
			var tbl_uploads_id = obj.tbl_uploads_id;
			var file_name_updated = obj.file_name_updated;
			$("#divThumb").hide();
			//alert("tbl_uploads_id: "+tbl_uploads_id)
			//alert("file_name_updated: "+file_name_updated)
			add_new_uploaded_item(tbl_uploads_id, file_name_updated);
		},
		afterUploadAll:function() {
 		},
		onError: function(files, status, errMsg) {
 		}
	});
	
	
	
	var uploadFileObj = $("#advancedFileUpload").uploadFile({
		url:"<?=HOST_URL?>/file_mgmt/upload_the_file",
		multiple:true,
		autoSubmit:true,
		maxFileSize:500000000,
		fileName:"myfile",
		formData: {"module_name":"parenting_file"},
		dynamicFormData: function() {
			var data = { item_id:item_id}
			return data;
		},
		showStatusAfterSuccess:false,
		dragDropStr: "<span class='d_d_text'>Optionally Drag and Drop the File to Upload.</span>",
		abortStr:"Abourt",
		cancelStr:"Cancel",
		doneStr:"Done",
		multiDragErrorStr: "Multi Drag Error.",
		extErrorStr:"Extention Error:",
		sizeErrorStr:"Max Size Error:",
		uploadErrorStr:"Upload Error",
		onSelect:function(files) {
 		},
		onSubmit:function(files) {
 		},
		onSuccess:function(files, data, xhr) {
			if (data == "error") {
				alert("Error uploading file. Please try again.");
				return;
			}
			var obj = JSON.parse(data);
			var tbl_uploads_id = obj.tbl_uploads_id;
			var file_name_updated = obj.file_name_updated;
			$("#divFile").hide();
			//alert("tbl_uploads_id: "+tbl_uploads_id)
			//alert("file_name_updated: "+file_name_updated)
			add_new_uploaded_file(tbl_uploads_id, file_name_updated);
		},
		afterUploadAll:function() {
 		},
		onError: function(files, status, errMsg) {
 		}
	});
	
	

	$("#startUpload").click(function() {
		uploadObj.startUpload();
	});
	
	$("#startUpload").click(function() {
		uploadFileObj.startUpload();
	});
	
	try { 
		$('input[type=file]').click();
	} catch(e) {
		alert(e)
	}
});

//Function called when file is uploaded
function add_new_uploaded_item(tbl_uploads_id, file_name_updated) {
	var str = "<div id='"+tbl_uploads_id+"' class='box-header with-border'> <div class='box-title'><img src='<?=IMG_UPLOAD_PATH?>/"+file_name_updated+"' /></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_img_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
		
	$("#div_listing_container_new").show();
	$("#div_listing_container_new").append(str);
	
	//$(".ajax-upload-dragdrop-new").hide();//Hide the upload button
return;
}

function add_new_uploaded_file(tbl_uploads_id, file_name_updated) {
	var result      = $(file_name_updated).text().split('.');
    var type_file   =  result[1];
	if(type_file=='png' || type_file=='jpg')
	{
		var str = "<div id='"+tbl_uploads_id+"' class='box-header with-border'> <div class='box-title'><img src='<?=IMG_UPLOAD_PATH?>/"+file_name_updated+"' /></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_file_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
	}else{
		var str = "<div id='"+tbl_uploads_id+"' class='box-header with-border'> <div class='box-title'><a target='_blank' href='<?=IMG_UPLOAD_PATH?>/"+file_name_updated+"' >"+file_name_updated+"</a></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_file_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
	}
		
	$("#div_listing_container").show();
	$("#div_listing_container").append(str);
	//$(".ajax-upload-dragdrop").hide();//Hide the upload button
return;
}


function confirm_delete_img_popup(tbl_uploads_id) {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		$('#'+tbl_uploads_id).hide();	
		//$(".ajax-upload-dragdrop-new").show();
		$("#divThumb").show();
		var url_str = "<?=HOST_URL?>/file_mgmt/delete_file";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					tbl_uploads_id: tbl_uploads_id
				},
			success: function(data) {
				$("#pre-loader").hide();
			}
		});	
	} else {
		$("#pre-loader").hide();		
	}
}


function confirm_delete_img() {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		$("#divThumb .ajax-upload-dragdrop").css({"display":"block"});
		$("#divThumb").show();
		$("#div_listing_container_new").html('');
		$("#pre-loader").hide();	
	} 
}

function confirm_delete_file(tbl_uploads_id) {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		$('#'+tbl_uploads_id).hide();	
		//$(".ajax-upload-dragdrop").show();
		$("#divFile .ajax-upload-dragdrop").css({"display":"block"});
		$("#divFile").show();
		
		var url_str = "<?=HOST_URL?>/file_mgmt/delete_file";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					tbl_uploads_id: tbl_uploads_id
				},
			success: function(data) {
				$("#pre-loader").hide();
			}
		});	
	} else {
		$("#pre-loader").hide();		
	}
}






function confirm_delete_file_popup(tbl_uploads_id) {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		$('#'+tbl_uploads_id).hide();	
		//$(".ajax-upload-dragdrop").show();
		$("#divFile").show();
		
		var url_str = "<?=HOST_URL?>/file_mgmt/delete_file";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					tbl_uploads_id: tbl_uploads_id
				},
			success: function(data) {
				$("#pre-loader").hide();
			}
		});	
	} else {
		$("#pre-loader").hide();		
	}
}


function get_files() {
	var url_str = "<?=HOST_URL?>/misc/get_files.php";
	
	$.ajax({
		type: "POST",
		url: url_str,
		data: {
				module_name: "student",
				show_del: "Y",
				item_id: item_id//global variable
			},
		success: function(data) {
			$('#div_listing_container_new').show();
			$('#div_listing_container_new').html(data)
			
		}
	});	
}


</script>

<!--File Upload END-->
        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >
function search_data() {
		var q = $("#q").val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/school_records/";
		
		if(q !='')
			url += "q/"+q+"/";
		
			url += "offset/0/";
		window.location.href = url;
	}

function reset_data() {
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/school_records/";
		url += "offset/0/";
		window.location.href = url;
	}

$(document).ready(function(){	
<?php  if($from=="category"){ ?>
		$("#mid1").hide(500);
		$('#mid1_list').hide(500);
		$('#mid2').show(500);
	<?php } ?>
});
</script>


