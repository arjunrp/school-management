<?php
//Init Parameters
$academic_year_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}


?>
<link rel="stylesheet" media="all" type="text/css" href="<?=JS_PATH?>/date_time_picker/css/jquery-ui-1.8.6.custom.css" />
<style type="text/css">
pre {
	padding: 20px;
	background-color: #ffffcc;
	border: solid 1px #fff;
}
.example-container {
	background-color: #f4f4f4;
	border-bottom: solid 2px #777777;
	margin: 0 0 40px 0;
	padding: 20px;
}

.example-container p {
	font-weight: bold;
}

.example-container dt {
	font-weight: bold;
	height: 20px;
}

.example-container dd {
	margin: -20px 0 10px 100px;
	border-bottom: solid 1px #fff;
}

.example-container input {
	width: 150px;
}

.clear {
	clear: both;
}

#ui-datepicker-div {
}

.ui-timepicker-div .ui-widget-header {
	margin-bottom: 8px;
}

.ui-timepicker-div dl {
	text-align: left;
}

.ui-timepicker-div dl dt {
	height: 25px;
}

.ui-timepicker-div dl dd {
	margin: -25px 0 10px 65px;
}

.ui-timepicker-div td {
	font-size: 90%;
}
</style>

<script type="text/javascript" src="<?=JS_PATH?>/date_time_picker/js/jquery-ui-1.8.6.custom.min.js"></script>
<script type="text/javascript" src="<?=JS_PATH?>/date_time_picker/js/jquery-ui-timepicker-addon.js"></script>
<!--<script language="javascript">
	$(document).ready(function(){
		$('#start_date').datepicker({
			dateFormat: '',
			timeFormat: 'hh:mm tt',
			timeOnly: true   
		});


		$('#end_date').datepicker({
			dateFormat: '',
			timeFormat: 'hh:mm tt',
			timeOnly: true   
		});
	});
</script>-->
<script type="text/javascript" src="<?=JS_COLOR_PATH?>/jscolor.js"></script>

<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/academic_year_conf";
			}
		})
	});
	
function confirm_delete_popup() {
		var len = $("input[id='academic_year_id_enc']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more academic year(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/deleteAcademicYear",
			data: {
				academic_year_id_enc: $("input[id='academic_year_id_enc']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Academic Year(s) deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}	
	function ajax_activate(academic_year_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/activateAcademicYear",
			data: {
				academic_year_id_enc: academic_year_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Academic Year activated successfully.", 'green')

				$('#act_deact_'+academic_year_id_enc).html('<span style="cursor:pointer" onClick="ajax_deactivate(\''+academic_year_id_enc+'\')" onMouseOver="deactivate_me(this)" onMouseOut="reset_activate(this)" class="label label-success">Active</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_deactivate(academic_year_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/deactivateAcademicYear",
			data: {
				academic_year_id_enc: academic_year_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Academic Year de-activated successfully.", 'green')
				
				$('#act_deact_'+academic_year_id_enc).html('<span style="cursor:pointer" onClick="ajax_activate(\''+academic_year_id_enc+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/is_exist_academic_year",
			data: {
			    tbl_academic_year_id: "<?=$academic_year_id_enc?>",
				academic_start: $('#academic_start').val(),
				academic_end: $('#academic_end').val(),
				is_ajax: true
			},
			success: function(data) {
				
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='T') {
					refresh_page = "N";
					my_alert("Academic Year already exists.", 'red');
					$("#pre-loader").hide();
				}else if (temp=='Y') {
					refresh_page = "N";
					my_alert("Academic Year already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_create();
				}
			
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist_edit() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/is_exist_academic_year",
			data: {
				tbl_academic_year_id: $('#academic_year_id_enc').val(),
				academic_start: $('#academic_start').val(),
				academic_end: $('#academic_end').val(),
				is_ajax: true
			},
			success: function(data) {
				
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='T') {
					refresh_page = "N";
					my_alert("Academic Year already exists.", 'red');
					$("#pre-loader").hide();
				}else if (temp=='Y') {
					refresh_page = "N";
					my_alert("Academic Year already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_save_changes();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_create() {
		 
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/create_academic_year",
			data: {
			    tbl_academic_year_id: "<?=$academic_year_id_enc?>",
				academic_start: $('#academic_start').val(),
				academic_end: $('#academic_end').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
				refresh_page = "Y";
				    my_alert("Academic Year is added successfully.", 'green');
				    $("#pre-loader").hide();
				}else{
					refresh_page = "N";
					my_alert("Academic Year is added failed, Please try again.", 'red');
					$("#pre-loader").hide();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_save_changes() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/save_academic_year_changes",
			data: {
				tbl_academic_year_id: $('#academic_year_id_enc').val(), 
				academic_start      : $('#academic_start').val(),
				academic_end        : $('#academic_end').val(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Changes saved successfully.", 'green');
				
				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
</script>
<script language="javascript">
	function ajax_validate() {
		if (validate_academic_year() == false ) {
			return false;
		} else {
			is_exist();
		}
	} 

	function ajax_validate_edit() {
		if (validate_academic_year() == false ) {
			return false;
		} else {
			is_exist_edit();
		}
	} 

  
	
	 function validate_academic_year() {
		var regExp = / /g;
		var str = $('#academic_start').val();
		if (str.length <= 0) {
			my_alert("Start Year is not selected. Please select Start Year.");
		return false;
		}
		
		var strEnd = $('#academic_end').val();
		if (strEnd.length <= 0) {
			my_alert("End Year is not selected. Please select End Year.");
		return false;
		}
		
		if(Math.abs(str)>Math.abs(strEnd))
		{
			my_alert("Start Year should be less than or equal to End Year.");
			return false;
		}
		return true;
	}
	
	
</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Academic Years Configuration <small> Management</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Academic Years</li>
    </ol>
     <div style=" float:right; padding-right:10px;"> <button onclick="show_semester()" title="Semesters" type="button" class="btn btn-primary">Semesters</button></div> 
    <!--/BREADCRUMB--> 
    <div style="clear:both"></div>
  </section>
  <script>
   function show_semester()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/semester_conf/";
		window.location.href = url;
	}
  </script>
  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {
			$tbl_sel_academic_year_id= $academic_year_obj['tbl_academic_year_id'];
			$academic_start          = $academic_year_obj['academic_start'];
			$academic_end            = $academic_year_obj['academic_end'];
	?>
        <!--Edit-->
        
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Semester</h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/academic_year_conf"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_edit" id="frm_listing" class="form-horizontal" method="post">
                    <div class="box-body">
                   
                       <div class="form-group">
                      <label class="col-sm-2 control-label" for="title">Academic Year<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-1">
                         <select name="academic_start" id="academic_start" class="form-control">
                      <option value="" >Start Year</option>  
                      <option value="2015" <?PHP if (($academic_start == "2015")) { echo "selected";}?>>2015</option>
                      <option value="2016" <?PHP if (($academic_start == "2016")) { echo "selected";}?>>2016</option>
                      <option value="2017" <?PHP if (($academic_start == "2017")) { echo "selected";}?>>2017</option>
                      <option value="2018" <?PHP if (($academic_start == "2018")) { echo "selected";}?>>2018</option>
                      <option value="2019" <?PHP if (($academic_start == "2019")) { echo "selected";}?>>2019</option>
                      <option value="2020" <?PHP if (($academic_start == "2020")) { echo "selected";}?>>2020</option>
                      <option value="2021" <?PHP if (($academic_start == "2021")) { echo "selected";}?>>2021</option>
                      <option value="2022" <?PHP if (($academic_start == "2022")) { echo "selected";}?>>2022</option>
                      <option value="2023" <?PHP if (($academic_start == "2023")) { echo "selected";}?>>2023</option>
                      <option value="2024" <?PHP if (($academic_start == "2024")) { echo "selected";}?>>2024</option>
                      <option value="2025" <?PHP if (($academic_start == "2025")) { echo "selected";}?>>2025</option>
                      <option value="2026" <?PHP if (($academic_start == "2026")) { echo "selected";}?>>2026</option>
                      <option value="2027" <?PHP if (($academic_start == "2027")) { echo "selected";}?>>2027</option>
                      <option value="2028" <?PHP if (($academic_start == "2028")) { echo "selected";}?>>2028</option>
                      <option value="2029" <?PHP if (($academic_start == "2029")) { echo "selected";}?>>2029</option>
                      <option value="2030" <?PHP if (($academic_start == "2030")) { echo "selected";}?>>2030</option>
                      <option value="2031" <?PHP if (($academic_start == "2031")) { echo "selected";}?>>2031</option>
                      <option value="2032" <?PHP if (($academic_start == "2032")) { echo "selected";}?>>2032</option>
                      <option value="2033" <?PHP if (($academic_start == "2033")) { echo "selected";}?>>2033</option>
                      <option value="2034" <?PHP if (($academic_start == "2034")) { echo "selected";}?>>2034</option>
                      <option value="2035" <?PHP if (($academic_start == "2035")) { echo "selected";}?>>2035</option>
                      <option value="2036" <?PHP if (($academic_start == "2036")) { echo "selected";}?>>2036</option>
                      <option value="2037" <?PHP if (($academic_start == "2037")) { echo "selected";}?>>2037</option>
                      <option value="2038" <?PHP if (($academic_start == "2038")) { echo "selected";}?>>2038</option>
                      <option value="2039" <?PHP if (($academic_start == "2039")) { echo "selected";}?>>2039</option>
                      <option value="2040" <?PHP if (($academic_start == "2040")) { echo "selected";}?>>2040</option>
                      <option value="2041" <?PHP if (($academic_start == "2041")) { echo "selected";}?>>2041</option>
                      <option value="2042" <?PHP if (($academic_start == "2042")) { echo "selected";}?>>2042</option>
                      <option value="2043" <?PHP if (($academic_start == "2043")) { echo "selected";}?>>2043</option>
                      <option value="2044" <?PHP if (($academic_start == "2044")) { echo "selected";}?>>2044</option>
                      <option value="2045" <?PHP if (($academic_start == "2045")) { echo "selected";}?>>2045</option>
                      <option value="2046" <?PHP if (($academic_start == "2046")) { echo "selected";}?>>2046</option>
                      <option value="2047" <?PHP if (($academic_start == "2047")) { echo "selected";}?>>2047</option>
                      <option value="2048" <?PHP if (($academic_start == "2048")) { echo "selected";}?>>2048</option>
                      <option value="2049" <?PHP if (($academic_start == "2049")) { echo "selected";}?>>2049</option>
                      <option value="2050" <?PHP if (($academic_start == "2050")) { echo "selected";}?>>2050</option>
                    </select>
                  </div>
                  <div class="col-sm-1">
                   <select name="academic_end" id="academic_end" class="form-control">
                      <option value="" >End Year</option>  
                      <option value="2015" <?PHP if (($academic_end == "2015")) { echo "selected";}?>>2015</option>
                      <option value="2016" <?PHP if (($academic_end == "2016")) { echo "selected";}?>>2016</option>
                      <option value="2017" <?PHP if (($academic_end == "2017")) { echo "selected";}?>>2017</option>
                      <option value="2018" <?PHP if (($academic_end == "2018")) { echo "selected";}?>>2018</option>
                      <option value="2019" <?PHP if (($academic_end == "2019")) { echo "selected";}?>>2019</option>
                      <option value="2020" <?PHP if (($academic_end == "2020")) { echo "selected";}?>>2020</option>
                      <option value="2021" <?PHP if (($academic_end == "2021")) { echo "selected";}?>>2021</option>
                      <option value="2022" <?PHP if (($academic_end == "2022")) { echo "selected";}?>>2022</option>
                      <option value="2023" <?PHP if (($academic_end == "2023")) { echo "selected";}?>>2023</option>
                      <option value="2024" <?PHP if (($academic_end == "2024")) { echo "selected";}?>>2024</option>
                      <option value="2025" <?PHP if (($academic_end == "2025")) { echo "selected";}?>>2025</option>
                      <option value="2026" <?PHP if (($academic_end == "2026")) { echo "selected";}?>>2026</option>
                      <option value="2027" <?PHP if (($academic_end == "2027")) { echo "selected";}?>>2027</option>
                      <option value="2028" <?PHP if (($academic_end == "2028")) { echo "selected";}?>>2028</option>
                      <option value="2029" <?PHP if (($academic_end == "2029")) { echo "selected";}?>>2029</option>
                      <option value="2030" <?PHP if (($academic_end == "2030")) { echo "selected";}?>>2030</option>
                      <option value="2031" <?PHP if (($academic_end == "2031")) { echo "selected";}?>>2031</option>
                      <option value="2032" <?PHP if (($academic_end == "2032")) { echo "selected";}?>>2032</option>
                      <option value="2033" <?PHP if (($academic_end == "2033")) { echo "selected";}?>>2033</option>
                      <option value="2034" <?PHP if (($academic_end == "2034")) { echo "selected";}?>>2034</option>
                      <option value="2035" <?PHP if (($academic_end == "2035")) { echo "selected";}?>>2035</option>
                      <option value="2036" <?PHP if (($academic_end == "2036")) { echo "selected";}?>>2036</option>
                      <option value="2037" <?PHP if (($academic_end == "2037")) { echo "selected";}?>>2037</option>
                      <option value="2038" <?PHP if (($academic_end == "2038")) { echo "selected";}?>>2038</option>
                      <option value="2039" <?PHP if (($academic_end == "2039")) { echo "selected";}?>>2039</option>
                      <option value="2040" <?PHP if (($academic_end == "2040")) { echo "selected";}?>>2040</option>
                      <option value="2041" <?PHP if (($academic_end == "2041")) { echo "selected";}?>>2041</option>
                      <option value="2042" <?PHP if (($academic_end == "2042")) { echo "selected";}?>>2042</option>
                      <option value="2043" <?PHP if (($academic_end == "2043")) { echo "selected";}?>>2043</option>
                      <option value="2044" <?PHP if (($academic_end == "2044")) { echo "selected";}?>>2044</option>
                      <option value="2045" <?PHP if (($academic_end == "2045")) { echo "selected";}?>>2045</option>
                      <option value="2046" <?PHP if (($academic_end == "2046")) { echo "selected";}?>>2046</option>
                      <option value="2047" <?PHP if (($academic_end == "2047")) { echo "selected";}?>>2047</option>
                      <option value="2048" <?PHP if (($academic_end == "2048")) { echo "selected";}?>>2048</option>
                      <option value="2049" <?PHP if (($academic_end == "2049")) { echo "selected";}?>>2049</option>
                      <option value="2050" <?PHP if (($academic_end == "2050")) { echo "selected";}?>>2050</option>
                    </select>
                    </div>
                    <div class="col-sm-1">
                      Eg: 2016 - 2017
                    </div>
                    </div>
                    
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate_edit()">Save Changes</button>
                    <input type="hidden" name="academic_year_id_enc" id="academic_year_id_enc" value="<?=$tbl_sel_academic_year_id?>" />
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
		        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/classes/semester_conf";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
    
  
 <link href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" rel="stylesheet">
 <script src="http://code.jquery.com/jquery-1.11.1.js"></script>
 <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
  <script>
  $( function() {
		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
				
				/* var order = $("#tabledivbody").sortable("serialize");
				
				alert(order);
				
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/your/url/here'
				});*/
				
				
				
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	

				
				
				
				
				
			}
	  } );
  
  } );
  
  
  
  </script> 
    
                    <div id="mid1" class="box box-success">
                        <div class="box-header">
                          <div style="width:20%; float:left;">
                          <h3 class="box-title">SEARCH</h3>
                          </div>
                          <div style="width:60%; float:left;"> 
                    	     <div class="col-sm-5">
                             <?php //echo $category_parent_list ?>
                   		      </div>
                             
                          </div>
                        </div>  
                     </div>     
    
            <!--Listing-->
                    <div id="mid1_list" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Academic Years</h3>
                          <div class="box-tools">
                            <?php if (count($rs_all_academic_years)>0) { echo $paging_string;}?>	
                            <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
                            <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>
                          </div>
                        </div>
                        
                        <div class="box-body">
                     <!--   <div style="color:#030; font-weight:bold;">You can sort categories by using drag and drop of rows </div>-->
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle"><input id="select_all" type="checkbox" value="" /></th>
                              <!--<th width="10%" align="center" valign="middle">Sl No.</th>-->
                          
                              <th width="60%" align="center" valign="middle">Academic Year</th>
                              <th width="7%" align="center" valign="middle">Date</th>
                              <th width="7%" align="center" valign="middle">Status</th>
                              <th width="7%" align="center" valign="middle">Action</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                                for ($i=0; $i<count($rs_all_academic_years); $i++) { 
                                    $id                        = $rs_all_academic_years[$i]['id'];
                                    $tbl_academic_year_id      = $rs_all_academic_years[$i]['tbl_academic_year_id'];
									$academic_year             = $rs_all_academic_years[$i]['academic_start']." - ".$rs_all_academic_years[$i]['academic_end'];
                                    $added_date                = $rs_all_academic_years[$i]['added_date'];
                                    $is_active                 = $rs_all_academic_years[$i]['is_active'];
                                    
                                    $added_date                = date('m-d-Y',strtotime($added_date));
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_academic_year_id?>" >
                              <td align="left" valign="middle">
                              <span style="float:left;">
                              <input id="academic_year_id_enc" name="academic_year_id_enc" class="checkbox" type="checkbox" value="<?=$tbl_academic_year_id?>" />
                              </span>
                              
                             <?php /*?> <span style="float:left;">&nbsp;
                              <?php if($i<>0){ ?> <i class="fa fa-arrow-up"  style="color:#3c8dbc; cursor:pointer;"  aria-hidden="true" title="Sorting - Drag & Drop To Up"></i> &nbsp; <?php } ?>
                               <?php if($i<> count($rs_all_categories)-1){ ?> <i class="fa fa-arrow-down" style="color:#3c8dbc;cursor:pointer;" aria-hidden="true" title="Sorting - Drag & Drop To Down"></i> <?php } ?>
                              </span><?php */?>
                              </td>
                         <!-- <td align="left" valign="middle"><?=$offset+$i+1?></td>-->
                              <td align="left" valign="middle"><?=$academic_year?></td>
                              <td align="left" valign="middle"><?=$added_date?></td>
                              <td align="left" valign="middle">
                                <div id="act_deact_<?=$tbl_academic_year_id?>">
                                <?php if (trim($is_active) == "Y") { ?>
                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_academic_year_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                <?php } else { ?>
                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_academic_year_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                <?php } ?>
                                </div>
                              </td>
                              <td align="left" valign="middle">
                                <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/edit_academic_year/academic_year_id_enc/<?=$tbl_academic_year_id?>"><button class="btn bg-purple fa fa-pencil" type="button" title="Edit"></button></a>
                              </td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="9" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_all_academic_years)<=0) {
                            ?>
                            <tr>
                              <td colspan="9" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no categories available. Click on the + button to create one.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
    
            <!--Add or Create-->
              <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Academic Year</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                  
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="title">Academic Year<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-1">
                         <select name="academic_start" id="academic_start" class="form-control">
                      <option value="" >Start Year</option>  
                      <option value="2015" <?PHP if (($academic_start == "2015")) { echo "selected";}?>>2015</option>
                      <option value="2016" <?PHP if (($academic_start == "2016")) { echo "selected";}?>>2016</option>
                      <option value="2017" <?PHP if (($academic_start == "2017")) { echo "selected";}?>>2017</option>
                      <option value="2018" <?PHP if (($academic_start == "2018")) { echo "selected";}?>>2018</option>
                      <option value="2019" <?PHP if (($academic_start == "2019")) { echo "selected";}?>>2019</option>
                      <option value="2020" <?PHP if (($academic_start == "2020")) { echo "selected";}?>>2020</option>
                      <option value="2021" <?PHP if (($academic_start == "2021")) { echo "selected";}?>>2021</option>
                      <option value="2022" <?PHP if (($academic_start == "2022")) { echo "selected";}?>>2022</option>
                      <option value="2023" <?PHP if (($academic_start == "2023")) { echo "selected";}?>>2023</option>
                      <option value="2024" <?PHP if (($academic_start == "2024")) { echo "selected";}?>>2024</option>
                      <option value="2025" <?PHP if (($academic_start == "2025")) { echo "selected";}?>>2025</option>
                      <option value="2026" <?PHP if (($academic_start == "2026")) { echo "selected";}?>>2026</option>
                      <option value="2027" <?PHP if (($academic_start == "2027")) { echo "selected";}?>>2027</option>
                      <option value="2028" <?PHP if (($academic_start == "2028")) { echo "selected";}?>>2028</option>
                      <option value="2029" <?PHP if (($academic_start == "2029")) { echo "selected";}?>>2029</option>
                      <option value="2030" <?PHP if (($academic_start == "2030")) { echo "selected";}?>>2030</option>
                      <option value="2031" <?PHP if (($academic_start == "2031")) { echo "selected";}?>>2031</option>
                      <option value="2032" <?PHP if (($academic_start == "2032")) { echo "selected";}?>>2032</option>
                      <option value="2033" <?PHP if (($academic_start == "2033")) { echo "selected";}?>>2033</option>
                      <option value="2034" <?PHP if (($academic_start == "2034")) { echo "selected";}?>>2034</option>
                      <option value="2035" <?PHP if (($academic_start == "2035")) { echo "selected";}?>>2035</option>
                      <option value="2036" <?PHP if (($academic_start == "2036")) { echo "selected";}?>>2036</option>
                      <option value="2037" <?PHP if (($academic_start == "2037")) { echo "selected";}?>>2037</option>
                      <option value="2038" <?PHP if (($academic_start == "2038")) { echo "selected";}?>>2038</option>
                      <option value="2039" <?PHP if (($academic_start == "2039")) { echo "selected";}?>>2039</option>
                      <option value="2040" <?PHP if (($academic_start == "2040")) { echo "selected";}?>>2040</option>
                      <option value="2041" <?PHP if (($academic_start == "2041")) { echo "selected";}?>>2041</option>
                      <option value="2042" <?PHP if (($academic_start == "2042")) { echo "selected";}?>>2042</option>
                      <option value="2043" <?PHP if (($academic_start == "2043")) { echo "selected";}?>>2043</option>
                      <option value="2044" <?PHP if (($academic_start == "2044")) { echo "selected";}?>>2044</option>
                      <option value="2045" <?PHP if (($academic_start == "2045")) { echo "selected";}?>>2045</option>
                      <option value="2046" <?PHP if (($academic_start == "2046")) { echo "selected";}?>>2046</option>
                      <option value="2047" <?PHP if (($academic_start == "2047")) { echo "selected";}?>>2047</option>
                      <option value="2048" <?PHP if (($academic_start == "2048")) { echo "selected";}?>>2048</option>
                      <option value="2049" <?PHP if (($academic_start == "2049")) { echo "selected";}?>>2049</option>
                      <option value="2050" <?PHP if (($academic_start == "2050")) { echo "selected";}?>>2050</option>
                    </select>
                  </div>
                  <div class="col-sm-1">
                   <select name="academic_end" id="academic_end" class="form-control">
                      <option value="" >End Year</option>  
                      <option value="2015" <?PHP if (($academic_end == "2015")) { echo "selected";}?>>2015</option>
                      <option value="2016" <?PHP if (($academic_end == "2016")) { echo "selected";}?>>2016</option>
                      <option value="2017" <?PHP if (($academic_end == "2017")) { echo "selected";}?>>2017</option>
                      <option value="2018" <?PHP if (($academic_end == "2018")) { echo "selected";}?>>2018</option>
                      <option value="2019" <?PHP if (($academic_end == "2019")) { echo "selected";}?>>2019</option>
                      <option value="2020" <?PHP if (($academic_end == "2020")) { echo "selected";}?>>2020</option>
                      <option value="2021" <?PHP if (($academic_end == "2021")) { echo "selected";}?>>2021</option>
                      <option value="2022" <?PHP if (($academic_end == "2022")) { echo "selected";}?>>2022</option>
                      <option value="2023" <?PHP if (($academic_end == "2023")) { echo "selected";}?>>2023</option>
                      <option value="2024" <?PHP if (($academic_end == "2024")) { echo "selected";}?>>2024</option>
                      <option value="2025" <?PHP if (($academic_end == "2025")) { echo "selected";}?>>2025</option>
                      <option value="2026" <?PHP if (($academic_end == "2026")) { echo "selected";}?>>2026</option>
                      <option value="2027" <?PHP if (($academic_end == "2027")) { echo "selected";}?>>2027</option>
                      <option value="2028" <?PHP if (($academic_end == "2028")) { echo "selected";}?>>2028</option>
                      <option value="2029" <?PHP if (($academic_end == "2029")) { echo "selected";}?>>2029</option>
                      <option value="2030" <?PHP if (($academic_end == "2030")) { echo "selected";}?>>2030</option>
                      <option value="2031" <?PHP if (($academic_end == "2031")) { echo "selected";}?>>2031</option>
                      <option value="2032" <?PHP if (($academic_end == "2032")) { echo "selected";}?>>2032</option>
                      <option value="2033" <?PHP if (($academic_end == "2033")) { echo "selected";}?>>2033</option>
                      <option value="2034" <?PHP if (($academic_end == "2034")) { echo "selected";}?>>2034</option>
                      <option value="2035" <?PHP if (($academic_end == "2035")) { echo "selected";}?>>2035</option>
                      <option value="2036" <?PHP if (($academic_end == "2036")) { echo "selected";}?>>2036</option>
                      <option value="2037" <?PHP if (($academic_end == "2037")) { echo "selected";}?>>2037</option>
                      <option value="2038" <?PHP if (($academic_end == "2038")) { echo "selected";}?>>2038</option>
                      <option value="2039" <?PHP if (($academic_end == "2039")) { echo "selected";}?>>2039</option>
                      <option value="2040" <?PHP if (($academic_end == "2040")) { echo "selected";}?>>2040</option>
                      <option value="2041" <?PHP if (($academic_end == "2041")) { echo "selected";}?>>2041</option>
                      <option value="2042" <?PHP if (($academic_end == "2042")) { echo "selected";}?>>2042</option>
                      <option value="2043" <?PHP if (($academic_end == "2043")) { echo "selected";}?>>2043</option>
                      <option value="2044" <?PHP if (($academic_end == "2044")) { echo "selected";}?>>2044</option>
                      <option value="2045" <?PHP if (($academic_end == "2045")) { echo "selected";}?>>2045</option>
                      <option value="2046" <?PHP if (($academic_end == "2046")) { echo "selected";}?>>2046</option>
                      <option value="2047" <?PHP if (($academic_end == "2047")) { echo "selected";}?>>2047</option>
                      <option value="2048" <?PHP if (($academic_end == "2048")) { echo "selected";}?>>2048</option>
                      <option value="2049" <?PHP if (($academic_end == "2049")) { echo "selected";}?>>2049</option>
                      <option value="2050" <?PHP if (($academic_end == "2050")) { echo "selected";}?>>2050</option>
                    </select>
                    </div>
                    <div class="col-sm-1">
                      Eg: 2016 - 2017
                    </div>
                    </div>
                 
                  
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate()">Submit</button>
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
            <!--/Add or Create-->
                
        <!--/Admin Category Management-->
        

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>

        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >
function search_data() {
		var tbl_category_id = $("#tbl_category_id").val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/all_categories/";
		if(tbl_category_id !='')
			url += "tbl_category_id/"+tbl_category_id+"/";
		
			url += "offset/0/";
		window.location.href = url;
		<?php /*?>window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/enquiry/all_enquiries/is_not_replied/"+is_not_replied+"/tbl_court_id/"+tbl_court_id+"/tbl_category_id/"+tbl_category_id;<?php */?>
	}



	

</script>