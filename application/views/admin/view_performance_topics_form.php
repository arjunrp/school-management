          <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
          <link rel="stylesheet" href="<?=HOST_URL?>/css/jquery-ui.css">
          <style>
         .ui-slider-handle {
            width: 3em;
            height: 1.6em;
            top: 50%;
            margin-top: -.8em;
            text-align: center;
            line-height: 1.6em;
          }
          </style>
  
        <table width="100%" class="table table-bordered table-striped">
          <thead>
            <tr>
                <th width="50%" align="center" valign="middle">Topic</th>
                <th width="50%" align="center" valign="middle">Action</th>
            </tr> 
          </thead>
          <tbody>
         

          <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		  <script src="<?=HOST_URL?>/js/jquery-ui.js"></script>  


  
<?php	
	for($i=0; $i<count($cards); $i++) {
		$id                = $cards[$i]['id'];
		//$performance_value = isset($cards[$i]['performance_value'])? $cards[$i]['performance_value'] :'0';
?>		
		 <script>
		  $( function() {
			var handle = $( "#custom-handle<?=$id?>" );
			$( "#slider<?=$id?>" ).slider({
			  create: function() {
				handle.text( $( this ).slider( "value" ) );
			  },
			  slide: function( event, ui ) {
				handle.text( ui.value );
				$("#percentage_complete<?=$id?>").val( ui.value );
			  }
			});
		  } );
		  
		  $("#custom-handle<?=$id?>").html("<?=$cards[$i]['performance_value']?>");
		  $("#custom-handle<?=$id?>").css("left","<?=$cards[$i]['performance_value']?>%");
		  
		  </script>
          <tr>
		  <td>
          <div><?=$cards[$i]["title"]?><span style="float:right;"><?=$cards[$i]["title_ar"]?></span> </div>
         </td>
         <td >
          <div>
           <div style="float:left;padding-left:2px; padding-bottom:5px;">
            <div id="slider<?=$id?>" style="width:200px;" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content">  
            <div id="custom-handle<?=$id?>" class="ui-slider-handle ui-corner-all ui-state-default ui-state-focus" style="left:<?=$cards[$i]['performance_value']?>%;" ><?=$cards[$i]['performance_value']?></div>
            </div>
            </div>
            <input name="percentage_complete<?=$id?>" id="percentage_complete<?=$id?>" value="<?=$cards[$i]['performance_value']?>" size="2" maxlength="3" type="hidden" /> %
           <button class="btn btn-primary" type="button" onclick="add_student_performance('<?=$cards[$i]["id"]?>', '<?=$tbl_student_id?>', '<?=$tbl_class_id?>',  '<?=$tbl_school_id?>', '<?=$tbl_teacher_id?>' )">SAVE</button>
           </div>
            
          </td>
         </tr>
<?php
	}
?>
 </tbody>
          <tfoot>
          </tfoot>
        </table>
<script>


function add_student_performance(topic_id,tbl_student_id,tbl_class_id,tbl_school_id,tbl_teacher_id) {
		
		var topic_val = $("#percentage_complete"+topic_id).val();
		
		$("#pre-loader").show();
		var url_str = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/save_student_performance_rate";
       	$.ajax({
			type: "POST",
			url: url_str,
			data: {
				is_admin: "Y",
				topic_val: topic_val,
				topic_id: topic_id,
				tbl_student_id: tbl_student_id,
				tbl_class_id: tbl_class_id,
				tbl_teacher_id: tbl_teacher_id,
				tbl_school_id: tbl_school_id
			},
			success: function(data) {
				alert("Performance rate updated successfully.");
				$("#pre-loader").hide();
			}
		});
	}

</script>