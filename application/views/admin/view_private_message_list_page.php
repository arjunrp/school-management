<?php	
	//print_r($response_array);
	//exit();
	$data_rs = $response_array["messages"];
	//print_r($data_rs);
	//exit();

	$CI = & get_instance();
	$index = 0;
	for ($i=0; $i<count($data_rs); $i++) {

        $message_img = $data_rs[$i]['message_img'];
        $message_img_thumb = $data_rs[$i]['message_img_thumb'];
        $message_audio = $data_rs[$i]['message_audio'];
        $name = $data_rs[$i]['name'];
        $message = $data_rs[$i]['message'];
		$teacher_id = $data_rs[$i]['teacher_id'];
        $added_date = $data_rs[$i]['date'];
        $added_date = date("H:i - d/m/Y",strtotime($data_rs[$i]["added_date"]));
		
		$CI->load->model("model_teachers");
        $teacher_obj =  $CI->model_teachers->get_teacher_details($teacher_id);	
        $first_name = $teacher_obj[0]['first_name'];
        $last_name = $teacher_obj[0]['last_name'];
		$file_name_updated_teacher = $teacher_obj[0]['file_name_updated'];
		
		$url = IMG_PATH."/teacher/".$file_name_updated_teacher;
        if (trim($lan) == "ar") {
            $first_name = $teacher_obj[0]['first_name_ar'];	
            $last_name = $teacher_obj[0]['last_name_ar'];
		}

      		?>
        
        	<tr>
              <td align="left" valign="middle">
			  	<?php
              		if ($file_name_updated_teacher != ""){
				?>
              		<img src="<?=$url?>" />
				<?php					
					} else {
			  	?>
                	<img src="<?=IMG_PATH?>/person.png" />
				<?php					
					}
			  	?>

              </td>
              <td align="left" valign="middle"><?=$first_name." ".$last_name;?></td>
              <td align="left" valign="middle">
              	<?=$message?>
              </td>
              <td align="left" valign="middle">
			  <?php
              	if ($message_audio != "") {
				?>
                <a href="<?=$message_audio?>" target="_blank"><img src="<?=IMG_PATH?>/audio.png" style="max-height:100px" /></a>
                <?php					
				}
			  ?>
              <?php
              	if ($message_img != "") {
				?>
                <a href="<?=$message_img?>" target="_blank"><img src="<?=$message_img?>" style="max-height:100px" /></a>
                <?php					
				}
			  ?>
              </td>
              <td align="left" valign="middle"><?=$added_date?></td>
            </tr>
            
        <?
	}
	
	//print_r($arr_msg);
	//$arr["messages"] = $arr_msg;
	//echo json_encode($arr);
?>
