<?php ob_start(); 
?>
<html>
<link rel="stylesheet" href="<?=ADMIN_CSS_PATH?>print_id_card.css" type="text/css">
<link rel="stylesheet" href="<?=ADMIN_CSS_PATH?>id_card_style.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<body  id="printarea">
<?php
header('Content-Type: text/html; charset=utf-8');
for ($i=0; $i<count($rs_all_students); $i++) { 

		$id               = $rs_all_students[$i]["id"];
		$tbl_student_id   = $rs_all_students[$i]["tbl_student_id"];
		$first_name       = $rs_all_students[$i]["first_name"];
		$last_name        = $rs_all_students[$i]["last_name"];
		$first_name_ar    = $rs_all_students[$i]["first_name_ar"];
		$last_name_ar     = $rs_all_students[$i]["last_name_ar"];
		$mobile           = $rs_all_students[$i]["mobile"];
		$dob_month        = $rs_all_students[$i]["dob_month"];
		$dob_day          = $rs_all_students[$i]["dob_day"];
		$dob_year         = $rs_all_students[$i]["dob_year"];
		$gender           = $rs_all_students[$i]["gender"];
		$country          = $rs_all_students[$i]["country"];
		$pic              = $rs_all_students[$i]['file_name_updated'];
		$parent_name_en     = ucfirst($rs_all_students[$i]['parent_first_name'])." ".ucfirst($rs_all_students[$i]['parent_last_name']);
		$parent_name_ar     = $rs_all_students[$i]['parent_first_name_ar']." ".$rs_all_students[$i]['parent_last_name_ar'];
		
		if($pic<>"") // class="img-circle"
			$pic_path           =   '<img width="100" height="80" src="'.IMG_PATH_STUDENT.'/'.$pic.'"  />';
		else
			$pic_path           =   '<img width="100" height="80" src="'.IMG_PATH_STUDENT.'/no_img.png"  
										style="border-color:1px solid #7C858C !important;" />';

		if($gender =="male"){
			$gender  ="Male";
			$gender_ar ="ذكر";
		}else{
			$gender  ="Female";
			$gender_ar ="أنثى";
		}

		$email                  = isset($rs_all_students[$i]["email"])? $rs_all_students[$i]["email"] :"-";
		$file_name_updated      = $rs_all_students[$i]["file_name_updated"];
		$emirates_id_father     = $rs_all_students[$i]["emirates_id_father"];

		if($rs_all_students[$i]["emirates_id_mother"]<>""){ 
		 	$emirates_id_mother = $rs_all_students[$i]["emirates_id_mother"]; 
		}else{ 
			$emirates_id_mother = "Not mentioned"; 
		} 

		$class_name             = $rs_all_students[0]["class_name"];
		$class_name_ar          = $rs_all_students[0]["class_name_ar"];
		$section_name           = $rs_all_students[0]["section_name"];
		$section_name_ar        = $rs_all_students[0]["section_name_ar"];
		$user_id                = $rs_all_students[0]["user_id"];
		$password = base64_decode($rs_all_students[0]["pass_code"]);
		?>


<table width="400" class="tble" style="direction:ltr; background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #b9b063, #c9c087, #f7f7ef) repeat scroll 0 0; border-radius: 12px 12px 12px 12px; font-family:Verdana,Arial,Helvetica,sans-serif; font-size:12px; font-weight:normal;">
		  <tr style="background-color:#fff;">
			<td  colspan="2" align="center"><img src="<?=HOST_URL?>/admin/images/inner_header_logo.png" height="60"/></td>
		  </tr>
         
          
          <tr>
          <td colspan="2">
              <table width="100%">
               <tr>
                   <td width="100%">
                           <table width="100%" >
                              <tr>
                                <td colspan="2"><div class="txt_ar" dir="rtl" style="font-size:14px; font-weight:normal !important; float:right; text-align:right"> <strong>اسم</strong> :&nbsp;<strong>
                                    <?=$first_name_ar?>&nbsp;<?=$last_name_ar?></div></td>
                              </tr>
                
                              <tr>
                                    <td width="28%"><div class="txt_en" dir="ltr">Name </div></td>
                                    <td width="72%">:&nbsp;<?=$first_name?>&nbsp;<?=$last_name?></td>
                              </tr>
                    
                              <tr>
                                    <td> Gender </td>
                                    <td>:&nbsp;<?=$gender?></td>
                              </tr>
                
                              <tr>
                                <td colspan="2"><div class="txt_ar" dir="rtl" style="font-size:14px; font-weight:normal !important;"> جنس :&nbsp;<strong>
                                    <?=$gender_ar?>
                                    </strong></div>
                                </td>
                              </tr>
                         
                
                              <tr>
                                <td> Mobile No </td>
                                <td> :&nbsp;<?=$mobile?>
                                </td>
                              </tr>
                
                            <?php /*?>  <tr>
                                <td> Email Id </td>
                                <td> :&nbsp;<?=$email?>
                                </td>
                              </tr><?php */?>
                
                              <tr>
                                 <td> Nationality</td>
                                 <td> :&nbsp;<?=$country?></td> 
                              </tr>
                          </table>
                   </td>
                   <!--<td width="20%"><?=$pic_path?></td>-->
               </tr>
               </table>
           </td>
          </tr>
          <tr>
              <td colspan="2">
                  <table width="100%">
                      <tr>
                            <td width="33%"> Class</td>
                            <td width="67%">:&nbsp;<?=$class_name?> <?=$section_name?>  [::] <?=$class_name_ar?> <?=$section_name_ar?></td>
                      </tr>
                      <tr>
                        <td colspan="2"><div class="txt_ar" dir="rtl" style="font-size:14px; font-weight:normal; float:right; text-align:right"> <strong>اسم ولي الأمر</strong> :&nbsp;<strong>
                            <?=$parent_name_ar?></div></td>
                      </tr>
            
                      <tr>
                        <td><div class="txt_en" dir="ltr">Guardian Name </div></td>
                        <td>:&nbsp;<?=$parent_name_en?></td>
                      </tr>
        
					<?php /*?>  <tr>
                         <td> Emirates ID Father</td>
                         <td> :&nbsp;<?=$emirates_id_father?></td>
                      </tr>
            
                      <tr>
                        <td> Emirates ID Mother</td>
                        <td> :&nbsp;<?=$emirates_id_mother?></td>
                      </tr><?php */?>
        
                      <tr>
                        <td> User Id/Password </td>
                        <td> :&nbsp;<?=$user_id?>&nbsp;/&nbsp;<?=$password?></td>
                      </tr>
					  <?php /*?><tr>
                       <td>Student Picture (42 X 42):</td>
                       <td>:&nbsp;
                                  <?php if ($file_name_updated  != "") { ?>
                                  <img style="float:left" src="<?=IMG_PATH_STUDENT?>/<?=$file_name_updated?>" alt="<?=$name?>" title="" width="100" height="100">
                                  <?php } else {
                                          echo " Not Specified"	;
                                      }
                                 ?>                  
                          </td>
                          </tr><?php */?>
                       
                      <?php /*?><tr>
                        <td> Issued Date </td>
                        <td> :&nbsp;
                          <?=date("d/m/Y ",strtotime($added_date));?></td>
                      </tr><?php */?>
                  </table>
                </td>
            </tr>
		</table>
        <br>
        <?php } ?>
        </body>

</html>
<?php
$output = ob_get_clean( );
$html = trim($output);
/*PDF Code START*/
include("mpdf60/mpdf.php");
$mpdf = new mPDF('utf-8'); 
$mpdf = new mPDF('ar','A4','','',32,25,27,25,16,13); 
$mpdf->SetDirectionality('rtl');
$mpdf->mirrorMargins = true;
$mpdf->WriteHTML($html);
$mpdf->Output($pdf_path);
ob_end_flush();
exit;

?>