<?php header('Content-Type: text/html; charset=utf-8');?>
<?php
function get_random_color() {
	$colors = array('#00C0EF', '#F39C12', '#164852', '#00A65A', '#DD4B39', '#282E33', '#25373A', '#164852', '#495E67', '#FF3838');
	$count = count($colors) - 1;
	$i = rand(0, $count);
	$rand_color = $colors[$i];
	
return $rand_color;
}
?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Welcome to <?=APP_HEAD_SMALL?><small> School Application</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style="float:left; position:relative; top:0px">
      <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
      <li>Welcome</li>
    </ol>
    <!--/BREADCRUMB--> 
    <div style="clear:both"></div>
  </section>
  
  <!--<section class="content" style="min-height:90px !important"> 
                <div class="row">
                  <div class="col-lg-3 col-xs-6"> 
                    <div class="small-box bg-yellow">
                      <div class="inner">
                        <h3>Students</h3>
                       	<p>Total: 150</p>
                      </div>
                      <div class="icon"> <i class="ion ion-person-add"></i> </div>
                      <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> </div>
                  </div>
                  <div class="col-lg-3 col-xs-6"> 
                    <div class="small-box bg-aqua">
                      <div class="inner">
                        <h3>Teachers</h3>
                       	<p>Total: 30</p>
                      </div>
                      <div class="icon"> <i class="ion ion-bag"></i> </div>
                      <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> </div>
                  </div>
                  <div class="col-lg-3 col-xs-6"> 
                    <div class="small-box bg-green">
                      <div class="inner">
                        <h3>Result</h3>
                        <p>Exam - Passing Percentage: 98%</p>
                      </div>
                      <div class="icon"> <i class="ion ion-stats-bars"></i> </div>
                      <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> </div>
                  </div>
                  <div class="col-lg-3 col-xs-6"> 
                    <div class="small-box bg-red">
                      <div class="inner">
                        <h3>Teacher</h3>
                       	<p>Available Courses</p>
                      </div>
                      <div class="icon"> <i class="ion ion-pie-graph"></i> </div>
                      <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> </div>
                  </div>
                </div>
  </section>-->

  <section class="content"> 
    <!--WORKING AREA-->	
           <!--Listing-->
             <div id="mid1" class="box">
                <div class="box-header">
                  <h3 class="box-title">Messages</h3>
                  <div class="box-tools">
                   
                  </div>
                </div>
                
                <div class="box-body">
                  <table width="100%" class="table table-bordered table-striped" id="example1">
                    <thead>
                    <tr>
                      <th width="5%" align="center" valign="middle">Ser No.</th>
                      <th width="5%" align="center" valign="middle">&nbsp;</th>
                      <th align="center" valign="middle">Message</th>
                      <th width="20%" align="center" valign="middle">Date</th>
                      </tr>
                     
                    </thead>
                    <tbody>
                    <?php
                        for ($i=0; $i<count($data_messages); $i++) { 
                            $id = $data_messages[$i]['id'];
                            $message = $data_messages[$i]['message'];
                            $added_date = $data_messages[$i]['added_date'];
                            $is_active = $data_messages[$i]['is_active'];
                            
                            $added_date = date('m-d-Y',strtotime($added_date));
							
							$char = strtoupper(mb_substr($message,0,1,'utf-8'));
                    ?>
                    <tr>
                      <td align="center" valign="middle"><?=$i+1?></td>
                      <td align="left" valign="middle"><div class="score_box" style="background-color:<?=get_random_color();?>"><?=$char?></div></td>
                      <td align="left" valign="middle"><?=$message?></td>
                      <td align="left" valign="middle"><?=$added_date?></td>
                      </tr>
                    <?php } ?>
                    <tr>
                      <td colspan="4" align="right" valign="middle">
                      <?php //echo $this->pagination->create_links(); ?>
                      </td>
                    </tr>
                    <?php 
                        if (count($data_messages)<=0) {
                    ?>
                    <tr>
                      <td colspan="4" align="center" valign="middle">
                      <div class="alert alert-warning alert-dismissible" style="width:50%">
                        <button aria-hidden="true" data_messages-dismiss="alert" class="close" type="button">×</button>
                        <h4><i class="icon fa fa-info"></i> Information!</h4>
                        There are no messages. 
                      </div>                                
                      </td>
                    </tr>
                    <?php   
                        }
                    ?>
                    </tbody>
                    <tfoot>
                    </tfoot>
                  </table>
                </div>
            </div>        
            <!--/Listing-->
    
    <!--/WORKING AREA--> 
  </section>  
  
  
</div>