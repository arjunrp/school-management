<?php
//Init Parameters
//$admin_user_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}
?>

<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkboxModule').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkboxModule').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkboxModule').on('click',function(){
			if($('.checkboxModule:checked').length == $('.checkboxModule').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/admin_user/all_users";
			}
		})
	});
	

	function ajax_update() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/admin_user/save_admin_rights",
			data: {
				admin_users_allowed: $("input[id='admin_users_allowed']:checked").serialize(),
				tbl_admin_user_id: $('#admin_user_id_enc').val(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Changes saved successfully.", 'green');
				
				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	
	function ajax_validate() {
		if ( validate_module() == false) {
			return false;
		} else {
			ajax_update();
		}
	} 

	function validate_module() {
		
		var len = $("input[id='admin_users_allowed']:checked").length;
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more module(s)", 'green');
		return false;	
		}
		return true;
	
	}

	
</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> <?php if(LAN_SEL=="ar"){?> إدارة مشرف المستخدمين <?php }else{ ?> Admin Users <small> Management</small>  <?php }?></h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i><?php if(LAN_SEL=="ar"){?> الرئيسية <?php }else{?> Home <?php }?></a></li>
      <li><?php if(LAN_SEL=="ar"){?> الاداريين المستخدمين <?php } else{ ?> Admin Users <?php } ?> </li>
    </ol>
    
     <div style=" float:right; padding-right:10px;"> <button onclick="show_admin_users()" title="Admin Users" type="button" class="btn btn-primary">Admin Users</button></div>

    <!--/BREADCRUMB--> 
    <div style="clear:both"></div>
  </section>
  <script>
   function show_admin_users()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/admin_user/all_users/";
		window.location.href = url;
	}
	
  </script>
  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {
			$tbl_admin_user_id 		 = $admin_user_obj[0]['tbl_admin_id'];
			$first_name 				= $admin_user_obj[0]['first_name'];
			$last_name 				 = $admin_user_obj[0]['last_name'];
			$dob 					   = $admin_user_obj[0]['dob'];
			$gender                    = $admin_user_obj[0]['gender'];
			$mobile                    = $admin_user_obj[0]['phone'];
			$picture                   = $admin_user_obj[0]['picture'];
			$email                     = $admin_user_obj[0]['email'];
			$username                  = $admin_user_obj[0]['user_id'];
			$password 				  = $admin_user_obj[0]['password'];
			$salt                      = $admin_user_obj[0]['salt'];
			$ip                        = $admin_user_obj[0]['ip'];
			$added_date                = $admin_user_obj[0]['reg_date'];
			$is_active                 = $admin_user_obj[0]['is_active'];
	?>
        <!--Edit-->
<!--File Upload START-->
<link href="http://hayageek.github.io/jQuery-Upload-File/uploadfile.min.css" rel="stylesheet">

<style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url('<?=IMG_PATH?>/delete.jpg');
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
		clear:both;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
	#overlay_container {
		position:relative;
	}
	#overloading {
		background-image:url('<?=IMG_PATH?>/preloader/preloader_2.gif');
		background-repeat:no-repeat;
		background-position:center;
		background-color:#CCC;
		position:absolute;
		left:0px;
		top:0px;
		opacity: 0.3;
		z-index: 10000;
	}
	#div_listing_container {
		display:none;	
	}
	.d_d_text {
		color:#745156;
		font-size:20px;
			
	}
	.ajax-upload-dragdrop {
		margin:auto;
		margin-bottom:10px;
		width:700px !important;
	}
	.ajax-file-upload-statusbar {
		margin:auto;
		margin-top:10px;
	}
	.ajax-file-upload {
		height:31px;
	}
</style>
 
<script src="http://hayageek.github.io/jQuery-Upload-File/jquery.uploadfile.min.js"></script>

<script language="javascript">

var item_id = "<?=$tbl_admin_user_id?>";//Primary Key for a Form. 

function set_item_id(obj) {
	item_id = obj.value;
	get_files();	
}

$(document).ready(function() {
	var uploadObj = $("#advancedUpload").uploadFile({
		url:"<?=HOST_URL?>/file_mgmt/upload_the_file",
		multiple:true,
		autoSubmit:true,
		maxFileSize:130000,
		fileName:"myfile",
		formData: {"module_name":"admin_user"},
		dynamicFormData: function() {
			var data = { item_id:item_id}
			return data;
		},
		showStatusAfterSuccess:false,
		dragDropStr: "<span class='d_d_text'>Optionally Drag and Drop the File to Upload.</span>",
		abortStr:"Abourt",
		cancelStr:"Cancel",
		doneStr:"Done",
		multiDragErrorStr: "Multi Drag Error.",
		extErrorStr:"Extention Error:",
		sizeErrorStr:"Max Size Error:",
		uploadErrorStr:"Upload Error",
		onSelect:function(files) {
 		},
		onSubmit:function(files) {
 		},
		onSuccess:function(files, data, xhr) {
			if (data == "error") {
				alert("Error uploading file. Please try again.");
				return;
			}
			var obj = JSON.parse(data);
			var tbl_uploads_id = obj.tbl_uploads_id;
			var file_name_updated = obj.file_name_updated;
			
			//alert("tbl_uploads_id: "+tbl_uploads_id)
			//alert("file_name_updated: "+file_name_updated)
			add_uploaded_item(tbl_uploads_id, file_name_updated);
		},
		afterUploadAll:function() {
 		},
		onError: function(files, status, errMsg) {
 		}
	});

	$("#startUpload").click(function() {
		uploadObj.startUpload();
	});
	
	try { 
		$('input[type=file]').click();
	} catch(e) {
		alert(e)
	}
});

//Function called when file is uploaded
function add_uploaded_item(tbl_uploads_id, file_name_updated) {
	var str = "<div id='"+tbl_uploads_id+"' class='box-header with-border'> <div class='box-title'><img src='<?=HOST_URL?>/admin/uploads/"+file_name_updated+"' /></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_img_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
		
	$("#div_listing_container").show();
	$("#div_listing_container").append(str);
	$(".ajax-upload-dragdrop").hide();//Hide the upload button
return;
}

function confirm_delete_img_popup(tbl_uploads_id) {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		$('#'+tbl_uploads_id).hide();	
		$(".ajax-upload-dragdrop").show();
		
		var url_str = "<?=HOST_URL?>/file_mgmt/delete_file";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					tbl_uploads_id: tbl_uploads_id
				},
			success: function(data) {
				$("#pre-loader").hide();
			}
		});	
	} else {
		$("#pre-loader").hide();		
	}
}

function get_files() {
	var url_str = "<?=HOST_URL?>/misc/get_files.php";
	
	$.ajax({
		type: "POST",
		url: url_str,
		data: {
				module_name: "admin_user",
				show_del: "Y",
				item_id: item_id//global variable
			},
		success: function(data) {
			$('#div_listing_container').show();
			$('#div_listing_container').html(data)
			
		}
	});	
}
</script>
<!--File Upload END-->

            <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">User Rights</h3>
                  
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_edit" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                  
                  
                 <div class="form-group">
                      <label class="col-sm-2 control-label" for="user_rights">Modules List</label>
                      <br />
                      <div class="col-sm-10">
                      		<div id="div_rights">
                            
                            <div class="col-sm-10" style="padding-bottom:10px;">   <input type="checkbox" value="" id="select_all">&nbsp;&nbsp;&nbsp;&nbsp;Select All</div>
                            
                            
                           <?php for($b=0;$b<count($backend_modules_rights);$b++) { ?> 
                           
                                  <?php if($backend_modules_rights[$b]['module_type'] == "Parent") {
                                                  $class = 'col-sm-10';
                                   }else{ 
                                                  $class = 'col-sm-5';
                                   } ?>
                                  <div class="<?=$class?>" style="padding-bottom:10px;">  
         <input id="admin_users_allowed" name="admin_users_allowed" type="checkbox" value="<?=$backend_modules_rights[$b]['tbl_module_id']?>"  <?=$backend_modules_rights[$b]['check_status']?> class="checkboxModule" >
         &nbsp;&nbsp;<?php /*?><?=$backend_modules_rights[$b]['module_cat_name']?>&nbsp;[::]&nbsp;<?php */?>
							                   <?php if($backend_modules_rights[$b]['module_type'] == "Parent") { ?>
                                               <strong> <?=$backend_modules_rights[$b]['module_name']?></strong>
                                               <?php }else{ ?>
                                                     <?=$backend_modules_rights[$b]['module_name']?>   
                                               <?php } ?>
                           </div>
                           <?php } ?>
                           
							</div>                                
                      </div>
                    </div>
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate()">Save Changes</button>
                    <input type="hidden" name="admin_user_id_enc" id="admin_user_id_enc" value="<?=$admin_user_id_enc?>" />
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
		        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/admin_user/all_users";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>   
<!--File Upload START-->
<link href="http://hayageek.github.io/jQuery-Upload-File/uploadfile.min.css" rel="stylesheet">

<style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url('<?=IMG_PATH?>/delete.jpg');
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
		clear:both;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
	#overlay_container {
		position:relative;
	}
	#overloading {
		background-image:url('<?=IMG_PATH?>/preloader/preloader_2.gif');
		background-repeat:no-repeat;
		background-position:center;
		background-color:#CCC;
		position:absolute;
		left:0px;
		top:0px;
		opacity: 0.3;
		z-index: 10000;
	}
	#div_listing_container {
		display:none;	
	}
	.d_d_text {
		color:#745156;
		font-size:20px;
			
	}
	.ajax-upload-dragdrop {
		margin:auto;
		margin-bottom:10px;
		width:700px !important;
	}
	.ajax-file-upload-statusbar {
		margin:auto;
		margin-top:10px;
	}
	.ajax-file-upload {
		height:31px;
	}
</style>
 
<script src="http://hayageek.github.io/jQuery-Upload-File/jquery.uploadfile.min.js"></script>

<script language="javascript">


function ()
{
		var len = $("input[id='admin_user_id_enc']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more user(s)", 'green');
		return;	
		}
	
}



function confirm_delete_img_popup(tbl_uploads_id) {
	
	
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		
		$('#'+tbl_uploads_id).hide();	
		$(".ajax-upload-dragdrop").show();
		
		var url_str = "<?=HOST_URL?>/file_mgmt/delete_file";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					tbl_uploads_id: tbl_uploads_id
				},
			success: function(data) {
				$("#pre-loader").hide();
			}
		});	
	} 
}

function get_files() {
	var url_str = "<?=HOST_URL?>/misc/get_files.php";
	
	$.ajax({
		type: "POST",
		url: url_str,
		data: {
				module_name: "admin_user",
				show_del: "Y",
				item_id: item_id//global variable
			},
		success: function(data) {
			$('#div_listing_container').show();
			$('#div_listing_container').html(data)
			
		}
	});	
}
</script>
<!--File Upload END-->

                      <div id="mid1" class="box box-success">
                        <div class="box-header">
                          <div class="col-sm-1" >
                          <h3 class="box-title">SEARCH</h3>
                          </div>
                          <div class="col-sm-11"> 
                               <div class="col-sm-6"><input name="q" id="q" value="<?=urldecode($q)?>" type="text" class="form-control" placeholder="Search By Name, Email etc."   > </div>
                               <div class="col-sm-2"><button class="btn btn-success" type="button" onclick="search_data()">Search</button>&nbsp;<button class="btn btn-success" type="button" 
                               onclick="reset_data();">Reset</button>
                               </div>
                           
                          </div>
                        </div>  
                     </div>   
            
            
            
            <!--Listing-->
                    <div id="mid1" class="box">
                        <div class="box-header">
                          <h3 class="box-title"><?php if(LAN_SEL=="ar"){?> الاداريين المستخدمين <?php } else{ ?> Admin Users <?php } ?></h3>
                          <div class="box-tools">
                            <?php if (count($rs_all_users)>0) { echo $paging_string;}?>	
                            <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
                            <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>
                          </div>
                        </div>
                        
                        <div class="box-body">
                          <table width="100%" class="table table-bordered table-striped" id="example1">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle"><input id="select_all" type="checkbox" value="" /></th>
                              <th width="10%" align="center" valign="middle"><?php if(LAN_SEL=="ar"){?> الرقم المسلسل <?php } else{ ?> Sl No.<?php } ?></th>
                              <th width="25%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y"><?php if(LAN_SEL=="ar"){?> اسم <?php } else{ ?> Name <?php } ?><?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?><div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>
                              <th width="25%" align="center" valign="middle"> <?php if(LAN_SEL=="ar"){?> البريد الإلكتروني <?php } else{ ?> Email <?php } ?></th>
                              <th width="15%" align="center" valign="middle"> <?php if(LAN_SEL=="ar"){?> تاريخ <?php } else{ ?> Date <?php } ?></th>
                              <th width="10%" align="center" valign="middle"> <?php if(LAN_SEL=="ar"){?> الحالة <?php } else{ ?> Status <?php } ?></th>
                              <th width="10%" align="center" valign="middle"> <?php if(LAN_SEL=="ar"){?> إجراء <?php } else{ ?> Action <?php } ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                                for ($i=0; $i<count($rs_all_users); $i++) { 
                                    $id = $rs_all_users[$i]['id'];
                                    $tbl_admin_user_id = $rs_all_users[$i]['tbl_admin_id'];
                                    $first_name = $rs_all_users[$i]['first_name'];
                                    $last_name = $rs_all_users[$i]['last_name'];
                                    $dob = $rs_all_users[$i]['dob'];
                                    $gender = $rs_all_users[$i]['gender'];
                                    $mobile = $rs_all_users[$i]['mobile'];
                                    $picture = $rs_all_users[$i]['picture'];
                                    $email = $rs_all_users[$i]['email'];
                                    $password = $rs_all_users[$i]['password'];
                                    $salt = $rs_all_users[$i]['salt'];
                                    $ip = $rs_all_users[$i]['ip'];
                                    $last_login = date("d M, Y",strtotime($rs_all_users[$i]['last_login']));
                                    $added_date = date("d M, Y",strtotime($rs_all_users[$i]['reg_date']));
                                    $is_active = $rs_all_users[$i]['is_active'];
                                    $user_type = $rs_all_users[$i]['user_type'];
                                    
                                    $first_name = ucfirst($first_name);
                                    $last_name = ucfirst($last_name);
                                    
                            ?>  
							<?php if(LAN_SEL=="ar"){ $tbAlign = "right"; $paginationAlign="left"; }else{ $tbAlign = "left"; $paginationAlign="right";  } ?>
                            <tr>
                              <td align="<?=$tbAlign?>" valign="middle">
                              	
								
								<?php
							
								
                                	if (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "S" ) { 
								?>
                                <input id="admin_user_id_enc" name="admin_user_id_enc" class="checkbox" type="checkbox" value="<?=$tbl_admin_user_id?>" />
                                <?php
									}
								?>
                              
                                
                              </td>
                              <td align="<?=$tbAlign?>" valign="middle"><?=$offset+$i+1?></td>
                              <td align="<?=$tbAlign?>" valign="middle"><?=$first_name?> <?=$last_name?></td>
                              <td align="<?=$tbAlign?>" valign="middle"><?=$email?></td>
                              <td align="<?=$tbAlign?>" valign="middle"><?=$added_date?></td>
                              <td align="<?=$tbAlign?>" valign="middle">
                                <div id="act_deact_<?=$tbl_admin_user_id?>">
                                <?php if (trim($is_active) == "Y") { ?>
                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_admin_user_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                <?php } else { ?>
                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_admin_user_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                <?php } ?>
                                </div>
                              </td>
                              <td align="<?=$tbAlign?>" valign="middle">
                                <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/admin_user/edit_user/admin_user_id_enc/<?=$tbl_admin_user_id?>"><button class="btn bg-purple fa fa-pencil" type="button" title="Edit"></button></a>
                              </td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="7" align="<?=$paginationAlign?>" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>

							<?php 
                                if (count($rs_all_users)<=0) {
                            ?>
                            <tr>
                              <td colspan="7" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no users available. Click on the + button to create one.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
    
            <!--Add or Create-->
              <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Create User</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                  
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="first_name">First Name</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="First Name" id="first_name" name="first_name" class="form-control">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="last_name">Last Name</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Last Name" id="last_name" name="last_name" class="form-control">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="dob">Date of Birth</label>
    
                      <div class="col-sm-10">
                         <input type="text" data-mask="" id="dob" name="dob" data-inputmask="'alias': 'dd/mm/yyyy'" class="form-control">
                      </div>
                    </div>
                    
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="gender">Gender</label>
    
                      <div class="col-sm-10">
                        <label>
                          <input type="radio" id="gender" name="gender" value="male" class="minimal" checked>
                          Male
                        </label>
                        &nbsp;
                        <label>
                          <input type="radio" id="gender" name="gender" value="female" class="minimal">
                          Female
                        </label>
                      </div>
                    </div>
    
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="mobile">Mobile</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Mobile" id="mobile" class="form-control">
                      </div>
                    </div>
                    
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="email">Email</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Email" id="email" class="form-control" >
                      </div>
                    </div>
    
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="picture">Picture</label>
    
                      <div class="col-sm-10">
                        <!--File Upload START-->
                            <style>
                            #advancedUpload {
                                padding-bottom:0px;
                            }
                            </style>
                                 
                            <div id="advancedUpload">Upload File</div>
                            
                            <div id="uploaded_items" >
                                <div id="div_listing_container" class="listing_container">	            
                                </div>        
                            </div>
                        <!--File Upload END-->
                      </div>
                    </div>
                    
                    <hr />                
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="username">Username</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Username" id="username" class="form-control">
                      </div>
                    </div>
    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="password">Password</label>
    
                      <div class="col-sm-10">
                        <input type="password" placeholder="Password" id="password" class="form-control">
                      </div>
                    </div>
    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="confirm_password">Confirm Password</label>
    
                      <div class="col-sm-10">
                        <input type="password" placeholder="Confirm Password" id="confirm_password" class="form-control">
                      </div>
                    </div>
    
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate()">Create User</button>
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
            <!--/Add or Create-->
                
        <!--/Admin User Management-->

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>

        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript">
function search_data() {
		var q = $("#q").val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/admin_user/all_users/";
		if(q !='')
			url += "q/"+q+"/";
		
			url += "offset/0/";
		window.location.href = url;
		<?php /*?>window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/enquiry/all_enquiries/is_not_replied/"+is_not_replied+"/tbl_court_id/"+tbl_court_id+"/tbl_category_id/"+tbl_category_id;<?php */?>
	}

function reset_data() {
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/admin_user/all_users/";
		url += "offset/0/";
		window.location.href = url;
	}





$(function () {
	$("#dob").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});	
});
</script>