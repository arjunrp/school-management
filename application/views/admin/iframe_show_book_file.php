<?php error_reporting(0); ?>
<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--><html class="no-js" lang="en"><!--<![endif]-->
<script src="<?=HOST_URL?>/assets/js/jquery-1.11.1.min.js"></script>
<head>

<style>
body {
	margin: 0 auto;
	background-image:none;
}
</style>

<script language="javascript">
	function uploadBookFile() {
		   document.frm_book_file.submit();
	}
	function resizeIframe(iframeID) {
		var iframe = window.parent.document.getElementById(iframeID);
		var body_offsetHeight = document.body.offsetHeight;
	
		iframe.style.height = body_offsetHeight + "px";
	}
</script>
</head>
<body onload="resizeIframe('iframe_show_book_file')">
<form name="frm_book_file" enctype="multipart/form-data" method="post" action="<?=HOST_URL?>/<?=LAN_SEL?>/admin/books/book_file_upload/">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-color:#f4f1f1;">
  <tr>
    <td width="70%" align="left" valign="middle">
     <?php if($book_file<>""){ ?>
     <i class="fa fa-file-pdf-o" style="font-size:48px;color:red"></i>
        <?php if($book_file_name<>"") {?>
                              <div id="divFile" >
                              <img src="<?=IMG_PATH?>/pdf_icon.jpg" />
							  </div>
							 <?php } ?>
     
      <?php /*?><img src="<?=HOST_URL?>/uploads/books/<?=$book_file?>" width="93" height="90" style="padding:10px" /><?php */?>
      <?php } ?>
      <label for="fileField"></label>
      <input type="file" name="book_file" id="book_file" onChange="uploadBookFile()">
    </td>
    <?php /*?> <input type="hidden" name="book_file" class="contact-subject" id="book_file" value="<?=$book_file?>" ><?php */?>
     
     <input type="hidden" name="tbl_book_id" class="contact-subject" id="tbl_book_id" value="<?=$unique_id?>" >
    <td align="left" valign="middle" style="color:#CC0000"><?php echo $MSG;?></td>
  </tr>
</table>
</form>
<?php if($book_file<>""){ ?>
<script>
$(document).ready(function(){
   window.parent.$("#book_file").val('<?=$book_file?>');
   window.parent.$("#divFile").hide();
});
</script>
<?php } ?>
</body>
</html>

