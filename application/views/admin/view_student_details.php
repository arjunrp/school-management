<script language="javascript">
	function ajax_save_notes() {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/user/ajax_save_notes",
			data: {
				user_id_enc:"<?=$tbl_user_id?>",
				notes: $('#notes').val(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Notes saved successfully.", 'green');
				
				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
</script>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Student <small> Details</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style="float:left; position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home"><i class="fa fa-home"></i>Home</a></li>
      <li>Student Details</li>
    </ol>
    <!--/BREADCRUMB--> 
    <div class="box-tools" style="float:right;">
        <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/all_students"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
    </div> 
    <div style="clear:both"></div>
  </section>
  
  <section class="content"> 
  
    <!--WORKING AREA-->	
    
        <div class="row">
            <div class="col-md-3">
               <?php 
                    $name          		   = $student_obj[0]['first_name']." ".$student_obj[0]['last_name'];
					$name_ar       			= $student_obj[0]['first_name_ar']." ".$student_obj[0]['last_name_ar'];
					$mobile        		 = $student_obj[0]['mobile'];
		            $dob_month     		  = $student_obj[0]['dob_month'];
		            $dob_day       			= $student_obj[0]["dob_day"];
		            $dob_year               = $student_obj[0]["dob_year"];
		            $gender                 = ucfirst($student_obj[0]["gender"]);
					
		            $email                  = $student_obj[0]["email"];
		            $country                = $student_obj[0]["country"];
		            $tbl_class_id           = $student_obj[0]["tbl_class_id"];
		            $picture                = $student_obj[0]["file_name_updated"];
		            $added_date             = $student_obj[0]["added_date"];
		            $is_active              = $student_obj[0]["is_active"];
					$student_emirates_id 	= $student_obj[0]["student_emirates_id"];
					$student_user_id 		= $student_obj[0]["student_user_id"];
					$student_pass_code 		= base64_decode($student_obj[0]["student_pass_code"]);
					
					
					
		            $emirates_id_father     = $student_obj[0]["emirates_id_father"];
		            $emirates_id_mother     = $student_obj[0]["emirates_id_mother"];
                    $parent_name             = $student_obj[0]["parent_first_name"]." ".$student_obj[0]["parent_last_name"];
		            $parent_name_ar          = $student_obj[0]["parent_first_name_ar"]." ".$student_obj[0]["parent_last_name_ar"];
		            $parent_gender 		   = ucfirst($student_obj[0]["parent_gender"]);
					if($parent_gender=="Male")
					{
						$parent_gender = "Father";
					}else if($gender=="Female"){
						$parent_gender = "Mother";
					}
		            $parent_user_id 		  = $student_obj[0]["parent_user_id"];
					$parent_pass_code 		= base64_decode($student_obj[0]["parent_pass_code"]);
					
					$parent_emirates_id 	  = $student_obj[0]["parent_emirates_id"];
					$parent_mobile 	       = $student_obj[0]["parent_mobile"];
					$parent_email 	        = $student_obj[0]["parent_email"];
					
					$school_type 	         = $student_obj[0]["school_type"];
					$school_type_ar 	      = $student_obj[0]["school_type_ar"];
					
					if($student_obj[0]["parent_dob_year"]<>""){
						$parent_dob 	          = $student_obj[0]["parent_dob_year"]."-".$student_obj[0]["parent_dob_month"]."-".$student_obj[0]["parent_dob_day"];
						$parent_dob             = date('M d, Y',strtotime($parent_dob));
					}else{
						$parent_dob              =  "NA";
					}
		            $parent_password 		 = base64_decode($student_obj[0]["parent_password"]);

		            $class_name              = $student_obj[0]["class_name"]." ".$student_obj[0]["section_name"];
		            $class_name_ar           = $student_obj[0]["class_name_ar"]." ".$student_obj[0]["section_name_ar"];

                    if($student_obj[0]["dob_year"]<>""){
						$dob_date                =  $dob_year."-".$dob_month."-".$dob_day;
                    	$dob_date                = date('M d, Y',strtotime($dob_date));
					}else{
						$dob_date                = "NA";
					}

					if($picture<>"")
					{
						$picture_path = IMG_PATH_STUDENT."/".$picture;
					}else{
						$picture_path = IMG_PATH_STUDENT."/no_img.png";
					}
					
            	    ?>
                    <!--Profile Image-->
                    <div class="box box-primary">
                      <div class="box-body box-profile"> 
                      	<?php
                        if (trim($picture)=="") {
						?>
    	                    <img class="profile-user-img img-responsive" src="<?=$picture_path?>" alt="User profile picture">
                        <?php }  else { ?>
	                        <a href="<?=$picture_path?>" target="_blank"><img class="profile-user-img img-responsive" src="<?=$picture_path?>" alt="User profile picture"></a>
						<?php } ?>
                        <h3 class="profile-username text-center"><?=$name_ar?> <br /> <?=$name?> </h3>
                        <p class="text-muted text-center"><?=$class_name_ar?>&nbsp;/&nbsp<?=$class_name?> </p>
                        <p class="text-muted text-center"><?=$school_type_ar?>&nbsp;/&nbsp<?=$school_type?> </p>
                        
                        <ul class="list-group list-group-unbordered">
                          <li class="list-group-item"> <b>Gender</b> <a class="pull-right"><?=$gender?></a> </li>
                          <li class="list-group-item"> <b>DOB</b> <a class="pull-right"><?=$dob_date?></a> </li>
                          <li class="list-group-item"> <b>Nationality</b> <a class="pull-right"><?=$country?></a> </li>
                          <li class="list-group-item"> <b>Mobile</b> <a class="pull-right"><?=$mobile?></a> </li>
                          <li class="list-group-item"> <b>Email</b> <a class="pull-right"><?=$email?></a> </li>
                          <li class="list-group-item"> <b>Student Emirates Id</b> <a class="pull-right"><?=$student_emirates_id?></a> </li>
                          <li class="list-group-item"> <b>Student User Id</b> <a class="pull-right"><strong><?=$student_user_id?></strong></a> </li>
                          <li class="list-group-item"> <b>Student Password</b> <a class="pull-right"><strong><?=$student_pass_code?></strong></a> </li>
                          <li class="list-group-item">&nbsp;</li>
                        
                          <li class="list-group-item"> <b>Parent Name [Ar]</b> <a class="pull-right"><?=$parent_name_ar?></a> </li>
                          <li class="list-group-item"> <b>Parent Name [En]</b> <a class="pull-right"><?=$parent_name?> </a> </li>
                          <li class="list-group-item"> <b>Relationship</b> <a class="pull-right"><?=$parent_gender?></a> </li>
                          <li class="list-group-item"> <b>DOB</b> <a class="pull-right"><?=$parent_dob?></a> </li>
                          <li class="list-group-item"> <b>Mobile</b> <a class="pull-right"><?=$parent_mobile?></a> </li>
                          <li class="list-group-item"> <b>Parent Emirates Id</b> <a class="pull-right"><?=$parent_emirates_id?></a> </li>
                          <li class="list-group-item"> <b>Parent User Id</b> <a class="pull-right"><strong><?=$parent_user_id?></strong></a> </li>
                          <li class="list-group-item">  <b>Parent Password</b> <a class="pull-right"><strong><?=$parent_pass_code?></strong></a> </li>
                        </ul>
                      </div>
                    </div>
                    <!--/Profile Image--> 
                
            </div>
            <div class="col-md-9">
				<!--TABS-->
                        <div class="nav-tabs-custom">
                          <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_education" data-toggle="tab"><i class="fa fa-tasks margin-r-5"></i>Points</a></li>
                            <li><a href="#tab_experience" data-toggle="tab"><i class="fa fa-list margin-r-5"></i>Cards</a></li>
                            <li><a href="#tab_course" data-toggle="tab"><i class="fa fa-file margin-r-5"></i>Records</a></li>
                            <li><a href="#tab_jobs_applied_for" data-toggle="tab"><i class="fa fa-comments margin-r-5"></i>Message</a></li>
                          <!--  <li><a href="#tab_courses_applied_for" data-toggle="tab"><i class="fa fa-map-o margin-r-5"></i>Courses Applied For</a></li>
                            <li><a href="#tab_attachments" data-toggle="tab"><i class="fa fa-file-archive-o margin-r-5"></i>Attachments</a></li>
                            <li><a href="#tab_notes" data-toggle="tab"><i class="fa fa-sticky-note-o margin-r-5"></i>Notes</a></li>-->
                          </ul>
                          <div class="tab-content">
                                <div class="active tab-pane" id="tab_education">
                                        <!--Listing-->
                                                <div id="div_tab1" class="box box-success">
                                                    <div class="box-body">
                                                      <table width="100%" class="table table-bordered table-striped" id="example1">
                                                        <thead>
                                                        <tr>
                                                          <th width="25%" align="center" valign="middle">Point Information</th>
                                                          <th width="10%" align="center" valign="middle">Points</th>
                                                          <th width="35%" align="center" valign="middle">Added By</th>
                                                          <th width="10%" align="center" valign="middle">Date</th>
                                                          <th width="20%" align="center" valign="middle">Class</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        
                                                        <?php
                                                            for ($e=0; $e<count($points_obj); $e++) {
                                                                $id_e                     = $points_obj[$e]['id'];
                                                                $comments_student         = $points_obj[$e]['comments_student'];
                                                                $points_student           = $points_obj[$e]['points_student'];
                                                                $class_name               = $points_obj[$e]['class_name']." ".$points_obj[$e]['section_name'];
                                                                $class_name_ar            = $points_obj[$e]['class_name_ar']." ".$points_obj[$e]['section_name_ar'];
                                                                $name                     = $points_obj[$e]['first_name']." ".$points_obj[$e]['last_name'];
                                                                $name_ar                  = $points_obj[$e]['first_name_ar']." ".$points_obj[$e]['last_name_ar'];
																$added_date               = date("M d, Y H:i a", strtotime($points_obj[$e]['added_date']));
                                                        ?>
                                                        <tr>
                                                          <td align="left" valign="middle"><?=$comments_student?></td>
                                                          <td align="left" valign="middle"><?=$points_student?></td>
                                                          <td align="left" valign="middle"><?=$name?><div class="pull-right"><?=$name_ar?></div></td>
                                                          <td align="left" valign="middle"><?=$added_date?></td>
                                                          <td align="left" valign="middle"><?=$class_name?><div class="pull-right"><?=$class_name_ar?></div></td>
                                                          <td align="left" valign="middle"><?=$specialization_str?></td>
                                                        </tr>
                                                        <?php } ?>
                            
                                                        <?php 
                                                            if (count($points_obj)<=0) {
                                                        ?>
                                                        <tr>
                                                          <td colspan="7" align="center" valign="middle">
                                                          <div class="alert alert-warning alert-dismissible" style="width:50%">
                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                            <h4><i class="icon fa fa-info"></i> Information!</h4>
                                                            The point details are not available.
                                                          </div>                                
                                                          </td>
                                                        </tr>
                                                        <?php } ?>
                                                       
                                                        </tbody>
                                                        <tfoot>
                                                        </tfoot>
                                                      </table>
                                                  </div>
                                                </div>        
                                        <!--/Listing-->
                                </div>
                                
                               

                                <div class="tab-pane" id="tab_experience">
                                        <!--Listing-->
                                                <div id="div_tab2" class="box box-success">
                                                    <div class="box-body">
                                                    <table width="100%" class="table table-bordered table-striped" id="example1">
                                                        <thead>
                                                        <tr>
                                                          <th width="20%" align="center" valign="middle">Card Title</th>
                                                          <th width="10%" align="center" valign="middle">Type(Issue/Cancel)</th>
                                                          <th width="10%" align="center" valign="middle">Points</th>
                                                          <th width="20%" align="center" valign="middle">Added By</th>
                                                          <th width="10%" align="center" valign="middle">Date</th>
                                                          <th width="20%" align="center" valign="middle">Class</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        
                                                        <?php
                                                            for ($e=0; $e<count($cards_obj); $e++) {
                                                                $id_e                     = $cards_obj[$e]['id'];
																$card_name                = $cards_obj[$e]['category_name_en'];
																$card_name_ar             = $cards_obj[$e]['category_name_ar'];
                                                                $comments_student         = $cards_obj[$e]['card_issue_type'];
                                                                $points_student           = $cards_obj[$e]['card_point'];
                                                                $class_name               = $cards_obj[$e]['class_name']." ".$points_obj[$e]['section_name'];
                                                                $class_name_ar            = $cards_obj[$e]['class_name_ar']." ".$points_obj[$e]['section_name_ar'];
                                                                $name                     = $cards_obj[$e]['first_name']." ".$points_obj[$e]['last_name'];
                                                                $name_ar                  = $cards_obj[$e]['first_name_ar']." ".$points_obj[$e]['last_name_ar'];
																$added_date               = date("M d, Y H:i a", strtotime($points_obj[$e]['added_date']));
                                                        ?>
                                                        <tr>
                                                           <td align="left" valign="middle"><?=$card_name?><div class="pull-right"><?=$card_name_ar?></div></td>
                                                          <td align="left" valign="middle"><?=$comments_student?></td>
                                                          <td align="left" valign="middle"><?=$points_student?></td>
                                                          <td align="left" valign="middle"><?=$name?><div class="pull-right"><?=$name_ar?></div></td>
                                                          <td align="left" valign="middle"><?=$added_date?></td>
                                                          <td align="left" valign="middle"><?=$class_name?><div class="pull-right"><?=$class_name_ar?></div></td>
                                                        </tr>
                                                        <?php } ?>
                            
                                                        <?php 
                                                            if (count($cards_obj)<=0) {
                                                        ?>
                                                        <tr>
                                                          <td colspan="6" align="center" valign="middle">
                                                          <div class="alert alert-warning alert-dismissible" style="width:50%">
                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                            <h4><i class="icon fa fa-info"></i> Information!</h4>
                                                            The card details are not available.
                                                          </div>                                
                                                          </td>
                                                        </tr>
                                                        <?php   
                                                            }// for ($e=0; $e<count($rs_user_education); $e++)
                                                        ?>
                                                        
                                                        </tbody>
                                                        <tfoot>
                                                        </tfoot>
                                                      </table>
                                                  </div>
                                                </div>        
                                        <!--/Listing-->
                                </div>

                                <div class="tab-pane" id="tab_course">
                                        <!--Listing-->
                                                <div id="div_tab3" class="box box-success">
                                                    <div class="box-body">
                                                      <table width="100%" class="table table-bordered table-striped" id="example1">
                                                        <thead>
                                                        <tr>
                                                          <th width="45%" align="center" valign="middle">Records</th>
                                                          <th width="40%" align="center" valign="middle">Category</th>
                                                          <!--<th width="25%" align="center" valign="middle">Added By</th>-->
                                                          <th width="15%" align="center" valign="middle">Date</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        
                                                        <?php
                                                            for ($e=0; $e<count($records_obj); $e++) {
                                                                $id_e                     = $records_obj[$e]['id'];
                                                                $title_en                 = $records_obj[$e]['title_en'];
																$title_ar                 = $records_obj[$e]['title_ar'];
																$parenting_title_en       = $records_obj[$e]['parenting_title_en'];
																$parenting_title_ar        = $records_obj[$e]['parenting_title_ar'];
																
                                                                
                                                                $class_name               = $records_obj[$e]['class_name']." ".$records_obj[$e]['section_name'];
                                                                $class_name_ar            = $records_obj[$e]['class_name_ar']." ".$records_obj[$e]['section_name_ar'];
                                                                $name                     = $records_obj[$e]['first_name']." ".$records_obj[$e]['last_name'];
                                                                $name_ar                  = $records_obj[$e]['first_name_ar']." ".$records_obj[$e]['last_name_ar'];
																$added_date               = date("M d, Y H:i a", strtotime($records_obj[$e]['added_date']));
                                                        ?>
                                                        <tr>
                                                          
                                                          <td align="left" valign="middle"><?=$parenting_title_en?><div class="pull-right"><?=$parenting_title_ar?></div></td>
                                                          <td align="left" valign="middle"><?=$title_en?><div class="pull-right"><?=$title_ar?></div></td>
                                                          <!--<td align="left" valign="middle"><?=$name?><div class="pull-right"><?=$name_ar?></div></td>-->
                                                          <td align="left" valign="middle"><?=$added_date?></td>
                                                        </tr>
                                                        <?php } ?>
                            
                                                        <?php 
                                                            if (count($records_obj)<=0) {
                                                        ?>
                                                        <tr>
                                                          <td colspan="7" align="center" valign="middle">
                                                          <div class="alert alert-warning alert-dismissible" style="width:50%">
                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                            <h4><i class="icon fa fa-info"></i> Information!</h4>
                                                            The records details are not available.
                                                          </div>                                
                                                          </td>
                                                        </tr>
                                                        <?php   
                                                            }// for ($e=0; $e<count($rs_user_education); $e++)
                                                        ?>
                                                        
                                                        </tbody>
                                                        <tfoot>
                                                        </tfoot>
                                                      </table>
                                                  </div>
                                                </div>        
                                        <!--/Listing-->
                                </div>

                                <div class="tab-pane" id="tab_jobs_applied_for">
                                        <!--Listing-->
                                                <div id="div_tab3" class="box box-success">
                                                    <div class="box-body">
                                                    <table width="100%" class="table table-bordered table-striped" id="example1">
                                                        <thead>
                                                        <tr>
                                                          <th width="50%" align="center" valign="middle">Message</th>
                                                          <th width="15%" align="center" valign="middle">Email</th>
                                                          <th width="25%" align="center" valign="middle">Sent By</th>
                                                          <th width="10%" align="center" valign="middle">Date</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        
                                                        <?php
                                                            for ($e=0; $e<count($messages_obj); $e++) {
                                                                $id_e                     = $messages_obj[$e]['id'];
                                                                $comments_parent          = $messages_obj[$e]['contact_us_comments'];
                                                                $email                    = $messages_obj[$e]['email'];
                                                                $class_name               = $messages_obj[$e]['class_name']." ".$messages_obj[$e]['section_name'];
                                                                $class_name_ar            = $messages_obj[$e]['class_name_ar']." ".$messages_obj[$e]['section_name_ar'];
                                                                $name                     = $messages_obj[$e]['first_name']." ".$messages_obj[$e]['last_name'];
                                                                $name_ar                  = $messages_obj[$e]['first_name_ar']." ".$messages_obj[$e]['last_name_ar'];
																$added_date               = date("M d, Y H:i a", strtotime($messages_obj[$e]['added_date']));
                                                        ?>
                                                        <tr>
                                                          <td align="left" valign="middle"><?=$comments_parent?></td>
                                                          <td align="left" valign="middle"><?=$email?></td>
                                                          <td align="left" valign="middle"><?=$name?><div class="pull-right"><?=$name_ar?></div></td>
                                                          <td align="left" valign="middle"><?=$added_date?></td>
                                                        </tr>
                                                        <?php } ?>
                            
                                                        <?php 
                                                            if (count($messages_obj)<=0) {
                                                        ?>
                                                        <tr>
                                                          <td colspan="7" align="center" valign="middle">
                                                          <div class="alert alert-warning alert-dismissible" style="width:50%">
                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                            <h4><i class="icon fa fa-info"></i> Information!</h4>
                                                            The message details are not available.
                                                          </div>                                
                                                          </td>
                                                        </tr>
                                                        <?php   
                                                            }// for ($e=0; $e<count($rs_user_education); $e++)
                                                        ?>
                                                        
                                                        </tbody>
                                                        <tfoot>
                                                        </tfoot>
                                                      </table>
                                                  </div>
                                                </div>        
                                        <!--/Listing-->
                                </div>
                                
                                <div class="tab-pane" id="tab_courses_applied_for">
                                        <!--Listing-->
                                                <div id="div_tab3" class="box box-success">
                                                    <div class="box-body">
                                                      <table width="100%" class="table table-bordered table-striped" id="example1">
                                                        <thead>
                                                        <tr>
                                                          <th width="10%" align="center" valign="middle">Ser No.</th>
                                                          <th width="60%" align="center" valign="middle">Course Title</th>
                                                          <th width="30%" align="center" valign="middle">Application Date</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                            for ($c=0; $c<count($rs_course_user); $c++) {
                                                                $id_c = $rs_course_user[$c]['id'];
                                                                $tbl_course_user_id_c = $rs_course_user[$c]['tbl_course_user_id'];
                                                                $tbl_course_id_c = $rs_course_user[$c]['tbl_course_id'];
                                                                $tbl_user_id_c = $rs_course_user[$c]['tbl_user_id'];
                                                                $added_date_c = $rs_course_user[$c]['added_date'];
																
																
																$course_obj = $CI->model_course->get_course_obj($tbl_course_id_c);
																$title_en = $course_obj['title_en'];
																$title_ar = $course_obj['title_ar'];
                                                        ?>
                                                        <tr>
                                                          <td align="left" valign="middle"><?=$c+1?></td>
                                                          <td align="left" valign="middle">
														  	<a href="<?=HOST_URL?>/en/admin/course/edit_course/course_id_enc/<?=$tbl_course_id_c?>" target="_blank"><?=$title_en?><div class="pull-right"><?=$title_ar?></div></a>
                                                          </td>
                                                          <td align="left" valign="middle"><?=$added_date_c?></td>
                                                        </tr>
                                                        <?php } ?>
                            
                                                        <?php 
                                                            if (count($rs_course_user)<=0) {
                                                        ?>
                                                        <tr>
                                                          <td colspan="7" align="center" valign="middle">
                                                          <div class="alert alert-warning alert-dismissible" style="width:50%">
                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                            <h4><i class="icon fa fa-info"></i> Information!</h4>
                                                            The are no courses available.
                                                          </div>                                
                                                          </td>
                                                        </tr>
                                                        <?php   
                                                            }// for ($e=0; $e<count($rs_user_experience); $e++)
                                                        ?>
                                                        
                                                        </tbody>
                                                        <tfoot>
                                                        </tfoot>
                                                      </table>
                                                  </div>
                                                </div>        
                                        <!--/Listing-->
                                </div>

                                <div class="tab-pane" id="tab_attachments">
                                        <!--Listing-->
                                                <div id="div_tab2" class="box box-success">
                                                    <div class="box-body">
                                                      <table width="100%" class="table table-bordered table-striped" id="example1">
                                                        <thead>
                                                        <tr>
                                                          <th width="10%" align="center" valign="middle">Ser No.</th>
                                                          <th width="30%" align="center" valign="middle">Title</th>
                                                          <th width="30%" align="center" valign="middle">Document</th>
                                                          <th width="20%" align="center" valign="middle">Size(KB)</th>
                                                          <th width="10%" align="center" valign="middle">Date</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                            for ($a=0; $a<count($rs_user_document); $a++) {
                                                                $id_a = $rs_user_experience[$a]['id'];
                                                                $tbl_user_document_id_a = $rs_user_document[$a]['tbl_user_document_id'];
                                                                $tbl_user_id_a = $rs_user_document[$a]['tbl_user_id'];
                                                                $title_document_a = $rs_user_document[$a]['title_document'];
                                                                $file_name_original_a = $rs_user_document[$a]['file_name_original'];
                                                                $file_name_updated_a = $rs_user_document[$a]['file_name_updated'];
                                                                $file_type_a = $rs_user_document[$a]['file_type'];
                                                                $file_size_a = $rs_user_document[$a]['file_size'];
                                                                $added_date_a = $rs_user_document[$a]['added_date'];
																
																//$title_document_a = ucfirst($title_document_a);
																$added_date_a = date('m-d-Y',strtotime($added_date_a));
																
																$document_path = HOST_URL."/misc/uploads/".$file_name_updated_a;
																
																$title_document_display = "";
																switch($title_document_a) {
																	case("cv"): {
																		$title_document_display = "CV";
																		break;
																	}	
																	case("passport"): {
																		$title_document_display = "Passport";
																		break;
																	}	
																	case("family_book"): {
																		$title_document_display = "Family Book";
																		break;
																	}	
																	case("educational_certificate"): {
																		$title_document_display = "Educational Certificate";
																		break;
																	}	
																	case("work_experience_certificate1"): {
																		$title_document_display = "Work Experience Certificate 1";
																		break;
																	}	
																	case("work_experience_certificate2"): {
																		$title_document_display = "Work Experience Certificate 2";
																		break;
																	}	
																	case("work_experience_certificate3"): {
																		$title_document_display = "Work Experience Certificate 3";
																		break;
																	}	
																	case("work_experience_certificate4"): {
																		$title_document_display = "Work Experience Certificate 4";
																		break;
																	}	
																	case("work_experience_certificate5"): {
																		$title_document_display = "Work Experience Certificate 5";
																		break;
																	}	
																	case("training_certificate1"): {
																		$title_document_display = "Training Certificate 1";
																		break;
																	}	
																	case("training_certificate2"): {
																		$title_document_display = "Training Certificate 2";
																		break;
																	}	
																	case("training_certificate3"): {
																		$title_document_display = "Training Certificate 3";
																		break;
																	}	
																	case("training_certificate4"): {
																		$title_document_display = "Training Certificate 4";
																		break;
																	}	
																	case("training_certificate5"): {
																		$title_document_display = "Training Certificate 5";
																		break;
																	}	
																	case("emirate_id_card"): {
																		$title_document_display = "Emirate ID Card";
																		break;
																	}	
																}
																
                                                        ?>
                                                        <tr>
                                                          <td align="left" valign="middle"><?=$a+1?></td>
                                                          <td align="left" valign="middle"><a target="_blank" href="<?=$document_path?>"><li class="fa fa-download"></li> <?=$title_document_display?></a></td>
                                                          <td align="left" valign="middle"><?=$file_name_updated_a?></td>
                                                          <td align="left" valign="middle"><?=$file_size_a?></td>
                                                          <td align="left" valign="middle"><?=$added_date_a?></td>
                                                        </tr>
                                                        <?php } ?>
                            
                                                        <?php 
                                                            if (count($rs_user_document)<=0) {
                                                        ?>
                                                        <tr>
                                                          <td colspan="7" align="center" valign="middle">
                                                          <div class="alert alert-warning alert-dismissible" style="width:50%">
                                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                            <h4><i class="icon fa fa-info"></i> Information!</h4>
                                                            There are no attachments.
                                                          </div>                                
                                                          </td>
                                                        </tr>
                                                        <?php   
                                                            }// for ($e=0; $e<count($rs_user_experience); $e++)
                                                        ?>
                                                        
                                                        </tbody>
                                                        <tfoot>
                                                        </tfoot>
                                                      </table>
                                                  </div>
                                                </div>        
                                        <!--/Listing-->
                                </div>

                                <div class="tab-pane" id="tab_notes">


                                            <div id="mid2" class="box box-success">
                                                            <div class="box-header with-border">
                                                              <h3 class="box-title">Notes</h3>
                                                              <div class="box-tools"></div>
                                                            </div>
                                                            <!-- /.box-header -->
                                                            <!-- form start -->
                                                            <form name="frm_notes" id="frm_notes" class="form-horizontal" method="post">
                                                              <div class="box-body">
                                                              
                                                                <div class="form-group">
                                                                  <label class="col-sm-2 control-label" for="dob">Notes</label>
                                                
                                                                  <div class="col-sm-10">
                                                                     <textarea class="textarea" placeholder="Notes" id="notes" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?=$notes?></textarea>
                                                                  </div>
                                                                </div>

                                                                
                                                              </div>
                                                              <!-- /.box-body -->
                                                              <div class="box-footer">
                                                                <button class="btn btn-primary" type="button" onclick="ajax_save_notes()">Save</button>
                                                                <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                                                              </div>
                                                              <!-- /.box-footer -->
                                                            </form>
                                           </div>

                                	
                                </div>
                                
                          </div>
                          <!-- /.tab-content --> 
                        </div>
                <!--/TABS-->
            </div>
        </div>    

    <!--/WORKING AREA--> 
  </section>
</div>