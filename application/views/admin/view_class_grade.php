<?php
//Init Parameters
$class_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}
?>
 

<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/class_grades";
			}
		})
	});
	
function confirm_delete_popup() {
		var len = $("input[id='class_id_enc']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more class(s) / grade(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/deleteGrade",
			data: {
				class_id_enc: $("input[id='class_id_enc']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Class(s) / Grade(s) deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}	
	function ajax_activate(class_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/activateGrade",
			data: {
				class_id_enc: class_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Class / Grade activated successfully.", 'green')

				$('#act_deact_'+class_id_enc).html('<span style="cursor:pointer" onClick="ajax_deactivate(\''+class_id_enc+'\')" onMouseOver="deactivate_me(this)" onMouseOut="reset_activate(this)" class="label label-success">Active</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_deactivate(class_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/deactivateGrade",
			data: {
				class_id_enc: class_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Class / Grade de-activated successfully.", 'green')
				
				$('#act_deact_'+class_id_enc).html('<span style="cursor:pointer" onClick="ajax_activate(\''+class_id_enc+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/is_exist_grade",
			data: {
				class_id_enc: "<?=$class_id_enc?>",
				tbl_school_type_id: $('#tbl_school_type_id').val(),
				school_type: $('#school_type').val(),
				class_name: $('#class_name').val(),
				tbl_section_id: $('#tbl_section_id').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
					refresh_page = "N";
					my_alert("Class / Grade already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_create();
				}
			
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist_edit() {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/is_exist_grade",
			data: {
				class_id_enc:$('#class_id_enc').val(),
				tbl_school_type_id: $('#tbl_school_type_id').val(),
				class_name: $('#class_name').val(),
				class_name_ar: "",
				tbl_section_id: $('#tbl_section_id').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String(data);
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
					refresh_page = "N";
					my_alert("Class / Grade already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_save_changes();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_create() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/create_class_grade",
			data: {
				class_id_enc: "<?=$class_id_enc?>",
				tbl_school_type_id : $('#tbl_school_type_id').val(),
				school_type: $('#school_type').val(),
				school_type_ar: $('#school_type_ar').val(),
				class_name: $('#class_name').val(),
				class_name_ar: $('#class_name_ar').val(),
				tbl_section_id : $('#tbl_section_id').val(),
				section_name: $('#section_name').val(),
				section_name_ar: $('#section_name_ar').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
					refresh_page = "Y";
				    my_alert("Class / Grade created successfully.", 'green');
				    $("#pre-loader").hide();
				}else if (temp=='X') {
					refresh_page = "N";
				    my_alert("Class / Grade already exist.", 'red');
				    $("#pre-loader").hide();
				}else{
					refresh_page = "N";
					my_alert("Class / Grade added failed, Please try again.", 'red');
					$("#pre-loader").hide();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_save_changes() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/save_grade_changes",
			data: {
				class_id_enc:$('#class_id_enc').val(),
				tbl_school_type_id : $('#tbl_school_type_id').val(),
				class_name: $('#class_name').val(),
				class_name_ar: $('#class_name_ar').val(),
				tbl_section_id : $('#tbl_section_id').val(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Changes saved successfully.", 'green');
				
				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_sort_order(class_id_enc) {
		$("#pre-loader").show();
		var sortVal = $("#sort_order_"+class_id_enc).val();
		if(sortVal!="" && sortVal > 0 )
		{
			$.ajax({
				type: "POST",
				url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/update_sort_order",
				data: {
					class_id_enc: class_id_enc,
					sortVal     : sortVal,
					is_ajax: true
				},
				success: function(data) {
					refresh_page = "N";
					my_alert("Classes / Grades sort order updated successfully.", 'green')
					$("#pre-loader").hide();
				},
				error: function() {
					$("#pre-loader").hide();
				}, 
				complete: function() {
					$("#pre-loader").hide();
				}
			});
		}
	}
	
</script>
<script language="javascript">
	function ajax_validate() {
		if (validate_school_type() == false ||  validate_class_name() == false || validate_class_name_ar() == false || validate_class_section() == false ) {
			return false;
		} else {
			//is_exist();
			ajax_create();
		}
	} 

	function ajax_validate_edit() {
		if ( validate_school_type() == false ||  validate_class_name() == false || validate_class_name_ar() == false || validate_class_section() == false) {
			return false;
		} else {
			is_exist_edit();
		}
	} 

    function validate_school_type() {
		var regExp = / /g;
		var str = $('#tbl_school_type_id').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			var school_type    = $("#school_type").val();
			var school_type_ar = $("#school_type_ar").val();
			if($('#divType').is(':visible'))
			{

			   $('#tbl_school_type_id').val('').change();
			   if(school_type=="")
			   {
				   my_alert("Please enter Class Type in English.");
				   return false;
			   }
			   
			    if(school_type_ar=="")
			   {
				   my_alert("Please enter Class Type in Arabic.");
				   return false;
			   }
			}else{
				my_alert("Class Type is not selected. Please select Class Type.");
				return false;
			}
		}
		
	 return true;
		
	}

	function validate_class_name() {
		var regExp = / /g;
		var str = $('#class_name').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Class / Grade Name [En] is blank. Please enter Class / Grade Name [En].");
		return false;
		}
	}

	function validate_class_name_ar() {
		var regExp = / /g;
		var str = $('#class_name_ar').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Class / Grade Name [Ar] is blank. Please enter Class / Grade Name [Ar].");
		return false;
		}
	}
	
	 function validate_class_section() {
		var regExp = / /g;
		var section_id      = $('#tbl_section_id').val();
		var section_name    = $("#section_name").val();
		if(section_name=="")
		{
			if (section_id == '' ) {
				 my_alert("Section / Division is not selected. Please select Section / Division or Add New Section / Division.");
			     return false;
			}
		}else{
		   var section_name_ar = $("#section_name_ar").val();
		   if(section_name_ar=="")
		   {
			   my_alert("Please enter Section / Division in Arabic.");
			   return false;
		   }
			
		}
		return true;
		
	}

</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Class / Grades<small> Management</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Class / Grades</li>
    </ol>
    <!--/BREADCRUMB--> 
    <div style=" float:right; "> <button onclick="show_class_types()" title="Class Types" type="button" class="btn btn-primary">Class Types</button></div> 
    <div style=" float:right; padding-right:10px;"> <button onclick="show_class_sections()" title="Class Divisions" type="button" class="btn btn-primary">Class Divisions</button></div> 
    <div style="clear:both"></div>
  </section>
  <script>
   function show_class_types()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/school_types/";
		window.location.href = url;
	}  
    function show_class_sections()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/class_sections/";
		window.location.href = url;
	}        
  </script> 
  
  
  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {

			$tbl_class_id               = $class_grade_obj['tbl_class_id'];
			$class_name                 = $class_grade_obj['class_name'];
			$class_name_ar              = $class_grade_obj['class_name_ar'];
			$added_date                 = $class_grade_obj['added_date'];
			$is_active                  = $class_grade_obj['is_active'];
			$tbl_sel_section_id         = $class_grade_obj['tbl_section_id'];
			$tbl_sel_school_type_id     = $class_grade_obj['tbl_school_type_id'];
	?>
        <!--Edit-->
        
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Class / Grade </h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/class_grades"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_edit" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                  
                    <div class="form-group">
                     <label class="col-sm-2 control-label" for="tbl_school_type_id">Class Type<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-10">
                                 <select name="tbl_school_type_id" id="tbl_school_type_id" class="form-control"  >
                                 <option value="">Select Class Type</option>
                              <?php
                                    for ($u=0; $u<count($school_types); $u++) { 
                                        $tbl_school_type_id_u   = $school_types[$u]['tbl_school_type_id'];
                                        $school_type            = $school_types[$u]['school_type'];
                                        $school_type_ar         = $school_types[$u]['school_type_ar'];
                                        if($tbl_sel_school_type_id == $tbl_school_type_id_u)
                                           $selType = "selected";
                                         else
                                           $selType = "";
                                  ?>
                                      <option value="<?=$tbl_school_type_id_u?>"  <?=$selType?> >
                                      <?=$school_type?>&nbsp;[::]&nbsp;
                                    <?=$school_type_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>
                   </div>
                   </div>
                    
                  
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="class_name">Class / Grade [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Class / Grade Name [En]" id="class_name" name="class_name" class="form-control" value="<?=$class_name?>">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="school_type_ar">Class / Grade [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Class / Grade Name [Ar]" id="class_name_ar" name="class_name_ar" class="form-control" value="<?=$class_name_ar?>" dir="rtl">
                      </div>
                    </div>
                    
                    
                     <div class="form-group">
                     <label class="col-sm-2 control-label" for="tbl_section_id">Section / Division<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-10">
                                 <select name="tbl_section_id" id="tbl_section_id" class="form-control"  >
                                 <option value="">Select Section / Division</option>
                              <?php
                                    for ($u=0; $u<count($class_sections); $u++) { 
                                        $tbl_section_id_u    = $class_sections[$u]['tbl_section_id'];
                                        $section_name        = $class_sections[$u]['section_name'];
                                        $section_name_ar     = $class_sections[$u]['section_name_ar'];
                                        if($tbl_sel_section_id == $tbl_section_id_u)
                                           $selSection = "selected";
                                         else
                                           $selSection = "";
                                  ?>
                                      <option value="<?=$tbl_section_id_u?>"  <?=$selSection?> >
                                      <?=$section_name?>&nbsp;[::]&nbsp;
                                      <?=$section_name_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>
                   </div>
                   </div>
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate_edit()">Save Changes</button>
                    <input type="hidden" name="class_id_enc" id="class_id_enc" value="<?=$tbl_class_id?>" />
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
		        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/classes/class_grades";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
    
  
 <link href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" rel="stylesheet">
 <script src="http://code.jquery.com/jquery-1.11.1.js"></script>
 <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
  <script>
  $( function() {
		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
				
				/* var order = $("#tabledivbody").sortable("serialize");
				
				alert(order);
				
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/your/url/here'
				});*/
				
				
				
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	

				
				
				
				
				
			}
	  } );
  
  } );
  </script> 
    
                       <div id="mid1" class="box box-success">
                        <div class="box-header">
                          <div class="col-sm-1" >
                          <h3 class="box-title">SEARCH</h3>
                          </div>
                          <div class="col-sm-11"> 
                               <div class="col-sm-6"><input name="q" id="q" value="<?=urldecode($q)?>" type="text" class="form-control" placeholder="Search By Class Name "   > </div>
                               <div class="col-sm-2"><button class="btn btn-success" type="button" onclick="search_data()">Search</button>&nbsp;<button class="btn btn-success" type="button" 
                               onclick="reset_data();">Reset</button>
                               </div>
                           
                          </div>
                        </div>  
                     </div>   
    
            <!--Listing-->
                    <div id="mid1_list" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Classes / Grades</h3>
                          <div class="box-tools">
                            <?php if (count($rs_all_class_grades)>0) { echo $paging_string;}?>	
                            <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
                            <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>
                          </div>
                        </div>
                        
                        <div class="box-body">
                     <!--   <div style="color:#030; font-weight:bold;">You can sort categories by using drag and drop of rows </div>-->
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle"><input id="select_all" type="checkbox" value="" /></th>
                              <!--<th width="10%" align="center" valign="middle">Sl No.</th>-->
                              <th width="17%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y">Class / Grade [En] <?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?><div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>
                              <th width="17%" align="center" valign="middle">Class / Grade [Ar]</th>
                              <th width="21%" align="center" valign="middle">Class Type</th>
                              <th width="10%" align="center" valign="middle"><a href="<?=$sort_url?>/sort_name/P/sort_by/<?=$sort_by?>/sort_by_click/Y">Sorting Order<?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "P" && $sort_by == "ASC") { ?>&nbsp;<div class="fa fa-sort-up"></div><?php } else {?>&nbsp;<div class="fa fa-sort-desc"></div><?php } ?></a></th>
                              <th width="5%" align="center" valign="middle">Status</th>
                              <th width="15%" align="center" valign="middle">Action</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                                for ($i=0; $i<count($rs_all_class_grades); $i++) { 
                                    $id = $rs_all_class_grades[$i]['id'];
                                    $tbl_class_id     = $rs_all_class_grades[$i]['tbl_class_id'];
                                    $class_name       = $rs_all_class_grades[$i]['class_name'];
                                    $class_name_ar    = $rs_all_class_grades[$i]['class_name_ar'];
									$section_name     = $rs_all_class_grades[$i]['section_name'];
									$section_name_ar  = $rs_all_class_grades[$i]['section_name_ar'];
                                    $school_type      = $rs_all_class_grades[$i]['school_type'];
									$school_type_ar   = $rs_all_class_grades[$i]['school_type_ar'];
                                    $added_date       = $rs_all_class_grades[$i]['added_date'];
                                    $is_active        = $rs_all_class_grades[$i]['is_active'];
									$priority         = $rs_all_class_grades[$i]['priority'];
									$cntPeriods       = $rs_all_class_grades[$i]['cntPeriods'];
                                    
                                    $class_name       = ucfirst($class_name);
                                    $added_date         = date('m-d-Y',strtotime($added_date));
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_class_id?>" >
                              <td align="left" valign="middle">
                              <span style="float:left;">
                              <input id="class_id_enc" name="class_id_enc" class="checkbox" type="checkbox" value="<?=$tbl_class_id?>" />
                              </span>
                              
                             <?php /*?> <span style="float:left;">&nbsp;
                              <?php if($i<>0){ ?> <i class="fa fa-arrow-up"  style="color:#3c8dbc; cursor:pointer;"  aria-hidden="true" title="Sorting - Drag & Drop To Up"></i> &nbsp; <?php } ?>
                               <?php if($i<> count($rs_all_categories)-1){ ?> <i class="fa fa-arrow-down" style="color:#3c8dbc;cursor:pointer;" aria-hidden="true" title="Sorting - Drag & Drop To Down"></i> <?php } ?>
                              </span><?php */?>
                              </td>
                             <!-- <td align="left" valign="middle"><?=$offset+$i+1?></td>-->
                              <td align="left" valign="middle"><?=$class_name?>&nbsp;<?=$section_name?></td>
                              <td align="right" valign="middle"><?=$section_name_ar?>&nbsp;<?=$class_name_ar?></td>
                               <td align="left" valign="middle"><?=$school_type?><span style="float:right;"><?=$school_type_ar?></span></td>
                              <td align="left" valign="middle"><input name="sort_order" id="sort_order_<?=$tbl_class_id?>" value="<?=$priority?>" class="form-control" placeholder="Sort Order" type="text" maxlength="3"   style="width:30%" onchange="ajax_sort_order('<?=$tbl_class_id?>');"></td>
                              <td align="left" valign="middle">
                                <div id="act_deact_<?=$tbl_class_id?>">
                                <?php if (trim($is_active) == "Y") { ?>
                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_class_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                <?php } else { ?>
                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_class_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                <?php } ?>
                                </div>
                              </td>
                              <td align="left" valign="middle">
                               <?php if($cntPeriods==0){?>
                               
                                   <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/class_sessions/class_id_enc/<?=$tbl_class_id?>"><button style="font-size:12px;" class="btn bg-maroon" type="button" title="Add Periods"> Add Periods</button></a>
                               
							   <?php }else{ ?>
                                 <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/class_sessions/class_id_enc/<?=$tbl_class_id?>"><button style="font-size:12px;" class="btn bg-green" type="button" title="<?=$cntPeriods?> Periods - Add More"><?=$cntPeriods?> Periods - Add More</button></a>

                               
                               <?php } ?>
                              
                                <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/edit_class_grade/class_id_enc/<?=$tbl_class_id?>"><button class="btn bg-purple fa fa-pencil" type="button" title="Edit"></button></a>
                              </td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="8" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_all_class_grades)<=0) {
                            ?>
                            <tr>
                              <td colspan="8" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no classes / grades available. Click on the + button to create one.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
    
            <!--Add or Create-->
              <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Create Class / Grade</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                  
                   
                    
                     <div class="form-group">
                     <label class="col-sm-2 control-label" for="tbl_school_type_id">Class Type<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-4">
                                 <select name="tbl_school_type_id" id="tbl_school_type_id" class="form-control"  >
                                 <option value="">Select Class Type</option>
                              <?php
                                    for ($u=0; $u<count($school_types); $u++) { 
                                        $tbl_school_type_id_u   = $school_types[$u]['tbl_school_type_id'];
                                        $school_type            = $school_types[$u]['school_type'];
                                        $school_type_ar         = $school_types[$u]['school_type_ar'];
                                        if($tbl_sel_school_type_id == $tbl_school_type_id_u)
                                           $selType = "selected";
                                         else
                                           $selType = "";
                                  ?>
                                      <option value="<?=$tbl_school_type_id_u?>"  <?=$selType?> >
                                      <?=$school_type?>&nbsp;[::]&nbsp;
                                    <?=$school_type_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select> 
                     </div>
                      <div class="col-sm-6">
                        <label class="col-sm-2 control-label" for="tbl_parenting_school_cat_id" style="font-size:12px; text-decoration:underline; cursor:pointer; color:#030;" ><span style="padding-left:2px;" onclick="addNewType();">+&nbsp;Add New Type</span></label>
                      </div>
                      </div>
                    
                    <div class="form-group" id="divType" style="display:none;" >
                      <label class="col-sm-2 control-label" for="category_en">Class Type [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-4">
                        <input type="text" placeholder="Class Type [En]" id="school_type" name="school_type" class="form-control"  value="" >
                      </div> 
                      
                      <label class="col-sm-2 control-label" for="school_type_ar">Class Type [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-4">
                        <input type="text" placeholder="Class Type [Ar]" id="school_type_ar" name="school_type_ar" class="form-control" dir="rtl" value="" >
                      </div>
                    </div>
                    
                    
                    
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="class_name">Class / Grade Name [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-4">
                        <input type="text" placeholder="Class / Grade Name [En]" id="class_name" name="class_name" class="form-control">
                      </div>
                      
                      <label class="col-sm-2 control-label" for="class_name_ar">Class / Grade Name [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-4">
                        <input type="text" placeholder="Class / Grade Name [Ar]" id="class_name_ar" name="class_name_ar" class="form-control" dir="rtl">
                      </div>
                    </div>
                                        
                   
                    <div class="form-group">
                     <label class="col-sm-2 control-label" for="tbl_section_id">Section / Division<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-4">
                                 <select name="tbl_section_id" id="tbl_section_id" class="form-control"  >
                                 <option value="">Select Section / Division</option>
                              <?php
                                    for ($u=0; $u<count($class_sections); $u++) { 
                                        $tbl_section_id_u    = $class_sections[$u]['tbl_section_id'];
                                        $section_name        = $class_sections[$u]['section_name'];
                                        $section_name_ar     = $class_sections[$u]['section_name_ar'];
                                        if($tbl_sel_section_id == $tbl_section_id_u)
                                           $selSection = "selected";
                                         else
                                           $selSection = "";
                                  ?>
                                      <option value="<?=$tbl_section_id_u?>"  <?=$selSection?> >
                                      <?=$section_name?>&nbsp;[::]&nbsp;
                                      <?=$section_name_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>
                    </div>
                    <div class="col-sm-6">
                    <label class="col-sm-2 control-label" for="tbl_parenting_school_cat_id" style="font-size:12px; text-decoration:underline; cursor:pointer; color:#030;" ><span style="padding-left:2px;" onclick="addNewDivision();">+&nbsp;Add New Division</span></label>
                    </div>
                    </div>
                    
                    <div class="form-group" id="divDivision" style="display:none;" >
                      <label class="col-sm-2 control-label" for="section_name">Section / Division [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-4">
                        <input type="text" placeholder="Section / Division [En]" id="section_name" name="section_name" class="form-control"  value="" >
                      </div> 
                      
                      <label class="col-sm-2 control-label" for="section_name_ar">Division [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-4">
                        <input type="text" placeholder="Section / Division [Ar]" id="section_name_ar" name="section_name_ar" class="form-control" dir="rtl" value="" >
                      </div>
                      
                    </div>
                    
                    
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate()">Submit</button>
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
            <!--/Add or Create-->
                
        <!--/Admin Category Management-->
           <script>
					  
			 function addNewType()
			 {
				// divCategory
				if($('#divType').is(':visible'))
				{
					$("#divType").hide();
					$("#school_type").val();
					$("#school_type").val();
					$("#tbl_school_type_id").val();
				}else{
					$("#divType").show();
					$("#tbl_school_type_id").val();
					
				}
				 
			 }
			 
			  function addNewDivision()
			 {
				// divCategory
				if($('#divDivision').is(':visible'))
				{
					$("#divDivision").hide();
					$("#section_name").val();
					$("#section_name_ar").val();
				}else{
					$("#divDivision").show();
					$("#tbl_section_id").val();
				}
				 
			 }
                 </script>    
	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>

        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >
function search_data() {
		var q = $.trim($("#q").val());
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/class_grades/";
		
		if(q !='')
			url += "q/"+q+"/";
		
			url += "offset/0/";
		window.location.href = url;
		<?php /*?>window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/enquiry/all_enquiries/is_not_replied/"+is_not_replied+"/tbl_court_id/"+tbl_court_id+"/tbl_category_id/"+tbl_category_id;<?php */?>
	}

function reset_data() {
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/class_grades/";
		url += "offset/0/";
		window.location.href = url;
	}


</script>