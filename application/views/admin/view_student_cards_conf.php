<?php
//Init Parameters
$card_category_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}
?>
 

<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/student_cards_conf";
			}
		})
	});
	
function confirm_delete_popup() {
		var len = $("input[id='card_category_id_enc']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more card(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/deleteStudentCard",
			data: {
				card_category_id_enc: $("input[id='card_category_id_enc']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Student card(s) deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}	
	function ajax_activate(card_category_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/activateStudentCard",
			data: {
				card_category_id_enc: card_category_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Student card is activated successfully.", 'green')

				$('#act_deact_'+card_category_id_enc).html('<span style="cursor:pointer" onClick="ajax_deactivate(\''+card_category_id_enc+'\')" onMouseOver="deactivate_me(this)" onMouseOut="reset_activate(this)" class="label label-success">Active</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_deactivate(card_category_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/deactivateStudentCard",
			data: {
				card_category_id_enc: card_category_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Student card is de-activated successfully.", 'green')
				
				$('#act_deact_'+card_category_id_enc).html('<span style="cursor:pointer" onClick="ajax_activate(\''+card_category_id_enc+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/is_exist_student_card",
			data: {
				card_category_id_enc: "<?=$card_category_id_enc?>",
				category_name_en: $('#category_name_en').val(),
				category_name_ar: "",
				is_ajax: true
			},
			success: function(data) {
				
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
					refresh_page = "N";
					my_alert("Student card is already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_create();
				}
			
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist_edit() {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/is_exist_student_card",
			data: {
				card_category_id_enc:$('#card_category_id_enc').val(),
				category_name_en: $('#category_name_en').val(),
				category_name_ar: "",
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
					refresh_page = "N";
					my_alert("Student card is already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_save_changes();
				}
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_create() {
		var selectedCategory = '';
		//var selectedCategory = new Array();
        var n = $(".chkBox:checked").length;
        if (n > 0){
            $(".chkBox:checked").each(function(){
               // selectedCategory.push($(this).val());
				selectedCategory += $(this).val()+"||";
            });
        }
		
		
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/add_student_card",
			data: {
				card_category_id_enc: "<?=$card_category_id_enc?>",
				category_name_en: $('#category_name_en').val(),
				category_name_ar: $('#category_name_ar').val(),
				card_point: $('#card_point').val(),
				tbl_category_id:selectedCategory,
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
				refresh_page = "Y";
				    my_alert("Student card is added successfully.", 'green');
				    $("#pre-loader").hide();
				}else{
					refresh_page = "N";
					my_alert("Student card is added failed, Please try again.", 'red');
					$("#pre-loader").hide();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_save_changes() {
		
		var selectedCategory = '';
		//var selectedCategory = new Array();
        var n = $(".chkBox:checked").length;
        if (n > 0){
            $(".chkBox:checked").each(function(){
               // selectedCategory.push($(this).val());
				selectedCategory += $(this).val()+"||";
            });
        }
		
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/save_student_card_changes",
			data: {
				card_category_id_enc:$('#card_category_id_enc').val(),
				category_name_en: $('#category_name_en').val(),
				category_name_ar: $('#category_name_ar').val(),
				tbl_category_id:selectedCategory,
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
				refresh_page = "Y";
				    my_alert("Student card is updated successfully.", 'green');
				    $("#pre-loader").hide();
				}else{
					refresh_page = "N";
					my_alert("Student card updation failed, Please try again.", 'red');
					$("#pre-loader").hide();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
</script>
<script language="javascript">
	function ajax_validate() {
		if (validate_card_info() == false || validate_card_info_ar() == false  || validate_category() == false ) {
			return false;
		} else {
		   is_exist();
		}
	} 

	function ajax_validate_edit() {
		if (validate_card_info() == false || validate_card_info_ar() == false  || validate_category() == false ) {
			return false;
		} else {
			is_exist_edit();
		}
	} 


	function validate_card_info() {
		var regExp = / /g;
		var str = $('#category_name_en').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Card name [En] is blank. Please enter card name [En].");
		return false;
		}
	}

	function validate_card_info_ar() {
		var regExp = / /g;
		var str = $('#category_name_ar').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Card name [Ar] is blank. Please enter card name [Ar].");
		return false;
		}
	}
	
	function validate_category() {
		
		var selectedCategory = new Array();
        var n = $(".chkBox:checked").length;
        if (n > 0){
            $(".chkBox:checked").each(function(){
                selectedCategory.push($(this).val());
            });
        }
		
		if (selectedCategory=='') {
			my_alert("Category is not selected. Please select category(s).");
		   return false;
		}
		return true;
	}
	
	

</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Students Cards <small> Management</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Students Cards Configuration</li>
    </ol>
    <!--/BREADCRUMB--> 
     <div style=" float:right; padding-right:10px;"> <button onclick="show_card_points()" title="Card Points" type="button" class="btn btn-primary">Cards Point Settings</button></div> 
    <div style="clear:both"></div>
  </section>
  
  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {

			$tbl_card_category_id       = $card_obj[0]['tbl_card_category_id'];
			$category_name_en           = $card_obj[0]['category_name_en'];
			$category_name_ar           = $card_obj[0]['category_name_ar'];
			$card_point                 = $card_obj[0]['card_point'];
			$school_type_ids            = $card_obj[0]['tbl_school_type_id'];
			$is_active                  = $card_obj[0]['is_active'];
			
			$tbl_school_type_id         = explode("||",$school_type_ids);
			
		/*	
			for($y=0;$y<count($school_type_id);$y++)
			{
				$tbl_school_type_id[$y] = $school_type_id[$y]['tbl_class_id'];
			} 
			
			
			print_r($tbl_school_type_id);*/
			
	?>
        <!--Edit-->
        
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Student Card</h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/student_cards_conf"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_edit" id="frm_listing" class="form-horizontal" method="post">
                 <div class="box-body">
                  
                   
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="category_name_en">Card Name [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Card Name [En]" id="category_name_en" name="category_name_en" class="form-control" value="<?=$category_name_en?>">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="category_name_ar">Card Name [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Card Name [Ar]" id="category_name_ar" name="category_name_ar" class="form-control" dir="rtl" value="<?=$category_name_ar?>">
                      </div>
                    </div>
                     <!--<div class="form-group">
                      <label class="col-sm-2 control-label" for="card_point">Points ( + or - sign with point) </label>
    
                      <div class="col-sm-10">
                        <input type="hidden" placeholder="Points ( + or - sign with point)" id="card_point" name="card_point" class="form-control" value="-1" >
                     </div>
                    </div>-->
                    <div class="form-group">
                     <label class="col-sm-2 control-label" for="tbl_school_type_id">Category<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-10" id="tbl_school_type_id"> 
                         <?php
                                    for ($u=0; $u<count($school_types); $u++) { 
                                        $tbl_school_type_id_u   = $school_types[$u]['tbl_school_type_id'];
                                        $school_type            = $school_types[$u]['school_type'];
                                        $school_type_ar         = $school_types[$u]['school_type_ar'];   
										
										if(in_array($tbl_school_type_id_u, $tbl_school_type_id, true)){
                                               $selClass = "checked";
										}else{
                                               $selClass = "";
										}
										
										
										?>  
                                       <input class="chkBox" type="checkbox" value="<?=$tbl_school_type_id_u?>" name="tbl_school_type_id" <?=$selClass?> >&nbsp;<?=$school_type?>&nbsp;[::]<?=$school_type_ar?>&nbsp;&nbsp;
                       
                        
                       <?php } ?>  
                       
                      <?php if(in_array('general', $tbl_school_type_id, true)){
                                               $selClass = "checked";
										}else{
                                               $selClass = "";
											   
										}  ?>
                       <input class="chkBox" type="checkbox" value="general" name="tbl_school_type_id" <?=$selClass?> >&nbsp;General&nbsp;[::]جنرال لواء&nbsp;&nbsp;       
                   </div>
                   </div>
                    
                    
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate_edit()">Save Changes</button>
                    <input type="hidden" name="card_category_id_enc" id="card_category_id_enc" value="<?=$tbl_card_category_id?>" />
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
		        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/student/student_cards_conf";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
    
  
 <link href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" rel="stylesheet">
 <script src="http://code.jquery.com/jquery-1.11.1.js"></script>
 <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
  <script>
  $( function() {
		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
				
				/* var order = $("#tabledivbody").sortable("serialize");
				
				alert(order);
				
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/your/url/here'
				});*/
				
				
				
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	
				
				
				
				
				
			}
	  } );
  
  } );
  
   function show_card_points()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/student_cards_points_conf/";
		window.location.href = url;
	}
	
  </script> 
 
    
                    <div id="mid1" class="box box-success">
                        <div class="box-header">
                          <div class="col-sm-1" >
                          <h3 class="box-title">SEARCH</h3>
                          </div>
                          <div class="col-sm-11"> 
                             
                               <div class="col-sm-6"><input name="q" id="q" value="<?=urldecode($q)?>" type="text" class="form-control" placeholder="Search By Card Name."   > </div>
                               <div class="col-sm-2"><button class="btn btn-success" type="button" onclick="search_data()">Search</button>&nbsp;<button class="btn btn-success" type="button" 
                               onclick="reset_data();">Reset</button>
                               </div>
                           
                          </div>
                        </div>  
                     </div>     
    
            <!--Listing-->
                    <div id="mid1_list" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Student Cards Configuration</h3>
                          <div class="box-tools">
                            <?php if (count($rs_student_cards)>0) { echo $paging_string;}?>	
                            <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
                            <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>
                          </div>
                        </div>
                        
                        <div class="box-body">
                     <!--   <div style="color:#030; font-weight:bold;">You can sort categories by using drag and drop of rows </div>-->
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle"><input id="select_all" type="checkbox" value="" /></th>
                              <!--<th width="10%" align="center" valign="middle">Sl No.</th>-->
                              <th width="25%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y">Card Name [En] <?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?><div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>
                              <th width="25%" align="center" valign="middle">Card Name [Ar]</th>
                              <th width="15%" align="center" valign="middle">Category</th>
                              <th width="10%" align="center" valign="middle">Status</th>
                              <th width="10%" align="center" valign="middle">Action</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                                for ($i=0; $i<count($rs_student_cards); $i++) { 
                                    $id                    = $rs_student_cards[$i]['id'];
                                    $tbl_card_category_id  = $rs_student_cards[$i]['tbl_card_category_id'];
                                    $category_name_en      = $rs_student_cards[$i]['category_name_en'];
                                    $category_name_ar      = $rs_student_cards[$i]['category_name_ar'];
                                    $card_point            = $rs_student_cards[$i]['card_point'];
									$added_date            = $rs_student_cards[$i]['added_date'];
                                    $is_active             = $rs_student_cards[$i]['is_active'];
									$classTypes            = $rs_student_cards[$i]['classTypes'];
									
                                    
                                    $title_en = ucfirst($title_en);
                                    $added_date = date('m-d-Y',strtotime($added_date));
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_card_category_id?>" >
                              <td align="left" valign="middle">
                              <span style="float:left;">
                              <input id="card_category_id_enc" name="card_category_id_enc" class="checkbox" type="checkbox" value="<?=$tbl_card_category_id?>" />
                              </span>
                              
                             <?php /*?> <span style="float:left;">&nbsp;
                              <?php if($i<>0){ ?> <i class="fa fa-arrow-up"  style="color:#3c8dbc; cursor:pointer;"  aria-hidden="true" title="Sorting - Drag & Drop To Up"></i> &nbsp; <?php } ?>
                               <?php if($i<> count($rs_all_categories)-1){ ?> <i class="fa fa-arrow-down" style="color:#3c8dbc;cursor:pointer;" aria-hidden="true" title="Sorting - Drag & Drop To Down"></i> <?php } ?>
                              </span><?php */?>
                              </td>
                             <!-- <td align="left" valign="middle"><?=$offset+$i+1?></td>-->
                              <td align="left" valign="middle"><?=$category_name_en?></td>
                              <td align="left" valign="middle"><?=$category_name_ar?></td>
                              <td align="left" valign="middle"><?=$classTypes?></td>
                              <td align="left" valign="middle">
                                <div id="act_deact_<?=$tbl_card_category_id?>">
                                <?php if (trim($is_active) == "Y") { ?>
                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_card_category_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                <?php } else { ?>
                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_card_category_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                <?php } ?>
                                </div>
                              </td>
                              <td align="left" valign="middle">
                                <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/edit_student_card/card_category_id_enc/<?=$tbl_card_category_id?>"><button class="btn bg-purple fa fa-pencil" type="button" title="Edit"></button></a>
                              </td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="8" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_student_cards)<=0) {
                            ?>
                            <tr>
                              <td colspan="8" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no cards available. Click on the + button to create one.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
    
            <!--Add or Create-->
              <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Student Card</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                  
                   
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="category_name_en">Card Name [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Card Name [En]" id="category_name_en" name="category_name_en" class="form-control">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="category_name_ar">Card Name [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Card Name [Ar]" id="category_name_ar" name="category_name_ar" class="form-control" dir="rtl">
                      </div>
                    </div>
                     <!--<div class="form-group">
                      <label class="col-sm-2 control-label" for="card_point">Points ( + or - sign with point) </label>
    
                      <div class="col-sm-10">-->
                        <input type="hidden" placeholder="Points ( + or - sign with point)" id="card_point" name="card_point" class="form-control" value="-1" >
                     <!-- </div>
                    </div>-->
                    
                    <div class="form-group">
                     <label class="col-sm-2 control-label" for="tbl_school_type_id">Category<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-10" id="tbl_school_type_id"> 
                         <?php
                                    for ($u=0; $u<count($school_types); $u++) { 
                                        $tbl_school_type_id_u   = $school_types[$u]['tbl_school_type_id'];
                                        $school_type            = $school_types[$u]['school_type'];
                                        $school_type_ar         = $school_types[$u]['school_type_ar'];   ?>  
                        <input class="chkBox" type="checkbox" value="<?=$tbl_school_type_id_u?>" name="tbl_school_type_id">&nbsp;<?=$school_type?>&nbsp;[::]<?=$school_type_ar?>&nbsp;&nbsp;
                        <?php } ?> 
                        <input class="chkBox" type="checkbox" value="general" name="tbl_school_type_id">&nbsp;General&nbsp;[::]جنرال لواء&nbsp;&nbsp;   
                             
                   </div>
                   </div>
                    
                    
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate()">Submit</button>
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
            <!--/Add or Create-->
                
        <!--/Admin Category Management-->

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>

        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >
function search_data() {
		var q = $("#q").val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/student_cards_conf/";
		
		if(q !='')
			url += "q/"+q+"/";
		
			url += "offset/0/";
		window.location.href = url;
	}

function reset_data() {
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/student_cards_conf/";
		url += "offset/0/";
		window.location.href = url;
	}


</script>