<?php
//Init Parameters
$tbl_book_id_enc = md5(uniqid(rand()));
$tbl_uploads_id = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}


?>
<link rel="stylesheet" media="all" type="text/css" href="<?=JS_PATH?>/date_time_picker/css/jquery-ui-1.8.6.custom.css" />
<style type="text/css">
pre {
	padding: 20px;
	background-color: #ffffcc;
	border: solid 1px #fff;
}
.example-container {
	background-color: #f4f4f4;
	border-bottom: solid 2px #777777;
	margin: 0 0 40px 0;
	padding: 20px;
}

.example-container p {
	font-weight: bold;
}

.example-container dt {
	font-weight: bold;
	height: 20px;
}

.example-container dd {
	margin: -20px 0 10px 100px;
	border-bottom: solid 1px #fff;
}

.example-container input {
	width: 150px;
}

.clear {
	clear: both;
}

#ui-datepicker-div {
}

.ui-timepicker-div .ui-widget-header {
	margin-bottom: 8px;
}

.ui-timepicker-div dl {
	text-align: left;
}

.ui-timepicker-div dl dt {
	height: 25px;
}

.ui-timepicker-div dl dd {
	margin: -25px 0 10px 65px;
}

.ui-timepicker-div td {
	font-size: 90%;
}
</style>

<script type="text/javascript" src="<?=JS_PATH?>/date_time_picker/js/jquery-ui-1.8.6.custom.min.js"></script>
<script type="text/javascript" src="<?=JS_PATH?>/date_time_picker/js/jquery-ui-timepicker-addon.js"></script>
<script language="javascript">
	$(document).ready(function(){
		$('#start_date').datepicker({
			dateFormat: '',
			timeFormat: 'hh:mm tt',
			timeOnly: true   
		});


		$('#end_date').datepicker({
			dateFormat: '',
			timeFormat: 'hh:mm tt',
			timeOnly: true   
		});
	});
</script>
<script type="text/javascript" src="<?=JS_COLOR_PATH?>/jscolor.js"></script>

<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
	    $('#select_all_class').on('click',function(){
			if(this.checked){
				$('.checkboxClass').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkboxClass').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
			
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/books/index";
			}
		})
	});
	
function confirm_delete_popup() {
		var len = $("input[id='book_id_enc']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more book(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/books/delete_book",
			data: {
				book_id_enc: $("input[id='book_id_enc']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Book(s) deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}	
	function ajax_activate(book_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/books/activate_book",
			data: {
				bid: book_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Book activated successfully.", 'green')

				$('#act_deact_'+book_id_enc).html('<span style="cursor:pointer" onClick="ajax_deactivate(\''+book_id_enc+'\')" onMouseOver="deactivate_me(this)" onMouseOut="reset_activate(this)" class="label label-success">Active</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_deactivate(book_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/books/deactivate_book",
			data: {
				bid: book_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Book de-activated successfully.", 'green')
				
				$('#act_deact_'+book_id_enc).html('<span style="cursor:pointer" onClick="ajax_activate(\''+book_id_enc+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/books/is_exist_book",
			data: {
			    bid: "<?=$book_id_enc?>",
				title_en: $('#title_en').val(),
				is_ajax: true
			},
			success: function(data) {
				
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='T') {
					refresh_page = "N";
					my_alert("Book is already exists.", 'red');
					$("#pre-loader").hide();
				}else if (temp=='Y') {
					refresh_page = "N";
					my_alert("Book is already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_add_topic();
				}
			
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist_edit() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/books/is_exist_book",
			data: {
			    bid: $('#book_id_enc').val(),
				title_en: $('#title_en').val(),
				is_ajax: true
			},
			success: function(data) {
				
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='T') {
					refresh_page = "N";
					my_alert("Book is already exists.", 'red');
					$("#pre-loader").hide();
				}else if (temp=='Y') {
					refresh_page = "N";
					my_alert("Book is already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_update_book();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	


	function ajax_update_book() {
			
		var selectednumbers = "";
				$('.checkboxClass:checked').each(function(i){
				  selectednumbers += $(this).val()+"&";
				});	
	
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/books/edit_book",
			data: {
			  			tbl_book_id: $("#book_id_enc").val(),
						tbl_book_category_id  : $("#tbl_book_category_id").val(),
						tbl_language_id: $("#tbl_language_id").val(), 
						title_en: $("#title_en").val(),
						title_ar: $("#title_ar").val(),
						author_name_en: $("#author_name_en").val(),
						author_name_ar: $("#author_name_ar").val(),
						isbn: $("#isbn").val(),
						description_en: $("#description_en").val(),
						description_ar: $("#description_ar").val(),
						tbl_class_id: selectednumbers,
						is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Changes saved successfully.", 'green');
				
				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
</script>
<script language="javascript">

	function ajax_validate() {
		if(val_title() == false || val_tbl_book_category_id() == false  ||  val_tbl_language_id() == false   ||  val_author() == false ||  val_cover_pic() == false ||  val_book_file() == false ||  val_description() == false ) {
			return false;
		} else {
			saveBook();
		}	
		
	}
	
    //validate topic
	
	function ajax_validate_edit() {
		if(val_title() == false || val_tbl_book_category_id() == false  ||  val_tbl_language_id() == false   ||  val_author() == false ||  val_cover_pic() == false ||  val_book_file() == false ||  val_description() == false ) {
			return false;
		} 
		else{
			is_exist_edit();
		}
	} 
	
  /************************************* START MESSAGE VALIDATION *******************************/

        function val_tbl_book_category_id() {
			var strCat = $("#tbl_book_category_id").val();	
			if(strCat==""){
				my_alert("Please select book category");
				return false;
			}
			return true;
		}
		
		//VALIDATE LANGUAGE
        function val_tbl_language_id() {
			var strLan = $("#tbl_language_id").val();	
			if(strLan==""){
				my_alert("Please select book language");
				return false;
			}
			return true;
		}
		
		//VALIDATE title
		function val_title() {
			var strTitleEn = $("#title_en").val();
			var strTitleAr = $("#title_ar").val();	
			
		    if(strTitleAr==""){
				my_alert("Title[Ar] is blank. Please enter title in Arabic");
				return false;
			}
			
			if(strTitleEn==""){
				my_alert("Title[En] is blank. Please enter title in English");
				return false;
			}
			return true;
		}
		
		function val_author() {
			var strAuthorEn = $("#author_name_en").val();
			var strAuthorAr = $("#author_name_ar").val();	
			
			if(strAuthorAr==""){
				my_alert("Author[Ar] is blank. Please enter author in Arabic");
				return false;
			}
			
			if(strAuthorEn==""){
				my_alert("Author[En] is blank. Please enter author in English");
				return false;
			}
			
			return true;
		}
		
		
		function val_isbn() {
			var strISBN = $("#isbn").val();
			
			if(strISBN==""){
				my_alert("ISBN is blank. Please enter ISBN");
				return false;
			}
			return true;
		}
		
		
		
		function val_cover_pic() {
			var strCoverPic = $("#cover_pic").val();
			if(strCoverPic == ""){
				my_alert("Please upload cover picture");
				return false;
			}
			return true;
		}
		
		function val_book_file() {
			var strCoverPic = $("#book_file").val();
			if(strCoverPic==""){
				 my_alert("Please upload cover picture");
				return false;
			}
			return true;
		}
		
		
		function val_description() {
			var strBookDescEn = $("#description_en").val();
			var strBookDescAr = $("#description_ar").val();	
			
			if(strBookDescAr==""){
				 my_alert("Please enter book description in Arabic");
				return false;
			}
			
			if(strBookDescEn==""){
				my_alert("Please enter book description in English");
				return false;
			}
			return true;
		}
		
	
		//VALIDATE CHECK LOGIN 
		  function saveBook()
		  {
			var selectednumbers='';
				$('#tbl_class_id :selected').each(function(i, selected) {
					selectednumbers += $(selected).val()+"&";
				});
				
				
				try { 
					$.ajax({
					type: "POST",
					dataType:"text",
					url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/books/add_book",
					data: {
						tbl_book_id: $("#tbl_book_id").val(),
						tbl_book_category_id  : $("#tbl_book_category_id").val(),
						tbl_language_id: $("#tbl_language_id").val(), 
						title_en: $("#title_en").val(),
						title_ar: $("#title_ar").val(),
						author_name_en: $("#author_name_en").val(),
						author_name_ar: $("#author_name_ar").val(),
						isbn: $("#isbn").val(),
						description_en: $("#description_en").val(),
						description_ar: $("#description_ar").val(),
						tbl_class_id: selectednumbers,
						is_ajax: true
					},
					success: function(data) {
						
						if(data=="X")
						{
							refresh_page = "N";
							my_alert("Book is already exist, Please try another name.", 'red');
							$("#pre-loader").hide();
						}else if(data=="N")
						{
							refresh_page = "N";
							my_alert("Book couldn't added, Please try once again.", 'red');
							$("#pre-loader").hide();
						}else{
							refresh_page = "Y";
							my_alert("Book added successfully.", 'green');
							$("#pre-loader").hide();
						}
					},
					error: function() {
						$('#pre_loader').css('display','none');	
					}, 
					complete: function() {
						$('#pre_loader').css('display','none');	
					}
		
					});
				} catch(e) {
					//alert(e)	
				}	
		  }
        </script>

</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>
 <link href="<?=HOST_URL?>/assets/admin/dist/css/uploadfile.min.css" rel="stylesheet">
             <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery.uploadfile.min.js"></script>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Books List<small></small></h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Books List</li>
    </ol>
    <!--/BREADCRUMB--> 
         <div style=" float:right; padding-right:10px;"> <button onclick="show_categories()" title="Categories" type="button" class="btn btn-primary">Categories</button></div>

    <div style="clear:both"></div>
  </section>
   
  <script>
   function show_categories()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/books/list_category/";
		window.location.href = url;
	}
 </script>
  <section class="content"> 

    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {
			$tbl_book_id             = $getBookInfo[0]['tbl_book_id'];
			$tbl_book_category_id    = $getBookInfo[0]['tbl_book_category_id'];
			$book_language           = $getBookInfo[0]['book_language'];
			$isbn                    = $getBookInfo[0]['isbn'];
			$title_en                = $getBookInfo[0]['title_en'];
			$title_ar                = $getBookInfo[0]['title_ar'];
			$author_name_en          = $getBookInfo[0]['author_name_en'];
			$author_name_ar          = $getBookInfo[0]['author_name_ar'];
			$description_ar          = $getBookInfo[0]['description_ar'];
			$description_en          = $getBookInfo[0]['description_en'];
			$book_file_name          = $getBookInfo[0]['book_file_name'];
			$cover_pic               = $getBookInfo[0]['cover_pic'];
			/*$tbl_parent_group_id     = $topic_obj['tbl_parent_group_id'];
			$group_id_array          = explode("||",$tbl_parent_group_id);*/
			$cover_pic_path         = HOST_URL."/uploads/books/".$getBookInfo[0]['cover_pic'];
			$book_file_name         = $getBookInfo[0]['book_file_name'];
			$book_file_path         = HOST_URL."/uploads/books/".$getBookInfo[0]['book_file_name'];

	?>
        <!--Edit-->
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Book</h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/books/index"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_edit" id="frm_listing" class="form-horizontal" method="post">
                    <div class="box-body">
                   
                      <div class="form-group"> 
                     <label class="col-sm-2 control-label" for="title_ar">Title [Ar]</label>
                        <div class="col-sm-4">
                                <input type="text" name="title_ar" id="title_ar"  placeholder="Title [Ar]" class="form-control"  dir="rtl" tabindex="3" value="<?=$title_ar?>">
                         </div>  
                         
                      <label class="col-sm-2 control-label" for="title_en">Title [En]</label>
                      <div class="col-sm-4">
                        <input type="text" placeholder="Title[En]" id="title_en" name="title_en" class="form-control" value="<?=$title_en?>" >
                      </div>
                      
                   </div>
                   
                   <div id="book_upload" style="display:block;">
                    <div class="form-group"> 
                     <label class="col-sm-2 control-label" for="tbl_book_category_id">Category</label>
                        <div class="col-sm-4">
	                        	<select id="tbl_book_category_id" name="tbl_book_category_id" class="form-control" tabindex="1" autofocus>
                                <option value="">--SELECT--</option>
                                <?php for($m=0;$m<count($list_category);$m++){?>
                                <option value="<?php echo $list_category[$m]['tbl_book_category_id']; ?>" 
								<?php if($tbl_book_category_id == $list_category[$m]['tbl_book_category_id']) { echo "selected"; } ?> ><?php echo $list_category[$m]['category_name_en']; ?> - <?php echo $list_category[$m]['category_name_ar']; ?></option>
                                <?php } ?>
                                </select>
                        </div> 
                        
                        
                        <label class="col-sm-2 control-label" for="tbl_language_id">Language</label>
                        <div class="col-sm-4">
	                        	<select id="tbl_language_id" name="tbl_language_id" class="form-control" tabindex="2" autofocus>
                                 <option value="">--SELECT--</option>
                                <option value="ar" <?php if($book_language == 'ar') { echo "selected"; } ?>  >Arabic</option>
                                <option value="en" <?php if($book_language == 'en') { echo "selected"; } ?>  >English</option>
                               </select>
                        </div>
                     </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="author_name_ar">Author Name [Ar]</label>
                      <div class="col-sm-4">
                        <input type="text" placeholder="Author Name[Ar]" id="author_name_ar" name="author_name_ar" class="form-control" dir="rtl" value="<?=$author_name_ar?>">
                      </div> 
                      
                      <label class="col-sm-2 control-label" for="author_name_en">Author Name [En]</label>
                      <div class="col-sm-4">
                        <input type="text" placeholder="Author Name[En]" id="author_name_en" name="author_name_en" class="form-control" value="<?=$author_name_en?>">
                      </div>
                    </div>
                   
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="isbn">ISBN (International Standard Book Number)</label>
                      <div class="col-sm-4">
                        <input type="text" placeholder="ISBN" id="isbn" name="isbn" class="form-control" value="<?=$isbn?>">
                      </div>
                    </div>
                    
                   <div class="form-group">
                      <label class="col-sm-2 control-label" for="cover_pic">Cover Picture</label>
    
                        <div class="col-sm-4">
                             
                        <!--File Upload START-->
                            <style>
                            #advancedUpload1 {
                                padding-bottom:0px;
                            }
                            </style>
                                 
                            <div id="advancedUpload1">Upload File</div>
                            
                            <div id="uploaded_items1" >
                                <div id="div_listing_container1" class="listing_container" style="display:block">	            
										<?php
                                            if (trim($cover_pic_path) != "") {
                                        ?>
                                                    <div id='<?=$tbl_book_id?>1' class='box-header'>
                                                      <div class='box-title'><a href="<?=$cover_pic_path?>" target="_blank"> <img src="<?=$cover_pic_path?>" height="100" width="100" style="border:1px solid #CCC;" /></a></div>
                                                      <div class='box-tools'> 
                                                      
                                                    
                                                      <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick="confirm_delete_img_popup1('<?=$tbl_book_id?>')" tabindex="8">
                                                        </button>
                                                      </div>
                                                    </div>
											<style>
												.ajax-upload-dragdrop1 {
													display:none;	
												}
                                            </style>        
                                        <?php		
                                            }
                                        ?>
                                </div>        
                            </div>
                        <!--File Upload END-->
                      </div>
                              
               
                      
                        <label class="col-sm-2 control-label" for="cover_pic">Upload Book</label>
                        <div class="col-sm-4">
                             
                        <!--File Upload START-->
                            <style>
                            #advancedUpload {
                                padding-bottom:0px;
                            }
                            </style>
                                 
                            <div id="advancedUpload">Upload File</div>
                            
                            <div id="uploaded_items" >
                                <div id="div_listing_container" class="listing_container" style="display:block">	            
										<?php
                                            if (trim($book_file_path) != "") {
                                        ?>
                                                    <div id='<?=$tbl_book_id?>' class='box-header '>
                                                      <div class='box-title'>  <a href="<?=$book_file_path?>" target="_blank"><img src="<?=IMG_PATH_DEFAULT?>/epub_icon.png" width="100" height="100"/><br>
                              <?=$title_en?>.epub</a>
                                                      </div>
                                                      <div class='box-tools'> <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick="confirm_delete_img_popup('<?=$tbl_book_id?>')"  tabindex="9" >
                                                        </button>
                                                      </div>
                                                    </div>
											<style>
												.ajax-upload-dragdrop {
													display:none;	
												}
                                            </style>        
                                        <?php		
                                            }
                                        ?>
                                </div>        
                            </div>
                        <!--File Upload END-->
                      </div>
                       
                      
                    </div>
                  
                   <div class="form-group"> 
                     <label class="col-sm-2 control-label" for="description_ar">Description [Ar]</label>
                        <div class="col-sm-4">
                         <textarea type="text" name="description_ar" id="description_ar"  placeholder="Description [Ar]" class="form-control"  dir="rtl" tabindex="3" ><?=$description_ar?></textarea>
                         </div> 
                         
                         <label class="col-sm-2 control-label" for="description_en">Description [En]</label>
                         <div class="col-sm-4">
                                <textarea type="text" name="description_en" id="description_en"  placeholder="Description [En]" class="form-control"  tabindex="3" ><?=$description_en?></textarea>
                         </div>
                   
                    </div>
                     
                </div>
                 
                 <div class="form-group">
                     <label class="col-sm-2 control-label" for="description_en"></label>
                     <div class="col-sm-10">
                        <div id="divClassList" onclick="openClassList()" style=" display:block; color:#360; font-weight:bold; text-decoration:underline; cursor:pointer;">Show Assigned Class List</div> 
                     </div>
                 </div>  
                 
                  <?php $class_array = array(); 
						for($y=0;$y<count($assign_classes_obj);$y++)
						{
							$class_array[$y] = $assign_classes_obj[$y]['tbl_class_id'];
						} 
						
						?> 
                 
                 <div id="class_list" style="display:none;">
                    
                    <div class="form-group" id="all_student">
                          <label class="col-sm-2 control-label" for="class_list"></label>
                          <div class="col-sm-10" id="divClass" style="height:100%; overflow-y:scroll;">
                          <div class"col-sm-10" >
	 
                        <?php  if(count($classes_list)>0){ ?>
                           <div style="padding-bottom:10px;background-color:#e8eaeb;" > 
                           <input type="checkbox" value="" id="select_all_class" class="checkboxClass" style="margin-left:10px;" >&nbsp;Select All</div>
                        <?php  } ?>
                        <?php
                            for ($u=0; $u<count($classes_list); $u++) { 
								$tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
								$class_name             = $classes_list[$u]['class_name'];
								$class_name_ar          = $classes_list[$u]['class_name_ar'];
								$section_name           = $classes_list[$u]['section_name'];
								$section_name_ar        = $classes_list[$u]['section_name_ar'];
								
								if(in_array($tbl_class_id_u, $class_array, true))
                                    $checkClass = "checked";
                                else
                                    $checkClass = "";
                             ?>
                             <div class="col-sm-3" style="padding-top:10px; padding-bottom:10px; border:1px solid #CCC;"> 
								  <input id="tbl_class_id_<?=$u?>" name="tbl_class_id[]" class="checkboxClass" type="checkbox" value="<?=$tbl_class_id_u?>" <?=$checkClass?>  />&nbsp;
								   <?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;<?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                             </div>
							<?php  
	                         }
							 ?>
	                          </div> 
                        </div>
                        </div>    
                   
                    </div>   
                    </div>
                    
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate_edit()">Save Changes</button>
                    <input type="hidden" name="book_id_enc" id="book_id_enc" value="<?=$tbl_book_id?>" />
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
	 <script language="javascript">

var item_id = "<?=$tbl_book_id?>";//Primary Key for a Form. 
if(item_id=="")
  var item_id = $("#book_id_enc").val();

function set_item_id(obj) {
	item_id = obj.value;
	get_files();	
}

$(document).ready(function() {
	/************ Upload cover picture ***********************/
	var uploadObj = $("#advancedUpload").uploadFile({
		url:"<?=HOST_URL?>/file_mgmt/book_file_upload",
		multiple:true,
		autoSubmit:true,
		maxFileSize:1300000000,
		fileName:"myfile",
		formData: {"module_name":"book"},
		dynamicFormData: function() {
			var data = { item_id:item_id}
			return data;
		},
		showStatusAfterSuccess:false,
		dragDropStr: "<span class='d_d_text'>Optionally Drag and Drop the File to Upload.</span>",
		abortStr:"Abourt",
		cancelStr:"Cancel",
		doneStr:"Done",
		multiDragErrorStr: "Multi Drag Error.",
		extErrorStr:"Extention Error:",
		sizeErrorStr:"Max Size Error:",
		uploadErrorStr:"Upload Error",
		onSelect:function(files) {
 		},
		onSubmit:function(files) {
 		},
		onSuccess:function(files, data, xhr) {
			if (data == "error") {
				alert("Error uploading file. Please try again.");
				return;
			}
			var obj = JSON.parse(data);
			var tbl_uploads_id = obj.tbl_uploads_id;
			var file_name_updated = obj.file_name_updated;
			
			//alert("tbl_uploads_id: "+tbl_uploads_id)
			//alert("file_name_updated: "+file_name_updated)
			add_uploaded_item(tbl_uploads_id, file_name_updated);
		},
		afterUploadAll:function() {
 		},
		onError: function(files, status, errMsg) {
 		}
	});

	$("#startUpload").click(function() {
		uploadObj.startUpload();
	});
	
	try { 
		$('input[type=file]').click();
	} catch(e) {
		alert(e)
	}
	
	/************** End Upload Cover Picture *************/
	
	/************* Start Upload Book Epub file **********/
	
	var uploadObj1 = $("#advancedUpload1").uploadFile({
		url:"<?=HOST_URL?>/file_mgmt/cover_photo_upload",
		multiple:true,
		autoSubmit:true,
		maxFileSize:130000,
		fileName:"myfile",
		acceptFiles:"image/*",
		formData: {"module_name":"book"},
		dynamicFormData: function() {
			var data = { item_id:item_id}
			return data;
		},
		showStatusAfterSuccess:false,
		dragDropStr: "<span class='d_d_text'>Optionally Drag and Drop the File to Upload.</span>",
		abortStr:"Abourt",
		cancelStr:"Cancel",
		doneStr:"Done",
		multiDragErrorStr: "Multi Drag Error.",
		extErrorStr:"Extention Error:",
		sizeErrorStr:"Max Size Error:",
		uploadErrorStr:"Upload Error",
		onSelect:function(files) {
 		},
		onSubmit:function(files) {
 		},
		onSuccess:function(files, data, xhr) {
			if (data == "error") {
				alert("Error uploading file. Please try again.");
				return;
			}
			var obj = JSON.parse(data);
			var tbl_uploads_id = obj.tbl_uploads_id;
			var file_name_updated = obj.file_name_updated;
			
			//alert("tbl_uploads_id: "+tbl_uploads_id)
			//alert("file_name_updated: "+file_name_updated)
			add_uploaded_item1(tbl_uploads_id, file_name_updated);
		},
		afterUploadAll:function() {
 		},
		onError: function(files, status, errMsg) {
 		}
	});

	$("#startUpload").click(function() {
		uploadObj1.startUpload();
	});
	
	try { 
		$('input[type=file]').click();
	} catch(e) {
		alert(e)
	}
	
	/***************** End Upload epub book file ********************/
	
	
	
});

//Function called when file is uploaded
function add_uploaded_item(tbl_uploads_id, file_name_updated) {
	var str = "<div id='"+tbl_uploads_id+"' class='box-header'> <div class='box-title'><a target='_blank'  href='<?=IMG_PATH_BOOK?>/"+file_name_updated+"' ><img src='<?=IMG_PATH_DEFAULT?>/epub_icon.png' /><br>"+file_name_updated+"</a></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_img_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
		
	$("#div_listing_container").show();
	$("#div_listing_container").append(str);
	$(".ajax-upload-dragdrop").hide();//Hide the upload button
return;
}

function add_uploaded_item1(tbl_uploads_id, file_name_updated) {
	var str = "<div id='"+tbl_uploads_id+"' class='box-header'> <div class='box-title'><img src='<?=IMG_PATH_BOOK?>/"+file_name_updated+"' /></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_img_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
		
	$("#div_listing_container1").show();
	$("#div_listing_container1").append(str);
	$(".ajax-upload-dragdrop1").hide();//Hide the upload button
return;
}





function confirm_delete_img_popup(tbl_uploads_id) {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		$('#'+tbl_uploads_id).hide();	
		$(".ajax-upload-dragdrop").show();
		
		var url_str = "<?=HOST_URL?>/file_mgmt/delete_file";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					tbl_uploads_id: tbl_uploads_id
				},
			success: function(data) {
				$("#pre-loader").hide();
			}
		});	
	} else {
		$("#pre-loader").hide();		
	}
}

function confirm_delete_img_popup1(tbl_uploads_id) {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		$('#'+tbl_uploads_id+'1').hide();	
		$(".ajax-upload-dragdrop1").show();
		
		var url_str = "<?=HOST_URL?>/file_mgmt/delete_file";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					tbl_uploads_id: tbl_uploads_id
				},
			success: function(data) {
				$("#pre-loader").hide();
			}
		});	
	} else {
		$("#pre-loader").hide();		
	}
}
</script>
   	        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/forum/all_topics";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  

  
 <link href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" rel="stylesheet">
 <script src="http://code.jquery.com/jquery-1.11.1.js"></script>
 <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
  <script>
  $( function() {
		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
				
				/* var order = $("#tabledivbody").sortable("serialize");
				
				alert(order);
				
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/your/url/here'
				});*/
				
				
				
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	

				
				
				
				
				
			}
	  } );
  
  } );
  </script> 
            <!--Listing-->
 <?php } ?>
 </div>
<script>
       function openClassList()
	   {
		 if($('#class_list').is(':visible')){ 
		 	$('#class_list').hide();
			$('#book_upload').show();
			$('#divClassList').html("Show Assigned Class List");
		 }else{
			$('#class_list').show();
			$('#divClassList').html("Hide Assigned Class List");
			$('#book_upload').hide();
		 }
	   }
</script>
<script language="javascript" >
function search_data() {
		var q = $("#q").val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/books/index/";
		
		if(q !='')
			url += "q/"+q+"/";
		
			url += "offset/0/";
		window.location.href = url;
	}

function reset_data() {
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/books/index/";
		url += "offset/0/";
		window.location.href = url;
	}
</script>
