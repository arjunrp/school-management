<?php $css = " login-page";
    include(ROOT_ADMIN_PATH."/admin/include/header.php");
?>

<script language="javascript">
	function validateForm() {
		if ( validate_username() == false || validate_password() == false) {
			return false;
		} else {
			ajax_validate_and_login_user();
		}
	}//function validateForm

/*	function validate_email() {
		var regExp = / /g;
		var str = $('#email').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Email is blank. Please enter email.");
		return false;
		}
	
		if (!isNaN(str)) {
			my_alert("Invalid Email.");
			return false;
		}
	
		if(str.indexOf('@', 0) == -1) {
			my_alert("Invalid Email.");
			return false;
		}
	}
	*/
	
	function validate_username() {
		var regExp = / /g;
		var str = $('#username').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Username is blank. Please enter username.");
		return false;
		}
	}

	function validate_password() {
		var regExp = / /g;
		var str = $('#password').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Password is blank. Please enter password.");
		return false;
		}
	}
	
	function ajax_validate_and_login_user() {
		$('#my_button').addClass("fa fa-spin fa-refresh");//For spinner
		
		var url = "";
		
		<?php
			if (trim($user_type) == "T") {
		?>
			url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/ajax_validate_and_login_teacher";
		<?php		
			} else {
		?>
			url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/admin_user/ajax_validate_and_login_user";
		<?php			
			}
		?>
		
		$.ajax({
		type: "POST",
		url: url,
		dataType: "html",
		data: {
			username: $('#username').val(),
			password: $('#password').val(),
			remember_me: $('#remember_me').val(),
			is_ajax: true
			
		},
		success: function(data) {
			var temp = new String();
			temp = data;
			temp = temp.trim();
			
			if (temp=='*N*') {
				my_alert("Invalid Username or Password");
			} else if (temp=='*Y*') {
				
				
			<?php
					if (trim($user_type) == "T") {
				?>
					window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/home/teacher";
				<?php		
					} else {
				?>
					  window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/home/welcome";
				<?php			
					}
				?>
			}
		},
		error: function() {
		}, 
		complete: function() {
		}
		});
	}
</script>
<style>
ul.tab {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #f1f1f1;
}

/* Float the list items side by side */
ul.tab li {float: left; text-align:center; width:50%; border: 1px solid #fff;}

/* Style the links inside the list items */
ul.tab li a {
    display: inline-block;
    color: grey;
    text-align: center;
    padding: 14px 0;
    text-decoration: none;
    transition: 0.3s;
    font-size: 17px;
	width:100% !important;
	
}

/* Change background color of links on hover */
ul.tab li a:hover {width:100% !important; background-color: #ddd; color: #000; }

/* Create an active/current tablink class */
ul.tab li a:focus, .active {width:100% !important; background-color:#3c8dbc; color:#fff !important; } /*#c3a336*/

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}

.btnLogin{
	background-color:#3c8dbc;/*#ba3a39*/
	color:#fff;
}
</style>


<div class="login-box">
  <div class="login-logo">
  
  <img src="<?=HOST_URL?>/admin/images/inner_header_logo.png">
  
	<!--Logo goes here--> 
    <?php if (trim($user_type) == "T"){ 
	    $teacherStatus = "active";
		$adminStatus = "";
		$user_mode = "teacher";
			 if(LAN_SEL=="en"){
				 $loginHead = '<b>Teacher</b> Login</b>';
			 }else{
				 $loginHead = '<b> المعلم الدخول </b>';
			 } 
	 
	   
	 }else{
		$teacherStatus = "";
		$adminStatus = "active";	
		$user_mode = "admin";
		$user_type = "A";
		 if(LAN_SEL=="en"){
				$loginHead = '<b>Admin</b>istration  Login';
		 }else{
			 $loginHead = '<b>الاداري المسئول</b>';
		 }
	 } 
	 
	 echo $loginHead;
	?>
  </div>
  
  <ul class="tab">
      <?php if(LAN_SEL=="en"){?>
      <li><a href="javascript:void(0)" class="tablinks <?=$adminStatus?> " <?php if($adminStatus=="") { ?> onclick="getPanel('admin')" <?php } ?> >ADMINISTRATION</a></li>
      <li><a href="javascript:void(0)" class="tablinks <?=$teacherStatus?> " <?php if($teacherStatus=="") { ?> onclick="getPanel('teacher')" <?php } ?> >TEACHER</a></li>
      <?php } else { ?>
      <li><a href="javascript:void(0)" class="tablinks <?=$teacherStatus?> " <?php if($teacherStatus=="") { ?> onclick="getPanel('teacher')" <?php } ?> >المعلم</a></li>
<li><a href="javascript:void(0)" class="tablinks <?=$adminStatus?> " <?php if($adminStatus=="") { ?> onclick="getPanel('admin')" <?php } ?> >
إدارة</a></li>
      <?php } ?>
      
    </ul>
  
  
  <!-- /.login-logo -->
  <div class="login-box-body">
     <select name="tbl_language_id" id="tbl_language_id" class="form-control" onchange="getLanPanel('<?=$user_mode?>',this.value);">
     <option value="en" <?php if(LAN_SEL=="en"){?> selected <?php } ?> >English</option>
     <option value="ar" <?php if(LAN_SEL=="ar"){?> selected <?php } ?> >العربية</option>
     </select> 
    <!--<p class="login-box-msg">Sign in to start your session</p>-->
    <br />
    <form action="<?=HOST_URL?>/assets/admin/index2.html" method="post">
      <div class="form-group has-feedback">
      
        <?php
		 if(LAN_SEL=="en"){ 
		 	$placeHolderUser = 'placeholder="Username"'; 
			$placeHolderPass =  'placeholder="Password"'; 
			$submitLink = 'Sign In';
			$submitStyle= ' style="margin-right:5px" ';
			$submitDivAr = '';
		 }else {    
		 	$placeHolderUser = ' placeholder="معرف المستخدم" dir="rtl" ';
			$placeHolderPass = ' placeholder="كلمه السر" dir="rtl" ';
			$submitLink = 'تسجيل الدخول ';
			$submitStyle = '';
			$submitDivAr = 'style="padding-right: 0 !important;"';
		 } ?>
         
        <input id="username" name="username" type="text" class="form-control"  <?=$placeHolderUser?> >
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input id="password" name="password" type="password" class="form-control" <?=$placeHolderPass?> >
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
     <div class="row">
        <!-- <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input id="remember_me" name="remember_me" type="checkbox"> Remember Me
            </label>
          </div>
        </div>-->
        <!-- /.col -->
        <div class="col-xs-4" <?=$submitDivAr?> >
          <button type="button" class="btn btnLogin btn-block btn-flat" onClick="validateForm()">
          <i id="my_button"  <?=$submitStyle?> ></i>
          	<?=$submitLink?>
            </li>
          </button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- iCheck -->
<script src="<?=HOST_URL?>/assets/admin/plugins/iCheck/icheck.min.js"></script>

<script>
  $(function () {
	$('input').iCheck({
		checkboxClass: 'icheckbox_square-blue',
		radioClass: 'iradio_square-blue',
		increaseArea: '20%' // optional
	});
	//show_modal();
  });
</script>

<script language="javascript">
function show_modal() {
	$('#alert_box').modal('show');
}

function getPanel(panel)
{
	
	if(panel=="admin")
	{
		
		window.parent.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/";
		
	}else if(panel=="teacher")
	{
		window.parent.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/";
		
	}
}

function getLanPanel(user_mode,lan)
{
	
	if(user_mode=="admin")
	{
		window.parent.location.href = "<?=HOST_URL?>/"+lan+"/admin/";
	}else if(user_mode=="teacher")
	{
		window.parent.location.href = "<?=HOST_URL?>/"+lan+"/admin/teacher/";
	}
	
}

</script>

<?php
include(ROOT_ADMIN_PATH."/admin/include/footer.php");
?>