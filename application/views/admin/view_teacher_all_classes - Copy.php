<?php
$dictionary_id_enc = md5(uniqid(rand()));
if (trim($mid) == "") {
	$mid = "1";	
}
if ($module_id == "m_give_points") {
	$module_text = "Assign Points";
	$module_text_big = "Assign";
	$module_text_small = "Points";
}
?>
<script language="javascript">

	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		});
	}

</script>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1>
      <?=$module_text_big?>
      <small>
      <?=$module_text_small?>
      </small> </h1>
    <!--/HEADING--> 
    
    <!--BREADCRUMB-->
    <ol class="breadcrumb" style="float:left; position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home"><i class="fa fa-home"></i>Home</a></li>
      <li>
        <?=$module_text?>
      </li>
    </ol>
    <!--/BREADCRUMB-->
    <div style="clear:both"></div>
  </section>
  <section class="content">
   
    <!--WORKING AREA-->

    <!--Listing-->
    <div id="mid1" class="box">
      <div class="box-header">
        <h3 class="box-title">Class List</h3>
        <?php /*?><div class="box-tools">
          <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
          <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>
        </div><?php */?>
      </div>
      <div class="box-body">
        <table width="100%" class="table table-bordered table-striped" id="example1">
          <thead>
          </thead>
          <tbody id="item_list_ajax">
           
            <tr>
              <td width="10%" align="left" valign="middle"></td>
            </tr>

          </tbody>
          <tfoot>
          </tfoot>
        </table>
      </div>
    </div>
    <!--/Listing--> 
    
    <!--Add or Create-->
    <div id="mid2" class="box box-primary" style="display:none">
      <div class="box-header with-border">
        <h3 class="box-title">Create Dictionary</h3>
        <div class="box-tools">
          <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
        </div>
      </div>
      <!-- /.box-header --> 
      <!-- form start -->
      <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
        <div class="box-body">
          <div class="box-body">
            <table class="table table-bordered table-striped" id="example1" width="100%">
              <thead>
                <tr>
                  <th valign="middle" align="center">Language</th>
                  <th width="25%" valign="middle" align="center">Dictionary Text</th>
                  <th width="25%" valign="middle" align="center"> Speech Text
                    <div class="fa"></div>
                  </th>
                  <th width="30%" valign="middle" align="center">Comments
                    <div class="fa"></div>
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php	for($i=0; $i<count($data_trans_rs); $i++){	
									$tbl_translation_language_idd = $data_trans_rs[$i]["tbl_translation_language_id"];
									$translation_language = $data_trans_rs[$i]["translation_language"];
						?>
                <tr>
                  <td valign="middle" align="left"><?=$translation_language?></td>
                  <td valign="middle" align="left"><input type="text" name="translation_word_arr[]" value="" /></td>
                  <td valign="middle" align="left"><input type="text" name="speech_text_arr[]" value="" /></td>
                  <td valign="middle" align="left"><textarea name="description_arr[]" rows="3" cols="30"></textarea></td>
                </tr>
              <input type="hidden" name="tbl_translation_language_id_arr[]" value="<?=$tbl_translation_language_idd?>" />
              <?php	}	?>
              <tr>
                <td colspan="4" valign="middle" align="right"></td>
              </tr>
                </tbody>
              
              <tfoot>
              </tfoot>
            </table>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <button class="btn btn-primary" type="button" onclick="ajax_validate()">Create Dictionary</button>
          <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>--> 
        </div>
        <!-- /.box-footer -->
      </form>
    </div>
    <!--/Add or Create--> 
    
    <!--/WORKING AREA--> 
  </section>
</div>