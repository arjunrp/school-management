         <?php
		
		
		 ?>  

      <div class="box-body" id="add_report" style="display:block;">
       
        <div class="form-group">
        <div class="col-sm-12" style="float:left;">
        	<label class="col-sm-4 control-label" for="tbl_parenting_school_cat_id">Attendance Date</label>
            
        	<div class="col-sm-8" ><?=date("d-m-Y", strtotime($attendance_report_details[0]['attendance_date']))?></div>
        </div>
        <div class="col-sm-12" style="float:left;">
        	<label class="col-sm-4 control-label" for="tbl_parenting_school_cat_id">Session</label>
        	<div class="col-sm-8" ><?=$attendance_report_details[0]['title']?>[::]<?=$attendance_report_details[0]['title_ar']?>&nbsp;&nbsp;&nbsp;&nbsp;<?=date("h-i A", strtotime($attendance_report_details[0]['start_time']))?> - <?=date("h-i A", strtotime($attendance_report_details[0]['end_time']))?></div>
        </div>
        </div>
        <table width="100%" class="table table-bordered table-striped">
          <thead>
            <tr>
                <th width="60%" align="center" valign="middle">Students</th>
                <th width="40%" align="center" valign="middle">Attendance</th>
            </tr> 
          </thead>
          <tbody>
          <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		  <script src="<?=HOST_URL?>/js/jquery-ui.js"></script>  
  
<?php	
    for ($u=0; $u<count($attendance_report_details); $u++) { 
         $name 			    = $attendance_report_details[$u]['first_name']." ".$attendance_report_details[$u]['last_name'];
		 $name_ar 			= $attendance_report_details[$u]['first_name_ar']." ".$attendance_report_details[$u]['last_name_ar'];
		 $is_present 	    = $attendance_report_details[$u]['is_present'];
		 
		 if($is_present=="Y")
		 {
		 	$is_present = "Present";
			$color      = "#093";
		 }
		 else if($is_present=="N")
		 {
		 	$is_present = "Absent";
			$color      = "#F00";
		 }
		else if($is_present=="LC")
		{
		 	$is_present = "Late Coming";
			$color      = "#F0F";
		}
		else if($is_present=="EG")
		{
		 	$is_present = "Early Going";
			$color      = "#F60";
		}
       ?>        
         <tr>
		  <td>
          <div><?=$name?><span style="float:right;"><?=$name_ar?></span> </div>
         </td>
         <td style="color:<?=$color?>;"><?=$is_present?></td>
         
         </tr>
<?php
	}
?>

 </tbody>
          <tfoot>
          </tfoot>
        </table>
                
    </div>

<script>
function update_student_progress_report(tbl_progress_report_id, tbl_student_id,tbl_class_id,tbl_school_id, tbl_teacher_id) {
		
		var subject_ids 		   =  $("#subject_ids").val();
		var id_data     		=  "";
		var sub_mark_data       =  "";
		var tot_mark_data       =  "";
		var cnt                 =  "";
		
		var id_array            =  new Array;
		if(subject_ids != '')
		{
			var id_array    = subject_ids.split('||');
			for(k = 0; k < id_array.length; k++)
			{
				var ids          = id_array[k];
				if(ids!='')
				{
					var subMark  	= $("#marks_"+ids).val();
					var totMark  	= $("#totals_"+ids).val();
					if(subMark != '' && totMark != '' )
					{
						id_data 		= id_data+ids+"||";
						sub_mark_data 	= sub_mark_data+subMark+"||";
						tot_mark_data 	= tot_mark_data+totMark+"||";
						cnt = 1;
						
					}
				}
			
			}
			
		}
		
		
		if(cnt!= '1')
		{
			alert("Please enter student marks and total marks.");
			return false;
		}else{
			
			$("#pre-loader").show();
			var url_str = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/update_student_progress_report";
			$.ajax({
				type: "POST",
				url: url_str,
				data: {
					is_admin: "Y",
					tbl_progress_report_id: tbl_progress_report_id,
					id_data: id_data,
					sub_mark_data: sub_mark_data,
					tot_mark_data: tot_mark_data,
					tbl_student_id: tbl_student_id,
					tbl_class_id: tbl_class_id,
					tbl_teacher_id: tbl_teacher_id,
					tbl_school_id: tbl_school_id
				},
				success: function(result_data) {
					if($.trim(result_data)=="E")
					{
						 alert("Progress report already exist.");
						$("#pre-loader").hide();
					}else if($.trim(result_data)=="N")
					{
						 alert("Progress report added failed, Please try again.");
						$("#pre-loader").hide();
					}else{
						alert("Progress report added successfully.");
						$("#pre-loader").hide();
					}
				}
			});
		}
	}
	
	function get_progress_report_close()
	{
		jQuery.noConflict(); 
		$('#myModal').modal('hide');
		
	}
	</script>