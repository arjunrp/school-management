<?php
//Init Parameters
$subject_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}
?>
 
<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/school_subject";
			}
		})
	});
	
function confirm_delete_popup() {
		var len = $("input[id='subject_id_enc']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more subject(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/deleteSubject",
			data: {
				subject_id_enc: $("input[id='subject_id_enc']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Subject(s) deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}	
	function ajax_activate(subject_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/activateSubject",
			data: {
				subject_id_enc: subject_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Subject activated successfully.", 'green')

				$('#act_deact_'+subject_id_enc).html('<span style="cursor:pointer" onClick="ajax_deactivate(\''+subject_id_enc+'\')" onMouseOver="deactivate_me(this)" onMouseOut="reset_activate(this)" class="label label-success">Active</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_deactivate(subject_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/deactivateSubject",
			data: {
				subject_id_enc: subject_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Subject de-activated successfully.", 'green')
				
				$('#act_deact_'+subject_id_enc).html('<span style="cursor:pointer" onClick="ajax_activate(\''+subject_id_enc+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/is_exist_subject",
			data: {
				subject_id_enc: "<?=$subject_id_enc?>",
				subject: $('#subject').val(),
				subject_ar: "",
				is_ajax: true
			},
			success: function(data) {
				
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
					refresh_page = "N";
					my_alert("Subject is already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_create();
				}
			
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist_edit() {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/is_exist_subject",
			data: {
				subject_id_enc:$('#subject_id_enc').val(),
				subject: $('#subject').val(),
				subject_ar: "",
				is_ajax: true
			},
			success: function(data) {
				var temp = new String(data);
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
					refresh_page = "N";
					my_alert("Subject already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_save_changes();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_create() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/create_school_subject",
			data: {
				subject_id_enc:'<?=$subject_id_enc?>',
				subject: $('#subject').val(),
				subject_ar: $('#subject_ar').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
				refresh_page = "Y";
				    my_alert("Subject created successfully.", 'green');
				    $("#pre-loader").hide();
				}else{
					refresh_page = "N";
					my_alert("Subject added failed, Please try again.", 'red');
					$("#pre-loader").hide();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_save_changes() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/save_subject_changes",
			data: {
				subject_id_enc:$('#subject_id_enc').val(),
				subject: $('#subject').val(),
				subject_ar: $('#subject_ar').val(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Changes saved successfully.", 'green');
				
				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
</script>
<script language="javascript">
	function ajax_validate() {
		if (validate_subject() == false || validate_subject_ar() == false ) {
			return false;
		} else {
			is_exist();
		}
	} 

	function ajax_validate_edit() {
		if ( validate_subject() == false || validate_subject_ar() == false ) {
			return false;
		} else {
			is_exist_edit();
		}
	} 


	function validate_subject() {
		var regExp = / /g;
		var str = $('#subject').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Subject [En] is blank. Please enter Subject[En].");
		return false;
		}
	}

	function validate_subject_ar() {
		var regExp = / /g;
		var str = $('#subject_ar').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Subject [Ar] is blank. Please enter Subject[Ar].");
		return false;
		}
	}

</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Subject(s) <small> Management</small> </h1>
    <!--/HEADING--> 
    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Subject</li>
    </ol>
    <!--/BREADCRUMB--> 
    <div style="clear:both"></div>
  </section>
 
  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {

			$tbl_subject_id       = $school_subject_obj[0]['tbl_subject_id'];
			$subject              = $school_subject_obj[0]['subject'];
			$subject_ar           = $school_subject_obj[0]['subject_ar'];
			$added_date           = $school_subject_obj[0]['added_date'];
			$is_active            = $school_subject_obj[0]['is_active'];
	?>
        <!--Edit-->
        
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Subject</h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/school_subject"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_edit" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                  
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="subject">Subject [En]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Subject [En]" id="subject" name="subject" class="form-control" value="<?=$subject?>" >
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="subject_ar">Subject [Ar]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Subject [Ar]" id="subject_ar" name="subject_ar" class="form-control" dir="rtl" value="<?=$subject_ar?>" >
                      </div>
                    </div>
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate_edit()">Save Changes</button>
                    <input type="hidden" name="subject_id_enc" id="subject_id_enc" value="<?=$tbl_subject_id?>" />
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
		        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/school/school_subject";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
    
  
 <link href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" rel="stylesheet">
 <script src="http://code.jquery.com/jquery-1.11.1.js"></script>
 <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
  <script>
  $( function() {
		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
				
				/* var order = $("#tabledivbody").sortable("serialize");
				
				alert(order);
				
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/your/url/here'
				});*/
				
				
				
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	
				
				
				
				
				
			}
	  } );
  
  } );
  </script> 
    
                    <div id="mid1" class="box box-success">
                        <div class="box-header">
                          <div style="width:20%; float:left;">
                          <h3 class="box-title">SEARCH</h3>
                          </div>
                          <div style="width:60%; float:left;"> 
                    	     <div class="col-sm-5">
                             <?php //echo $category_parent_list ?>
                   		      </div>
                             
                          </div>
                        </div>  
                     </div>     
    
            <!--Listing-->
                    <div id="mid1_list" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Subject(s)</h3>
                          <div class="box-tools">
                            <?php if (count($rs_all_subjects)>0) { echo $paging_string;}?>	
                            <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
                            <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>
                          </div>
                        </div>
                        
                        <div class="box-body">
                     <!--   <div style="color:#030; font-weight:bold;">You can sort categories by using drag and drop of rows </div>-->
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle"><input id="select_all" type="checkbox" value="" /></th>
                              <!--<th width="10%" align="center" valign="middle">Sl No.</th>-->
                              <th width="25%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y">Subject [En] <?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?>
                                <div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>
                              <th width="25%" align="center" valign="middle">Subject [Ar]</th>
                              <th width="15%" align="center" valign="middle">Date</th>
                              <th width="10%" align="center" valign="middle">Status</th>
                              <th width="10%" align="center" valign="middle">Action</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                                for ($i=0; $i<count($rs_all_subjects); $i++) { 
                                    $id                 = $rs_all_subjects[$i]['id'];
                                    $tbl_subject_id     = $rs_all_subjects[$i]['tbl_subject_id'];
                                    $subject            = $rs_all_subjects[$i]['subject'];
                                    $subject_ar         = $rs_all_subjects[$i]['subject_ar'];
                                    $added_date         = $rs_all_subjects[$i]['added_date'];
                                    $is_active          = $rs_all_subjects[$i]['is_active'];
                                    
                                    $school_type = ucfirst($school_type);
                                    $added_date = date('m-d-Y',strtotime($added_date));
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_subject_id?>" >
                              <td align="left" valign="middle">
                              <span style="float:left;">
                              <input id="subject_id_enc" name="subject_id_enc" class="checkbox" type="checkbox" value="<?=$tbl_subject_id?>" />
                              </span>
                              
                             <?php /*?> <span style="float:left;">&nbsp;
                              <?php if($i<>0){ ?> <i class="fa fa-arrow-up"  style="color:#3c8dbc; cursor:pointer;"  aria-hidden="true" title="Sorting - Drag & Drop To Up"></i> &nbsp; <?php } ?>
                               <?php if($i<> count($rs_all_categories)-1){ ?> <i class="fa fa-arrow-down" style="color:#3c8dbc;cursor:pointer;" aria-hidden="true" title="Sorting - Drag & Drop To Down"></i> <?php } ?>
                              </span><?php */?>
                              </td>
                             <!-- <td align="left" valign="middle"><?=$offset+$i+1?></td>-->
                              <td align="left" valign="middle"><?=$subject?></td>
                              <td align="left" valign="middle"><?=$subject_ar?></td>
                              <td align="left" valign="middle"><?=$added_date?></td>
                              <td align="left" valign="middle">
                                <div id="act_deact_<?=$tbl_subject_id?>">
                                <?php if (trim($is_active) == "Y") { ?>
                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_subject_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                <?php } else { ?>
                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_subject_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                <?php } ?>
                                </div>
                              </td>
                              <td align="left" valign="middle">
                                <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/school/edit_school_subject/subject_id_enc/<?=$tbl_subject_id?>"><button class="btn bg-purple fa fa-pencil" type="button" title="Edit"></button></a>
                              </td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="8" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_all_subjects)<=0) {
                            ?>
                            <tr>
                              <td colspan="8" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no subject available. Click on the + button to create one.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
    
            <!--Add or Create-->
              <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Subject</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                  
                   
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="subject">Subject [En]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Subject [En]" id="subject" name="subject" class="form-control">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="subject_ar">Subject [Ar]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Subject [Ar]" id="subject_ar" name="subject_ar" class="form-control" dir="rtl">
                      </div>
                    </div>
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate()">Submit</button>
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
            <!--/Add or Create-->
                
        <!--/Admin Category Management-->

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>

        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >
function search_data() {
		var tbl_category_id = $("#tbl_category_id").val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/all_categories/";
		if(tbl_category_id !='')
			url += "tbl_category_id/"+tbl_category_id+"/";
		
			url += "offset/0/";
		window.location.href = url;
		<?php /*?>window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/enquiry/all_enquiries/is_not_replied/"+is_not_replied+"/tbl_court_id/"+tbl_court_id+"/tbl_category_id/"+tbl_category_id;<?php */?>
	}
</script>