   <link href="<?=HOST_URL?>/css/jquery.bsPhotoGallery.css" rel="stylesheet">
    <script src="<?=HOST_URL?>/js/jquery.bsPhotoGallery.js"></script>
    <script>
      $(document).ready(function(){
        $('ul.first').bsPhotoGallery({
          "classes" : "col-lg-2 col-md-4 col-sm-3 col-xs-4 col-xxs-12",
          "hasModal" : true,
          // "fullHeight" : false
        });
      });
    </script>
    <style>

    .text {
      /*font-family: 'Bree Serif';*/
      color:#666;
      font-size:13px;
      margin-bottom:10px;
      padding:12px;
      background:#fff;
    }
    </style>
     <ul class="row first" style=" padding:0 0 0 0; margin:0 0 40px 0;" >
<?php	$CI = & get_instance();
	
	for ($i=0; $i<count($data_rs); $i++) {
		$tbl_class_id = $data_rs[$i]['tbl_class_id'];
		$tbl_section_id = $data_rs[$i]['tbl_section_id'];
		
		$CI->load->model("model_section");
		$data_sec = $CI->model_section->get_section_obj($tbl_section_id);
		
		$CI->load->model("model_classes");
		$data_ct = $CI->model_classes->get_class_teacher($tbl_class_id);
		
		//if (trim($lan) == "ar") {
			$class_name_ar = $data_rs[$i]['class_name_ar'];
			$section_name_ar = $data_sec[0]['section_name_ar'];
			$class_teacher_name_ar = $data_ct[0]['first_name_ar']." ".$data_ct[0]['last_name_ar'];
		//} else {
			$class_name = $data_rs[$i]['class_name'];
			$section_name = $data_sec[0]['section_name'];
			$class_teacher_name = $data_ct[0]['first_name']." ".$data_ct[0]['last_name'];
		//}
		$color = $data_rs[$i]['color'];
		$arr_classes[$i]["class_id"] = $tbl_class_id;
		$arr_classes[$i]["title"] = $class_name;
		$arr_classes[$i]["section_id"] = $tbl_section_id;
		$arr_classes[$i]["section_title"] = $section_name;
		$arr_classes[$i]["class_teacher"] = trim($class_teacher_name);
		$arr_classes[$i]["color"] = $color;
	?>	
            <li class="col-lg-2 col-md-4 col-sm-3 col-xs-4 col-xxs-12 bspHasModal" style="border:1px solid #CCC; background-color:#f9fafc; cursor:pointer; border-radius:15px; list-style:none; margin:10px; text-align:center;  width: 18%; height:180px !important;">
                <a title="Click On To Students List" alt="Click On To Students List"  onclick="get_teacher_all_students('<?=$tbl_class_id?>', '<?=$class_name?>', '<?=$section_name?>')" >
                <div style="float:left; width:100%; padding-top:10px;">
                <img alt="<?=$arr_classes[$i]["title"]?>"  src="<?=HOST_URL?>/assets/images/class_room.png" width="60" height="60" style="color:#778070;" title="Click On To Students List" alt="Click On To Students List"><br />
                <div style="float:left; width:100%; font-size:17px;"><?=$class_name?>-<?=$section_name?>&nbsp;[<?=$class_name_ar?>-<?=$section_name_ar?>]</div>
                <div style="clear:both;"></div>
                <div style="float:left; width:100%; font-size:17px;"><?php if (trim($class_teacher_name) !="") {echo "<sub style='font-size:100%'>".$class_teacher_name."&nbsp;(".$class_teacher_name_ar.")</sub>"; } ?></div>
                </a>
            </li>
	<?php    		
	}
?>
      </ul>





