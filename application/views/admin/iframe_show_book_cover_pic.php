<?php error_reporting(0); ?>
<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--><html class="no-js" lang="en"><!--<![endif]-->
<script src="<?=HOST_URL?>/assets/js/jquery-1.11.1.min.js"></script>
<head>

<style>
body {
	margin: 0 auto;
	background-image:none;
}
</style>

<script language="javascript">
	function uploadImg() {
		document.frm_image.submit();
	}
	function resizeIframe(iframeID) {
		var iframe = window.parent.document.getElementById(iframeID);
		var body_offsetHeight = document.body.offsetHeight;
	
		iframe.style.height = body_offsetHeight + "px";
	}
</script>
</head>
<body onload="resizeIframe('iframe_book_cover_pic')">
<form name="frm_image" enctype="multipart/form-data" method="post" action="<?=HOST_URL?>/<?=LAN_SEL?>/admin/books/cover_photo_upload/">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-color:#f4f1f1;">
  <tr>
    <td width="70%" align="left" valign="middle">
      <?php if($cover_pic<>""){ ?>
      <img src="<?=HOST_URL?>/uploads/books/<?=$cover_pic?>" width="93" height="90" style="padding:10px" />
      <?php } ?>
      <label for="fileField"></label>
      <input type="file" name="book_cover" id="book_cover" onChange="uploadImg()"><!--Picture Size(118 X 118)-->
    </td>
     <?php /*?><input type="hidden" name="cover_pic" class="contact-subject" id="cover_pic" value="<?=$cover_pic?>" ><?php */?>
     
     <input type="hidden" name="tbl_book_id" class="contact-subject" id="tbl_book_id" value="<?=$unique_id?>" >
    <td align="left" valign="middle" style="color:#CC0000"><?php echo $MSG;?></td>
  </tr>
</table>
</form>
<?php if($cover_pic<>"") { ?>
<script>
$(document).ready(function(){
   window.parent.$("#cover_pic").val('<?=$cover_pic?>');
   window.parent.$("#divCover").hide();
   $("#sel_cover_pic").hide();
});
</script>
<?php } ?>

</body>
</html>

