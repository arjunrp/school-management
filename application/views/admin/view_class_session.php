<?php
//Init Parameters
$class_sessions_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}


?>
<link rel="stylesheet" media="all" type="text/css" href="<?=JS_PATH?>/date_time_picker/css/jquery-ui-1.8.6.custom.css" />
<style type="text/css">
pre {
	padding: 20px;
	background-color: #ffffcc;
	border: solid 1px #fff;
}
.example-container {
	background-color: #f4f4f4;
	border-bottom: solid 2px #777777;
	margin: 0 0 40px 0;
	padding: 20px;
}

.example-container p {
	font-weight: bold;
}

.example-container dt {
	font-weight: bold;
	height: 20px;
}

.example-container dd {
	margin: -20px 0 10px 100px;
	border-bottom: solid 1px #fff;
}

.example-container input {
	width: 150px;
}

.clear {
	clear: both;
}

#ui-datepicker-div {
}

.ui-timepicker-div .ui-widget-header {
	margin-bottom: 8px;
}

.ui-timepicker-div dl {
	text-align: left;
}

.ui-timepicker-div dl dt {
	height: 25px;
}

.ui-timepicker-div dl dd {
	margin: -25px 0 10px 65px;
}

.ui-timepicker-div td {
	font-size: 90%;
}
</style>

<script type="text/javascript" src="<?=JS_PATH?>/date_time_picker/js/jquery-ui-1.8.6.custom.min.js"></script>
<script type="text/javascript" src="<?=JS_PATH?>/date_time_picker/js/jquery-ui-timepicker-addon.js"></script>
<!--<script language="javascript">
	$(document).ready(function(){
		$('#start_time').datetimepicker({
			dateFormat: '',
			timeFormat: 'hh:mm tt',
			timeOnly: true   
		});


		$('#end_time').datetimepicker({
			dateFormat: '',
			timeFormat: 'hh:mm tt',
			timeOnly: true   
		});
	});
	$('#time_start').timepicker();
    $('#time_end').timepicker();
</script>
-->
<script type="text/javascript" src="<?=JS_COLOR_PATH?>/jscolor.js"></script>

<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/class_sessions";
			}
		})
	});
	
function confirm_delete_popup() {
		var len = $("input[id='class_sessions_id_enc']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more class Period(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/deleteSession",
			data: {
				class_sessions_id_enc: $("input[id='class_sessions_id_enc']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Class Period(s) deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}	
	function ajax_activate(class_sessions_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/activateSession",
			data: {
				class_sessions_id_enc: class_sessions_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Class Period activated successfully.", 'green')

				$('#act_deact_'+class_sessions_id_enc).html('<span style="cursor:pointer" onClick="ajax_deactivate(\''+class_sessions_id_enc+'\')" onMouseOver="deactivate_me(this)" onMouseOut="reset_activate(this)" class="label label-success">Active</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_deactivate(class_sessions_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/deactivateSession",
			data: {
				class_sessions_id_enc: class_sessions_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Class Period de-activated successfully.", 'green')
				
				$('#act_deact_'+class_sessions_id_enc).html('<span style="cursor:pointer" onClick="ajax_activate(\''+class_sessions_id_enc+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/is_exist_session",
			data: {
			    tbl_class_id:  $('#tbl_class_id').val(),
				class_sessions_id_enc: "<?=$class_sessions_id_enc?>",
				title: $('#title').val(),
				title_ar: "",
				start_time: $('#start_time').val(),
				end_time: $('#end_time').val(),
				is_ajax: true
			},
			success: function(data) {
				
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='T') {
					refresh_page = "N";
					my_alert("Class Period / Period time already exists.", 'red');
					$("#pre-loader").hide();
				}else if (temp=='Y') {
					refresh_page = "N";
					my_alert("Class Period already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_create();
				}
			
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist_edit() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/is_exist_session",
			data: {
			    tbl_class_id:  $('#tbl_class_id').val(),
				class_sessions_id_enc: $('#class_sessions_id_enc').val(), 
				title: $('#title').val(),
				title_ar: "",
				start_time: $('#start_time').val(),
				end_time: $('#end_time').val(),
				is_ajax: true
			},
			success: function(data) {
				
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='T') {
					refresh_page = "N";
					my_alert("Class Period / Period time  is already exists.", 'red');
					$("#pre-loader").hide();
				}else if (temp=='Y') {
					refresh_page = "N";
					my_alert("Class Period already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_save_changes();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_create() {
		  
		var selectednumbers = "";
		$('.checkboxClass:checked').each(function(i){
			selectednumbers += $(this).val()+"&";
		});
		
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/create_class_session",
			data: {
				tbl_class_id:selectednumbers,
				class_sessions_id_enc: "<?=$class_sessions_id_enc?>",
				title: $('#title').val(),
				title_ar: $('#title_ar').val(),
				start_time: $('#start_time').val(),
				end_time: $('#end_time').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
				refresh_page = "Y";
				    my_alert("Class Period is created successfully.", 'green');
				    $("#pre-loader").hide();
				}else{
					refresh_page = "N";
					my_alert("Class Period added failed, Please try again.", 'red');
					$("#pre-loader").hide();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_save_changes() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/save_session_changes",
			data: {
			    tbl_class_id:  $('#tbl_class_id').val(),
				class_sessions_id_enc: $('#class_sessions_id_enc').val(), 
				title: $('#title').val(),
				title_ar: $('#title_ar').val(),
				start_time: $('#start_time').val(),
				end_time: $('#end_time').val(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Changes saved successfully.", 'green');
				
				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
</script>
<script language="javascript">
	function ajax_validate() {
		if (validate_class() == false || validate_session_name() == false || validate_session_name_ar() == false || validate_start_time() == false || validate_end_time() == false ) {
			return false;
		} else {
			//is_exist();
			ajax_create();
		}
	} 

	function ajax_validate_edit() {
		if (validate_session_name() == false || validate_session_name_ar() == false || validate_start_time() == false || validate_end_time() == false  ) {
			return false;
		} else {
			is_exist_edit();
		}
	} 

     function validate_class() {
		var regExp = / /g;
		var str = $('#tbl_class_id').val();
		
		var tbl_class_id = "";
        $('.checkboxClass:checked').each(function(i){
          tbl_class_id += $(this).val()+",";
        });
		
		if (tbl_class_id=="" ) {
			my_alert("Class(s)/ Grade(s) is not selected. Please select Class/ Grade .");
		return false;
		}
	}

	function validate_session_name() {
		var regExp = / /g;
		var str = $('#title').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Class Period [En] is blank. Please enter Class Period [En].");
		return false;
		}
	}

	function validate_session_name_ar() {
		var regExp = / /g;
		var str = $('#title_ar').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Class Period [Ar] is blank. Please enter Class Period [Ar].");
		return false;
		}
	}
	
	 function validate_start_time() {
		var regExp = / /g;
		var str = $('#start_time').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Please provide Start Time.");
		return false;
		}
	}
	
	 function validate_end_time() {
		var regExp = / /g;
		var str = $('#end_time').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Please provide End Time.");
		return false;
		}
	}

</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Class Periods <small> Management</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Class Periods</li>
    </ol>
    <!--/BREADCRUMB--> 
    <div style=" float:right; padding-right:10px;"><button class="btn bg-purple fa fa-arrow-circle-o-left" onclick="show_all_classes()" type="button" title="Back"></button></div> 
    <div style="clear:both"></div>
  </section>
  
  <script>
    function show_all_classes()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/class_grades/";
		window.location.href = url;
	}  
  </script>
  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {
			$tbl_class_sessions_id   = $class_sessions_obj['tbl_class_sessions_id'];
			$tbl_sel_class_id        = $class_sessions_obj['tbl_class_id'];
			$title                   = $class_sessions_obj['title'];
			$title_ar                = $class_sessions_obj['title_ar'];
			$start_time              = date("h:i a", strtotime($class_sessions_obj['start_time']));
			$end_time                = date("h:i a", strtotime($class_sessions_obj['end_time']));
	?>
        <!--Edit-->
        
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Class Periods</h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/class_sessions"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_edit" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                      <div class="form-group">
                     <label class="col-sm-2 control-label" for="tbl_class_id">Class/Grade<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-10">
                                 <select name="tbl_selected_class_id" id="tbl_selected_class_id" class="form-control" disabled="disabled">
                              <?php
                                    for ($u=0; $u<count($classes_list); $u++) { 
                                        $tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
                                        $class_name             = $classes_list[$u]['class_name'];
                                        $class_name_ar          = $classes_list[$u]['class_name_ar'];
										$section_name           = $classes_list[$u]['section_name'];
                                        $section_name_ar        = $classes_list[$u]['section_name_ar'];
                                        if($tbl_sel_class_id == $tbl_class_id_u)
										{
                                           $selClass = "selected";
										   $tbl_class_id = $tbl_class_id_u;
										}
                                         else
										{
                                           $selClass = "";
										}
                                  ?>
                                      <option value="<?=$tbl_class_id_u?>"  <?=$selClass?> >
                                      <?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;
                                    <?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>
                   </div>
                   <input type="hidden" name="tbl_class_id" id="tbl_class_id" value="<?=$tbl_class_id?>" />
                   </div>
                  
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="title">Class Period [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Class Period [En]" id="title" name="title" class="form-control" value="<?=$title?>">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="title_ar">Class Period [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Class Period [Ar]" id="title_ar" name="title_ar" class="form-control" value="<?=$title_ar?>" dir="rtl">
                      </div>
                    </div>
                    
                       <div class="form-group">
                      <label class="col-sm-2 control-label" for="start_time">Start Time <span style="color:#F30; padding-left:2px;">*</span></label>
                      <div class="col-sm-10 bootstrap-timepicker">
                        <input type="text" placeholder="Start Time" id="start_time" name="start_time" class="form-control timepicker" value="<?=$start_time?>"  >
                        
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="end_time">End Time <span style="color:#F30; padding-left:2px;">*</span></label>
                      <div class="col-sm-10 bootstrap-timepicker">
                        <input type="text" placeholder="End Time" id="end_time" name="end_time" class="form-control timepicker" value="<?=$end_time?>" >
                      </div>
                    </div>
                    
                    
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate_edit()">Save Changes</button>
                    <input type="hidden" name="class_sessions_id_enc" id="class_sessions_id_enc" value="<?=$tbl_class_sessions_id?>" />
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
		        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/classes/class_sessions";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
    
  
 <link href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" rel="stylesheet">
 <script src="http://code.jquery.com/jquery-1.11.1.js"></script>
 <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
  <script>
  $( function() {
		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
				
				/* var order = $("#tabledivbody").sortable("serialize");
				
				alert(order);
				
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/your/url/here'
				});*/
				
				
				
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	

				
				
				
				
				
			}
	  } );
  
  } );
  
  
  
  </script> 
    
                  <div id="mid1" class="box box-success">
                        <div class="box-header">
                          <div class="col-sm-1" >
                          <h3 class="box-title">SEARCH</h3>
                          </div>
                          <div class="col-sm-11"> 
                               <div class="col-sm-3"><input name="q" id="q" value="<?=urldecode($q)?>" type="text" class="form-control" placeholder="Search By Period, Class etc."   > </div>
                               <div class="col-sm-3">
                               <select name="tbl_class_search_id" id="tbl_class_search_id" class="form-control">
                              <option value="">--Select Class --</option>
							  
							  <?php
                                    for ($u=0; $u<count($classes_list); $u++) { 
                                        $tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
                                        $class_name             = $classes_list[$u]['class_name'];
                                        $class_name_ar          = $classes_list[$u]['class_name_ar'];
										$section_name           = $classes_list[$u]['section_name'];
                                        $section_name_ar        = $classes_list[$u]['section_name_ar'];
                                        if($tbl_class_search_id == $tbl_class_id_u)
                                           $selClass = "selected";
                                         else
                                           $selClass = "";
                                  ?>
                                      <option value="<?=$tbl_class_id_u?>"  <?=$selClass?>  >
                                      <?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;
                                    <?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>
                             </div>
                               <div class="col-sm-2"><button class="btn btn-success" type="button" onclick="search_data()">Search</button>&nbsp;<button class="btn btn-success" type="button" 
                               onclick="reset_data();">Reset</button>
                               </div>
                           
                          </div>
                        </div>  
                     </div>   
    
            <!--Listing-->
                    <div id="mid1_list" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Class/Grade Periods</h3>
                          <div class="box-tools">
                            <?php if (count($rs_all_class_sessions)>0) { echo $paging_string;}?>	
                            <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
                            <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>
                          </div>
                        </div>
                        
                        <div class="box-body">
                     <!--   <div style="color:#030; font-weight:bold;">You can sort categories by using drag and drop of rows </div>-->
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle"><input id="select_all" type="checkbox" value="" /></th>
                              <!--<th width="10%" align="center" valign="middle">Sl No.</th>-->
                              <th width="15%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y">Class/Grade Period [En] <?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?>
	                              <div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>
                              <th width="15%" align="center" valign="middle">Class/Grade Period[Ar]</th>
                              <th width="25%" align="center" valign="middle">Class/Grade</th>
                              <th width="9%" align="center" valign="middle">Time</th>
                              <th width="7%" align="center" valign="middle">Date</th>
                              <th width="7%" align="center" valign="middle">Status</th>
                              <th width="7%" align="center" valign="middle">Action</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                                for ($i=0; $i<count($rs_all_class_sessions); $i++) { 
                                    $id = $rs_all_class_sessions[$i]['id'];
                                    $tbl_class_sessions_id     = $rs_all_class_sessions[$i]['tbl_class_sessions_id'];
                                    $title                     = $rs_all_class_sessions[$i]['title'];
                                    $title_ar                  = $rs_all_class_sessions[$i]['title_ar'];
									 
									$class_name                = $rs_all_class_sessions[$i]['class_name'];
                                    $class_name_ar             = $rs_all_class_sessions[$i]['class_name_ar'];
									$section_name              = $rs_all_class_sessions[$i]['section_name'];
                                    $section_name_ar           = $rs_all_class_sessions[$i]['section_name_ar'];
									$start_time                = $rs_all_class_sessions[$i]['start_time'];
                                    $end_time                  = $rs_all_class_sessions[$i]['end_time'];
									
                                    $added_date                = $rs_all_class_sessions[$i]['added_date'];
                                    $is_active                 = $rs_all_class_sessions[$i]['is_active'];
                                    
                                    $title       = ucfirst($title);
                                    $added_date         = date('m-d-Y',strtotime($added_date));
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_class_sessions_id?>" >
                              <td align="left" valign="middle">
                              <span style="float:left;">
                              <input id="class_sessions_id_enc" name="class_sessions_id_enc" class="checkbox" type="checkbox" value="<?=$tbl_class_sessions_id?>" />
                              </span>
                              
                             <?php /*?> <span style="float:left;">&nbsp;
                              <?php if($i<>0){ ?> <i class="fa fa-arrow-up"  style="color:#3c8dbc; cursor:pointer;"  aria-hidden="true" title="Sorting - Drag & Drop To Up"></i> &nbsp; <?php } ?>
                               <?php if($i<> count($rs_all_categories)-1){ ?> <i class="fa fa-arrow-down" style="color:#3c8dbc;cursor:pointer;" aria-hidden="true" title="Sorting - Drag & Drop To Down"></i> <?php } ?>
                              </span><?php */?>
                              </td>
                             <!-- <td align="left" valign="middle"><?=$offset+$i+1?></td>-->
                              <td align="left" valign="middle"><?=$title?></td>
                              <td align="left" valign="middle"><?=$title_ar?></td>
                              <td align="left" valign="middle"><?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;<?=$section_name_ar?>&nbsp;<?=$class_name_ar?></td>
                              <td align="left" valign="middle"><?=$start_time?>&nbsp;-&nbsp;<?=$end_time?></td>
                              <td align="left" valign="middle"><?=$added_date?></td>
                              <td align="left" valign="middle">
                                <div id="act_deact_<?=$tbl_class_sessions_id?>">
                                <?php if (trim($is_active) == "Y") { ?>
                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_class_sessions_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                <?php } else { ?>
                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_class_sessions_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                <?php } ?>
                                </div>
                              </td>
                              <td align="left" valign="middle">
                                <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/edit_class_session/class_sessions_id_enc/<?=$tbl_class_sessions_id?>"><button class="btn bg-purple fa fa-pencil" type="button" title="Edit"></button></a>
                              </td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="9" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_all_class_sessions)<=0) {
                            ?>
                            <tr>
                              <td colspan="9" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no periods available. Click on the + button to create one.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
    
            <!--Add or Create-->
              <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Create Class Period</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                  
                    <?php /*?><div class="form-group">
                     <label class="col-sm-2 control-label" for="tbl_class_id">Class<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-10">
                                 <select name="tbl_class_id" id="tbl_class_id" class="form-control" multiple size="10" style="padding:0px 5px 0px 2px; font-size:14px; "  >
                                 <option value="">Select Class</option>
                              <?php
                                    for ($u=0; $u<count($classes_list); $u++) { 
                                        $tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
                                        $class_name             = $classes_list[$u]['class_name'];
                                        $class_name_ar          = $classes_list[$u]['class_name_ar'];
										$section_name           = $classes_list[$u]['section_name'];
                                        $section_name_ar        = $classes_list[$u]['section_name_ar'];
                                        if($tbl_sel_class_id == $tbl_class_id_u)
                                           $selClass = "selected";
                                         else
                                           $selClass = "";
                                  ?>
                                      <option value="<?=$tbl_class_id_u?>"  <?=$selClass?> >
                                      <?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;
                                    <?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>
                   </div>
                   </div><?php */?>
                   
                   
                   
                   <div class="form-group" id="all_student">
                        <label class="col-sm-2 control-label" for="tbl_class_id">Class(s)<!--<span style="color:#F30; padding-left:2px;">*</span>--></label>
        
                          <div class="col-sm-10" id="divClass" style="max-height:800px; overflow-y:scroll;">
                          <div class"col-sm-10" >
	 
                        <?php  if(count($classes_list)>0){ ?>
                           <div style="padding-bottom:10px;background-color:#e8eaeb;" class="col-sm-12"> 
                           <input type="checkbox" value="" id="select_all_class" class="checkboxClass" >&nbsp;Select All</div>
                        <?php  } ?>
                        <?php
                            for ($u=0; $u<count($classes_list); $u++) { 
								$tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
								$class_name             = $classes_list[$u]['class_name'];
								$class_name_ar          = $classes_list[$u]['class_name_ar'];
								$section_name           = $classes_list[$u]['section_name'];
								$section_name_ar        = $classes_list[$u]['section_name_ar'];
                             ?>
                             <div class="col-sm-4" style="padding-top:10px; padding-bottom:10px; border:1px solid #CCC;"> 
								  <input id="tbl_class_id_<?=$u?>" name="tbl_class_id[]" class="checkboxClass" type="checkbox" value="<?=$tbl_class_id_u?>"  onChange="get_students_ajax()" />&nbsp;
								   <?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;<?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                             </div>
							<?php  
	                         }
							 ?>
	                          </div> 
                        </div>
                        </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="title">Period Name [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-2">
                        <input type="text" placeholder="Session Name [En]" id="title" name="title" class="form-control">
                      </div>
                      
                      <label class="col-sm-2 control-label" for="title_ar">Period Name [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-2">
                        <input type="text" placeholder="Session Name [Ar]" id="title_ar" name="title_ar" class="form-control" dir="rtl">
                      </div>  
                      
                      <label class="col-sm-1 control-label" for="start_time">Start Time <span style="color:#F30; padding-left:2px;">*</span></label>
                      <div class="col-sm-1 bootstrap-timepicker" >
                        <input type="text" placeholder="Start Time" id="start_time" name="start_time" class="form-control timepicker" >
                      </div> 
                      <label class="col-sm-1 control-label" for="end_time">End Time <span style="color:#F30; padding-left:2px;">*</span></label>
                      <div class="col-sm-1 bootstrap-timepicker">
                        <input type="text" placeholder="End Time" id="end_time" name="end_time" class="form-control timepicker" >
                      </div> 
                    </div>
                  
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate()">Submit</button>
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
            <!--/Add or Create-->
                
        <!--/Admin Category Management-->
          <script>
		 
		  $('#select_all_class').on('click',function(){
			if(this.checked){
				$('.checkboxClass').each(function(){
					this.checked = true;
				});
				
			}else{
				 $('.checkboxClass').each(function(){
					this.checked = false;
				});
			}
		  });
			
		  </script>
	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>

        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >
function search_data() {
		var q = $.trim($("#q").val());
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/class_sessions/";
		
		if(q !='')
			url += "q/"+q+"/";
		
		var tbl_class_search_id = $.trim($("#tbl_class_search_id").val());
		
		if(tbl_class_search_id !='')
			url += "class_id_enc/"+tbl_class_search_id+"/";	
		
			url += "offset/0/";
		window.location.href = url;
		<?php /*?>window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/enquiry/all_enquiries/is_not_replied/"+is_not_replied+"/tbl_court_id/"+tbl_court_id+"/tbl_category_id/"+tbl_category_id;<?php */?>
	}

function reset_data() {
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/classes/class_sessions/";
		url += "offset/0/";
		window.location.href = url;
	}

$(function () {
   $(".timepicker").timepicker({
      showInputs: false
    });	
});
</script>