<?php
function get_random_color() {
	$colors = array('#00C0EF', '#F39C12', '#164852', '#00A65A', '#DD4B39', '#282E33', '#25373A', '#164852', '#495E67', '#FF3838');
	$count = count($colors) - 1;
	$i = rand(0, $count);
	$rand_color = $colors[$i];
	
return $rand_color;
}
?>

<?php	
	$CI = & get_instance();
   
	for ($i=0; $i<count($data_rs); $i++) {
		$id = $data_rs[$i]['id']; 	
		$tbl_message_id = $data_rs[$i]['tbl_message_id'];
		$message_from = $data_rs[$i]['message_from'];
		$message_to = $data_rs[$i]['message_to'];
		$message_type = $data_rs[$i]['message_type'];
		$message = $data_rs[$i]['message'];
		$added_date = $data_rs[$i]['added_date'];

		$added_date = date("H:i - d/m/Y",strtotime($data_rs[$i]["added_date"]));

		$item_id = $data_rs[$i]['item_id'];

			 if($item_id<>""){
				 $CI->load->model("model_message");
				 $dataRecords = $CI->model_message->get_upload_files($item_id);
				 if(trim($dataRecords[0]['file_name_original']) == "audio.mp4") {
					 $audio	= $dataRecords[0]['file_name_updated'];
					 $arr_msg[$i]['message_audio'] = HOST_URL."/admin/uploads/".$audio; 
					 $arr_msg[$i]['message_img'] = "";
					 $arr_msg[$i]['message_img_thumb'] = "";
				} else {
					 $image = $dataRecords[0]['file_name_updated'];
					 $image_thumb = $dataRecords[0]['file_name_updated_thumb'];
					 $arr_msg[$i]['message_img'] = HOST_URL."/admin/uploads/".$image; 
					 $arr_msg[$i]['message_img_thumb'] 	= HOST_URL."/admin/uploads/".$image_thumb;
					 $arr_msg[$i]['message_audio'] = "";
				 }
			 } else {
				  $arr_msg[$i]['message_img'] = ""; 
				  $arr_msg[$i]['message_img_thumb'] = "";
				  $arr_msg[$i]['message_audio'] = "";
			 }

			// Message from Ministry
			if ($tbl_teacher_id_from == "ministry") {
				$first_name = "Ministry";
			} else {
				$CI->load->model("model_teachers");
				$teacher_obj = $CI->model_teachers->get_teachers_obj($message_from);	
				$file_name_updated = $teacher_obj[0]['file_name_updated'];

				if (trim($lan) == "ar") {
					$first_name = $teacher_obj[0]['first_name_ar'];	
					$last_name = $teacher_obj[0]['last_name_ar'];
				}

				// Message from admin

				if (trim($first_name) == "") {
					$CI->load->model("model_admin");
					$admin_obj = $CI->model_admin->get_admin_user_obj($message_from);	
					$first_name = $admin_obj[0]['first_name'];
					$last_name = $admin_obj[0]['last_name'];
				}
			}


			$arr_msg[$i]["name"] = $first_name." ".$last_name;
			$arr_msg[$i]["date"] = $added_date;
			$arr_msg[$i]["message"] = $message;


			if ($file_name_updated != "") {
				$arr_msg[$i]["url"] = HOST_URL."/images/teacher/".$file_name_updated;
			}
			
			if (trim($message) == "") {
				continue;	
			}
	}

//$arr["messages"] = $arr_msg;
//echo json_encode($arr);

$arr = $arr_msg;
//print_r($arr);
?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> View <small> Messages</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style="float:left; position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home/teacher" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>View Messages</li>
    </ol>
    <!--/BREADCRUMB--> 
    <div style="clear:both"></div>
  </section>
  
  <section class="content"> 
    <!--WORKING AREA-->	
           <!--Listing-->
             <div id="mid1" class="box">
                <div class="box-header">
                  <h3 class="box-title">Messages Inbox</h3>
                  <div class="box-tools">
			          <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/teachers_roles_web/?user_id=<?=$_SESSION['aqdar_smartcare']['tbl_teacher_id_sess']?>&role=T&lan=en&school_id=<?=$_SESSION['aqdar_smartcare']['tbl_school_id_sess']?>&module_id=m_issue_cards"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                
                <div class="box-body">
                  <table width="100%" class="table table-bordered table-striped" id="example1">
                    <thead>
                    <tr>
                      <th width="5%" align="center" valign="middle">Ser No.</th>
                      <?php /*?><th width="5%" align="center" valign="middle">&nbsp;</th><?php */?>
                      <th width="15%" align="center" valign="middle">From</th>
                      <th align="center" valign="middle">Message Inbox</th>
                      <th width="20%" align="center" valign="middle">Date</th>
                      </tr>
                     
                    </thead>
                    <tbody>
                    <?php
                        for ($i=0; $i<count($arr); $i++) { 
							$message_img = $arr[$i]['message_img'];
							$message_img_thumb = $arr[$i]['message_img_thumb'];
							$message_audio = $arr[$i]['message_audio'];
							$name = $arr[$i]['name'];
							$date = $arr[$i]['date'];
							$message = $arr[$i]['message'];

							$char = strtoupper(mb_substr($message,0,1,'utf-8'));
                    ?>
                    <tr>
                      <td align="center" valign="middle"><?=$i+1?></td>
                      <?php /*?><td align="left" valign="middle"><div class="score_box" style="background-color:<?=get_random_color();?>"><?=$char?></div></td><?php */?>
                      <td align="left" valign="middle"><?=$name?></td>
                      <td align="left" valign="middle"><?=$message?></td>
                      <td align="left" valign="middle"><?=$date?></td>
                      </tr>
                    <?php } ?>
                    <tr>
                      <td colspan="5" align="right" valign="middle">
                      <?php //echo $this->pagination->create_links(); ?>
                      </td>
                    </tr>
                    <?php 
                        if (count($arr)<=0) {
                    ?>
                    <tr>
                      <td colspan="5" align="center" valign="middle">
                      <div class="alert alert-warning alert-dismissible" style="width:50%">
                        <button aria-hidden="true" data_messages-dismiss="alert" class="close" type="button">×</button>
                        <h4><i class="icon fa fa-info"></i> Information!</h4>
                        There are no messages available. 
                      </div>                                
                      </td>
                    </tr>
                    <?php   
                        }
                    ?>
                    </tbody>
                    <tfoot>
                    </tfoot>
                  </table>
                </div>
            </div>        
            <!--/Listing-->
    
    <!--/WORKING AREA--> 
  </section>  
  
  
</div>