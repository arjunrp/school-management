<?php include(ROOT_ADMIN_PATH."/admin/include/timeline_header.php");
?>
<!--WRAPPER-->
<div class="wrapper"> 
  
      <!--TOP BAR-->
      <?php include(ROOT_ADMIN_PATH."/admin/include/timeline_header_top_bar.php");?>
      <!--/TOP BAR--> 
      
      <!--LEFT-->
      <?php include(ROOT_ADMIN_PATH."/admin/include/timeline_left.php");?>
      <!--/LEFT--> 
      
      
      <!--INC FILE-->
      <?php
		//echo $page;
		//exit();
      	switch($page) {
			case("login_form"): {
				include(ROOT_ADMIN_PATH."/admin/login_form.php");
			break;	
			}	
			case("welcome_timeline"): {
				include(ROOT_ADMIN_PATH."/admin/welcome_timeline.php");
			break;	
			}	
			case("view_timeline_admin_user"): {
				include(ROOT_ADMIN_PATH."/admin/view_timeline_admin_user.php");
			break;	
			}
			case("view_timeline_user_rights"): {
				include(ROOT_ADMIN_PATH."/admin/view_timeline_user_rights.php");
			break;	
			}
			case("view_website_feedback"): {
				include(ROOT_ADMIN_PATH."/admin/view_website_feedback.php");
			break;	
			}
			case("view_all_schools"): {
				include(ROOT_ADMIN_PATH."/admin/view_all_schools.php");
			break;	
			}
		}
	  ?>
      <!--/INC FILE--> 
      
      
      <!--FOOTER COPYRIGHT-->
      <?php include(ROOT_ADMIN_PATH."/admin/include/footer_copyright.php");?>
      <!--/FOOTER COPYRIGHT--> 
      
      <!--MENU RIGHT-->
      <?php include(ROOT_ADMIN_PATH."/admin/include/menu_slide_right.php");?>
      <!--/MENU RIGHT--> 
  
</div>
<!--/WRAPPER-->
<?php include(ROOT_ADMIN_PATH."/admin/include/footer.php");?>