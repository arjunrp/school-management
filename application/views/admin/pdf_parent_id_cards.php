<?php ob_start(); ?>
<!DOCTYPE html>
<html>
<link rel="stylesheet" href="<?=ADMIN_CSS_PATH?>print_id_card.css" type="text/css">
<link rel="stylesheet" href="<?=ADMIN_CSS_PATH?>id_card_style.css" type="text/css">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<body  id="printarea">
<?php
header('Content-Type: text/html; charset=utf-8');
for ($i=0; $i<count($rs_all_parents); $i++) { 
		$id 			= $rs_all_parents[$i]["id"];
		$tbl_parent_id = $rs_all_parents[$i]["tbl_parent_id"];
		$first_name    = $rs_all_parents[$i]["first_name"];
		$last_name     = $rs_all_parents[$i]["last_name"];
		$first_name_ar = $rs_all_parents[$i]["first_name_ar"];
		$last_name_ar  = $rs_all_parents[$i]["last_name_ar"];
		$mobile        = $rs_all_parents[$i]["mobile"];
		$dob_month     = $rs_all_parents[$i]["dob_month"];
		$dob_day       = $rs_all_parents[$i]["dob_day"];
		$dob_year      = $rs_all_parents[$i]["dob_year"];
		$gender        = $rs_all_parents[$i]["gender"];
		if($gender =="male"){
			$gender_ar ="ذكر";
		}else{
			$gender_ar ="أنثى";
		}

		$email         = isset($rs_all_parents[$i]["email"])? $rs_all_parents[$i]["email"] :"-";
		$emirates_id   = $rs_all_parents[$i]["emirates_id"];
		$user_id       = $rs_all_parents[$i]["user_id"];
		$password      = base64_decode($rs_all_parents[$i]["pass_code"]);
		
		?>


<table width="400" class="tble" style="margin-bottom:10px;direction:ltr; background: rgba(0, 0, 0, 0) linear-gradient(to bottom, #b9b063, #c9c087, #f7f7ef) repeat scroll 0 0; border-radius: 12px 12px 12px 12px; font-family:Verdana,Arial,Helvetica,sans-serif; font-size:12px; font-weight:normal;">
		  <tr style="background-color:#fff;">
			<td  colspan="2" align="center"><img src="<?=HOST_URL?>/admin/images/inner_header_logo.png" height="60"/></td>
		  </tr>
         
          
          <tr>
          <td colspan="2">
              <table width="100%">
               <tr>
                   <td width="100%">
                           <table width="100%" >
                              <tr>
                                <td colspan="2"><div class="txt_ar" dir="rtl" style="font-size:14px; font-weight:normal !important; float:right; text-align:right"> <strong>اسم</strong> :&nbsp;<strong>
                                    <?=$first_name_ar?>&nbsp;<?=$last_name_ar?></div></td>
                              </tr>
                
                              <tr>
                                    <td width="28%"><div class="txt_en" dir="ltr">Name </div></td>
                                    <td width="72%">:&nbsp;<?=$first_name?>&nbsp;<?=$last_name?></td>
                              </tr>
                    
                              <tr>
                                    <td> Gender </td>
                                    <td>:&nbsp;<?=$gender?></td>
                              </tr>
                
                              <tr>
                                <td colspan="2"><div class="txt_ar" dir="rtl" style="font-size:14px; font-weight:normal !important;"> جنس :&nbsp;<strong>
                                    <?=$gender_ar?>
                                    </strong></div>
                                </td>
                              </tr>
                         
                
                              <tr>
                                <td> Mobile No </td>
                                <td> :&nbsp;<?=$mobile?>
                                </td>
                              </tr>
                
                              <tr>
                                <td> Email Id </td>
                                <td> :&nbsp;<?=$email?>
                                </td>
                              </tr>
                
                               <tr>
                                 <td> Emirates ID </td>
                                 <td> :&nbsp;<?=$emirates_id?></td>
                              </tr>
                               <tr>
                                <td> User Id/Password </td>
                                <td> :&nbsp;<?=$user_id?>&nbsp;/&nbsp;<?=$password?></td>
                              </tr>
                          </table>
                   </td>
                   <!--<td width="20%"><?=$pic_path?></td>-->
               </tr>
               </table>
           </td>
          </tr>
          
          <?php if(count($rs_all_parents[$i]['rs_all_children'])>0){
			  
			    $rs_all_children =  $rs_all_parents[$i]['rs_all_children'];
			  
			  
			  ?>
          
          <tr >
              <td colspan="2">
                  <table width="100%">
                     <tr><td><strong>Name</strong></td><td><strong>Class</strong></td></tr>
                    <?php  
					for($k=0;$k<count($rs_all_children);$k++)
                     {  
						$first_name 		= $rs_all_children[$k]["first_name"];
						$first_name_ar     = $rs_all_children[$k]["first_name_ar"];
						$last_name         = $rs_all_children[$k]["last_name"];
						$last_name_ar      = $rs_all_children[$k]["last_name_ar"];
						$tbl_section_id    = $rs_all_children[$k]["tbl_section_id"];
						$class_name        = $rs_all_children[$k]["class_name"];
						$class_name_ar     = $rs_all_children[$k]["class_name_ar"];
						$section_name      = $rs_all_children[$k]["section_name"];
						$section_name_ar   = $rs_all_children[$k]["section_name_ar"];
					 ?>
                         <tr>
                        <td width="33%">
                        <div class="txt_en" style="font-size:10px;" ><?=$first_name?>  <?=$last_name?></div>
                        <div class="txt_ar" style="font-size:10px;" ><?=$first_name_ar?>  <?=$last_name_ar?></div>
                        </td>
                        <td width="67%">:&nbsp;<?=$class_name?> <?=$section_name?>  [::] <?=$class_name_ar?> <?=$section_name_ar?></td>
                        </tr>
                        <tr><td colspan="2">&nbsp;</td></tr>
                 <?php } ?>
                  </table>
                </td>
            </tr>
           <?php } ?> 
            
           
		</table>
        <br>
        
        <?php } ?>
        </body>

</html>
<?php
$output = ob_get_clean( );
$output = htmlentities($output);
?>
<form method="post" name="frmsubmit" id="frmsubmit" action="<?=HOST_URL?>/id_cards_oldversion.php">
	<input type="hidden" name="html" value="<?=$output?>"/>
</form>
<script>
  document.frmsubmit.submit();
</script>