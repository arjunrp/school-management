<?php header('Content-Type: text/html; charset=utf-8');?>
<?php

	$CI =& get_instance();
	$CI->load->model("model_message");
	$CI->load->model("model_teacher_group");

	$arr = array();
	$index = 0;

	for ($i=0; $i<count($rs_groups); $i++) {
		$tbl_teacher_group_id = $rs_groups[$i]["tbl_teacher_group_id"];
		if (trim($tbl_teacher_group_id) == "") {
			$total_groups -=1;
			continue;	
		}

		$rs_group = $CI->model_teacher_group->get_group_details($tbl_teacher_group_id);		

		$group_name_en = $rs_group[0]['group_name_en'];
		$group_name_ar = $rs_group[0]['group_name_ar'];

		$total_messages = $CI->model_message->get_total_unread_messages_against_group($tbl_teacher_group_id, $tbl_teacher_id);

		if (trim($total_messages) == "") {
			$total_messages = 0;
		}

		$group_name = "";

		if ($lan == "ar") {
			$group_name = $group_name_ar;
		} else {
			$group_name = $group_name_en;
		}

		

		$arr[$index]['message_from'] = $group_name;
		$arr[$index]['src_role'] = $group_name;
		$arr[$index]['unread_cnt'] = $total_messages;
		$arr[$index]['tbl_teacher_id'] = $tbl_teacher_id;
		$arr[$index]['tbl_teacher_group_id'] = $tbl_teacher_group_id;
	$index += 1;
	}

	$total_messages = $CI->model_message->get_total_unread_messages_against_group("", $tbl_teacher_id);

	$arr[$index]['message_from'] = "teachers";
	$arr[$index]['src_role'] = "Teachers";
	$arr[$index]['unread_cnt'] = $total_messages;
	$arr[$index]['tbl_teacher_id'] = $tbl_teacher_id;
	$arr[$index]['tbl_teacher_group_id'] = "";

	$arr_roles["roles"] = $arr;
	
	//echo json_encode($arr_roles);
?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> View <small> Messages</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style="float:left; position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home/teacher" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>View Messages</li>
    </ol>
    <!--/BREADCRUMB--> 
    <div style="clear:both"></div>
  </section>
  
  <section class="content"> 
    <!--WORKING AREA-->	
           <!--Listing-->
             <div id="mid1" class="box">
                <div class="box-header">
                  <h3 class="box-title">Messages Inbox</h3>
                  <div class="box-tools">
                   
                  </div>
                </div>
                
                <div class="box-body">
                  <table width="100%" class="table table-bordered table-striped" id="example1">
                    <thead>
                    <tr>
                      <th width="5%" align="center" valign="middle">Ser No.</th>
                      <th align="center" valign="middle">Message Inbox</th>
                      <th width="20%" align="center" valign="middle">&nbsp;</th>
                      </tr>
                     
                    </thead>
                    <tbody>
                    <?php
                        for ($i=0; $i<count($arr); $i++) { 
							$message_from = $arr[$i]['message_from'];
							$src_role = $arr[$i]['src_role'];
							$unread_cnt = $arr[$i]['unread_cnt'];
							$tbl_teacher_id = $arr[$i]['tbl_teacher_id'];
							$tbl_teacher_group_id = $arr[$i]['tbl_teacher_group_id'];
                    ?>
                    <tr>
                      <td align="center" valign="middle"><?=$i+1?></td>
                      <td align="left" valign="middle"><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/teacher_msg_list_page_web/?user_id=<?=$_SESSION['aqdar_smartcare']['tbl_teacher_id_sess']?>&role=T&lan=en&school_id=<?=$_SESSION['aqdar_smartcare']['tbl_school_id_sess']?>&tbl_teacher_id_from=<?=rawurlencode($message_from)?>&tbl_teacher_id_to=<?=$_SESSION['aqdar_smartcare']['tbl_teacher_id_sess']?>&tbl_teacher_group_id=<?=$tbl_teacher_group_id?>&rnd=8"><?=$src_role?></a></td>
                      <td align="left" valign="middle">&nbsp;</td>
                      </tr>
                    <?php } ?>
                    <tr>
                      <td colspan="3" align="right" valign="middle">
                      <?php //echo $this->pagination->create_links(); ?>
                      </td>
                    </tr>
                    <?php 
                        if (count($arr)<=0) {
                    ?>
                    <tr>
                      <td colspan="3" align="center" valign="middle">
                      <div class="alert alert-warning alert-dismissible" style="width:50%">
                        <button aria-hidden="true" data_messages-dismiss="alert" class="close" type="button">×</button>
                        <h4><i class="icon fa fa-info"></i> Information!</h4>
                        There is no information available. 
                      </div>                                
                      </td>
                    </tr>
                    <?php   
                        }
                    ?>
                    </tbody>
                    <tfoot>
                    </tfoot>
                  </table>
                </div>
            </div>        
            <!--/Listing-->
    
    <!--/WORKING AREA--> 
  </section>  
  
  
</div>