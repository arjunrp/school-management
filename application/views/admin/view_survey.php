<?php
//Init Parameters
$survey_questions_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}


?>
<link rel="stylesheet" media="all" type="text/css" href="<?=JS_PATH?>/date_time_picker/css/jquery-ui-1.8.6.custom.css" />
<style type="text/css">
pre {
	padding: 20px;
	background-color: #ffffcc;
	border: solid 1px #fff;
}
.example-container {
	background-color: #f4f4f4;
	border-bottom: solid 2px #777777;
	margin: 0 0 40px 0;
	padding: 20px;
}

.example-container p {
	font-weight: bold;
}

.example-container dt {
	font-weight: bold;
	height: 20px;
}

.example-container dd {
	margin: -20px 0 10px 100px;
	border-bottom: solid 1px #fff;
}

.example-container input {
	width: 150px;
}

.clear {
	clear: both;
}

#ui-datepicker-div {
}

.ui-timepicker-div .ui-widget-header {
	margin-bottom: 8px;
}

.ui-timepicker-div dl {
	text-align: left;
}

.ui-timepicker-div dl dt {
	height: 25px;
}

.ui-timepicker-div dl dd {
	margin: -25px 0 10px 65px;
}

.ui-timepicker-div td {
	font-size: 90%;
}
</style>

<script type="text/javascript" src="<?=JS_PATH?>/date_time_picker/js/jquery-ui-1.8.6.custom.min.js"></script>
<script type="text/javascript" src="<?=JS_PATH?>/date_time_picker/js/jquery-ui-timepicker-addon.js"></script>
<script language="javascript">
	$(document).ready(function(){
		$('#start_date').datepicker({
			dateFormat: '',
			timeFormat: 'hh:mm tt',
			timeOnly: true   
		});


		$('#end_date').datepicker({
			dateFormat: '',
			timeFormat: 'hh:mm tt',
			timeOnly: true   
		});
	});
</script>
<script type="text/javascript" src="<?=JS_COLOR_PATH?>/jscolor.js"></script>

<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/survey/all_survey";
			}
		})
	});
	
function confirm_delete_popup() {
		var len = $("input[id='survey_questions_id_enc']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more survey question(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/survey/deleteSurveyQuestion",
			data: {
				survey_questions_id_enc: $("input[id='survey_questions_id_enc']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Survey Question(s) deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}	
	function ajax_activate(survey_questions_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/survey/activateSurveyQuestion",
			data: {
				survey_questions_id_enc: survey_questions_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Survey Question activated successfully.", 'green')


				$('#act_deact_'+survey_questions_id_enc).html('<span style="cursor:pointer" onClick="ajax_deactivate(\''+survey_questions_id_enc+'\')" onMouseOver="deactivate_me(this)" onMouseOut="reset_activate(this)" class="label label-success">Active</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_deactivate(survey_questions_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/survey/deactivateSurveyQuestion",
			data: {
				survey_questions_id_enc: survey_questions_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Survey Question de-activated successfully.", 'green')
				
				$('#act_deact_'+survey_questions_id_enc).html('<span style="cursor:pointer" onClick="ajax_activate(\''+survey_questions_id_enc+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/survey/is_exist_survey_question",
			data: {
			    tbl_survey_questions_id: "<?=$survey_questions_id_enc?>",
				question_text_en: $('#question_text_en').val(),
				question_text_ar: "",
				is_ajax: true
			},
			success: function(data) {
				
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='T') {
					refresh_page = "N";
					my_alert("Survey Question is already exists.", 'red');
					$("#pre-loader").hide();
				}else if (temp=='Y') {
					refresh_page = "N";
					my_alert("Survey Question is already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_create();
				}
			
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist_edit() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/survey/is_exist_survey_question",
			data: {
				tbl_survey_questions_id: $('#survey_questions_id_enc').val(), 
				question_text_en: $('#question_text_en').val(),
				question_text_ar: "",
				is_ajax: true
			},
			success: function(data) {
				
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='T') {
					refresh_page = "N";
					my_alert("Survey Question is already exists.", 'red');
					$("#pre-loader").hide();
				}else if (temp=='Y') {
					refresh_page = "N";
					my_alert("Survey Question is already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_save_changes();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_create() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/survey/add_survey_question",
			data: {
				tbl_survey_questions_id: "<?=$survey_questions_id_enc?>",
				question_text_en: $('#question_text_en').val(),
				question_text_ar: $('#question_text_ar').val(),
				option_1_en: $('#option_1_en').val(),
				option_1_ar: $('#option_1_ar').val(),
				option_2_en: $('#option_2_en').val(),
				option_2_ar: $('#option_2_ar').val(),
				option_3_en: $('#option_3_en').val(),
				option_3_ar: $('#option_3_ar').val(),
				option_4_en: $('#option_4_en').val(),
				option_4_ar: $('#option_4_ar').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
				refresh_page = "Y";
				    my_alert("Survey Question is added successfully.", 'green');
				    $("#pre-loader").hide();
				}else{
					refresh_page = "N";
					my_alert("Survey Question is added failed, Please try again.", 'red');
					$("#pre-loader").hide();
				}
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_save_changes() {
		$.ajax({

			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/survey/save_survey_question_changes",
			data: {
				tbl_survey_questions_id: $('#survey_questions_id_enc').val(), 
			    question_text_en: $('#question_text_en').val(),
				question_text_ar: $('#question_text_ar').val(),
				
				option_1_id: $('#option_1_id').val(),
				option_1_en: $('#option_1_en').val(),
				option_1_ar: $('#option_1_ar').val(),
				
				option_2_id: $('#option_2_id').val(),
				option_2_en: $('#option_2_en').val(),
				option_2_ar: $('#option_2_ar').val(),
				
				option_3_id: $('#option_3_id').val(),
				option_3_en: $('#option_3_en').val(),
				option_3_ar: $('#option_3_ar').val(),
				
				option_4_id: $('#option_4_id').val(),
				option_4_en: $('#option_4_en').val(),
				option_4_ar: $('#option_4_ar').val(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Changes saved successfully.", 'green');
				
				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
</script>
<script language="javascript">
	function ajax_validate() {
		if (validate_question_name() == false || validate_question_name_ar() == false || validate_option1() == false || validate_option1_ar() == false || 
		validate_option2() == false || validate_option2_ar() == false  || validate_option3() == false || validate_option3_ar() == false  || validate_option4() == false  || validate_option4_ar() == false ) {
			return false;
		} else {
			is_exist();
		}
	} 

	function ajax_validate_edit() {
		if (validate_question_name() == false || validate_question_name_ar() == false || validate_option1() == false || validate_option1_ar() == false || 
		validate_option2() == false || validate_option2_ar() == false  || validate_option3() == false || validate_option3_ar() == false || validate_option4() == false || validate_option4_ar() == false  ) {
			return false;
		} else {
			is_exist_edit();
		}
	} 

	function validate_question_name() {
		var regExp = / /g;
		var str = $('#question_text_en').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Survey Question [En] is blank. Please enter Survey Question [En].");
		return false;
		}
	}

	function validate_question_name_ar() {
		var regExp = / /g;
		var str = $('#question_text_ar').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Survey Question [Ar] is blank. Please enter Survey Question [Ar].");
		return false;
		}
	}
	
	
	 function validate_option1() {
		var regExp = / /g;
		var str = $('#option_1_en').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Option 1 [En] is blank. Please enter Option 1 [En].");
		return false;
		}
	}
	 
	 function validate_option1_ar() {
		var regExp = / /g;
		var str = $('#option_1_ar').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Option 1 [Ar] is blank. Please enter Option 1 [Ar].");
		return false;
		}
	}
	
	function validate_option2() {
		var regExp = / /g;
		var str = $('#option_2_en').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Option 2 [En] is blank. Please enter Option 2 [En].");
		return false;
		}
	}
	
    function validate_option2_ar() {
		var regExp = / /g;
		var str = $('#option_2_ar').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Option 2 [Ar] is blank. Please enter Option 2 [Ar].");
		return false;
		}
	}
	
	function validate_option3() {
		var regExp = / /g;
		var str = $('#option_3_en').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Option 3 [En] is blank. Please enter Option 3 [En].");
		return false;
		}
	}
	
	 function validate_option3_ar() {
		var regExp = / /g;
		var str = $('#option_3_ar').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Option 3 [Ar] is blank. Please enter Option 3 [Ar].");
		return false;
		}
	}
	
	function validate_option4() {
		var regExp = / /g;
		var str = $('#option_4_en').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Option 4 [En] is blank. Please enter Option 4 [En].");
		return false;
		}
	}
	
	 function validate_option4_ar() {
		var regExp = / /g;
		var str = $('#option_4_ar').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Option 4 [Ar] is blank. Please enter Option 4 [Ar].");
		return false;
		}
	}


</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Survey <small> Management</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Survey</li>
    </ol>
    <!--/BREADCRUMB--> 
    <div style="clear:both"></div>
  </section>
  
  <section class="content"> 
    <!--WORKING AREA-->
    
    <?php
	   	if (trim($mid) == "4" || trim($mid) == 4) {
			
			$question_text_en  = $survey_question_obj['question_text_en'];
			$question_text_ar  = $survey_question_obj['question_text_ar'];
			
			$option_en1        = $survey_question_option_obj[0]['option_en'];
			$option_ar1        = $survey_question_option_obj[0]['option_ar'];
			$option_count1     = $survey_question_option_obj[0]['option_count'];
			
			$option_en2        = $survey_question_option_obj[1]['option_en'];
			$option_ar2        = $survey_question_option_obj[1]['option_ar'];
			$option_count2     = $survey_question_option_obj[1]['option_count'];
			
			$option_en3        = $survey_question_option_obj[2]['option_en'];
			$option_ar3        = $survey_question_option_obj[2]['option_ar'];
			$option_count3     = $survey_question_option_obj[2]['option_count'];
			
			$option_en4        = $survey_question_option_obj[3]['option_en'];
			$option_ar4        = $survey_question_option_obj[3]['option_ar'];
			$option_count4     = $survey_question_option_obj[3]['option_count'];
		
	?>
			<!-- Donut chart -->

            <div class="box box-primary">
              <div class="box-header with-border"> <i class="fa fa-bar-chart-o"></i>
                <h3 class="box-title"><?=$question_text_en?>&nbsp;&nbsp; [::] &nbsp;&nbsp; <?=$question_text_ar?></h3>
                <div class="box-tools pull-right">
                	<div class="box-tools">
                    	<a href="<?=HOST_URL?>/en/admin/survey/all_survey"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  	</div>
                  
                </div>
              </div>
              <br>
              <div class="row">
              	<div class="col-sm-1">
                	&nbsp;
                </div>
                <div class="col-sm-1">
                	<?=$option_en1?>
                </div>
                <div class="col-sm-1">
                	<?=$option_ar1?>
                </div>
                <div class="col-sm-1">
                	<?=$option_count1?>
                </div>
                <div class="col-sm-1">
                	<div class="color_survey1"></div>
                </div>
              </div>
              <div class="row">
              	<div class="col-sm-1">
                	&nbsp;
                </div>
                <div class="col-sm-1">
                	<?=$option_en2?>
                </div>
                <div class="col-sm-1">
                	<?=$option_ar2?>
                </div>
                <div class="col-sm-1">
                	<?=$option_count2?>
                </div>
                <div class="col-sm-1">
                	<div class="color_survey2"></div>
                </div>
              </div>
              <div class="row">
              	<div class="col-sm-1">
                	&nbsp;
                </div>
                <div class="col-sm-1">
                	<?=$option_en3?>
                </div>
                <div class="col-sm-1">
                	<?=$option_ar3?>
                </div>
                <div class="col-sm-1">
                	<?=$option_count3?>
                </div>
                <div class="col-sm-1">
                	<div class="color_survey3"></div>
                </div>
              </div>
              <div class="row">
              	<div class="col-sm-1">
                	&nbsp;
                </div>
                <div class="col-sm-1">
                	<?=$option_en4?>
                </div>
                <div class="col-sm-1">
                	<?=$option_ar4?>
                </div>
                <div class="col-sm-1">
                	<?=$option_count4?>
                </div>
                <div class="col-sm-1">
                	<div class="color_survey4"></div>
                </div>
              </div>
              <div class="box-body" style="height:400px;">
                <canvas id="pieChart"></canvas>
              </div>
              <!-- /.box-body--> 
            </div>
             
	<style type="text/css">
        .color_survey1 , .color_survey2 , .color_survey3 , .color_survey4 {width:60px; height:10px;}
        .color_survey1 {background-color:#f56954}
        .color_survey2 {background-color:#00a65a}
        .color_survey3 {background-color:#f39c12}
        .color_survey4 {background-color:#00c0ef}
    </style>

<script>
  $(function () {
    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
    var pieChart = new Chart(pieChartCanvas);
    var PieData = [
      {
        value: <?=$option_count1?>,
        color: "#f56954",
        highlight: "#f56954",
        label: "<?=$option_en1?>"

      },
      {
        value: <?=$option_count2?>,
        color: "#00a65a",
        highlight: "#00a65a",
        label: "<?=$option_en2?>"
      },
      {
        value: <?=$option_count3?>,
        color: "#f39c12",
        highlight: "#f39c12",
        label: "<?=$option_en3?>"
      },
      {
        value: <?=$option_count4?>,
        color: "#00c0ef",
        highlight: "#00c0ef",
        label: "<?=$option_en4?>"
      }
    ];
    var pieOptions = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke: true,
      //String - The colour of each segment stroke
      segmentStrokeColor: "#fff",
      //Number - The width of each segment stroke
      segmentStrokeWidth: 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps: 100,
      //String - Animation easing effect
      animationEasing: "easeOutBounce",
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate: true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale: false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: false,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    };
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions);

  });
</script>

    <?php
		} else if (trim($mid) == "3" || trim($mid) == 3) {
			$tbl_sel_survey_questions_id = $survey_question_obj['tbl_survey_questions_id'];
			$question_text_en        = $survey_question_obj['question_text_en'];
			$question_text_ar        = $survey_question_obj['question_text_ar'];
			$option_1_id             = $survey_question_option_obj['option_1_id'];
			$option_1_en             = $survey_question_option_obj['option_1_en'];
			$option_1_ar             = $survey_question_option_obj['option_1_ar'];
			
			$option_2_id             = $survey_question_option_obj['option_2_id'];
			$option_2_en             = $survey_question_option_obj['option_2_en'];
			$option_2_ar             = $survey_question_option_obj['option_2_ar'];
			
			$option_3_id             = $survey_question_option_obj['option_3_id'];
			$option_3_en             = $survey_question_option_obj['option_3_en'];
			$option_3_ar             = $survey_question_option_obj['option_3_ar'];
			
			$option_4_id             = $survey_question_option_obj['option_4_id'];
			$option_4_en             = $survey_question_option_obj['option_4_en'];
			$option_4_ar             = $survey_question_option_obj['option_4_ar'];
	?>
        <!--Edit-->
        
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Survey</h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/survey/all_survey"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_edit" id="frm_listing" class="form-horizontal" method="post">
                    <div class="box-body">
                   
                  <div class="form-group">
                      <label class="col-sm-2 control-label" for="question_text_en">Survey Question [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <textarea tabindex="1" dir="ltr" id="question_text_en" placeholder="Survey Question [En]" name="question_text_en" class="form-control"  > <?=$question_text_en?> </textarea>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="question_text_ar">Survey Question [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                       <textarea tabindex="2"  id="question_text_ar" placeholder="Survey Question [Ar]" name="question_text_ar" class="form-control" dir="rtl" > <?=$question_text_ar?> </textarea>
                      </div>
                    </div>
                  
                     <div class="form-group">
                     <label class="col-sm-2 control-label" for="description"></label>
                     <div class="col-sm-5">
                              English
                     </div>
                     <div class="col-sm-5">
                              Arabic
                     </div>
                     </div>
                     
                    <div class="form-group">
                     <label class="col-sm-2 control-label" for="option_1_en">Option 1<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-5">
                              <input type="hidden" name="option_1_id" id="option_1_id" class="form-control"  value="<?=$option_1_id?>" >
                              <input type="text" placeholder="Option 1 [En]" id="option_1_en" name="option_1_en" class="form-control" tabindex="3" value="<?=$option_1_en?>" >
                     </div>
                     <div class="col-sm-5">
                              <input type="text" placeholder="Option 1 [Ar]" id="option_1_ar" name="option_1_ar" class="form-control" dir="rtl" tabindex="4"  value="<?=$option_1_ar?>"  >
                     </div>
                     </div>
                   
                     <div class="form-group">
                     <label class="col-sm-2 control-label" for="option_2_en">Option 2<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-5">
                              <input type="hidden" name="option_2_id" id="option_2_id"   class="form-control"  value="<?=$option_2_id?>" >
                              <input type="text" placeholder="Option 2 [En]" id="option_2_en" name="option_2_en" class="form-control" tabindex="5"  value="<?=$option_2_en?>" >
                     </div>
                     <div class="col-sm-5">
                              <input type="text" placeholder="Option 2 [Ar]" id="option_2_ar" name="option_2_ar" class="form-control" dir="rtl" tabindex="6"  value="<?=$option_2_ar?>" >
                     </div>
                     </div>
                     
                     
                     <div class="form-group">
                     <label class="col-sm-2 control-label" for="option_3_en">Option 3<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-5">
                               <input type="hidden" name="option_3_id" id="option_3_id"   class="form-control"  value="<?=$option_3_id?>" >
                              <input type="text" placeholder="Option 3 [En]" id="option_3_en" name="option_3_en" class="form-control" tabindex="7" value="<?=$option_3_en?>" >
                     </div>
                     <div class="col-sm-5">
                              <input type="text" placeholder="Option 3 [Ar]" id="option_3_ar" name="option_3_ar" class="form-control" dir="rtl" tabindex="8"  value="<?=$option_3_ar?>" >
                     </div>
                     </div>
                     
                       <div class="form-group">
                     <label class="col-sm-2 control-label" for="option_4_en">Option 4</label>
                     <div class="col-sm-5">
                              <input type="hidden" name="option_4_id" id="option_4_id"   class="form-control"  value="<?=$option_4_id?>" >
                              <input type="text" placeholder="Option 4 [En]" id="option_4_en" name="option_4_en" class="form-control" tabindex="9" value="<?=$option_4_en?>"  >
                     </div>
                     <div class="col-sm-5">
                              <input type="text" placeholder="Option 4 [Ar]" id="option_4_ar" name="option_4_ar" class="form-control" dir="rtl" tabindex="10" value="<?=$option_4_ar?>"  >
                     </div>
                     </div>
                   </div> 
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate_edit()">Save Changes</button>
                    <input type="hidden" name="survey_questions_id_enc" id="survey_questions_id_enc" value="<?=$tbl_sel_survey_questions_id?>" />
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  
                  <!-- /.box-footer -->
                </form>
              </div>
		        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/survey/all_survey";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
    
  
 <link href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" rel="stylesheet">
 <script src="http://code.jquery.com/jquery-1.11.1.js"></script>
 <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
  <script>
  $( function() {

		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
				
				/* var order = $("#tabledivbody").sortable("serialize");
				
				alert(order);
				
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/your/url/here'
				});*/
				
				
				
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	

				
				
				
				
				
			}
	  } );
  
  } );
  
  
  
  </script> 
    
                    <div id="mid1" class="box box-success">
                        <div class="box-header">
                          <div class="col-sm-1" >
                          <h3 class="box-title">SEARCH</h3>
                          </div>
                          <div class="col-sm-11"> 
                             
                               <div class="col-sm-6"><input name="q" id="q" value="<?=urldecode($q)?>" type="text" class="form-control" placeholder="Search By Question"   > </div>
                               <div class="col-sm-2"><button class="btn btn-success" type="button" onclick="search_data()">Search</button>&nbsp;<button class="btn btn-success" type="button" 
                               onclick="reset_data();">Reset</button>
                               </div>
                           
                          </div>
                        </div>  
                     </div>     
    
            <!--Listing-->
                    <div id="mid1_list" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Survey</h3>
                          <div class="box-tools">
                            <?php if (count($rs_all_survey)>0) { echo $paging_string;}?>	
                            <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
                            <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>
                          </div>
                        </div>
                        
                        <div class="box-body">
                     <!--   <div style="color:#030; font-weight:bold;">You can sort categories by using drag and drop of rows </div>-->
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle"><input id="select_all" type="checkbox" value="" /></th>
                              <!--<th width="10%" align="center" valign="middle">Sl No.</th>-->
                              <th width="25%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y">Survey Question [En] 
	                              <?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?>
                                  <div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>
                              <th width="25%" align="center" valign="middle">Survey Question [Ar]</th>
                              <th width="10%" align="center" valign="middle">Date</th>
                              <th width="7%" align="center" valign="middle">Status</th>
                              <th width="7%" align="center" valign="middle">Action</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                                for ($i=0; $i<count($rs_all_survey); $i++) { 
                                    $id                        = $rs_all_survey[$i]['id'];
                                    $tbl_survey_questions_id   = $rs_all_survey[$i]['tbl_survey_questions_id'];
                                    $question_text_en          = $rs_all_survey[$i]['question_text_en'];
                                    $question_text_ar          = $rs_all_survey[$i]['question_text_ar'];
                                    $added_date                = $rs_all_survey[$i]['added_date'];
                                    $is_active                 = $rs_all_survey[$i]['is_active'];
                                    
                                    $title                     = ucfirst($title);
                                    $added_date                = date('m-d-Y',strtotime($added_date));
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_survey_questions_id?>" >
                              <td align="left" valign="middle">
                              <span style="float:left;">
                              <input id="survey_questions_id_enc" name="survey_questions_id_enc" class="checkbox" type="checkbox" value="<?=$tbl_survey_questions_id?>" />
                              </span>
                              
                             <?php /*?> <span style="float:left;">&nbsp;
                              <?php if($i<>0){ ?> <i class="fa fa-arrow-up"  style="color:#3c8dbc; cursor:pointer;"  aria-hidden="true" title="Sorting - Drag & Drop To Up"></i> &nbsp; <?php } ?>
                               <?php if($i<> count($rs_all_categories)-1){ ?> <i class="fa fa-arrow-down" style="color:#3c8dbc;cursor:pointer;" aria-hidden="true" title="Sorting - Drag & Drop To Down"></i> <?php } ?>
                              </span><?php */?>
                              </td>
                             <!-- <td align="left" valign="middle"><?=$offset+$i+1?></td>-->
                              <td align="left" valign="middle"><?=$question_text_en?></td>
                              <td align="left" valign="middle"><?=$question_text_ar?></td>
                              <td align="left" valign="middle"><?=$added_date?></td>
                              <td align="left" valign="middle">
                                <div id="act_deact_<?=$tbl_survey_questions_id?>">
                                <?php if (trim($is_active) == "Y") { ?>
                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_survey_questions_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                <?php } else { ?>
                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_survey_questions_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                <?php } ?>
                                </div>
                              </td>
                              <td align="left" valign="middle">
                                <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/survey/edit_survey_question/survey_questions_id_enc/<?=$tbl_survey_questions_id?>"><button class="btn bg-purple fa fa-pencil" type="button" title="Edit"></button>&nbsp;<a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/survey/stats_survey/survey_questions_id_enc/<?=$tbl_survey_questions_id?>"><button class="btn bg-green fa ion-stats-bars" type="button" title="Statistics"></button></a>
                              </td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="9" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_all_survey)<=0) {
                            ?>
                            <tr>
                              <td colspan="9" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no survey available. Click on the + button to create one.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
    
            <!--Add or Create-->
              <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Survey Question</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                  
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="question_text_en">Survey Question [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <textarea tabindex="1" dir="ltr" id="question_text_en" placeholder="Survey Question [En]" name="question_text_en" class="form-control"  ></textarea>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="question_text_ar">Survey Question [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                       <textarea tabindex="2"  id="question_text_ar" placeholder="Survey Question [Ar]" name="question_text_ar" class="form-control" dir="rtl"  ></textarea>
                      </div>
                    </div>
                  
                     <div class="form-group">
                     <label class="col-sm-2 control-label" for="description"></label>
                     <div class="col-sm-5">
                              English
                     </div>
                     <div class="col-sm-5">
                              Arabic
                     </div>
                     </div>
                     
                    <div class="form-group">
                     <label class="col-sm-2 control-label" for="option_1_en">Option 1<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-5">
                              <input type="text" placeholder="Option 1 [En]" id="option_1_en" name="option_1_en" class="form-control" tabindex="3" >
                     </div>
                     <div class="col-sm-5">
                              <input type="text" placeholder="Option 1 [Ar]" id="option_1_ar" name="option_1_ar" class="form-control" dir="rtl" tabindex="4" >
                     </div>
                     </div>
                   
                     <div class="form-group">
                     <label class="col-sm-2 control-label" for="option_2_en">Option 2<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-5">
                              <input type="text" placeholder="Option 2 [En]" id="option_2_en" name="option_2_en" class="form-control" tabindex="5" >
                     </div>
                     <div class="col-sm-5">
                              <input type="text" placeholder="Option 2 [Ar]" id="option_2_ar" name="option_2_ar" class="form-control" dir="rtl" tabindex="6" >
                     </div>
                     </div>
                     
                     
                     <div class="form-group">
                     <label class="col-sm-2 control-label" for="option_3_en">Option 3<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-5">
                              <input type="text" placeholder="Option 3 [En]" id="option_3_en" name="option_3_en" class="form-control" tabindex="7">
                     </div>
                     <div class="col-sm-5">
                              <input type="text" placeholder="Option 3 [Ar]" id="option_3_ar" name="option_3_ar" class="form-control" dir="rtl" tabindex="8" >
                     </div>
                     </div>
                     
                       <div class="form-group">
                     <label class="col-sm-2 control-label" for="option_4_en">Option 4</label>
                     <div class="col-sm-5">
                              <input type="text" placeholder="Option 4 [En]" id="option_4_en" name="option_4_en" class="form-control" tabindex="9" >
                     </div>
                     <div class="col-sm-5">
                              <input type="text" placeholder="Option 4 [Ar]" id="option_4_ar" name="option_4_ar" class="form-control" dir="rtl" tabindex="10" >
                     </div>
                     </div>
                      
                   
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate()" tabindex="11">Submit</button>
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
            <!--/Add or Create-->
                
        <!--/Admin Category Management-->
        

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>

        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >
function search_data() {
		var q = $("#q").val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/survey/all_survey/";
		
		if(q !='')
			url += "q/"+q+"/";
		
			url += "offset/0/";
		window.location.href = url;
	}

function reset_data() {
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/survey/all_survey/";
		url += "offset/0/";
		window.location.href = url;
	}


</script>