<?php
$CI =& get_instance();
$CI->load->model("model_student");	

if (trim($mid) == "") {
	$mid = "1";	
}

?>
<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Absentees <small> Management</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style="float:left; position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home/teacher" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Absentees Management</li>
    </ol>
    <!--/BREADCRUMB--> 
    <div style="clear:both"></div>
  </section>
  
  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {
	?>
        <!--Edit-->
        <!--/Edit-->
	<?php							
		} else {
			
		
	?>   
            <!--Listing-->
            
                     <div id="mid1" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Students</h3>
                          <div class="box-tools">
                           
                          </div>
                        </div>
                        
                        <div class="box-body">
                          <table width="100%" class="table table-bordered table-striped" id="example1">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle">Ser No.</th>
                              <th width="25%" align="center" valign="middle">Picture</th>
                              <th width="20%" align="center" valign="middle">Name/Comments</th>
                              <th width="20%" align="center" valign="middle">Date/Time</th>
                              <th width="25%" align="center" valign="middle">Duration</th>
                              </tr>
                             
                            </thead>
                            <tbody>
                            <?php
                                for ($i=0; $i<count($data); $i++) { 
                                    $id = $data[$i]['id'];
                                    $tbl_student_id = $data[$i]['tbl_student_id'];
                                    $comments_parent = $data[$i]['comments_parent'];
                                    $start_date = $data[$i]['start_date'];
                                    $end_date = $data[$i]['end_date'];

                                    $added_date = $data[$i]['added_date'];
                                    $is_active = $data[$i]['is_active'];
                                    
                                    $added_date = date('m-d-Y',strtotime($added_date));

									//echo $tbl_student_id."<br />"; 
									
									$student_obj = $CI->model_student->get_student_obj($tbl_student_id);
									$tbl_student_id = $student_obj[0]['tbl_student_id'];
									$tbl_class_id = $student_obj[0]['tbl_class_id'];
									$first_name = $student_obj[0]['first_name'];	
									$last_name = $student_obj[0]['last_name'];	
									$file_name_updated = $student_obj[0]['file_name_updated'];
									
									$img_path = IMG_PATH."/student/".$file_name_updated;
									
									
									
									$today_date = date('Y-m-d');
									$today_date=date('Y-m-d', strtotime($today_date));;
									//echo $today_date; // echos today! 
									$start_date = date('Y-m-d', strtotime($start_date));
									$end_date = date('Y-m-d', strtotime($end_date));
								
									$color = "#000";
									if (($today_date > $start_date) && ($today_date < $end_date)) {
									  $color = "#CC0000";
									} 									
                            ?>
                            <tr style="color:<?=$color?>">
                              <td align="center" valign="middle"><?=$i+1?></td>
                              <td align="left" valign="middle"><?php if (trim($file_name_updated)!=""){	?><img src="<?=$img_path?>" style="width:100%; max-width:200px" /><?php	}	?></td>
                              <td align="left" valign="middle">
								  	<?=$first_name?> <?=$last_name?>
                                    <hr />
									<?=$comments_parent?>                                    
                              </td>
                              <td align="left" valign="middle">On <?=$added_date?></td>
                              <td align="left" valign="middle">Starts On <?=$start_date?>, Till <?=$end_date?></td>
                              </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="5" align="right" valign="middle">
                              <?php //echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($data)<=0) {
                            ?>
                            <tr>
                              <td colspan="5" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There is no information available. 
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
    
            <!--Add or Create-->
            <!--/Add or Create-->
                
        <!--/Admin User Management-->

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>
        
    <!--/WORKING AREA--> 
  </section>
</div>