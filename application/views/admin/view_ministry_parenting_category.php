<?php
//Init Parameters
$parenting_cat_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}
?>

<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/ministry_parenting_category/all_ministry_parenting_categorys";
			}
		})
	});
	
	function confirm_delete_popup() {
		var len = $("input[id='parenting_cat_id_enc']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more parenting category(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/ministry_parenting_category/delete",
			data: {
				parenting_cat_id_enc: $("input[id='parenting_cat_id_enc']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Parenting Category(s) deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_activate(parenting_cat_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/ministry_parenting_category/activate",
			data: {
				parenting_cat_id_enc: parenting_cat_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Parenting Category activated successfully.", 'green')

				$('#act_deact_'+parenting_cat_id_enc).html('<span style="cursor:pointer" onClick="ajax_deactivate(\''+parenting_cat_id_enc+'\')" onMouseOver="deactivate_me(this)" onMouseOut="reset_activate(this)" class="label label-success">Active</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_deactivate(parenting_cat_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/ministry_parenting_category/deactivate",
			data: {
				parenting_cat_id_enc: parenting_cat_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Parenting Category de-activated successfully.", 'green')
				
				$('#act_deact_'+parenting_cat_id_enc).html('<span style="cursor:pointer" onClick="ajax_activate(\''+parenting_cat_id_enc+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist() {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/ministry_parenting_category/is_exist",
			data: {
				parenting_cat_id_enc: "",
				title_en: $('#title_en').val(),
				title_ar: "",
				is_ajax: true
			},
			success: function(data) {
				var str = new String(data);
				if (str.indexOf("*N*")==0) {
					ajax_create();
				} else if (str.indexOf("*Y*")==0) {
					refresh_page = "N";
					my_alert("Parenting Category already exists.", 'red');
					$("#pre-loader").hide();
				}
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist_edit() {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/ministry_parenting_category/is_exist",
			data: {
				parenting_cat_id_enc:$('#parenting_cat_id_enc').val(),
				title_en: $('#title_en').val(),
				title_ar: "",
				is_ajax: true
			},
			success: function(data) {
				var str = new String(data);
				if (str.indexOf("*N*")==0) {
					ajax_save_changes();
				} else if (str.indexOf("*Y*")==0) {
					refresh_page = "N";
					my_alert("Parenting Category already exists.", 'red');
					$("#pre-loader").hide();
				}
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_create() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/ministry_parenting_category/create_ministry_parenting_category",
			data: {
				parenting_cat_id_enc: "<?=$parenting_cat_id_enc?>",
				title_en: $('#title_en').val(),
				title_ar: $('#title_ar').val(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Parenting Category created successfully.", 'green');
				
				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_save_changes() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/ministry_parenting_category/save_changes",
			data: {
				parenting_cat_id_enc:$('#parenting_cat_id_enc').val(),
				title_en: $('#title_en').val(),
				title_ar: $('#title_ar').val(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Changes saved successfully.", 'green');
				
				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
</script>
<script language="javascript">
	function ajax_validate() {
		if ( validate_title_en() == false || validate_title_ar() == false ) {
			return false;
		} else {
			is_exist();
		}
	} 

	function ajax_validate_edit() {
		if ( validate_title_en() == false || validate_title_ar() == false ) {
			return false;
		} else {
			is_exist_edit();
		}
	} 


	function validate_title_en() {
		var regExp = / /g;
		var str = $('#title_en').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Parenting Category Name[En] is blank. Please enter Parenting Category Name[En].");
		return false;
		}
	}

	function validate_title_ar() {
		var regExp = / /g;
		var str = $('#title_ar').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Parenting Category Name[Ar] is blank. Please enter Parenting Category Name[Ar].");
		return false;
		}
	}

</script>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Parenting Category(s) <small> Management</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style="float:left; position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home"><i class="fa fa-home"></i>Home</a></li>
      <li> Parenting Category(s) Management</li>
    </ol>
    <!--/BREADCRUMB--> 
    <div style="clear:both"></div>
  </section>
  
  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {

			$tbl_parenting_cat_id = $ministry_parenting_category_obj['tbl_parenting_cat_id'];
			$title_en = $ministry_parenting_category_obj['title_en'];
			$title_ar = $ministry_parenting_category_obj['title_ar'];
			$added_date = $ministry_parenting_category_obj['added_date'];
			$is_active = $ministry_parenting_category_obj['is_active'];
	?>
        <!--Edit-->
        
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Parenting Category</h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/ministry_parenting_category/all_ministry_parenting_categorys"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_edit" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                  
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="title_en">Parenting Category Name[En]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Parenting Category Name[En]" id="title_en" name="title_en" class="form-control" value="<?=$title_en?>">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="title_ar">Parenting Category Name[Ar]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Parenting Category Name[Ar]" id="title_ar" name="title_ar" class="form-control" value="<?=$title_ar?>" dir="rtl">
                      </div>
                    </div>
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate_edit()">Save Changes</button>
                    <input type="hidden" name="parenting_cat_id_enc" id="parenting_cat_id_enc" value="<?=$tbl_parenting_cat_id?>" />
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
		        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/ministry_parenting_category/all_ministry_parenting_categorys";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>   
    
            <!--Listing-->
                    <div id="mid1" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Parenting Category(s)</h3>
                          <div class="box-tools">
                            <?php if (count($rs_all_ministry_parenting_categorys)>0) { echo $paging_string;}?>	
                            <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
                            <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>
                          </div>
                        </div>
                        
                        <div class="box-body">
                          <table width="100%" class="table table-bordered table-striped" id="example1">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle"><input id="select_all" type="checkbox" value="" /></th>
                              <th width="10%" align="center" valign="middle">Ser No.</th>
                              <th width="25%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y">Parenting Category Name[En] <?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?>
	                              <div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>
                              <th width="25%" align="center" valign="middle">Parenting Category Name[Ar]</th>
                              <th width="15%" align="center" valign="middle">Date</th>
                              <th width="10%" align="center" valign="middle">Status</th>
                              <th width="10%" align="center" valign="middle">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                                for ($i=0; $i<count($rs_all_ministry_parenting_categorys); $i++) { 
                                    $id = $rs_all_ministry_parenting_categorys[$i]['id'];
                                    $tbl_parenting_cat_id = $rs_all_ministry_parenting_categorys[$i]['tbl_parenting_cat_id'];
                                    $title_en = $rs_all_ministry_parenting_categorys[$i]['title_en'];
                                    $title_ar = $rs_all_ministry_parenting_categorys[$i]['title_ar'];
                                    $added_date = $rs_all_ministry_parenting_categorys[$i]['added_date'];
                                    $is_active = $rs_all_ministry_parenting_categorys[$i]['is_active'];
                                    
                                    $title_en = ucfirst($title_en);
                                    $added_date = date('m-d-Y',strtotime($added_date));
                            ?>
                            <tr>
                              <td align="left" valign="middle">
                                <input id="parenting_cat_id_enc" name="parenting_cat_id_enc" class="checkbox" type="checkbox" value="<?=$tbl_parenting_cat_id?>" />
                              </td>
                              <td align="left" valign="middle"><?=$i+1?></td>
                              <td align="left" valign="middle"><?=$title_en?></td>
                              <td align="left" valign="middle"><?=$title_ar?></td>
                              <td align="left" valign="middle"><?=$added_date?></td>
                              <td align="left" valign="middle">
                                <div id="act_deact_<?=$tbl_parenting_cat_id?>">
                                <?php if (trim($is_active) == "Y") { ?>
                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_parenting_cat_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                <?php } else { ?>
                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_parenting_cat_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                <?php } ?>
                                </div>
                              </td>
                              <td align="left" valign="middle">
                                <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/ministry_parenting_category/edit_ministry_parenting_category/parenting_cat_id_enc/<?=$tbl_parenting_cat_id?>"><button class="btn bg-purple fa fa-pencil" type="button" title="Edit"></button></a>
                              </td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="7" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_all_ministry_parenting_categorys)<=0) {
                            ?>
                            <tr>
                              <td colspan="7" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no job categrories available. Click on the + button to create one.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
    
            <!--Add or Create-->
              <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Create Parenting Category</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                  
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="title_en">Parenting Category Name[En]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Parenting Category Name[En]" id="title_en" name="title_en" class="form-control">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="title_ar">Parenting Category Name[Ar]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Parenting Category Name[Ar]" id="title_ar" name="title_ar" class="form-control" dir="rtl">
                      </div>
                    </div>
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate()">Create Parenting Category</button>
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
            <!--/Add or Create-->
                
        <!--/Admin Parenting Category Management-->

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>

        
    <!--/WORKING AREA--> 
  </section>
</div>