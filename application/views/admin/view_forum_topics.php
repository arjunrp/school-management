<?php
//Init Parameters
$parent_forum_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}


?>
<link rel="stylesheet" media="all" type="text/css" href="<?=JS_PATH?>/date_time_picker/css/jquery-ui-1.8.6.custom.css" />
<style type="text/css">
pre {
	padding: 20px;
	background-color: #ffffcc;
	border: solid 1px #fff;
}
.example-container {
	background-color: #f4f4f4;
	border-bottom: solid 2px #777777;
	margin: 0 0 40px 0;
	padding: 20px;
}

.example-container p {
	font-weight: bold;
}

.example-container dt {
	font-weight: bold;
	height: 20px;
}

.example-container dd {
	margin: -20px 0 10px 100px;
	border-bottom: solid 1px #fff;
}

.example-container input {
	width: 150px;
}

.clear {
	clear: both;
}

#ui-datepicker-div {
}

.ui-timepicker-div .ui-widget-header {
	margin-bottom: 8px;
}

.ui-timepicker-div dl {
	text-align: left;
}

.ui-timepicker-div dl dt {
	height: 25px;
}

.ui-timepicker-div dl dd {
	margin: -25px 0 10px 65px;
}

.ui-timepicker-div td {
	font-size: 90%;
}
</style>

<script type="text/javascript" src="<?=JS_PATH?>/date_time_picker/js/jquery-ui-1.8.6.custom.min.js"></script>
<script type="text/javascript" src="<?=JS_PATH?>/date_time_picker/js/jquery-ui-timepicker-addon.js"></script>
<script language="javascript">
	$(document).ready(function(){
		$('#start_date').datepicker({
			dateFormat: '',
			timeFormat: 'hh:mm tt',
			timeOnly: true   
		});


		$('#end_date').datepicker({
			dateFormat: '',
			timeFormat: 'hh:mm tt',
			timeOnly: true   
		});
	});
</script>
<script type="text/javascript" src="<?=JS_COLOR_PATH?>/jscolor.js"></script>

<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/forum/all_topics";
			}
		})
	});
	
function confirm_delete_popup() {
		var len = $("input[id='parent_forum_id_enc']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more topic(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/forum/deleteTopic",
			data: {
				parent_forum_id_enc: $("input[id='parent_forum_id_enc']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Topic(s) deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}	
	function ajax_activate(parent_forum_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/forum/activateTopic",
			data: {
				parent_forum_id_enc: parent_forum_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Topic activated successfully.", 'green')

				$('#act_deact_'+parent_forum_id_enc).html('<span style="cursor:pointer" onClick="ajax_deactivate(\''+parent_forum_id_enc+'\')" onMouseOver="deactivate_me(this)" onMouseOut="reset_activate(this)" class="label label-success">Active</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_deactivate(parent_forum_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/forum/deactivateTopic",
			data: {
				parent_forum_id_enc: parent_forum_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Topic de-activated successfully.", 'green')
				
				$('#act_deact_'+parent_forum_id_enc).html('<span style="cursor:pointer" onClick="ajax_activate(\''+parent_forum_id_enc+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/forum/is_exist_topic",
			data: {
			    tbl_parent_forum_id: "<?=$parent_forum_id_enc?>",
				forum_topic: $('#forum_topic').val(),
				is_ajax: true
			},
			success: function(data) {
				
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='T') {
					refresh_page = "N";
					my_alert("Topic is already exists.", 'red');
					$("#pre-loader").hide();
				}else if (temp=='Y') {
					refresh_page = "N";
					my_alert("Topic already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_add_topic();
				}
			
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist_edit() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/forum/is_exist_topic",
			data: {
			    tbl_parent_forum_id: $('#parent_forum_id_enc').val(),
				forum_topic: $('#forum_topic').val(),
				is_ajax: true
			},
			success: function(data) {
				
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='T') {
					refresh_page = "N";
					my_alert("Topic is already exists.", 'red');
					$("#pre-loader").hide();
				}else if (temp=='Y') {
					refresh_page = "N";
					my_alert("Topic is already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_update_topic();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_add_topic() {
		
		var selectednumbers='';
        	$('#tbl_parent_group_id :selected').each(function(i, selected) {
            	selectednumbers += $(selected).val()+"||";
        	});
		
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/forum/add_forum_topic",
			data: {
				tbl_parent_forum_id: "<?=$parent_forum_id_enc?>",
				forum_topic: $('#forum_topic').val(),
				tbl_parent_group_id: selectednumbers,
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
				refresh_page = "Y";
				    my_alert("Topic is added successfully.", 'green');
				    $("#pre-loader").hide();
				}else{
					refresh_page = "N";
					my_alert("Topic is added failed, Please try again.", 'red');
					$("#pre-loader").hide();
				}
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_update_topic() {
			
		var selectednumbers='';
        	$('#tbl_parent_group_id :selected').each(function(i, selected) {
            	selectednumbers += $(selected).val()+"||";
        	});
	
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/forum/update_forum_topic",
			data: {
				tbl_parent_forum_id: $('#parent_forum_id_enc').val(),  
			    forum_topic: $('#forum_topic').val(),
				tbl_parent_group_id: selectednumbers,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Changes saved successfully.", 'green');
				
				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
</script>
<script language="javascript">

	function ajax_validate() {
		if (validate_topic() == false || validate_parents() == false ) 
		{
			return false;
		}
		else{
			is_exist();
			
		}
	}
	
    //validate topic
	
	function ajax_validate_edit() {
		if (validate_topic() == false  || validate_parents() == false) 
		{
			return false;
		} 
		else{
			is_exist_edit();
		}
	} 
	
  /************************************* START MESSAGE VALIDATION *******************************/

   function validate_topic() {
		var regExp = / /g;
		var str = $.trim($("#forum_topic").val());
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Topic is blank. Please enter topic")
			$("#forum_topic").val('');
			$("#forum_topic").focus();
		return false;
		}
		return true;
	
	}
	
	function validate_parents() {
		var selectednumbers='';
        	$('#tbl_parent_group_id :selected').each(function(i, selected) {
            	selectednumbers += $(selected).val()+"&";
        	});
	
		if (selectednumbers=='') {
				my_alert("Please select Parent Group(s)");
			return false;
			}	
	  return true;
	}

</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Forum <small>For Parents</small></h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Parents Forum</li>
    </ol>
    <!--/BREADCRUMB--> 
    <div style=" float:right; padding-right:10px;"> <button onclick="show_group()" title="Parents Group" type="button" class="btn btn-primary">Parents Group</button></div> 
    <div style="clear:both"></div>
  </section>
  
   <script>
   function show_group()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/parents_group/";
		window.location.href = url;
	}
 </script>
  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {
			$tbl_sel_parent_forum_id = $topic_obj['tbl_parent_forum_id'];
			$forum_topic             = $topic_obj['forum_topic'];
			$tbl_parent_group_id     = $topic_obj['tbl_parent_group_id'];
			$group_id_array          = explode("||",$tbl_parent_group_id);
	
	?>
        <!--Edit-->
        
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Topic</h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/forum/all_topics"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_edit" id="frm_listing" class="form-horizontal" method="post">
                    <div class="box-body">
                   
                      <div class="form-group">
                         <label class="col-sm-2 control-label" for="forum_topic">Topic </label>
                         <div class="col-sm-10">
                             <textarea tabindex="1" dir="ltr" id="forum_topic" placeholder="Topic" name="forum_topic" class="form-control" ><?=$forum_topic?></textarea>
                         </div>
                     </div>
                   
                    
                      <div class="form-group">
                          <label class="col-sm-2 control-label" for="tbl_parent_group_id">Parents Group</label>
        
                          <div class="col-sm-10">
	                        	<select id="tbl_parent_group_id" name="tbl_parent_group_id"  tabindex="2"  class="form-control" multiple size="10" style="padding:0px 5px 0px 2px; font-size:14px;" >
                                     <optgroup label="--Select Group--">
	                                    <?php for($m=0;$m<count($rs_parents_groups);$m++){
                                          if(in_array($rs_parents_groups[$m]['tbl_parent_group_id'], $group_id_array, true))
                                          {
                                              $selClass = "selected";
                                          }
                                          else if($tbl_parent_group_id==$rs_parents_groups[$m]['tbl_parent_group_id'])
                                          {
                                             $selClass = "selected"; 
                                              
                                          }else{
                                                   $selClass = "";
                                         }
                                          
                                        ?>
                                    <option value="<?php echo $rs_parents_groups[$m]['tbl_parent_group_id'];?>"  <?= $selClass?> ><?php echo $rs_parents_groups[$m]['group_name_en']; ?> - <?php echo $rs_parents_groups[$m]['group_name_ar']; ?></option><?php } ?>
                                </optgroup>
                                </select>
                          </div>
                     </div>
                    </div>
                    
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate_edit()">Save Changes</button>
                    <input type="hidden" name="parent_forum_id_enc" id="parent_forum_id_enc" value="<?=$tbl_sel_parent_forum_id?>" />
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
		        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/forum/all_topics";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
    
  
 <link href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" rel="stylesheet">
 <script src="http://code.jquery.com/jquery-1.11.1.js"></script>
 <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
  <script>
  $( function() {
		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
				
				/* var order = $("#tabledivbody").sortable("serialize");
				
				alert(order);
				
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/your/url/here'
				});*/
				
				
				
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	

				
				
				
				
				
			}
	  } );
  
  } );
  
  
  
  </script> 
    
                     <div id="mid1" class="box box-success">
                        <div class="box-header">
                          <div class="col-sm-1" >
                          <h3 class="box-title">SEARCH</h3>
                          </div>
                          <div class="col-sm-11"> 
                             
                               <div class="col-sm-6"><input name="q" id="q" value="<?=urldecode($q)?>" type="text" class="form-control" placeholder="Search By Topic "   > </div>
                               <div class="col-sm-2"><button class="btn btn-success" type="button" onclick="search_data()">Search</button>&nbsp;<button class="btn btn-success" type="button" 
                               onclick="reset_data();">Reset</button>
                               </div>
                           
                          </div>
                        </div>  
                     </div>     
    
            <!--Listing-->
                    <div id="mid1_list" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Forum Topics</h3>
                          <div class="box-tools">
                            <?php if (count($rs_all_topics)>0) { echo $paging_string;}?>	
                            <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
                            <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>
                          </div>
                        </div>
                        
                        <div class="box-body">
                     <!--   <div style="color:#030; font-weight:bold;">You can sort categories by using drag and drop of rows </div>-->
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle"><input id="select_all" type="checkbox" value="" /></th>
                              <!--<th width="10%" align="center" valign="middle">Sl No.</th>-->
                              <th width="40%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y">Topic 
	                              <?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?>
                                  <div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>
                              <th width="6%" align="center" valign="middle">Comments</th>
                              <th width="25%" align="center" valign="middle">Assigned Parent Groups</th>
                              <th width="10%" align="center" valign="middle">Added Date</th>
                              <th width="7%" align="center" valign="middle">Status</th>
                              <th width="7%" align="center" valign="middle">Action</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                                for ($i=0; $i<count($rs_all_topics); $i++) { 
                                    $id                        = $rs_all_topics[$i]['id'];
                                    $tbl_parent_forum_id       = $rs_all_topics[$i]['tbl_parent_forum_id'];
                                    $forum_topic               = $rs_all_topics[$i]['forum_topic'];
									$posted_by                 = $rs_all_topics[$i]['posted_by'];
                                    $added_date                = $rs_all_topics[$i]['added_date'];
                                    $is_active                 = $rs_all_topics[$i]['is_status'];
									$group_parents             = $rs_all_topics[$i]['group_parents'];
									$cntComments               = $rs_all_topics[$i]['cntComments'];
                                    
                                    $forum_topic               = ucfirst($forum_topic);
                                    $added_date                = date('m-d-Y',strtotime($added_date));
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_parent_forum_id?>" >
                              <td align="left" valign="middle">
                              <span style="float:left;">
                              <input id="parent_forum_id_enc" name="parent_forum_id_enc" class="checkbox" type="checkbox" value="<?=$tbl_parent_forum_id?>" />
                              </span>
                              
                             <?php /*?> <span style="float:left;">&nbsp;
                              <?php if($i<>0){ ?> <i class="fa fa-arrow-up"  style="color:#3c8dbc; cursor:pointer;"  aria-hidden="true" title="Sorting - Drag & Drop To Up"></i> &nbsp; <?php } ?>
                               <?php if($i<> count($rs_all_categories)-1){ ?> <i class="fa fa-arrow-down" style="color:#3c8dbc;cursor:pointer;" aria-hidden="true" title="Sorting - Drag & Drop To Down"></i> <?php } ?>
                              </span><?php */?>
                              </td>
                             <!-- <td align="left" valign="middle"><?=$offset+$i+1?></td>-->
                              <td align="left" valign="middle"><a onclick="show_comments('<?=$tbl_parent_forum_id?>');" style="text-decoration:underline; cursor:pointer;"><?=$forum_topic?></a></td>
                              <td align="left" valign="middle">
                             <?php if($cntComments<>"0") { ?>
                                  <a onclick="show_comments('<?=$tbl_parent_forum_id?>');" title="Comments" style="text-decoration:underline;cursor:pointer;"><?=$cntComments?></a>
                             <?php }else{ ?>
                                  <?=$cntComments?>
                              <?php } ?>
                              </td>
                              <td align="left" valign="middle"><?=$group_parents?></td>
                              <td align="left" valign="middle"><?=$added_date?></td> 
                              <td align="left" valign="middle">
                                <div id="act_deact_<?=$tbl_parent_forum_id?>">
                                <?php if (trim($is_active) == "Y") { ?>
                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_parent_forum_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                <?php } else { ?>
                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_parent_forum_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                <?php } ?>
                                </div>
                              </td>
                              <td align="left" valign="middle">
                                <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/forum/edit_forum_topic/parent_forum_id_enc/<?=$tbl_parent_forum_id?>"><button class="btn bg-purple fa fa-pencil" type="button" title="Edit"></button></a>
                              </td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="9" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_all_topics)<=0) {
                            ?>
                            <tr>
                              <td colspan="9" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no topics available. Click on the + button to create one.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
    
			  <script>
			    function show_comments(tbl_parent_forum_id)
				{
					var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/forum/all_topic_comments/tbl_parent_forum_id/"+tbl_parent_forum_id+"/";
					window.location.href = url;
				}
			 </script>
            <!--Add or Create-->
              <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Topic</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
               
                  
                    <div class="form-group">
                         <label class="col-sm-2 control-label" for="forum_topic">Topic </label>
                         <div class="col-sm-10">
                             <textarea tabindex="1" dir="ltr" id="forum_topic" placeholder="Topic" name="forum_topic" class="form-control" ></textarea>
                         </div>
                     </div>
                   
                    
                      <div class="form-group">
                          <label class="col-sm-2 control-label" for="tbl_parent_group_id">Parents Group</label>
        
                          <div class="col-sm-10">
	                        	<select id="tbl_parent_group_id" name="tbl_parent_group_id"  tabindex="2"  class="form-control" multiple size="10" style="padding:0px 5px 0px 2px; font-size:14px;" >
                                <optgroup label="--Select Group--">
									<?php for($m=0;$m<count($rs_parents_groups);$m++){?>
                                    	<option value="<?php echo $rs_parents_groups[$m]['tbl_parent_group_id']; ?>" ><?php echo $rs_parents_groups[$m]['group_name_en']; ?> - <?php echo $rs_parents_groups[$m]['group_name_ar']; ?></option>
									<?php } ?>
                                </optgroup>
                                </select>
                          </div>
                     </div>
                   
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate()">Submit</button>
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
            <!--/Add or Create-->
                
        <!--/Admin Category Management-->
        

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>

        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >
function search_data() {
		var q = $("#q").val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/forum/all_topics/";
		
		if(q !='')
			url += "q/"+q+"/";
		
			url += "offset/0/";
		window.location.href = url;
	}

function reset_data() {
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/forum/all_topics/";
		url += "offset/0/";
		window.location.href = url;
	}
</script>