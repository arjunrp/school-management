<?php ob_start(); ?>
<!DOCTYPE html>
<html>
<!--<html>
<link rel="stylesheet" href="<?=ADMIN_CSS_PATH?>print_id_card.css" type="text/css">
<link rel="stylesheet" href="<?=ADMIN_CSS_PATH?>id_card_style.css" type="text/css">-->
<?php
//header('Content-Type: text/html; charset=utf-8');
for ($i=0; $i<count($rs_all_students); $i++) { 

		$id               = $rs_all_students[$i]["id"];
		$tbl_student_id   = $rs_all_students[$i]["tbl_student_id"];
		$first_name       = $rs_all_students[$i]["first_name"];
		$last_name        = $rs_all_students[$i]["last_name"];
		$first_name_ar    = $rs_all_students[$i]["first_name_ar"];
		$last_name_ar     = $rs_all_students[$i]["last_name_ar"];
		$mobile           = $rs_all_students[$i]["mobile"];
		$dob_month        = $rs_all_students[$i]["dob_month"];
		$dob_day          = $rs_all_students[$i]["dob_day"];
		$dob_year         = $rs_all_students[$i]["dob_year"];
		$gender           = $rs_all_students[$i]["gender"];
		$country          = $rs_all_students[$i]["country"];
		$pic              = $rs_all_students[$i]['file_name_updated'];
		$parent_name_en     = ucfirst($rs_all_students[$i]['parent_first_name'])." ".ucfirst($rs_all_students[$i]['parent_last_name']);
		$parent_name_ar     = $rs_all_students[$i]['parent_first_name_ar']." ".$rs_all_students[$i]['parent_last_name_ar'];
		
		if($pic<>"") // class="img-circle"
			$pic_path           =   '<img width="100" height="80" src="'.IMG_PATH_STUDENT.'/'.$pic.'"  />';
		else
			$pic_path           =   '<img width="100" height="80" src="'.IMG_PATH_STUDENT.'/no_img.png"  
										style="border-color:1px solid #7C858C !important;" />';

		if($gender =="male"){
			$gender  ="Male";
			$gender_ar ="ذكر";
		}else{
			$gender  ="Female";
			$gender_ar ="أنثى";
		}

		$email                  = isset($rs_all_students[$i]["email"])? $rs_all_students[$i]["email"] :"-";
		$file_name_updated      = $rs_all_students[$i]["file_name_updated"];
		$emirates_id_father     = $rs_all_students[$i]["emirates_id_father"];

		if($rs_all_students[$i]["emirates_id_mother"]<>""){ 
		 	$emirates_id_mother = $rs_all_students[$i]["emirates_id_mother"]; 
		}else{ 
			$emirates_id_mother = "Not mentioned"; 
		} 

		$class_name             = $rs_all_students[$i]["class_name"];
		$class_name_ar          = $rs_all_students[$i]["class_name_ar"];
		$section_name           = $rs_all_students[$i]["section_name"];
		$section_name_ar        = $rs_all_students[$i]["section_name_ar"];
		$user_id                = $rs_all_students[$i]["user_id"];
		$password = base64_decode($rs_all_students[$i]["pass_code"]);
		?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" id="printarea" >
<style>
  html, body {
    padding:0;
    margin:0;
}
  body{
		font-family:Arial, Helvetica, sans-serif;
		font-size:11px;
		color:#000;		}
	table{
		border:none;
		}
	table td{
		font-size:15px;
		color:#000;	
		line-height:25px;
		}
</style>

<!-- Save for Web Slices (Untitled-1 - Slices: 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, 14, 15, 16) -->
<table id="Table_01" width="2481" height="1151" border="0" cellpadding="0" cellspacing="0" dir="ltr" style="margin-bottom:50px; ">
  <tr>
    <td colspan="2" rowspan="4" width="896" height="308"></td>
    <td colspan="3" width="575" height="37"></td>
    <td rowspan="4"  width="342" height="308"></td>
    <td colspan="4" rowspan="2" width="667" height="90"></td>
    <td width="1" height="37"></td>
  </tr>
  <tr>
    <td colspan="3" rowspan="3"><img src="<?=HOST_URL?>/assets/admin/images/home_05.png" width="575" height="271" alt=""></td>
    <td width="1" height="53"></td>
  </tr>
  <tr>
    <td colspan="4"  height="112" ></td>
    <td  width="1" height="112"></td>
  </tr>
  <tr>
    <td colspan="4"  width="667" height="106"></td>
    <td  width="1" height="106"></td>
  </tr>
  <tr>
    <td rowspan="5" width="219" height="842"></td>
    <td colspan="2" rowspan="2" width="933" height="248">
     <table width="300" border="0"  cellpadding="0" cellspacing="0">
        <tr>
          <td style="font-size:45px; line-height:70px;padding:5px; height:200px; width:300px"><?php /*?><strong> User Id</strong>:<?=$user_id?><br>
            <strong>Password</strong>:<?=$password?><?php */?><img src="<?=HOST_URL?>/assets/admin/images/home_17.jpg" width="300" height="200" alt=""></td>
        </tr>
      </table>
    
    </td>
    <td rowspan="4" width="162" height="670"></td>
    <td colspan="5" width="1061" height="92"></td>
    <td rowspan="4" width="105" height="670"></td>
    <td width="1" height="92"></td>
  </tr>
  <tr>
    <td colspan="3" rowspan="2" width="532" height="241" ></td>
    <td colspan="2" rowspan="2" width="529" height="241">
   <?php /*?> <table width="300" border="0"  cellpadding="0" cellspacing="0" style="margin-left:-350px; margin-top:0px; border:1px solid #000;" >
        <tr>
          <td style="font-size:45px; line-height:70px;padding:5px; height:100px; width:300px"><strong> User Id</strong>:<?=$user_id?><br>
            <strong>Password</strong>:<?=$password?></td>
        </tr>
      </table><?php */?>
    
    </td>
    <td width="1" height="156"></td>
  </tr>
  <tr>
    <td  width="933" height="594" colspan="2" rowspan="3" align="left" valign="top"><table width="1000" cellpadding="0" cellspacing="0" style="margin-top:150px;">
        <tr>
          <td width="1000" height="50" align="right" valign="top" style="font-size:45px; text-align:right" dir="rtl"><strong>اسم</strong> : <?=$first_name_ar?>&nbsp;<?=$last_name_ar?></td>
        </tr>
        <tr>
          <td width="1000" height="50" style="font-size:45px;" ><strong>Name :</strong> <?=$first_name?>&nbsp;<?=$last_name?></td>
        </tr>
        
         <tr>
          <td width="1000" height="50" valign="top" style="font-size:45px;">
          <table width="1000" cellpadding="0" cellspacing="0">
          <tr>
          <td dir="ltr" width="500" height="50" align="left" style="font-size:45px; text-align:left"  ><strong>Class :</strong><?=$class_name?> <?=$section_name?></td>
          <td dir="rtl" width="500" height="50"   align="right" style="font-size:45px; text-align:right" ><strong>الصف</strong> :<?=$class_name_ar?> <?=$section_name_ar?></td>
          </tr>
          </table>
          </td>
        </tr>
       
        
        <tr>
          <td width="1000" height="50" style="font-size:45px;" ><strong>Nationality </strong> : <?=$country?></td>
        </tr>
        <tr>
          <td width="1000" height="50" dir="rtl" style="font-size:45px; text-align:right"><strong>اسم ولي الأمر</strong> :  <?=$parent_name_ar?></td>
        </tr>
        <tr>
          <td width="1000" height="50" style="font-size:45px;" ><strong>Guardian Name:</strong> <?=$parent_name_en?> </td>
        </tr>
      </table></td>
    <td width="1" height="85"></td>
  </tr>
  <tr>
    <td colspan="4" width="572" height="337"></td>
    <td><!--<img src="<?=HOST_URL?>/assets/admin/images/home_17.jpg" width="489" height="337" alt="">--></td>
    <td width="1" height="337"></td>
  </tr>
  <tr>
    <td colspan="5" width="734" height="172"></td>
    <td><img src="<?=HOST_URL?>/assets/admin/images/home_19.png" width="489" height="172" alt=""></td>
    <td width="105" height="172"></td>
    <td width="1" height="172"></td>
  </tr>
  <tr>
    <td width="219" height="1"></td>
    <td width="677" height="1"></td>
    <td width="256" height="1"></td>
    <td width="162" height="1"></td>
    <td width="157" height="1"></td>
    <td width="342" height="1"></td>
    <td width="33" height="1"></td>
    <td width="40" height="1"></td>
    <td width="489" height="1"></td>
    <td width="105" height="1"></td>
    <td></td>
  </tr>
</table>

<!-- End Save for Web Slices -->
<?php } ?>
</body>
</html>
<?php
$output = ob_get_clean( );
$output = htmlentities($output);
?>
<form method="post" name="frmsubmit" id="frmsubmit" action="<?=HOST_URL?>/id_cards.php">
  <input type="hidden" name="html" value="<?=$output?>"/>
</form>
<script>
  document.frmsubmit.submit();
</script>