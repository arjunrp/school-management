    <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Select Session</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form role="form">
      <div class="box-body">
        <div class="form-group">
          <label for="exampleInputEmail1">Class Sessions: </label>
          <select name="tbl_class_sessions_id" id="tbl_class_sessions_id">
          	<option value=""></option>
          </select>
        </div>
      </div>
      <!-- /.box-body -->
    </form>
    </div>
    
	<?php
    $CI =& get_instance();
    $CI->load->model("model_teacher_attendance");				
    ?>
    	<div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Select Student</h3>
        </div>
        <table width="100%" class="table table-bordered table-striped">
          <thead>
            <tr>
                <th width="20%" align="center" valign="middle">Picture</th>
                <th width="" align="center" valign="middle">Student</th>
                <th width="" align="center" valign="middle">Status</th>
            </tr> 
          </thead>
          <tbody>
    <?
    $all_students_arr = array();
    for ($i=0; $i<count($data_rs); $i++) {
        $tbl_student_id = $data_rs[$i]['tbl_student_id'];
        $first_name = $data_rs[$i]['first_name'];
        $last_name = $data_rs[$i]['last_name'];
    
        $stu_obj = $CI->model_student->get_student_obj($tbl_student_id);
        $image_student = $stu_obj[0]['file_name_updated'];
    
        if (trim($lan) == "ar") {
            $first_name = $data_rs[$i]['first_name_ar'];
            $last_name = $data_rs[$i]['last_name_ar'];
        }
        
        $name = $first_name. " ".$last_name;
        $is_present = $CI->model_teacher_attendance->get_attendance_t($tbl_student_id, $tbl_teacher_id, $tbl_class_id, $attendance_date,  $att_option);
        $dataopt = $is_present;
        if (trim($dataopt) == "" || trim($dataopt) == "null") {
            $dataopt = "Y";
        }
        $student_arr = array();
        $student_arr['student_id'] = $tbl_student_id;
        $student_arr['title'] = $name;
        $student_arr['val'] = $dataopt;
        //$student_arr['url'] = IMG_PATH_STUDENT."/".$image_student;
        if($image_student=="")
        {
            $student_arr["url"] = IMG_PATH_STUDENT."/avatar_default.png";
        }else{
            $student_arr["url"] = IMG_PATH_STUDENT."/".$image_student;
        }
        $all_students_arr[$i] = $student_arr;
	
		/*$checked = "";
		if ($dataopt == "Y") {
			$checked = 'checked="checked"';
		}*/
			
	?>	
        <tr>
            <td align="center" valign="middle"><img src="<?=$student_arr["url"]?>" height="100" /></td>
            <td align="center" valign="middle"><?=$first_name?> <?=$last_name?></td>
            <td align="center" valign="middle">
            <select name="option" id="option<?=$i?>">
				<option value="Y_<?=$tbl_student_id?>" <?php if ($dataopt == "Y") { ?> selected="selected" <?php } ?> >Present</option>
				<option value="N_<?=$tbl_student_id?>" <?php if ($dataopt == "N") { ?> selected="selected" <?php } ?> >Absent</option>
		        <option value="LC_<?=$tbl_student_id?>" <?php if ($dataopt == "LC") { ?> selected="selected" <?php } ?> >Late Coming</option>
                <option value="EG_<?=$tbl_student_id?>" <?php if ($dataopt == "EG") { ?> selected="selected" <?php } ?> >Early Going</option>
	      </select>
            
            <!--<input type="checkbox" name="option" id="option<?=$i?>" data-studentid="<?=$tbl_student_id?>" <?=$checked?> />--></td>
        </tr>
	<?php			
	}
	?>	
		</tbody>
          <tfoot>
          </tfoot>
        </table>
        <input type="hidden" name="total_count" value="<?=count($data_rs)?>" id="total_count" />
	<?php		
       /* $arr_main = array();
        $arr_main["date"] = $attendance_date;
        $arr_main["is_attendance_exists"] = $is_attendance_exists;
        $arr_main["students"] = $all_students_arr;
        echo json_encode($arr_main);
		 */
    ?>
