<?php
//Init Parameters
$student_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}
?>
 
<style>
.txt_en {
	text-align:left;
	padding-left:2px;
}
.txt_ar {
	text-align:right;
	padding-right:2px;	
	direction:rtl;		
}
</style>
<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_view_form(card_type) {
		$('#mid1_list').hide();
		$('#mid2').show(500);
		
		
		var tbl_academic_year = $("#tbl_academic_year").val();
		var tbl_semester_id   = $("#tbl_semester_id").val();
		var tbl_class_id      = $("#tbl_class_id").val();
		var gender            = $("#gender").val();
		var tbl_country_id    = $("#tbl_country_id").val();
		var tbl_student_id    = $("#tbl_student_id").val();
		var tbl_teacher_id    = $("#tbl_teacher_id").val();
		
		
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/point_reports_list/";
		if(tbl_academic_year !='')
			url += "tbl_academic_year/"+tbl_academic_year+"/";
		if(tbl_semester_id !='')
			url += "tbl_semester_id/"+tbl_semester_id+"/";
		if(tbl_class_id !='')
			url += "tbl_class_id/"+tbl_class_id+"/";
		if(gender !='')
			url += "gender/"+gender+"/";
		if(tbl_country_id !='')
			url += "tbl_country_id/"+tbl_country_id+"/";
		if(tbl_student_id !='')
			url += "tbl_student_id/"+tbl_student_id+"/";
		if(tbl_teacher_id !='')
			url += "tbl_teacher_id/"+tbl_teacher_id+"/";	
		if(card_type !='')
			url += "card_type/"+card_type+"/";
		
			url += "offset/0/";
			
			
			
		var xmlHttp, rnd, url, search_param, ajax_timer;
		rnd = Math.floor(Math.random()*11);
		try{		
			xmlHttp = new XMLHttpRequest(); 
		}catch(e) {
			try{
				xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
			}catch(e) {
				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
				hide_loading();
			}
		}

		//AJAX response
		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
				alert(data);
				//ajax_timer.stop();
				var data = xmlHttp.responseText;
				$("#mid2").html(data);
				//$("#tbl_student_dropdown").multiselect('refresh');
				return;
			}
		}
		/*ajax_timer = $.timer(function() {
			xmlHttp.abort();
			alert(connectivity_msg);
			ajax_timer.stop();
		},connectivity_timeout_time,true);*/

		//Sending AJAX request
		xmlHttp.open("POST",url,true);
		xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlHttp.send("rnd="+rnd);	
			
			
			
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
	
	function show_listing() {
		$('#mid2').hide();
		$('#mid1_list').show(500);
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/school_point_reports";
			}
		})
	});
	
</script>
	
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Students Points Reports</h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Students</li>
    Points    
    </ol>
    <!--/BREADCRUMB--> 
     <div style=" float:right;"> <button onclick="show_student_cards()" title="Student Cards" type="button" class="btn btn-primary">Student Cards</button></div>
    <div style=" float:right; padding-right:10px;"> <button onclick="show_student_attendance()" title="Student Attendance" type="button" class="btn btn-primary">Student Attendance</button></div>
    <div style="clear:both"></div>
  </section>
      <link href="<?=HOST_URL?>/assets/admin/dist/css/jquery-ui.css" rel="stylesheet">
      <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-1.11.1.js"></script>
      <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-ui.js"></script>
      <link href="<?=HOST_URL?>/assets/admin/dist/css/uploadfile.min.css" rel="stylesheet">

 <script>
   function show_student_cards()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/school_reports/";
		window.location.href = url;
	}
	 function show_student_attendance()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/school_attendance_reports/";
		window.location.href = url;
	}
 </script>     
 <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {
			
	
	?>
        <!--Edit-->
   <script>
 $( function() {
    $( "#tabs" ).tabs();
  } );
  
 
</script>

        
              
		        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/student/all_students";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
    
  
 <link href="<?=HOST_URL?>/assets/admin/dist/css/jquery-ui.css" rel="stylesheet">
 <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-1.11.1.js"></script>
 <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-ui.js"></script>
  <script>
  $( function() {
		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
				
				/* var order = $("#tabledivbody").sortable("serialize");
				
				alert(order);
				
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/your/url/here'
				});*/
				
				
				
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	
				
				
				
				
				
			}
	  } );
  
  } );
  </script> 
  
  
  
  <!--File Upload START-->
<link href="<?=HOST_URL?>/assets/admin/dist/css/uploadfile.min.css" rel="stylesheet">
<script>
 $( function() {
    $( "#tabs" ).tabs();
  } );
  
 
</script>
<style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url('<?=IMG_PATH?>/delete.jpg');
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
		clear:both;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
	#overlay_container {
		position:relative;
	}
	#overloading {
		background-image:url('<?=IMG_PATH?>/preloader/preloader_2.gif');
		background-repeat:no-repeat;
		background-position:center;
		background-color:#CCC;
		position:absolute;
		left:0px;
		top:0px;
		opacity: 0.3;
		z-index: 10000;
	}
	#div_listing_container {
		display:none;	
	}
	.d_d_text {
		color:#745156;
		font-size:20px;
			
	}
	.ajax-upload-dragdrop {
		margin:auto;
		margin-bottom:10px;
		width:700px !important;
	}
	.ajax-file-upload-statusbar {
		margin:auto;
		margin-top:10px;
	}
	.ajax-file-upload {
		height:31px;
	}
	
	
	 #tabs-1{  
	    overflow-y:scroll; overflow-x:none;
	}

    #tabs-2{
		overflow-y:scroll; overflow-x:none;
	}
				  
  .ui-tabs-active{
		border-color:#efca86  !important;
   }
					 
	.ui-tabs .ui-tabs-nav li {
		float:left;
		font-size: 16px;
        font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif;
  }
  label{
	  display: inline-block;
      font-weight: 700;
  }
  
  .ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button {
    font-family:"Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
    font-size: 14px;
}
  
  .ui-widget{
	 font-size: 16px;
     font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
  }
  .form-control{
	 font-size: 14px; 
  }
</style>
                  
    
                    <div id="mid1" class="box box-success">
                        <div class="box-header">
                          <div class="col-sm-11"> 
                          <div class="col-sm-1" >
                          <h3 class="box-title">SEARCH</h3>
                          </div>
                          </div>
                          <div class="col-sm-11"> &nbsp;</div>
                          <div class="col-sm-11"> 
                               <div class="col-sm-3"> 
                             
                              <select name="tbl_academic_year" id="tbl_academic_year" class="form-control" onChange="get_semester_ajax()" >
                              <option value="">--Select Academic Year --</option>
							  
							  <?php
                                    for ($u=0; $u<count($academic_list); $u++) { 
                                        $tbl_academic_year_id  = $academic_list[$u]['tbl_academic_year_id'];
                                        $academic_start        = $academic_list[$u]['academic_start'];
                                        $academic_end          = $academic_list[$u]['academic_end'];
                                        if($tbl_sel_academic_year == $tbl_academic_year_id)
                                           $selAcademic = "selected";
                                         else
                                           $selAcademic = "";
                                  ?>
                                      <option value="<?=$tbl_academic_year_id?>"  <?=$selAcademic?> >
                                      <?=$academic_start?>&nbsp;-&nbsp;<?=$academic_end?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>   
                               </div>
                               
                                <div class="col-sm-3" id="divSemester" > 
                             
                              <select name="tbl_semester_id" id="tbl_semester_id" class="form-control">
                              <option value="">--Select Semester--</option>
							  
							  <?php
                                    for ($u=0; $u<count($rs_all_semesters); $u++) { 
                                        $tbl_semester_id_u = $rs_all_semesters[$u]['tbl_semester_id'];
                                        $title             = $rs_all_semesters[$u]['title'];
                                        $title_ar          = $rs_all_semesters[$u]['title_ar'];
										$duration          = $rs_all_semesters[$u]['start_date']." - ".$rs_all_semesters[$u]['end_date'];
										
                                        if($tbl_sel_semester_id == $tbl_semester_id_u)
                                           $selSemester = "selected";
                                         else
                                           $selSemester = "";
                                  ?>
                                      <option value="<?=$tbl_semester_id_u?>"  <?=$selSemester?>  >
                                     <?=$title_ar?>&nbsp;[::]&nbsp; <?=$title?>&nbsp; / &nbsp;<?=$duration?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>     
                              </div>
                              <div class="col-sm-3"> 
                             
                              <select name="tbl_class_id" id="tbl_class_id" class="form-control" onChange="get_students_ajax()" >
                              <option value="">--Select Class --</option>
							  
							  <?php
                                    for ($u=0; $u<count($classes_list); $u++) { 
                                        $tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
                                        $class_name             = $classes_list[$u]['class_name'];
                                        $class_name_ar          = $classes_list[$u]['class_name_ar'];
										$section_name           = $classes_list[$u]['section_name'];
                                        $section_name_ar        = $classes_list[$u]['section_name_ar'];
                                        if($tbl_sel_class_id == $tbl_class_id_u)
                                           $selClass = "selected";
                                         else
                                           $selClass = "";
                                  ?>
                                      <option value="<?=$tbl_class_id_u?>"  <?=$selClass?>  >
                                      <?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;
                                    <?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>   
                               </div>
                               
                               
                                 <div class="col-sm-3">
                                     <label>Gender&nbsp;&nbsp;</label>
                                    <label>
                                      <input id="gender" name="gender" value="male" class="minimal"  type="radio"  <?php if($tbl_sel_gender=="male"){?> checked="checked" <?php } ?> >
                                      Male
                                    </label>
                                    &nbsp;
                                    <label>
                                      <input id="gender" name="gender" value="female" class="minimal" type="radio"  <?php if($tbl_sel_gender=="female"){?> checked="checked" <?php } ?> >
                                      Female
                                    </label>
                                     &nbsp;
                                    <label>
                                      <input id="gender" name="gender" value="" class="minimal" type="radio" <?php if($tbl_sel_gender<>"female" && $tbl_sel_gender<>"male"  ){?> checked="checked" <?php } ?> >
                                      All
                                    </label>
                                </div>
                               
                               
                               
                               
                               
                               
                               </div>
                               
                               <div class="col-sm-11">&nbsp;</div>
                               
                                <div class="col-sm-11"> 
                                
                              <div class="col-sm-3"> 
                             
                              <select name="tbl_country_id" id="tbl_country_id" class="form-control">
                              <option value="">--Select Nationality --</option>
							  
							  <?php
                                    for ($u=0; $u<count($countries_list); $u++) { 
                                        $tbl_country_id_u       = $countries_list[$u]['country_id'];
                                        $country_name           = $countries_list[$u]['country_name'];
                                        if($tbl_sel_country == $tbl_country_id_u)
                                           $selCountry = "selected";
                                         else
                                           $selCountry = "";
                                  ?>
                                      <option value="<?=$tbl_country_id_u?>"  <?=$selCountry?>  >
                                      <?=$country_name?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>   
                             </div>
                                
                                
                                
                                 <div class="col-sm-3" id="divStudent" > 
                             
                              <select name="tbl_student_id" id="tbl_student_id" class="form-control">
                                <option value="">--Select Student--</option>
                                  <?php
                                    for ($u=0; $u<count($rs_all_students); $u++) { 
                                        $tbl_student_id_u         = $rs_all_students[$u]['tbl_student_id'];
                                        $name                     = $rs_all_students[$u]['first_name']." ".$rs_all_students[$u]['last_name'];
										$name_ar                    = $rs_all_students[$u]['first_name_ar']." ".$rs_all_students[$u]['last_name_ar'];
                                        if($tbl_sel_student_id == $tbl_student_id_u)
                                           $selStudent = "selected";
                                         else
                                           $selStudent = "";
                                  ?>
                                      <option value="<?=$tbl_student_id_u?>"  <?=$selStudent?>  >
                                      <?=$name?>[::]<?=$name_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                                
                               </select>     
                              </div>  
                                
                                
                          
                               <div class="col-sm-3"> 
                             
                              <select name="tbl_teacher_id" id="tbl_teacher_id" class="form-control">
                              <option value="">--Select Teacher --</option>
							  
							  <?php
                                    for ($u=0; $u<count($rs_all_teachers); $u++) { 
                                        $tbl_teacher_id_u        = $rs_all_teachers[$u]['tbl_teacher_id'];
                                        $name                  = $rs_all_teachers[$u]['first_name']." ".$rs_all_teachers[$u]['last_name'];
                                        $name_ar                  = $rs_all_teachers[$u]['first_name_ar']." ".$rs_all_teachers[$u]['last_name_ar'];
                                        if($tbl_sel_teacher_id == $tbl_teacher_id_u)
                                           $selTeacher = "selected";
                                         else
                                           $selTeacher = "";
                                  ?>
                                      <option value="<?=$tbl_teacher_id_u?>"  <?=$selTeacher?> >
                                      <?=$name?>&nbsp;[::]&nbsp;<?=$name_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>   
                               </div>
                               
                               <div class="col-sm-3"><button class="btn btn-success" type="button" onclick="search_data()">Search</button>&nbsp;<button class="btn btn-success" type="button" 
                               onclick="reset_data();">Reset</button>
                               </div>
                               </div>
                           
                          </div>
                        </div>  
                          
    
            <!--Listing-->
                    <div id="mid1_list" class="box">
                       <div class="box-header">
                          <h3 class="box-title">Behaviour Points History</h3>
                         <div class="box-tools">
								<?php  if (count($rs_all_point_details)>0) { echo $paging_string;} ?>&nbsp;
                                <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/school_point_reports"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a> 
								
                         </div>
                        </div> 
                        
                        <div class="box-body">
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
							  <th width="20%" align="center" valign="middle">Student Name</th>
							  <th width="20%" align="center" valign="middle">Class Name</th> 
							  <th width="20%" align="center" valign="middle">Behaviour</th>
							  <th width="5%" align="center" valign="middle">Point</th>
                              <th width="20%" align="center" valign="middle">Teacher Name</th>
							  <th width="10%" align="center" valign="middle">Date</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php    for ($i=0; $i<count($rs_all_point_details); $i++) { 
                                    $student_name_en   = $rs_all_point_details[$i]['first_name']." ".$rs_all_point_details[$i]['last_name'];
									$student_name_ar   = $rs_all_point_details[$i]['first_name_ar']." ".$rs_all_point_details[$i]['last_name_ar'];
									$class_name_en     = $rs_all_point_details[$i]['class_name']." ".$rs_all_point_details[$i]['section_name'];
									$class_name_ar     = $rs_all_point_details[$i]['class_name_ar']." ".$rs_all_point_details[$i]['section_name_ar'];
									$point_name_en     = $rs_all_point_details[$i]['point_name_en'];
                                    $point_name_ar     = $rs_all_point_details[$i]['point_name_ar'];
									$points_student    = $rs_all_point_details[$i]['points_student'];
									$teacher_name_en   = $rs_all_point_details[$i]['teacher_first_name']." ".$rs_all_point_details[$i]['teacher_last_name'];
									$teacher_name_ar   = $rs_all_point_details[$i]['teacher_first_name_ar']." ".$rs_all_point_details[$i]['teacher_last_name_ar'];
									$added_date        = $rs_all_point_details[$i]['added_date'];
									$semester_en       = $rs_all_point_details[$i]['title'];
                                    $semester_ar       = $rs_all_point_details[$i]['title_ar'];
							?>
									
                          <tr  class="sectionsid">
                              <td> <div class="txt_en"><?=$student_name_en?></div><div class="txt_ar"><?=$student_name_ar?></div></td>
							  <td> <div class="txt_en"><?=$class_name_en?></div><div class="txt_ar"><?=$class_name_ar?></div>
							       <div class="txt_en"><?=$semester_en?></div><div class="txt_ar"><?=$semester_ar?></div>
							  </td>
							  <td> <div class="txt_en"><?=$point_name_en?></div><div class="txt_ar"><?=$point_name_ar?></div></td>
							  <td><?=$points_student?></td>
							  <td> <div class="txt_en"><?=$teacher_name_en?></div><div class="txt_ar"><?=$teacher_name_ar?></div></td>
                              <td><?=$added_date?></td>
                            </tr>
                           <?php  }  ?>
                  
                             <tr>
                              <td colspan="10" align="right" valign="middle">
                           <?=$this->pagination->create_links();?>
                              </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
    
            <!--/Add or Create-->
                
        <!--/Admin Category Management-->

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>


<script src="<?=HOST_URL?>/assets/admin/dist/js/jquery.uploadfile.min.js"></script>

<script language="javascript">

var item_id = "<?=$student_id_enc?>";//Primary Key for a Form. 
if(item_id=="")
  var item_id = $("#student_id_enc").val();

function set_item_id(obj) {
	item_id = obj.value;
	get_files();	
}

$(document).ready(function() {
	var uploadObj = $("#advancedUpload").uploadFile({
		url:"<?=HOST_URL?>/file_mgmt/upload_the_file",
		multiple:true,
		autoSubmit:true,
		maxFileSize:130000,
		fileName:"myfile",
		formData: {"module_name":"student"},
		dynamicFormData: function() {
			var data = { item_id:item_id}
			return data;
		},
		showStatusAfterSuccess:false,
		dragDropStr: "<span class='d_d_text'>Optionally Drag and Drop the File to Upload.</span>",
		abortStr:"Abourt",
		cancelStr:"Cancel",
		doneStr:"Done",
		multiDragErrorStr: "Multi Drag Error.",
		extErrorStr:"Extention Error:",
		sizeErrorStr:"Max Size Error:",
		uploadErrorStr:"Upload Error",
		onSelect:function(files) {
 		},
		onSubmit:function(files) {
 		},
		onSuccess:function(files, data, xhr) {
			if (data == "error") {
				alert("Error uploading file. Please try again.");
				return;
			}
			var obj = JSON.parse(data);
			var tbl_uploads_id = obj.tbl_uploads_id;
			var file_name_updated = obj.file_name_updated;
			
			//alert("tbl_uploads_id: "+tbl_uploads_id)
			//alert("file_name_updated: "+file_name_updated)
			add_uploaded_item(tbl_uploads_id, file_name_updated);
		},
		afterUploadAll:function() {
 		},
		onError: function(files, status, errMsg) {
 		}
	});

	$("#startUpload").click(function() {
		uploadObj.startUpload();
	});
	
	try { 
		$('input[type=file]').click();
	} catch(e) {
		alert(e)
	}
});

//Function called when file is uploaded
function add_uploaded_item(tbl_uploads_id, file_name_updated) {
	var str = "<div id='"+tbl_uploads_id+"' class='box-header with-border'> <div class='box-title'><img src='<?=IMG_PATH_STUDENT?>/"+file_name_updated+"' /></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_img_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
		
	$("#div_listing_container").show();
	$("#div_listing_container").append(str);
	$(".ajax-upload-dragdrop").hide();//Hide the upload button
return;
}

function confirm_delete_img_popup(tbl_uploads_id) {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		$('#'+tbl_uploads_id).hide();	
		$(".ajax-upload-dragdrop").show();
		
		var url_str = "<?=HOST_URL?>/file_mgmt/delete_file";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					tbl_uploads_id: tbl_uploads_id
				},
			success: function(data) {
				$("#pre-loader").hide();
			}
		});	
	} else {
		$("#pre-loader").hide();		
	}
}

function get_files() {
	var url_str = "<?=HOST_URL?>/misc/get_files.php";
	
	$.ajax({
		type: "POST",
		url: url_str,
		data: {
				module_name: "student",
				show_del: "Y",
				item_id: item_id//global variable
			},
		success: function(data) {
			$('#div_listing_container').show();
			$('#div_listing_container').html(data)
			
		}
	});	
}
</script>
<!--File Upload END-->
        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >
function search_data() {
		
		var tbl_academic_year = $("#tbl_academic_year").val();
		var tbl_semester_id   = $("#tbl_semester_id").val();
		var tbl_class_id      = $("#tbl_class_id").val();
		var gender            = $("#gender").val();
		var tbl_country_id    = $("#tbl_country_id").val();
		var tbl_student_id    = $("#tbl_student_id").val();
		var tbl_teacher_id    = $("#tbl_teacher_id").val();
		
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/school_point_reports/";
		if(tbl_academic_year !='')
			url += "tbl_academic_year/"+tbl_academic_year+"/";
		if(tbl_semester_id !='')
			url += "tbl_semester_id/"+tbl_semester_id+"/";
		if(tbl_class_id !='')
			url += "tbl_class_id/"+tbl_class_id+"/";
		if(gender !='')
			url += "gender/"+gender+"/";
		if(tbl_country_id !='')
			url += "tbl_country_id/"+tbl_country_id+"/";
		if(tbl_student_id !='')
			url += "tbl_student_id/"+tbl_student_id+"/";
		if(tbl_teacher_id !='')
			url += "tbl_teacher_id/"+tbl_teacher_id+"/";	
		
			url += "offset/0/";
		window.location.href = url;
		<?php /*?>window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/enquiry/all_enquiries/is_not_replied/"+is_not_replied+"/tbl_court_id/"+tbl_court_id+"/tbl_category_id/"+tbl_category_id;<?php */?>
	}

function reset_data() {
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/school_point_reports/";
		url += "offset/0/";
		window.location.href = url;
	}
	
function get_students_ajax() {
		//show_loading();
		var class_ids = $("#tbl_class_id").val();
		 var tbl_class_id='';
        	$('#tbl_class_id :selected').each(function(i, selected) {
            	tbl_class_id += $(selected).val()+",";
        	});
		//alert(tbl_class_id);
		//return;
		var xmlHttp, rnd, url, search_param, ajax_timer;
		rnd = Math.floor(Math.random()*11);
		try{		
			xmlHttp = new XMLHttpRequest(); 
		}catch(e) {
			try{
				xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
			}catch(e) {
				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
				hide_loading();
			}
		}

		//AJAX response
		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
				//ajax_timer.stop();
				var data = xmlHttp.responseText;
				$("#divStudent").html(data);
				//$("#tbl_student_dropdown").multiselect('refresh');
				return;
			}
		}

		/*ajax_timer = $.timer(function() {
			xmlHttp.abort();
			alert(connectivity_msg);
			ajax_timer.stop();
		},connectivity_timeout_time,true);*/

		//Sending AJAX request
		url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/students_against_classes/selBox/1/tbl_class_id/"+tbl_class_id+"/rnd/"+rnd;
		xmlHttp.open("POST",url,true);
		xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlHttp.send("rnd="+rnd);
	}
	
	function get_semester_ajax() {
		//show_loading();
		var tbl_academic_year = $("#tbl_academic_year").val();
		
		//alert(tbl_class_id);
		//return;
		var xmlHttp, rnd, url, search_param, ajax_timer;
		rnd = Math.floor(Math.random()*11);
		try{		
			xmlHttp = new XMLHttpRequest(); 
		}catch(e) {
			try{
				xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
			}catch(e) {
				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
				hide_loading();
			}
		}

		//AJAX response
		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
				//ajax_timer.stop();
				var data = xmlHttp.responseText;
				$("#divSemester").html(data);
				//$("#tbl_student_dropdown").multiselect('refresh');
				return;
			}
		}

		/*ajax_timer = $.timer(function() {
			xmlHttp.abort();
			alert(connectivity_msg);
			ajax_timer.stop();
		},connectivity_timeout_time,true);*/

		//Sending AJAX request
		url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/semesters_against_academicyear/tbl_academic_year/"+tbl_academic_year+"/rnd/"+rnd;
		xmlHttp.open("POST",url,true);
		xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlHttp.send("rnd="+rnd);
	}
</script>