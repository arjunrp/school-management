<?php include(ROOT_ADMIN_PATH."/admin/include/header.php");

?>
<!--WRAPPER-->
<div class="wrapper"> 
  
      <!--TOP BAR-->
      <?php include(ROOT_ADMIN_PATH."/admin/include/header_top_bar.php");?>
      <!--/TOP BAR--> 
      
      <!--LEFT-->
      <?php include(ROOT_ADMIN_PATH."/admin/include/left.php");?>
      <!--/LEFT--> 
      
      
      <!--INC FILE-->
      <?php
		//echo $page;
		//exit();
      	switch($page) {
			case("login_form"): {
				include(ROOT_ADMIN_PATH."/admin/login_form.php");
			break;	
			}	
			case("welcome"): {
				include(ROOT_ADMIN_PATH."/admin/welcome.php");
			break;	
			}	
			
			
			case("view_admin_user"): {
				include(ROOT_ADMIN_PATH."/admin/view_admin_user.php");
			break;	
			}
			case("view_user_rights"): {
				include(ROOT_ADMIN_PATH."/admin/view_user_rights.php");
			break;	
			}
			
						
			case("view_school_type"): {
				include(ROOT_ADMIN_PATH."/admin/view_school_type.php");
			break;	
			}	
			case("view_class_section"): {
				include(ROOT_ADMIN_PATH."/admin/view_class_section.php");
			break;	
			}	
			case("view_class_grade"): {
				include(ROOT_ADMIN_PATH."/admin/view_class_grade.php");
			break;	
			}
			case("view_class_session"): {
				include(ROOT_ADMIN_PATH."/admin/view_class_session.php");
			break;	
			}	
			
			case("view_all_students"): {
				include(ROOT_ADMIN_PATH."/admin/view_all_students.php");
			break;	
			}
			case("view_student_details"): {
				include(ROOT_ADMIN_PATH."/admin/view_student_details.php");
			break;	
			}	
				
			case("view_all_parents"): {
				include(ROOT_ADMIN_PATH."/admin/view_all_parents.php");
			break;	
			}
			case("view_all_teachers"): {
				include(ROOT_ADMIN_PATH."/admin/view_all_teachers.php");
			break;	
			}
			case("view_teacher_details"): {
				include(ROOT_ADMIN_PATH."/admin/view_teacher_details.php");
			break;	
			}	
			case("view_teacher_role"): {
				include(ROOT_ADMIN_PATH."/admin/view_teacher_role.php");
			break;	
			}
			case("view_students_promotion"): {
				include(ROOT_ADMIN_PATH."/admin/view_students_promotion.php");
			break;	
			}	
			case("view_message_to_teachers"): {
				include(ROOT_ADMIN_PATH."/admin/view_message_to_teachers.php");
			break;	
			}	
			case("view_message_to_parents"): {
				include(ROOT_ADMIN_PATH."/admin/view_message_to_parents.php");
			break;	
			}	
			case("view_teachers_group"): {
				include(ROOT_ADMIN_PATH."/admin/view_teachers_group.php");
			break;	
			}	
			case("view_parents_group"): {
				include(ROOT_ADMIN_PATH."/admin/view_parents_group.php");
			break;	
			}	
			case("view_message_from_parents"): {
				include(ROOT_ADMIN_PATH."/admin/view_message_from_parents.php");
			break;	
			}	
			
			case("view_school_records"): {
				include(ROOT_ADMIN_PATH."/admin/view_school_records.php");
			break;	
			}
			case("view_school_records_category"): {
				include(ROOT_ADMIN_PATH."/admin/view_school_records_category.php");
			break;	
			}
			case("view_assign_records"): {
				include(ROOT_ADMIN_PATH."/admin/view_assign_records.php");
			break;	
			}
			case("view_students_against_classes"): {
				include(ROOT_ADMIN_PATH."/admin/view_students_against_classes.php");
			break;	
			}
			
			case("view_student_points_conf"): {
				include(ROOT_ADMIN_PATH."/admin/view_student_points_conf.php");
			break;	
			}
			case("view_student_cards_conf"): {
				include(ROOT_ADMIN_PATH."/admin/view_student_cards_conf.php");
			break;	
			}
			case("view_student_cards_points_conf"): {
				include(ROOT_ADMIN_PATH."/admin/view_student_cards_points_conf.php");
			break;	
			}
			case("view_student_performance_conf"): {
				include(ROOT_ADMIN_PATH."/admin/view_student_performance_conf.php");
			break;	
			}
			
			
			case("view_semester"): {
				include(ROOT_ADMIN_PATH."/admin/view_semester.php");
			break;	
			}
		    case("view_academic_year"): {
				include(ROOT_ADMIN_PATH."/admin/view_academic_year.php");
			break;	
			}
			
			case("view_school_contact"): {
				include(ROOT_ADMIN_PATH."/admin/view_school_contact.php");
			break;	
			}
			
			case("view_all_events"): {
				include(ROOT_ADMIN_PATH."/admin/view_events.php");
			break;	
			}
			case("view_all_survey"): {
				include(ROOT_ADMIN_PATH."/admin/view_survey.php");
			break;	
			}
			
			
			case("view_school_gallery"): {
				include(ROOT_ADMIN_PATH."/admin/view_school_gallery.php");
			break;	
			}
			case("view_school_gallery_category"): {
				include(ROOT_ADMIN_PATH."/admin/view_school_gallery_category.php");
			break;	
			}
			
			
			case("view_all_topics"): {
				include(ROOT_ADMIN_PATH."/admin/view_forum_topics.php");
			break;	
			}
			case("view_all_comments"): {
				include(ROOT_ADMIN_PATH."/admin/view_forum_comments.php");
			break;	
			}
			
			
			case("view_all_student_forum_topics"): {
				include(ROOT_ADMIN_PATH."/admin/view_student_forum_topics.php");
			break;	
			}
			case("view_all_student_forum_comments"): {
				include(ROOT_ADMIN_PATH."/admin/view_student_forum_comments.php");
			break;	
			}
			
			case("view_students_group"): {
				include(ROOT_ADMIN_PATH."/admin/view_students_group.php");
			break;	
			}	
			
			
			case("view_cards_report"): {
				include(ROOT_ADMIN_PATH."/admin/view_cards_report.php");
			break;	
			}
			case("view_cards_report_detailed"): {
				include(ROOT_ADMIN_PATH."/admin/view_cards_report_detailed.php");
			break;	
			}
			
			case("view_points_report"): {
				include(ROOT_ADMIN_PATH."/admin/view_points_report.php");
			break;	
			}
			case("view_points_report_detailed"): {
				include(ROOT_ADMIN_PATH."/admin/view_points_report_detailed.php");
			break;	
			}
			
			case("view_attendance_report"): {
				include(ROOT_ADMIN_PATH."/admin/view_attendance_report.php");
			break;	
			}
			
			case("view_attendance_report_detailed"): {
				include(ROOT_ADMIN_PATH."/admin/view_attendance_report_detailed.php");
			break;	
			}
			
			case("view_attendance_days_report"): {
				include(ROOT_ADMIN_PATH."/admin/view_attendance_days_report.php");
			break;	
			}
			
			case("view_books_list"): {
				include(ROOT_ADMIN_PATH."/admin/view_books_list.php");
			break;	
			}
			
			case("view_subject"): {
				include(ROOT_ADMIN_PATH."/admin/view_subject.php");
			break;	
			}
				
			case("view_edit_book"): {
				include(ROOT_ADMIN_PATH."/admin/view_edit_book.php");
			break;	
			}
			
			case("view_book_category"): {
				include(ROOT_ADMIN_PATH."/admin/view_book_category.php");
			break;	
			}
			
			case("view_performance_report"): {
				include(ROOT_ADMIN_PATH."/admin/view_performance_report.php");
			break;	
			}
			
			case("view_progress_report"): {
				include(ROOT_ADMIN_PATH."/admin/view_progress_report.php");
			break;	
			}
			
			case("view_school_gallery_details"): {
				include(ROOT_ADMIN_PATH."/admin/view_school_gallery_details.php");
			break;	
			}
			
			
		}
	  ?>
      <!--/INC FILE--> 
      
      
      <!--FOOTER COPYRIGHT-->
      <?php include(ROOT_ADMIN_PATH."/admin/include/footer_copyright.php");?>
      <!--/FOOTER COPYRIGHT--> 
      
      <!--MENU RIGHT-->
      <?php include(ROOT_ADMIN_PATH."/admin/include/menu_slide_right.php");?>
      <!--/MENU RIGHT--> 
  
</div>
<!--/WRAPPER-->
<?php include(ROOT_ADMIN_PATH."/admin/include/footer.php");?>