<?php
//Init Parameters
$school_contacts_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}
?>
 

<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/settings/school_contact";
			}
		})
	});
	


	function ajax_save_changes() {
		
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/settings/save_school_contact",
			data: {
				latitude: $('#latitude').val(),
				longitude: $('#longitude').val(),
				email: $('#email').val(),
				phone: $('#phone').val(),
				mobile: $('#mobile').val(),
				fax: $('#fax').val(),
				website: $('#website').val(),
				payment_link: $('#payment_link').val(),
				facebook: $('#facebook').val(),
				google: $('#google').val(),
				youtube: $('#youtube').val(),
				twitter: $('#twitter').val(),
				linkedin: $('#linkedin').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
				refresh_page = "Y";
				    my_alert("School Contact is updated successfully.", 'green');
				    $("#pre-loader").hide();
				}else{
					refresh_page = "N";
					my_alert("School Contact updation failed, Please try again.", 'red');
					$("#pre-loader").hide();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
</script>
<script language="javascript">

	function ajax_validate_edit() {
		if (validate_email() == false || validate_phone() == false || validate_mobile() == false  || validate_fax() == false ) {
			return false;
		} else {
			ajax_save_changes();
		}
	} 
	

	function validate_phone() {
		var regExp = / /g;
		var strPhone = $("#phone").val();
		strPhone = strPhone.replace(regExp,'');
		if (strPhone.length <= 0) {
			my_alert("Phone Number is blank. Please enter Phone Number.");
			$("#phone").val('');
			$("#phone").focus();
			return false;
		}
			
		if (strPhone.length < 8 || strPhone.length > 10) {
			my_alert("Please enter valid phone number.");
			$("#phone").focus();
			return false;
		}

        for (var i = 0; i < strPhone.length; i++) {
			var ch = strPhone.substring(i, i + 1);
			if  (ch < "0" || "9" < ch) {
				    my_alert("Phone Number should be digits, Please re-enter your valid Phone Number");
				    $("#phone").select();
				    $("#phone").focus();
					return false;
				}
	    }
		return true;
	}

	function validate_mobile() {
		var regExp = / /g;
		var strPhone = $("#mobile").val();
		strPhone = strPhone.replace(regExp,'');
		if (strPhone.length <= 0) {
			my_alert("Mobile Number is blank. Please enter Mobile Number.");
			$("#mobile").val('');
			$("#mobile").focus();
			return false;
		}
			
		if (strPhone.length < 8 || strPhone.length > 10) {
			my_alert("Please enter valid Mobile Number.");
			$("#mobile").focus();
			return false;
		}

        for (var i = 0; i < strPhone.length; i++) {
			var ch = strPhone.substring(i, i + 1);
			if  (ch < "0" || "9" < ch) {
				    my_alert("Mobile Number should be digits, Please re-enter your valid Mobile Number");
				    $("#mobile").select();
				    $("#mobile").focus();
					return false;
				}
		}
	 return true;
	}


    function validate_fax() {
		var regExp = / /g;
		var strPhone = $("#fax").val();
		strPhone = strPhone.replace(regExp,'');
		if (strPhone.length <= 0) {
			return true;
		}
			
		if (strPhone.length < 8 || strPhone.length > 10) {
			my_alert("Please enter valid Fax Number.");
			$("#fax").focus();
			return false;
		}

        for (var i = 0; i < strPhone.length; i++) {
			var ch = strPhone.substring(i, i + 1);
			if  (ch < "0" || "9" < ch) {
				    my_alert("Fax Number should be digits, Please re-enter your valid Fax Number");
				    $("#fax").select();
				    $("#fax").focus();
					return false;
				}
		}
	  return true;
	}


	 function validate_email() {
		var regExp = / /g;
		var str = $("#email").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Email is blank , Please enter your Email");
			$("#email").focus()
			return false;
		}
			
		if (!isNaN(str)) {
			my_alert("Invalid Email , Please enter your valid Email");
			$("#email").focus();
			return false;
		}

		if(str.indexOf('@', 0) == -1) {
            my_alert("Invalid Email , Please enter your valid Email");
			$("#email").focus();
			return false;
		}
		return true;
	}

</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> School Contacts <small> Management</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>School Contact Information</li>
    </ol>
    <!--/BREADCRUMB--> 
    <div style="clear:both"></div>
  </section>
  
  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {

			$tbl_school_contacts_id     = $rs_school_contact[0]['tbl_school_contacts_id'];
			$latitude                   = $rs_school_contact[0]['latitude'];
			$longitude                  = $rs_school_contact[0]['longitude'];
			$email                      = $rs_school_contact[0]['email'];
			$phone                      = str_replace("+971","",$rs_school_contact[0]['phone']); 
			$mobile                     = str_replace("+971","",$rs_school_contact[0]['mobile']);
			$fax                        = str_replace("+971","",$rs_school_contact[0]['fax']);
			$is_active                  = $rs_school_contact[0]['is_active'];
			
			$website                    = $rs_school_contact[0]['website'];
			$facebook                   = $rs_school_contact[0]['facebook'];
			$google                     = $rs_school_contact[0]['google'];
			$twitter                    = $rs_school_contact[0]['twitter'];
			$linkedin                   = $rs_school_contact[0]['linkedin'];
			$youtube                    = $rs_school_contact[0]['youtube'];
			$payment_link               = $rs_school_contact[0]['payment_link'];
	
	?>
        <!--Edit-->
        
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add / Edit School Contact</h3>
                  <div class="box-tools">
            <?php /*?> <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/settings/school_contact"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a><?php */?>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_edit" id="frm_listing" class="form-horizontal" method="post">
                 <div class="box-body">
                  
                   <div class="form-group">
                      <label class="col-sm-2 control-label" for="latitude">Latitude</label>
                      <div class="col-sm-4">
                        <input type="text" placeholder="Latitude" id="latitude" name="latitude" class="form-control" value="<?=$latitude?>" >
                      </div>
                      
                       <label class="col-sm-2 control-label" for="longitude">Longitude</label>
    
                      <div class="col-sm-4">
                        <input type="text" placeholder="Longitude" id="longitude" name="longitude" class="form-control" value="<?=$longitude?>" >
                      </div>
                   </div>
                 
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="email">Email</label>
                      <div class="col-sm-4">
                        <input type="text" placeholder="Email" id="email" name="email" class="form-control" value="<?=$email?>" >
                      </div>
                       <label class="col-sm-2 control-label" for="phone">Phone</label>
                      <div class="col-sm-4">
                        <span style="position:absolute; padding-left:20px; padding-top:5px;"> + 971 </span> <input type="text" placeholder="Phone" id="phone" name="phone" class="form-control" 
                        value="<?=$phone?>" style="padding-left:60px;" >
                      </div>
                    </div>

                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="mobile">Mobile</label>
                      <div class="col-sm-4">
                        <span style="position:absolute; padding-left:20px; padding-top:5px;"> + 971 </span>  <input type="text" placeholder="Mobile" id="mobile" name="mobile" class="form-control"  
                        value="<?=$mobile?>" style="padding-left:60px;" >
                      </div>
                       <label class="col-sm-2 control-label" for="fax">Fax</label>
                      <div class="col-sm-4">
                       <span style="position:absolute; padding-left:20px; padding-top:5px;"> + 971 </span>   <input type="text" placeholder="Fax" id="fax" name="fax" class="form-control" 
                       value="<?=$fax?>"  style="padding-left:60px;" >
                      </div>
                    </div>
                    
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="website">Website</label>
                      <div class="col-sm-4">
                       <input type="text" placeholder="Website" id="website" name="website" class="form-control" value="<?=$website?>" >
                      </div>
                      
                      <label class="col-sm-2 control-label" for="facebook">Facebook</label>
                      <div class="col-sm-4">
                      <input type="text" placeholder="Facebook" id="facebook" name="facebook" class="form-control" value="<?=$facebook?>"  >
                      </div>
                    </div>
                    
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="google">Google</label>
                      <div class="col-sm-4">
                      <input type="text" placeholder="Google" id="google" name="google" class="form-control" value="<?=$google?>"  >
                      </div>   
                      
                      <label class="col-sm-2 control-label" for="youtube">Youtube</label>
                      <div class="col-sm-4">
                      <input type="text" placeholder="Youtube" id="youtube" name="youtube" class="form-control" value="<?=$youtube?>"  >
                      </div>
                    </div>
                    
                     <div class="form-group">
                       <label class="col-sm-2 control-label" for="twitter">Twitter</label>
                      <div class="col-sm-4">
                      <input type="text" placeholder="Twitter" id="twitter" name="twitter" class="form-control" value="<?=$twitter?>"  >
                      </div>
                      
                       <label class="col-sm-2 control-label" for="linkedin">Linkedin</label>
                      <div class="col-sm-4">
                      <input type="text" placeholder="Linkedin" id="linkedin" name="linkedin" class="form-control" value="<?=$linkedin?>"  >
                      </div>
                    </div>
                   
                     <div class="form-group">&nbsp;</div>
                    
                    
                      <div class="form-group">
                    
                       <label class="col-sm-2 control-label" for="payment_link">Online Fee Link</label>
                      <div class="col-sm-4">
                      <input type="text" placeholder="Online Fee Link" id="payment_link" name="payment_link" class="form-control" value="<?=$payment_link?>"  >
                      </div>
                    </div>
                    
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate_edit()">Save Changes</button>
                    <input type="hidden" name="school_contacts_id_enc" id="school_contacts_id_enc" value="<?=$tbl_school_contacts_id?>" />
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
		        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/settings/school_contact";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
    
  
 <link href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" rel="stylesheet">
 <script src="http://code.jquery.com/jquery-1.11.1.js"></script>
 <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
  <script>
  $( function() {
		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
				
				/* var order = $("#tabledivbody").sortable("serialize");
				
				alert(order);
				
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/your/url/here'
				});*/
				
				
				
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	
				
				
				
				
				
			}
	  } );
  
  } );
	
  </script> 
 
    
                    <div id="mid1" class="box box-success">
                        <div class="box-header">
                          <div style="width:20%; float:left;">
                          <h3 class="box-title">SEARCH</h3>
                          </div>
                          <div style="width:60%; float:left;"> 
                    	     <div class="col-sm-5">
                             <?php //echo $category_parent_list ?>
                   		      </div>
                             
                          </div>
                        </div>  
                     </div>     
    
            <!--Listing-->
                    <div id="mid1_list" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Student Cards Configuration</h3>
                          <div class="box-tools">
                            <?php if (count($rs_student_cards)>0) { echo $paging_string;}?>	
                           <!-- <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
                            <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>-->
                          </div>
                        </div>
                        
                        <div class="box-body">
                     <!--   <div style="color:#030; font-weight:bold;">You can sort categories by using drag and drop of rows </div>-->
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle"><input id="select_all" type="checkbox" value="" /></th>
                              <!--<th width="10%" align="center" valign="middle">Sl No.</th>-->
                              <th width="25%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y">Card Name [En] <?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?><div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>
                              <th width="25%" align="center" valign="middle">Card Name [Ar]</th>
                              <th width="15%" align="center" valign="middle">Category</th>
                              <th width="10%" align="center" valign="middle">Status</th>
                              <th width="10%" align="center" valign="middle">Action</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                                for ($i=0; $i<count($rs_student_cards); $i++) { 
                                    $id                    = $rs_student_cards[$i]['id'];
                                    $tbl_card_category_id  = $rs_student_cards[$i]['tbl_card_category_id'];
                                    $category_name_en      = $rs_student_cards[$i]['category_name_en'];
                                    $category_name_ar      = $rs_student_cards[$i]['category_name_ar'];
                                    $card_point            = $rs_student_cards[$i]['card_point'];
									$added_date            = $rs_student_cards[$i]['added_date'];
                                    $is_active             = $rs_student_cards[$i]['is_active'];
									$classTypes            = $rs_student_cards[$i]['classTypes'];
									
                                    
                                    $title_en = ucfirst($title_en);
                                    $added_date = date('m-d-Y',strtotime($added_date));
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_card_category_id?>" >
                              <td align="left" valign="middle">
                              <span style="float:left;">
                              <input id="card_category_id_enc" name="card_category_id_enc" class="checkbox" type="checkbox" value="<?=$tbl_card_category_id?>" />
                              </span>
                              
                             <?php /*?> <span style="float:left;">&nbsp;
                              <?php if($i<>0){ ?> <i class="fa fa-arrow-up"  style="color:#3c8dbc; cursor:pointer;"  aria-hidden="true" title="Sorting - Drag & Drop To Up"></i> &nbsp; <?php } ?>
                               <?php if($i<> count($rs_all_categories)-1){ ?> <i class="fa fa-arrow-down" style="color:#3c8dbc;cursor:pointer;" aria-hidden="true" title="Sorting - Drag & Drop To Down"></i> <?php } ?>
                              </span><?php */?>
                              </td>
                             <!-- <td align="left" valign="middle"><?=$offset+$i+1?></td>-->
                              <td align="left" valign="middle"><?=$category_name_en?></td>
                              <td align="left" valign="middle"><?=$category_name_ar?></td>
                              <td align="left" valign="middle"><?=$classTypes?></td>
                              <td align="left" valign="middle">
                                <div id="act_deact_<?=$tbl_card_category_id?>">
                                <?php if (trim($is_active) == "Y") { ?>
                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_card_category_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                <?php } else { ?>
                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_card_category_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                <?php } ?>
                                </div>
                              </td>
                              <td align="left" valign="middle">
                                <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/edit_student_card/card_category_id_enc/<?=$tbl_card_category_id?>"><button class="btn bg-purple fa fa-pencil" type="button" title="Edit"></button></a>
                              </td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="8" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_student_cards)<=0) {
                            ?>
                            <tr>
                              <td colspan="8" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no categories available. Click on the + button to create one.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
    
            <!--Add or Create-->
              <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Student Card</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                  
                   
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="latitude">Latitude</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Latitude" id="latitude" name="latitude" class="form-control">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="longitude">Longitude</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Longitude" id="longitude" name="longitude" class="form-control" >
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="email">Email</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Email" id="email" name="email" class="form-control" >
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="email">Email</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Email" id="email" name="email" class="form-control" >
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="phone">Phone</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Phone" id="phone" name="phone" class="form-control" >
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="mobile">Mobile</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Mobile" id="mobile" name="mobile" class="form-control" >
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="fax">Fax</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Fax" id="fax" name="fax" class="form-control" >
                      </div>
                    </div>
                    
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate()">Submit</button>
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
            <!--/Add or Create-->
                
        <!--/Admin Category Management-->

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>

        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >
function search_data() {
		var tbl_category_id = $("#tbl_category_id").val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/all_categories/";
		if(tbl_category_id !='')
			url += "tbl_category_id/"+tbl_category_id+"/";
		
			url += "offset/0/";
		window.location.href = url;
		<?php /*?>window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/enquiry/all_enquiries/is_not_replied/"+is_not_replied+"/tbl_court_id/"+tbl_court_id+"/tbl_category_id/"+tbl_category_id;<?php */?>
	}
</script>