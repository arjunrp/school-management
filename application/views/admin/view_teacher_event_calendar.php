<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Event <small> Calendar</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style="float:left; position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home/teacher" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Event Calendar</li>
    </ol>
    <!--/BREADCRUMB--> 
    <div style="clear:both"></div>
  </section>
  
  <section class="content"> 
    <!--WORKING AREA-->	
      
    <div id="mid1" class="box">
      <div class="box-header">
        <h3 class="box-title">Event Listing</h3>
        <div class="box-tools">
        &nbsp;
        </div>
      </div>
      <div class="box-body">
		<script language="javascript">
                $(document).ready(function(e) {
                    load_calendar();
                });
                
                /* Function to load calendar */
                function load_calendar(){
                    $('#calendar_en').fullCalendar({draggable: false,
                        events: "<?=HOST_URL?>/events/event_list_web/?lan=en&school_id=<?=$_SESSION['aqdar_smartcare']['tbl_school_id_sess']?>",
                        eventClick: function(event) {
                            var title = event.title;
                            var description = event.description;
                            var start = new Date(event.start);
                            var end = new Date(event.end);
                            var color = event.color;
                            
                            var theyear = start.getFullYear();
                            var themonth = start.getMonth()+1;
                            var thetoday = start.getDate();
                            var date_time_str_start = themonth+"/"+thetoday+"/"+theyear;			
                
                            var theyear = end.getFullYear();
                            var themonth = end.getMonth()+1;
                            var thetoday = end.getDate();
                            var date_time_str_end = themonth+"/"+thetoday+"/"+theyear;			
                            
                            $('#title').html(title);
                            $('#description').html(description);
                            $('#event_date').html("Start: "+date_time_str_start+", End: "+date_time_str_end);
                        }		
                    });
                    $('#calendar_en').fullCalendar( 'refetchEvents' );
                }
                </script>
                <!--CALENDAR CSS-->
                <link href='<?=HOST_URL?>/css/fullcalendar.css' rel='stylesheet' />
                <link href='<?=HOST_URL?>/css/fullcalendar.print.css' rel='stylesheet' media='print' />
              
                <div id="calendar_en" style="border:1px solid #000; width:70%; margin:auto;"></div>

                <br>
                <br>
              
              <table width="100%" class="table table-bordered table-striped" id="example1">
                <thead>
                <tr>
                  <th width="25%" align="center" valign="middle">Title</th>
                  <th width="40%" align="center" valign="middle">Description</th>
                  <th width="35%" align="center" valign="middle">Date</th>
                  </tr>
                </thead>
                <tbody>
                <tr>
                  <td align="left" valign="middle"><div id="title"></div></td>
                  <td align="left" valign="middle"><div id="description"></div></td>
                  <td align="left" valign="middle"><div id="event_date"></div></td>
                  </tr>
        
                </tbody>
                <tfoot>
                </tfoot>
              </table>

                <br>
                <br>

      </div>        
    </div>    
    <!--/WORKING AREA--> 
  </section>
</div>