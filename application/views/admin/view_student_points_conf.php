<?php
//Init Parameters
$point_category_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}
?>
 
<script language="javascript">
/*$(function() {
    $('#behaviour_point').bind('keyup', function(event) {
        var currValue = $(this).val();

        if(currValue.search(/[^0-9+-]/) != -1)
        {
            // Change this to something less obnoxious
            alert('Only numerical inputs please');
        }

        $(this).val(currValue.replace(/[^0-9+-]/, ''));
    });
});
*/</script>

<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/student_points_conf";
			}
		})
	});
	
function confirm_delete_popup() {
		var len = $("input[id='point_category_id_enc']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more behavior point(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/deleteBehaviorPoint",
			data: {
				point_category_id_enc: $("input[id='point_category_id_enc']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Behavior point(s) deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}	
	function ajax_activate(point_category_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/activateBehaviorPoint",
			data: {
				point_category_id_enc: point_category_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Behavior points are activated successfully.", 'green')

				$('#act_deact_'+point_category_id_enc).html('<span style="cursor:pointer" onClick="ajax_deactivate(\''+point_category_id_enc+'\')" onMouseOver="deactivate_me(this)" onMouseOut="reset_activate(this)" class="label label-success">Active</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_deactivate(point_category_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/deactivateBehaviorPoint",
			data: {
				point_category_id_enc: point_category_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Behavior points are de-activated successfully.", 'green')
				
				$('#act_deact_'+point_category_id_enc).html('<span style="cursor:pointer" onClick="ajax_activate(\''+point_category_id_enc+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/is_exist_behavior_point",
			data: {
				point_category_id_enc: "<?=$point_category_id_enc?>",
				point_name_en: $('#point_name_en').val(),
				point_name_ar: "",
				is_ajax: true
			},
			success: function(data) {
				
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
					refresh_page = "N";
					my_alert("Behavior point already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_create();
				}
			
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist_edit() {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/is_exist_behavior_point",
			data: {
				point_category_id_enc:$('#point_category_id_enc').val(),
				point_name_en: $('#point_name_en').val(),
				point_name_ar: "",
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
					refresh_page = "N";
					my_alert("Behavior point already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_save_changes();
				}
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_create() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/add_behavior_point",
			data: {
				point_category_id_enc: "<?=$point_category_id_enc?>",
				point_name_en: $('#point_name_en').val(),
				point_name_ar: $('#point_name_ar').val(),
				behaviour_point: $('#behaviour_point').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
				refresh_page = "Y";
				    my_alert("Behavior point is added successfully.", 'green');
				    $("#pre-loader").hide();
				}else{
					refresh_page = "N";
					my_alert("Behavior point addition failed, Please try again.", 'red');
					$("#pre-loader").hide();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_save_changes() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/save_behavior_point_changes",
			data: {
				point_category_id_enc:$('#point_category_id_enc').val(),
				point_name_en: $('#point_name_en').val(),
				point_name_ar: $('#point_name_ar').val(),
				behaviour_point: $('#behaviour_point').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
				refresh_page = "Y";
				    my_alert("Behavior point is updated successfully.", 'green');
				    $("#pre-loader").hide();
				}else{
					refresh_page = "N";
					my_alert("Behavior point updation failed, Please try again.", 'red');
					$("#pre-loader").hide();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
</script>
<script language="javascript">
	function ajax_validate() {
		if (validate_point_info() == false || validate_point_info_ar() == false  || validate_behaviour_point() == false ) {
			return false;
		} else {
		   is_exist();
		}
	} 

	function ajax_validate_edit() {
		if ( validate_point_info() == false || validate_point_info_ar() == false  || validate_behaviour_point() == false ) {
			return false;
		} else {
			is_exist_edit();
		}
	} 


	function validate_point_info() {
		var regExp = / /g;
		var str = $('#point_name_en').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Point Information [En] is blank. Please enter point information [En].");
			return false;
		}
		
	}

	function validate_point_info_ar() {
		var regExp = / /g;
		var str = $('#point_name_ar').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Point Information [Ar] is blank. Please enter point information [Ar].");
		return false;
		}
	}
	
	function validate_behaviour_point() {
		var regExp = / /g;
		var str = new String();
		str = $('#behaviour_point').val().trim();
		var temp1, temp2;
		temp1 = str.indexOf("+");
		temp2 = str.indexOf("-");
		
		if(parseInt(temp1) >= 0 || parseInt(temp2) >= 0) {
		} else {
			my_alert("Please provide (-) or (+) sign with points.");
			return false;
		}

		str = str.replace(regExp,'');
		if($.isNumeric(str))
			str = Math.abs(str);

		if (str.length <= 0) {
			my_alert("Points is blank. Please enter points.");
		   return false;
		}else if(!$.isNumeric(str))
		{
			my_alert("Points should be integer. Please enter points.");
			//$('#behaviour_point').val('');
		   return false;
		}
		
		
		return true;
	}
	
	

</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Students Points <small> Management</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Students Points Configuration</li>
    </ol>
    <!--/BREADCRUMB--> 

    <div style="clear:both"></div>
  </section>
  
  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {

			$tbl_point_category_id      = $point_obj[0]['tbl_point_category_id'];
			$point_name_en              = $point_obj[0]['point_name_en'];
			$point_name_ar              = $point_obj[0]['point_name_ar'];
			$behaviour_point            = $point_obj[0]['behaviour_point'];
			$is_active                  = $point_obj[0]['is_active'];
	?>
        <!--Edit-->
        
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Behavior Point</h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/student_points_conf"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_edit" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="point_name_en">Point Information [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Point Information [En]" id="point_name_en" name="point_name_en" class="form-control" value="<?=$point_name_en?>" >
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="point_name_ar">Point Information [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Point Information [Ar]" id="point_name_ar" name="point_name_ar" class="form-control" dir="rtl" value="<?=$point_name_ar?>" >
                      </div>
                    </div>
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="behaviour_point">Points (+ or - sign)<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Points (+ or - sign)" id="behaviour_point" name="behaviour_point" class="form-control"  value="<?=$behaviour_point?>" >
                      </div>
                    </div>
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate_edit()">Save Changes</button>
                    <input type="hidden" name="point_category_id_enc" id="point_category_id_enc" value="<?=$tbl_point_category_id?>" />
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
		        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/student/student_points_conf";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
    
  
 <link href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" rel="stylesheet">
 <script src="http://code.jquery.com/jquery-1.11.1.js"></script>
 <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
  <script>
  $( function() {
		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
				
				/* var order = $("#tabledivbody").sortable("serialize");
				
				alert(order);
				
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/your/url/here'
				});*/
				
				
				
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	
				
				
				
				
				
			}
	  } );
  
  } );
  
   function show_records()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/school_records/";
		window.location.href = url;
	}
	
	function show_assign_records()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/records/list_assign_records/";
		window.location.href = url;
	}
  </script> 
 
    
                    <div id="mid1" class="box box-success">
                        <div class="box-header">
                          <div class="col-sm-1" >
                          <h3 class="box-title">SEARCH</h3>
                          </div>
                          <div class="col-sm-11"> 
                             
                               <div class="col-sm-6"><input name="q" id="q" value="<?=urldecode($q)?>" type="text" class="form-control" placeholder="Search By Point Information."   > </div>
                               <div class="col-sm-2"><button class="btn btn-success" type="button" onclick="search_data()">Search</button>&nbsp;<button class="btn btn-success" type="button" 
                               onclick="reset_data();">Reset</button>
                               </div>
                           
                          </div>
                        </div>  
                     </div>     
    
            <!--Listing-->
                    <div id="mid1_list" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Points Configuration</h3>
                          <div class="box-tools">
                            <?php if (count($rs_behaviour_points)>0) { echo $paging_string;}?>	
                            <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
                            <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>
                          </div>
                        </div>
                        
                        <div class="box-body">
                     <!--   <div style="color:#030; font-weight:bold;">You can sort categories by using drag and drop of rows </div>-->
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle"><input id="select_all" type="checkbox" value="" /></th>
                              <!--<th width="10%" align="center" valign="middle">Sl No.</th>-->
                              <th width="25%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y">Point Information [En] <?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?><div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>
                              <th width="25%" align="center" valign="middle">Point Information [Ar]</th>
                              <th width="15%" align="center" valign="middle">Points</th>
                              <th width="10%" align="center" valign="middle">Status</th>
                              <th width="10%" align="center" valign="middle">Action</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                                for ($i=0; $i<count($rs_behaviour_points); $i++) { 
                                    $id = $rs_behaviour_points[$i]['id'];
                                    $tbl_point_category_id = $rs_behaviour_points[$i]['tbl_point_category_id'];
                                    $point_name_en         = $rs_behaviour_points[$i]['point_name_en'];
                                    $point_name_ar         = $rs_behaviour_points[$i]['point_name_ar'];
                                    $behaviour_point       = $rs_behaviour_points[$i]['behaviour_point'];
									$added_date            = $rs_behaviour_points[$i]['added_date'];
                                    $is_active             = $rs_behaviour_points[$i]['is_active'];
                                    
                                    $title_en = ucfirst($title_en);
                                    $added_date = date('m-d-Y',strtotime($added_date));
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_point_category_id?>" >
                              <td align="left" valign="middle">
                              <span style="float:left;">
                              <input id="point_category_id_enc" name="point_category_id_enc" class="checkbox" type="checkbox" value="<?=$tbl_point_category_id?>" />
                              </span>
                              
                             <?php /*?> <span style="float:left;">&nbsp;
                              <?php if($i<>0){ ?> <i class="fa fa-arrow-up"  style="color:#3c8dbc; cursor:pointer;"  aria-hidden="true" title="Sorting - Drag & Drop To Up"></i> &nbsp; <?php } ?>
                               <?php if($i<> count($rs_all_categories)-1){ ?> <i class="fa fa-arrow-down" style="color:#3c8dbc;cursor:pointer;" aria-hidden="true" title="Sorting - Drag & Drop To Down"></i> <?php } ?>
                              </span><?php */?>
                              </td>
                             <!-- <td align="left" valign="middle"><?=$offset+$i+1?></td>-->
                              <td align="left" valign="middle"><?=$point_name_en?></td>
                              <td align="left" valign="middle"><?=$point_name_ar?></td>
                              <td align="left" valign="middle"><?=$behaviour_point?></td>
                              <td align="left" valign="middle">
                                <div id="act_deact_<?=$tbl_point_category_id?>">
                                <?php if (trim($is_active) == "Y") { ?>
                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_point_category_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                <?php } else { ?>
                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_point_category_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                <?php } ?>
                                </div>
                              </td>
                              <td align="left" valign="middle">
                                <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/edit_behavior_point/point_category_id_enc/<?=$tbl_point_category_id?>"><button class="btn bg-purple fa fa-pencil" type="button" title="Edit"></button></a>
                              </td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="8" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_behaviour_points)<=0) {
                            ?>
                            <tr>
                              <td colspan="8" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no categories available. Click on the + button to create one.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
    
            <!--Add or Create-->
              <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Behavior Point</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                  
                   
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="point_name_en">Point Information [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Point Information [En]" id="point_name_en" name="point_name_en" class="form-control">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="point_name_ar">Point Information [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Point Information [Ar]" id="point_name_ar" name="point_name_ar" class="form-control" dir="rtl">
                      </div>
                    </div>
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="behaviour_point">Points (+ or - sign)<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Points (+ or - sign)" id="behaviour_point" name="behaviour_point" class="form-control" >
                      </div>
                    </div>
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate()">Submit</button>
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
            <!--/Add or Create-->
                
        <!--/Admin Category Management-->

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>

        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >
function search_data() {
		var q = $("#q").val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/student_points_conf/";
		
		if(q !='')
			url += "q/"+q+"/";
		
			url += "offset/0/";
		window.location.href = url;
	}

function reset_data() {
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/student_points_conf/";
		url += "offset/0/";
		window.location.href = url;
	}


</script>