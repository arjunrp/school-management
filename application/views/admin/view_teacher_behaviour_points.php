        <tr>
          <td align="left" valign="middle"><?=$student_name?></td>
          <td align="left" valign="middle">
          	<select name="points_student" id="points_student" class="form-control">
			<option value="">Select Point</option>
            <?php if(count($points_arr['positive_points'])>0){ ?>
            
            <optgroup label="Positive">
			<?php
           		for ($i=0; $i<count($points_arr['positive_points']); $i++) {
				
				 	
					$positive_points = $points_arr['positive_points'];
					if (LAN_SEL == "ar") {
						$points_text = $positive_points[$i]["point_name_ar"];
					} else {
	                    $points_text = $positive_points[$i]["point_name_en"];
					}
					$behaviour_point = $positive_points[$i]["behaviour_point"];
            ?>
            	<option value="<?=$positive_points[$i]["tbl_point_category_id"]?>" data-pointsmessage="<?=$points_text?>"><?=$points_text?> <?=$behaviour_point?></option>
            <?php
				}
            ?>
            </optgroup>
            
            <?php } ?>
            
            <?php if(count($points_arr['negative_points'])>0){ ?>
            
            <optgroup label="Need To Improve">
            <?php
           		for ($i=0; $i<count($points_arr['negative_points']); $i++) {
					
					$negative_points = $points_arr['negative_points'];
					if (LAN_SEL == "ar") {
						$points_text = $negative_points[$i]["point_name_ar"];
					} else {
	                    $points_text = $negative_points[$i]["point_name_en"];
					}
					$behaviour_point = $negative_points[$i]["behaviour_point"];
            ?>
            	<option value="<?=$negative_points[$i]["tbl_point_category_id"]?>" data-pointsmessage="<?=$points_text?>"><?=$points_text?> <?=$behaviour_point?></option>
            <?php
				}
            ?>
            </optgroup>
            
            <?php } ?>
            
            
              <?php if(count($points_arr['ministry_points'])>0){ ?>
              
             <optgroup label="Special Incentives">
			<?php
           		for ($i=0; $i<count($points_arr['ministry_points']); $i++) {
					
					$ministry_points = $points_arr['ministry_points'];
					
					if (LAN_SEL == "ar") {
						$points_text = $ministry_points[$i]["point_name_ar"];
					} else {
	                    $points_text = $ministry_points[$i]["point_name_en"];
					}
					$behaviour_point = $ministry_points[$i]["behaviour_point"];
            ?>
            	<option value="<?=$ministry_points[$i]["tbl_point_category_id"]?>" data-pointsmessage="<?=$points_text?>"><?=$points_text?> <?=$behaviour_point?></option>
            <?php
				}
            ?>
            </optgroup>
            
            <?php } ?>
            </select>
          </td>
        </tr>









