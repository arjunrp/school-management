<?php
//Init Parameters
$teacher_group_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}
?>
 
<style>
.txt_en {
	text-align:left;
	padding-left:2px;
}
.txt_ar {
	text-align:right;
	padding-right:2px;	
	direction:rtl;		
}
textarea {
    height: 170px;
    padding-bottom: 6px;
    padding-top: 6px;
    width: 95%;
	font-size: 14px;
	border: 1px solid #ddd;
 }
 select {
   font-size: 14px;
   border: 1px solid #ddd;
 }
</style>
<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/teachers_group";
			}
		})
	});
	
function confirm_delete_popup() {
		var len = $("input[id='tbl_teacher_group_id']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more group(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/deleteTeacherGroup",
			data: {
				tbl_teacher_group_id: $("input[id='tbl_teacher_group_id']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Group(s) deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}	
	function ajax_activate(tbl_teacher_group_id) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/activateTeacherGroup",
			data: {
				tbl_teacher_group_id: tbl_teacher_group_id,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Group activated successfully.", 'green')

				$('#act_deact_'+tbl_teacher_group_id).html('<span style="cursor:pointer" onClick="ajax_deactivate(\''+tbl_teacher_group_id+'\')" onMouseOver="deactivate_me(this)" onMouseOut="reset_activate(this)" class="label label-success">Active</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_deactivate(tbl_teacher_group_id) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/deactivateTeacherGroup",
			data: {
				tbl_teacher_group_id: tbl_teacher_group_id,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Group de-activated successfully.", 'green')
				
				$('#act_deact_'+tbl_teacher_group_id).html('<span style="cursor:pointer" onClick="ajax_activate(\''+tbl_teacher_group_id+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	
	function ajax_submit() {
			
		var selectednumbers = "";
        $('.checkboxTeacher:checked').each(function(i){
          selectednumbers += $(this).val()+"&";
        });		
		
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/save_teacher_group",
			data: {
				group_name_en            : $('#group_name_en').val(),
				group_name_ar            : $('#group_name_ar').val(),
				tbl_teacher_id           : selectednumbers,
				tbl_teacher_group_id     : '<?=$teacher_group_id_enc?>',
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='X') {
					refresh_page = "N";
					my_alert("Group is already exist, Please try another name.", 'red');
					$("#pre-loader").hide();
				} else if (temp=='N') {
					refresh_page = "N";
					my_alert("Group added failed, Please try again.", 'red');
					$("#pre-loader").hide();
				   
				}else{
					 refresh_page = "Y";
				    my_alert("Group added successfully.", 'green');
				    $("#pre-loader").hide();
				}
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_update() {
		var selectednumbers = "";
        $('.checkboxTeacher:checked').each(function(i){
          selectednumbers += $(this).val()+"&";
        });			
			
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/update_teacher_group",
			data: {
				group_name_en            : $('#group_name_en').val(),
				group_name_ar            : $('#group_name_ar').val(),
				tbl_teacher_id           : selectednumbers,
				tbl_teacher_group_id     : $('#teacher_group_id_enc').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='N') {
					refresh_page = "N";
					my_alert("Group updation failed, Please try again.", 'red');
					$("#pre-loader").hide();
				   
				}else{
					 refresh_page = "Y";
				    my_alert("Group updated successfully.", 'green');
				    $("#pre-loader").hide();
				}
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
</script>
<script language="javascript">
   //add student
   /* || validate_picture() == false*/
	function ajax_validate() {
		if (validate_group_name() == false || validate_teacher() == false ) 
		{
			return false;
		}
		else{
			ajax_submit();
		}
	}
	
    //edit student
	function ajax_validate_edit() {
		if (validate_group_name() == false || validate_teacher() == false) 
		{
			return false;
		} 
		else{
			ajax_update();
		}
	} 
	
  /************************************* START MESSAGE VALIDATION *******************************/

   function validate_group_name() {
		var regExp = / /g;
		var str = $("#group_name_en").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Group Name [En] is blank. Please enter group name in English")
			$("#group_name_en").val('');
			$("#group_name_en").focus();
		return false;
		}
		var regExp = / /g;
		var str = $("#group_name_ar").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Group Name [Ar] is blank. Please enter group name in Arabic")
			$("#group_name_ar").val('');
			$("#group_name_ar").focus();
		return false;
		}
		return true;
	
	}
	
	function validate_teacher() {
		
		var tbl_teacher_id = "";
		$('.checkboxTeacher:checked').each(function(i){
		  tbl_teacher_id += $(this).val()+",";
		});
		
		if (tbl_teacher_id=="" ) {
			my_alert("Please select Teacher(s)");
		return false;
		}
	  return true;
	}
	
</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Teachers Group <small> Management</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Teachers Group</li>
    </ol>
    <!--/BREADCRUMB-->
<!--    <div style=" float:right; padding-right:10px;"> <button onclick="show_message_to_teachers()" title="Records" type="button" class="btn btn-primary">Message To Teachers</button></div> 
-->    <div style="clear:both"></div>
  </section>
      <link href="<?=HOST_URL?>/assets/admin/dist/css/jquery-ui.css" rel="stylesheet">
      <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-1.11.1.js"></script>
      <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-ui.js"></script>
      <link href="<?=HOST_URL?>/assets/admin/dist/css/uploadfile.min.css" rel="stylesheet">
   <script>
   function show_message_to_teachers()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/message_to_teachers/";
		window.location.href = url;
	}
	
 
 </script>
  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {
	?>
        <!--Edit-->
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Teacher Group</h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/teachers_group"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

            
     <style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url('<?=IMG_PATH?>/delete.jpg');
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
		clear:both;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
	#overlay_container {
		position:relative;
	}
	#overloading {
		background-image:url('<?=IMG_PATH?>/preloader/preloader_2.gif');
		background-repeat:no-repeat;
		background-position:center;
		background-color:#CCC;
		position:absolute;
		left:0px;
		top:0px;
		opacity: 0.3;
		z-index: 10000;
	}
	#div_listing_container {
		display:none;	
	}
	.d_d_text {
		color:#745156;
		font-size:20px;
			
	}
	.ajax-upload-dragdrop {
		margin:auto;
		margin-bottom:10px;
		width:700px !important;
	}
	.ajax-file-upload-statusbar {
		margin:auto;
		margin-top:10px;
	}
	.ajax-file-upload {
		height:31px;
	}
	
	
	 #tabs-1{  
	    overflow-y:scroll; overflow-x:none;
	}

    #tabs-2{
		overflow-y:scroll; overflow-x:none;
	}
				  
  .ui-tabs-active{
		border-color:#efca86  !important;
   }
					 
	.ui-tabs .ui-tabs-nav li {
		float:left;
		font-size: 16px;
        font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif;
  }
  label{
	  display: inline-block;
      font-weight: 700;
  }
  
  .ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button {
    font-family:"Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
    font-size: 14px;
}
  
  .ui-widget{
	 font-size: 16px;
     font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
  }
  .form-control{
	 font-size: 14px; 
  }
</style>         
         <?php
		 	$tbl_teacher_group_id             = $group_info[0]['tbl_teacher_group_id'];		
			$group_name_en             	    = $group_info[0]['group_name_en'];
			$group_name_ar             	    = $group_info[0]['group_name_ar'];
		 ?>       
                
             <div class="box-body">
                    <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
                         <div class="form-group">
                          <label class="col-sm-2 control-label" for="group_name_en">Group Name [En]<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10">
                            <input type="text" placeholder="Group Name [En]" id="group_name_en" name="group_name_en" class="form-control" value="<?=$group_name_en?>">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="group_name_ar">Group Name [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10">
                            <input type="text" placeholder="Group Name [Ar]" id="group_name_ar" name="group_name_ar" class="form-control" value="<?=$group_name_ar?>" dir="rtl">
                          </div>
                        </div>
                       
                    <?php /*?>     <div class="form-group">
                          <label class="col-sm-2 control-label" for="tbl_teacher_id">Teacher(s)<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10">
                              <select id="tbl_teacher_id" name="tbl_teacher_id" multiple size="10" style="padding:0px 5px 0px 2px; font-size:14px;">
                                    <?php for($i=0; $i<count($rs_all_teachers); $i++) { 
									       $tbl_teacher_id_u =  $rs_all_teachers[$i]["tbl_teacher_id"];
									
									         if(in_array($tbl_teacher_id_u, $assign_teachers_list, true))
                                               $selClass = "selected";
                                             else
                                               $selClass = "";
									
									?>
                                    <option value="<?=$rs_all_teachers[$i]["tbl_teacher_id"]?>" <?=$selClass?> ><?=$rs_all_teachers[$i]["first_name"]?> <?=$rs_all_teachers[$i]["last_name"]?> :: <?=$rs_all_teachers[$i]["first_name_ar"]?> <?=$rs_all_teachers[$i]["last_name_ar"]?> </option>
                                    <?php	} ?>
                            </select>
                          </div>
                        </div>
                        <?php */?>
                        
                         <div class="form-group" id="all_student">
                        <label class="col-sm-2 control-label" for="tbl_teacher_id">Teacher(s)</label>
        
                          <div class="col-sm-10" id="divTeacher" style="height:400px; overflow-y:scroll;">
                          <div class"col-sm-10" >
	 
                        <?php  if(count($rs_all_teachers)>0){ ?>
                           <div style="padding-bottom:10px;background-color:#e8eaeb;" class="col-sm-12"> 
                           <input type="checkbox" value="" id="select_all_teacher" class="checkboxTeacher" >&nbsp;Select All</div>
                        <?php  } ?>
                        <?php
                             for($i=0; $i<count($rs_all_teachers); $i++) { 
							 $tbl_teacher_id_u =  $rs_all_teachers[$i]["tbl_teacher_id"];
							 if(in_array($tbl_teacher_id_u, $assign_teachers_list, true))
                                               $selClass = "checked";
                                             else
                                               $selClass = "";
							 
							 
                             ?>
                             <div class="col-sm-4" style="padding-top:10px; padding-bottom:10px; border:1px solid #CCC;"> 
								  <input id="tbl_teacher_id_<?=$i?>" name="tbl_teacher_id[]" class="checkboxTeacher" type="checkbox" value="<?=$rs_all_teachers[$i]["tbl_teacher_id"]?>"  <?=$selClass?>/>&nbsp;
								   <?=$rs_all_teachers[$i]["first_name"]?> <?=$rs_all_teachers[$i]["last_name"]?> :: <?=$rs_all_teachers[$i]["first_name_ar"]?> <?=$rs_all_teachers[$i]["last_name_ar"]?>
                             </div>
							<?php  
	                         }
							 ?>
	                          </div> 
                        </div>
                        </div>    
                        
                        
                        
                        
                    
                     
                        <!-- /.box-body -->
                      <div class="box-footer">
                        <button class="btn btn-primary" type="button" onclick="ajax_validate_edit()">Submit</button>
                         <input type="hidden" name="teacher_group_id_enc" id="teacher_group_id_enc" value="<?=$tbl_teacher_group_id?>" />
                        <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                      </div>
                      <!-- /.box-footer -->  
                      
            
           </form>
                </div>
                
    </div>
	<script>
	$('#select_all_teacher').on('click',function(){
	if(this.checked){
		$('.checkboxTeacher').each(function(){
			this.checked = true;
		});
		
	}else{
		 $('.checkboxTeacher').each(function(){
			this.checked = false;
		});
	}
  });
	</script>	        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/message/teachers_group";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
    
  
 <link href="<?=HOST_URL?>/assets/admin/dist/css/jquery-ui.css" rel="stylesheet">
 <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-1.11.1.js"></script>
 <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-ui.js"></script>
  <script>
   $('#select_all_teacher').on('click',function(){
	if(this.checked){
		$('.checkboxTeacher').each(function(){
			this.checked = true;
		});
		
	}else{
		 $('.checkboxTeacher').each(function(){
			this.checked = false;
		});
	}
  });
  
  
  $( function() {
		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
				
				/* var order = $("#tabledivbody").sortable("serialize");
				
				alert(order);
				
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/your/url/here'
				});*/
				
				
				
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	
				
				
				
				
				
			}
	  } );
  
  } );
  </script> 
  
  
  
  <!--File Upload START-->
<link href="<?=HOST_URL?>/assets/admin/dist/css/uploadfile.min.css" rel="stylesheet">
<script>
 $( function() {
    $( "#tabs" ).tabs();
  } );
  
 
</script>
<style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url('<?=IMG_PATH?>/delete.jpg');
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
		clear:both;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
	#overlay_container {
		position:relative;
	}
	#overloading {
		background-image:url('<?=IMG_PATH?>/preloader/preloader_2.gif');
		background-repeat:no-repeat;
		background-position:center;
		background-color:#CCC;
		position:absolute;
		left:0px;
		top:0px;
		opacity: 0.3;
		z-index: 10000;
	}
	#div_listing_container {
		display:none;	
	}
	.d_d_text {
		color:#745156;
		font-size:20px;
			
	}
	.ajax-upload-dragdrop {
		margin:auto;
		margin-bottom:10px;
		width:700px !important;
	}
	.ajax-file-upload-statusbar {
		margin:auto;
		margin-top:10px;
	}
	.ajax-file-upload {
		height:31px;
	}
	
	
	 #tabs-1{  
	    overflow-y:scroll; overflow-x:none;
	}

    #tabs-2{
		overflow-y:scroll; overflow-x:none;
	}
				  
  .ui-tabs-active{
		border-color:#efca86  !important;
   }
					 
	.ui-tabs .ui-tabs-nav li {
		float:left;
		font-size: 16px;
        font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif;
  }
  label{
	  display: inline-block;
      font-weight: 700;
  }
  
  .ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button {
    font-family:"Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
    font-size: 14px;
}
  
  .ui-widget{
	 font-size: 16px;
     font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
  }
  .form-control{
	 font-size: 14px; 
  }
</style>
 
  
    
                    <div id="mid1" class="box box-success">
                        <div class="box-header">
                          <div style="width:20%; float:left;">
                          <h3 class="box-title">SEARCH</h3>
                          </div>
                          <div style="width:60%; float:left;"> 
                    	     <div class="col-sm-5">
                             <?php //echo $category_parent_list ?>
                   		      </div>
                             
                          </div>
                        </div>  
                     </div>     
    
            <!--Listing-->
                    <div id="mid1_list" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Teachers Group</h3>
                          <div class="box-tools">
                            <?php if (count($rs_all_groups)>0) { echo $paging_string;}?>	
                            <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
                            <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>
                          </div>
                        </div>
                        
                        <div class="box-body">
                     <!--   <div style="color:#030; font-weight:bold;">You can sort students by using drag and drop of rows </div>-->
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle"><input id="select_all" type="checkbox" value="" /></th>
                              <!--<th width="10%" align="center" valign="middle">Sl No.</th>-->
                              <th width="25%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y">Group Name <?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?><div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>
                              <th width="35%" align="center" valign="middle">Teachers</th>
                              <th width="10%" align="center" valign="middle">Date</th>
                              <th width="10%" align="center" valign="middle">Status</th>
                              <th width="10%" align="center" valign="middle">Action</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                                for ($i=0; $i<count($rs_all_groups); $i++) { 
                                    $id                         = $rs_all_groups[$i]['id'];
                                    $tbl_teacher_group_id       = $rs_all_groups[$i]['tbl_teacher_group_id'];
                                    $group_name_en              = $rs_all_groups[$i]['group_name_en'];
									$group_name_ar              = $rs_all_groups[$i]['group_name_ar'];
									$teachers_list	          = $rs_all_groups[$i]['teachers_list'];
									$cntTeacher	             = $rs_all_groups[$i]['cntTeacher'];
                                    $added_date                 = $rs_all_groups[$i]['added_date'];
                                    $is_active                  = $rs_all_groups[$i]['is_active'];
                                    $added_date = date('m-d-Y',strtotime($added_date));
									
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_teacher_group_id?>" >
                              <td align="left" valign="middle">
                              <span style="float:left;">
                              <input id="tbl_teacher_group_id" name="tbl_teacher_group_id" class="checkbox" type="checkbox" value="<?=$tbl_teacher_group_id?>" />
                              </span>
                              
                             <?php /*?> <span style="float:left;">&nbsp;
                              <?php if($i<>0){ ?> <i class="fa fa-arrow-up"  style="color:#3c8dbc; cursor:pointer;"  aria-hidden="true" title="Sorting - Drag & Drop To Up"></i> &nbsp; <?php } ?>
                               <?php if($i<> count($rs_all_categories)-1){ ?> <i class="fa fa-arrow-down" style="color:#3c8dbc;cursor:pointer;" aria-hidden="true" title="Sorting - Drag & Drop To Down"></i> <?php } ?>
                              </span><?php */?>
                              </td>
                             <!-- <td align="left" valign="middle"><?=$offset+$i+1?></td>-->
                              <td align="left" valign="middle">
                              <span style='float:left;'><?=$group_name_en?> </span><span style='float:right;'><?=$group_name_ar?></span>
                              
                              </td>
                              <td align="center" valign="middle" style="text-decoration:underline; cursor:pointer;"> <a onclick="openTeacherDiv('std_<?=$tbl_teacher_group_id?>');" > <?=$cntTeacher?>&nbsp;Teachers</a> <br />
                              <div id="std_<?=$tbl_teacher_group_id?>" style="display:none;">
                              <?php
                              for($t=0;$t<count($teachers_list);$t++)
									{
						echo "<span style='float:left;'>".$teachers_list[$t]['first_name']." ".$teachers_list[$t]['last_name']."</span><span style='float:right;'>".$teachers_list[$t]['first_name_ar']." ".$teachers_list[$t]['last_name_ar']."</span><br>";
									}
							  ?>
                              </div>
                              </td>
                             
                              <td align="left" valign="middle"><?=$added_date?></td>
                              <td align="left" valign="middle">
                                <div id="act_deact_<?=$tbl_teacher_group_id?>">
                                <?php if (trim($is_active) == "Y") { ?>
                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_teacher_group_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                <?php } else { ?>
                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_teacher_group_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                <?php } ?>
                                </div>
                              </td>
                              <td align="left" valign="middle">
                                <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/message/edit_teacher_group/tbl_teacher_group_id/<?=$tbl_teacher_group_id ?>"><button class="btn bg-purple fa fa-pencil" type="button" title="Edit"></button></a>
                              </td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="10" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_all_groups)<=0) {
                            ?>
                            <tr>
                              <td colspan="10" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no messages available. Click on the + button to add group.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>  
               <script>
			function openTeacherDiv(divId)
			{
			   if($("#"+divId).is(":visible"))
			   {
				    $("#"+divId).hide();
			   }else{
				   $("#"+divId).show();
			   }
			}
			</script>             
            <!--/Listing-->
    
            <!--Add or Create-->
            <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Teacher Group</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
   	            <div class="box-body">
                    <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
                            
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="group_name_en">Group Name [En]<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10">
                            <input type="text" placeholder="Group Name [En]" id="group_name_en" name="group_name_en" class="form-control" value="">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="group_name_ar">Group Name [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10">
                            <input type="text" placeholder="Group Name [Ar]" id="group_name_ar" name="group_name_ar" class="form-control" value="" dir="rtl">
                          </div>
                        </div>
                        
                         <?php /*?> <div class="form-group">
                          <label class="col-sm-2 control-label" for="tbl_teacher_id">Teacher(s)<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10">
                              <select id="tbl_teacher_id" name="tbl_teacher_id" multiple size="10" style="padding:0px 5px 0px 2px; font-size:14px;">
                                    <?php for($i=0; $i<count($rs_all_teachers); $i++) { ?>
                                    <option value="<?=$rs_all_teachers[$i]["tbl_teacher_id"]?>"><?=$rs_all_teachers[$i]["first_name"]?> <?=$rs_all_teachers[$i]["last_name"]?> :: <?=$rs_all_teachers[$i]["first_name_ar"]?> <?=$rs_all_teachers[$i]["last_name_ar"]?> </option>
                                    <?php	} ?>
                            </select>
                          </div>
                        </div><?php */?>
                        
                        
                        <div class="form-group" id="all_student">
                        <label class="col-sm-2 control-label" for="tbl_teacher_id">Teacher(s)</label>
        
                          <div class="col-sm-10" id="divTeacher" style="height:400px; overflow-y:scroll;">
                          <div class"col-sm-10" >
	 
                        <?php  if(count($rs_all_teachers)>0){ ?>
                           <div style="padding-bottom:10px;background-color:#e8eaeb;" class="col-sm-12"> 
                           <input type="checkbox" value="" id="select_all_teacher" class="checkboxTeacher" >&nbsp;Select All</div>
                        <?php  } ?>
                        <?php
                             for($i=0; $i<count($rs_all_teachers); $i++) { 
                             ?>
                             <div class="col-sm-4" style="padding-top:10px; padding-bottom:10px; border:1px solid #CCC;"> 
								  <input id="tbl_teacher_id_<?=$i?>" name="tbl_teacher_id[]" class="checkboxTeacher" type="checkbox" value="<?=$rs_all_teachers[$i]["tbl_teacher_id"]?>"  />&nbsp;
								   <?=$rs_all_teachers[$i]["first_name"]?> <?=$rs_all_teachers[$i]["last_name"]?> :: <?=$rs_all_teachers[$i]["first_name_ar"]?> <?=$rs_all_teachers[$i]["last_name_ar"]?>
                             </div>
							<?php  
	                         }
							 ?>
	                          </div> 
                        </div>
                        </div>    
                        
                        
                        
                        
                     
                        <!-- /.box-body -->
                      <div class="box-footer">
                        <button class="btn btn-primary" type="button" onclick="ajax_validate()">Submit</button>
                        <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                      </div>
                      <!-- /.box-footer -->  
                      
            
           </form>
                </div>
           </div>
 <script>
 $('#select_all_teacher').on('click',function(){
	if(this.checked){
		$('.checkboxTeacher').each(function(){
			this.checked = true;
		});
		
	}else{
		 $('.checkboxTeacher').each(function(){
			this.checked = false;
		});
	}
  });
  </script>               
  
            <!--/Add or Create-->
                
        <!--/Admin Category Management-->

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>


<script src="<?=HOST_URL?>/assets/admin/dist/js/jquery.uploadfile.min.js"></script>
<script language="javascript">

var item_id = "<?=$teacher_id_enc?>";//Primary Key for a Form. 

function set_item_id(obj) {
	item_id = obj.value;
	get_files();	
}

$(document).ready(function() {
	var uploadObj = $("#advancedUpload").uploadFile({
		url:"<?=HOST_URL?>/file_mgmt/upload_the_file",
		multiple:true,
		autoSubmit:true,
		maxFileSize:130000,
		fileName:"myfile",
		formData: {"module_name":"teacher"},
		dynamicFormData: function() {
			var data = { item_id:item_id}
			return data;
		},
		showStatusAfterSuccess:false,
		dragDropStr: "<span class='d_d_text'>Optionally Drag and Drop the File to Upload.</span>",
		abortStr:"Abourt",
		cancelStr:"Cancel",
		doneStr:"Done",
		multiDragErrorStr: "Multi Drag Error.",
		extErrorStr:"Extention Error:",
		sizeErrorStr:"Max Size Error:",
		uploadErrorStr:"Upload Error",
		onSelect:function(files) {
 		},
		onSubmit:function(files) {
 		},
		onSuccess:function(files, data, xhr) {
			if (data == "error") {
				alert("Error uploading file. Please try again.");
				return;
			}
			var obj = JSON.parse(data);
			var tbl_uploads_id = obj.tbl_uploads_id;
			var file_name_updated = obj.file_name_updated;
			
			//alert("tbl_uploads_id: "+tbl_uploads_id)
			//alert("file_name_updated: "+file_name_updated)
			add_uploaded_item(tbl_uploads_id, file_name_updated);
		},
		afterUploadAll:function() {
 		},
		onError: function(files, status, errMsg) {
 		}
	});

	$("#startUpload").click(function() {
		uploadObj.startUpload();
	});
	
	try { 
		$('input[type=file]').click();
	} catch(e) {
		alert(e)
	}
});

//Function called when file is uploaded
function add_uploaded_item(tbl_uploads_id, file_name_updated) {
	var str = "<div id='"+tbl_uploads_id+"' class='box-header with-border'> <div class='box-title'><img src='<?=IMG_PATH_TEACHER?>/"+file_name_updated+"' /></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_img_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
		
	$("#div_listing_container").show();
	$("#div_listing_container").append(str);
	$(".ajax-upload-dragdrop").hide();//Hide the upload button
return;
}

function confirm_delete_img_popup(tbl_uploads_id) {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		$('#'+tbl_uploads_id).hide();	
		$(".ajax-upload-dragdrop").show();
		
		var url_str = "<?=HOST_URL?>/file_mgmt/delete_file";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					tbl_uploads_id: tbl_uploads_id
				},
			success: function(data) {
				$("#pre-loader").hide();
			}
		});	
	} else {
		$("#pre-loader").hide();		
	}
}

function get_files() {
	var url_str = "<?=HOST_URL?>/misc/get_files.php";
	
	$.ajax({
		type: "POST",
		url: url_str,
		data: {
				module_name: "teacher",
				show_del: "Y",
				item_id: item_id//global variable
			},
		success: function(data) {
			$('#div_listing_container').show();
			$('#div_listing_container').html(data)
			
		}
	});	
}
</script>
<!--File Upload END-->
        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >
function search_data() {
		var tbl_category_id = $("#tbl_category_id").val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/all_categories/";
		if(tbl_category_id !='')
			url += "tbl_category_id/"+tbl_category_id+"/";
		
			url += "offset/0/";
		window.location.href = url;
		<?php /*?>window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/enquiry/all_enquiries/is_not_replied/"+is_not_replied+"/tbl_court_id/"+tbl_court_id+"/tbl_category_id/"+tbl_category_id;<?php */?>
	}
</script>