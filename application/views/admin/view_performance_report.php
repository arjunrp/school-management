<?php
//Init Parameters
$student_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}
$selDate = date("m/d/Y");
?>
 
<style>
.txt_en {
	text-align:left;
	padding-left:2px;
}
.txt_ar {
	text-align:right;
	padding-right:2px;	
	direction:rtl;		
}
</style>

<script type="text/javascript" src="<?=JS_PATH?>/date_time_picker/js/jquery-ui-1.8.6.custom.min.js"></script>
<script type="text/javascript" src="<?=JS_PATH?>/date_time_picker/js/jquery-ui-timepicker-addon.js"></script>
<script language="javascript">
	$(document).ready(function(){
		$('#attendance_date').datepicker({
			dateFormat: 'yyyy-mm-dd',
			timeFormat: 'hh:mm tt',
			timeOnly: true   
		});


	});
</script>

<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_students_list(tbl_class_id,attendance_date) {
		
		
		var tbl_academic_year = $("#tbl_academic_year").val();
		var tbl_semester_id   = $("#tbl_semester_id").val();
		
		var gender            = $("#gender").val();
		var tbl_country_id    = $("#tbl_country_id").val();
		var tbl_student_id    = $("#tbl_student_id").val();
		var tbl_teacher_id    = $("#tbl_teacher_id").val();
		
		
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/attendance_reports_list/";
		
			
		if(tbl_semester_id !='')
			url += "tbl_semester_id/"+tbl_semester_id+"/";
			
		if(tbl_class_id !='')
			url += "tbl_class_id/"+tbl_class_id+"/";
			
	
		if(tbl_student_id !='')
			url += "tbl_student_id/"+tbl_student_id+"/";
			
		if(attendance_date !='')
			url += "attendance_date/"+attendance_date+"/";
			
		if(tbl_teacher_id !='')
			url += "tbl_teacher_id/"+tbl_teacher_id+"/";

			url += "offset/0/";
		window.location.href = url;	
		
	}
	
	function show_listing() {
		$('#mid2').hide();
		$('#mid1_list').show(500);
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/school_attendance_reports";
			}
		})
	});
	
</script>
	
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper" >
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Students Performance Reports</h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Students</li>
    Performance
    </ol>
    <!--/BREADCRUMB--> 
  <?php /*?>  <div style=" float:right; "> <button onclick="show_student_cards()" title="Student Cards" type="button" class="btn btn-primary">Student Cards</button></div>
    <div style=" float:right; padding-right:10px;"> <button onclick="show_student_points()" title="Student Points" type="button" class="btn btn-primary">Student Points</button></div><?php */?>
    <div style="clear:both"></div>
  </section>
      <link href="<?=HOST_URL?>/assets/admin/dist/css/jquery-ui.css" rel="stylesheet">
      <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-1.11.1.js"></script>
      <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-ui.js"></script>
      <link href="<?=HOST_URL?>/assets/admin/dist/css/uploadfile.min.css" rel="stylesheet">
 <script>
   function show_student_cards()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/school_reports/";
		window.location.href = url;
	}
	 function show_student_points()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/school_point_reports/";
		window.location.href = url;
	}
 </script>
      
 <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {
			
	
	?>
        <!--Edit-->
   <script>
 $( function() {
    $( "#tabs" ).tabs();
  } );
  
 
</script>

        
              
		        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/student/all_students";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
    
  
 <link href="<?=HOST_URL?>/assets/admin/dist/css/jquery-ui.css" rel="stylesheet">
 <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-1.11.1.js"></script>
 <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-ui.js"></script>
  <script>
  $( function() {
		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
				
				/* var order = $("#tabledivbody").sortable("serialize");
				
				alert(order);
				
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/your/url/here'
				});*/
				
				
				
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	
				
				
				
				
				
			}
	  } );
  
  } );
  </script> 
  
  
  
  <!--File Upload START-->
<link href="<?=HOST_URL?>/assets/admin/dist/css/uploadfile.min.css" rel="stylesheet">
<script>
 $( function() {
    $( "#tabs" ).tabs();
  } );
  
 
</script>
<style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url('<?=IMG_PATH?>/delete.jpg');
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
		clear:both;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
	#overlay_container {
		position:relative;
	}
	#overloading {
		background-image:url('<?=IMG_PATH?>/preloader/preloader_2.gif');
		background-repeat:no-repeat;
		background-position:center;
		background-color:#CCC;
		position:absolute;
		left:0px;
		top:0px;
		opacity: 0.3;
		z-index: 10000;
	}
	#div_listing_container {
		display:none;	
	}
	.d_d_text {
		color:#745156;
		font-size:20px;
			
	}
	.ajax-upload-dragdrop {
		margin:auto;
		margin-bottom:10px;
		width:700px !important;
	}
	.ajax-file-upload-statusbar {
		margin:auto;
		margin-top:10px;
	}
	.ajax-file-upload {
		height:31px;
	}
	
	
	 #tabs-1{  
	    overflow-y:scroll; overflow-x:none;
	}

    #tabs-2{
		overflow-y:scroll; overflow-x:none;
	}
				  
  .ui-tabs-active{
		border-color:#efca86  !important;
   }
					 
	.ui-tabs .ui-tabs-nav li {
		float:left;
		font-size: 16px;
        font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif;
  }
  label{
	  display: inline-block;
      font-weight: 700;
  }
  
  .ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button {
    font-family:"Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
    font-size: 14px;
}
  
  .ui-widget{
	 font-size: 16px;
     font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
  }
  .form-control{
	 font-size: 14px; 
  }
</style>
                  
    
                    <div id="mid1" class="box box-success">
                        <div class="box-header">
                          <div class="col-sm-11"> 
                          <div class="col-sm-1" >
                          <h3 class="box-title">SEARCH</h3>
                          </div>
                          </div>
                          <div class="col-sm-11"> &nbsp;</div>
                          <div class="col-sm-11"> 
                              <?php /*?> <div class="col-sm-3"> 
                             
                              <select name="tbl_academic_year" id="tbl_academic_year" class="form-control" onChange="get_semester_ajax()" >
                              <option value="">--Select Academic Year --</option>
							  
							  <?php
                                    for ($u=0; $u<count($academic_list); $u++) { 
                                        $tbl_academic_year_id  = $academic_list[$u]['tbl_academic_year_id'];
                                        $academic_start        = $academic_list[$u]['academic_start'];
                                        $academic_end          = $academic_list[$u]['academic_end'];
                                        if($tbl_sel_academic_year == $tbl_academic_year_id)
                                           $selAcademic = "selected";
                                         else
                                           $selAcademic = "";
                                  ?>
                                      <option value="<?=$tbl_academic_year_id?>"  <?=$selAcademic?> >
                                      <?=$academic_start?>&nbsp;-&nbsp;<?=$academic_end?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>   
                               </div>
                               
                                <div class="col-sm-3" id="divSemester"> 
                             
                              <select name="tbl_semester_id" id="tbl_semester_id" class="form-control">
                              <option value="">--Select Semester--</option>
							  
							  <?php
                                    for ($u=0; $u<count($rs_all_semesters); $u++) { 
                                        $tbl_semester_id_u = $rs_all_semesters[$u]['tbl_semester_id'];
                                        $title             = $rs_all_semesters[$u]['title'];
                                        $title_ar          = $rs_all_semesters[$u]['title_ar'];
										$duration          = $rs_all_semesters[$u]['start_date']." - ".$rs_all_semesters[$u]['end_date'];
										
                                        if($tbl_sel_semester_id == $tbl_semester_id_u)
                                           $selSemester = "selected";
                                         else
                                           $selSemester = "";
                                  ?>
                                      <option value="<?=$tbl_semester_id_u?>"  <?=$selSemester?>  >
                                     <?=$title_ar?>&nbsp;[::]&nbsp; <?=$title?>&nbsp; / &nbsp;<?=$duration?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>     
                              </div><?php */?>
                              <div class="col-sm-3"> 
                             
                              <select name="tbl_class_id" id="tbl_class_id" class="form-control" onChange="get_students_ajax()" >
                              <option value="">--Select Class --</option>
							  
							  <?php
                                    for ($u=0; $u<count($classes_list); $u++) { 
                                        $tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
                                        $class_name             = $classes_list[$u]['class_name'];
                                        $class_name_ar          = $classes_list[$u]['class_name_ar'];
										$section_name           = $classes_list[$u]['section_name'];
                                        $section_name_ar        = $classes_list[$u]['section_name_ar'];
                                        if($tbl_sel_class_id == $tbl_class_id_u)
                                           $selClass = "selected";
                                         else
                                           $selClass = "";
                                  ?>
                                      <option value="<?=$tbl_class_id_u?>"  <?=$selClass?>  >
                                      <?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;
                                    <?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>   
                               </div>
                               
                             
                               
                              <div class="col-sm-3" id="divStudent" > 
                             
                              <select name="tbl_student_id" id="tbl_student_id" class="form-control">
                                <option value="">--Select Student--</option>
                                  <?php
                                    for ($u=0; $u<count($rs_all_students); $u++) { 
                                        $tbl_student_id_u         = $rs_all_students[$u]['tbl_student_id'];
                                        $name                     = $rs_all_students[$u]['first_name']." ".$rs_all_students[$u]['last_name'];
										$name_ar                    = $rs_all_students[$u]['first_name_ar']." ".$rs_all_students[$u]['last_name_ar'];
                                        if($tbl_sel_student_id == $tbl_student_id_u)
                                           $selStudent = "selected";
                                         else
                                           $selStudent = "";
                                  ?>
                                      <option value="<?=$tbl_student_id_u?>"  <?=$selStudent?>  >
                                      <?=$name?>[::]<?=$name_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                                
                               </select>     
                              </div>  
                                <div class="col-sm-3"><button class="btn btn-success" type="button" onClick="search_data()">Search</button>&nbsp;<button class="btn btn-success" type="button" 
                               onclick="reset_data();">Reset</button>
                               </div>
                             
                             
                               </div>
                               
                               <div class="col-sm-11">&nbsp;</div>
                               
                                <?php /*?><div class="col-sm-11"> 
                                
                            
                                
                                 <div class="col-sm-3" id="divStudent" > 
                             
                              <select name="tbl_student_id" id="tbl_student_id" class="form-control">
                                <option value="">--Select Student--</option>
                                  <?php
                                    for ($u=0; $u<count($rs_all_students); $u++) { 
                                        $tbl_student_id_u         = $rs_all_students[$u]['tbl_student_id'];
                                        $name                     = $rs_all_students[$u]['first_name']." ".$rs_all_students[$u]['last_name'];
										$name_ar                    = $rs_all_students[$u]['first_name_ar']." ".$rs_all_students[$u]['last_name_ar'];
                                        if($tbl_sel_student_id == $tbl_student_id_u)
                                           $selStudent = "selected";
                                         else
                                           $selStudent = "";
                                  ?>
                                      <option value="<?=$tbl_student_id_u?>"  <?=$selStudent?>  >
                                      <?=$name?>[::]<?=$name_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                                
                               </select>     
                              </div>  
                                
                                
                          
                               <div class="col-sm-3"> 
                             
                              <select name="tbl_teacher_id" id="tbl_teacher_id" class="form-control">
                              <option value="">--Select Teacher --</option>
							  
							  <?php
                                    for ($u=0; $u<count($rs_all_teachers); $u++) { 
                                        $tbl_teacher_id_u        = $rs_all_teachers[$u]['tbl_teacher_id'];
                                        $name                  = $rs_all_teachers[$u]['first_name']." ".$rs_all_teachers[$u]['last_name'];
                                        $name_ar                  = $rs_all_teachers[$u]['first_name_ar']." ".$rs_all_teachers[$u]['last_name_ar'];
                                        if($tbl_sel_teacher_id == $tbl_teacher_id_u)
                                           $selTeacher = "selected";
                                         else
                                           $selTeacher = "";
                                  ?>
                                      <option value="<?=$tbl_teacher_id_u?>"  <?=$selTeacher?> >
                                      <?=$name?>&nbsp;[::]&nbsp;<?=$name_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>   
                               </div>
                               
                               <div class="col-sm-3"><button class="btn btn-success" type="button" onClick="search_data()">Search</button>&nbsp;<button class="btn btn-success" type="button" 
                               onclick="reset_data();">Reset</button>
                               </div>
                               </div><?php */?>
                           
                          </div>
                        </div>  
                          
                   <link id="theme-style" rel="stylesheet" href="<?=CSS_WEB_PATH?>/progress_report/styles_admin.css">
                   

            <!--Listing-->
     <?php
	    if($tbl_sel_student_id<>"")
		{
			$display = "block";
		}else{
			
			$display = "none";
		}
	 
	 ?>
     <?php $colorCode = array("#f56954","#00a65a","#f39c12","#00c0ef","#3c8dbc","#d2d6de","#f56954","#00a65a","#f39c12","#00c0ef","#3c8dbc","#d2d6de"); ?>
        
   <style>
    .img-circle {
        border-radius: 50%;
    }
    .score_box {
        width: 40px;
        height: 40px;
        border-radius: 50px;
        text-align: center;
        padding-top: 8px;
        color: #FFFFFF;
    }
    </style>
      
      <div id="mid1_list" class="box" style="display:<?=$display?>;" >
      
       <?php
	    if(!empty($performance_activities))
		{
			$displayPerformance = "block";
		}else{
			
			$displayPerformance = "none";
		}
	 
	 ?>
      
      
      
      
      <div class="col-sm-12" style="display:<?=$displayPerformance?>;" >
      <div class="col-sm-6">     
                        <div class="box-body">
                      
                      <?php  if(!empty($performance_activities)) { ?>
                        <section class="skills-section section">
                            <h3 class="section-title"><i class="fa fa-rocket"></i>&nbsp;Skills &amp; Proficiency</h3>
                            <div class="skillset" >        
                              <?php for($p=0;$p<count($performance_activities);$p++){ ?> 
                                <div class="item">
                                    <h3 class="level-title" style="float:left; width: 30%;"><?php echo $performance_activities[$p]['title']; ?></h3>
                                    <div class="level-bar" style="top:0px !important; width: 55%; float:left;">
                                        <div class="level-bar-inner" style="background-color:<?=$colorCode[$p]?>;" data-level="<?php echo $performance_activities[$p]['performance_value']; ?>%" >
                                        </div> 
                                    </div><!--//level-bar-->   
                                    <div style="float:left;width: 5%;">
                                    <span style="padding-left:100%; color:<?=$colorCode[$p]?>;" > <?php echo $performance_activities[$p]['performance_value']; ?>%</span>  <!--color:#F60;-->
                                    </div>
                                    
                                                                  
                                </div><!--//item-->
                                <?php } ?>
                            </div>  
                        </section><!--//skills-section-->
                     <?php } ?>   
                          
                        </div>
                    </div>    
                    
                    
      <div class="box box-primary" style="display:none;">
            <div class="box-header with-border">
               <?php if(LAN_SEL=="ar"){?> 
                    <h3 class="box-title">  الإستفسارات والردود </h3>
               <?php }else{ ?>
                     <h3 class="box-title"><!--Area Chart - -->Skills &amp; Proficiency vs Months</h3>
              <?php } ?>
              <div class="box-tools pull-right">
               <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
              </div>
            </div>
            <div class="box-body">
                <canvas id="areaChart" style="height:250px"></canvas>
            </div>
            <!-- /.box-body -->
         </div>
         
      
      <div class="col-sm-6">            
                  <div class="box box-primary" style="margin-top:20px;">
                    <div class="box-header with-border">
                       <?php if(LAN_SEL=="ar"){?> 
                            <h3 class="box-title">  الإستفسارات والردود </h3>
                       <?php }else{ ?>
                             <h3 class="box-title">Skills &amp; Proficiency vs Months</h3>
                      <?php } ?>
                      <div class="box-tools pull-right">
                      </div>
                    </div>
                    <div class="box-body">
                        <canvas id="lineChart1" style="height:250px"></canvas>
                    </div>
                    <!-- /.box-body -->
                 </div>
                
         <!-- class="sm-6" -->   
       </div>  
      </div>
      <!--/Listing-->
      
      
     <?php
	    if(!empty($rs_student_attendance))
		{
			$displayAttendance = "block";
		}else{
			
			$displayAttendance = "none";
		}
	 ?>
      
      <div class="col-sm-12" style="display:<?=$displayAttendance?>;" >      
      <div class="col-sm-6">           
            
                 <?php  if(!empty($rs_student_attendance)) { ?>
            <section class="skills-section section">
                <h3 class="section-title"><i class="fa fa-calendar"></i>&nbsp;Attendance</h3>
                <div class="skillset">        
                     
                     
                     
                     <div style="float:left; width:35%" >
                       
                         <div style="float:left; padding:10px;">
                            <div class="score_box" style="background-color:#2d7788; text-align:center;"><?=$rs_student_attendance['total_count']; ?></div>
                            <h3 style="font-size:14px;">Working Days</h3>
                         </div><!--//level-bar-->       
                    </div><!--//item-->
                    
                    <div style="float:left; width:35%">
                        
                        <div style="float:left; padding:10px;">
                            <div class="score_box" style="background-color:#0a7429; text-align:center;"><?=$rs_student_attendance['present_count']; ?></div>
                            <h3 style="font-size:14px;" >Total Present</h3>
                         </div> <!--//level-bar-->       
                    </div>
                    
                    <div style="float:left; width:30%">
                       
                        <div style="float:left; padding:10px;">
                             <div class="score_box" style="background-color:#fa2307; text-align:center;"><?=$rs_student_attendance['absent_count']; ?></div>
                             <h3 style="font-size:14px;" >Total Absent</h3>
                         </div> <!--//level-bar-->       
                    </div>
                </div>  
                <div style="clear:both;"></div>
            </section><!--//skills-section-->
         <?php } ?> 
  </div>
  
  
      <div class="col-sm-6">            
         
          <div class="box box-primary">
          <div class="box-header with-border">
              <h3 class="box-title">Attendance vs  Months</h3>

              <div class="box-tools pull-right">
             <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
              </div>
            </div>
            <div class="box-body">
                <canvas id="barChart" style="height:250px"></canvas>
            </div>
          </div>
            
    <!-- class="sm-6" --> </div>  
      </div> 
      
       <?php
	    if(!empty($rs_student_point))
		{
			$displayPoint = "block";
		}else{
			
			$displayPoint = "none";
		}
	 ?>
    
      <div class="col-sm-12" style="display:<?=$displayPoint?>;" >
      <div class="col-sm-6">   
             <?php  if(!empty($rs_student_point)) { ?>
            <section class="skills-section section">
                <h3 class="section-title" style="float:left;"><i class="fa fa-star-o"></i>&nbsp;Points Earned </h3> <span style=" padding:10px; padding-right:20px; float:right;font-size:18px; color:#F90;"><strong>Total Point :&nbsp;<?=$rs_student_total_point?></strong></span>
                <div style="clear:both;"></div>
                <div class="skillset">        
                  <?php for($s=0;$s<count($rs_student_point);$s++){ ?> 
                     <div class="item">
                        <div width="30%" style="float:left; padding-right:30px;">
						<div class="score_box" style="background-color:<?=$colorCode[$s]?>;"><?=$rs_student_point[$s]["student_point"]?></div>
						</div>
                        <div width="70%" style="float:left; padding-top:10px;">
							<?php echo $rs_student_point[$s]['comments_student']; ?>
                         </div> <!--//level-bar-->       
                    </div><!--//item-->
                    <?php } ?>
                </div>  
            </section><!--//skills-section-->
         <?php } ?> 
          <div style="clear:both;"></div>
     </div>
     
     
      <div class="col-sm-6">             
          <!-- <div class="box box-primary" style="display:none;">
            <div class="box-header with-border">
               <?php if(LAN_SEL=="ar"){?> 
                    <h3 class="box-title">  الإستفسارات والردود </h3>
               <?php }else{ ?>
                     <h3 class="box-title">Area Chart - Earned Points vs Months</h3>
              <?php } ?>
              <div class="box-tools pull-right">
              </div>
            </div>
            <div class="box-body">
                <canvas id="areaChart2" style="height:250px"></canvas>
            </div>
         </div>-->
         
         
          <div class="box box-primary">
            <div class="box-header with-border">
               <?php if(LAN_SEL=="ar"){?> 
                    <h3 class="box-title">  الإستفسارات والردود </h3>
               <?php }else{ ?>
                     <h3 class="box-title">Earned Points vs Months</h3>
              <?php } ?>
              <div class="box-tools pull-right">
               <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
              </div>
            </div>
            <div class="box-body">
                <canvas id="lineChart" style="height:350px"></canvas>
            </div>
            <!-- /.box-body -->
         </div>
   <div style="clear:both;"></div>
 </div>
 
      </div>
 
         <?php
	    if(!empty($cards))
		{
			$displayCards = "block";
		}else{
			
			$displayCards = "none";
		}
	 ?>
      <div class="col-sm-12" style="display:<?=$displayCards?>;"  >
      <div class="col-sm-6">      
             <?php  if(!empty($cards)) { ?>
            <section class="skills-section section">
                <h3 class="section-title"><i class="fa fa-file "></i>&nbsp;Cards &amp; Issued</h3>
                <div class="skillset">        
                  <?php for($c=0;$c<count($cards);$c++){ ?> 
                     <div class="item">
                        <div width="30%" style="float:left; padding-right:30px;">
						<div class="score_box" style="background-color:<?=$cards[$c]["color"]?>"><?=abs($cards[$c]["score"])?></div>
						</div>
                        <div width="70%" style="float:left; padding-top:10px;">
							<?php echo $cards[$c]['title']; ?>
                         </div> <!--//level-bar-->       
                    </div><!--//item-->
                    <?php } ?>
                </div>  
            </section><!--//skills-section-->
         <?php } ?>   
</div>


      <div class="col-sm-6">                 
     
          
            <div class="box box-primary">
              <div class="box-header with-border">
               <?php if(LAN_SEL=="ar"){?> 
                    <h3 class="box-title">  الإستفسارات والردود </h3>
               <?php }else{ ?>
                     <h3 class="box-title">Issued Cards</h3>
              <?php } ?>

              <div class="box-tools pull-right">
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="chart-responsive">
                    <canvas id="pieChart" height="160" style="width: 225px; height: 160px;" width="225"></canvas>
                  </div>
                  <!-- ./chart-responsive -->
                </div>
                
              
         </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
           
           
	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>
    </div>
    </div>
      </div> 
    
      <div style="clear:both;"></div>
      </div>
    <!--/WORKING AREA--> 
  </section>
  <div style="clear:both;"></div>
</div>
   
<script language="javascript" >
function search_data() {
		
		var tbl_academic_year = $("#tbl_academic_year").val();
		var tbl_semester_id   = $("#tbl_semester_id").val();
		var tbl_class_id      = $("#tbl_class_id").val();
		var tbl_student_id    = $("#tbl_student_id").val();
		var tbl_teacher_id    = $("#tbl_teacher_id").val();
		
		
	
		
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/student_performance_report/";
		if(tbl_academic_year !='')
			url += "tbl_academic_year/"+tbl_academic_year+"/";
		if(tbl_semester_id !='')
			url += "tbl_semester_id/"+tbl_semester_id+"/";
		if(tbl_class_id !='')
			url += "tbl_class_id/"+tbl_class_id+"/";
		if(tbl_student_id !='')
			url += "tbl_student_id/"+tbl_student_id+"/";
		if(tbl_teacher_id !='')
			url += "tbl_teacher_id/"+tbl_teacher_id+"/";	
		
			url += "offset/0/";
		window.location.href = url;
		<?php /*?>window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/enquiry/all_enquiries/is_not_replied/"+is_not_replied+"/tbl_court_id/"+tbl_court_id+"/tbl_category_id/"+tbl_category_id;<?php */?>
	}

function reset_data() {
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/reports/student_performance_report/";
		url += "offset/0/";
		window.location.href = url;
	}
	
function get_students_ajax() {
		//show_loading();
		var class_ids = $("#tbl_class_id").val();
		 var tbl_class_id='';
        	$('#tbl_class_id :selected').each(function(i, selected) {
            	tbl_class_id += $(selected).val()+",";
        	});
		//alert(tbl_class_id);
		//return;
		var xmlHttp, rnd, url, search_param, ajax_timer;
		rnd = Math.floor(Math.random()*11);
		try{		
			xmlHttp = new XMLHttpRequest(); 
		}catch(e) {
			try{
				xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
			}catch(e) {
				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
				hide_loading();
			}
		}

		//AJAX response
		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
				//ajax_timer.stop();
				var data = xmlHttp.responseText;
				$("#divStudent").html(data);
				//$("#tbl_student_dropdown").multiselect('refresh');
				return;
			}
		}

		/*ajax_timer = $.timer(function() {
			xmlHttp.abort();
			alert(connectivity_msg);
			ajax_timer.stop();
		},connectivity_timeout_time,true);*/

		//Sending AJAX request
		url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/students_against_classes/selBox/1/tbl_class_id/"+tbl_class_id+"/rnd/"+rnd;
		xmlHttp.open("POST",url,true);
		xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlHttp.send("rnd="+rnd);
	}
	
	
	function get_semester_ajax() {
		//show_loading();
		var tbl_academic_year = $("#tbl_academic_year").val();
		
		//alert(tbl_class_id);
		//return;

		var xmlHttp, rnd, url, search_param, ajax_timer;
		rnd = Math.floor(Math.random()*11);
		try{		
			xmlHttp = new XMLHttpRequest(); 
		}catch(e) {
			try{
				xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
			}catch(e) {
				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
				hide_loading();
			}
		}

		//AJAX response
		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
				//ajax_timer.stop();
				var data = xmlHttp.responseText;
				$("#divSemester").html(data);
				//$("#tbl_student_dropdown").multiselect('refresh');
				return;
			}
		}

		/*ajax_timer = $.timer(function() {
			xmlHttp.abort();
			alert(connectivity_msg);
			ajax_timer.stop();
		},connectivity_timeout_time,true);*/

		//Sending AJAX request
		url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/semesters_against_academicyear/tbl_academic_year/"+tbl_academic_year+"/rnd/"+rnd;
		xmlHttp.open("POST",url,true);
		xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlHttp.send("rnd="+rnd);
	}
</script>

 <!-- Javascript -->          
    <script type="text/javascript" src="<?=JS_WEB_PATH?>/plugins/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="<?=JS_WEB_PATH?>/plugins/bootstrap/js/bootstrap.min.js"></script>    
    <script type="text/javascript" src="<?=JS_WEB_PATH?>/plugins/main.js"></script>    
    <!-- Charts -->
    <script src="<?=HOST_URL?>/assets/admin/plugins/chartjs/Chart.min.js"></script>
    <script>
 	 $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

     //-------------
    //- BAR CHART -
    //-------------
	
	
	
    //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var areaChart = new Chart(areaChartCanvas);
  

	var areaChartData = {
      labels: ["<?=$months['11']?>", "<?=$months['10']?>", "<?=$months['9']?>", "<?=$months['8']?>", "<?=$months['7']?>", "<?=$months['6']?>", "<?=$months['5']?>", "<?=$months['4']?>", "<?=$months['3']?>", "<?=$months['2']?>", "<?=$months['1']?>", "<?=$months['0']?>"],
      datasets: [
        <?php for($m=0;$m<count($performance_graph);$m++){ 
		      $performance_array = array();
		      $performance_array = $performance_graph[$m]['performance_data'];
			  
			  if($performance_array[11]['performance_value']=='1000'){ $performance_value_11 = ''; }else{ $performance_value_11 = $performance_array[11]['performance_value']; } 
		      if($performance_array[10]['performance_value']=='1000'){ $performance_value_10 = ''; }else{ $performance_value_10 = $performance_array[10]['performance_value']; }  
			  if($performance_array[9]['performance_value']=='1000'){ $performance_value_9   = ''; }else{ $performance_value_9 = $performance_array[9]['performance_value']; }  
			  if($performance_array[8]['performance_value']=='1000'){ $performance_value_8 = ''; }else{ $performance_value_8 = $performance_array[8]['performance_value']; }  
			  if($performance_array[7]['performance_value']=='1000'){ $performance_value_7 = ''; }else{ $performance_value_7 = $performance_array[7]['performance_value']; }  
			  if($performance_array[6]['performance_value']=='1000'){ $performance_value_6 = ''; }else{ $performance_value_6 = $performance_array[6]['performance_value']; }  
			  if($performance_array[5]['performance_value']=='1000'){ $performance_value_5 = ''; }else{ $performance_value_5 = $performance_array[5]['performance_value']; }  
			  if($performance_array[4]['performance_value']=='1000'){ $performance_value_4 = ''; }else{ $performance_value_4 = $performance_array[4]['performance_value']; }  
			  if($performance_array[3]['performance_value']=='1000'){ $performance_value_3 = ''; }else{ $performance_value_3 = $performance_array[3]['performance_value']; }  
			  if($performance_array[2]['performance_value']=='1000'){ $performance_value_2 = ''; }else{ $performance_value_2 = $performance_array[2]['performance_value']; }  
			  if($performance_array[1]['performance_value']=='1000'){ $performance_value_1 = ''; }else{ $performance_value_1 = $performance_array[1]['performance_value']; }  
			  if($performance_array[0]['performance_value']=='1000'){ $performance_value_0 = ''; }else{ $performance_value_0 = $performance_array[0]['performance_value']; }  
		
		  ?>
		{
		  label: "<?=$performance_graph[$m]['topic_en']?>",
          fillColor: "<?=$colorCode[$m]?>",
          strokeColor: "<?=$colorCode[$m]?>",
          pointColor: "<?=$colorCode[$m]?>",
          pointStrokeColor: "<?=$colorCode[$m]?>",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "<?=$colorCode[$m]?>",
          data: [<?=$performance_value_11?>,
		        <?=$performance_value_10?>,
				<?=$performance_value_9?>,
				<?=$performance_value_8?>,
				<?=$performance_value_7?>,
				<?=$performance_value_6?>,
				<?=$performance_value_5?>,
				<?=$performance_value_4?>,
				<?=$performance_value_3?>,
				<?=$performance_value_2?>,
				<?=$performance_value_1?>,
				<?=$performance_value_0?>]
        }
		<?php if($m<>count($performance_graph)-1){?>
		,
		<?php } 
		} ?>
      ]
    };
	
	
	

    var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: false,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - Whether the line is curved between points
      bezierCurve: true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension: 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot: false,
      //Number - Radius of each point dot in pixels
      pointDotRadius: 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth: 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius: 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke: true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth: 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true
    };
    //Create the line chart
    areaChart.Line(areaChartData, areaChartOptions);

   
     //-------------
    //- LINE CHART -
    //--------------
	var lineChartCanvas = $("#lineChart1").get(0).getContext("2d");
    var lineChart = new Chart(lineChartCanvas);
    var lineChartOptions = areaChartOptions;
    lineChartOptions.datasetFill = false;
    lineChart.Line(areaChartData, lineChartOptions);
   //Attendance Section
    
		var areaChartData1 = {
      labels: ["<?=$months['11']?>", "<?=$months['10']?>", "<?=$months['9']?>", "<?=$months['8']?>", "<?=$months['7']?>", "<?=$months['6']?>", "<?=$months['5']?>", "<?=$months['4']?>", "<?=$months['3']?>", "<?=$months['2']?>", "<?=$months['1']?>", "<?=$months['0']?>"],
      datasets: [
			 <?php if($rs_student_attendance_graph[11]['total_count']=='1000'){ $total_month_11 = ''; }else{ $total_month_11= $rs_student_attendance_graph[11]['total_count']; } 
		      if($rs_student_attendance_graph[10]['total_count']=='1000'){ $total_month_10 = ''; }else{ $total_month_10 	= $rs_student_attendance_graph[10]['total_count']; }  
			  if($rs_student_attendance_graph[9]['total_count']=='1000'){ $total_month_9   = ''; }else{ $total_month_9 	    = $rs_student_attendance_graph[9]['total_count']; }  
			  if($rs_student_attendance_graph[8]['total_count']=='1000'){ $total_month_8 = ''; }else{ $total_month_8 		= $rs_student_attendance_graph[8]['total_count']; }  
			  if($rs_student_attendance_graph[7]['total_count']=='1000'){ $total_month_7 = ''; }else{ $total_month_7 		= $rs_student_attendance_graph[7]['total_count']; }  
			  if($rs_student_attendance_graph[6]['total_count']=='1000'){ $total_month_6 = ''; }else{ $total_month_6 		= $rs_student_attendance_graph[6]['total_count']; }  
			  if($rs_student_attendance_graph[5]['total_count']=='1000'){ $total_month_5 = ''; }else{ $total_month_5 		= $rs_student_attendance_graph[5]['total_count']; }  
			  if($rs_student_attendance_graph[4]['total_count']=='1000'){ $total_month_4 = ''; }else{ $total_month_4		= $rs_student_attendance_graph[4]['total_count']; }  
			  if($rs_student_attendance_graph[3]['total_count']=='1000'){ $total_month_3 = ''; }else{ $total_month_3 		= $rs_student_attendance_graph[3]['total_count']; }  
			  if($rs_student_attendance_graph[2]['total_count']=='1000'){ $total_month_2 = ''; }else{ $total_month_2 		= $rs_student_attendance_graph[2]['total_count']; }  
			  if($rs_student_attendance_graph[1]['total_count']=='1000'){ $total_month_1 = ''; }else{ $total_month_1 		= $rs_student_attendance_graph[1]['total_count']; }  
			  if($rs_student_attendance_graph[0]['total_count']=='1000'){ $total_month_0 = ''; }else{ $total_month_0 		= $rs_student_attendance_graph[0]['total_count']; }  
			  
			  if($rs_student_attendance_graph[11]['present_count']=='1000'){ $present_month_11 = ''; }else{ $present_month_11   = $rs_student_attendance_graph[11]['present_count']; } 
		      if($rs_student_attendance_graph[10]['present_count']=='1000'){ $present_month_10 = ''; }else{ $present_month_10 	= $rs_student_attendance_graph[10]['present_count']; }  
			  if($rs_student_attendance_graph[9]['present_count']=='1000'){ $present_month_9   = ''; }else{ $present_month_9 	= $rs_student_attendance_graph[9]['present_count']; }  
			  if($rs_student_attendance_graph[8]['present_count']=='1000'){ $present_month_8 = ''; }else{ $present_month_8 		= $rs_student_attendance_graph[8]['present_count']; }  
			  if($rs_student_attendance_graph[7]['present_count']=='1000'){ $present_month_7 = ''; }else{ $present_month_7 		= $rs_student_attendance_graph[7]['present_count']; }  
			  if($rs_student_attendance_graph[6]['present_count']=='1000'){ $present_month_6 = ''; }else{ $present_month_6 		= $rs_student_attendance_graph[6]['present_count']; }  
			  if($rs_student_attendance_graph[5]['present_count']=='1000'){ $present_month_5 = ''; }else{ $present_month_5 		= $rs_student_attendance_graph[5]['present_count']; }  
			  if($rs_student_attendance_graph[4]['present_count']=='1000'){ $present_month_4 = ''; }else{ $present_month_4		= $rs_student_attendance_graph[4]['present_count']; }  
			  if($rs_student_attendance_graph[3]['present_count']=='1000'){ $present_month_3 = ''; }else{ $present_month_3 		= $rs_student_attendance_graph[3]['present_count']; }  
			  if($rs_student_attendance_graph[2]['present_count']=='1000'){ $present_month_2 = ''; }else{ $present_month_2 		= $rs_student_attendance_graph[2]['present_count']; }  
			  if($rs_student_attendance_graph[1]['present_count']=='1000'){ $present_month_1 = ''; }else{ $present_month_1 		= $rs_student_attendance_graph[1]['present_count']; }  
			  if($rs_student_attendance_graph[0]['present_count']=='1000'){ $present_month_0 = ''; }else{ $present_month_0 		= $rs_student_attendance_graph[0]['present_count']; }  
			  
			    if($rs_student_attendance_graph[11]['absent_count']=='1000'){ $absent_month_11 = ''; }else{ $absent_month_11    = $rs_student_attendance_graph[11]['absent_count']; } 
		      if($rs_student_attendance_graph[10]['absent_count']=='1000'){ $absent_month_10 = ''; }else{ $absent_month_10 	    = $rs_student_attendance_graph[10]['absent_count']; }  
			  if($rs_student_attendance_graph[9]['absent_count']=='1000'){ $absent_month_9   = ''; }else{ $absent_month_9 	    = $rs_student_attendance_graph[9]['absent_count']; }  
			  if($rs_student_attendance_graph[8]['absent_count']=='1000'){ $absent_month_8 = ''; }else{ $absent_month_8 		= $rs_student_attendance_graph[8]['absent_count']; }  
			  if($rs_student_attendance_graph[7]['absent_count']=='1000'){ $absent_month_7 = ''; }else{ $absent_month_7 		= $rs_student_attendance_graph[7]['absent_count']; }  
			  if($rs_student_attendance_graph[6]['absent_count']=='1000'){ $absent_month_6 = ''; }else{ $absent_month_6 		= $rs_student_attendance_graph[6]['absent_count']; }  
			  if($rs_student_attendance_graph[5]['absent_count']=='1000'){ $absent_month_5 = ''; }else{ $absent_month_5 		= $rs_student_attendance_graph[5]['absent_count']; }  
			  if($rs_student_attendance_graph[4]['absent_count']=='1000'){ $absent_month_4 = ''; }else{ $absent_month_4		    = $rs_student_attendance_graph[4]['absent_count']; }  
			  if($rs_student_attendance_graph[3]['absent_count']=='1000'){ $absent_month_3 = ''; }else{ $absent_month_3 		= $rs_student_attendance_graph[3]['absent_count']; }  
			  if($rs_student_attendance_graph[2]['absent_count']=='1000'){ $absent_month_2 = ''; }else{ $absent_month_2 		= $rs_student_attendance_graph[2]['absent_count']; }  
			  if($rs_student_attendance_graph[1]['absent_count']=='1000'){ $absent_month_1 = ''; }else{ $absent_month_1 		= $rs_student_attendance_graph[1]['absent_count']; }  
			  if($rs_student_attendance_graph[0]['absent_count']=='1000'){ $absent_month_0 = ''; }else{ $absent_month_0 		= $rs_student_attendance_graph[0]['absent_count']; }  
			  
		
		  ?>
		{
		  label: "Total Working Days",
          fillColor: "#2d7788",
          strokeColor: "#2d7788",
          pointColor: "#2d7788",
          pointStrokeColor: "#2d7788",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "#2d7788",
          data: [<?=$total_month_11?>,
		        <?=$total_month_10?>,
				<?=$total_month_9?>,
				<?=$total_month_8?>,
				<?=$total_month_7?>,
				<?=$total_month_6?>,
				<?=$total_month_5?>,
				<?=$total_month_4?>,
				<?=$total_month_3?>,
				<?=$total_month_2?>,
				<?=$total_month_1?>,
				<?=$total_month_0?>]
        },
		{
		  label: "Total Presents",
          fillColor: "#0a7429",
          strokeColor: "#0a7429",
          pointColor: "#0a7429",
          pointStrokeColor: "#0a7429",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "#0a7429",
          data: [<?=$present_month_11?>,
		        <?=$present_month_10?>,
				<?=$present_month_9?>,
				<?=$present_month_8?>,
				<?=$present_month_7?>,
				<?=$present_month_6?>,
				<?=$present_month_5?>,
				<?=$present_month_4?>,
				<?=$present_month_3?>,
				<?=$present_month_2?>,
				<?=$present_month_1?>,
				<?=$present_month_0?>]
        },
		{
		  label: "Total Absents",
          fillColor: "#fa2307",
          strokeColor: "#fa2307",
          pointColor: "#fa2307",
          pointStrokeColor: "#fa2307",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "#fa2307",
          data: [<?=$absent_month_11?>,
		        <?=$absent_month_10?>,
				<?=$absent_month_9?>,
				<?=$absent_month_8?>,
				<?=$absent_month_7?>,
				<?=$absent_month_6?>,
				<?=$absent_month_5?>,
				<?=$absent_month_4?>,
				<?=$absent_month_3?>,
				<?=$absent_month_2?>,
				<?=$absent_month_1?>,
				<?=$absent_month_0?>]
        }
      ]
    };

	
	
	
	
	var barChartCanvas = $("#barChart").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas);
    var barChartData = areaChartData1;
    barChartData.datasets[1].fillColor = "#00a65a";
    barChartData.datasets[1].strokeColor = "#00a65a";
    barChartData.datasets[1].pointColor = "#00a65a";
    var barChartOptions = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: true,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - If there is a stroke on each bar
      barShowStroke: true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth: 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing: 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing: 1,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to make the chart responsive
      responsive: true,
      maintainAspectRatio: true
    };

    barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions);
   
   
   //Points Section 
   
       // Get context with jQuery - using jQuery's .get() method.
  //  var areaChartCanvas = $("#areaChart2").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
  //  var areaChart = new Chart(areaChartCanvas);
  

	
    //Create the line chart
 //  areaChart.Line(areaChartData2, areaChartOptions2);
   
   var areaChartData2 = {
      labels: ["<?=$months['11']?>", "<?=$months['10']?>", "<?=$months['9']?>", "<?=$months['8']?>", "<?=$months['7']?>", "<?=$months['6']?>", "<?=$months['5']?>", "<?=$months['4']?>", "<?=$months['3']?>", "<?=$months['2']?>", "<?=$months['1']?>", "<?=$months['0']?>"],
      datasets: [
        <?php for($m=0;$m<count($rs_student_point_graph);$m++){ 
		      $point_array = array();
		      $point_array = $rs_student_point_graph[$m]['points_data'];
			  
			  if($point_array[11]['total_point']=='1000'){ $total_point_11 = ''; }else{ $total_point_11 = $point_array[11]['total_point']; } 
		      if($point_array[10]['total_point']=='1000'){ $total_point_10 = ''; }else{ $total_point_10 = $point_array[10]['total_point']; }  
			  if($point_array[9]['total_point']=='1000'){ $total_point_9   = ''; }else{ $total_point_9 = $point_array[9]['total_point']; }  
			  if($point_array[8]['total_point']=='1000'){ $total_point_8 = ''; }else{ $total_point_8 = $point_array[8]['total_point']; }  
			  if($point_array[7]['total_point']=='1000'){ $total_point_7 = ''; }else{ $total_point_7 = $point_array[7]['total_point']; }  
			  if($point_array[6]['total_point']=='1000'){ $total_point_6 = ''; }else{ $total_point_6 = $point_array[6]['total_point']; }  
			  if($point_array[5]['total_point']=='1000'){ $total_point_5 = ''; }else{ $total_point_5 = $point_array[5]['total_point']; }  
			  if($point_array[4]['total_point']=='1000'){ $total_point_4 = ''; }else{ $total_point_4 = $point_array[4]['total_point']; }  
			  if($point_array[3]['total_point']=='1000'){ $total_point_3 = ''; }else{ $total_point_3 = $point_array[3]['total_point']; }  
			  if($point_array[2]['total_point']=='1000'){ $total_point_2 = ''; }else{ $total_point_2 = $point_array[2]['total_point']; }  
			  if($point_array[1]['total_point']=='1000'){ $total_point_1 = ''; }else{ $total_point_1 = $point_array[1]['total_point']; }  
			  if($point_array[0]['total_point']=='1000'){ $total_point_0 = ''; }else{ $total_point_0 = $point_array[0]['total_point']; }  
		
		  ?>
		{
		  label: "<?=$rs_student_point_graph[$m]['comments_student']?>",
          fillColor: "<?=$colorCode[$m]?>",
          strokeColor: "<?=$colorCode[$m]?>",
          pointColor: "<?=$colorCode[$m]?>",
          pointStrokeColor: "<?=$colorCode[$m]?>",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "<?=$colorCode[$m]?>",
          data: [<?=$total_point_11?>,
		        <?=$total_point_10?>,
				<?=$total_point_9?>,
				<?=$total_point_8?>,
				<?=$total_point_7?>,
				<?=$total_point_6?>,
				<?=$total_point_5?>,
				<?=$total_point_4?>,
				<?=$total_point_3?>,
				<?=$total_point_2?>,
				<?=$total_point_1?>,
				<?=$total_point_0?>]
        }
		<?php if($m<>count($rs_student_point_graph)-1){?>
		,
		<?php } 
		} ?>
      ]
    };
	
	
	

    var areaChartOptions2 = {
      //Boolean - If we should show the scale at all
      showScale: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: false,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - Whether the line is curved between points
      bezierCurve: true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension: 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot: false,
      //Number - Radius of each point dot in pixels
      pointDotRadius: 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth: 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius: 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke: true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth: 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true
    };

   
   
    var lineChartCanvas = $("#lineChart").get(0).getContext("2d");
    var lineChart = new Chart(lineChartCanvas);
    var lineChartOptions = areaChartOptions2;
    lineChartOptions.datasetFill = false;
    lineChart.Line(areaChartData2, lineChartOptions);
   
   
     //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
    var pieChart = new Chart(pieChartCanvas);
    var PieData = [ 
	   <?php for($c=0;$c<count($cards);$c++)
	  { ?>
	  {  
	    value: <?=abs($cards[$c]['score'])?>,
        color: "<?=$colorCode[$c]?>",
        highlight: "<?=$colorCode[$c]?>",
        label: "<?=$cards[$c]['title']?>"
      }
	  <?php  
	  if($c<>count($cards)-1) { ?>
	  ,
	  <?php } } ?>
	
    ];
    var pieOptions = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke: true,
      //String - The colour of each segment stroke
      segmentStrokeColor: "#fff",
      //Number - The width of each segment stroke
      segmentStrokeWidth: 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps: 100,
      //String - Animation easing effect
      animationEasing: "easeOutBounce",
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate: true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale: false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    };
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions);

   
   
  });
  </script>

