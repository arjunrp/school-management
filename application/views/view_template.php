<?php 	

        switch($page) {
			
			 case("view_school_web"): {
					include(ROOT_PATH."/application/views/include_web/".$lan."/view_index.php");
                    break;
                }
			case("view_student_web"): {
					include(ROOT_PATH."/application/views/include_web/".$lan."/student_details.php");
                    break;
                }
			case("view_parent_web"): {
					include(ROOT_PATH."/application/views/include_web/".$lan."/parents_details.php");
                    break;
                }
			case("view_feedback_web"): {
					include(ROOT_PATH."/application/views/include_web/".$lan."/feedback.php");
                    break;
                }
			case("view_contact_web"): {
					include(ROOT_PATH."/application/views/include_web/".$lan."/contact_us.php");
                    break;
                }	
			case("view_school_details"): {
					include(ROOT_PATH."/application/views/include_web/".$lan."/school_details.php");
                    break;
                }	
			case("view_teacher_web"): {
					include(ROOT_PATH."/application/views/include_web/".$lan."/teacher_details.php");
                    break;
                }	
			case("view_student_progress_report"): {
					include(ROOT_PATH."/application/views/include_web/".$lan."/progress_report.php");
                    break;
                }	
			case("view_student_progress_report_mobile"): {
					include(ROOT_PATH."/application/views/include_web/".$lan."/progress_report_mobile.php");
                    break;
                }		
				
		    case("view_home_page"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_home_page.php");
                    break;
                }

            case("view_parents_login_page"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_parents_login_page.php");
                    break;
                }

            case("view_parents_landing_page"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_parents_landing_page.php");
                    break;
                }

			case("view_app_info"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_app_info.php");
                    break;
                }		

			case("view_children_landing_page"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_children_landing_page.php");
                    break;
                }
		
			case("view_all_messages"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_all_messages.php");
                    break;
                }	

			case("view_leave_app_form"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_leave_app_form.php");
                    break;
                }


			case("view_points_detail"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_points_detail.php");
                    break;
                }	


			case("view_my_cards"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_my_cards.php");
                    break;
                }	


			case("view_teachers_login_page"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_teachers_login_page.php");
                    break;
                }


			case("view_teachers_landing_page"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_teachers_landing_page.php");
                    break;
                }

			case("view_register_absent_form"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_register_absent_form.php");
                    break;
                }


			case("view_all_students_against_class_ddm"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_all_students_against_class_ddm.php");
                    break;
                }
				
           case("view_all_students_against_selected_class"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_all_students_against_selected_class.php");
                    break;
                }

        
			case("view_contact_parents_form"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_contact_parents_form.php");
                    break;
                }


			case("view_contact_parents_sub_form"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_contact_parents_sub_form.php");
                    break;
                }	


			case("view_issue_cards_form"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_issue_cards_form.php");
                    break;
                }


			case("view_give_points_form"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_give_points_form.php");
                    break;
                }	


			case("view_school_contacts"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_school_contacts.php");
                    break;
                }


			case("event_details"): {
					include(ROOT_PATH."/application/views/include/".$lan."/event_details.php");
                    break;
                }


			case("event_list"): {
					include(ROOT_PATH."/application/views/include/".$lan."/event_list.php");
                    break;
                }

			case("view_parenting_page"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_parenting_page.php");
                    break;
                }


			case("view_card_messages"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_card_messages.php");
                    break;
                }


			case("view_child_data"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_child_data.php");
                    break;
                }


			case("view_load_survey"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_load_survey.php");
                    break;
                }	


			case("view_all_classes_against_teacher_ddm"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_all_classes_against_teacher_ddm.php");
                    break;
                }			



			case("view_children_instant_snaps"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_children_instant_snaps.php");
                    break;
                }


			case("view_contact_teacher_page"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_contact_teacher_page.php");
                    break;
                }	

			case("view_class_list"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_class_list.php");
                    break;
                }

			case("view_student_list"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_student_list.php");
                    break;
                }	


			case("view_message_list_page"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_message_list_page.php");
                    break;
                }

			case("view_message_list"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_message_list.php");
                    break;
                }	

			case("view_teacher_message_list_page"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_teacher_message_list_page.php");
                    break;
                }	


			case("view_teacher_message_list"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_teacher_message_list.php");
                    break;
                }	


			case("view_class_list_cdrpt"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_class_list_cdrpt.php");
                    break;
                }	


			case("view_student_list_cdrpt"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_student_list_cdrpt.php");
                    break;
                }


			case("view_daily_report"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_daily_report.php");
                    break;
                }


			case("view_card_messages"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_card_messages.php");
                    break;
                }


			case("view_bus_tracking_login_page"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_bus_tracking_login_page.php");
                    break;
                }


			case("view_start_bus_tracking_page"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_start_bus_tracking_page.php");
                    break;
                }


			case("view_student_bus_tracking_page"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_student_bus_tracking_page.php");
                    break;
                }	



			case("view_attendance_options_page"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_attendance_options_page.php");
                    break;
                }	



			case("view_attendance_page"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_attendance_page.php");
                    break;
                }	



			case("view_attendance_options_page_teacher"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_attendance_options_page_teacher.php");
                    break;
                }	



			case("view_attendance_page_teacher"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_attendance_page_teacher.php");
                    break;
                }		



			case("view_attendance_page_bus"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_attendance_page_bus.php");
                    break;
                }		



			case("view_teacher_attendance_page"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_teacher_attendance_page.php");
                    break;
                }	



			case("view_parenting_categories_page"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_parenting_categories_page.php");
                    break;
                }



			case("view_help_file"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_help_file.php");
                    break;
                }	


			case("view_image_gallery"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_image_gallery.php");
                    break;
                }	


			case("view_parenting_text_page"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_parenting_text_page.php");
                    break;
                }


			case("view_child_data_text"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_child_data_text.php");
                    break;
                }		



			case("view_general_login_page"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_general_login_page.php");
                    break;
                }		


			case("view_parents_modules_page"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_parents_modules_page.php");
                    break;
                }


			case("view_image_gallery_categories"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_image_gallery_categories.php");
                    break;
                }				


			case("view_image_gallery_images"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_image_gallery_images.php");
                    break;
                }				


			case("view_image_gallery_image"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_image_gallery_image.php");
                    break;
                }		


			case("view_image_gallery_images_student"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_image_gallery_images_student.php");
                    break;
                }		


			case("view_no_data_found"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_no_data_found.php");
                    break;
                }		


			case("view_leave_applications"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_leave_applications.php");
                    break;
                }


			case("view_teachers_roles"): {
					//echo ROOT_PATH."/application/views/include/".$lan."/view_teachers_roles.php";
					include(ROOT_PATH."/application/views/include/".$lan."/view_teachers_roles.php");
                    break;
                }	


			case("view_all_notofication_from_ministry"): {
					//echo ROOT_PATH."/application/views/include/".$lan."/view_teachers_roles.php";
					include(ROOT_PATH."/application/views/include/".$lan."/view_all_notofication_from_ministry.php");
                    break;
                }	


			case("view_parenting_school_categories_page"): {
				   include(ROOT_PATH."/application/views/include/".$lan."/view_parenting_school_categories_page.php");
                   break;
                }


			case("view_parenting_school_page"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_parenting_school_page.php");
                    break;
                }	


			case("view_parenting_school_text_page"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_parenting_school_text_page.php");
                    break;
                }	


			case("view_teacher_messages"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_teacher_messages.php");
                    break;
                }	


			case("view_parents_students_against_class_ddm"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_parents_students_against_class_ddm.php");
                    break;
                }


			case("view_all_parent_messages"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_all_parent_messages.php");
                    break;
                }	

			case("view_bus_announcement_page"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_bus_announcement_page.php");
                    break;
                }

			case("view_bus_announcement"): {
					include(ROOT_PATH."/application/views/include/".$lan."/view_bus_announcement.php");
                    break;
                }

			default: {
				include(ROOT_PATH."views/show_all_messages.php");
				break;
			}	

        }
    ?>



