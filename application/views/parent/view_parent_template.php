<?php include(ROOT_ADMIN_PATH."/parent/include/header.php");

?>
<!--WRAPPER-->
<div class="wrapper"> 
  
      <!--TOP BAR-->
      <?php include(ROOT_ADMIN_PATH."/parent/include/header_top_bar.php");?>
      <!--/TOP BAR--> 
      
      <!--LEFT-->
      <?php include(ROOT_ADMIN_PATH."/parent/include/left.php");?>
      <!--/LEFT--> 
      
      
      <!--INC FILE-->
      <?php
		//echo $page;
		//exit();
      	switch($page) {
			case("login_form"): {
				include(ROOT_ADMIN_PATH."/parent/login_form.php");
			break;	
			}	
			case("welcome"): {
				include(ROOT_ADMIN_PATH."/parent/welcome.php");
			break;	
			}
			case("view_my_children"): {
				include(ROOT_ADMIN_PATH."/parent/view_my_children.php");
			break;	
			}	
			case("view_child_details"): {
				include(ROOT_ADMIN_PATH."/parent/view_child_details.php");
			break;	
			}	
			case("view_ministry_parenting_category"): {
				include(ROOT_ADMIN_PATH."/parent/view_ministry_parenting_category.php");
			break;	
			}	
			case("view_ministry_parenting"): {
				include(ROOT_ADMIN_PATH."/parent/view_ministry_parenting.php");
			break;	
			}	
			case("view_my_children_record"): {
				include(ROOT_ADMIN_PATH."/parent/view_my_children_record.php");
			break;	
			}	
			case("view_school_records"): {
				include(ROOT_ADMIN_PATH."/parent/view_school_records.php");
			break;	
			}
			case("view_my_children_leave"): {
				include(ROOT_ADMIN_PATH."/parent/view_my_children_leave.php");
			break;	
			}
			case("view_child_leaves"): {
				include(ROOT_ADMIN_PATH."/parent/view_child_leaves.php");
			break;	
			}
			case("view_attendance_report_detailed"): {
				include(ROOT_ADMIN_PATH."/parent/view_attendance_report_detailed.php");
			break;	
			}
			case("view_my_children_pvtmessage"): {
				include(ROOT_ADMIN_PATH."/parent/view_my_children_pvtmessage.php");
			break;	
			}
			case("view_mychild_teachers"): {
				include(ROOT_ADMIN_PATH."/parent/view_mychild_teachers.php");
			break;	
			}
			case("view_mychild_teacher_pvtmessage"): {
				include(ROOT_ADMIN_PATH."/parent/view_mychild_teacher_pvtmessage.php");
			break;	
			}
			case("view_my_children_school_gallery"): {
				include(ROOT_ADMIN_PATH."/parent/view_my_children_school_gallery.php");
			break;	
			}
			case("view_child_school_gallery_category"): {
				include(ROOT_ADMIN_PATH."/parent/view_child_school_gallery_category.php");
			break;	
			}
			case("view_school_gallery_details"): {
				include(ROOT_ADMIN_PATH."/parent/view_school_gallery_details.php");
			break;	
			}
			
		    case("view_progress_report"): {
				include(ROOT_ADMIN_PATH."/parent/view_child_progress_report.php");
			break;	
			}
	
			
			
		}
	  ?>
      <!--/INC FILE--> 
      
      
      <!--FOOTER COPYRIGHT-->
      <?php include(ROOT_ADMIN_PATH."/parent/include/footer_copyright.php");?>
      <!--/FOOTER COPYRIGHT--> 
      
      <!--MENU RIGHT-->
      <?php include(ROOT_ADMIN_PATH."/parent/include/menu_slide_right.php");?>
      <!--/MENU RIGHT--> 
  
</div>
<!--/WRAPPER-->
<?php include(ROOT_ADMIN_PATH."/parent/include/footer.php");?>