
<?php
//Init Parameters
$leave_application_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}


?>
<link rel="stylesheet" media="all" type="text/css" href="<?=JS_PATH?>/date_time_picker/css/jquery-ui-1.8.6.custom.css" />
<style type="text/css">
pre {
	padding: 20px;
	background-color: #ffffcc;
	border: solid 1px #fff;
}
.example-container {
	background-color: #f4f4f4;
	border-bottom: solid 2px #777777;
	margin: 0 0 40px 0;
	padding: 20px;
}

.example-container p {
	font-weight: bold;
}

.example-container dt {
	font-weight: bold;
	height: 20px;
}

.example-container dd {
	margin: -20px 0 10px 100px;
	border-bottom: solid 1px #fff;
}

.example-container input {
	width: 150px;
}

.clear {
	clear: both;
}

#ui-datepicker-div {
}

.ui-timepicker-div .ui-widget-header {
	margin-bottom: 8px;
}

.ui-timepicker-div dl {
	text-align: left;
}

.ui-timepicker-div dl dt {
	height: 25px;
}

.ui-timepicker-div dl dd {
	margin: -25px 0 10px 65px;
}

.ui-timepicker-div td {
	font-size: 90%;
}
</style>

<script type="text/javascript" src="<?=JS_PATH?>/date_time_picker/js/jquery-ui-1.8.6.custom.min.js"></script>
<script type="text/javascript" src="<?=JS_PATH?>/date_time_picker/js/jquery-ui-timepicker-addon.js"></script>
<script language="javascript">
	$(document).ready(function(){
		$('#start_date').datepicker({
			dateFormat: '',
			timeFormat: 'hh:mm tt',
			timeOnly: true   
		});


		$('#end_date').datepicker({
			dateFormat: '',
			timeFormat: 'hh:mm tt',
			timeOnly: true   
		});
	});
</script>
<script type="text/javascript" src="<?=JS_COLOR_PATH?>/jscolor.js"></script>

<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid2').show(500);
		$('#mid1_list').hide(500);
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/child_leaves/child_id_enc/<?=$tbl_sel_student_id?>/tbl_school_id/<?=$tbl_sel_school_id?>";
			}
		})
	});
	
function confirm_delete_popup() {
		var len = $("input[id='event_id_enc']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more event(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/event/deleteEvent",
			data: {
				event_id_enc: $("input[id='event_id_enc']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Event(s) deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}	
	function ajax_activate(event_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/event/activateEvent",
			data: {
				event_id_enc: event_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Event activated successfully.", 'green')

				$('#act_deact_'+event_id_enc).html('<span style="cursor:pointer" onClick="ajax_deactivate(\''+event_id_enc+'\')" onMouseOver="deactivate_me(this)" onMouseOut="reset_activate(this)" class="label label-success">Active</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_deactivate(event_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/event/deactivateEvent",
			data: {
				event_id_enc: event_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Event de-activated successfully.", 'green')
				
				$('#act_deact_'+event_id_enc).html('<span style="cursor:pointer" onClick="ajax_activate(\''+event_id_enc+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/is_exist_leave",
			data: {
			    tbl_leave_application_id: "<?=$leave_application_id_enc?>",
				comments_parent: $('#comments_parent').val(),
				start_date: $('#start_date').val(),
				end_date: $('#end_date').val(),
				tbl_student_id: $('#student_id_enc').val(),
				tbl_school_id: $('#school_id_enc').val(),
				is_ajax: true
			},
			success: function(data) {
				
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='T') {
					refresh_page = "N";
					my_alert("Leave Application is already exists.", 'red');
					$("#pre-loader").hide();
				}else if (temp=='Y') {
					refresh_page = "N";
					my_alert("Leave Application is already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_create();
				}
			
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist_edit() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/event/is_exist_event",
			data: {
				tbl_event_id: $('#event_id_enc').val(), 
				title: $('#title').val(),
				title_ar: "",
				start_date: $('#start_date').val(),
				end_date: $('#end_date').val(),
				is_ajax: true
			},
			success: function(data) {
				
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='T') {
					refresh_page = "N";
					my_alert("Event is already exists.", 'red');
					$("#pre-loader").hide();
				}else if (temp=='Y') {
					refresh_page = "N";
					my_alert("Event is already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_save_changes();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_create() {
		 
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/add_leave_application",
			data: {
				tbl_leave_application_id: "<?=$leave_application_id_enc?>",
				comments_parent: $('#comments_parent').val(),
				start_date: $('#start_date').val(),
				end_date: $('#end_date').val(),
				tbl_student_id: $('#student_id_enc').val(),
				tbl_school_id: $('#school_id_enc').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
				refresh_page = "Y";
				    my_alert("Leave Application is added successfully.", 'green');
				    $("#pre-loader").hide();
				}else{
					refresh_page = "N";
					my_alert("Leave Application is added failed, Please try again.", 'red');
					$("#pre-loader").hide();
				}
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_save_changes() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/event/save_event_changes",
			data: {
				tbl_event_id: $('#event_id_enc').val(), 
			    title: $('#title').val(),
				title_ar: $('#title_ar').val(),
				description: $('#description').val(),
				description_ar: $('#description_ar').val(),
				start_date: $('#start_date').val(),
				end_date: $('#end_date').val(),
				start_time: $('#start_time').val(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Changes saved successfully.", 'green');
				
				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
</script>
<script language="javascript">
	function ajax_validate() {
		if (validate_reason() == false || validate_start_date() == false || validate_end_date() == false || validate_dates() == false  ) {
			return false;
		} else {
			is_exist();
		}
	} 

	function ajax_validate_edit() {
		if (validate_reason() == false || validate_start_date() == false || validate_end_date() == false || validate_dates() == false ) {
			return false;
		} else {
			is_exist_edit();
		}
	} 

	function validate_reason() {
		var regExp = / /g;
		var str = $('#comments_parent').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Reason for Leave is blank. Please enter Reason.");
		return false;
		}
	}

	
	
	
	 function validate_start_date() {
		var regExp = / /g;
		var str = $('#start_date').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Start Date is not selected. Please select Start Date.");
		return false;
		}
	}
	
	 function validate_end_date() {
		var regExp = / /g;
		var str = $('#end_date').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			return true;
			/*my_alert("End Date is not selected. Please select End Date.");
		return false;*/
		}
	}
	
	
	function validate_dates() {
		var st   = new Date($("#start_date").val());
		var et   = new Date($("#end_date").val());
	    if(et.length >0)
		{
			if(st!='')
			{
				//how do i compare time
				if(st > et)
				{
				   my_alert("The Leave End Date should always be equal to or greater than leave Start Date.");
				   return false;
				}
				
			}
		}
		return true;
	}

	
	

</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Leave <small> Applications</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Leave Applications</li>
    </ol>
    <!--/BREADCRUMB--> 
    <div class="box-tools" style="float:right;">
        <a href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/mychild_leave/tbl_parent_id/<?=$_SESSION['aqdar_smartcare']['tbl_admin_id_sess']?>"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
    </div> 
    <div style="clear:both"></div>
  </section>
  
  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {
			$tbl_sel_event_id        = $event_obj['tbl_events_id'];
			$title                   = $event_obj['title'];
			$title_ar                = $event_obj['title_ar'];
			$description             = $event_obj['description'];
			$description_ar          = $event_obj['description_ar'];
			$start_date              = date("m/d/Y", strtotime($event_obj['start_date']));
			$end_date                = date("m/d/Y", strtotime($event_obj['end_date']));
			$start_time              = $event_obj['start_time'];
	?>
        <!--Edit-->
        
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Event</h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/event/all_events"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_edit" id="frm_listing" class="form-horizontal" method="post">
                    <div class="box-body">
                   
                   <div class="form-group">
                      <label class="col-sm-2 control-label" for="title">Event Title [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Event Title [En]" id="title" name="title" class="form-control" value="<?=$title?>" >
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="title_ar">Event Title [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Event Title [Ar]" id="title_ar" name="title_ar" class="form-control" dir="rtl" value="<?=$title_ar?>" >
                      </div>
                    </div>
                  
                  
                    <div class="form-group">
                     <label class="col-sm-2 control-label" for="description">Description </label>
                     <div class="col-sm-5">
                              <textarea tabindex="1" dir="ltr" id="description" placeholder="Description [En]" name="description" class="form-control" ><?=$description?></textarea>
                     </div>
                     <div class="col-sm-5">
                              <textarea tabindex="1" dir="rtl" id="description_ar" placeholder="Description [Ar]" name="description_ar" class="form-control" ><?=$description_ar?></textarea>
                     </div>
                     </div>
                   
                    
                      <div class="form-group">
                      <label class="col-sm-2 control-label" for="start_date">Event Date<span style="color:#F30; padding-left:2px;">*</span></label>
                      <div class="col-sm-3">
                        <input type="text" placeholder="Start Date" id="start_date" name="start_date" class="form-control" value="<?=$start_date?>"  >
                      </div>
                      <div class="col-sm-3">
                        <input type="text" placeholder="End Date" id="end_date" name="end_date" class="form-control" value="<?=$end_date?>" >
                      </div>
                      <div class="col-sm-2">
                        <input type="text" placeholder="Start Time" id="start_time" name="start_time" class="form-control" value="<?=$start_time?>" >
                      </div>
                    </div>
                    
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate_edit()">Save Changes</button>
                    <input type="hidden" name="event_id_enc" id="event_id_enc" value="<?=$tbl_sel_event_id?>" />
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
		        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/event/all_events";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
    
  
 <link href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" rel="stylesheet">
 <script src="http://code.jquery.com/jquery-1.11.1.js"></script>
 <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
  <script>
  $( function() {
		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
				
				/* var order = $("#tabledivbody").sortable("serialize");
				
				alert(order);
				
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/your/url/here'
				});*/
				
				
				
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	

				
				
				
				
				
			}
	  } );
  
  } );
  
  
  
  </script> 
    
            <!--Listing-->
                    <div id="mid1_list" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Leave Applications For &nbsp;<strong><?=$child_info[0]['first_name']." ".$child_info[0]['last_name']?>&nbsp;&nbsp;/&nbsp;&nbsp;<?=$child_info[0]['first_name_ar']." ".$child_info[0]['last_name_ar']?></strong></h3>
                          <div class="box-tools">
                            <?php if (count($rs_all_leaves_application)>0) { echo $paging_string;}?>	
                            <button class="btn bg-orange fa fa-plus" type="button" title="Send Leave Application" onclick="show_create_form()"></button>
                           <!-- <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>-->
                          </div>
                        </div>
                        
                        <div class="box-body">
                     <!--   <div style="color:#030; font-weight:bold;">You can sort categories by using drag and drop of rows </div>-->
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="40%" align="center" valign="middle">Reason</th>
                              <th width="15%" align="center" valign="middle">Start Date</th>
                              <th width="15%" align="center" valign="middle">End Date</th>
                              <th width="15%" align="center" valign="middle">Leave Applied</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                                for ($i=0; $i<count($rs_all_leaves_application); $i++) { 
                                    $id                        = $rs_all_leaves_application[$i]['id'];
                                    $tbl_leave_application_id  = $rs_all_leaves_application[$i]['tbl_leave_application_id'];
                                    $comments_parent           = $rs_all_leaves_application[$i]['comments_parent'];
									 
									$start_date                = $rs_all_leaves_application[$i]['start_date'];
                                    $end_date                  = $rs_all_leaves_application[$i]['end_date'];
									
                                    $added_date                = $rs_all_leaves_application[$i]['added_date'];
                                    $is_active                 = $rs_all_leaves_application[$i]['is_active'];
                                    
                                    $title                     = ucfirst($title);
									$start_date                = date('m-d-Y',strtotime($start_date));
									$end_date                  = date('m-d-Y',strtotime($end_date));
                                    $added_date                = date('m-d-Y',strtotime($added_date));
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_leave_application_id?>" >
                              <td align="left" valign="middle"><?=$comments_parent?></td>
                              <td align="left" valign="middle"><?=$start_date?></td>
                              <td align="left" valign="middle"><?=$end_date?></td> 
                              <td align="left" valign="middle"><?=$added_date?></td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="9" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_all_leaves_application)<=0) {
                            ?>
                            <tr>
                              <td colspan="9" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no leave request
                                 available. Click on the + button to apply leave request.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
    
            <!--Add or Create-->
              <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Leave Application For &nbsp;<strong><?=$child_info[0]['first_name']." ".$child_info[0]['last_name']?>&nbsp;&nbsp;/&nbsp;&nbsp;<?=$child_info[0]['first_name_ar']." ".$child_info[0]['last_name_ar']?></strong></h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
               <?php $start_date                = date("m/d/Y"); ?>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                  
                  
                    <div class="form-group">
                     <label class="col-sm-2 control-label" for="description">Reason</label>
                     <div class="col-sm-10">
                              <textarea tabindex="1" dir="ltr" id="comments_parent" placeholder="Specify Reason For Leave" name="comments_parent" class="form-control" ></textarea>
                     </div>
                     
                     </div>
                   
                    
                      <div class="form-group">
                      <label class="col-sm-2 control-label" for="start_date">Date<span style="color:#F30; padding-left:2px;">*</span></label>
                      <div class="col-sm-3">
                        <input type="text" placeholder="Start Date" id="start_date" name="start_date" class="form-control"  value="<?=$start_date?>">
                      </div>
                      <div class="col-sm-3">
                        <input type="text" placeholder="End Date" id="end_date" name="end_date" class="form-control" value="<?=$start_date?>" >
                      </div>
                     
                    </div>
                   
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate()">Submit</button>
                    <input type="hidden" name="student_id_enc" id="student_id_enc" value="<?=$tbl_sel_student_id?>" />
                    <input type="hidden" name="school_id_enc" id="school_id_enc" value="<?=$tbl_sel_school_id?>" />
                    
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
            <!--/Add or Create-->
                
        <!--/Admin Category Management-->
        

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>

        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >
function search_data() {
		var q = $("#q").val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/event/all_events/";
		
		if(q !='')
			url += "q/"+q+"/";
		
			url += "offset/0/";
		window.location.href = url;
	}

function reset_data() {
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/event/all_events/";
		url += "offset/0/";
		window.location.href = url;
	}
</script>