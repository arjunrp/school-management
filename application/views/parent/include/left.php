<?php
if($_SESSION['aqdar_smartcare']['admin_pic']<>"")
	{
		$admin_pic = $_SESSION['aqdar_smartcare']['admin_pic'];
	}
?>

<aside class="main-sidebar"> 
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar"> 
      <!-- Sidebar user panel (optional) -->
   <?php /*?>   <div class="user-panel">
        <div class="pull-left image"> 
            <?php
            	if (trim($admin_pic) != "") { 
			?>
        	<img src="<?=$admin_pic?>" height="50" class="img-circle" alt="User Image">
            <?php } else { ?>
	            <img src="<?=HOST_URL?>/assets/admin/images/users/default_avatar.png" class="img-circle" alt="User Image">
			<?php } ?>
        </div>
        <div class="pull-left info">
          <p>
				<?php if (trim($_SESSION['aqdar_smartcare']['user_type_sess']) == "admin") { ?>
                    <?=$_SESSION['aqdar_smartcare']['admin_first_name_sess']?> <?=$_SESSION['aqdar_smartcare']['admin_last_name_sess']?>
                <?php }  else { ?>
                    <?=$_SESSION['aqdar_smartcare']['judge_first_name_en_sess']?> <?=$_SESSION['aqdar_smartcare']['judge_last_name_en_sess']?>
                <?php } ?>
          </p>
          <!-- Status --> 
          <a href="#"><i class="fa fa-circle text-success"></i>  <?php if(LAN_SEL=="ar"){?> عبر الانترنت <?php }else{?> Online <?php } ?></a> 
          </div>
      </div><?php */?>
      
      <!-- search form (Optional) -->
      <!--<form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i> </button>
          </span> </div>
      </form>-->
      <!-- /.search form --> 
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header"> <?php if(LAN_SEL=="ar"){?> قائمة  <?php }else{?> MENU <?php } ?>  </li>
      
          <?php if ((trim($_SESSION['aqdar_smartcare']['user_type_sess']) == "parent")  && (trim($_SESSION['aqdar_smartcare']['admin_auth_sess']) == "SA" ))  { ?>
         
          <li>
            <a <?php if($sub_menu == "children") {?> style="color:#fff;" <?php }?>   href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/mychild/tbl_parent_id/<?=$_SESSION['aqdar_smartcare']['tbl_admin_id_sess']?>" ><i class="fa fa-users" ></i> <span>
                <?php if(LAN_SEL=="ar"){ ?> أطفالي <?php }else{?> My Children <?php } ?></span>
          </a></li>
          
           <li>
                <a <?php if($sub_menu == "parenting") {?> style="color:#fff;" <?php }?>   href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/parenting_category/" ><i class="fa fa-newspaper-o" ></i> <span>
                <?php if(LAN_SEL=="ar"){ ?> اولياء الامور <?php }else{?> Parenting <?php } ?></span>
          </a></li>
          
          <li>
                <a <?php if($sub_menu == "records") {?> style="color:#fff;" <?php }?>   href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/mychild_record/tbl_parent_id/<?=$_SESSION['aqdar_smartcare']['tbl_admin_id_sess']?>" ><i class="fa fa-files-o" ></i> <span>
                <?php if(LAN_SEL=="ar"){ ?> السجلات <?php }else{?> Records <?php } ?></span>
          </a>
          </li>
          
          
           <li>
               <a <?php if($sub_menu == "leaves") {?> style="color:#fff;" <?php }?>   href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/mychild_leave/tbl_parent_id/<?=$_SESSION['aqdar_smartcare']['tbl_admin_id_sess']?>" ><i class="fa fa-pencil-square-o" ></i> <span>
                <?php if(LAN_SEL=="ar"){ ?> استبيان <?php }else{?> Leave Application <?php } ?></span>
               </a>
           </li>
        
           <li>
               <a <?php if($sub_menu == "attendance") {?> style="color:#fff;" <?php }?>   href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/attendance_reports_list/tbl_parent_id/<?=$_SESSION['aqdar_smartcare']['tbl_admin_id_sess']?>" ><i class="fa fa-hand-paper-o" ></i> <span>
                <?php if(LAN_SEL=="ar"){ ?> استبيان <?php }else{?> Attendance Report <?php } ?></span>
               </a>
           </li>
           
           <li ><a <?php if($sub_menu == "student_progress_report") {?> style="color:#fff;" <?php }?>  href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/child_progress_report/tbl_parent_id/<?=$_SESSION['aqdar_smartcare']['tbl_admin_id_sess']?>"><i class="fa fa-bar-chart" ></i> <span>
                <?php if(LAN_SEL=="ar"){ ?>تقرير الأداء<?php }else{?> Progress Report <?php } ?>
                </a>
           </li>
          
           <li>
               <a <?php if($sub_menu == "private_message") {?> style="color:#fff;" <?php }?>   href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/mychild_private_message/tbl_parent_id/<?=$_SESSION['aqdar_smartcare']['tbl_admin_id_sess']?>" ><i class="fa fa-comments" ></i> <span>
                <?php if(LAN_SEL=="ar"){ ?> استبيان <?php }else{?> Private Message <?php } ?></span>
               </a>
           </li>
          
      
          <li>
                <a <?php if($sub_menu == "gallery") {?> style="color:#fff;" <?php }?>   href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/mychild_school_gallery/tbl_parent_id/<?=$_SESSION['aqdar_smartcare']['tbl_admin_id_sess']?>"><i class="fa fa-picture-o" ></i> <span>
                <?php if(LAN_SEL=="ar"){ ?> معرض الصور <?php }else{?> Image Gallery <?php } ?></span>
               </a>
          </li>
      
     
      
          <?php } ?>
      </ul>
      <!-- /.sidebar-menu --> 
    </section>
    <!-- /.sidebar --> 
  </aside>