         <?php
		 $report_name 		= $progress_report_details[0]['report_name'];
		 $report_name_ar 	= $progress_report_details[0]['report_name_ar'];
		 $tbl_student_id 	= $progress_report_details[0]['tbl_student_id'];
		 $tbl_class_id 	    = $progress_report_details[0]['tbl_class_id'];
		 $tbl_school_id 	= $progress_report_details[0]['tbl_school_id'];
		 ?>  

      <div class="box-body" id="add_report" style="display:block;">
       
        <div class="form-group">
        	<div class="col-sm-12" style="float:left;"><?=$report_name?></div>
        	<div class="col-sm-12" style=" text-align:right;"><?=$report_name_ar?></div>
         </div>
        <table width="100%" class="table table-bordered table-striped">
          <thead>
            <tr>
                <th width="60%" align="center" valign="middle">Subject</th>
                <th width="20%" align="center" valign="middle">Mark</th>
                <th width="20%" align="center" valign="middle">Total Mark</th>
            </tr> 
          </thead>
          <tbody>
          <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		  <script src="<?=HOST_URL?>/js/jquery-ui.js"></script>  
  
<?php	
    for ($u=0; $u<count($progress_report_details); $u++) { 
            $tbl_progress_report_id   	= $progress_report_details[$u]['tbl_progress_report_id'];
			$tbl_subject_id             = $progress_report_details[$u]['tbl_subject_id'];
            $subject         		    = $progress_report_details[$u]['subject'];
            $subject_ar         	    = $progress_report_details[$u]['subject_ar'];
			$mark         	            = $progress_report_details[$u]['mark'];
			$total_mark         	    = $progress_report_details[$u]['total_mark'];
		    $subject_ids                = $tbl_subject_id."||";
 ?>        
         <tr>
		  <td>
          <div><?=$subject?><span style="float:right;"><?=$subject_ar?></span> </div>
         </td>
         <td><?=$mark?></td>
         <td><?=$total_mark?></td>
         </tr>
<?php
	}
?>

 </tbody>
          <tfoot>
          </tfoot>
        </table>
                
    </div>

<script>
function update_student_progress_report(tbl_progress_report_id, tbl_student_id,tbl_class_id,tbl_school_id, tbl_teacher_id) {
		
		var subject_ids 		   =  $("#subject_ids").val();
		var id_data     		=  "";
		var sub_mark_data       =  "";
		var tot_mark_data       =  "";
		var cnt                 =  "";
		
		var id_array            =  new Array;
		if(subject_ids != '')
		{
			var id_array    = subject_ids.split('||');
			for(k = 0; k < id_array.length; k++)
			{
				var ids          = id_array[k];
				if(ids!='')
				{
					var subMark  	= $("#marks_"+ids).val();
					var totMark  	= $("#totals_"+ids).val();
					if(subMark != '' && totMark != '' )
					{
						id_data 		= id_data+ids+"||";
						sub_mark_data 	= sub_mark_data+subMark+"||";
						tot_mark_data 	= tot_mark_data+totMark+"||";
						cnt = 1;
						
					}
				}
			
			}
			
		}
		
		
		if(cnt!= '1')
		{
			alert("Please enter student marks and total marks.");
			return false;
		}else{
			
			$("#pre-loader").show();
			var url_str = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/update_student_progress_report";
			$.ajax({
				type: "POST",
				url: url_str,
				data: {
					is_admin: "Y",
					tbl_progress_report_id: tbl_progress_report_id,
					id_data: id_data,
					sub_mark_data: sub_mark_data,
					tot_mark_data: tot_mark_data,
					tbl_student_id: tbl_student_id,
					tbl_class_id: tbl_class_id,
					tbl_teacher_id: tbl_teacher_id,
					tbl_school_id: tbl_school_id
				},
				success: function(result_data) {
					if($.trim(result_data)=="E")
					{
						 alert("Progress report already exist.");
						$("#pre-loader").hide();
					}else if($.trim(result_data)=="N")
					{
						 alert("Progress report added failed, Please try again.");
						$("#pre-loader").hide();
					}else{
						alert("Progress report added successfully.");
						$("#pre-loader").hide();
					}
				}
			});
		}
	}
	
	function get_progress_report_close()
	{
		jQuery.noConflict(); 
		$('#myModal').modal('hide');
		
	}
	</script>