         <?php
		 	$tbl_parenting_school_id             = $school_record[0]['tbl_parenting_school_id'];		
			$tbl_parenting_school_cat_id         = $school_record[0]['tbl_parenting_school_cat_id'];
			$parenting_title_en                  = $school_record[0]['parenting_title_en'];		
			$parenting_title_ar                  = $school_record[0]['parenting_title_ar'];
			$parenting_type                      = $school_record[0]['parenting_type'];		
			$parenting_logo                      = $school_record[0]['parenting_logo'];
			$parenting_text_en                   = $school_record[0]['parenting_text_en'];	
			$parenting_text_ar                   = $school_record[0]['parenting_text_ar'];	
			$parenting_url                       = $school_record[0]['parenting_url'];
			$is_active                           = $school_record[0]['is_active'];
			$file                                = $school_record[0]['file_name_updated'];
			$tbl_uploads_id                      = $school_record[0]['tbl_uploads_id'];
			if($parenting_logo<>""){
			  $img_path                           = IMG_UPLOAD_PATH."/".$parenting_logo;
			}
			if($file<>""){
			  $file_url                          = IMG_UPLOAD_PATH."/".$file;
			  $file_ext_array = explode(".",$file);
			  $ext = $file_ext_array[1];
			}
			
		 ?>  
         <div id="div_popup_message" style="display:block;">
                    <?php /*?>  <div class="form-group">
                      <span style="float:left;"><?=$parenting_title_en?></span>
                     </div>
                     <div class="form-group">
                      <span style="float:right;"><?=$parenting_title_ar?></span>
                    </div>
                    <div style="clear:both;"></div>
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="parenting_title_ar">Thumb Image</label>
    				  <?php
                      	if (trim($img_path) != "") { 
					  ?>
                        <div class="col-sm-10">
                      		<div id="div_img">	
	                            <br />
                                <img src="<?=$img_path?>" style="max-width:400px" /> 
                                <br />
                            </div>
                        </div>
					 <?php		
						}
					  ?>
                      <div style="clear:both;"></div>
                    </div>
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="dob">Type</label>
                      <div class="col-sm-10">
                       <?php if(strtolower(trim($parenting_type)) == "d") { echo "Text"; } else if(strtolower(trim($parenting_type)) == "u") { echo "url"; } else { echo "File"; } ?>
                      </div>
                      <div style="clear:both;"></div>
                    </div><?php */?>
                    
                     <?php if (strtolower(trim($parenting_type)) == "v") { ?>
                              <div class="form-group">
                              <div class="col-sm-12">
                                   
									<?php
									if (trim($file_url) != "" && ($ext=="mp3" || $ext=="swf" || $ext=="mp4" )) { 
									?>
                                    <div id="<?=$tbl_uploads_id?>">
                                      <div class="box-title">
                                        <video width="100%" height="100%" controls>
                                          <source src="<?=$file_url?>" type="video/mp4">
                                        </video>
                                      </div>
                                    </div>         
									<?php
									}else if(trim($file_url) != "" && ($ext=="jpg" || $ext=="png" || $ext=="gif" || $ext=="jpeg" )) {
                                      ?>
                                    <div id='<?=$tbl_uploads_id?>' class='box-header with-border' style="text-align:center;">
                                      <div class='box-title'><img src='<?=$file_url?>' /></div>
                                      </div>
                                    </div>
                                    <?php
									}else if(trim($file_url) != "" && ($ext=="pdf" )) {
                                      ?>
                                    <div id='<?=$tbl_uploads_id?>'>
                                      <div class='box-title' ><a target="_blank" href="<?=$file_url?>"><img src="<?=IMG_PATH?>/pdf_icon.png" width="80" height="80" /><br />Click Here To View/Download</a></div>
                                      </div>
                                    </div>
                                    <?php		
                                     }
                                    ?>
                                        
                                <!--File Upload END-->
                              </div>
                     <?php }else if (strtolower(trim($parenting_type)) == "d"){?>
                       <div class="form-group">
                        <span style="float:left;">
                          <p>  <?=$parenting_text_en?> </p>
                          </span>
                          <div style="clear:both;"></div>
                       </div>
                   
                       <div class="form-group">
                         <span style="float:right;">
                          <p>  <?=$parenting_text_ar?> </p>
                          </span>
                        </div>
                     
                      <?php } else if (strtolower(trim($parenting_type)) == "u"){ ?>
                       <div class="form-group">
                       <?php  if($parenting_logo<>""){ ?>
                         <div>
                         	<div class='box-title' ><a target="_blank" href="<?=$parenting_url?>"><img src="<?=$img_path?>" width="80" height="80" /></a></div>
                         </div>
                        <?php } ?>
                        <span style="float:left;">
                            <a href="<?=$parenting_url?>" target="_blank"><?=$parenting_url?></a>
                          </span>
                          <div style="clear:both;"></div>
                       </div>
                     
                     <?php } ?>
                           
                    
            </div>
                
    </div>
