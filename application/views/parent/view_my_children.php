<?php
//Init Parameters
$student_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}
?>
<style>
.txt_en {
	text-align:left;
	padding-left:2px;
}
.txt_ar {
	text-align:right;
	padding-right:2px;	
	direction:rtl;		
}
</style>
<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#divPromo').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
			$('#divPromo').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/all_students";
			}
		})
	});
	
function confirm_delete_popup() {
		var len = $("input[id='student_id_enc']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more student(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/deleteStudent",
			data: {
				student_id_enc: $("input[id='student_id_enc']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Student(s) deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}	
	function ajax_activate(student_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/activateStudent",
			data: {
				student_id_enc: student_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Student activated successfully.", 'green')

				$('#act_deact_'+student_id_enc).html('<span style="cursor:pointer" onClick="ajax_deactivate(\''+student_id_enc+'\')" onMouseOver="deactivate_me(this)" onMouseOut="reset_activate(this)" class="label label-success">Active</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_deactivate(student_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/deactivateStudent",
			data: {
				student_id_enc: student_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Student de-activated successfully.", 'green')
				
				$('#act_deact_'+student_id_enc).html('<span style="cursor:pointer" onClick="ajax_activate(\''+student_id_enc+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	

	
	function is_exist_edit() {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/is_exist_student",
			data: {
				student_id_enc: $('#student_id_enc').val(),
				first_name: $('#first_name').val(),
				last_name: $('#last_name').val(),
				tbl_class_id: $('#tbl_class_id').val(),
				tbl_emirates_id: $('#tbl_emirates_id').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String(data);
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
					refresh_page = "N";
					my_alert("Student is already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_save_changes();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_create() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/add_student",
			data: {
				student_id_enc: "<?=$student_id_enc?>",
				first_name    : $('#first_name').val(),
				last_name     : $('#last_name').val(),
				first_name_ar : $('#first_name_ar').val(),
				last_name_ar  : $('#last_name_ar').val(),
				dob_month     : $('#dob_month').val(),
				dob_day       : $('#dob_day').val(),
				dob_year      : $('#dob_year').val(),
				gender        : $('input[name=gender]:checked').val(),
				mobile        : $('#mobile').val(),
				email         : $('#email').val(),
				tbl_emirates_id : $('#tbl_emirates_id').val(),
				country       : $('#country').val(),
				emirates_id_father : $('#emirates_id_father').val(),
				emirates_id_mother : $('#emirates_id_mother').val(),
				tbl_academic_year_id : $('#tbl_academic_year_id').val(),
				tbl_class_id         : $('#tbl_class_id').val(),
				
				first_name_parent      : $('#first_name_parent').val(),
				first_name_parent_ar   : $('#first_name_parent_ar').val(),
				last_name_parent       : $('#last_name_parent').val(),
				last_name_parent_ar    : $('#last_name_parent_ar').val(),
				dob_month_parent       : $('#dob_month_parent').val(),
				dob_day_parent         : $('#dob_day_parent').val(),
				dob_year_parent        : $('#dob_year_parent').val(),
				gender_parent          : $('input[name=gender_parent]:checked').val(), 
				mobile_parent          : $('#mobile_parent').val(),
				email_parent           : $('#email_parent').val(),
				emirates_id_parent     : $('#emirates_id_parent').val(),
				parent_user_id         : $('#parent_user_id').val(),
				password               : $('#password').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
				refresh_page = "Y";
				    my_alert("Student added successfully.", 'green');
				    $("#pre-loader").hide();
				}else{
					refresh_page = "N";
					my_alert("Student added failed, Please try again.", 'red');
					$("#pre-loader").hide();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_save_changes() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/save_student_changes",
			data: {
				student_id_enc: $('#student_id_enc').val(),
				first_name    : $('#first_name').val(),
				last_name     : $('#last_name').val(),
				first_name_ar : $('#first_name_ar').val(),
				last_name_ar  : $('#last_name_ar').val(),
				dob_month     : $('#dob_month').val(),
				dob_day       : $('#dob_day').val(),
				dob_year      : $('#dob_year').val(),
				gender        : $('input[name=gender]:checked').val(),
				mobile        : $('#mobile').val(),
				email         : $('#email').val(),
				tbl_emirates_id : $('#tbl_emirates_id').val(),
				country       : $('#country').val(),
				emirates_id_father : $('#emirates_id_father').val(),
				emirates_id_mother : $('#emirates_id_mother').val(),
				tbl_academic_year_id : $('#tbl_academic_year_id').val(),
				tbl_class_id         : $('#tbl_class_id').val(),
				
				parent_id_enc          : $('#parent_id_enc').val(),
				first_name_parent      : $('#first_name_parent').val(),
				first_name_parent_ar   : $('#first_name_parent_ar').val(),
				last_name_parent       : $('#last_name_parent').val(),
				last_name_parent_ar    : $('#last_name_parent_ar').val(),
				dob_month_parent       : $('#dob_month_parent').val(),
				dob_day_parent         : $('#dob_day_parent').val(),
				dob_year_parent        : $('#dob_year_parent').val(),
				gender_parent          : $('input[name=gender_parent]:checked').val(), 
				mobile_parent          : $('#mobile_parent').val(),
				email_parent           : $('#email_parent').val(),
				emirates_id_parent     : $('#emirates_id_parent').val(),
				parent_user_id         : $('#parent_user_id').val(),
				password               : $('#password').val(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Changes saved successfully.", 'green');
				
				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
</script>
<script language="javascript">
   //add student
   /* || validate_picture() == false*/
	function ajax_validate() {
		if (validate_first_name() == false || validate_last_name() == false || validate_emirates_id() == false || validate_dob() == false || isMobile() == false ||  validate_email() == false || validate_country() == false || validate_emirates_id_father() == false || validate_emirates_id_mother() == false || validate_emirates_id_parent_same()== false || validate_emirates_id_parent() == false || validate_class() == false || validate_academic_year() == false || validate_exist_student()==false ) 
		{
			return false;
		} 
		else {
		
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/is_exist_parent_info",
			dataType: "json",
			data: {
				emirates_id_father: $('#emirates_id_father').val(),
				emirates_id_mother: $('#emirates_id_mother').val(),
				is_ajax: true
			},
			success: function(data) {
			   $("#first_name_parent").val(data[0]['first_name']);
			   $("#first_name_parent_ar").val(data[0]['first_name_ar']);
			   $("#last_name_parent").val(data[0]['last_name']);
			   $("#last_name_parent_ar").val(data[0]['last_name_ar']);
			   $("#dob_month_parent").val(data[0]['dob_month']);
			   $("#dob_day_parent").val(data[0]['dob_day']);
			   $("#dob_year_parent").val(data[0]['dob_year']);
			   $("#gender_parent").val(data[0]['gender']);
			   $("#email_parent").val(data[0]['email']);
			   $("#parent_user_id").val(data[0]['user_id']);
			   if(data[0]['emirates_id']!='')
			    	$("#emirates_id_parent").val(data[0]['emirates_id']);
			   else
			   		$("#emirates_id_parent").val($('#emirates_id_father').val());
			   
			   
			   if(data[0]['mobile']!='')
			   {
				   var str = data[0]['mobile'];
				   str = str.replace("+971", "");
			   }
			   $("#mobile_parent").val($.trim(str));
			   $("#password").val('123456');
			   $("#confirm_password").val('123456');
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
			
			
			
			
		}
	}
	
	//Validate Parent
	
	function ajax_validate_parent() {
		if (validate_first_name_parent() == false || validate_last_name_parent() == false || validate_dob_parent() == false || isMobile_parent() == false ||  validate_email_parent() == false || validate_parent_emirates_id() == false || validate_user_id() == false || validate_password() == false || validate_confirm_password() == false || isPasswordSame()==false || validate_exist_parent()==false   ) 
		{
			return false;
		} 
		
	}	
	
	
  function validate_exist_parent()
   {
	   $.ajax({
				type: "POST",
				url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/parents/is_exist_parent_user_id",
				dataType: "text",
				data: {
					 parent_id_enc  : '<?=$parent_id_enc?>',
					 parent_user_id : $("#parent_user_id").val(),
					is_ajax: true
				},
				success: function(data) {
					    var temp = new String();
						temp = data;
						temp = temp.trim();
						if (temp=='Y') {
							my_alert("User Id is already exist. please try another one.", 'red');
							$("#pre-loader").hide();
							return false;
						}else{
						   is_exist();
						}
					
					},
					error: function() {
						$("#pre-loader").hide();
					}, 
					complete: function() {
						$("#pre-loader").hide();
					}
				});
   }
	
	
	
	
	function is_exist() {
		$("#pre-loader").show();
		
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/is_exist_student",
			data: {
				student_id_enc: "<?=$student_id_enc?>",
				first_name: $('#first_name').val(),
				last_name: $('#last_name').val(),
				tbl_class_id: $('#tbl_class_id').val(),
				tbl_emirates_id: $('#tbl_emirates_id').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
					refresh_page = "N";
					my_alert("Student is already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_create();
				}
			
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	
    //edit student
	function ajax_validate_edit() {
		if ( validate_first_name() == false || validate_last_name() == false || validate_emirates_id() == false || validate_dob() == false || isMobile() == false  || validate_email() == false || validate_country() == false || validate_emirates_id_father() == false || validate_emirates_id_mother() == false || validate_emirates_id_mother() == false  || validate_emirates_id_parent_same()== false || validate_emirates_id_parent() == false || validate_class() == false || validate_academic_year() == false  ||validate_exist_edit_student()==false ) {
			
			return false;
		} else {
					
					$.ajax({
					type: "POST",
					url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/is_exist_parent_info",
					dataType: "json",
					data: {
						emirates_id_father: $('#emirates_id_father').val(),
						emirates_id_mother: $('#emirates_id_mother').val(),
						assigned_parent_id_enc: $('#assigned_parent_id_enc').val(),
						is_ajax: true
					},
					success: function(data) {
					   $("#first_name_parent").val(data[0]['first_name']);
					   $("#first_name_parent_ar").val(data[0]['first_name_ar']);
					   $("#last_name_parent").val(data[0]['last_name']);
					   $("#last_name_parent_ar").val(data[0]['last_name_ar']);
					   $("#dob_month_parent").val(data[0]['dob_month']);
					   $("#dob_day_parent").val(data[0]['dob_day']);
					   $("#dob_year_parent").val(data[0]['dob_year']);
					   $("#dob_gender_parent").val(data[0]['dob_gender']);
					   $("#email_parent").val(data[0]['email']);
					   $("#parent_user_id").val(data[0]['user_id']);
					   $("#emirates_id_parent").val(data[0]['emirates_id']);
					   
					    if(data[0]['emirates_id']!='')
			    				$("#emirates_id_parent").val(data[0]['emirates_id']);
			  			else
			   					$("#emirates_id_parent").val($('#emirates_id_father').val());
					   
					   if(data[0]['mobile']!='')
					   {
						   var str = data[0]['mobile'];
						   str = str.replace("+971", "");
					   }
					   $("#mobile_parent").val($.trim(str));
					   $("#password").val('123456');
					   $("#confirm_password").val('123456');
					},
					error: function() {
						$("#pre-loader").hide();
					}, 
					complete: function() {
						$("#pre-loader").hide();
					}
				});
			
			
		   }
	} 
	
	function ajax_validate_parent_edit() {
		if (validate_first_name_parent() == false || validate_last_name_parent() == false || validate_dob_parent() == false || isMobile_parent() == false ||  validate_email_parent() == false || validate_parent_emirates_id() == false || validate_user_id() == false || validate_edit_password() == false || validate_edit_confirm_password() == false  ) 
		{
			return false;
		} 
		else{
			is_exist_edit();
			
		}
	}	
	
	
  /************************************* START STUDENT VALIDATION *******************************/
   function validate_exist_student()
   {
	   $.ajax({
				type: "POST",
				url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/is_exist_student",
				dataType: "text",
				data: {
					 student_id_enc: '<?=$student_id_enc?>',
					 first_name    : $('#first_name').val(),
				     last_name     : $('#last_name').val(),
				     tbl_class_id  : $('#tbl_class_id').val(),
					is_ajax: true
				},
				success: function(data) {
					    var temp = new String();
						temp = data;
						temp = temp.trim();
						if (temp=='Y') {
							my_alert("Student is already exist.", 'red');
							$("#pre-loader").hide();
							return false;
						}else{
						   $("#li-tab2").removeClass();
			               $("#tab2").click();
						   return true;
						}
					
					},
					error: function() {
						$("#pre-loader").hide();
					}, 
					complete: function() {
						$("#pre-loader").hide();
					}
				});
   }
   
   
    function validate_exist_edit_student()
   {
	   $.ajax({
				type: "POST",
				url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/is_exist_student",
				dataType: "text",
				data: {
					 student_id_enc: $('#student_id_enc').val(),
					 first_name    : $('#first_name').val(),
				     last_name     : $('#last_name').val(),
				     tbl_class_id  : $('#tbl_class_id').val(),
					 tbl_emirates_id: $('#tbl_emirates_id').val(),
					is_ajax: true
				},
				success: function(data) {
					    var temp = new String();
						temp = data;
						temp = temp.trim();
						if (temp=='Y') {
							my_alert("Student is already exist.", 'red');
							$("#pre-loader").hide();
							return false;
						}else{
						    $("#li-tab2").removeClass();
			                $("#tab2").click();
						   return true;
						}
					
					},
					error: function() {
						$("#pre-loader").hide();
					}, 
					complete: function() {
						$("#pre-loader").hide();
					}
				});
   }


   function validate_first_name() {
		var regExp = / /g;
		var str = $("#first_name").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("The First Name [En] is blank. Please write First Name [En].")
			$("#first_name").val('');
			$("#first_name").focus();
		return false;
		}
		var regExp = / /g;
		var str = $("#first_name_ar").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("The First Name [Ar] is blank. Please write First Name [Ar].")
			$("#first_name_ar").val('');
			$("#first_name_ar").focus();
		return false;
		}
	}
	
	function validate_last_name() {
		var regExp = / /g;
		var str = $("#last_name").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("The Last Name [En] is blank. Please write Last Name [En].")
			$("#last_name").val('');
			$("#last_name").focus();
		return false;
		}
		var regExp = / /g;
		var str = $("#last_name_ar").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("The Last Name [Ar] is blank. Please write Last Name [Ar].")
			$("#last_name_ar").val('');
			$("#last_name_ar").focus();
		return false;
		}
	}
	
	
	
	function isMobile() {
		var strPhone = $.trim($("#mobile").val());
		if($.trim(strPhone)== "") {
			return true;
		}else{
				if (strPhone.length < 8 || strPhone.length > 10) {
					my_alert("Please enter valid mobile number.");
					$("#mobile").focus();
					return false;
			    }

				for (var i = 0; i < strPhone.length; i++) {
				var ch = strPhone.substring(i, i + 1);
					if  (ch < "0" || "9" < ch) {
						my_alert("The mobile number in digits only, Please re-enter your valid mobile number");
						$("#mobile").focus();
						return false;
					}
				}
		}
	 return true;
	}



	function validate_dob() {
		var month_index 	= $("#dob_month").val();
		var day_index	  = $("#dob_day").val();
		var year_index 	 = $("#dob_year").val();
		if (month_index == 0 && day_index == 0 && year_index ==0) {
			return true;
		}else if(month_index == 0 || day_index == 0 || year_index ==0) {
			my_alert("Please select Date of Birth.")
			return false		
		}
	}
	
		
	function validate_email() {
		var regExp = / /g;
		var str = $("#email").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
		     return true;	
		}
		if (!isNaN(str)) {
			my_alert("Invalid Email Id.");
			$("#email").focus();
			$("#email").select();
			return false;
		}

		if(str.indexOf('@', 0) == -1) {
			my_alert("Invalid Email Id.");
			$("#email").focus();
			$("#email").select();
			return false;
		}

	 return true;
	}


    
    function validate_emirates_id() {
		var regExp = / /g;
		var str = $("#tbl_emirates_id").val(); 
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			  my_alert("Please provide Student Emirates Id");  
			  return false;  
		}

		  var emrno = /^\(?([0-9]{3})\)?[-]?([0-9]{4})[-]?([0-9]{7})[-]?([0-9]{1})$/;  
		  if(str.match(emrno))  
		  {  
			   return true;    
		  }  
		  else  
		  {  
			   my_alert("Please provide correct Student Emirates Id");  
			   return false;  
		 }  
	}

	function validate_emirates_id_father() {
		var regExp = / /g;
		var str = $("#emirates_id_father").val(); 
		str = str.replace(regExp,'');
		if (str.length <= 0) {
		    return true;
		}

		  var emrno = /^\(?([0-9]{3})\)?[-]?([0-9]{4})[-]?([0-9]{7})[-]?([0-9]{1})$/;  
		  if(str.match(emrno))  
		  {  
			   return true;    
		  }  
		  else  
		  {  
			   my_alert("Please provide correct Father Emirates Id");  
			   return false;  
		 }  
	}



	function validate_emirates_id_mother() {
		var regExp = / /g;
		var str = $("#emirates_id_mother").val();  
		str = str.replace(regExp,'');
		if (str.length <= 0) {
		   return true;
		}

		 var emrno = /^\(?([0-9]{3})\)?[-]?([0-9]{4})[-]?([0-9]{7})[-]?([0-9]{1})$/;  
		if(str.match(emrno))  
		{  
			return true;    
		}  
		else  
		{  
			my_alert("Please provide correct Mother Emirates ID."); 
			return false;  
		}  
	}

   function validate_emirates_id_parent_same(){
	   var regExp = / /g;
		var strFather = $("#emirates_id_father").val();  
		strFather = strFather.replace(regExp,'');
		if (strFather.length <= 0) {
		   return true;
		}
		var strMother = $("#emirates_id_mother").val();  
		strMother = strMother.replace(regExp,'');
		if (strMother.length <= 0) {
		   return true;
		}	
		
		
		if (strMother.length > 0 && strFather.length > 0)
		{
			if(strFather == strMother)
			{
				my_alert("Parent's Emirates IDs are same. Please provide correct Father and Mother Emirates ID"); 
				return false; 
			}
			
		}
   }


   function validate_emirates_id_parent() {
		var regExp = / /g;
		var strFather = $("#emirates_id_father").val();  
		strFather = strFather.replace(regExp,'');
		var strMother =  $("#emirates_id_mother").val();   
		strMother = strMother.replace(regExp,'');
		if (strFather.length <= 0 && strMother.length <= 0) {
			my_alert("The Emirates ID for Father or Mother is mandatory. Please provide Father or Mother Emirates ID.")  
			$("#emirates_id_father").focus();
			return false;
		}
		else if(strFather==strMother)
		{
			my_alert("Please provide Father or Mother Emirates ID.")
			$("#emirates_id_father").focus();
			return false;
		}
	}


	function validate_country() {
		var regExp = / /g;
		var str = $('#country').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Nationality is mandatory. Please choose Nationality to proceed.")
		return false;
		}
	}

   function validate_academic_year() {
		var regExp = / /g;
		var str = $('#tbl_academic_year_id').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Academic year is mandatory. Please choose academic year to proceed.")
		return false;
		}
	}


   function validate_class() {
		var class_index = $('#tbl_class_id').val(); 
		if (class_index == '') 
		{
			my_alert("The Class is mandatory. Please select Class.");
			return false;
		}
	return true;
	}
  /*********************************** END STUDENT VALIDATION SECTION *********************/

	
  /*********************************** PARENT VALIDATION SECTION **************************/
	
	 function validate_first_name_parent() {
		var regExp = / /g;
		var str = $("#first_name_parent").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("The First Name [En] is blank. Please write First Name [En].")
			$("#first_name_parent").val('');
			$("#first_name_parent").focus();
		return false;
		}
		var regExp = / /g;
		var str = $("#first_name_parent_ar").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("The First Name [Ar] is blank. Please write First Name [Ar].")
			$("#first_name_parent_ar").val('');
			$("#first_name_parent_ar").focus();
		return false;
		}
	}
	
	function validate_last_name_parent() {
		var regExp = / /g;
		var str = $("#last_name_parent").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("The Last Name [En] is blank. Please write Last Name [En].")
			$("#last_name_parent").val('');
			$("#last_name_parent").focus();
		return false;
		}
		var regExp = / /g;
		var str = $("#last_name_parent_ar").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("The Last Name [Ar] is blank. Please write Last Name [Ar].")
			$("#last_name_parent_ar").val('');
			$("#last_name_parent_ar").focus();
		return false;
		}
	}
	
	function validate_dob_parent() {
		var month_index 	= $("#dob_month_parent").val();
		var day_index	  = $("#dob_day_parent").val();
		var year_index 	 = $("#dob_year_parent").val();
		if (month_index == 0 && day_index == 0 && year_index ==0) {
			return true;
		}else if(month_index == 0 || day_index == 0 || year_index ==0) {
			my_alert("Please select Date of Birth.")
			return false		
		}
	}
	
	function isMobile_parent() {
		var strPhone = $("#mobile_parent").val();
		if($.trim(strPhone)== "") {
			return true;
		}else{
				if (strPhone.length < 8 || strPhone.length > 10) {
					my_alert("Please enter valid mobile number.");
					$("#mobile_parent").focus();
					return false;
			    }

				for (var i = 0; i < strPhone.length; i++) {
				var ch = strPhone.substring(i, i + 1);
					if  (ch < "0" || "9" < ch) {
						my_alert("The mobile number in digits only, Please re-enter your valid mobile number");
						$("#mobile_parent").focus();
						return false;
					}
				}
		}
	 return true;
	}
		
	function validate_email_parent() {
		var regExp = / /g;
		var str = $("#email_parent").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
		     return true;	
		}
		if (!isNaN(str)) {
			my_alert("Invalid Email Id.");
			$("#email_parent").focus();
			$("#email_parent").select();
			return false;
		}

		if(str.indexOf('@', 0) == -1) {
			my_alert("Invalid Email Id.");
			$("#email_parent").focus();
			$("#email_parent").select();
			return false;
		}

	 return true;
	}

   function validate_parent_emirates_id() {
	    var regExp = / /g;
		var str = $("#emirates_id_parent").val();  
		str = str.replace(regExp,'');
		if (str.length <= 0) {
		    my_alert("The Emirates ID for Parent is mandatory. Please write Parent’s Emirates ID.")
			$("#emirates_id_parent").focus();
			return false;
		}

		var emrno = /^\(?([0-9]{3})\)?[-]?([0-9]{4})[-]?([0-9]{7})[-]?([0-9]{1})$/;  
		if(str.match(emrno))  
		{  
			return true;    
		}  
		else  
		{  
			my_alert("Please write valid Parent’s Emirates ID"); 
			return false;  
		}  	
	}

	
	function validate_user_id() {
		var str =  $.trim($("#parent_user_id").val());
		if ( str=="" ) {
			my_alert("The User ID is blank. Please write User ID.");
			$("#parent_user_id").focus();
			return false;
		}
		if (str.length < 6 ) {
			my_alert("The User ID should be greater than 5 characters.");
			$("#parent_user_id").focus();
			return false;
		}
		/*if (!isNaN(str)) {
			my_alert("The User ID have only letters & digits, Please re-enter User ID");
			$("#parent_user_id").focus();
			return false;
		}*/
	   return true;
	 }
	 
    /* Password */
	function validate_password() {
		var str = $('#password').val(); 
		if (str == "") {
			my_alert("The Password field is blank. Please enter Password.");
			$('#password').focus();
			return false;
		}
		if($('#password').val()!="") {
			var str = $('#password').val();
			var regExp = / /g;
			var tmp = $('#password').val();
			tmp = tmp.replace(regExp,'');
			if (tmp.length <= 0) {
				my_alert("Enter valid Password.");
				$('#password').focus();
			return false;
			}	
		}
		if (str.length < 6) {
			my_alert("The Password should be greater than 5 Characters.");
			$('#password').focus();
			$('#password').select();
			return false;
		}

	return true;
	}


	/* Retype Password */
	function validate_confirm_password() {
		var str = $('#confirm_password').val();
		if (str == "") {
			my_alert("The Confirm Password field is blank. Please Retype Password.");
			$('#confirm_password').focus();
			return false;
		}
		if($('#confirm_password').val()!="") {
				var str = $('#confirm_password').val();
				var regExp = / /g;
				var tmp = $('#confirm_password').val();
				tmp = tmp.replace(regExp,'');
				if (tmp.length <= 0) {
					my_alert("Enter valid Confirm Password.");
					$('#confirm_password').focus();
					return false;
				}	
			
		}
	return true;
	}

	/* Check both password */
	function isPasswordSame() {
		var str1 = $('#password').val();
		var str2 = $('#confirm_password').val();
		if (str1 != str2) {
			my_alert("Password mismatch, Please Retype same Passwords in both fields.");
			$('#confirm_password').focus();
			return false;
		}
	return true;
	}




      /* Password */
	function validate_edit_password() {
		var str = $('#password').val(); 
		if ($.trim(str) == "") {
			return true;
		}
		if($('#password').val()!="") {
			var str = $('#password').val();
			var regExp = / /g;
			var tmp = $('#password').val();
			tmp = tmp.replace(regExp,'');
			if (tmp.length <= 0) {
				my_alert("Enter valid Password.");
				$('#password').focus();
			return false;
			}	
		}
		if (str.length < 6) {
			my_alert("The Password should be greater than 5 Characters.");
			$('#password').focus();
			$('#password').select();
			return false;
		}

	return true;
	}


	/* Retype Password */
	function validate_edit_confirm_password() {
		var strPass = $('#password').val(); 
		if ($.trim(strPass) == "") {
			return true;
		}else{
			var str = $('#confirm_password').val();
				if (str == "") {
					my_alert("The Confirm Password field is blank. Please Retype Password.");
					$('#confirm_password').focus();
					return false;
				}
		}
		if($('#confirm_password').val()!="") {
				var str = $('#confirm_password').val();
				var regExp = / /g;
				var tmp = $('#confirm_password').val();
				tmp = tmp.replace(regExp,'');
				if (tmp.length <= 0) {
					my_alert("Enter valid Confirm Password.");
					$('#confirm_password').focus();
					return false;
				}	
		}
	return true;
	}



</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>
<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> My Child<small> &nbsp;</small> </h1>
    <!--/HEADING--> 
    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>My Child</li>
    </ol>
    <!--/BREADCRUMB--> 
       <?php /*?><div class="col-sm-10" id="divPromo" > 
                        <?php if (count($rs_all_students)>0) { ?>
                             <label class="col-sm-4 control-label" for="tbl_class_id_promotion" style="text-align:right;">Promote To</label>
                              <div class="col-sm-3"> 
                              <select name="tbl_class_id_promotion" id="tbl_class_id_promotion" class="form-control">
                              <option value="">--Select Class --</option>
							  <?php
                                    for ($u=0; $u<count($classes_list); $u++) { 
                                        $tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
                                        $class_name             = $classes_list[$u]['class_name'];
                                        $class_name_ar          = $classes_list[$u]['class_name_ar'];
										$section_name           = $classes_list[$u]['section_name'];
                                        $section_name_ar        = $classes_list[$u]['section_name_ar'];
                                        if($tbl_sel_class_id == $tbl_class_id_u)
                                           $selClass = "selected";
                                         else
                                           $selClass = "";
                                  ?>
                                      <option value="<?=$tbl_class_id_u?>"  <?=$selClass?>  >
                                      <?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;
                                    <?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>   
                             </div>
                             <div class="col-sm-3">
                             <select name="tbl_academic_year_promotion" id="tbl_academic_year_promotion" class="form-control">
                                 <option value="">Select Year</option>
                              <?php
                                    for ($u=0; $u<count($academic_list); $u++) { 
                                        $tbl_academic_year_id_u = $academic_list[$u]['tbl_academic_year_id'];
                                        $academic_start         = $academic_list[$u]['academic_start'];
                                        $academic_end           = $academic_list[$u]['academic_end'];
										
                                        if($tbl_academic_year_id   == $tbl_academic_year_id_u)
                                           $selClass = "selected";
                                         else
                                           $selClass = "";
                                  ?>
                                      <option value="<?=$tbl_academic_year_id_u?>"  <?=$selClass?> >
                                      <?=$academic_start?>&nbsp;-&nbsp;<?=$academic_end?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>
                            
                          </div>
                          
                          <div class="col-sm-2"> 
                          <button name="show" type="button" class="btn btn-success" id="submit_promotion" onclick="valueCheckedForPagesManagement();" >Submit Promotion</button> 
                          </div>  
                            <?php } ?> 
                    </div><?php */?>
    
    <div style="clear:both"></div>
  </section>
      <link href="<?=HOST_URL?>/assets/admin/dist/css/jquery-ui.css" rel="stylesheet">
      <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-1.11.1.js"></script>
      <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-ui.js"></script>
      <link href="<?=HOST_URL?>/assets/admin/dist/css/uploadfile.min.css" rel="stylesheet">

      
  

  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {
			
	
	?>
        <!--Edit-->
   <script>
 $( function() {
    $( "#tabs" ).tabs();
  } );
  
 
</script>

        
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Student</h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/all_students"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

              <style>
	 .disabled {
		pointer-events:none; //This makes it not clickable
		opacity:0.6;         //This grays it out to look disabled
	}
	 </style>  
     <style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url('<?=IMG_PATH?>/delete.jpg');
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
		clear:both;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
	#overlay_container {
		position:relative;
	}
	#overloading {
		background-image:url('<?=IMG_PATH?>/preloader/preloader_2.gif');
		background-repeat:no-repeat;
		background-position:center;
		background-color:#CCC;
		position:absolute;
		left:0px;
		top:0px;
		opacity: 0.3;
		z-index: 10000;
	}
	#div_listing_container {
		display:none;	
	}
	.d_d_text {
		color:#745156;
		font-size:20px;
			
	}
	.ajax-upload-dragdrop {
		margin:auto;
		margin-bottom:10px;
		width:700px !important;
	}
	.ajax-file-upload-statusbar {
		margin:auto;
		margin-top:10px;
	}
	.ajax-file-upload {
		height:31px;
	}
	
	
	 #tabs-1{  
	    overflow-y:scroll; overflow-x:none;
	}

    #tabs-2{
		overflow-y:scroll; overflow-x:none;
	}
				  
  .ui-tabs-active{
		border-color:#efca86  !important;
   }
					 
	.ui-tabs .ui-tabs-nav li {
		float:left;
		font-size: 16px;
        font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif;
  }
  label{
	  display: inline-block;
      font-weight: 700;
  }
  
  .ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button {
    font-family:"Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
    font-size: 14px;
}
  
  .ui-widget{
	 font-size: 16px;
     font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
  }
  .form-control{
	 font-size: 14px; 
  }
</style>         
         <?php
		 	$tbl_student_id            = $student_obj[0]['tbl_student_id'];		
			$first_name                = $student_obj[0]['first_name'];
			$last_name                 = $student_obj[0]['last_name'];
			$first_name_ar             = $student_obj[0]['first_name_ar'];
			$last_name_ar         	   = $student_obj[0]['last_name_ar'];		
			$dob_month                 = $student_obj[0]['dob_month'];
			$dob_day                   = $student_obj[0]['dob_day'];
			$dob_year                  = $student_obj[0]['dob_year'];
			$gender                    = $student_obj[0]['gender'];		
			$mobile                    = $student_obj[0]['mobile'];
			$email                     = $student_obj[0]['email'];
			$country                   = $student_obj[0]['country'];
			$picture                   = $student_obj[0]['file_name_updated'];
			$emirates_id_father        = $student_obj[0]['emirates_id_father'];		
			$emirates_id_mother        = $student_obj[0]['emirates_id_mother'];
			$tbl_sel_academic_year_id  = $student_obj[0]['tbl_academic_year_id'];
			$tbl_sel_class_id          = $student_obj[0]['tbl_class_id'];
			$mobile                    = str_replace("+971",'',$mobile);
			$tbl_emirates_id           = $student_obj[0]['tbl_emirates_id'];	
			
			if($picture<>"") // class="img-circle"
				$pic_path           =   '<img width="100" height="80" src="'.IMG_PATH_STUDENT.'/'.$picture.'"  />';
			else
				$pic_path           =   '<img width="100" height="80" src="'.IMG_PATH_STUDENT.'/no_img.png"  
										style="border-color:1px solid #7C858C !important;" />';
										
			$tbl_assigned_parent_id	=	isset($assigned_parent_obj[0]['tbl_parent_id'])? $assigned_parent_obj[0]['tbl_parent_id']:'' ;

			
		 ?>       
                
                
   	     <div id="tabs" >
          <ul>
            <li id="li-tab1" ><a href="#tabs-1" id="tab1">STUDENT INFORMATION</a></li>
           <li id="li-tab2" class="disabled" ><a  href="#tabs-2" id="tab2">PARENT INFORMATION</a></li> <!---->
          </ul>     
          <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">

                <div id="tabs-1">
                <!-- /.box-header -->
                <!-- form start -->
                  <div class="box-body">
                  
                   
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="first_name">First Name [En]<span style="color:#F30; padding-left:2px;">*</span></label>
                      <div class="col-sm-10">
                        <input type="text" placeholder="First Name[En]" id="first_name" name="first_name" class="form-control" value="<?=$first_name?>">
                      </div>
                      
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="first_name_ar">First Name [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="First Name[Ar]" id="first_name_ar" name="first_name_ar" class="form-control" dir="rtl" value="<?=$first_name_ar?>" >
                      </div>
                    </div>
                    
                      <div class="form-group">
                      <label class="col-sm-2 control-label" for="last_name">Last Name [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Last Name[En]" id="last_name" name="last_name" class="form-control" value="<?=$last_name?>" >
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="last_name_ar">Last Name [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Last Name[Ar]" id="last_name_ar" name="last_name_ar" class="form-control" dir="rtl" value="<?=$last_name_ar?>"  >
                      </div>
                    </div>
                    
                 
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="category_name_ar">DOB</label>
    
                      <div class="col-sm-2">
                        <select name="dob_month" id="dob_month"  class="form-control" tabindex="6">
                                      	<option value="">--Month--</option>
                                      	<?php for ($m=1; $m<=12; $m++) { ?>
                                      	<option value="<?=$m?>" <?php if ($dob_month == $m) {echo "selected";}?> ><?=$m?></option>
                                        <?php } ?>
                        </select>
                     </div>
                     <div class="col-sm-2">
                                      <?php if (!isset($dob_day) || trim($dob_day) == "") {$dob_day = '';}?>
                                      <select name="dob_day" id="dob_day" tabindex="7" class="form-control">
                                        <option value="">--Day--</option>
                                      	<?php for ($d=1; $d<=31; $d++) { ?>
                                      	<option value="<?=$d?>" <?php if ($dob_day == $d) {echo "selected";}?> ><?=$d?></option>
                                        <?php } ?>
                    </select>
                    </div>
                    <div class="col-sm-2">
                                      <?php if (!isset($dob_year) || trim($dob_year) == "") {$dob_year = '';}?>
                                      <select name="dob_year" id="dob_year" tabindex="8" class="form-control">
                                        <option value="">--Year--</option>

                                      	<?php for ($y=1950; $y<=date('Y'); $y++) { ?>
                                      	<option value="<?=$y?>" <?php if ($dob_year == $y) {echo "selected";}?> ><?=$y?></option>
                                        <?php } ?>
                                      </select>              
                    </div>  
                    </div>
                    
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="gender">Gender<span style="color:#F30; padding-left:2px;">*</span></label>
                       <div class="col-sm-10">
                        <label>
                          <input type="radio" id="gender" name="gender" value="male" class="minimal" <?php if ($gender == 'male'){?> checked="checked" <?php } ?>  >
                          Male
                        </label>
                        &nbsp;
                        <label>
                          <input type="radio" id="gender1" name="gender" value="female" class="minimal"  <?php if ($gender == 'female'){?> checked="checked" <?php } ?> >
                          Female
                        </label>
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="tbl_emirates_id">Emirates Id [Student]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Emirates Id [Student]" id="tbl_emirates_id" name="tbl_emirates_id" class="form-control" value="<?=$tbl_emirates_id?>" >
                      </div>
                    </div>
                    
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="mobile">Parent's Mobile</label>
    
                      <div class="col-sm-10">
                       <span style="position:absolute; padding-left:20px; padding-top:5px;"> +971</span><input type="text" placeholder="" id="mobile" name="mobile" class="form-control" style="padding-left:60px;" value="<?=trim($mobile)?>">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="email">Email</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Email" id="email" name="email" class="form-control" value="<?=$email?>" >
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="country">Nationality</label>
    
                      <div class="col-sm-10">
                      
                                      <select name="country" id="country" tabindex="8" class="form-control">
                                      	<?php for ($c=0; $c<count($countries_list); $c++) { ?>
                                      	<option  value="<?=$countries_list[$c]['country_id']?>"  <?php if($country==$countries_list[$c]['country_id']) { ?> selected="selected" <?php } ?> ><?=$countries_list[$c]['country_name']?></option>
                                        <?php } ?>
                                      </select>         
                      </div>
                    </div>
                    
                    
                      <div class="form-group">
                      <label class="col-sm-2 control-label" for="emirates_id_father">Emirates Id [Father]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Emirates Id [Father]" id="emirates_id_father" name="emirates_id_father" class="form-control" value="<?=$emirates_id_father?>" >
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="emirates_id_mother">Emirates Id [Mother]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Emirates Id [Mother]" id="emirates_id_mother" name="emirates_id_mother" class="form-control" value="<?=$emirates_id_mother?>" >
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="tbl_academic_year_id">Academic Year<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                       <select name="tbl_academic_year_id" id="tbl_academic_year_id" class="form-control" >
                              <?php
                                    for ($a=0; $a<count($academic_list); $a++) { 
                                        $tbl_academic_year_id    = $academic_list[$a]['tbl_academic_year_id'];
                                        $academic_start          = $academic_list[$a]['academic_start'];
                                        $academic_end            = $academic_list[$a]['academic_end'];
									    echo $tbl_sel_academic_year_id;
                                  ?>
                                      <option value="<?=$tbl_academic_year_id?>" <?php if($tbl_academic_year_id==$tbl_sel_academic_year_id) { ?> selected="selected" <?php } ?> >
                                      <?=$academic_start?>&nbsp;-&nbsp;<?=$academic_end?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>
                      </div>
                    </div>
                    
                 
                    
                    <div class="form-group">
                     <label class="col-sm-2 control-label" for="tbl_class_id">Class<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-10">
                              <select name="tbl_class_id" id="tbl_class_id" class="form-control">
                              <option value="">--Select Class --</option>
							  
							  <?php
                                    for ($u=0; $u<count($classes_list); $u++) { 
                                        $tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
                                        $class_name             = $classes_list[$u]['class_name'];
                                        $class_name_ar          = $classes_list[$u]['class_name_ar'];
										$section_name           = $classes_list[$u]['section_name'];
                                        $section_name_ar        = $classes_list[$u]['section_name_ar'];
                                        if($tbl_sel_class_id == $tbl_class_id_u)
                                           $selClass = "selected";
                                         else
                                           $selClass = "";
                                  ?>
                                      <option value="<?=$tbl_class_id_u?>"  <?=$selClass?>  >
                                      <?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;
                                    <?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>
                   </div>
                   </div>
                    
                  
                       <div class="form-group">
                      <label class="col-sm-2 control-label" for="picture">Picture</label>
    
                      <div class="col-sm-10">
                        <!--File Upload START-->
                            <style>
                            #advancedUpload {
                                padding-bottom:0px;
                            }
                            </style>
                                 
                            <div id="advancedUpload">Upload File</div>
                            
                            <div id="uploaded_items" >
                                <div id="div_listing_container" class="listing_container" style="display:block">	            
										<?php
                                            if (trim($pic_path) != "") {
                                        ?>
                                                    <div id='<?=$tbl_uploads_id?>' class='box-header with-border'>
                                                      <div class='box-title'><?=$pic_path?></div>
                                                      <div class='box-tools'> <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick="confirm_delete_img_popup('<?=$tbl_uploads_id?>')">
                                                        </button>
                                                      </div>
                                                    </div>
											<style>
												.ajax-upload-dragdrop {
													display:none;	
												}
                                            </style>        
                                        <?php		
                                            }
                                        ?>
                                </div>        
                            </div>
                        <!--File Upload END-->
                      </div>
                   
                    
                    </div>
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate_edit()">Continue</button>
                     <input type="hidden" name="student_id_enc" id="student_id_enc" value="<?=$tbl_student_id?>" />
                     <input type="hidden" name="assigned_parent_id_enc" id="assigned_parent_id_enc" value="<?=$tbl_assigned_parent_id?>" />
                     
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
               
                </div>
      
      
               <div id="tabs-2">
                <!-- /.box-header -->
                <!-- form start -->
                  <div class="box-body">
                  
                    <p  style="padding-left:10px;"><strong>Parent Details</strong></p>
                    <br />
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="first_name_parent">Parent First Name [En]<span style="color:#F30; padding-left:2px;">*</span></label>
                      <div class="col-sm-10">
                        <input type="text" placeholder="Parent First Name[En]" id="first_name_parent" name="first_name_parent" class="form-control">
                      </div>
                      
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="first_name_parent_ar">Parent First Name [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Parent First Name[Ar]" id="first_name_parent_ar" name="first_name_parent_ar" class="form-control" dir="rtl">
                      </div>
                    </div>
                    
                      <div class="form-group">
                      <label class="col-sm-2 control-label" for="last_name_parent">Parent Last Name [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Parent Last Name[En]" id="last_name_parent" name="last_name_parent" class="form-control">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="last_name_parent_ar">Parent Last Name [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Parent Last Name[Ar]" id="last_name_parent_ar" name="last_name_parent_ar" class="form-control" dir="rtl">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="dob_month_parent"> DOB</label>
    
                      <div class="col-sm-2">
                        <select name="dob_month_parent" id="dob_month_parent"  class="form-control" tabindex="6">
                                      	<option value="">--Month--</option>
                                      	<?php for ($m=1; $m<=12; $m++) { ?>
                                      	<option value="<?=$m?>" <?php if ($dob_month_parent == $m) {echo "selected";}?> ><?=$m?></option>
                                        <?php } ?>
                        </select>
                     </div>
                     <div class="col-sm-2">
                                      <?php if (!isset($dob_day_parent) || trim($dob_day_parent) == "") {$dob_day = '';}?>
                                      <select name="dob_day_parent" id="dob_day_parent" tabindex="7" class="form-control">
                                        <option value="">--Day--</option>
                                      	<?php for ($d=1; $d<=31; $d++) { ?>
                                      	<option value="<?=$d?>" <?php if ($dob_day_parent == $d) {echo "selected";}?> ><?=$d?></option>
                                        <?php } ?>
                    </select>
                    </div>
                    <div class="col-sm-2">
                                      <?php if (!isset($dob_year_parent) || trim($dob_year_parent) == "") {$dob_year = '';}?>
                                      <select name="dob_year_parent" id="dob_year_parent" tabindex="8" class="form-control">
                                        <option value="">--Year--</option>

                                      	<?php for ($y=1950; $y<=date('Y'); $y++) { ?>
                                      	<option value="<?=$y?>" <?php if ($dob_year_parent == $y) {echo "selected";}?> ><?=$y?></option>
                                        <?php } ?>
                                      </select>              
                    </div>  
                    </div>
                    
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="gender_parent">Relationship<span style="color:#F30; padding-left:2px;">*</span></label>
                       <div class="col-sm-10">
                        <label>
                          <input type="radio" id="gender_parent" name="gender_parent" value="male" class="minimal" checked="checked"  >
                          Father
                        </label>
                        &nbsp;
                        <label>
                          <input type="radio" id="gender_parent1" name="gender_parent" value="female" class="minimal"  >
                          Mother
                        </label>
                         &nbsp;
                        <label>
                          <input type="radio" id="gender_parent1" name="gender_parent" value="other" class="minimal"  >
                          Other
                        </label>
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="mobile_parent">Parent's Mobile</label>
    
                      <div class="col-sm-10">
                       <span style="position:absolute; padding-left:20px; padding-top:5px;"> +971</span> 
                       <input type="text" placeholder="Parent's Mobile" id="mobile_parent" name="mobile_parent" class="form-control" style="padding-left:60px;">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="email_parent">Parent's Email</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Parent's Email" id="email_parent" name="email_parent" class="form-control">
                      </div>
                    </div>
                    
                  
                    
                    
                      <div class="form-group">
                      <label class="col-sm-2 control-label" for="emirates_id_parent">Parent Emirates Id <span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Parent Emirates Id" id="emirates_id_parent" name="emirates_id_parent" class="form-control">
                      </div>
                    </div>
                    
                   
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="parent_user_id">User Id<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="User Id " id="parent_user_id" name="parent_user_id" class="form-control" value="" ><!--readonly="readonly" -->
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="password">Password<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="password" placeholder="Password" id="password" name="password" class="form-control">
                      </div>
                    </div>
                    
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="confirm_password">Confirm Password <span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="password" placeholder="Confirm Password" id="confirm_password" name="confirm_password" class="form-control">
                      </div>
                    </div>
                    
                    </div>
                    
                    <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate_parent_edit()">Submit</button>
                    
                     <input type="hidden" name="parent_id_enc" id="parent_id_enc"  />
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->  
                  
               </div>
                
               
            
       </form>
      </div>
                
    </div>
		        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/parent/parent_user/mychild";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
    
  
 <link href="<?=HOST_URL?>/assets/admin/dist/css/jquery-ui.css" rel="stylesheet">
 <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-1.11.1.js"></script>
 <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-ui.js"></script>
  <script>
  $( function() {
		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
				
				/* var order = $("#tabledivbody").sortable("serialize");
				
				alert(order);
				
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/your/url/here'
				});*/
				
				
				
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	
				
				
				
				
				
			}
	  } );
  
  } );
  </script> 
  
  
  
  <!--File Upload START-->
<link href="<?=HOST_URL?>/assets/admin/dist/css/uploadfile.min.css" rel="stylesheet">
<script>
 $( function() {
    $( "#tabs" ).tabs();
  } );
  
 
</script>
<style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url('<?=IMG_PATH?>/delete.jpg');
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
		clear:both;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
	#overlay_container {
		position:relative;
	}
	#overloading {
		background-image:url('<?=IMG_PATH?>/preloader/preloader_2.gif');
		background-repeat:no-repeat;
		background-position:center;
		background-color:#CCC;
		position:absolute;
		left:0px;
		top:0px;
		opacity: 0.3;
		z-index: 10000;
	}
	#div_listing_container {
		display:none;	
	}
	.d_d_text {
		color:#745156;
		font-size:20px;
			
	}
	.ajax-upload-dragdrop {
		margin:auto;
		margin-bottom:10px;
		width:700px !important;
	}
	.ajax-file-upload-statusbar {
		margin:auto;
		margin-top:10px;
	}
	.ajax-file-upload {
		height:31px;
	}
	
	
	 #tabs-1{  
	    overflow-y:scroll; overflow-x:none;
	}

    #tabs-2{
		overflow-y:scroll; overflow-x:none;
	}
				  
  .ui-tabs-active{
		border-color:#efca86  !important;
   }
					 
	.ui-tabs .ui-tabs-nav li {
		float:left;
		font-size: 16px;
        font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif;
  }
  label{
	  display: inline-block;
      font-weight: 700;
  }
  
  .ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button {
    font-family:"Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
    font-size: 14px;
}
  
  .ui-widget{
	 font-size: 16px;
     font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
  }
  .form-control{
	 font-size: 14px; 
  }
</style>
                
                           <!--Listing-->
                   
                           <ul class="row first" style=" padding:0 0 0 0; margin:0 0 40px 0;" >
							 <?php
                                for ($i=0; $i<count($rs_all_students); $i++) { 
                                    $id = $rs_all_students[$i]['id'];
                                    $tbl_student_id     = $rs_all_students[$i]['tbl_student_id'];
                                    $name               = ucfirst(trim($rs_all_students[$i]['first_name']))." ".ucfirst(trim($rs_all_students[$i]['last_name'])); 
									$name_ar            = $rs_all_students[$i]['first_name_ar']." ".$rs_all_students[$i]['last_name_ar'];
									$pic                = $rs_all_students[$i]['file_name_updated'];
									$class_name         = $rs_all_students[$i]['class_name'];
                                    $class_name_ar      = $rs_all_students[$i]['class_name_ar'];
									$section_name       = $rs_all_students[$i]['section_name'];
									$section_name_ar    = $rs_all_students[$i]['section_name_ar'];
									
									if($pic<>"") // class="img-circle"
										$pic_path           =   IMG_PATH_STUDENT.'/'.$pic;
									else
										$pic_path           =   IMG_PATH_STUDENT.'/no_img.png';
                                ?>		
                             <li class="col-lg-2 col-md-4 col-sm-3 col-xs-4 col-xxs-12 bspHasModal" style="border: 3px solid #d2d6de; background-color:#f9fafc; cursor:pointer;  list-style:none; margin:10px 10px 0px 10px; text-align:center;  width: 100%; height:350px !important; ">
                                <?php /*?><input type="checkbox"  style="float:right; position:absolute; right:10px; top:0;" name="option" id="option<?=$i?>" value="<?=$tbl_student_id?>" /><?php */?>
                                <div style="float:left; width:100%; padding-top:40px;">
                                    <img  alt="<?=$name?>&nbsp;[<?=$name_ar?>]"  src="<?=$pic_path?>" width="200" height="200" style="color:#778070;  border-radius:20px;" title="Click On To Message"  /><br />
                                    <div style="float:left; width:100%; font-size:15px;"><a href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/child_details/child_id_enc/<?=$tbl_student_id?>/tab/points/offset/0#tab_points"><?=$name_ar?></a></div>
                                    <div style="clear:both;"></div>
                                    <div style="float:left; width:100%; font-size:15px;"><a href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/child_details/child_id_enc/<?=$tbl_student_id?>/tab/points/offset/0#tab_points"><?=$name?></a></div>
                                    
                                   <div style="float:left; width:50%"><?=$class_name?>&nbsp;<?=$section_name?></div>
                                   <div  style="float:left; width:50%"><?=$class_name_ar?>&nbsp;<?=$section_name_ar?></div>
                                </div>
                               <?php /*?> <div style="bottom:8px;float:left;position: absolute;width:100%; left:0;">
                                 <div style="width:65%; float:left;"> <button class="btn-01" type="button" style="cursor:pointer" onclick="get_behaviour_points('<?=$tbl_student_id?>', '<?=$name?>')">Add Behaviour Points</button></div>
                                 <div style="width:35%; float:left;"><span>Points</span><div style="border: 1px solid #2b649b; border-radius: 50%; height:30px; width:30px; float:right; margin-right:15px; box-shadow: 0 1px 0 rgba(255, 255, 255, 0.4) inset; color:#fff; background:#3c8dbc;"><?=$total_points?></div></div>
                                </div><?php */?>
                            </li>    
                           <?php } ?>
                        
                            
            <!--/Listing-->
    
            <!--Add or Create-->
              <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Student</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
     <style>
	 .disabled {
		pointer-events:none; //This makes it not clickable
		opacity:0.6;         //This grays it out to look disabled
	}
	 </style>           
                
                
                
   	  <div id="tabs" >
          <ul>
            <li id="li-tab1" ><a href="#tabs-1" id="tab1">STUDENT INFORMATION</a></li>
           <li id="li-tab2" class="disabled" ><a  href="#tabs-2" id="tab2">PARENT INFORMATION</a></li> <!---->
          </ul>     
          <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">

                <div id="tabs-1">
                <!-- /.box-header -->
                <!-- form start -->
                  <div class="box-body">
                  
                   
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="first_name">First Name [En]<span style="color:#F30; padding-left:2px;">*</span></label>
                      <div class="col-sm-10">
                        <input type="text" placeholder="First Name[En]" id="first_name" name="first_name" class="form-control">
                      </div>
                      
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="first_name_ar">First Name [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="First Name[Ar]" id="first_name_ar" name="first_name_ar" class="form-control" dir="rtl">
                      </div>
                    </div>
                    
                      <div class="form-group">
                      <label class="col-sm-2 control-label" for="last_name">Last Name [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Last Name[En]" id="last_name" name="last_name" class="form-control">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="last_name_ar">Last Name [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Last Name[Ar]" id="last_name_ar" name="last_name_ar" class="form-control" dir="rtl">
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="tbl_emirates_id">Emirates Id [Student]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Emirates Id [Student]" id="tbl_emirates_id" name="tbl_emirates_id" class="form-control" value="" >
                      </div>
                    </div>
                 
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="category_name_ar">DOB</label>
    
                      <div class="col-sm-2">
                        <select name="dob_month" id="dob_month"  class="form-control" tabindex="6">
                                      	<option value="">--Month--</option>
                                      	<?php for ($m=1; $m<=12; $m++) { ?>
                                      	<option value="<?=$m?>" <?php if ($dob_month == $m) {echo "selected";}?> ><?=$m?></option>
                                        <?php } ?>
                        </select>
                     </div>
                     <div class="col-sm-2">
                                      <?php if (!isset($dob_day) || trim($dob_day) == "") {$dob_day = '';}?>
                                      <select name="dob_day" id="dob_day" tabindex="7" class="form-control">
                                        <option value="">--Day--</option>
                                      	<?php for ($d=1; $d<=31; $d++) { ?>
                                      	<option value="<?=$d?>" <?php if ($dob_day == $d) {echo "selected";}?> ><?=$d?></option>
                                        <?php } ?>
                    </select>
                    </div>
                    <div class="col-sm-2">
                                      <?php if (!isset($dob_year) || trim($dob_year) == "") {$dob_year = '';}?>
                                      <select name="dob_year" id="dob_year" tabindex="8" class="form-control">
                                        <option value="">--Year--</option>

                                      	<?php for ($y=1950; $y<=date('Y'); $y++) { ?>
                                      	<option value="<?=$y?>" <?php if ($dob_year == $y) {echo "selected";}?> ><?=$y?></option>
                                        <?php } ?>
                                      </select>              
                    </div>  
                    </div>
                    
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="gender">Gender<span style="color:#F30; padding-left:2px;">*</span></label>
                       <div class="col-sm-10">
                        <label>
                          <input type="radio" id="gender" name="gender" value="male" class="minimal"  checked="checked"  >
                          Male
                        </label>
                        &nbsp;
                        <label>
                          <input type="radio" id="gender" name="gender" value="female" class="minimal"  >
                          Female
                        </label>
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="mobile">Parent's Mobile</label>
    
                      <div class="col-sm-10">
                       <span style="position:absolute; padding-left:20px; padding-top:5px;"> +971</span><input type="text" placeholder="" id="mobile" name="mobile" class="form-control" style="padding-left:60px;">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="email">Email</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Email" id="email" name="email" class="form-control">
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="country">Nationality</label>
    
                      <div class="col-sm-10">
                      
                                      <select name="country" id="country" tabindex="8" class="form-control">
                                      	<?php for ($c=0; $c<count($countries_list); $c++) { ?>
                                      	<option value="<?=$countries_list[$c]['country_id']?>"><?=$countries_list[$c]['country_name']?></option>
                                        <?php } ?>
                                      </select>         
                      </div>
                    </div>
                    
                    
                      <div class="form-group">
                      <label class="col-sm-2 control-label" for="emirates_id_father">Emirates Id [Father]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Emirates Id [Father]" id="emirates_id_father" name="emirates_id_father" class="form-control">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="emirates_id_mother">Emirates Id [Mother]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Emirates Id [Mother]" id="emirates_id_mother" name="emirates_id_mother" class="form-control" >
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="tbl_academic_year_id">Academic Year<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                       <select name="tbl_academic_year_id" id="tbl_academic_year_id" class="form-control" >
                              <?php
                                    for ($a=0; $a<count($academic_list); $a++) { 
                                        $tbl_academic_year_id    = $academic_list[$a]['tbl_academic_year_id'];
                                        $academic_start          = $academic_list[$a]['academic_start'];
                                        $academic_end            = $academic_list[$a]['academic_end'];
									
                                  ?>
                                      <option value="<?=$tbl_academic_year_id?>">
                                      <?=$academic_start?>&nbsp;-&nbsp;<?=$academic_end?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>
                             <a href="" style="font-size:9px;">Add New Academic Year</a>
                      </div>
                    </div>
                    
                 
                    
                    <div class="form-group">
                     <label class="col-sm-2 control-label" for="tbl_class_id">Class<span style="color:#F30; padding-left:2px;">*</span></label>
                     <div class="col-sm-10">
                                 <select name="tbl_class_id" id="tbl_class_id" class="form-control">
                              <option value="">--Select Class --</option>
							  
							  <?php
                                    for ($u=0; $u<count($classes_list); $u++) { 
                                        $tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
                                        $class_name             = $classes_list[$u]['class_name'];
                                        $class_name_ar          = $classes_list[$u]['class_name_ar'];
										$section_name           = $classes_list[$u]['section_name'];
                                        $section_name_ar        = $classes_list[$u]['section_name_ar'];
                                        if($tbl_sel_class_id == $tbl_class_id_u)
                                           $selClass = "selected";
                                         else
                                           $selClass = "";
                                  ?>
                                      <option value="<?=$tbl_class_id_u?>"  <?=$selClass?> >
                                      <?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;
                                    <?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                                      </option>
                                      <?php
                                    }
                                ?>
                             </select>
                   </div>
                   </div>
                    
                  
                       <div class="form-group">
                      <label class="col-sm-2 control-label" for="picture">Picture</label>
    
                      <div class="col-sm-10">
                        <!--File Upload START-->
                            <style>
                            #advancedUpload {
                                padding-bottom:0px;
                            }
                            </style>
                                 
                            <div id="advancedUpload">Upload File</div>
                            
                            <div id="uploaded_items" >
                                <div id="div_listing_container" class="listing_container" style="display:block">	            
										<?php
                                            if (trim($img_url) != "") {
                                        ?>
                                                    <div id='<?=$tbl_uploads_id?>' class='box-header with-border'>
                                                      <div class='box-title'><img src='<?=$img_url?>' /></div>
                                                      <div class='box-tools'> <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick="confirm_delete_img_popup('<?=$tbl_uploads_id?>')">
                                                        </button>
                                                      </div>
                                                    </div>
											<style>
												.ajax-upload-dragdrop {
													display:none;	
												}
                                            </style>        
                                        <?php		
                                            }
                                        ?>
                                </div>        
                            </div>
                        <!--File Upload END-->
                      </div>
                   
                    
                    </div>
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate()">Continue</button>
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
               
                </div>
      
      
               <div id="tabs-2">
                <!-- /.box-header -->
                <!-- form start -->
                  <div class="box-body">
                  
                    <p  style="padding-left:10px;"><strong>Parent Details</strong></p>
                    <br />
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="first_name_parent">Parent First Name [En]<span style="color:#F30; padding-left:2px;">*</span></label>
                      <div class="col-sm-10">
                        <input type="text" placeholder="Parent First Name[En]" id="first_name_parent" name="first_name_parent" class="form-control">
                      </div>
                      
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="first_name_parent_ar">Parent First Name [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Parent First Name[Ar]" id="first_name_parent_ar" name="first_name_parent_ar" class="form-control" dir="rtl">
                      </div>
                    </div>
                    
                      <div class="form-group">
                      <label class="col-sm-2 control-label" for="last_name_parent">Parent Last Name [En]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Parent Last Name[En]" id="last_name_parent" name="last_name_parent" class="form-control">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="last_name_parent_ar">Parent Last Name [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Parent Last Name[Ar]" id="last_name_parent_ar" name="last_name_parent_ar" class="form-control" dir="rtl">
                      </div>
                    </div>
                    
                 
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="dob_month_parent"> DOB</label>
    
                      <div class="col-sm-2">
                        <select name="dob_month_parent" id="dob_month_parent"  class="form-control" tabindex="6">
                                      	<option value="">--Month--</option>
                                      	<?php for ($m=1; $m<=12; $m++) { ?>
                                      	<option value="<?=$m?>" <?php if ($dob_month_parent == $m) {echo "selected";}?> ><?=$m?></option>
                                        <?php } ?>
                        </select>
                     </div>
                     <div class="col-sm-2">
                                      <?php if (!isset($dob_day_parent) || trim($dob_day_parent) == "") {$dob_day = '';}?>
                                      <select name="dob_day_parent" id="dob_day_parent" tabindex="7" class="form-control">
                                        <option value="">--Day--</option>
                                      	<?php for ($d=1; $d<=31; $d++) { ?>
                                      	<option value="<?=$d?>" <?php if ($dob_day_parent == $d) {echo "selected";}?> ><?=$d?></option>
                                        <?php } ?>
                    </select>
                    </div>
                    <div class="col-sm-2">
                                      <?php if (!isset($dob_year_parent) || trim($dob_year_parent) == "") {$dob_year = '';}?>
                                      <select name="dob_year_parent" id="dob_year_parent" tabindex="8" class="form-control">
                                        <option value="">--Year--</option>

                                      	<?php for ($y=1950; $y<=date('Y'); $y++) { ?>
                                      	<option value="<?=$y?>" <?php if ($dob_year_parent == $y) {echo "selected";}?> ><?=$y?></option>
                                        <?php } ?>
                                      </select>              
                    </div>  
                    </div>
                    
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="gender_parent">Relationship<span style="color:#F30; padding-left:2px;">*</span></label>
                       <div class="col-sm-10">
                        <label>
                          <input type="radio" id="gender_parent" name="gender_parent" value="male" class="minimal" checked="checked"  >
                          Father
                        </label>
                        &nbsp;
                        <label>
                          <input type="radio" id="gender_parent" name="gender_parent" value="female" class="minimal"  >
                          Mother
                        </label>
                        &nbsp;
                        <label>
                          <input type="radio" id="gender_parent" name="gender_parent" value="other" class="minimal"  >
                          Other
                        </label>
                      </div>
                    </div>
                    
                     <div class="form-group">
                      <label class="col-sm-2 control-label" for="mobile_parent">Parent's Mobile</label>
    
                      <div class="col-sm-10">
                       <span style="position:absolute; padding-left:20px; padding-top:5px;"> +971</span> <input type="text" placeholder="Parent's Mobile" id="mobile_parent" name="mobile_parent" class="form-control" style="padding-left:60px;">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="email_parent">Paren'st Email</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Parent's Email" id="email_parent" name="email_parent" class="form-control">
                      </div>
                    </div>
                    
                  
                    
                    
                      <div class="form-group">
                      <label class="col-sm-2 control-label" for="emirates_id_parent">Parent Emirates Id<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Parent Emirates Id" id="emirates_id_parent" name="emirates_id_parent" class="form-control">
                      </div>
                    </div>
                    
                   
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="parent_user_id">User Id<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="User Id " id="parent_user_id" name="parent_user_id" class="form-control">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="password">Password<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="password" placeholder="Password" id="password" name="password" class="form-control">
                      </div>
                    </div>
                    
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="confirm_password">Confirm Password<span style="color:#F30; padding-left:2px;">*</span></label>
    
                      <div class="col-sm-10">
                        <input type="password" placeholder="Confirm Password" id="confirm_password" name="confirm_password" class="form-control">
                      </div>
                    </div>
                    
                    </div>
                    
                    <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate_parent()">Submit</button>
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->  
                  
               </div>
                
               
            </div>
       </form>
      </div>
                
  
            <!--/Add or Create-->
                
        <!--/Admin Category Management-->

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>


<script src="<?=HOST_URL?>/assets/admin/dist/js/jquery.uploadfile.min.js"></script>

<script language="javascript">

var item_id = "<?=$student_id_enc?>";//Primary Key for a Form. 
if(item_id=="")
  var item_id = $("#student_id_enc").val();

function set_item_id(obj) {
	item_id = obj.value;
	get_files();	
}

$(document).ready(function() {
	var uploadObj = $("#advancedUpload").uploadFile({
		url:"<?=HOST_URL?>/file_mgmt/upload_the_file",
		multiple:true,
		autoSubmit:true,
		maxFileSize:130000,
		fileName:"myfile",
		formData: {"module_name":"student"},
		dynamicFormData: function() {
			var data = { item_id:item_id}
			return data;
		},
		showStatusAfterSuccess:false,
		dragDropStr: "<span class='d_d_text'>Optionally Drag and Drop the File to Upload.</span>",
		abortStr:"Abourt",
		cancelStr:"Cancel",
		doneStr:"Done",
		multiDragErrorStr: "Multi Drag Error.",
		extErrorStr:"Extention Error:",
		sizeErrorStr:"Max Size Error:",
		uploadErrorStr:"Upload Error",
		onSelect:function(files) {
 		},
		onSubmit:function(files) {
 		},
		onSuccess:function(files, data, xhr) {
			if (data == "error") {
				alert("Error uploading file. Please try again.");
				return;
			}
			var obj = JSON.parse(data);
			var tbl_uploads_id = obj.tbl_uploads_id;
			var file_name_updated = obj.file_name_updated;
			
			//alert("tbl_uploads_id: "+tbl_uploads_id)
			//alert("file_name_updated: "+file_name_updated)
			add_uploaded_item(tbl_uploads_id, file_name_updated);
		},
		afterUploadAll:function() {
 		},
		onError: function(files, status, errMsg) {
 		}
	});

	$("#startUpload").click(function() {
		uploadObj.startUpload();
	});
	
	try { 
		$('input[type=file]').click();
	} catch(e) {
		alert(e)
	}
});

//Function called when file is uploaded
function add_uploaded_item(tbl_uploads_id, file_name_updated) {
	var str = "<div id='"+tbl_uploads_id+"' class='box-header with-border'> <div class='box-title'><img src='<?=IMG_PATH_STUDENT?>/"+file_name_updated+"' /></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_img_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
		
	$("#div_listing_container").show();
	$("#div_listing_container").append(str);
	$(".ajax-upload-dragdrop").hide();//Hide the upload button
return;
}

function confirm_delete_img_popup(tbl_uploads_id) {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		$('#'+tbl_uploads_id).hide();	
		$(".ajax-upload-dragdrop").show();
		
		var url_str = "<?=HOST_URL?>/file_mgmt/delete_file";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					tbl_uploads_id: tbl_uploads_id
				},
			success: function(data) {
				$("#pre-loader").hide();
			}
		});	
	} else {
		$("#pre-loader").hide();		
	}
}

function get_files() {
	var url_str = "<?=HOST_URL?>/misc/get_files.php";
	
	$.ajax({
		type: "POST",
		url: url_str,
		data: {
				module_name: "student",
				show_del: "Y",
				item_id: item_id//global variable
			},
		success: function(data) {
			$('#div_listing_container').show();
			$('#div_listing_container').html(data)
			
		}
	});	
}
</script>
<!--File Upload END-->
        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >
function search_data() {
		var tbl_class_search_id = $("#tbl_class_search_id").val();
		var q = $.trim($("#q").val());
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/all_students/";
		
		if(tbl_class_search_id !='')
			url += "tbl_class_search_id/"+tbl_class_search_id+"/";
		
		if(q !='')
			url += "q/"+q+"/";
		
			url += "offset/0/";
		window.location.href = url;
		<?php /*?>window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/enquiry/all_enquiries/is_not_replied/"+is_not_replied+"/tbl_court_id/"+tbl_court_id+"/tbl_category_id/"+tbl_category_id;<?php */?>
	}
function generate_student_id(offset) {
		var tbl_class_id = $("#tbl_class_search_id").val();
		var q = $("#q").val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/generate_student_id/";
		if(tbl_class_id !='')
			url += "tbl_class_id/"+tbl_class_id+"/";
		if(q !='')
			url += "q/"+q+"/";
		
			url += "offset/"+offset+"/";
		window.open(url);
	}
	
	
function generate_student_id_card(offset) {
		var tbl_class_id = $("#tbl_class_search_id").val();
		var q = $("#q").val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/generate_student_id_card/";
		if(tbl_class_id !='')
			url += "tbl_class_id/"+tbl_class_id+"/";
		if(q !='')
			url += "q/"+q+"/";
		
			url += "offset/"+offset+"/";
		window.open(url);
	}
	


function reset_data() {
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/all_students/";
		url += "offset/0/";
		window.location.href = url;
	}
	
	
	
function valueCheckedForPagesManagement(){
	    var len = $("input[id='student_id_enc']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more student(s)", 'green');
		return;	
		}
		
		var tbl_class_id = $("#tbl_class_id_promotion").val();
		if(tbl_class_id=="")
		{
			refresh_page = "N";
			my_alert("Please select class to promote", 'green');
		return;	
		}
		
		var tbl_academic_year_promotion = $("#tbl_academic_year_promotion").val();
		if(tbl_academic_year_promotion=="")
		{
			refresh_page = "N";
			my_alert("Please select academic year to promote", 'green');
		return;	
		}
		
			
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/student/updatePromotion",
			data: {
				student_id_enc: $("input[id='student_id_enc']:checked").serialize(),
				tbl_class_id_promotion: $("#tbl_class_id_promotion").val(),
				tbl_academic_year_promotion: $("#tbl_academic_year_promotion").val(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Student(s) promoted to selected class successfully.", 'green')
				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
		
	
}
//end

//start
function update_student_promotion(student_array,tbl_class_id,tbl_academic_year) {
	
		var a = confirm("Are you sure you want to promote the selected students to selected class in the current academic year");
		if (a){
						try { 
							$.ajax({
							type: "POST",
							url: "<?=HOST_URL_ADMIN?>/student_management/update_promotion.php",
							data: {
								student_array: student_array,
								tbl_class_id:tbl_class_id,
								tbl_academic_year:tbl_academic_year,
								is_ajax: true
							},
							success: function(data) {
							    alert("Students has been promoted successfully");
								//window.location.reload();
								window.location.href='<?=HOST_URL_ADMIN?>/student_management/student_promotion_management.php?mid=1&sid=<?=$sid?>&tab=members&subtab=students_promotion';
							},
							error: function() {
								$('#pre_loader').css('display','none');	
							}, 
							complete: function() {
								$('#pre_loader').css('display','none');	
								window.location.reload();
							}
				
							});
						} catch(e) {
							//alert(e)	
						}	
					
      }else{
			return false;
		}	
		
}

//end	
	


</script>