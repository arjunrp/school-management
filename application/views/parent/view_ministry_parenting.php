<?php
//Init Parameters
$parenting_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}
?>

<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/ministry_parenting/all_ministry_parentings";
			}
		})
	});
	
	function confirm_delete_popup() {
		var len = $("input[id='parenting_id_checkbox_enc']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more record(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/ministry_parenting/delete",
			data: {
				parenting_id_enc: $("input[id='parenting_id_checkbox_enc']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Information deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_activate(parenting_id_checkbox_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/ministry_parenting/activate",
			data: {
				parenting_id_checkbox_enc: parenting_id_checkbox_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Selected records activated successfully.", 'green')

				$('#act_deact_'+parenting_id_checkbox_enc).html('<span style="cursor:pointer" onClick="ajax_deactivate(\''+parenting_id_checkbox_enc+'\')" onMouseOver="deactivate_me(this)" onMouseOut="reset_activate(this)" class="label label-success">Active</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_deactivate(parenting_id_checkbox_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/ministry_parenting/deactivate",
			data: {
				parenting_id_checkbox_enc: parenting_id_checkbox_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Selected records de-activated successfully.", 'green')
				
				$('#act_deact_'+parenting_id_checkbox_enc).html('<span style="cursor:pointer" onClick="ajax_activate(\''+parenting_id_checkbox_enc+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist() {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/ministry_parenting/is_exist",
			data: {
				parenting_id_enc: "",
				parenting_title_en: $('#parenting_title_en').val(),
				parenting_title_ar: "",
				is_ajax: true
			},
			success: function(data) {
				var str = new String(data);
				if (str.indexOf("*N*")==0) {
					$('#parenting_text_en_hidden').val($('#parenting_text_en').val());
					$('#parenting_text_ar_hidden').val($('#parenting_text_ar').val());

					$('#frm_listing').submit();
				} else if (str.indexOf("*Y*")==0) {
					refresh_page = "N";
					my_alert("Title already exists.", 'red');
					$("#pre-loader").hide();
				}
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist_edit() {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/ministry_parenting/is_exist",
			data: {
				parenting_id_enc:$('#parenting_id_enc').val(),
				parenting_title_en: $('#parenting_title_en').val(),
				parenting_title_ar: "",
				is_ajax: true
			},
			success: function(data) {
				var str = new String(data);
				if (str.indexOf("*N*")==0) {

					$('#parenting_text_en_hidden').val($('#parenting_text_en').val());
					$('#parenting_text_ar_hidden').val($('#parenting_text_ar').val());

					$('#frm_edit').submit();
				} else if (str.indexOf("*Y*")==0) {
					refresh_page = "N";
					my_alert("Parenting  already exists.", 'red');
					$("#pre-loader").hide();
				}
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_create() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/ministry_parenting/create_ministry_parenting",
			data: {
				parenting_id_enc: "<?=$parenting_id_enc?>",
				parenting_cat_id_enc: $('#parenting_cat_id_enc').val(),
				parenting_title_en: $('#parenting_title_en').val(),
				parenting_title_ar: $('#parenting_title_ar').val(),
				parenting_type: $("input[name='parenting_type']:checked").val(),
				parenting_text_en: $('#parenting_text_en').val(),
				parenting_text_ar: $('#parenting_text_ar').val(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Parenting  created successfully.", 'green');
				
				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_save_changes() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/ministry_parenting/save_changes",
			data: {
				parenting_id_enc:$('#parenting_id_enc').val(),
				parenting_title_en: $('#parenting_title_en').val(),
				parenting_title_ar: $('#parenting_title_ar').val(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Changes saved successfully.", 'green');
				
				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
</script>
<script language="javascript">
	function ajax_validate() {
		if ( validate_parenting_category() == false || validate_parenting_title_en() == false || validate_parenting_title_ar() == false ) {
			return false;
		} else {
			is_exist();
		}
	} 

	function ajax_validate_edit() {
		if ( validate_parenting_category() == false || validate_parenting_title_en() == false || validate_parenting_title_ar() == false ) {
			return false;
		} else {
			is_exist_edit();
		}
	} 

	function validate_parenting_category() {
		var regExp = / /g;
		var str = $('#parenting_cat_id_enc').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Category is blank. Please select Category.");
		return false;
		}
	}

	function validate_parenting_title_en() {
		var regExp = / /g;
		var str = $('#parenting_title_en').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Title[En] is blank. Please enter Title[En].");
		return false;
		}
	}

	function validate_parenting_title_ar() {
		var regExp = / /g;
		var str = $('#parenting_title_ar').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Title[Ar] is blank. Please enter Title[Ar].");
		return false;
		}
	}

</script>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1>Parenting (Videos/Text)</h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style="float:left; position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/home"><i class="fa fa-home"></i>Home</a></li>
      <li> Parenting (Videos/Text) </li>
    </ol>
    <!--/BREADCRUMB--> 
    <div class="box-tools" style="float:right;">
        <a href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/parenting_category"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
    </div> 
    <div style="clear:both"></div>
  </section>
  
  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {

			$tbl_parenting_id = $ministry_parenting_obj['tbl_parenting_id'];
			$parenting_title_en = $ministry_parenting_obj['parenting_title_en'];
			$parenting_title_ar = $ministry_parenting_obj['parenting_title_ar'];
			$added_date = $ministry_parenting_obj['added_date'];
			$is_active = $ministry_parenting_obj['is_active'];
	?>
    
<!--File Upload START-->
<link href="http://hayageek.github.io/jQuery-Upload-File/uploadfile.min.css" rel="stylesheet">

<style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url('<?=IMG_PATH?>/delete.jpg');
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
		clear:both;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
	#overlay_container {
		position:relative;
	}
	#overloading {
		background-image:url('<?=IMG_PATH?>/preloader/preloader_2.gif');
		background-repeat:no-repeat;
		background-position:center;
		background-color:#CCC;
		position:absolute;
		left:0px;
		top:0px;
		opacity: 0.3;
		z-index: 10000;
	}
	#div_listing_container {
		display:none;	
	}
	.d_d_text {
		color:#745156;
		font-size:20px;
			
	}
	.ajax-upload-dragdrop {
		margin:auto;
		margin-bottom:10px;
		width:700px !important;
	}
	.ajax-file-upload-statusbar {
		margin:auto;
		margin-top:10px;
	}
	.ajax-file-upload {
		height:31px;
	}
</style>
 
<script src="http://hayageek.github.io/jQuery-Upload-File/jquery.uploadfile.min.js"></script>

<script language="javascript">

var item_id = "<?=$tbl_parenting_id?>";//Primary Key for a Form. 

function set_item_id(obj) {
	item_id = obj.value;
	get_files();	
}

$(document).ready(function() {
	var uploadObj = $("#advancedUpload").uploadFile({
		url:"<?=HOST_URL?>/file_mgmt/upload_the_file",
		multiple:true,
		autoSubmit:true,
		maxFileSize:13000000,
		fileName:"myfile",
		allowedTypes:"mp4",
		formData: {"module_name":"parenting"},
		dynamicFormData: function() {
			var data = { item_id:item_id}
			return data;
		},
		showStatusAfterSuccess:false,
		dragDropStr: "<span class='d_d_text'>Optionally Drag and Drop the File to Upload.</span>",
		abortStr:"Abourt",
		cancelStr:"Cancel",
		doneStr:"Done",
		multiDragErrorStr: "Multi Drag Error.",
		extErrorStr:"Extention Error:",
		sizeErrorStr:"Max Size Error:",
		uploadErrorStr:"Upload Error",
		onSelect:function(files) {
 		},
		onSubmit:function(files) {
 		},
		onSuccess:function(files, data, xhr) {
			if (data == "error") {
				alert("Error uploading file. Please try again.");
				return;
			}
 			
			var obj = JSON.parse(data);
			var tbl_uploads_id = obj.tbl_uploads_id;
			var file_name_updated = obj.file_name_updated;
			
			//alert("tbl_uploads_id: "+tbl_uploads_id)
			//alert("file_name_updated: "+file_name_updated)
			add_uploaded_item(tbl_uploads_id, file_name_updated);
		},
		afterUploadAll:function() {
 		},
		onError: function(files, status, errMsg) {
 		}
	});

	$("#startUpload").click(function() {
		uploadObj.startUpload();
	});
	
	try { 
		$('input[type=file]').click();
	} catch(e) {
		alert(e)
	}
});

//Function called when file is uploaded
function add_uploaded_item(tbl_uploads_id, file_name_updated) {
	var str = "<div id='"+tbl_uploads_id+"' class='box-header with-border'> <div class='box-title'><video width='320' height='240' controls><source src='<?=HOST_URL?>/admin/uploads/"+file_name_updated+"' type='video/mp4'></video></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_video_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
		
	$("#div_listing_container").show();
	$("#div_listing_container").append(str);
	$(".ajax-upload-dragdrop").hide();//Hide the upload button
return;
}

function confirm_delete_video_popup(tbl_uploads_id) {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		$('#'+tbl_uploads_id).hide();	
		$(".ajax-upload-dragdrop").show();
		
		var url_str = "<?=HOST_URL?>/file_mgmt/delete_file";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					tbl_uploads_id: tbl_uploads_id
				},
			success: function(data) {
				$("#pre-loader").hide();
			}
		});	
	} else {
		$("#pre-loader").hide();		

	}
}

function get_files() {
	var url_str = "<?=HOST_URL?>/misc/get_files.php";
	
	$.ajax({
		type: "POST",
		url: url_str,
		data: {
				module_name: "admin_user",
				show_del: "Y",
				item_id: item_id//global variable
			},
		success: function(data) {
			$('#div_listing_container').show();
			$('#div_listing_container').html(data)
			
		}
	});	
}

function confirm_delete_image_popup(parenting_id_enc) {
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		var url_str = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/ministry_parenting/ajax_delete_image";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					parenting_id_enc: parenting_id_enc
				},
			success: function(data) {
				$('#div_img').hide();
			}
		});	
	} 
}

function show_text_edit() {
	$('#div_text_edit').show('slow');
	$('#div_video_edit').hide();
}

function show_video_edit() {
	$('#div_video_edit').show('slow');
	$('#div_text_edit').hide();
}

</script>
<!--File Upload END-->
    
        <!--Edit-->
        	<?php
				$tbl_parenting_id = $ministry_parenting_obj['tbl_parenting_id'];
				$tbl_parenting_cat_id = $ministry_parenting_obj['tbl_parenting_cat_id'];
				$parenting_title_en = $ministry_parenting_obj['parenting_title_en'];
				$parenting_title_ar = $ministry_parenting_obj['parenting_title_ar'];
				$parenting_type = $ministry_parenting_obj['parenting_type'];//D,V
				$parenting_logo = $ministry_parenting_obj['parenting_logo'];
				$parenting_text_en = $ministry_parenting_obj['parenting_text_en'];
				$parenting_text_ar = $ministry_parenting_obj['parenting_text_ar'];
				
				$img_path = "";
				if (trim($parenting_logo) != "") {
					$img_path = HOST_URL."/admin/uploads/".$parenting_logo;	
				}
			?>
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Videos/Text </h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/ministry_parenting/parenting_cat_id_enc/<?=$tbl_parenting_cat_id?>/"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                
               
                  <div class="box-body">
                  
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="parenting_title_en">Parenting Category</label>
    
                      <div class="col-sm-10">
                        <select name="parenting_cat_id_enc" id="parenting_cat_id_enc" class="form-control">
                        	<option value="">SELECT</option>
                        	<?php
                            	for ($p=0; $p<count($rs_parenting_categorys); $p++) { 
									$id_p = $rs_parenting_categorys[$p]['id'];
									$tbl_parenting_cat_id_p = $rs_parenting_categorys[$p]['tbl_parenting_cat_id'];
									$title_en_p = $rs_parenting_categorys[$p]['title_en'];
									$title_ar_p = $rs_parenting_categorys[$p]['title_ar'];
							?>
                        	<option value="<?=$tbl_parenting_cat_id_p?>" <?php if (trim($tbl_parenting_cat_id) == trim($tbl_parenting_cat_id_p)) {echo "selected";}?> ><?=$title_en_p?> - <?=$title_ar_p?></option>
                            <?php
								}
							?>
                        </select>
                      </div>
                    </div>
                    
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="parenting_title_en">Title[En]</label><p>&nbsp;&nbsp;<?=$parenting_title_en?></p>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="parenting_title_ar">Title[Ar]</label><p>&nbsp;&nbsp;<?=$parenting_title_ar?></p>
                      
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="parenting_title_ar">Thumb Image</label>
    				  <?php
                      	if (trim($img_path) != "") { 
					  ?>
                        <div class="col-sm-10">
                      		<div id="div_img">	
	                            <br />
                                <img src="<?=$img_path?>" style="max-width:400px" /> 
                                <br />
                            </div>
                            
                        </div>
	                        
                      <?php
						} else {
					  ?>
                            
					 <?php		
						}
					  ?>
                     
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="dob">Type</label>
    
                      <div class="col-sm-10">
                         <label><input name="parenting_type" id="parenting_type" type="radio" value="D" onclick="show_text_edit()" <?php if (strtolower(trim($parenting_type)) == "d") {echo "checked";}?> /> Text</label>
                         <label><input name="parenting_type" id="parenting_type" type="radio" value="V" onclick="show_video_edit()" <?php if (strtolower(trim($parenting_type)) == "v") {echo "checked";}?> /> Video</label>
                      </div>
                    </div>

					
                    
                    <div id="div_text_edit" <?php if (strtolower(trim($parenting_type)) == "v") {echo "style=display:none";}?>>
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="dob">Description[En]</label>
        
                          <div class="col-sm-10">
                             <textarea class="textarea" placeholder="Description[En]" id="parenting_text_en" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?=$parenting_text_en?></textarea>
                          	 <input type="hidden" id="parenting_text_en_hidden" name="parenting_text_en_hidden" value="" />
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="parenting_text_ar">Description[Ar]</label>
        
                          <div class="col-sm-10">
	                         <textarea class="textarea" placeholder="Description[Ar]" id="parenting_text_ar" dir="rtl" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?=$parenting_text_ar?></textarea>
                         	 <input type="hidden" id="parenting_text_ar_hidden" name="parenting_text_ar_hidden" value="" />
                          </div>
                        </div>
                    </div>
                    
                    
                    <div id="div_video_edit" <?php if (strtolower(trim($parenting_type)) == "d") {echo "style=display:none";}?>>
                            <div class="form-group">
                              <label class="col-sm-2 control-label" for="picture">Video</label>
            
                              <div class="col-sm-10">

									<?php
									if (trim($video_url) != "") { 
									?>
                                    <div id="<?=$tbl_uploads_id?>" class="box-header with-border">
                                      <div class="box-title">
                                        <video width="320" height="240" controls>
                                          <source src="<?=$video_url?>" type="video/mp4">
                                        </video>
                                      </div>
                                    </div>         
									<?php
									}
									?>
                                    
                                    <div id="uploaded_items" >
                                        <div id="div_listing_container" class="listing_container" style="display:block">	            
                                                <?php
                                                    if (trim($img_url) != "") {
                                                ?>
                                                            <div id='<?=$tbl_uploads_id?>' class='box-header with-border'>
                                                              <div class='box-title'><img src='<?=$img_url?>' /></div>
                                                                </button>
                                                              </div>
                                                            </div>
                                                 
                                                <?php		
                                                    }
                                                ?>
                                        </div>        
                                    </div>
                                <!--File Upload END-->
                              </div>
                            </div>
				  </div>
                  	
                    
                  </div>
             
		        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/parent/parent_user/ministry_parenting";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
		if (trim($parenting_cat_id_enc) != "") {
			$sort_url .= "/parenting_cat_id_enc/".rawurlencode($parenting_cat_id_enc);
		}
	?>   
<!--File Upload START-->
<link href="http://hayageek.github.io/jQuery-Upload-File/uploadfile.min.css" rel="stylesheet">

<style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url('<?=IMG_PATH?>/delete.jpg');
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
		clear:both;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
	#overlay_container {
		position:relative;
	}
	#overloading {
		background-image:url('<?=IMG_PATH?>/preloader/preloader_2.gif');
		background-repeat:no-repeat;
		background-position:center;
		background-color:#CCC;
		position:absolute;
		left:0px;
		top:0px;
		opacity: 0.3;
		z-index: 10000;
	}
	#div_listing_container {
		display:none;	
	}
	.d_d_text {
		color:#745156;
		font-size:20px;
			
	}
	.ajax-upload-dragdrop {
		margin:auto;
		margin-bottom:10px;
		width:700px !important;
	}
	.ajax-file-upload-statusbar {
		margin:auto;
		margin-top:10px;
	}
	.ajax-file-upload {
		height:31px;
	}
</style>
 
<script src="http://hayageek.github.io/jQuery-Upload-File/jquery.uploadfile.min.js"></script>

<script language="javascript">

var item_id = "<?=$parenting_id_enc?>";//Primary Key for a Form. 

function set_item_id(obj) {
	item_id = obj.value;
	get_files();	
}

$(document).ready(function() {
	var uploadObj = $("#advancedUpload").uploadFile({
		url:"<?=HOST_URL?>/file_mgmt/upload_the_file",
		multiple:true,
		autoSubmit:true,
		maxFileSize:13000000,
		fileName:"myfile",
		allowedTypes:"mp4",
		formData: {"module_name":"parenting"},
		dynamicFormData: function() {
			var data = { item_id:item_id}
			return data;
		},
		showStatusAfterSuccess:false,
		dragDropStr: "<span class='d_d_text'>Optionally Drag and Drop the File to Upload.</span>",
		abortStr:"Abourt",
		cancelStr:"Cancel",
		doneStr:"Done",
		multiDragErrorStr: "Multi Drag Error.",
		extErrorStr:"Extention Error:",
		sizeErrorStr:"Max Size Error:",
		uploadErrorStr:"Upload Error",
		onSelect:function(files) {
 		},
		onSubmit:function(files) {
 		},
		onSuccess:function(files, data, xhr) {
			if (data == "error") {
				alert("Error uploading file. Please try again.");
				return;
			}
 			
			var obj = JSON.parse(data);
			var tbl_uploads_id = obj.tbl_uploads_id;
			var file_name_updated = obj.file_name_updated;
			
			//alert("tbl_uploads_id: "+tbl_uploads_id)
			//alert("file_name_updated: "+file_name_updated)
			add_uploaded_item(tbl_uploads_id, file_name_updated);
		},
		afterUploadAll:function() {
 		},
		onError: function(files, status, errMsg) {
 		}
	});

	$("#startUpload").click(function() {
		uploadObj.startUpload();
	});
	
	try { 
		$('input[type=file]').click();
	} catch(e) {
		alert(e)
	}
});

//Function called when file is uploaded
function add_uploaded_item(tbl_uploads_id, file_name_updated) {
	var str = "<div id='"+tbl_uploads_id+"' class='box-header with-border'> <div class='box-title'><video width='320' height='240' controls><source src='<?=HOST_URL?>/admin/uploads/"+file_name_updated+"' type='video/mp4'></video></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_video_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
		
	$("#div_listing_container").show();
	$("#div_listing_container").append(str);
	$(".ajax-upload-dragdrop").hide();//Hide the upload button
return;
}

function confirm_delete_video_popup(tbl_uploads_id) {
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		
		$('#'+tbl_uploads_id).hide();	
		$(".ajax-upload-dragdrop").show();
		
		var url_str = "<?=HOST_URL?>/file_mgmt/delete_file";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					tbl_uploads_id: tbl_uploads_id
				},
			success: function(data) {
				$("#pre-loader").hide();
			}
		});	
	} 
}

function get_files() {
	var url_str = "<?=HOST_URL?>/misc/get_files.php";
	
	$.ajax({
		type: "POST",
		url: url_str,
		data: {
				module_name: "admin_user",
				show_del: "Y",
				item_id: item_id//global variable
			},
		success: function(data) {
			$('#div_listing_container').show();
			$('#div_listing_container').html(data)
			
		}
	});	
}

function show_text() {
	$('#div_text').show('slow');
	$('#div_video').hide();
}

function show_video() {
	$('#div_video').show('slow');
	$('#div_text').hide();
}


function send_message(tbl_parenting_id) {
	$('#myModal').modal('show');
	$.ajax({
		type: "POST",
		url: "<?=HOST_URL?>/en/parent/parent_user/view_ministry_parenting",
		data: {
			parenting_id_enc: tbl_parenting_id,
			is_ajax: true
		},
		success: function(data) {
			$("#modal-body").html(data);
 			//$('#txt_message').html(data);
			//$( "#alert_message_sent" ).dialog({ modal: true },{ resizable: false }, { draggable: false });
			//$( "#alert_message_sent" ).dialog({ title: "Message" },{ buttons: [ { text: "Ok", click: function() { window.location.reload();	} } ] });
		//	$('#myModal').modal('hide');
		//	my_alert("Message sent successfully.", "green");
		//	setTimeout(function(){window.location.reload();},1000);
		},
		error: function() {
			$('#pre_loader').css('display','none');	
		}, 
		complete: function() {
			$('#pre_loader').css('display','none');	
		}
	});	
	
	
	
}


</script>
<!--File Upload END-->
    
            <!--Listing-->
                    <div id="mid1" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Videos/Text</h3>
                          <div class="box-tools">
                            <?php if (count($rs_all_ministry_parentings)>0) { echo $paging_string;}?>	
                         <!--   <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
                            <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>-->
                          </div>
                        </div>
                        
                        <div class="box-body">
                          <table width="100%" class="table table-bordered table-striped" id="example1">
                            <thead>
                            <tr>
                              <th width="25%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y">Title[En] <?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?>
	                              <div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>
                              <th width="25%" align="center" valign="middle">Title[Ar]</th>
                              <th width="15%" align="center" valign="middle">Logo</th>
                              <th width="15%" align="center" valign="middle">Type</th>
                              <th width="20%" align="center" valign="middle">Date</th>
                              <!--<th width="10%" align="center" valign="middle">Status</th>
                              <th width="10%" align="center" valign="middle">Action</th>-->
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                                for ($i=0; $i<count($rs_all_ministry_parentings); $i++) { 
                                    $id = $rs_all_ministry_parentings[$i]['id'];
                                    $tbl_parenting_id     = $rs_all_ministry_parentings[$i]['tbl_parenting_id'];
									$tbl_parenting_cat_id = $rs_all_ministry_parentings[$i]['tbl_parenting_cat_id'];
                                    $parenting_title_en = $rs_all_ministry_parentings[$i]['parenting_title_en'];
                                    $parenting_title_ar = $rs_all_ministry_parentings[$i]['parenting_title_ar'];
                                    $added_date = $rs_all_ministry_parentings[$i]['added_date'];
                                    $is_active = $rs_all_ministry_parentings[$i]['is_active'];
                                    $parenting_type = $rs_all_ministry_parentings[$i]['parenting_type'];
                                    $parenting_title_en = ucfirst($parenting_title_en);
                                    $added_date = date('m-d-Y',strtotime($added_date));
									$parenting_logo = $rs_all_ministry_parentings[$i]['parenting_logo'];
									$img_path = "";
									if (trim($parenting_logo) != "") {
										$img_path = HOST_URL."/admin/uploads/".$parenting_logo;	
									}else{
										
										$img_path = HOST_URL."/images/no_image150.jpg";
									}
                            ?>
                            <tr> <!--href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/view_ministry_parenting/parenting_id_enc/<?=$tbl_parenting_id?>/parenting_cat_id_enc/<?=$tbl_parenting_cat_id?>"-->
                             <td align="left" valign="middle"><a style="cursor:pointer;" onclick="send_message('<?=$tbl_parenting_id?>')" ><?=$parenting_title_en?></a></td>
                             <td align="left" valign="middle"><a style="cursor:pointer;" onclick="send_message('<?=$tbl_parenting_id?>')" ><?=$parenting_title_ar?></a></td>
                             <td align="left" valign="middle"><img onclick="send_message('<?=$tbl_parenting_id?>')"  src="<?=$img_path?>" width="80" height="80" /></td>
                             <td align="left" valign="middle" > <?php if (strtolower(trim($parenting_type)) == "d") { echo "Text"; } else { echo "Video"; } ?></td>
                             <td align="left" valign="middle"><?=$added_date?></td>
                             <?php /*?> <td align="left" valign="middle">
                                <div id="act_deact_<?=$tbl_parenting_id?>">
                                <?php if (trim($is_active) == "Y") { ?>
                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_parenting_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                <?php } else { ?>
                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_parenting_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                <?php } ?>
                                </div>
                              </td>
                              <td align="left" valign="middle">
                                <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/ministry_parenting/edit_ministry_parenting/parenting_id_enc/<?=$tbl_parenting_id?>"><button class="btn bg-purple fa fa-pencil" type="button" title="Edit"></button></a>
                              </td><?php */?>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="7" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_all_ministry_parentings)<=0) {
                            ?>
                            <tr>
                              <td colspan="7" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no Parenting (Videos/Text) available. </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
    
            <!--Add or Create-->
              <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Create Parenting (Videos/Text)</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post" action="<?=HOST_URL?>/<?=LAN_SEL?>/admin/ministry_parenting/create_ministry_parenting" enctype="multipart/form-data">
                  <div class="box-body">
                  
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="parenting_title_en">Parenting Category</label>
    
                      <div class="col-sm-10">
                        <select name="parenting_cat_id_enc" id="parenting_cat_id_enc" class="form-control">
                        	<option value="">SELECT</option>
                        	<?php
                            	for ($p=0; $p<count($rs_parenting_categorys); $p++) { 
									$id_p = $rs_parenting_categorys[$p]['id'];
									$tbl_parenting_cat_id_p = $rs_parenting_categorys[$p]['tbl_parenting_cat_id'];
									$title_en_p = $rs_parenting_categorys[$p]['title_en'];
									$title_ar_p = $rs_parenting_categorys[$p]['title_ar'];
							?>
                        	<option value="<?=$tbl_parenting_cat_id_p?>"><?=$title_en_p?> - <?=$title_ar_p?></option>
                            <?php
								}
							?>
                        </select>
                      </div>
                    </div>
                    
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="parenting_title_en">Title[En]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Title[En]" id="parenting_title_en" name="parenting_title_en" class="form-control">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="parenting_title_ar">Title[Ar]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Title[Ar]" id="parenting_title_ar" name="parenting_title_ar" class="form-control" dir="rtl">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="parenting_title_ar">Thumb Image</label>
    
                      <div class="col-sm-10">
                        <input name="parenting_logo" id="parenting_logo" type="file" accept="image/*" />
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="dob">Type</label>
    
                      <div class="col-sm-10">
                         <label><input name="parenting_type" id="parenting_type" type="radio" value="D" onclick="show_text()" checked="checked" /> Text</label>
                         <label><input name="parenting_type" id="parenting_type" type="radio" value="V" onclick="show_video()" /> Video</label>
                      </div>
                    </div>

					
                    
                    <div id="div_text">
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="dob">Description[En]</label>
        
                          <div class="col-sm-10">
                             <textarea class="textarea" placeholder="Description[En]" id="parenting_text_en" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?=$parenting_text_en?></textarea>
                          	 <input type="hidden" id="parenting_text_en_hidden" name="parenting_text_en_hidden" value="" />
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="parenting_text_ar">Description[Ar]</label>
        
                          <div class="col-sm-10">
	                         <textarea class="textarea" placeholder="Description[Ar]" id="parenting_text_ar" dir="rtl" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?=$parenting_text_ar?></textarea>
                         	 <input type="hidden" id="parenting_text_ar_hidden" name="parenting_text_ar_hidden" value="" />
                          </div>
                        </div>
                    </div>
                    
                    
                    <div id="div_video" style="display:none">
                            <div class="form-group">
                              <label class="col-sm-2 control-label" for="picture">Video</label>
            
                              <div class="col-sm-10">
                                <!--File Upload START-->
                                    <style>
                                    #advancedUpload {
                                        padding-bottom:0px;
                                    }
                                    </style>
                                         
                                    <div id="advancedUpload">Upload File</div>
                                    
                                    <div id="uploaded_items" >
                                        <div id="div_listing_container" class="listing_container" style="display:block">	            
                                                <?php
                                                    if (trim($img_url) != "") {
                                                ?>
                                                            <div id='<?=$tbl_uploads_id?>' class='box-header with-border'>
                                                              <div class='box-title'><img src='<?=$img_url?>' /></div>
                                                              <div class='box-tools'> <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick="confirm_delete_video_popup('<?=$tbl_uploads_id?>')">
                                                                </button>
                                                              </div>
                                                            </div>
                                                    <style>
                                                        .ajax-upload-dragdrop {
                                                            display:none;	
                                                        }
                                                    </style>        
                                                <?php		
                                                    }
                                                ?>
                                        </div>        
                                    </div>
                                <!--File Upload END-->
                              </div>
                            </div>
				  </div>
                  	
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate()">Create Parenting </button>
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                  
                  <input type="hidden" name="parenting_id_enc" id="parenting_id_enc" value="<?=$parenting_id_enc?>" />
                </form>
              </div>
            <!--/Add or Create-->
                
        <!--/Admin Parenting  Management-->

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>


        <!--Upload Files START-->
        <div id="myModal" class="modal fade" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document" style="width:80% !important; height:80%;">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Parenting (From Ministry) </h4>
              </div>
              <div class="modal-body" id="modal-body" >
                	<!--Upload Files START-->
                   <?php /*?> <div id="div_popup_message" style="display:block;">
                    <div style="text-align:center;">
	                    <textarea name="txt_message" id="txt_message" cols="53" rows="5"></textarea>
                    </div>
                    </div><?php */?>
                    <!--Upload Files END--> 
              </div>
              <div class="modal-footer" style="border:none !important;">
               <!-- <button type="button" class="btn btn-default" onclick="send_message_user()" >Send Message</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->         
        <!--Upload Files END-->    
    <!--/WORKING AREA--> 
  </section>
</div>


<script language="javascript">
	$(document).ready(function(e) {
		<?php
			if (trim($MSG) != "") { 
		?>
	    	my_alert("<?=$MSG?>", 'green');    
		<?php } ?>
    });
	
</script>
