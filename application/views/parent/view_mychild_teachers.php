<?php
//Init Parameters
$teacher_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}
?>
 
<style>
.txt_en {
	text-align:left;
	padding-left:2px;
}
.txt_ar {
	text-align:right;
	padding-right:2px;	
	direction:rtl;		
}
</style>
<script>
var item_id = "<?=$teacher_id_enc?>";
</script>

<script language="javascript">
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/all_teachers";
			}
		})
	});
	
function confirm_delete_popup() {
		var len = $("input[id='teacher_id_enc']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more teacher(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/deleteTeacher",
			data: {
				teacher_id_enc: $("input[id='teacher_id_enc']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Teacher(s) deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}	
	function ajax_activate(teacher_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/activateTeacher",
			data: {
				teacher_id_enc: teacher_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Teacher activated successfully.", 'green')

				$('#act_deact_'+teacher_id_enc).html('<span style="cursor:pointer" onClick="ajax_deactivate(\''+teacher_id_enc+'\')" onMouseOver="deactivate_me(this)" onMouseOut="reset_activate(this)" class="label label-success">Active</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_deactivate(teacher_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/deactivateTeacher",
			data: {
				teacher_id_enc: teacher_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Teacher de-activated successfully.", 'green')
				
				$('#act_deact_'+teacher_id_enc).html('<span style="cursor:pointer" onClick="ajax_activate(\''+teacher_id_enc+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	
	
	
	function ajax_create() {
		 var selectednumbers='';
        	$('#tbl_class_id :selected').each(function(i, selected) {
            	selectednumbers += $(selected).val()+"&";
        	});
		
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/add_teacher",
			data: {
				teacher_id_enc: "<?=$teacher_id_enc?>",
				first_name    : $('#first_name').val(),
				last_name     : $('#last_name').val(),
				first_name_ar : $('#first_name_ar').val(),
				last_name_ar  : $('#last_name_ar').val(),
				dob_month     : $('#dob_month').val(),
				dob_day       : $('#dob_day').val(),
				dob_year      : $('#dob_year').val(),
				gender        : $('#gender').val(),
				mobile        : $('#mobile').val(),
				email         : $('#email').val(),
				country       : $('#country').val(),
				emirates_id   : $('#emirates_id').val(),
				tbl_school_roles_id : $('#tbl_school_roles_id').val(),
				tbl_class_teacher_id : $('#tbl_class_teacher_id').val(),
				tbl_class_id  : selectednumbers,
				user_id       : $('#user_id').val(),
				password      : $('#password').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
				refresh_page = "Y";
				    my_alert("Teacher added successfully.", 'green');
				    $("#pre-loader").hide();
				}else{
					refresh_page = "N";
					my_alert("Teacher added failed, Please try again.", 'red');
					$("#pre-loader").hide();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_save_changes() {
		 
		 var selectednumbers='';
        	$('#tbl_class_id :selected').each(function(i, selected) {
            	selectednumbers += $(selected).val()+"&";
        	});
		
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/save_teacher_changes",
			data: {
				teacher_id_enc: $('#teacher_id_enc').val(),
				first_name    : $('#first_name').val(),
				last_name     : $('#last_name').val(),
				first_name_ar : $('#first_name_ar').val(),
				last_name_ar  : $('#last_name_ar').val(),
				dob_month     : $('#dob_month').val(),
				dob_day       : $('#dob_day').val(),
				dob_year      : $('#dob_year').val(),
				gender        : $('#gender').val(),
				mobile        : $('#mobile').val(),
				email         : $('#email').val(),
				country       : $('#country').val(),
				emirates_id   : $('#emirates_id').val(),
				tbl_school_roles_id : $('#tbl_school_roles_id').val(),
				tbl_class_teacher_id : $('#tbl_class_teacher_id').val(),
				tbl_class_id  : selectednumbers,
				user_id       : $('#user_id').val(),
				password      : $('#password').val(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Changes saved successfully.", 'green');
				
				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
</script>
<script language="javascript">
   //add student
   /* || validate_picture() == false*/
	function ajax_validate() {
		if (validate_first_name() == false || validate_last_name() == false || validate_dob() == false || isMobile() == false ||  validate_email() == false || 
		validate_country() == false || validate_emirates_id() == false ||  validate_class() == false || validate_user_id() == false || validate_password() == false 
		|| validate_confirm_password() == false  || isPasswordSame()==false || validate_exist_teacher()==false  || validate_exist_teacher_user_id()==false) 
		{
			return false;
		} 
	}
	

	
    //edit student
	function ajax_validate_edit() {
		if (validate_first_name() == false || validate_last_name() == false || validate_dob() == false || isMobile() == false ||  validate_email() == false || 
		validate_country() == false || validate_emirates_id() == false ||  validate_class() == false || validate_user_id() == false || validate_edit_password() == false || 
		validate_edit_confirm_password() == false || isEditPasswordSame()==false  || validate_exist_edit_teacher()==false || validate_exist_edit_teacher_user_id()==false ) 
		{
			return false;
		} 
	} 
	

	
  /************************************* START TEACHER VALIDATION *******************************/
   function validate_exist_teacher()
   {
	  
	   $.ajax({
				type: "POST",
				url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/is_exist_teacher",
				dataType: "text",
				data: {
					 teacher_id_enc: '<?=$teacher_id_enc?>',
					 emirates_id   : $('#emirates_id').val(),
					is_ajax: true
				},
				success: function(data) {
					    var temp = new String();
						temp = data;
						temp = temp.trim();
						if (temp=='Y') {
							my_alert("Teacher is already exist.", 'red');
							$("#pre-loader").hide();
							return false;
						}else{
							return true;
						}
					},
					error: function() {
						$("#pre-loader").hide();
					}, 
					complete: function() {
						$("#pre-loader").hide();
					}
				});
   }
   
    function validate_exist_teacher_user_id()
   {
	   $.ajax({
				type: "POST",
				url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/is_exist_teacher_user_id",
				dataType: "text",
				data: {
					 teacher_id_enc: '<?=$teacher_id_enc?>',
					 user_id   : $('#user_id').val(),
					is_ajax: true
				},
				success: function(data) {
					    var temp = new String();
						temp = data;
						temp = temp.trim();
						if (temp=='Y') {
							my_alert("User id is already exist. please use another User id", 'red');
							$("#pre-loader").hide();
							return false;
						}else{
						   ajax_create();
						}
					
					},
					error: function() {
						$("#pre-loader").hide();
					}, 
					complete: function() {
						$("#pre-loader").hide();
					}
				});
   }
   
   
  //edit validation case
  
    function validate_exist_edit_teacher()
   {
	   $.ajax({
				type: "POST",
				url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/is_exist_teacher",
				dataType: "text",
				data: {
					 teacher_id_enc: $('#teacher_id_enc').val(),
					 emirates_id   : $('#emirates_id').val(),
					is_ajax: true
				},
				success: function(data) {
					    var temp = new String();
						temp = data;
						temp = temp.trim();
						if (temp=='Y') {
							my_alert("Teacher is already exist.", 'red');
							$("#pre-loader").hide();
							return false;
						}else{
							return true;
						}
					},
					error: function() {
						$("#pre-loader").hide();
					}, 
					complete: function() {
						$("#pre-loader").hide();
					}
				});
   }
   
    function validate_exist_edit_teacher_user_id()
   {
	   $.ajax({
				type: "POST",
				url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/is_exist_teacher_user_id",
				dataType: "text",
				data: {
					 teacher_id_enc: $('#teacher_id_enc').val(),
					 user_id   : $('#user_id').val(),
					is_ajax: true
				},
				success: function(data) {
					    var temp = new String();
						temp = data;
						temp = temp.trim();
						if (temp=='Y') {
							my_alert("User id is already exist. please use another User id", 'red');
							$("#pre-loader").hide();
							return false;
						}else{
						    ajax_save_changes();
						}
					
					},
					error: function() {
						$("#pre-loader").hide();
					}, 
					complete: function() {
						$("#pre-loader").hide();
					}
				});
   }


   function validate_first_name() {
		var regExp = / /g;
		var str = $("#first_name").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("The First Name [En] is blank. Please write First Name [En].")
			$("#first_name").val('');
			$("#first_name").focus();
		return false;
		}
		var regExp = / /g;
		var str = $("#first_name_ar").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("The First Name [Ar] is blank. Please write First Name [Ar].")
			$("#first_name_ar").val('');
			$("#first_name_ar").focus();
		return false;
		}
	}
	
	function validate_last_name() {
		var regExp = / /g;
		var str = $("#last_name").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("The Last Name [En] is blank. Please write Last Name [En].")
			$("#last_name").val('');
			$("#last_name").focus();
		return false;
		}
		var regExp = / /g;
		var str = $("#last_name_ar").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("The Last Name [Ar] is blank. Please write Last Name [Ar].")
			$("#last_name_ar").val('');
			$("#last_name_ar").focus();
		return false;
		}
	}
	
	
	
	function isMobile() {
		var strPhone = $("#mobile").val();
		if($.trim(strPhone)== "") {
				   /* my_alert("Please enter mobile number.");
					$("#mobile").focus();
					return false;*/
					return true;
		}else{
				if (strPhone.length < 8 || strPhone.length > 10) {
					my_alert("Please enter valid mobile number.");
					$("#mobile").focus();
					return false;
			    }

				for (var i = 0; i < strPhone.length; i++) {
				var ch = strPhone.substring(i, i + 1);
					if  (ch < "0" || "9" < ch) {
						my_alert("The mobile number in digits only, Please re-enter your valid mobile number");
						$("#mobile").focus();
						return false;
					}
				}
		}
	 return true;
	}



	function validate_dob() {
		var month_index 	= $("#dob_month").val();
		var day_index	  = $("#dob_day").val();
		var year_index 	 = $("#dob_year").val();
		if (month_index == 0 && day_index == 0 && year_index ==0) {
			return true;
		}else if(month_index == 0 || day_index == 0 || year_index ==0) {
			my_alert("Please select Date of Birth.")
			return false		
		}
	}
	
		
	function validate_email() {
		var regExp = / /g;
		var str = $("#email").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
		     return true;	
		}
		if (!isNaN(str)) {
			my_alert("Invalid Email Id.");
			$("#email").focus();
			$("#email").select();
			return false;
		}

		if(str.indexOf('@', 0) == -1) {
			my_alert("Invalid Email Id.");
			$("#email").focus();
			$("#email").select();
			return false;
		}

	 return true;
	}



	function validate_emirates_id() {
		var regExp = / /g;
		var str = $("#emirates_id").val(); 
		str = str.replace(regExp,'');
		if (str.length <= 0) {
		    /*my_alert("Emirates Id is blank. Please write Emirates Id.")
			$("#emirates_id").focus();
		     return false;*/
			 return true;
		 }

		  var emrno = /^\(?([0-9]{3})\)?[-]?([0-9]{4})[-]?([0-9]{7})[-]?([0-9]{1})$/;  
		  if(str.match(emrno))  
		  {  
			   return true;    
		  }  
		  else  
		  {  
			   my_alert("Please write correct emirates id");  
			   return false;  
		 }  
	}


	function validate_country() {
		var regExp = / /g;
		var str = $('#country').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Nationality is mandatory. Please choose Nationality to proceed.")
		return false;
		}
	}

	
	 function validate_class() {
		var regExp = / /g;
		var str = $('#tbl_class_id').val();
		if (str==null ) {
			my_alert("Class Name not selected. Please select Class Name .");
		return false;
		}
	 return true;
	}
	

	
	function validate_user_id() {
		var str =  $("#user_id").val();
		if ( str=="" ) {
			my_alert("The User ID is blank. Please write User ID.");
			$("#user_id").focus();
			return false;
		}
		if (str.length < 6 ) {
			my_alert("The User ID should be greater than 5 characters.");
			$("#user_id").focus();
			return false;
		}
		if (!isNaN(str)) {
			my_alert("The User ID have only letters & digits, Please re-enter User ID");
			$("#user_id").focus();
			return false;
		}
	   return true;
	 }
	 
    /* Password */
	function validate_password() {
		var str = $('#password').val(); 
		if (str == "") {
			my_alert("The Password field is blank. Please enter Password.");
			$('#password').focus();
			return false;
		}
		if($('#password').val()!="") {
			var str = $('#password').val();
			var regExp = / /g;
			var tmp = $('#password').val();
			tmp = tmp.replace(regExp,'');
			if (tmp.length <= 0) {
				my_alert("Enter valid Password.");
				$('#password').focus();
			return false;
			}	
		}
		if (str.length < 6) {
			my_alert("The Password should be greater than 5 Characters.");
			$('#password').focus();
			$('#password').select();
			return false;
		}

	return true;
	}


	/* Retype Password */
	function validate_confirm_password() {
		var str = $('#confirm_password').val();
		if (str == "") {
			my_alert("The Confirm Password field is blank. Please Retype Password.");
			$('#confirm_password').focus();
			return false;
		}
		if($('#confirm_password').val()!="") {
				var str = $('#confirm_password').val();
				var regExp = / /g;
				var tmp = $('#confirm_password').val();
				tmp = tmp.replace(regExp,'');
				if (tmp.length <= 0) {
					my_alert("Enter valid Confirm Password.");
					$('#confirm_password').focus();
					return false;
				}	
			
		}
	return true;
	}

	/* Check both password */
	function isPasswordSame() {
		var str1 = $('#password').val();
		var str2 = $('#confirm_password').val();
		if (str1 != str2) {
			my_alert("Password mismatch, Please Retype same Passwords in both fields.");
			$('#confirm_password').focus();
			return false;
		}
	return true;
	}




      /* Password */
	function validate_edit_password() {
		var str = $('#password').val(); 
		if ($.trim(str) == "") {
			return true;
		}
		if($('#password').val()!="") {
			var str = $('#password').val();
			var regExp = / /g;
			var tmp = $('#password').val();
			tmp = tmp.replace(regExp,'');
			if (tmp.length <= 0) {
				my_alert("Enter valid Password.");
				$('#password').focus();
			return false;
			}	
		}
		if (str.length < 6) {
			my_alert("The Password should be greater than 5 Characters.");
			$('#password').focus();
			$('#password').select();
			return false;
		}

	return true;
	}


	/* Retype Password */
	function validate_edit_confirm_password() {
		var strPass = $('#password').val(); 
		if ($.trim(strPass) == "") {
			return true;
		}else{
			var str = $('#confirm_password').val();
				if (str == "") {
					my_alert("The Confirm Password field is blank. Please Retype Password.");
					$('#confirm_password').focus();
					return false;
				}
		}
		if($('#confirm_password').val()!="") {
				var str = $('#confirm_password').val();
				var regExp = / /g;
				var tmp = $('#confirm_password').val();
				tmp = tmp.replace(regExp,'');
				if (tmp.length <= 0) {
					my_alert("Enter valid Confirm Password.");
					$('#confirm_password').focus();
					return false;
				}	
		}
	return true;
	}
	
	function isEditPasswordSame() {
		var str1 = $('#password').val();
		var strPass = $('#password').val(); 
		if ($.trim(strPass) == "") {
			return true;
		}else{
				var str2 = $('#confirm_password').val();
				if (str1 != str2) {
					my_alert("Password mismatch, Please Retype same Passwords in both fields.");
					$('#confirm_password').focus();
					return false;
				}
		}
	return true;
	}



</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Teachers <small> Management</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Teachers</li>
    </ol>
     <div class="box-tools" style="float:right;">
     <a href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/mychild_private_message/tbl_parent_id/<?=$tbl_sel_parent_id?>/offset/0"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
     </div>
    
    <!--/BREADCRUMB-->
    <div style="clear:both"></div>
  </section>
      <link href="<?=HOST_URL?>/assets/admin/dist/css/jquery-ui.css" rel="stylesheet">
      <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-1.11.1.js"></script>
      <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-ui.js"></script>
      <link href="<?=HOST_URL?>/assets/admin/dist/css/uploadfile.min.css" rel="stylesheet">
 
  <script>
   function show_roles()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/school_role/";
		window.location.href = url;
	}     
  </script>
  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {
	?>
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/teacher/all_teachers";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
    
  
 <link href="<?=HOST_URL?>/assets/admin/dist/css/jquery-ui.css" rel="stylesheet">
 <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-1.11.1.js"></script>
 <script src="<?=HOST_URL?>/assets/admin/dist/js/jquery-ui.js"></script>
  <script>
  $( function() {
		   $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
		
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	
				
				
				
				
				
			}
	  } );
  
  } );
  </script> 
  
  
  
  <!--File Upload START-->
<link href="<?=HOST_URL?>/assets/admin/dist/css/uploadfile.min.css" rel="stylesheet">
<script>
 $( function() {
    $( "#tabs" ).tabs();
  } );
  
 
</script>
<style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url('<?=IMG_PATH?>/delete.jpg');
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
		clear:both;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
	#overlay_container {
		position:relative;
	}
	#overloading {
		background-image:url('<?=IMG_PATH?>/preloader/preloader_2.gif');
		background-repeat:no-repeat;
		background-position:center;
		background-color:#CCC;
		position:absolute;
		left:0px;
		top:0px;
		opacity: 0.3;
		z-index: 10000;
	}
	#div_listing_container {
		display:none;	
	}
	.d_d_text {
		color:#745156;
		font-size:20px;
			
	}
	.ajax-upload-dragdrop {
		margin:auto;
		margin-bottom:10px;
		width:700px !important;
	}
	.ajax-file-upload-statusbar {
		margin:auto;
		margin-top:10px;
	}
	.ajax-file-upload {
		height:31px;
	}
	
	
	 #tabs-1{  
	    overflow-y:scroll; overflow-x:none;
	}

    #tabs-2{
		overflow-y:scroll; overflow-x:none;
	}
				  
  .ui-tabs-active{
		border-color:#efca86  !important;
   }
					 
	.ui-tabs .ui-tabs-nav li {
		float:left;
		font-size: 16px;
        font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif;
  }
  label{
	  display: inline-block;
      font-weight: 700;
  }
  
  .ui-widget input, .ui-widget select, .ui-widget textarea, .ui-widget button {
    font-family:"Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
    font-size: 14px;
}
  
  .ui-widget{
	 font-size: 16px;
     font-family: "Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif; 
  }
  .form-control{
	 font-size: 14px; 
  }
</style>
 

  
          
    
            <!--Listing-->
                    <div id="mid1_list" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Teachers</h3>
                          <div class="box-tools">
                            <?php if (count($rs_all_teachers)>0) { echo $paging_string;}?>	
                          </div>
                        </div>
                        
                        <div class="box-body">
                     <!--   <div style="color:#030; font-weight:bold;">You can sort students by using drag and drop of rows </div>-->
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <!--<th width="10%" align="center" valign="middle">Sl No.</th>-->
                              <th width="25%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y">Name <?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?><div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>
                             <!-- <th width="20%" align="center" valign="middle">Class</th>-->
                              <th width="20%" align="center" valign="middle">Contact</th>
                              <th width="10%" align="center" valign="middle">Picture</th>                              
                              <th width="15%" align="center" valign="middle">Messages</th>
                              <th width="5%" align="center" valign="middle">Status</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                                for ($i=0; $i<count($rs_all_teachers); $i++) { 
                                    $id = $rs_all_students[$i]['id'];
                                    $tbl_teacher_id     = $rs_all_teachers[$i]['tbl_teacher_id'];
                                    $name_en            = ucfirst($rs_all_teachers[$i]['first_name'])." ".ucfirst($rs_all_teachers[$i]['last_name']);
									$name_ar            = $rs_all_teachers[$i]['first_name_ar']." ".$rs_all_teachers[$i]['last_name_ar'];
                                    $mobile             = $rs_all_teachers[$i]['mobile'];
									$pic                = $rs_all_teachers[$i]['file_name_updated'];
									$email              = $rs_all_teachers[$i]['email'];
									$gender             = ucfirst($rs_all_teachers[$i]['gender']);
                                    $added_date         = $rs_all_teachers[$i]['added_date'];
                                    $is_active          = $rs_all_teachers[$i]['is_active'];
									$class_name         = $rs_all_teachers[$i]['class_name'];
                                    $class_name_ar      = $rs_all_teachers[$i]['class_name_ar'];
									$section_name       = $rs_all_teachers[$i]['section_name'];
									$section_name_ar    = $rs_all_teachers[$i]['section_name_ar'];
                                    $school_type        = $rs_all_teachers[$i]['school_type'];
									$school_type_ar     = $rs_all_teachers[$i]['school_type_ar'];
									if($pic<>"") // class="img-circle"
										$pic_path           =   '<img width="100" height="80" src="'.IMG_PATH_TEACHER.'/'.$pic.'"  />';
									else
										$pic_path           =   '<img width="100" height="80" src="'.IMG_PATH_STUDENT.'/no_img.png"  
										style="border-color:1px solid #7C858C !important;" />';
                                    
                                    
                                    $added_date = date('m-d-Y',strtotime($added_date));
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_teacher_id?>" >
                              <td align="left" valign="middle">
                              <div class="txt_en"><?=$name_en?></div>
                              <div class="txt_ar"><?=$name_ar?></div><br />
                             <?php if($gender=="Female"){?>
                              <i class="fa fa-female" aria-hidden="true"></i>
                             <?php } else { ?>
                              <i class="fa fa-male" aria-hidden="true"></i>
                             <?php } ?>
                              
                              
                              
                              </td>
                             <?php /*?> <td align="left" valign="middle"> 
                              <div class="txt_en"><?=$class_name?>&nbsp;<?=$section_name?></div>
                              <div class="txt_ar"><?=$class_name_ar?>&nbsp;<?=$section_name_ar?></div></td><?php */?>
                              <td align="left" valign="middle"><?php /*?><a href="mailto:<?=$email?>"><?php */?><?=$email?><br /><?=$mobile?><!--</a>--></td>
                              <td align="left" valign="middle"><?=$pic_path?></td>
                              <td align="left" valign="middle"><a href="<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/mychild_teacher_messages/tbl_teacher_id/<?=$tbl_teacher_id?>/child_id_enc/<?=$tbl_sel_student_id?>/tbl_school_id/<?=$tbl_sel_school_id?>/offset/0">View Messages</a></td>
                              <td align="left" valign="middle">
                                <div id="act_deact_<?=$tbl_teacher_id?>">
                                <?php if (trim($is_active) == "Y") { ?>
                                    Active
                                <?php } else { ?>
                                   Inactive
                                <?php } ?>
                                </div>
                              </td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="10" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_all_teachers)<=0) {
                            ?>
                            <tr>
                              <td colspan="10" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no teachers available. Click on the + button to create one.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
    
            <!--Add or Create-->
            <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Teacher</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
   	            <div class="box-body">
                    <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
    
                  
                       
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="first_name">First Name [En]<span style="color:#F30; padding-left:2px;">*</span></label>
                          <div class="col-sm-10">
                            <input type="text" placeholder="First Name[En]" id="first_name" name="first_name" class="form-control">
                          </div>
                          
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="first_name_ar">First Name [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10">
                            <input type="text" placeholder="First Name[Ar]" id="first_name_ar" name="first_name_ar" class="form-control" dir="rtl">
                          </div>
                        </div>
                        
                          <div class="form-group">
                          <label class="col-sm-2 control-label" for="last_name">Last Name [En]<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10">
                            <input type="text" placeholder="Last Name[En]" id="last_name" name="last_name" class="form-control">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="last_name_ar">Last Name [Ar]<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10">
                            <input type="text" placeholder="Last Name[Ar]" id="last_name_ar" name="last_name_ar" class="form-control" dir="rtl">
                          </div>
                        </div>
                        
                     
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="dob_month">DOB</label>
        
                          <div class="col-sm-2">
                            <select name="dob_month" id="dob_month"  class="form-control" tabindex="6">
                                            <option value="">--Month--</option>
                                            <?php for ($m=1; $m<=12; $m++) { ?>
                                            <option value="<?=$m?>" <?php if ($dob_month == $m) {echo "selected";}?> ><?=$m?></option>
                                            <?php } ?>
                            </select>
                         </div>
                         <div class="col-sm-2">
                                          <?php if (!isset($dob_day) || trim($dob_day) == "") {$dob_day = '';}?>
                                          <select name="dob_day" id="dob_day" tabindex="7" class="form-control">
                                            <option value="">--Day--</option>
                                            <?php for ($d=1; $d<=31; $d++) { ?>
                                            <option value="<?=$d?>" <?php if ($dob_day == $d) {echo "selected";}?> ><?=$d?></option>
                                            <?php } ?>
                        </select>
                        </div>
                        <div class="col-sm-2">
                                          <?php if (!isset($dob_year) || trim($dob_year) == "") {$dob_year = '';}?>
                                          <select name="dob_year" id="dob_year" tabindex="8" class="form-control">
                                            <option value="">--Year--</option>
    
                                            <?php for ($y=1950; $y<=date('Y'); $y++) { ?>
                                            <option value="<?=$y?>" <?php if ($dob_year == $y) {echo "selected";}?> ><?=$y?></option>
                                            <?php } ?>
                                          </select>              
                        </div>  
                        </div>
                        
                        
                         <div class="form-group">
                          <label class="col-sm-2 control-label" for="gender">Gender<span style="color:#F30; padding-left:2px;">*</span></label>
                           <div class="col-sm-10">
                            <label>
                              <input type="radio" id="gender" name="gender" value="male" class="minimal"  checked="checked"  >
                              Male
                            </label>
                            &nbsp;
                            <label>
                              <input type="radio" id="gender" name="gender" value="female" class="minimal"  >
                              Female
                            </label>
                          </div>
                        </div>
                        
                         <div class="form-group">
                          <label class="col-sm-2 control-label" for="mobile">Mobile</label>
        
                          <div class="col-sm-10">
                           <span style="position:absolute; padding-left:20px; padding-top:5px;"> +971</span><input type="text" placeholder="" id="mobile" name="mobile" class="form-control" style="padding-left:60px;">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="email">Email</label>
        
                          <div class="col-sm-10">
                            <input type="text" placeholder="Email" id="email" name="email" class="form-control">
                          </div>
                        </div>
                        
                         <div class="form-group">
                          <label class="col-sm-2 control-label" for="country">Nationality</label>
        
                          <div class="col-sm-10">
                          
                                          <select name="country" id="country" tabindex="8" class="form-control">
                                            <?php for ($c=0; $c<count($countries_list); $c++) { ?>
                                            <option value="<?=$countries_list[$c]['country_id']?>"><?=$countries_list[$c]['country_name']?></option>
                                            <?php } ?>
                                          </select>         
                          </div>
                        </div>
                        
                        
                          <div class="form-group">
                          <label class="col-sm-2 control-label" for="emirates_id">Emirates Id</label>
        
                          <div class="col-sm-10">
                            <input type="text" placeholder="Emirates Id" id="emirates_id" name="emirates_id" class="form-control">
                          </div>
                        </div>
                        
                        
                        
                        <div class="form-group">
                         <label class="col-sm-2 control-label" for="tbl_school_roles_id">Role</label>
                         <div class="col-sm-10">
                                     <select name="tbl_school_roles_id" id="tbl_school_roles_id" class="form-control">
                                  <option value="">--Select Role --</option>
                                  
                                  <?php
                                        for ($u=0; $u<count($teacher_roles_list); $u++) { 
                                            $tbl_school_roles_id_u  = $teacher_roles_list[$u]['tbl_school_roles_id'];
                                            $role                   = $teacher_roles_list[$u]['role'];
                                            $role_ar                = $teacher_roles_list[$u]['role_ar'];
                                            $section_name           = $classes_list[$u]['section_name'];
                                            $section_name_ar        = $classes_list[$u]['section_name_ar'];
                                            if($tbl_sel_roles_id == $tbl_school_roles_id_u)
                                               $selClass = "selected";
                                             else
                                               $selClass = "";
                                      ?>
                                          <option value="<?=$tbl_school_roles_id_u?>"  <?=$selClass?> >
                                          <?=$role?>&nbsp;[::]&nbsp;<?=$role_ar?>&nbsp;
                                          </option>
                                          <?php
                                        }
                                    ?>
                                 </select>
                       </div>
                       </div> 
                        
                        <div class="form-group">
                         <label class="col-sm-2 control-label" for="tbl_class_id">Class<span style="color:#F30; padding-left:2px;">*</span></label>
                         <div class="col-sm-10">
                                     <select name="tbl_class_id" id="tbl_class_id" class="form-control" multiple size="10" style="padding:0px 5px 0px 2px; font-size:14px; " >
                                  <option value="">--Select Class --</option>
                                  
                                  <?php
                                        for ($u=0; $u<count($classes_list); $u++) { 
                                            $tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
                                            $class_name             = $classes_list[$u]['class_name'];
                                            $class_name_ar          = $classes_list[$u]['class_name_ar'];
                                            $section_name           = $classes_list[$u]['section_name'];
                                            $section_name_ar        = $classes_list[$u]['section_name_ar'];
                                            if($tbl_sel_class_id == $tbl_class_id_u)
                                               $selClass = "selected";
                                             else
                                               $selClass = "";
                                      ?>
                                          <option value="<?=$tbl_class_id_u?>"  <?=$selClass?> >
                                          <?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;
                                        <?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                                          </option>
                                          <?php
                                        }
                                    ?>
                                 </select>
                       </div>
                       </div>
                       
                         <div class="form-group">
                         <label class="col-sm-2 control-label" for="tbl_class_teacher_id">Class Teacher</label>
                         <div class="col-sm-10">
                                     <select name="tbl_class_teacher_id" id="tbl_class_teacher_id" class="form-control">
                                  <option value="">--Select Class --</option>
                                  
                                  <?php
                                        for ($u=0; $u<count($classes_list); $u++) { 
                                            $tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
                                            $class_name             = $classes_list[$u]['class_name'];
                                            $class_name_ar          = $classes_list[$u]['class_name_ar'];
                                            $section_name           = $classes_list[$u]['section_name'];
                                            $section_name_ar        = $classes_list[$u]['section_name_ar'];
                                            if($tbl_sel_class_id == $tbl_class_id_u)
                                               $selClass = "selected";
                                             else
                                               $selClass = "";
                                      ?>
                                          <option value="<?=$tbl_class_id_u?>"  <?=$selClass?> >
                                          <?=$class_name?>&nbsp;<?=$section_name?>&nbsp;[::]&nbsp;
                                        <?=$class_name_ar?>&nbsp;<?=$section_name_ar?>
                                          </option>
                                          <?php
                                        }
                                    ?>
                                 </select>
                       </div>
                       </div>
                       
                       
                        
                      
                           <div class="form-group">
                          <label class="col-sm-2 control-label" for="picture">Picture</label>
        
                          <div class="col-sm-10">
                            <!--File Upload START-->
                                <style>
                                #advancedUpload {
                                    padding-bottom:0px;
                                }
                                </style>
                                     
                                <div id="advancedUpload">Upload File</div>
                                
                                <div id="uploaded_items" >
                                    <div id="div_listing_container" class="listing_container" style="display:block">	            
                                            <?php
                                                if (trim($img_url) != "") {
                                            ?>
                                                        <div id='<?=$tbl_uploads_id?>' class='box-header with-border'>
                                                          <div class='box-title'><img src='<?=$img_url?>' /></div>
                                                          <div class='box-tools'> <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick="confirm_delete_img_popup('<?=$tbl_uploads_id?>')">
                                                            </button>
                                                          </div>
                                                        </div>
                                                <style>
                                                    .ajax-upload-dragdrop {
                                                        display:none;	
                                                    }
                                                </style>        
                                            <?php		
                                                }
                                            ?>
                                    </div>        
                                </div>
                            <!--File Upload END-->
                          </div>
                        </div>
                        
                   
                       
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="user_id">User Id<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10">
                            <input type="text" placeholder="User Id " id="user_id" name="user_id" class="form-control">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="password">Password<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10">
                            <input type="password" placeholder="Password" id="password" name="password" class="form-control">
                          </div>
                        </div>
                        
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="confirm_password">Confirm Password<span style="color:#F30; padding-left:2px;">*</span></label>
        
                          <div class="col-sm-10">
                            <input type="password" placeholder="Confirm Password" id="confirm_password" name="confirm_password" class="form-control">
                          </div>
                        </div>
                     
                        <!-- /.box-body -->
                      <div class="box-footer">
                        <button class="btn btn-primary" type="button" onclick="ajax_validate()">Submit</button>
                        <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                      </div>
                      <!-- /.box-footer -->  
                      
            
           </form>
                </div>
           </div>
                
  
            <!--/Add or Create-->
                
        <!--/Admin Category Management-->

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>


<script src="<?=HOST_URL?>/assets/admin/dist/js/jquery.uploadfile.min.js"></script>
<script language="javascript">

//Primary Key for a Form. 

function set_item_id(obj) {
	item_id = obj.value;
	get_files();	
}

$(document).ready(function() {
	var uploadObj = $("#advancedUpload").uploadFile({
		url:"<?=HOST_URL?>/file_mgmt/upload_the_file",
		multiple:true,
		autoSubmit:true,
		maxFileSize:130000,
		fileName:"myfile",
		formData: {"module_name":"teacher"},
		dynamicFormData: function() {
			var data = { item_id:item_id}
			return data;
		},
		showStatusAfterSuccess:false,
		dragDropStr: "<span class='d_d_text'>Optionally Drag and Drop the File to Upload.</span>",
		abortStr:"Abourt",
		cancelStr:"Cancel",
		doneStr:"Done",
		multiDragErrorStr: "Multi Drag Error.",
		extErrorStr:"Extention Error:",
		sizeErrorStr:"Max Size Error:",
		uploadErrorStr:"Upload Error",
		onSelect:function(files) {
 		},
		onSubmit:function(files) {
 		},
		onSuccess:function(files, data, xhr) {
			if (data == "error") {
				alert("Error uploading file. Please try again.");
				return;
			}
			var obj = JSON.parse(data);
			var tbl_uploads_id = obj.tbl_uploads_id;
			var file_name_updated = obj.file_name_updated;
			
			//alert("tbl_uploads_id: "+tbl_uploads_id)
			//alert("file_name_updated: "+file_name_updated)
			add_uploaded_item(tbl_uploads_id, file_name_updated);
		},
		afterUploadAll:function() {
 		},
		onError: function(files, status, errMsg) {
 		}
	});

	$("#startUpload").click(function() {
		uploadObj.startUpload();
	});
	
	try { 
		$('input[type=file]').click();
	} catch(e) {
		alert(e)
	}
});

//Function called when file is uploaded
function add_uploaded_item(tbl_uploads_id, file_name_updated) {
	var str = "<div id='"+tbl_uploads_id+"' class='box-header with-border'> <div class='box-title'><img src='<?=IMG_PATH_TEACHER?>/"+file_name_updated+"' /></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_img_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
		
	$("#div_listing_container").show();
	$("#div_listing_container").append(str);
	$(".ajax-upload-dragdrop").hide();//Hide the upload button
return;
}

function confirm_delete_img_popup(tbl_uploads_id) {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		$('#'+tbl_uploads_id).hide();	
		$(".ajax-upload-dragdrop").show();
		
		var url_str = "<?=HOST_URL?>/file_mgmt/delete_file";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					tbl_uploads_id: tbl_uploads_id
				},
			success: function(data) {
				$("#pre-loader").hide();
			}
		});	
	} else {
		$("#pre-loader").hide();		
	}
}

function get_files() {
	var url_str = "<?=HOST_URL?>/misc/get_files.php";
	
	$.ajax({
		type: "POST",
		url: url_str,
		data: {
				module_name: "teacher",
				show_del: "Y",
				item_id: item_id//global variable
			},
		success: function(data) {
			$('#div_listing_container').show();
			$('#div_listing_container').html(data)
			
		}
	});	
}
</script>
<!--File Upload END-->
        
    <!--/WORKING AREA--> 
  </section>
</div>

<script language="javascript" >
function search_data() {
		var tbl_class_search_id = $("#tbl_class_search_id").val();
		var q = $.trim($("#q").val()); 
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/all_teachers/";
		if(tbl_class_search_id !='')
			url += "tbl_class_search_id/"+tbl_class_search_id+"/";
		if(q !='')
			url += "q/"+q+"/";
		
			url += "offset/0/";
		window.location.href = url;
		<?php /*?>window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/enquiry/all_enquiries/is_not_replied/"+is_not_replied+"/tbl_court_id/"+tbl_court_id+"/tbl_category_id/"+tbl_category_id;<?php */?>
	}
function generate_teacher_id(offset) {
		var tbl_class_search_id = $("#tbl_class_search_id").val();
		var q = $("#q").val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/generate_teacher_id/";
		if(tbl_class_search_id !='')
			url += "tbl_class_id/"+tbl_class_search_id+"/";
		if(q !='')
			url += "q/"+q+"/";
		
			url += "offset/"+offset+"/";
		window.open(url);
	}


function reset_data() {
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/all_teachers/";
		url += "offset/0/";
		window.location.href = url;
	}
</script>