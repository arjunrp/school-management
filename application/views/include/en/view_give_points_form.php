<div class="spc_h20"></div>
<div class="panel_star panel_star_pthm">
	<div class="spc_panel_inner"></div>
	<div class="leave_panel">
		<div class="spc_h10">&nbsp;</div>
        <div class="head_sml">Points</div>
        <div class="spc_controls">&nbsp;</div>
        <div id="points_bar" class="points_bar">
        	<div id="points_bar_pointer" class="points_bar_pointer"></div>
        </div>

        <div class="spc_h10">&nbsp;</div>
        <div class="grid">
        	<div class="col3" style="float:left; width:33%; text-align:left"><img id="minus" src="images/minus.jpg" onclick="goto_minus()" /></div>
            <div class="col3" style="float:left; width:32%; text-align:center"><div class="points_student" id="points">0</div></div>
            <div class="col3" style="float:left; width:33%; text-align:right"><img id="plus" src="images/plus.jpg" onclick="goto_plus()" /></div>
        </div>

        <div class="spc_h10">&nbsp;</div>
        <div class="head_sml">Comments</div>
        <div class="spc_controls">&nbsp;</div>
		<textarea name="application_message_gp" id="application_message_gp" class="application_message"></textarea>
        <div class="spc_h10">&nbsp;</div>
        <div style="width:100%; margin:auto">
            <div class="btn_green" onclick="give_points('gp', '<?=$tbl_class_id?>', '<?=$tbl_student_id?>')">ASSIGN POINTS</div>
        </div>
        <div class="spc_panel_inner">&nbsp;</div>
        <div class="spc_h10">&nbsp;</div>
        
         </div>
    <input type="hidden" id="file_name_updated_gp" name="file_name_updated_gp" value="<?=$file_name_updated?>" />
    
    
