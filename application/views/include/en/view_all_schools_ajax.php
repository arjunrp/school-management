<?php	
	for ($i=0; $i<count($rs_all_schools); $i++) {
		$tbl_school_id = $rs_all_schools[$i]['tbl_school_id'];
		$school_name = $rs_all_schools[$i]['school_name'];
		$school_name_ar = $rs_all_schools[$i]['school_name_ar'];
		$school_type = $rs_all_schools[$i]['school_type'];
		$email = $rs_all_schools[$i]['email'];
		$contact_person = $rs_all_schools[$i]['contact_person'];		
		$phone = $rs_all_schools[$i]['phone'];
		$mobile = $rs_all_schools[$i]['mobile'];
		$fax  = $rs_all_schools[$i]['fax'];
		$address = $rs_all_schools[$i]['address'];
		$color_code = $rs_all_schools[$i]['color_code'];	
		$picture = $rs_all_schools[$i]['logo'];
		$is_active = $rs_all_schools[$i]['is_active'];
		
		$arr[$i]["tbl_school_id"] = $tbl_school_id;
		$arr[$i]["school_name_en"] = $school_name;
		$arr[$i]["school_name_ar"] = $school_name_ar;
	}
	$arr_schools["school_list"] = $arr;
	echo json_encode($arr_schools);
?>