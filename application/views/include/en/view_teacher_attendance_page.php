<?php
$CI =& get_instance();
$CI->load->model("model_teacher_attendance");				
?>
<?
$all_students_arr = array();
for ($i=0; $i<count($data_rs); $i++) {
	$tbl_student_id = $data_rs[$i]['tbl_student_id'];
	$first_name = $data_rs[$i]['first_name'];
	$last_name = $data_rs[$i]['last_name'];

	$stu_obj = $CI->model_student->get_student_obj($tbl_student_id);
	$image_student = $stu_obj[0]['file_name_updated'];

	if (trim($lan) == "ar") {
		$first_name = $data_rs[$i]['first_name_ar'];
		$last_name = $data_rs[$i]['last_name_ar'];
	}
	
	$name = $first_name. " ".$last_name;
	$is_present = $CI->model_teacher_attendance->get_attendance_t($tbl_student_id, $tbl_teacher_id, $tbl_class_id, $attendance_date,  $att_option);
	$dataopt = $is_present;
	if (trim($dataopt) == "" || trim($dataopt) == "null") {
		$dataopt = "Y";
	}
	$student_arr = array();
	$student_arr['student_id'] = $tbl_student_id;
	$student_arr['title'] = $name;
	$student_arr['val'] = $dataopt;
	//$student_arr['url'] = IMG_PATH_STUDENT."/".$image_student;
	if($image_student=="")
	{
		$student_arr["url"] = IMG_PATH_STUDENT."/avatar_default.png";
	}else{
		$student_arr["url"] = IMG_PATH_STUDENT."/".$image_student;
	}
		$all_students_arr[$i] = $student_arr;
	}
	$arr_main = array();
	$arr_main["date"] = $attendance_date;
	$arr_main["is_attendance_exists"] = $is_attendance_exists;
	$arr_main["students"] = $all_students_arr;
	
	echo json_encode($arr_main);
?>
