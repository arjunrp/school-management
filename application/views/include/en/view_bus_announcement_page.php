<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Khalifa Empowerment Program For Studends - U.A.E.</title>
<style>
	body { width:1366px; height:768px; margin:0 auto; border:1px solid red; font-family:Arial, Helvetica, sans-serif;}
	.logo {text-align:center;}
	.clr_black {color:#000000 !important;}
	
	.spacer {height:100px;}
	.spacer_mid {height:12px;}
	.picture {text-align:center; width:250px; height:250px; margin:0 auto;background-color:#CCC;}
	.picture img { width:220px; height:220px; margin:16px;}
	.child_details { margin:0px 16px;; margin-top:0px; background-color:#CCC; height:75px; text-align:center; color:#999; line-height:80px; font-size:26px;}
	.container {width:900px; margin:0 auto;}
	.message {text-align:center; font-size:30px; color:#FFF; background-color:#900; padding:10px 0px; animation: btn_blink 2s;animation-iteration-count: infinite;-webkit-animation:btn_blink 2s; -webkit-animation-iteration-count: infinite; display:none; }
	
	/*Animation Blink*/
	@keyframes btn_blink {0% {opacity: 1;}49% {opacity: 1;}50% {opacity: 0;}100% {opacity: 0;}}
	@-webkit-keyframes btn_blink {0% {opacity: 1;}49% {opacity: 1;}50% {opacity: 0;}100% {opacity: 0;}}
</style>

<script language="javascript" src="<?=JS_PATH?>/jquery-1.3.2.min.js"></script>

<script language="javascript">

	var host = "<?=HOST_URL?>";
	var img_path = "<?=IMG_PATH?>";
	var lan = "<?=$lan?>";
	var tbl_school_id = "<?=$tbl_school_id?>";
	var ajax_timeout = 150000;
	
	$(document).ready(function(e) {
		get_bus_announcement_ajax();
		setInterval(function(){ get_bus_announcement_ajax(); }, 10000);
    });
	
	function get_bus_announcement_ajax() {
		var url = host+"/bus_announcement/get_bus_announcement_ajax/?lan="+lan+"&tbl_school_id="+tbl_school_id;
		//alert(url);
		$.ajax({
			url: url,
			success: function (data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
		
				if (temp.indexOf("no--")>0 ) {
					$("#student_name").html("Student Name");
					$("#class_name").html("Class Name");
					$("#picture").attr("src", img_path+"/student.png");
					$("#bus_number").html("Bus Number");
					$("#bus_message_display").hide();
					
					$("#student_name").removeClass("clr_black");
					$("#class_name").removeClass("clr_black");
					$("#bus_number").removeClass("clr_black");
					return;
				}
				
				var obj = JSON.parse(data);
				var first_name_stu = obj.first_name_stu;
				var last_name_stu = obj.last_name_stu;
				var class_name = obj.class_name;
				var section_name = obj.section_name;
				var picture = obj.picture;
				var bus_number = obj.bus_number;
				var bus_code = obj.bus_code;
				var bus_message_display = obj.bus_message_display;
				var bus_message_speak = obj.bus_message_speak;
				
				$("#student_name").html(first_name_stu+" "+last_name_stu);
				$("#class_name").html(class_name+" "+section_name);
				
				if (picture != "") {
					$("#picture").attr("src", picture);
				} else {
					$("#picture").attr("src", img_path+"/student.png");
				}
				
				$("#bus_number").html(bus_number+" ["+bus_code+"]");
				$("#class_name").html(class_name+" "+section_name);
				$("#bus_message_display").html(bus_message_display);
				$("#bus_message_display").show();
				
				$("#student_name").addClass("clr_black");
				$("#class_name").addClass("clr_black");
				$("#bus_number").addClass("clr_black");
				
			},timeout:ajax_timeout,
			error: function(){return;}	// Ajax request timed out
		});
	}

</script>

</head>
<body>
    <div class="logo">
		<img src="<?=IMG_PATH?>/aqdar.png" />
    </div>
	<div class="spacer"></div>
    <div class="container">
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td width="250"><div class="picture"> <img id="picture" src="<?=IMG_PATH?>/student.png" alt="" /> </div></td>
            <td align="left" valign="top">
                <div class="child_details" id="student_name">Student Name</div>
                <div class="spacer_mid"></div>
                
                <div class="child_details" id="class_name">Class Name</div>
                <div class="spacer_mid"></div>
                
                <div class="child_details" id="bus_number">Bus Number</div>
            </td>
          </tr>
        </table>
        <div class="spacer"></div>
        <div class="message" id="bus_message_display">Please go to the bus XXXXXXX</div>
	</div>    
</body>
</html>