<?php 	
	ini_set('default_charset', 'utf-8'); 
	$CI = & get_instance();
	$arr["first_name"] 		= $first_name;
	$arr["last_name"] 		= $last_name;
	//print_r($data_stu);
	for ($i=0; $i<count($data_stu); $i++) {	
		$tbl_student_id = $data_stu[$i]["tbl_student_id"];
		$CI->load->model("model_student");
		$CI->load->model("model_classes");
		$stu_obj 		=  $CI->model_student->get_student_obj($tbl_student_id);
		
		if ($stu_obj[0]["is_active"] != "Y") {
			continue;	
		}
		if ($stu_obj[0]["is_approved"] != "Y") {
			continue;	
		}
		$first_name 	= $stu_obj[0]['first_name'];
		$last_name 		= $stu_obj[0]['last_name'];
		$first_name_ar 	= $stu_obj[0]['first_name_ar'];
		$last_name_ar 	= $stu_obj[0]['last_name_ar'];
		$first_name_ar 	= $stu_obj[0]['first_name_ar'];
		$last_name_ar 	= $stu_obj[0]['last_name_ar'];
		$tbl_class_id 	= $stu_obj[0]['tbl_class_id'];
		$rs_class 		= $CI->model_classes->getClassInfo($tbl_class_id);
		$tbl_section_id = $rs_class[0]["tbl_section_id"];
		$class_name 	= isset($rs_class[0]["class_name"])? $rs_class[0]["class_name"] : '' ;
		$class_name_ar 	= isset($rs_class[0]["class_name_ar"])? $rs_class[0]["class_name_ar"] : '' ;
		$data_se 		= $CI->model_classes->getClassSectionInfo($tbl_section_id);
		$section_name 	=  isset($data_se[0]["section_name"])? $data_se[0]["section_name"]:'';
		$section_name_ar=  isset($data_se[0]["section_name_ar"])? $data_se[0]["section_name_ar"]:'';
		$file_name_updated 	= $stu_obj[0]['file_name_updated'];
		$tbl_school_id 		= $stu_obj[0]['tbl_school_id'];
		if ($lan == "en") {
			$name = $first_name." ".$last_name;
		} else {
			$name = $first_name_ar." ".$last_name_ar;
		}
		$CI->load->model("model_school");
		$school_name = $CI->model_school->get_school_name($tbl_school_id,$lan); 
		$arr_students[$i]["student_id"] = $tbl_student_id;
		$arr_students[$i]["name"] = $name;
		$arr_students[$i]["school_name"] = $school_name;
		$arr_students[$i]["school_id"] = $tbl_school_id;
		if($file_name_updated<>""){
			$arr_students[$i]["url"] = IMG_PATH."/student/".$file_name_updated;
		}else{
			$arr_students[$i]["url"] = "";
		}
		if ($lan == "en") {
			$arr_students[$i]["class_name"] = $class_name;
			$arr_students[$i]["section_name"] = $section_name;
		}else{
			$arr_students[$i]["class_name"] = $class_name_ar;
			$arr_students[$i]["section_name"] = $section_name_ar;
		}
		$arr_students[$i]["tbl_class_id"] = $tbl_class_id;
	}	
	//print_r($arr_students);
	$arr["login_id"] 		= $tbl_parent_id;
	$arr["role"] = "P";
	$arr["students"] = !empty($arr_students)? $arr_students : array();
	echo json_encode($arr,JSON_UNESCAPED_UNICODE);
?>

