<?php	$CI = & get_instance();
		
	for ($i=0; $i<count($data_rs); $i++) {
		$tbl_class_id = $data_rs[$i]['tbl_class_id'];
		$tbl_section_id = $data_rs[$i]['tbl_section_id'];
		$CI->load->model("model_section");
		$data_sec = $CI->model_section->get_section_obj($tbl_section_id);
		
		$CI->load->model("model_classes");
		$data_ct = $CI->model_classes->get_class_teacher($tbl_class_id);
		
		if (trim($lan) == "ar") {
			$class_name = $data_rs[$i]['class_name_ar'];
			$section_name = $data_sec[0]['section_name_ar'];
			$class_teacher_name = $data_ct[0]['first_name_ar']." ".$data_ct[0]['last_name_ar'];
		} else {
			$class_name = $data_rs[$i]['class_name'];
			$section_name = $data_sec[0]['section_name'];
			$class_teacher_name = $data_ct[0]['first_name']." ".$data_ct[0]['last_name'];
		}
		
		$arr_classes[$i]["class_id"] = $tbl_class_id;
		$arr_classes[$i]["title"] = $class_name;
		$arr_classes[$i]["section_id"] = $tbl_section_id;
		$arr_classes[$i]["section_title"] = $section_name;
		$arr_classes[$i]["class_teacher"] = trim($class_teacher_name);
	}
	
	$arr["classes"] = $arr_classes;
	echo json_encode($arr);
?>


    
