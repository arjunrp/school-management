<?php 
	$CI =& get_instance();
	$CI->load->model("model_parents");				
	
	$arr_main = array();
	for ($i=0; $i<count($data_rs); $i++) {
		$id = $data_rs[$i]['id'];
		$tbl_parent_id = $data_rs[$i]['tbl_parent_id'];
		$tbl_student_id = $data_rs[$i]['tbl_student_id'];
		$comments_parent = $data_rs[$i]['comments_parent'];
		$added_date = $data_rs[$i]['added_date'];
		$added_date = date("H:i - d/m/Y",strtotime($added_date));
		
		/*$CI->load->model("model_parents");	
		$parent_obj = $CI->model_parents->get_parent_obj($tbl_parent_id);
		$first_name = $parent_obj[0]['first_name'];	
		$last_name = $parent_obj[0]['last_name'];	
	
		if (trim($lan) == "ar") {
			$first_name = $parent_obj[0]['first_name_ar'];	
			$last_name = $parent_obj[0]['last_name_ar'];
		}
		$arr_leaves[$i]["parent"] = $first_name." ".$last_name;*/
		
		$CI->load->model("model_student");	
		$student_obj = $CI->model_student->get_student_obj($tbl_student_id);
		$tbl_student_id = $student_obj[0]['tbl_student_id'];
		$tbl_class_id = $student_obj[0]['tbl_class_id'];
		$first_name = $student_obj[0]['first_name'];	
		$last_name = $student_obj[0]['last_name'];	
	
		if (trim($lan) == "ar") {
			$first_name = $student_obj[0]['first_name_ar'];	
			$last_name = $student_obj[0]['last_name_ar'];
		}
		
		$CI->load->model("model_classes");	
		$class_name = $CI->model_classes->get_class_name($tbl_class_id);
		
		$arr_leaves[$i]["name"] = $first_name." ".$last_name;
		$arr_leaves[$i]["class"] = $class_name;
		$arr_leaves[$i]["message"] = $comments_parent;
		$arr_leaves[$i]["date"] = $added_date;
		
	}
	
	$arr_main["leaves"] = $arr_leaves;
	echo json_encode($arr_main);
?>	
		
		