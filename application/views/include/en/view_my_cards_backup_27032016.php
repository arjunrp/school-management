<?php 
$CI =& get_instance();
$CI->load->model("model_teachers");				
$CI->load->model("model_config");
	
	$arr_main = array();
	$arr_main["login_id"] = isset($login_id)? $login_id: '';
	$arr_main["role"] = isset($role)? $role :'';
	$arr_main["school_id"] = isset($school_id)?$school_id:'';
	$arr_main["total"] = $total_points;
	$arr_main["tbl_student_id"] = $tbl_student_id;

	$all_messages_arr = array();
//	print_r($all_messages_rs);
	
	if(count($all_messages_rs)>0){	
			for ($i=0; $i<count($all_messages_rs); $i++) {
				$id = $all_messages_rs[$i]['id']; 	
				$tbl_card_id = $all_messages_rs[$i]['tbl_card_id'];
				$tbl_teacher_id = $all_messages_rs[$i]['tbl_teacher_id'];
				$tbl_class_id = $all_messages_rs[$i]['tbl_class_id'];
				$tbl_student_id = $all_messages_rs[$i]['tbl_student_id'];
				$card_type = $all_messages_rs[$i]['card_type'];
				$card_issue_type = $all_messages_rs[$i]['card_issue_type'];
				$comments_student = $all_messages_rs[$i]['comments_student'];
				$added_date = $all_messages_rs[$i]['added_date'];
				$added_date = date("H:i - d/m/Y",strtotime($all_messages_rs[$i]["added_date"]));
	
				$teacher_obj = $CI->model_teachers->get_teachers_obj($tbl_teacher_id);
				
				$first_name = $teacher_obj[0]['first_name'];	
				$last_name = $teacher_obj[0]['last_name'];
				
				if (trim($lan) == "ar") {
					$first_name = $teacher_obj[0]['first_name_ar'];	
					$last_name = $teacher_obj[0]['last_name_ar'];
				}
		
				if (trim($comments_student) == "") {
					if (trim($lan) == "ar") {
						$comments_student = "لا توجد تعليقات";
					} else {
						$comments_student = "No Comments";
					}
				}
				$image = "";
				$cardCategoryPoints = $this->model_config->get_cards_categories($school_id,$card_type);
				$points 			= $cardCategoryPoints[0]['card_point'];
				$color 			    = isset($cardCategoryPoints[0]['color'])? $cardCategoryPoints[0]['color']: "N" ;
				if($cardCategoryPoints[0]['card_logo']<>"")
					$image 			    = IMG_GALLERY_PATH."/".$cardCategoryPoints[0]['card_logo'];
				else
					$image 			    =  "N";
				
				if ($points>0) {
					$points_sign = "+";	
				} else {
					$points_sign = "-";	
				}
				
				$points = str_replace("+","",$points);
				$points = str_replace("-","",$points);
				$message = $comments_student;						
	
				$message_arr = array();
				$message_arr['name'] = $first_name." ".$last_name;
				$message_arr['date'] = $added_date;
				$message_arr['message'] = $message;
				$message_arr['score'] = $points;
				$message_arr['sign'] = $points_sign;
				$message_arr['color'] = $color;
				$message_arr['image'] = $image;
				
				$all_messages_arr[$i] = $message_arr;
			}
			
			
			$arr_main["cards"] = $all_cards_arr;
			$arr_main["messages"] = $all_messages_arr;
	}else{
			$arr_main["cards"] = array();
			$arr_main["messages"] = array();
		
	}
	
		
		echo json_encode($arr_main);
		
		
		