<?php
$CI =& get_instance();
$CI->load->model("model_teachers");				

if (count($all_messages_rs) == 0) {
	echo "{\"code\":\"N\"}";
exit;	
}

	$messages_arr = array();
	$index = 0;

	for ($i=0; $i<count($all_messages_rs); $i++) {
		$id = $all_messages_rs[$i]['id']; 	
		$tbl_message_id = $all_messages_rs[$i]['tbl_message_id'];
		$message_from = $all_messages_rs[$i]['message_from'];
		$message_to = $all_messages_rs[$i]['message_to'];
		$message_type = $all_messages_rs[$i]['message_type'];
		$message = $all_messages_rs[$i]['message'];
		$added_date = $all_messages_rs[$i]['added_date'];
		
		$added_date = date("H:i - d/m/Y",strtotime($all_messages_rs[$i]["added_date"]));
		
		$CI->load->model("model_message");
		$data_image_rs =  $CI->model_message->get_message_image($tbl_message_id);	
		$file_name_updated = $data_image_rs[0]['file_name_updated'];
		$file_name_updated_thumb = $data_image_rs[0]['file_name_updated_thumb'];
		
		$data_audio_rs =  $CI->model_message->get_message_audio($tbl_message_id);	
		$file_name_updated_audio = $data_audio_rs[0]['file_name_updated'];
		
		$CI->load->model("model_teachers");
        $teacher_obj =  $CI->model_teachers->get_teachers_obj($message_from);	
        $first_name = $teacher_obj[0]['first_name'];
        $last_name = $teacher_obj[0]['last_name'];
		$file_name_updated_teacher = $teacher_obj[0]['file_name_updated'];
		
		$tbl_teacher_id = $teacher_obj[0]['tbl_teacher_id'];

        if (trim($lan) == "ar") {
            $first_name = $teacher_obj[0]['first_name_ar'];	
            $last_name = $teacher_obj[0]['last_name_ar'];
		}

       // if (trim($message) == "") {
        //    continue;	
        //}
		
		$arr_msg[$index]["name"] = $first_name." ".$last_name;
		$arr_msg[$index]["teacher_id"] = $tbl_teacher_id;
		$arr_msg[$index]["date"] = $added_date;
		$arr_msg[$index]["message"] = $message;
		$arr_msg[$index]["url"] = IMG_PATH."/teacher/".$file_name_updated_teacher;
		
		if (trim($file_name_updated) != "") {
			$arr_msg[$i]["message_img"] = HOST_URL."/admin/uploads/".$file_name_updated;
			$arr_msg[$i]["message_img_thumb"] = HOST_URL."/admin/uploads/".$file_name_updated_thumb;
		} else {
			$arr_msg[$i]["message_img"] = "";
			$arr_msg[$i]["message_img_thumb"] = "";
		}
		
		if (trim($file_name_updated_audio) != "") {
			$arr_msg[$i]["message_audio"] = HOST_URL."/admin/uploads/".$file_name_updated_audio;
		} else {
			$arr_msg[$i]["message_audio"] = "";
		}
		
		$index = $index + 1;
	}
	
	$arr["messages"] = $arr_msg;
	echo json_encode($arr);
?>

            