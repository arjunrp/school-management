<?php	$CI = & get_instance();
	$arr_mod[0]["id"]="0";
	$arr_mod[0]["title"]="My Child";
	$arr_mod[0]["url"]="http://192.168.2.13/aqdar/images/mychild.png";
	$arr_mod[1]["id"]="1";
	$arr_mod[1]["title"]="Parenting";
	$arr_mod[1]["url"]="http://192.168.2.13/aqdar/images/parenting.png";
	$arr_mod[2]["id"]="2";
	$arr_mod[2]["title"]="Event Calendar";
	$arr_mod[2]["url"]="http://192.168.2.13/aqdar/images/event_calendar.png";
	$arr_mod[3]["id"]="3";
	$arr_mod[3]["title"]="Survey";
	$arr_mod[3]["url"]="http://192.168.2.13/aqdar/images/survey.png";
	$arr_mod[4]["id"]="4";
	$arr_mod[4]["title"]="School Contacts";
	$arr_mod[4]["url"]="http://192.168.2.13/aqdar/images/contact.png";
	$arr_mod[5]["id"]="5";
	$arr_mod[5]["title"]="Help";
	$arr_mod[5]["url"]="http://192.168.2.13/aqdar/images/help_file.png";
	
	$CI->load->model("model_teachers");
	$index = 0;
	for ($i=0; $i<count($all_messages_rs); $i++) {
		if ($index>=10) {break;}
		$id = $all_messages_rs[$i]['id']; 	
		$tbl_message_id = $all_messages_rs[$i]['tbl_message_id'];
		$message_from = $all_messages_rs[$i]['message_from'];
		$message_to = $all_messages_rs[$i]['message_to'];
		$message_type = $all_messages_rs[$i]['message_type'];
		$message = $all_messages_rs[$i]['message'];
		$added_date = $all_messages_rs[$i]['added_date'];
		$added_date = date("H:i - d/m/Y",strtotime($all_messages_rs[$i]["added_date"]));
		
		$teacher_obj = $CI->model_teachers->get_teachers_obj($message_from);	
		$first_name = $teacher_obj[0]['first_name'];	
		$last_name = $teacher_obj[0]['last_name'];	
	
		if (trim($lan) == "ar") {
			$first_name = $teacher_obj[0]['first_name_ar'];	
			$last_name = $teacher_obj[0]['last_name_ar'];
		}
	
		if (trim($message) == "") {
			continue;	
		}
		
		$arr_msg[$index]["name"] = $first_name." ".$last_name;
		$arr_msg[$index]["date"] = $added_date;
		$arr_msg[$index]["message"] = $message;
		$index = $index+1;
	}
	
	$arr["login_id"] = $user_id;
	$arr["role"] = $role;
	$arr["school_id"] = $school_id;
	$arr["modules"] = $arr_mod;
	if (count($arr_msg)>0) {
		$arr["messages"] = $arr_msg;
	}
	echo json_encode($arr);
?>
