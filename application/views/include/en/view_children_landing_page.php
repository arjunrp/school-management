    <div class="spc_h20"></div>
    <div class="sub_module_box" onclick="get_messages_from_teacher_page()">
        <div class="sub_module_img"><img src="images/msg_icon.png" /></div>
        <div class="sub_module_txt">Message</div>     
	</div>
    <div class="spc_h5"></div>
    
     <div class="sub_module_box" onclick="get_leave_app_form_page()">
        <div class="sub_module_img"><img src="images/leave_icon.png" /></div>
        <div class="sub_module_txt">Leave Application</div>     
	</div>
    <div class="spc_h5"></div>
    
    <div class="sub_module_box" onclick="get_my_cards_page()">
        <div class="sub_module_img"><img src="images/cards_icon.png" /></div>
        <div class="sub_module_txt">Cards</div>     
	</div>
    <div class="spc_h5"></div>
	
    <div class="sub_module_box" onclick="get_point_details_page()">
        <div class="sub_module_img"><img src="images/points_icon.png" /></div>
        <div class="sub_module_txt">Points Earned</div>     
        <div class="points_box"><span id="span_total_points">0</span></div>
	</div>
    <div class="spc_h5"></div>
        
    <div class="sub_module_box" onclick="get_daily_report_page()">
        <div class="sub_module_img"><img src="images/daily_report_icon.png" /></div>
        <div class="sub_module_txt">Daily Report</div>     
	</div>
    <div class="spc_h5"></div>
        
    <div class="sub_module_box" onclick="load_gallery_images_student()">
        <div class="sub_module_img"><img src="images/instant_snaps_icon.png" /></div>
        <div class="sub_module_txt">Photos</div>     
	</div>
    <div class="spc_h5"></div>
        
    <div class="sub_module_box" onclick="get_children_data()">
        <div class="sub_module_img"><img src="images/child_reports_icon.png" /></div>
        <div class="sub_module_txt">Academic Information</div>     
	</div>
    
   
    <div class="spc_h5"></div>
    <div class="sub_module_box" onclick="load_student_tracker_page()">
        <div class="sub_module_img"><img src="images/bus_icon.png" /></div>
        <div class="sub_module_txt">Track Your Bus</div>     
	</div>
    
    <div class="spc_h5"></div>
    <div class="sub_module_box" onclick="get_child_bus_attendance()">
        <div class="sub_module_img"><img src="images/attendance.png" /></div>
        <div class="sub_module_txt">Bus Attendance</div>     
	</div>
    
	<?php	
        if ($lan == "en") {
            $choose_str = "Select Child ";
        } else {
            $choose_str = "Select Child Ar";
        }
    ?>
    
    <!--CHILDREN DDM DATA-->
    <?php /*?><div style="display:none" id="children_ddm">
        <option disabled="disabled" value="0" selected="selected">- <?=$choose_str?> -</option>
        <?php	for ($i=0; $i<count($rs_children); $i++) {
                    $tbl_student_id = $rs_children[$i]['tbl_student_id'];
                    $student_obj = get_student_obj($tbl_student_id);
                    $first_name = $student_obj[0]['first_name'];
                    $last_name = $student_obj[0]['last_name'];
                    if ($lan == "ar") {
                        $first_name = $student_obj[0]['first_name_ar'];
                        $last_name = $student_obj[0]['last_name_ar'];
                    }
        ?>
            <option id="<?=$tbl_student_id?>" value="<?=$tbl_student_id?>">&nbsp;&nbsp;<?=$first_name?> <?=$last_name?></option>			
        <?php
                }
        ?>
    </div><?php */?>

	<!--CHILDREN PICTURES-->
    <div style="display:none" id="children_ddm">
        <?php	for ($i=0; $i<count($rs_children); $i++) {
					$tbl_student_id = $rs_children[$i]['tbl_student_id'];
					$file_name_updated = get_student_picture($tbl_student_id);
					if ($file_name_updated == "") {continue;}
        ?>
            <div class="picture_box" id="<?=$tbl_student_id?>pbox"  style="float:right; margin-left:10px" data-filenameupdated="<?=$file_name_updated?>" onclick="get_student_details('<?=$tbl_student_id?>', '<?=$tbl_student_id?>pbox', '<?=$file_name_updated?>')"><img src="<?=IMG_PATH_STUDENT?>/<?=$file_name_updated?>" /></div>
        <?php
				}
        ?>
    </div>

<!--CHILDREN CLASS DATA-->
<?php	for ($i=0; $i<count($rs_children); $i++) {
			$tbl_student_id = $rs_children[$i]['tbl_student_id'];
			$student_obj = get_student_obj($tbl_student_id);
			$tbl_class_id = $student_obj[0]['tbl_class_id'];
			
			$class_name = get_class_name($tbl_class_id);
?>
	<input type="hidden" id="<?=$tbl_student_id?>class" value="<?=$class_name?>">			
<?php
		}
?>