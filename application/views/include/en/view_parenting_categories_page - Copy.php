<div class="spc_h20">&nbsp;</div>
<?php
	for ($i=0; $i<count($data_prnt); $i++)	 {
		$id = $data_prnt[$i]["id"];
		$tbl_parenting_id = $data_prnt[$i]["tbl_parenting_id"];
		$tbl_case_id = $data_prnt[$i]["tbl_case_id"];
		$parenting_logo = $data_prnt[$i]["parenting_logo"];
		$parenting_type = $data_prnt[$i]["parenting_type"];
		$parenting_title_en = $data_prnt[$i]["parenting_title_en"];
		$parenting_title_ar = $data_prnt[$i]["parenting_title_ar"];
		$parenting_text_en = $data_prnt[$i]["parenting_text_en"];
		$parenting_text_ar = $data_prnt[$i]["parenting_text_ar"];
		$added_date = $data_prnt[$i]["added_date"];				
		$is_active = $data_prnt[$i]["is_active"];
		
		$parenting_type = strtoupper($parenting_type);
		if ($lan == "en") {
			$parenting_title = $parenting_title_en;
			$parenting_text = $parenting_text_en;
			$cls = "";
		} else {
			$parenting_title = $parenting_title_ar;
			$parenting_text = $parenting_text_en;
			$cls = "prnt_box_ar";
		}
		
		if (strtoupper($parenting_type) == "V") {
			$img = "camera.jpg";
		} else {
			$img = "notebook.jpg";
		}
		
		$file_name_updated = get_parenting_video($tbl_parenting_id);
?>
    <div class="prnt_box <?=$cls?>" onclick="load_parenting_sub_cat_page('<?=$tbl_parenting_id?>')">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="15%" align="center" valign="middle" style="border-left:1px solid #CCCCCC"><?=$i+1?></td>
            <td align="left" valign="middle" style="padding:5px" class="parent_title"><?=$parenting_title?></td>
          </tr>
        </table>
      </div>
    <div class="spc_h6">&nbsp;</div>
<?php	}	?>