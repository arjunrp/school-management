<?php

	$arr_data = array();

	if (count($data_rs)>0) {

		

		for($k=0;$k<count($data_rs);$k++){

			$title = html_entity_decode($data_rs[$k]['title']);

			$title_ar = html_entity_decode($data_rs[$k]['title_ar']);

			$description = html_entity_decode($data_rs[$k]['description']);

			$description_ar = html_entity_decode($data_rs[$k]['description_ar']);

			$start_date = $data_rs[$k]['start_date'];

			$end_date = $data_rs[$k]['end_date'];

			$start_time = $data_rs[$k]['start_time'];

			$added_date = $data_rs[$k]['added_date'];

			$is_active = $data_rs[$k]['is_active'];

            if($device=="Android")
			{
				$start_date = date("d-m-Y",strtotime($start_date));
				$end_date = date("d-m-Y",strtotime($end_date));
			}else{
				$start_date = date("d M Y ",strtotime($start_date));
			    $end_date = date("d M Y ",strtotime($end_date));
			}

			$arr_data[$k]['title'] = $title;

			$arr_data[$k]['title_ar'] = $title_ar;

			$arr_data[$k]['start_date'] = $start_date;

			$arr_data[$k]['end_date'] = $end_date;

			$arr_data[$k]['time'] = $start_time;

			$arr_data[$k]['desc'] = $description;

			$arr_data[$k]['desc_ar'] = $description_ar;

		} 

		echo json_encode($arr_data);

	} else {

		echo json_encode($arr_data);

	}

?>

