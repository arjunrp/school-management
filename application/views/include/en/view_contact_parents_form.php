<div class="spc_h20"></div>
<div class="panel_star">
	<div class="spc_panel_inner"></div>
	<div class="leave_panel">
        <div id="student_ddm" style="padding-right:0px; width:100%;">
            <div class="ddm_org" style="left:86%"></div>
            <select id="tbl_class_id_pa" name="tbl_class_id_pa" class="select_box" onchange="get_all_students_against_class('pa')"> 
	            <option id="0" value="0">-- Select Class --</option>
				<?php                
                    for ($i=0; $i<count($all_classes_against_teacher_rs); $i++) {
                        $tbl_class_id = $all_classes_against_teacher_rs[$i]['tbl_class_id'];
                        $class_name =get_class_name($tbl_class_id);
                        if (trim($lan) == "ar") {
                            $class_name = get_class_name_ar($tbl_class_id);
                        }
                ?>
 		               <option id="<?=$tbl_class_id?>" value="<?=$tbl_class_id?>"><?=$class_name?></option>
                <?php
                    }
                ?>
            </select>
        </div>
        <div class="spc_controls">&nbsp;</div>
        <div id="student_ddm" style="padding-right:0px; width:100%;">
            <div class="ddm_org" style="left:86%"></div>
            <select id="tbl_student_id_pa" name="tbl_student_id_pa" class="select_box" onchange="contact_parents_sub_form('pa')"> 
	            <option id="0" value="0">-- Select Student --</option>
            </select>
        </div>
        
        <div class="spc_controls">&nbsp;</div>
        <div class="head_sml">Message</div>
        <div class="spc_controls">&nbsp;</div>
		<textarea name="application_message_pa" id="application_message_pa" class="application_message"></textarea>
        <div class="spc_h10">&nbsp;</div>
        <div id="cp_sub_form"></div>
		
        <?php	if (DDM_FOR_TESTING == 1) {	?>
        <!-- COMMENT BEFORE RELEASE START -->
            <div style="width:40%; margin:auto">
                <div class="btn_green" onclick="get_all_students_against_class('pa')">DDM 1 Click</div>
            </div>
            <div class="spc_h10">&nbsp;</div>
            <div style="width:40%; margin:auto">
                <div class="btn_green" onclick="contact_parents_sub_form('pa')">DDM 2 Click</div>
            </div>
		<!-- COMMENT BEFORE RELEASE END -->   
        <?php	}	?>                 
    </div>
