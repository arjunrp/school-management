<?php
	//print_r($home_page_images_rs);
	$img1 = $home_page_images_rs[0]['image_name'];
	$img2 = $home_page_images_rs[1]['image_name'];
	$img3 = $home_page_images_rs[2]['image_name'];
	$img4 = $home_page_images_rs[3]['image_name'];
	
	if (trim($img1) == "") { $img1 = "no_image.jpg";}
	if (trim($img2) == "") { $img2 = "no_image.jpg";}
	if (trim($img3) == "") { $img3 = "no_image.jpg";}
	if (trim($img4) == "") { $img4 = "no_image.jpg";}
	
	$img1 = IMG_PATH."/gallery/".$img1;
	$img2 = IMG_PATH."/gallery/".$img2;
	$img3 = IMG_PATH."/gallery/".$img3;
	$img4 = IMG_PATH."/gallery/".$img4;
?>
	<div class="spc_h15"></div>
	<div class="gallery_box">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="25%" align="center" valign="middle"><img class="imgg" src="images/img4.jpg" style="display:block" /></td>
            <td width="25%" align="center" valign="middle"><img class="imgg" src="images/img3.jpg" style="display:block" /></td>
            <td width="25%" align="center" valign="middle"><img class="imgg" src="images/img2.jpg" style="display:block" /></td>
            <td width="25%" align="center" valign="middle"><img class="imgg" src="images/img1.jpg" style="display:block" /></td>
          </tr>
        </table>
	</div>
    <div class="spc_h15"></div>
    <div class="content_reduced panel_head_home">
        <div class="panel1_padd">
        	<div class="panel_heading"><?=$home_page_message_rs[0]["title"]?></div>
        	<div class="spc_h15"></div>
            <div style="text-align:left" class="panel_txt"><?=$home_page_message_rs[0]["message"]?></div>
        </div>                    
    </div>
