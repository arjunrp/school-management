<?php
	for ($i=0; $i<count($data_teachers); $i++) {
		$tbl_teacher_id = $data_teachers[$i]['tbl_teacher_id'];
		$file_name_updated = $data_teachers[$i]['file_name_updated'];
		
		if (trim($lan) == "ar") {
			$teacher_name = $data_teachers[$i]['first_name_ar']." ".$data_teachers[$i]['last_name_ar'];
		} else {
			$teacher_name = $data_teachers[$i]['first_name']." ".$data_teachers[$i]['last_name'];
		}
		$arr_teachers[$i]["teacher_id"] = $tbl_teacher_id;
		$arr_teachers[$i]["title"] = $teacher_name;
		$arr_teachers[$i]["url"] = HOST_URL."/images/teacher/".$file_name_updated;
	}
	$arr["teachers"] = $arr_teachers;
	echo json_encode($arr);
?>
