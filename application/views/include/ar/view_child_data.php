<div class="spc_h20">&nbsp;</div>
<?php

	for ($i=0; $i<count($data_prnt); $i++)	 {
		$id = $data_prnt[$i]["id"];
		$tbl_child_data_id = $data_prnt[$i]["tbl_child_data_id"];
		$child_data_logo = $data_prnt[$i]["child_data_logo"];
		$child_data_type = $data_prnt[$i]["child_data_type"];
		$child_data_title_en = $data_prnt[$i]["child_data_title_en"];
		$child_data_title_ar = $data_prnt[$i]["child_data_title_ar"];
		$child_data_text_en = $data_prnt[$i]["child_data_text_en"];
		$child_data_text_ar = $data_prnt[$i]["child_data_text_ar"];
		$added_date = $data_prnt[$i]["added_date"];				
		$is_active = $data_prnt[$i]["is_active"];
		
		$child_data_type = strtoupper($child_data_type);
		if ($lan == "en") {
			$child_data_title = $child_data_title_en;
			$child_data_text = $child_data_text_en;
		} else {
			$child_data_title = $child_data_title_ar;
			$child_data_text = $child_data_text_en;
		}
		
		if (strtoupper($child_data_type) == "V") {
			$img = "camera.jpg";
		} else {
			$img = "notebook.jpg";
		}
		$added_date = date("d M Y",strtotime($data_prnt[$i]["added_date"]));
		
		$file_name_updated = get_child_data_video($tbl_child_data_id);
		
		// For demo
		//$file_name_updated = "a.mp4";
?>
    <div class="prnt_box" <? if ($file_name_updated != "" && strtoupper($child_data_type) == "V"){ ?>  onclick="play_video('<?=$file_name_updated?>')" <?php }else if ($child_data_text != "" && $child_data_type == "D"){ ?>  onclick="get_child_data_text('<?=$tbl_child_data_id?>')" <?php } ?>>
    <?php	if ($data_prnt[$i]["child_data_logo"] != "") {	?>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="15%" align="center" valign="middle" style="border-right:1px solid #CCCCCC"><?=$i+1?></td>
            <td width="15%" align="center" valign="middle"><?php if ($child_data_logo != "") { ?><img src="<?=IMG_LOGO_IMG_PATH?>/<?=$data_prnt[$i]["child_data_logo"]?>" /><?php } ?></td>
            <td align="left" valign="middle" style="padding:5px"><div  class="child_data_txt"><?=$child_data_title?></div> <div class="rpt_date"><?=$added_date?></div></td>
            <td width="18%" align="right" valign="middle" class="pimg">
                <img class="ptype" src="images/<?=$img?>" />
            </td>
          </tr>
        </table>
    <?php	} else {	?>
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="15%" align="center" valign="middle" style="border-right:1px solid #CCCCCC"><?=$i+1?></td>
            <td align="left" valign="middle" style="padding:5px"><div  class="child_data_txt"><?=$child_data_title?></div> <div class="rpt_date"><?=$added_date?></div></td>
            <td width="18%" align="right" valign="middle" class="pimg">
                <img class="ptype" src="images/<?=$img?>" />
            </td>
          </tr>
        </table>
    <?php	}	?>
    </div>
<div class="spc_h6">&nbsp;</div>
<?php	}	?>