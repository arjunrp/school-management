<?php	$CI = & get_instance();
		//$j=0;
    for ($i=0; $i<count($data_rs); $i++) {
		
		$id = $data_rs[$i]['id']; 	
		$tbl_message_id = $data_rs[$i]['tbl_message_id'];
		$message_from = $data_rs[$i]['message_from'];
		$message_to = $data_rs[$i]['message_to'];
		$message_type = $data_rs[$i]['message_type'];
		$message = $data_rs[$i]['message'];
		$added_date = $data_rs[$i]['added_date'];
		
		$added_date = date("H:i - d/m/Y",strtotime($data_rs[$i]["added_date"]));
		$item_id 				= $data_rs[$i]['item_id'];
		
			 if($item_id<>""){
				 $CI->load->model("model_message");
				 
				 $dataRecords = $CI->model_message->get_upload_files($item_id);
				
				 //print_r($dataRecords[0]['file_name_original']); exit;
				 if(trim($dataRecords[0]['file_name_original']) == "audio.mp4")
				 {
					 $audio 								= $dataRecords[0]['file_name_updated'];
					 $arr_msg[$i]['message_audio'] 			= HOST_URL."/admin/uploads/".$audio; 
					 $arr_msg[$i]['message_img'] 			= "";
					 $arr_msg[$i]['message_img_thumb'] 		= "";
				 }else{

					 $image 		= $dataRecords[0]['file_name_updated'];
					 $image_thumb 	= $dataRecords[0]['file_name_updated_thumb'];
					 $arr_msg[$i]['message_img'] = HOST_URL."/admin/uploads/".$image; 
					 $arr_msg[$i]['message_img_thumb'] 	= HOST_URL."/admin/uploads/".$image_thumb;
					 $arr_msg[$i]['message_audio'] 		= "";
				 }
			  //print_r($arr_msg); exit;
			 }else{
				  $arr_msg[$i]['message_img'] 		= ""; 
				  $arr_msg[$i]['message_img_thumb'] 	= "";
				  $arr_msg[$i]['message_audio'] 		= "";
			 }
		
             
			// Message from Ministry
			if ($tbl_teacher_id_from == "ministry") {
				$first_name = "Ministry";
			} else {
				// Message could be from another teacher (tbl_teacher_id) or from roles like Supervisor, Headmistress etc (tbl_admin_users)
				$CI->load->model("model_teachers");
				$teacher_obj = $CI->model_teachers->get_teachers_obj($message_from);	
				$file_name_updated = $teacher_obj[0]['file_name_updated'];
				
				if (trim($lan) == "ar") {
					$first_name = $teacher_obj[0]['first_name_ar'];	
					$last_name = $teacher_obj[0]['last_name_ar'];
				}
				
				// Message from admin
				if (trim($first_name) == "") {
					$CI->load->model("model_admin");
					$admin_obj = $CI->model_admin->get_admin_user_obj($message_from);	
					$first_name = $admin_obj[0]['first_name'];
					$last_name = $admin_obj[0]['last_name'];
				}
			}
		
			if (trim($message) == "") {
				continue;	
			}
			$arr_msg[$i]["name"] = $first_name." ".$last_name;
			$arr_msg[$i]["date"] = $added_date;
			$arr_msg[$i]["message"] = $message;
			if ($file_name_updated != "") {
				$arr_msg[$i]["url"] = HOST_URL."/images/teacher/".$file_name_updated;
			}
			//$j= $j+1;
	}
	//print_r($arr_msg); exit;
	$arr["messages"] = $arr_msg;
	echo json_encode($arr);
?>
    
