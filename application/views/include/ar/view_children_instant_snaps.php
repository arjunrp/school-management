<?php	
	$CI = & get_instance();
	
?>
	<div class="spc_h15"></div>
    <?php
		$msg = "<div>No snaps are available.</div>";
		$msg_ar = "<div>.لا توجد تفاصيل متاحة للكروت </div>";
		
		if (count($data_isnps) == 0) {
			if (trim($lan) == "ar") {
				echo "<div style='float:right'>". $msg_ar."</div>";
			} else {
				echo $msg;
			}
		exit;	
		}
	?>
    <!--<div class="content_reduced panel_head">
        <div class="panel1_padd"><div class="panel_heading">How To Use App</div></div>                    
    </div>-->
    <div class="desc_container">
    	<div>
            <div class="panel1_body_padd">
				<?php
                	for ($i=0; $i<count($data_isnps); $i++) {
						$id = $data_isnps[$i]["id"];
						$tbl_instant_pics_id = $data_isnps[$i]["tbl_instant_pics_id"];
						$tbl_teacher_id = $data_isnps[$i]["tbl_teacher_id"];
						$tbl_student_id = $data_isnps[$i]["tbl_student_id"];
						$tbl_uploads_id = $data_isnps[$i]["tbl_uploads_id"];
						$device = $data_isnps[$i]["device"];
						$message = $data_isnps[$i]["message"];
						$lan = $data_isnps[$i]["lan"];
						$is_watched = $data_isnps[$i]["is_watched"];
						$added_date = $data_isnps[$i]["added_date"];
						
						$added_date = date("H:i - d/m/Y",strtotime($added_date));
						$file_name_updated = get_snap($tbl_uploads_id);
						
						$teacher_obj = get_teachers_obj($tbl_teacher_id);
						
						if ($lan == "ar") {
							$name = $teacher_obj[0]["first_name_ar"]." ".$teacher_obj[0]["last_name_ar"];;
						} else  {
							$name = $teacher_obj[0]["first_name"]." ".$teacher_obj[0]["last_name"];;
						}
					
				?>
                	<div class="ipx_box">
                        <div style="height:100%">
                            <img src="<?=IMG_LOGO_IMG_PATH?>/<?=$file_name_updated?>" style="width: 100%; max-height: 100%" />
                        </div>
                        <div class="ipx_date" style=""><?=$added_date?></div>
                        <div class="ipx_hd"><?=$name?></div>
                        <div class="ipx_txt"><?=$message?></div>
                    </div>
				<?php	
					}
				?>
            </div>
        </div>
    </div>
    <div class="content_reduced panel_footer">&nbsp;</div>


    
