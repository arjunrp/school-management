<?php
    for ($i=0; $i<count($data_rs); $i++) {
        $tbl_student_id = $data_rs[$i]['tbl_student_id'];
		$attendance_date = $data_rs[$i]['attendance_date'];
		$tbl_bus_id = $data_rs[$i]['tbl_bus_id'];
		$is_present = $data_rs[$i]['is_present'];
		$att_option = $data_rs[$i]['att_option'];
		
		if ($att_option == "M") { $title = "( Morning )"; } else {$title = "( Afternoon )";}
		
		$attendance_date_prev = $attendance_date;
		$arr_att[$i]["date"] = $attendance_date;
		$arr_att[$i]["title"] = $title;
		$arr_att[$i]["val"] = $is_present;
	}

	$arr["bus_attendance"] = $arr_att;
	echo json_encode($arr);
?>
