<?php
$CI =& get_instance();
$CI->load->model("model_teachers");				

	$total_points = 0;
	
	$all_messages_arr = array();
	
	for ($i=0; $i<count($all_messages_rs); $i++) {
		$id = $all_messages_rs[$i]['id']; 	
		$tbl_card_id = $all_messages_rs[$i]['tbl_card_id'];
		$tbl_teacher_id = $all_messages_rs[$i]['tbl_teacher_id'];
		$tbl_class_id = $all_messages_rs[$i]['tbl_class_id'];
		$tbl_student_id = $all_messages_rs[$i]['tbl_student_id'];
		$card_type = $all_messages_rs[$i]['card_type'];
		$card_issue_type = $all_messages_rs[$i]['card_issue_type'];
		$comments_student = $all_messages_rs[$i]['comments_student'];
		$added_date = $all_messages_rs[$i]['added_date'];

		$message = $comments_student;
		$added_date = date("H:i - d/m/Y",strtotime($all_messages_rs[$i]["added_date"]));
		
		$teacher_obj = $CI->model_teachers->get_teachers_obj($tbl_teacher_id);	
		$first_name = $teacher_obj[0]['first_name_ar'];	
		$last_name = $teacher_obj[0]['last_name_ar'];
		
		$points = 0;
		
		/*if ($card_type == "blue") {
			$points = $config_rs[0]["blue_points"];
			$styl = "float:right;";
			$styl_clr = "#FD8EF4";
		} else if ($card_type == "purple") {
			$points = $config_rs[0]["purple_points"];
			$styl = "float:right;";
			$styl_clr = "#983AD0";
		} else if ($card_type == "grey") {
			$points = $config_rs[0]["grey_points"];
			$styl = "float:right;";
			$styl_clr = "#FD9D2D";
		} else if ($card_type == "green") {
			$points = $config_rs[0]["green_points"];
			$styl = "float:right;";
			$styl_clr = "#00A652";
		} else if ($card_type == "yellow") {
			$points = $config_rs[0]["yellow_points"];
			$styl = "float:left;";
			$styl_clr = "#FCE62D";
		} else if ($card_type == "orange") {
			$points = $config_rs[0]["orange_points"];
			$styl = "float:left;";
			$styl_clr = "#FF5D5E";
		} else if ($card_type == "pink") {
			$points = $config_rs[0]["pink_points"];
			$styl = "float:left;";
			$styl_clr = "#000000";
		} else if ($card_type == "red") {
			$points = $config_rs[0]["red_points"];
			$styl = "float:left;";
			$styl_clr = "#764C24";
		} else if ($card_type == "harass") {
			$points = $config_rs[0]["harass_points"];
			$styl = "float:left;";
			$styl_clr = "#ff5e5e";
		} else if ($card_type == "weapon") {
			$points = $config_rs[0]["weapon_points"];
			$styl = "float:left;";
			$styl_clr = "#ff5e5e";
		} else if ($card_type == "drugs") {
			$points = $config_rs[0]["drugs_points"];
			$styl = "float:left;";
			$styl_clr = "#ff5e5e";
		} else if ($card_type == "violence" || $card_type == "viol") {
			$points = $config_rs[0]["viol_points"];
			$styl = "float:left;";
			$styl_clr = "#ff5e5e";
		}

		$total_points += $points;		
			
		if ($points>0) {
			$points_sign = "+";	
		} else {
			$points_sign = "-";	
		}
		
		$points = str_replace("+","",$points);
		$points = str_replace("-","",$points);
		*/
		
		
		
		
		 $cardCategoryPoints = $this->model_config->get_cards_categories($school_id,$card_type);
            $points 			= $cardCategoryPoints[0]['card_point'];
			$color 			    = isset($cardCategoryPoints[0]['color'])? $cardCategoryPoints[0]['color']: "N" ;
			if($cardCategoryPoints[0]['card_logo']<>"")
				$image 			    = IMG_GALLERY_PATH."/".$cardCategoryPoints[0]['card_logo'];
			else
				$image 			    =  "N";
			
			
			$color 			    = isset($cardCategoryPoints[0]['color'])? $cardCategoryPoints[0]['color']: "N" ;
			if($cardCategoryPoints[0]['card_logo']<>"")
				$image 			    = IMG_GALLERY_PATH."/".$cardCategoryPoints[0]['card_logo'];
			else
				$image 			    =  "N";
			
			
				
				
			$total_points += $points;		
			
			if ($points>0) {
				$points_sign = "+";	
			} else {
				$points_sign = "-";	
			}
			
			$points = str_replace("+","",$points);
			$points = str_replace("-","",$points);

		$message_arr = array();
		$message_arr['name'] = $first_name." ".$last_name;
		$message_arr['date'] = $added_date;
		$message_arr['message'] = $message;
		$message_arr['score'] = $points;
		$message_arr['sign'] = $points_sign;
		$message_arr['color'] = $color;
		$message_arr['image'] = $image ;
		
		$all_messages_arr[$i] = $message_arr;
	}
	
	$arr_main = array();
	$arr_main["login_id"] = $login_id;
	$arr_main["role"] = $role;
	$arr_main["school_id"] = $school_id;
	$arr_main["total"] = (string)$total_points;
	$arr_main["tbl_student_id"] = $tbl_student_id;
	$arr_main["card_id"] 	= $card_type;
	$arr_main["cards"] 		= $all_cards_arr;
	$arr_main["messages"] 	= $all_messages_arr;

	echo json_encode($arr_main);
?>
