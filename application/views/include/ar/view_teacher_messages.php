<?php
	$CI =& get_instance();
	$CI->load->model("model_message");
	$CI->load->model("model_teacher_group");
	
	$arr = array();
	
	$index = 0;
	for ($i=0; $i<count($rs_groups); $i++) {
		$tbl_teacher_group_id = $rs_groups[$i]["tbl_teacher_group_id"];
		
		if (trim($tbl_teacher_group_id) == "") {
			$total_groups -=1;
			continue;	
		}

		$rs_group = $CI->model_teacher_group->get_group_details($tbl_teacher_group_id);		
		
		$group_name_en = $rs_group[0]['group_name_en'];
		$group_name_ar = $rs_group[0]['group_name_ar'];
		
		$total_messages = $CI->model_message->get_total_unread_messages_against_group($tbl_teacher_group_id, $tbl_teacher_id);
		
		if (trim($total_messages) == "") {
			$total_messages = 0;
		}

		$group_name = "";
		if ($lan == "ar") {
			$group_name = $group_name_ar;
		} else {
			$group_name = $group_name_en;
		}
		
		$arr[$index]['message_from'] = $group_name;
		$arr[$index]['src_role'] = $group_name;
		$arr[$index]['unread_cnt'] = $total_messages;
		$arr[$index]['tbl_teacher_id'] = $tbl_teacher_id;
		$arr[$index]['tbl_teacher_group_id'] = $tbl_teacher_group_id;
		
		$index += 1;
		
	}


	//$total_groups -= 1;
	//Fetching details for non group messages
	
	$total_messages = $CI->model_message->get_total_unread_messages_against_group("", $tbl_teacher_id);
	$arr[$index]['message_from'] = "teachers";
	$arr[$index]['src_role'] = "Teachers";
	$arr[$index]['unread_cnt'] = $total_messages;
	$arr[$index]['tbl_teacher_id'] = $tbl_teacher_id;
	$arr[$index]['tbl_teacher_group_id'] = "";

	//print_r($arr);

	$arr_roles["roles"] = $arr;
	echo json_encode($arr_roles);
?>


<?php
	/*	
	$arr = array();
	
	$arr[0]['message_from'] = "teachers";
	$arr[0]['src_role'] = "Teachers";
	$arr[0]['unread_cnt'] = 5;


	
	$arr[1]['message_from'] = "Math Teachers";
	$arr[1]['src_role'] = "Math Teachers";
	$arr[1]['unread_cnt'] = 20;
	
	
	
	$arr_roles["roles"] = $arr;


	echo json_encode($arr_roles);
	
	
		echo '
				{
					"roles": [
				
						{
							"message_from": "teachers",
							"src_role": "Teachers",
							"unread_cnt": "5"
						}, {
							"message_from": "teachers",
							"src_role": "Teachers",
							"unread_cnt": "5"
						}
				
					]
				}
	
		
		';
	*/	
?>