<?php 
	$CI = & get_instance();
	$CI->load->model("model_classes");
	$option_str = '<option value="0">-- Select Class --</option>';
	
	for ($i=0; $i<count($all_classes_against_teacher_rs); $i++) {
		$tbl_class_id = $all_classes_against_teacher_rs[$i]['tbl_class_id'];
		$class_name = $CI->model_classes->get_class_name($tbl_class_id);
		if (trim($lan) == "ar") {
			$class_name = $CI->model_classes->get_class_name_ar($tbl_class_id);
		}
		$option_str .= '<option value="'.$tbl_class_id.'">'.$class_name.'</option>';
	}
	echo $option_str;
?>