	<?php date_default_timezone_set('Asia/Dubai'); ?>
    <div class="spc_h15"></div>
    <div style="float:left; ">
    <strong>Date:</strong> <?=$attendance_date?>
    </div>
    <div class="spc_h10"></div>
	<?php
    $msg = "<h2>No messages available.</h2>";
    $msg_ar = "<h2>.لا توجد رسائل متاحة</h2>";
    
    if (count($data_rs) == 0) {
        if (trim($lan) == "ar") {
            echo "<div style='float:right'>". $msg_ar."</div>";
        } else {
            echo $msg;
        }
    exit;	
    }
	?>
	<div class="cdrpt_row">
        <div class="cdrpt_name opt_head_fx">
	        Student
        </div>
        <div style="clear:both"></div>
     </div>
    <?
    for ($i=0; $i<count($data_rs); $i++) {
        $tbl_student_id = $data_rs[$i]['tbl_student_id'];
		$first_name = $data_rs[$i]['first_name'];
		$last_name = $data_rs[$i]['last_name'];

		if (trim($lan) == "ar") {
			$first_name = $data_rs[$i]['first_name_ar'];
			$last_name = $data_rs[$i]['last_name_ar'];
		}
		
		$name = $first_name. " ".$last_name;
		$file_name_updated = $data_rs[$i]['file_name_updated'];
		$cls = "";
		$cls_opt = "cdrpt_options_att";
		if ($file_name_updated != "") {
			$cls = "class='stu_att'";
			$cls_opt = "cdrpt_options_att_mid";
		}
		
		$is_present = get_attendance($tbl_student_id, $tbl_bus_id, $attendance_date,  $att_option);
		$dataopt = $is_present;
    ?>

<div class="cdrpt_row">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="15%" align="left" valign="top"><?php	if ($file_name_updated != "") {	?><div style="float:left"><img class="student_img" src="<?=IMG_PATH_STUDENT?>/<?=$file_name_updated?>" /></div><?php }	?>&nbsp;</td>
        <td align="left" valign="middle" class="stu_name" style="color:#5e5e5e"><?=$name?>&nbsp;</td>
        <td width="18%" align="left" valign="middle"><div class="cdrpt_opt_<?=$dataopt?>" data-id="<?=$tbl_student_id?>" data-opt="<?=$dataopt?>" id="<?=$i?>optprsnt" onclick="cdrtp_option_click('<?=$i?>optprsnt')"></div>&nbsp;</td>
        <td width="12%" align="center" valign="middle"><div class="opt_col cdrpt_opt_notify" onclick="send_absent_notification('<?=$tbl_student_id?>', 'absent_bus')"></div>&nbsp;</td>
      </tr>
    </table>
</div>
	<?php			
		}
	?>
<div class="spc_20">&nbsp;</div>
	<div style="width:50%; margin:0 auto;" onclick="submit_attendance('<?=$tbl_bus_id?>', '<?=$attendance_date?>', '<?=$att_option?>', '<?=count($data_rs)?>', '<?=$tbl_school_id?>')" class="btn_green btn_green_pthm">SAVE ATTENDANCE</div>