	<div class="spc_h15"></div>
	<?php
    $msg = "<h2>No messages available.</h2>";
    $msg_ar = "<h2>لا توجد رسائل متاحة</h2>";
    
    if (count($data_rs) == 0) {
        if (trim($lan) == "ar") {
            echo "<div style='float:right'>". $msg_ar."</div>";
        } else {
            echo $msg;
        }
    exit;	
    }

    for ($i=0; $i<count($data_rs); $i++) {
        $id = $data_rs[$i]['id']; 	
        $tbl_message_id = $data_rs[$i]['tbl_message_id'];
        $message_from = $data_rs[$i]['message_from'];
        $message_to = $data_rs[$i]['message_to'];
        $message_type = $data_rs[$i]['message_type'];
        $message = $data_rs[$i]['message'];
        $added_date = $data_rs[$i]['added_date'];
        $added_date = date("H:i - d/m/Y",strtotime($data_rs[$i]["added_date"]));
        
        
        $teacher_obj = get_teachers_obj($message_from);	
        $first_name = $teacher_obj[0]['first_name'];	
        $last_name = $teacher_obj[0]['last_name'];	

        if (trim($lan) == "ar") {
            $first_name = $teacher_obj[0]['first_name_ar'];	
            $last_name = $teacher_obj[0]['last_name_ar'];
		}

        if (trim($message) == "") {
            continue;	
        }
        
        $styl = "width:100%;";
        $styl_clr = "#5599E2";
	if (trim($lan) == "ar") {
	?>
        <div class="mbx mbx_ar">
            <div class="mbx_name mbx_name_ar"><?=$first_name?> <?=$last_name?></div>
            <div class="mbx_date mbx_date_ar"><?=$added_date?></div>
            <div class="mbx_txt mbx_txt_ar"><?=$message?></div>
        </div>
        <div class="spc_msg">&nbsp;</div>
	<?php } else { ?>
		<div class="mbx">
			<div class="mbx_name"><?=$first_name?> <?=$last_name?></div>
			<div class="mbx_date"><?=$added_date?></div>
			<div class="mbx_txt"><?=$message?></div>
		</div>
		<div class="spc_msg">&nbsp;</div>
	<?php } ?>
	<?php			
		}
	?>
    <input type="hidden" name="student_name" id="student_name" value="<?=$name?>" />
    
