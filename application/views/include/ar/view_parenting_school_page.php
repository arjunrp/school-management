<?php	
	$CI = & get_instance();
	$CI->load->model('model_parenting_school');
	for ($i=0; $i<count($data_prnt); $i++)	 {
			$id = $data_prnt[$i]["id"];
			$tbl_parenting_school_id = $data_prnt[$i]["tbl_parenting_school_id"];
			$tbl_case_id = $data_prnt[$i]["tbl_case_id"];
			$parenting_logo = $data_prnt[$i]["parenting_logo"];
			$parenting_type = $data_prnt[$i]["parenting_type"];
			$parenting_title_en = $data_prnt[$i]["parenting_title_en"];
			$parenting_title_ar = $data_prnt[$i]["parenting_title_ar"];
			$parenting_text_en = $data_prnt[$i]["parenting_text_en"];
			$parenting_text_ar = $data_prnt[$i]["parenting_text_ar"];
			$url = $data_prnt[$i]["parenting_url"];
			$added_date = $data_prnt[$i]["added_date"];				
			$is_active = $data_prnt[$i]["is_active"];
			
			$parenting_type = strtoupper($parenting_type);
			
			if ($lan == "en") {
				$parenting_title = $parenting_title_en;
				$parenting_text = $parenting_text_en;
				$cls = "";
			} else {
				$parenting_title = $parenting_title_ar;
				$parenting_text = $parenting_text_ar;
				$cls = "prnt_box_ar";
			}
			
			  if (strtoupper($parenting_type) == "V") {
				$img = "camera.jpg";
				$file_name_updated = $CI->model_parenting_school->get_parenting_school_video($tbl_parenting_school_id);
			} else {
				$img = "notebook.jpg";
			}
			
			$arr[$i]["tbl_parenting_school_id"] = $tbl_parenting_school_id;
			$arr[$i]["title"] = $parenting_title;
			$arr[$i]["type"] = $parenting_type;
			$arr[$i]["data"] = "*";
			
			if (strtoupper($parenting_type) == "V") {
				if (trim($file_name_updated) != ""){
					$arr[$i]["data"] =  HOST_URL."/admin/uploads/".$file_name_updated;
				}
			} else if (strtoupper($parenting_type) == "U") {
					$arr[$i]["data"] =  $url;
			} else {
				if (trim($parenting_text) != ""){
					//$arr[$i]["data"] =  HOST_URL."/admin/uploads/".$parenting_text;
					$arr[$i]["data"] =  $parenting_text;
				}
			}
			
			
?>
<?php	}	
		
		if (count($data_prnt)>0) {
			$arr_data["parenting"] = $arr;
			echo json_encode($arr_data,JSON_UNESCAPED_UNICODE);
		} else {
			$arr["code"] = "N";
			echo json_encode($arr,JSON_UNESCAPED_UNICODE);
		}
?>