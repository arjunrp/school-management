<?php
$CI =& get_instance();
$CI->load->model("model_student");				
$CI->load->model("model_bus");				
?>
<?
	$all_students_arr = array();
	
    for ($i=0; $i<count($data_rs); $i++) {
        $tbl_student_id = $data_rs[$i]['tbl_student_id'];
		$tbl_bus_id = $data_rs[$i]['tbl_bus_id'];
		$is_present = $data_rs[$i]['is_present'];
		
		$stu_obj = $CI->model_student->get_student_obj($tbl_student_id);
		$image_student = $stu_obj[0]['file_name_updated'];
		
		$first_name = $stu_obj[0]['first_name'];
		$last_name = $data_rs[0]['last_name'];

		if (trim($lan) == "ar") {
			$first_name = $stu_obj[0]['first_name_ar'];
			$last_name = $stu_obj[0]['last_name_ar'];
		}

		$name = $first_name. " ".$last_name;
				
		$bus_number = $CI->model_bus->get_bus_number($tbl_bus_id);
		
		$student_arr = array();
		
		$student_arr['student_id'] = $tbl_student_id;
		$student_arr['title'] = $name;
		$student_arr['val'] = $is_present;
		$student_arr['url'] = IMG_PATH_STUDENT."/".$image_student;
		
		$all_students_arr[$i] = $student_arr;
	}

	$arr_main = array();
	$arr_main["date"] = $attendance_date;
	$arr_main["students"] = $all_students_arr;

	echo json_encode($arr_main);
?>