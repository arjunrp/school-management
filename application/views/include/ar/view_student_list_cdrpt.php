	<?php
		$CI = & get_instance();
		$CI->load->model("model_config");

		$data_config = $CI->model_config->get_config($school_id);
		
		$option1_caption = $data_config[0]["option1_caption"];
		$option2_caption = $data_config[0]["option2_caption"];
		$option3_caption = $data_config[0]["option3_caption"];
		$option4_caption = $data_config[0]["option4_caption"];
	?>

	<?php	
		$arr_cols = array();
	    if($option1_caption<>"" ||  $option2_caption<>"" || $option3_caption<>"" || $option4_caption<>"") 	
		{
			$arr_cols[0]["col"] = "1";
			$arr_cols[0]["title"] = isset($option1_caption)? $option1_caption : "";
			$arr_cols[0]["url"] = IMG_PATH."/ontime.jpg";
			
			$arr_cols[1]["col"] = "1";
			$arr_cols[1]["title"] = isset($option2_caption)? $option2_caption :"";
			$arr_cols[1]["url"] = IMG_PATH."/writing.jpg";
			
			$arr_cols[2]["col"] = "1";
			$arr_cols[2]["title"] = isset($option4_caption)? $option4_caption : "" ;
			$arr_cols[2]["url"] = IMG_PATH."/homework.jpg";
			/*$arr_cols[3]["col"] = "1";
			$arr_cols[3]["title"] = $option3_caption;
			$arr_cols[3]["url"] = IMG_PATH."/homework.jpg";*/
		}

	?>    
    
    <?php
		$cnt = count($data_rs);
		$cnt = 5;
        for ($i=0; $i<count($data_rs); $i++) {
            $tbl_student_id = $data_rs[$i]['tbl_student_id'];
            
            if (trim($lan) == "ar") {
                $name = $data_rs[$i]['first_name_ar']." ".$data_rs[$i]['last_name_ar'];
            } else {
                $name = $data_rs[$i]['first_name']." ".$data_rs[$i]['last_name'];
            }
			
			if ($i == 5) {
				break;
			}

        	$arr_stu[$i]["student_id"] = $tbl_student_id;
			$arr_stu[$i]["title"] = $name;
			
			$arr_columns[0]["1"] = "Y";
			$arr_columns[1]["2"] = "Y";
			$arr_columns[2]["3"] = "Y";
			
			$arr_stu[$i]["columns"] = $arr_columns;
		}
    ?>
	<?php    
        $arr["columns"] = $arr_cols;
        $arr["students"] = $arr_stu;
        $temp = json_encode($arr);
		$temp = str_replace('{"1":"Y"},{"2":"Y"},{"3":"Y"}','{"1":"Y", "2":"Y", "3":"Y"}',$temp);
		echo $temp;
	?>
