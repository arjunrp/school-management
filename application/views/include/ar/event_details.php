<?php
	
	if (count($data_rs)>0) {
		$title = $data_rs[0]['title'];
		$description = $data_rs[0]['description'];
		$start_date = $data_rs[0]['start_date'];
		$end_date = $data_rs[0]['end_date'];
		$start_time = $data_rs[0]['start_time'];
		$added_date = $data_rs[0]['added_date'];
		$is_active = $data_rs[0]['is_active'];
		
		$start_date = date("d M Y ",strtotime($start_date));
		$end_date = date("d M Y ",strtotime($end_date));
	
		$arr_data = array();
		
		$arr_data['title'] = $title;
		$arr_data['start_date'] = $start_date;
		$arr_data['end_date'] = $end_date;
		$arr_data['time'] = $start_time;
		$arr_data['desc'] = $description;
		echo json_encode($arr_data);
	} else {
		echo "{\"code\":\"N\"}";
	}
?>
