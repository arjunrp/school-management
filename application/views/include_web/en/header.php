<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Aqdar Smart Care</title>
<link href="<?=CSS_WEB_PATH?>/bootstrap.min.css" type="text/css" rel="stylesheet" />
<link href="<?=CSS_WEB_PATH?>/custom.css" type="text/css" rel="stylesheet" />
<link href="<?=CSS_WEB_PATH?>/device.css" type="text/css" rel="stylesheet" />
<script src="<?=JS_PATH?>/jquery-1.7.1.min.js"></script> 

</head>
<body>
<style>
#pre-loader  {
		display:none;
		width: 200px;
		height: 200px;
		position: absolute;
		left: 50%;
		top: 50%;
		background-image: url('<?=IMG_PATH?>/preloader/preloader_2.gif');
		background-repeat: no-repeat;
		background-position: center;
		margin: -100px 0 0 -100px;
} 
navbar-header ul {
  margin: 0;
  padding: 0;
  list-style: none;
  position: relative;
  float: right;
  background: #eee;
  border-bottom: 1px solid #fff;
  border-radius: 3px;    
}

navbar-header li {
  float: left;          
}

navbar-header #login {
  border-right: 1px solid #ddd;
  box-shadow: 1px 0 0 #fff;  
}

navbar-header #login-trigger,
navbar-header #signup a {
  display: inline-block;
  *display: inline;
  *zoom: 1;
  height: 25px;
  line-height: 25px;
  font-weight: bold;
  padding: 0 8px;
  text-decoration: none;
  color: #444;
  text-shadow: 0 1px 0 #fff; 
}

navbar-header #signup a {
  border-radius: 0 3px 3px 0;
}

navbar-header #login-trigger {
  border-radius: 3px 0 0 3px;
}

navbar-header #login-trigger:hover,
navbar-header #login .active,
navbar-header #signup a:hover {
  background: #fff;
}

navbar-header #login-content {
  display: none;
  position: absolute;
  top: 24px;
  right: 0;
  z-index: 999;    
  background: #fff;
  background-image: linear-gradient(top, #fff, #eee);  
  padding: 15px;
  box-shadow: 0 2px 2px -1px rgba(0,0,0,.9);
  border-radius: 3px 0 3px 3px;
}

navbar-header li #login-content {
  right: 0;
  width: 250px;  
}

/*--------------------*/

#inputs input, select {
  /*background: #f1f1f1;*/
  padding: 6px 5px;
  margin: 0 0 5px 0;
  width: 238px;
  border: 1px solid #ccc;
  border-radius: 3px;
  /*box-shadow: 0 1px 1px #ccc inset;*/
}

#inputs input:focus select:focus {
  background-color: #fff;
  border-color: #e8c291;
  outline: none;
 /* box-shadow: 0 0 0 1px #e8c291 inset;*/
}

/*--------------------*/

#login #actions {
  margin: 10px 0 0 0;
}

#login #submit {    
  background-color: #d14545;
  background-image: linear-gradient(top, #e97171, #d14545);
  -moz-border-radius: 3px;
  -webkit-border-radius: 3px;
  border-radius: 3px;
  text-shadow: 0 1px 0 rgba(0,0,0,.5);
  box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;    
  border: 1px solid #7e1515;
  float: left;
  height: 30px;
  padding: 0;
  width: 100px;
  cursor: pointer;
  font: bold 14px Arial, Helvetica;
  color: #fff;
}

#login #submit:hover,
#login #submit:focus {    
  background-color: #e97171;
  background-image: linear-gradient(top, #d14545, #e97171);
} 

#login #submit:active {   
  outline: none;
  box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;   
}

#login #submit::-moz-focus-inner {
  border: none;
}

#login label {
  float: right;
  line-height: 30px;
}

#login label input {
  position: relative;
  top: 2px;
  right: 2px;
}

</style>
<!--Header-->
<?php 
$profileDisplay = "none";
$loginDisplay   = "block";
$relocation     = "#"; 
//echo $_SESSION['aqdar_smartcare']['user_type_sess'];
if(isset($_SESSION['aqdar_smartcare']['user_type_sess']) && $_SESSION['aqdar_smartcare']['user_type_sess']<>"" ){ 
  	$profileDisplay = "block";
	$loginDisplay   = "none";
	
	$logout_location = HOST_URL."/".LAN_SEL."/admin/signout";

	if($_SESSION['aqdar_smartcare']['user_type_sess']=="student")
	{
		$myprofile_location = HOST_URL."/".LAN_SEL."/students/home/welcome/";
		
	}else if($_SESSION['aqdar_smartcare']['user_type_sess']=="parent"){
		
		$myprofile_location = HOST_URL."/".LAN_SEL."/parent/home/welcome/";
		
	}else if($_SESSION['aqdar_smartcare']['user_type_sess']=="teacher"){
		
		$myprofile_location = HOST_URL."/".LAN_SEL."/admin/home/teacher";
		
	}else if($_SESSION['aqdar_smartcare']['user_type_sess']=="admin"){
		
		$myprofile_location = HOST_URL."/".LAN_SEL."/admin/home/welcome";		
		
	}else if($_SESSION['aqdar_smartcare']['user_type_sess']=="ministry"){
		
		$myprofile_location = HOST_URL."/".LAN_SEL."/admin/home/welcome";		
		
	}else if($_SESSION['aqdar_smartcare']['user_type_sess']=="timeline"){
		
		$myprofile_location = HOST_URL."/".LAN_SEL."/admin/home/welcome";		
		
	}else if($_SESSION['aqdar_smartcare']['user_type_sess']=="bus"){
		
		$myprofile_location = HOST_URL."/".LAN_SEL."/admin/home/welcome";		
	}
 }

 
 ?>
<script>
// sessionStorage -  Remember Me 
$(document).ready(function()
{
	if(sessionStorage.getItem("remember_me") == "Y")
	{
		$("#remember_me").attr('checked');
		$("#username").val(sessionStorage.getItem('remember_me_username'));
		$("#password").val(sessionStorage.getItem('remember_me_password'));
		$("#user_type").val(sessionStorage.getItem('remember_me_user_type'));
	}else{
		$("#remember_me").removeAttr('checked');
		$("#username").val('');
		$("#password").val('');
	}
});
</script>

<div class="container-fluid bottom_shadow">
  <div class="row header">
    <ul class="social">
      
      <li><a href=""><img src="<?=IMG_WEB_PATH?>/in.png"></a></li>
      <li><a href=""><img src="<?=IMG_WEB_PATH?>/twitter.png"></a></li>
      <li><a href=""><img src="<?=IMG_WEB_PATH?>/youtube.png"></a></li>
      <li><a href=""><img src="<?=IMG_WEB_PATH?>/google_p.png"></a></li>
      <li><a href=""><img src="<?=IMG_WEB_PATH?>/facebook.png"></a></li>
     
      <li style="width:80px; display:<?=$loginDisplay?>;"><a href="#" id="login-trigger">
                    Login<!-- <span>▼</span>-->
                  </a>
      </li> 
      <li  style="display:<?=$loginDisplay?>;" >&nbsp;|&nbsp;</li>
      <li style="width:80px;  display:<?=$loginDisplay?>;"><a href="#" id="registration-trigger">Registration</a>
      </li>
       
   <li style="width:80px;display:<?=$profileDisplay?>;"><a href="<?=$logout_location?>">Logout</a></li> 
   <li style="display:<?=$profileDisplay?>;">&nbsp;|&nbsp;</li>
   <li style="width:85px; white-space: nowrap; display:<?=$profileDisplay?>;"><a href="<?=$myprofile_location?>">My Account</a></li> 
   
   
   <li style="width:85px; white-space: nowrap; font-size:16px;" ><a href="<?=HOST_URL?>/ar" title="العربية" style="color:#060;">العربية</li>
      
      
    </ul>
    <div class="col-md-3 col-sm-2 col-xs-3 logo"> <a href=""> <img src="<?=IMG_WEB_PATH?>/logo.png" alt="Agdar" /></a> </div>
    <div class="col-md-9 col-sm-10 col-xs-9">
      <nav class="navbar navbar-default">
        <div class=" navbar-header">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          </div>
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="active"><a href="<?=HOST_URL?>/<?=LAN_SEL?>">Home</a></li>
              <li><a href="#">Features</a></li>
              <li><a href="#">Resources </a></li>
              <li><a href="#">Partners</a></li>
              <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/school_web/feedback/">Feedback</a></li>
              <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/school_web/contact_us/">Contact Us</a></li>
              
              <?php /*?> <li id="login" style="display:<?=$loginDisplay?>">
                  <a id="login-trigger" href="#">
                    Log in / Registration <span>▼</span>
                  </a>
                  <div id="login-content" style="display:none; position:relative; width:100px;">
                    <span style="color:#F00;" id="errorMsg"></span>  

                    <form name="frmLogin" id="frmLogin" >
                      <fieldset id="inputs">
                         <input id="username" type="text" name="username" placeholder="Username" required> 
                         <input id="password" type="password" name="password" placeholder="Password" required>
                         <span style="float:left; width:49px; font-size:16px; padding-top:5px;"> I am </span>
                         <span style="width:50px; float:right;">
                         <select id="user_type" style="width:190px;" >
                         <!-- <option value="">Select Type</option>-->
                          <option value="STUDENT">Student</option>
                          <option value="PARENT">Parent</option>
                          <option value="TEACHER">Teacher</option>
                          <option value="SCHOOL">School Administrator</option>
                          <option value="MINISTRY">Government</option>
                          <option value="BUS">Bus Supervisor</option>
                          <option value="TIMELINE">Timeline Technology</option>
                          </option>
                        </select> 
                        </span>
                      </fieldset>
                  
                      <fieldset id="actions">
                       <!-- <label><input type="checkbox" checked="checked"> Keep me signed in</label> -->
                        <input type="button" id="submit" value="Log in" onClick="validateForm()">
                      </fieldset>
                      
                      
                      
                      
                    </form>
                  </div>                     
                </li>
                
                
              <li style="display:<?=$profileDisplay?>;"><a href="<?=$myprofile_location?>">My Profile</a></li>  
                
                <?php */?>
              
            </ul>
          </div>
          <!-- /.navbar-collapse --> 
        </div>
      </nav>
    </div>
  </div>
</div>
<!--Banner-->
<div class="container-fluid banner"> 

   
               <div id="login-content" style="display:none; position:absolute; width:270px; right:18%; top:0; bottom:auto; z-index:99999;">
                   <div class="login-box regstr_form">
                    <p style="font-size:17px;">&nbsp;User Login</p>
                    
                  <!-- /.login-logo -->
                  <div class="login-box-body form-group has-feedback" style="margin-top:10px;">
                   
                    <span style="color:#F00;" id="errorMsg"></span>  

                    <form name="frmLogin" id="frmLogin" >
                      <fieldset id="inputs">
                         <input id="username" type="text" name="username" placeholder="Username" class="form-control" value="<?php echo $_COOKIE['remember_me_username'];?>" required > 
                         <input id="password" type="password" name="password" placeholder="Password" class="form-control" value="<?php echo $_COOKIE['remember_me_password'];?>" required >
                         <span style="float:left; width:49px; font-size:16px; padding-top:5px;"> I am </span>
                         <span style="width:50px; float:left;">
                         <select id="user_type" style="width:190px;" >
                         <!-- <option value="">Select Type</option>-->
                          <option value="STUDENT">Student</option>
                          <option value="PARENT">Parent</option>
                          <option value="TEACHER">Teacher</option>
                          <option value="SCHOOL">School Administrator</option>
                          <option value="MINISTRY">Government</option>
                          <option value="BUS">Bus Supervisor</option>
                          <option value="TIMELINE">Timeline Technology</option>
                          </option>
                        </select> 
                        </span>
                      </fieldset>
                  
                      <fieldset id="actions" style="margin-top:12px;">
                       <label style="font-size:13px;"><input type="checkbox"  class="w3-check" name="remember_me" id="remember_me"  > Remember Me</label>
                        <input type="button" class="btn btn-primary" id="submit" value="Log in" onClick="validateForm()" style="font-size:15px  !important; height:30px !important; padding:0px; float:right;">
                      </fieldset>
                       <fieldset id="actions" style="margin-top:12px;">
                       <label style=" margin-top:13px; float:right;"> <a style="text-decoration:underline; font-size:12px; width:100%" href="#" onClick="forgotPass();" >Forgot Password ?</a></label>
                      </fieldset>
                     
                    </form>
                    </div>
              </div>
               </div>
            
               <div id="registration-content" style="display:none; position:absolute; width:270px; right:18%; top:0; bottom:auto; z-index:99999; ">
                   <div class="login-box regstr_form">
                    <p style="font-size:17px;">&nbsp;Registration</p>
                    
                  <!-- /.login-logo -->
                  <div class="login-box-body form-group has-feedback" style="margin-top:10px;">
                   
                    <span style="color:#F00;" id="errorMsg"></span>  

                    <form name="frmReg" id="frmReg" >
                      <fieldset id="inputs">
                        
                         <span style="float:left; width:49px; font-size:16px; padding-top:5px;"> I am </span>
                         <span style="width:50px; float:left;">
                         <select id="user_type_reg" style="width:190px;" >
                         <!-- <option value="">Select Type</option>-->
                          <option value="STUDENT">Student</option>
                          <option value="PARENT">Parent</option>
                          <option value="TEACHER">Teacher</option>
                          <option value="SCHOOL">School Administrator</option>
                          <option value="MINISTRY">Government</option>
                          <option value="BUS">Bus Supervisor</option>
                          <option value="TIMELINE">Timeline Technology</option>
                          </option>
                        </select> 
                        </span>
                      </fieldset>
                  
                      <fieldset id="actions" style="margin-top:10px;">
                      
                        <input type="button" class="btn btn-primary" id="submit" value="Submit" onClick="regForm();" style="font-size:15px  !important; height:30px !important; padding:0px; float:right;">
                      </fieldset>
                    </form>
                    </div>
              </div>
               </div> 
            
              <div id="forgot_pass" style="display:none; position:absolute; right:18%; top:0; bottom:auto; z-index:99999; width:333px;">
                   <div class="login-box regstr_form">
                     <p style="font-size:17px;">&nbsp;Forgot Password</p>
                    
                  <!-- /.login-logo -->
                  <div class="login-box-body form-group has-feedback" style="margin-top:10px;">
                   
                    <span style="color:#F00;" id="errorForgotMsg"></span>  

                    <form name="frmForgot" id="frmForgot">
                      <fieldset id="inputs">
                         <input id="forgot_email" name="forgot_email" placeholder="Email" class="form-control" value="" required type="email" style="width:301px;"> 
                         <span style="float:left; width:70px; font-size:16px; padding-top:5px;"> I am </span>
                         <span style="width:50px; float:left;">
                         <select id="forgot_user_type" style="width:233px;">
                         <!-- <option value="">Select Type</option>-->
                          <option value="STUDENT">Student</option>
                          <option value="PARENT">Parent</option>
                          <option value="TEACHER">Teacher</option>
                          <option value="SCHOOL">School Administrator</option>
                          <option value="MINISTRY">Government</option>
                          <option value="BUS">Bus Supervisor</option>
                          <option value="TIMELINE">Timeline Technology</option>
                          
                        </select> 
                        </span>
                      </fieldset>
                  
                      <fieldset id="actions" style="margin-top:12px;">
                        <label style="float:left;"><a style="text-decoration:underline; font-size:12px; width:100%" onClick="get_login_panel();">&nbsp;Back To Login&nbsp;</a></label>
                        <input class="btn btn-primary" id="submit" value="Submit" onclick="validateForgot()" style="font-size:15px  !important; height:30px !important; padding:0px; float:right;" type="button">
                      </fieldset>
                    
                    </form>
                    </div>
              </div>
              </div>            
              



<img src="<?=IMG_WEB_PATH?>/banner_img.jpg" alt="image" />
  <div class="banner_title"><strong>Aqdar Smart Care</strong>
    <p>Khalifa Empowerment Program For Students</p>
  </div>
  <div class="appstore_wrap"> <a class="app" href=""><img src="<?=IMG_WEB_PATH?>/appstore.png" alt="App store"></a> <a class="google" href=""><img src="<?=IMG_WEB_PATH?>/google_play.png" alt="App store"></a> </div>
</div>

<script>
$(document).ready(function(){
  $('#login-trigger').click(function(){
	
	if($('#login-content').is(':visible')) {
		$("#login-content").hide();
		$("#forgot_pass").hide();
		$("#registration-content").hide();
	}else{
		$("#login-content").show();
		$("#forgot_pass").hide();
		$("#registration-content").hide();
	}
	
/*    $(this).next('#login-content').slideToggle();
    login-content
   
    $(this).toggleClass('active');
	$('#registration-content').hide(); 
	$("#forgot_pass").hide();         
    
    if ($(this).hasClass('active')) $(this).find('span').html('&#x25B2;')
      else $(this).find('span').html('&#x25BC;')*/
    
	})
	
     $('#registration-trigger').click(function(){
		
		if($('#registration-content').is(':visible')) { 
			$("#registration-content").hide();
			$("#login-content").hide();
			$("#forgot_pass").hide();
		}else{
		 	$("#registration-content").show();
			$("#login-content").hide();
			$("#forgot_pass").hide();
		}
		 
	 });
	
	
  /* $('#registration-trigger').click(function(){
    $(this).next('#registration-content').slideToggle();
    $(this).toggleClass('active');          
    $('#login-content').hide();
	$("#forgot_pass").hide();   
    if ($(this).hasClass('active')) $(this).find('span').html('&#x25B2;')
      else $(this).find('span').html('&#x25BC;')
    })	*/

});

  function forgotPass()
  {
	  $("#login-content").hide();
	  $("#forgot_pass").show();
  }
  
  function get_login_panel()
  {
	  $("#login-content").show();
	  $("#forgot_pass").hide(); 
  }

	function validateForm() {
		if ( validate_login_username() == false || validate_login_password() == false) {
			return false;
		} else {
			ajax_validate_and_login_user();
		}
	}//function validateForm

   function validateForgot()
   {
	   if ( validate_forgot_email() == false ) {
			return false;
		} else {
			ajax_send_password();
		}
   }


     function validate_forgot_email() {
		var regExp = / /g;
		var str = $('#forgot_email').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			var errorForgotMsg = " Email id is blank. Please enter your email id. "; 
			$("#errorForgotMsg").html(errorForgotMsg);
		return false;
		}
	
		if (!isNaN(str)) {
			var errorForgotMsg = " Invalid Email. "; 
			$("#errorForgotMsg").html(errorForgotMsg);
			return false;
		}
	
		if(str.indexOf('@', 0) == -1) {
			var errorForgotMsg = " Invalid Email. "; 
			$("#errorForgotMsg").html(errorForgotMsg);
			return false;
		}
	}
	
	
	function validate_login_username() {
		var regExp = / /g;
		var str = $('#username').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			/*my_alert("Username is blank. Please enter username.");*/
			var errorMsg = " Please enter username "; 
			$("#errorMsg").html(errorMsg);
		return false;
		}
	}

	function validate_login_password() {
		var regExp = / /g;
		var str = $('#password').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			/*my_alert("Password is blank. Please enter password.");*/
			var errorMsg = " Please enter password "; 
			$("#errorMsg").html(errorMsg);
		return false;
		}
	}
	
	
	function ajax_send_password() {
		$('#my_button').addClass("fa fa-spin fa-refresh");//For spinner
		
		var email =  $('#forgot_email').val();
		var user_type =  $('#forgot_user_type').val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/login/web_forgot_password/";
		
		
		$.ajax({
		type: "POST",
		url: url,
		dataType: "html",
		data: {
			email: $('#forgot_email').val(),
			user_type: $('#forgot_user_type').val(),
			is_ajax: true
			
		},
		success: function(data) {
			var temp = new String();
			temp = data;
			temp = temp.trim();
			if (temp=='N') {
				var errorForgotMsg = " Invalid User or Email Id. "; 
				$("#errorForgotMsg").html(errorForgotMsg);
			} else if (temp=='Y') {
			      alert("Your login details has been sent to your email account. Please check it and login.");
				  setTimeout(function () {
					location.href = '<?=HOST_URL?>/<?=LAN_SEL?>/school_web/'
					}, 300);
			}
		},
		error: function() {
		}, 
		complete: function() {
		}
		});
	}
	
	
	function ajax_validate_and_login_user() {
		$('#my_button').addClass("fa fa-spin fa-refresh");//For spinner
		
		var user_type =  $('#user_type').val();
		if(user_type=="STUDENT")
		{
			var module_access = "ST";
			var url = "<?=HOST_URL?>/<?=LAN_SEL?>/students/student_user/ajax_validate_and_login_user";
			var relocation = "<?=HOST_URL?>/<?=LAN_SEL?>/students/home/welcome/";
		}
		else if (user_type=="PARENT")
		{
			var module_access = "P";
			var url = "<?=HOST_URL?>/<?=LAN_SEL?>/parent/parent_user/ajax_validate_and_login_user";
			var relocation = "<?=HOST_URL?>/<?=LAN_SEL?>/parent/home/welcome/";
		}
		else if (user_type=="TEACHER")
		{
			var module_access = "T";
			var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/ajax_validate_and_login_teacher"
			var relocation = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/home/teacher";
			
		}
		else if (user_type=="SCHOOL")
		{
			var module_access = "S";
			var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/admin_user/ajax_validate_and_login_admin";
			var relocation = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/home/welcome";
			
		}
		else if (user_type=="MINISTRY")
		{
			var module_access = "G";
			var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/admin_user/ajax_validate_and_login_admin";
			var relocation = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/home/welcome";
			
		}
		else if (user_type=="TIMELINE")
		{
			var module_access = "T";
			var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/admin_user/ajax_validate_and_login_admin";
			var relocation = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/home/welcome";
		}
		
		if($("#remember_me").is(':checked')) {
				var remember_me ='Y';
				sessionStorage.setItem("remember_me_username", $('#username').val());
				sessionStorage.setItem("remember_me_password", $('#password').val());
				sessionStorage.setItem("remember_me_user_type", user_type);
				
		}else{
				var remember_me ='N';
				sessionStorage.setItem("remember_me_username", '');
				sessionStorage.setItem("remember_me_password", '');
		}
		
		$.ajax({
		type: "POST",
		url: url,
		dataType: "html",
		data: {
			username: $('#username').val(),
			password: $('#password').val(),
			module_access: module_access,
			remember_me: remember_me,
			is_ajax: true
			
		},
		success: function(data) {
			var temp = new String();
			temp = data;
			temp = temp.trim();
			
			if (temp=='*N*') {
				/*my_alert("Invalid Username or Password");*/
				var errorMsg = " Invalid Username or Password"; 
				$("#errorMsg").html(errorMsg);
			} else if (temp=='*Y*') {
			       window.location.href = relocation;
			}
		},
		error: function() {
		}, 
		complete: function() {
		}
		});
	}
	
	
	
	// Registration
	
	function regForm() {
		$('#my_button').addClass("fa fa-spin fa-refresh");//For spinner
		
		var user_type =  $('#user_type_reg').val();
		if(user_type=="STUDENT")
		{
			var relocation = "<?=HOST_URL?>/<?=LAN_SEL?>/school_web/student_details#anchor_reg";
			window.location.href = relocation;
		}
		else if (user_type=="PARENT")
		{
			var relocation = "<?=HOST_URL?>/<?=LAN_SEL?>/school_web/parents_details/";	
			window.location.href = relocation;
		}
		else if (user_type=="TEACHER")
		{
			var relocation = "<?=HOST_URL?>/<?=LAN_SEL?>/school_web/teacher_details/";
			window.location.href = relocation;
		}
		else if (user_type=="SCHOOL")
		{
			var relocation = "<?=HOST_URL?>/<?=LAN_SEL?>/school_web/school_details/";	
			window.location.href = relocation;		
		}
		else if (user_type=="BUS")
		{
			var relocation = "<?=HOST_URL?>/<?=LAN_SEL?>/school_web/school_details/";	
			window.location.href = relocation;		
		}
		
		
	
	}
</script>
<!--Header End--> 
