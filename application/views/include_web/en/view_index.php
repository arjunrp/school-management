<?php include("header.php"); ?>


<!--Banner End--> 
<!--About us-->
<div class="container-fluid about_home">
  <div class="row">
    <div class="col-md-12">
      <h2>Welcome to AQDAR - Khalifa Empowerment Program For Students</h2>
      <p> Aqdar Smart Care app is developed to create strong relation between parents and teachers. It can inform every movement of the child to parent instantly. 
        It also educate parent how to grow their child with the help of experts from the field. 
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Pri quas audiam virtute ut, case utamur fuisset eam ut, iisque accommodare an eam. Reque blandit qui eu, cu vix nonumy volumus. Legendos intellegam id usu, vide oporteat vix eu, id illud principes has. Nam tempor utamur gubergren no.</p>
      <p>Ex soleat habemus usu, te nec eligendi deserunt vituperata. Natum consulatu vel ea, duo cetero repudiare efficiendi cu. Has at quas nonumy facilisis, enim percipitur mei ad. Mazim possim adipisci sea ei, omnium aeterno platonem mei no.</p>
      <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      <p>Nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    </div>
  </div>
</div>
<!--About us End--> 
<!--Aplications-->
<div class="container-fluid  bg_grey">
  <div class="row">
    <div class="widget">
      <div class="outer_w"><span class="icon_w"> <img src="<?=IMG_WEB_PATH?>/school_icon.png" alt="image"> </span></div>
      <strong>School</strong>
      <p>Duis aute irure dolor in reprehenderit  Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      <a href="<?=HOST_URL?>/en/school_web/school_details/"> More Info</a> </div>
    <div class="widget">
      <div class="outer_w"><span class="icon_w"> <img src="<?=IMG_WEB_PATH?>/teacher_icon.png" alt="image"> </span></div>
      <strong>Teachers</strong>
      <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      <a href="<?=HOST_URL?>/en/school_web/teacher_details/"> More Info</a> </div>
    <div class="widget">
      <div class="outer_w"><span class="icon_w"> <img src="<?=IMG_WEB_PATH?>/parents_icon.png" alt="image"> </span></div>
      <strong>parents</strong>
      <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      <a href="<?=HOST_URL?>/en/school_web/parents_details/"> More Info</a> </div>
    <div class="widget">
      <div class="outer_w"><span class="icon_w"> <img src="<?=IMG_WEB_PATH?>/students_icon.png" alt="image"> </span></div>
      <strong>Students</strong>
      <p>esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      <a href="<?=HOST_URL?>/en/school_web/student_details/"> More Info</a> </div>
    <div class="widget last">
      <div class="outer_w"><span class="icon_w"> <img src="<?=IMG_WEB_PATH?>/supervisor_icon.png" alt="image"> </span></div>
      <strong>Supervisor</strong>
      <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident</p>
      <a href="<?=HOST_URL?>/en/school_web/supervisor_details/"> More Info</a> </div>
  </div>
</div>

<!--Aplications end--> 
<!--Posts-->
<div class="container-fluid  post_wrap">
  <div class="row">
    <div class="col-md-6 news">
      <h2>Latest News</h2>
      <ul>
        <li>
          <div class="left"> <img src="<?=IMG_WEB_PATH?>/news_1.jpg" alt="image" /> </div>
          <div class="right">
            <div class="date">25 December 2016</div>
            <strong>Lorum lupsum</strong>
            <p> Reference site about Lorem Ipsumas well as a random Lipsum generator. </p>
          </div>
        </li>
        <li>
          <div class="left"> <img src="<?=IMG_WEB_PATH?>/news_2.jpg" alt="image" /> </div>
          <div class="right">
            <div class="date">25 December 2016</div>
            <strong>Lorum lupsum</strong>
            <p> Reference site about Lorem Ipsumas well as a random Lipsum generator. </p>
          </div>
        </li>
        <li>
          <div class="left"> <img src="<?=IMG_WEB_PATH?>/news_3.jpg" alt="image" /> </div>
          <div class="right">
            <div class="date">25 December 2016</div>
            <strong>Lorum lupsum</strong>
            <p> Reference site about Lorem Ipsumas well as a random Lipsum generator. </p>
          </div>
        </li>
      </ul>
    </div>
    <div class="col-md-6 download">
      <h2>Downloads</h2>
      <div class="download_wrap"> <strong> 1000+</strong>
        <p>Downloads</p>
      </div>
      <div class="qr_code"> <img src="<?=IMG_WEB_PATH?>/qr_code.jpg" alt="qr" />
        <p> Download</p>
      </div>
    </div>
  </div>
</div>
<!--Posts end--> 
<?php include("footer.php"); ?>