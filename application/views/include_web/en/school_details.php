<?php include("header.php"); 
?>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

<style>
.submit_resume input {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
    border-color: currentcolor currentcolor #8a9ca7;
    border-image: none;
    border-style: none none solid;
    border-width: 0 0 1px;
    margin: 0 0 15px;
    padding: 18px 0 5px;
    transition: all 3s ease 0s;
    width: 100%;
}
</style>

<!--Banner End--> 
<!--About us-->
<div class="container-fluid title_des">
  <div class="row">
  
    <div class="col-md-12">
      <h2>Schools can have the following:</h2>
      <ul class="list">
        <li>Inform parents about the attendance of child for every session of the class</li>
        <li>Motivate child by providing points for their good deeds and reduce points for wrong doings</li>
        <li>Issue pre-defined cards to inform parents to take quick action</li>
        <li> Send daily report to parents on day to day affairs</li>
        <li> Show the parents various events at school</li>
        <li>Communicate between teachers about various subjects</li>
      </ul>
    </div>
    
     <?php /*?><div class="col-md-4">
                 <div class="login-box regstr_form" style="margin-top:30px;">
                    <h2>Student Login</h2>
                    
                  <!-- /.login-logo -->
                  <div class="login-box-body" style="margin-top:20px;">
                    
                    <form name="login" method="post">
                      <div class="form-group has-feedback">
                        <input id="username" name="username" class="form-control" placeholder="Username" type="text">
                        <span class="form-control-feedback"><i class="fa fa-user"></i></span>
                      </div>
                      <div class="form-group has-feedback">
                        <input id="password" name="password" class="form-control" placeholder="Password" type="password">
                        <span class="form-control-feedback"><i class="fa fa-unlock-alt"></i></span>
                      </div>
                     <div class="row">
                       <div class="col-xs-8">
                         <!-- <div class="checkbox icheck">
                            <label>
                              <input id="remember_me" name="remember_me" type="checkbox"> Remember Me
                            </label>
                          </div>-->
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-4">
                          <button type="button" class="btn btn-primary" onclick="validateForm()">Sign In</button>
                        
                        </div>
                        <!-- /.col -->
                      </div>
                    </form>
                  </div>
                  <!-- /.login-box-body -->
                </div>




       </div><?php */?>
       
    
  </div>
</div>

<!--About us End--> 
<!--Phone 1-->
<div class="container-fluid phone_1">
  <div class="row">
    <div class="col-md-12">
      <div class="mobile_1">
        <div class="text text-1"><strong>Students</strong>
          <p>Reference site about Lorem Ipsum,giving information on its origins, as well as a random Lipsum generator.</p>
        </div>
        <div class="text text-2"><strong>Teachers</strong>
          <p>Reference site about Lorem Ipsum,giving information on its origins, as well as </p>
        </div>
        <div class="text text-3"><strong>Gallery</strong>
          <p>Reference site about Lorem Ipsum,giving information on its origins, as well as a random Lipsum generator.</p>
        </div>
        <div class="text text-4"><strong>Calender</strong>
          <p>Reference site about Lorem Ipsum,giving information on ndom Lipsum generator.</p>
        </div>
        <div class="text text-5"><strong>Others</strong>
          <p>Reference site about Lorem Ipsumas well as a random Lipsum generator.</p>
        </div>
        <img class="desktop" src="<?=IMG_WEB_PATH?>/phone_1.png" alt="image" /> <img class="mobile" src="<?=IMG_WEB_PATH?>/phone_1_600.png" alt="image" /> <img class="mobile_small" src="<?=IMG_WEB_PATH?>/phone_1_300.png" alt="image" /> </div>
    </div>
  </div>
</div>
<!--Phone 1 end--> 
<!--Phone 2-->
<div class="container-fluid phone_2">
  <div class="row">
    <div class="col-md-12">
      <div class="mobile_2">
        <div class="text text-1"><strong>Messages</strong>
          <p>Reference site about Lorem Ipsum,giving information on its origins, as well as a random Lipsum generator.</p>
        </div>
        <div class="text text-2"><strong>Private Messages</strong>
          <p>Reference site about Lorem Ipsum,giving information on its origins, as well as </p>
        </div>
        <div class="text text-3"><strong>Incentives</strong>
          <p>Reference site about Lorem Ipsum,giving information on its origins, as well as a random Lipsum generator.</p>
        </div>
        <div class="text text-4"><strong>Information</strong>
          <p>Reference site about Lorem Ipsum,giving information on ndom Lipsum generator.</p>
        </div>
        <div class="text text-5"><strong>Attendance</strong>
          <p>Reference site about Lorem Ipsumas well as a random Lipsum generator.</p>
        </div>
        <div class="text text-6"><strong>Behavioral Control</strong>
          <p>Reference site about Lorem Ipsumas well as a random Lipsum generator.</p>
        </div>
        <img class="desktop" src="<?=IMG_WEB_PATH?>/phone_2.png" alt="image" /> <img class="mobile" src="<?=IMG_WEB_PATH?>/phone_2_600.png" alt="image" /> <img class="mobile_small" src="<?=IMG_WEB_PATH?>/phone_2_300.png" alt="image" /> </div>
    </div>
  </div>
</div>

<!--Phone 2 end--> 
<!--tab-->

<!--tab end--> 
<?php include("footer.php"); ?>