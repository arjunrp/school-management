<?php include("header.php"); 
     $teacher_id_enc = md5(uniqid(rand()));
?>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

<style>
.submit_resume input {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
    border-color: currentcolor currentcolor #8a9ca7;
    border-image: none;
    border-style: none none solid;
    border-width: 0 0 1px;
    margin: 0 0 15px;
    padding: 18px 0 5px;
    transition: all 3s ease 0s;
    width: 100%;
}
</style>

<!--Banner End--> 
<!--About us-->
<div class="container-fluid title_des">
  <div class="row">
  
    <div class="col-md-12">
      <h2>Teachers can have the following:</h2>
      <ul class="list">
        <li>Inform parents about the attendance of child for every session of the class</li>
        <li>Motivate child by providing points for their good deeds and reduce points for wrong doings</li>
        <li>Issue pre-defined cards to inform parents to take quick action</li>
        <li> Send daily report to parents on day to day affairs</li>
        <li> Show the parents various events at school</li>
        <li>Communicate between teachers about various subjects</li>
      </ul>
    </div>
    
     <?php /*?><div class="col-md-4">
                 <div class="login-box regstr_form" style="margin-top:30px;">
                    <h2>Student Login</h2>
                    
                  <!-- /.login-logo -->
                  <div class="login-box-body" style="margin-top:20px;">
                    
                    <form name="login" method="post">
                      <div class="form-group has-feedback">
                        <input id="username" name="username" class="form-control" placeholder="Username" type="text">
                        <span class="form-control-feedback"><i class="fa fa-user"></i></span>
                      </div>
                      <div class="form-group has-feedback">
                        <input id="password" name="password" class="form-control" placeholder="Password" type="password">
                        <span class="form-control-feedback"><i class="fa fa-unlock-alt"></i></span>
                      </div>
                     <div class="row">
                       <div class="col-xs-8">
                         <!-- <div class="checkbox icheck">
                            <label>
                              <input id="remember_me" name="remember_me" type="checkbox"> Remember Me
                            </label>
                          </div>-->
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-4">
                          <button type="button" class="btn btn-primary" onclick="validateForm()">Sign In</button>
                        
                        </div>
                        <!-- /.col -->
                      </div>
                    </form>
                  </div>
                  <!-- /.login-box-body -->
                </div>




       </div><?php */?>
       
    
  </div>
</div>

<!--About us End--> 
<!--Phone 1-->
<div class="container-fluid phone_1">
  <div class="row">
    <div class="col-md-12">
      <div class="mobile_1">
        <div class="text text-1"><strong>Students</strong>
          <p>Reference site about Lorem Ipsum,giving information on its origins, as well as a random Lipsum generator.</p>
        </div>
        <div class="text text-2"><strong>Teachers</strong>
          <p>Reference site about Lorem Ipsum,giving information on its origins, as well as </p>
        </div>
        <div class="text text-3"><strong>Gallery</strong>
          <p>Reference site about Lorem Ipsum,giving information on its origins, as well as a random Lipsum generator.</p>
        </div>
        <div class="text text-4"><strong>Calender</strong>
          <p>Reference site about Lorem Ipsum,giving information on ndom Lipsum generator.</p>
        </div>
        <div class="text text-5"><strong>Others</strong>
          <p>Reference site about Lorem Ipsumas well as a random Lipsum generator.</p>
        </div>
        <img class="desktop" src="<?=IMG_WEB_PATH?>/phone_1.png" alt="image" /> <img class="mobile" src="<?=IMG_WEB_PATH?>/phone_1_600.png" alt="image" /> <img class="mobile_small" src="<?=IMG_WEB_PATH?>/phone_1_300.png" alt="image" /> </div>
    </div>
  </div>
</div>
<!--Phone 1 end--> 
<!--Phone 2-->
<div class="container-fluid phone_2">
  <div class="row">
    <div class="col-md-12">
      <div class="mobile_2">
        <div class="text text-1"><strong>Messages</strong>
          <p>Reference site about Lorem Ipsum,giving information on its origins, as well as a random Lipsum generator.</p>
        </div>
        <div class="text text-2"><strong>Private Messages</strong>
          <p>Reference site about Lorem Ipsum,giving information on its origins, as well as </p>
        </div>
        <div class="text text-3"><strong>Incentives</strong>
          <p>Reference site about Lorem Ipsum,giving information on its origins, as well as a random Lipsum generator.</p>
        </div>
        <div class="text text-4"><strong>Information</strong>
          <p>Reference site about Lorem Ipsum,giving information on ndom Lipsum generator.</p>
        </div>
        <div class="text text-5"><strong>Attendance</strong>
          <p>Reference site about Lorem Ipsumas well as a random Lipsum generator.</p>
        </div>
        <div class="text text-6"><strong>Behavioral Control</strong>
          <p>Reference site about Lorem Ipsumas well as a random Lipsum generator.</p>
        </div>
        <img class="desktop" src="<?=IMG_WEB_PATH?>/phone_2.png" alt="image" /> <img class="mobile" src="<?=IMG_WEB_PATH?>/phone_2_600.png" alt="image" /> <img class="mobile_small" src="<?=IMG_WEB_PATH?>/phone_2_300.png" alt="image" /> </div>
    </div>
  </div>
</div>

<!--Phone 2 end--> 
<!--tab-->
<style>
.errorClass{
	color:red; 
	position:absolute;	
	font-size:12px;
}

.errorMsg{
	color:red; 
	position:absolute;	
	font-size:14px;
}

.successMsg{
	color:green; 
	position:absolute;	
	font-size:14px;
}

</style>
 <script src="<?=JS_PATH?>/mask_input/jquery.maskedinput.min.js"></script>
     <script>
      $(document).ready(function() {
        $.mask.definitions['~'] = "[+-]";
		$("#mobile").mask("999999999999");
		$("#mobile_parent").mask("999999999999");
		$("#tbl_emirates_id").mask("999-9999-9999999-9");
		$("#emirates_id_father").mask("999-9999-9999999-9");
		$("#emirates_id_mother").mask("999-9999-9999999-9");
		$("#emirates_id_parent").mask("999-9999-9999999-9");
    });
	</script>	   
<a name="anchor_reg"></a>
<div class="container-fluid tab_wrap_border">
  <div class="row">
    <div class="col-md-12">
      <div class="tab_wrap">
        <ul class="nav nav-tabs responsive-tabs">
          <li class="active"><a href="#register">Registration </a></li>
         <!-- <li><a href="#login">Guardian Information</a></li>-->
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="register">
            <div class="regstr_form">
              <div class="col-md-6">
               <h3>Teacher Information</h3>
                <ul>
                  <li>
                    <label>First Name*</label>
                    <input name="first_name" id="first_name" class="form-control" type="text" tabindex="1" maxlength="50">
                    <span class="errorClass" id="first_name_error" style="display:none;">The First Name is blank. Please write First Name.</span>
                  </li>
                   <li>
                    <label>Last Name*</label>
                    <input name="last_name" id="last_name" class="form-control" type="text" tabindex="2" maxlength="50">
                    <span class="errorClass"  id="last_name_error" style="display:none;">The Last Name is blank. Please write Last Name.</span>
                  </li>
                  
                  <li>
                    <div class="first">
                      <label>Date of Birth</label>
                    <select name="dob_month" id="dob_month"  class="form-control" tabindex="3">
                                    <option value="">--Month--</option>
                                    <?php for ($m=1; $m<=12; $m++) { ?>
                                    <option value="<?=$m?>" <?php if ($dob_month == $m) {echo "selected";}?> ><?=$m?></option>
                                    <?php } ?>
                    </select>
                    <span class="errorClass"  id="dob_error" style="display:none;">Please select Date of birth.</span>
                    </div>
                    <div class="second">
                       <?php if (!isset($dob_day) || trim($dob_day) == "") {$dob_day = '';}?>
                                      <select name="dob_day" id="dob_day" tabindex="4" class="form-control">
                                        <option value="">--Day--</option>
                                      	<?php for ($d=1; $d<=31; $d++) { ?>
                                      	<option value="<?=$d?>" <?php if ($dob_day == $d) {echo "selected";}?> ><?=$d?></option>
                                        <?php } ?>
                    </select>
                    </div>
                    <div class="third">
                       <?php if (!isset($dob_year) || trim($dob_year) == "") {$dob_year = '';}?>
                                      <select name="dob_year" id="dob_year" tabindex="5" class="form-control">
                                        <option value="">--Year--</option>

                                      	<?php for ($y=1950; $y<=date('Y'); $y++) { ?>
                                      	<option value="<?=$y?>" <?php if ($dob_year == $y) {echo "selected";}?> ><?=$y?></option>
                                        <?php } ?>
                                      </select>         
                    </div>
                    
                  </li>
                  
                 <div class="left">
                        <label>Nationality</label><br>
                  		<select name="country" id="country" tabindex="6" class="form-control">
                                      	<?php for ($c=0; $c<count($countries_list); $c++) { ?>
                                      	<option  value="<?=$countries_list[$c]['country_id']?>"  <?php if($country==$countries_list[$c]['country_id']) { ?> selected="selected" <?php } ?> ><?=$countries_list[$c]['country_name']?></option>
                                        <?php } ?>
                        </select>
                        <span class="errorClass"  id="country_error" style="display:none;">Please select Nationality.</span>
                  </div>
                   <div class="right"> 
                    <label>Gender*</label></br>
                     <label>
                          <input type="radio" id="gender" name="gender" value="male" class="minimal"  checked="checked"  tabindex="7" >
                          Male
                     </label>
                        &nbsp;
                     <label>
                          <input type="radio" id="gender1" name="gender" value="female" class="minimal"  >
                          Female
                   </label>
                  </div>  
                  </li>
                  
                  
                   <li>
                 
                   <div class="left"> 
                    <label>Mobile</label></br>
                    <span style="position:absolute; padding-left:20px; padding-top:5px;"> +</span>
                    <input name="mobile" id="mobile" class="form-control" type="text" style="padding-left:30px;" tabindex="14" maxlength="20">
                    <span class="errorClass"  id="mobile_error" style="display:none;">Please enter valid mobile number.</span>
                  </div>  
                  <div class="right">
                        <label>Emirates Id* (xxx-xxxx-xxxxxxx-x)</label><br>
                  		<input name="tbl_emirates_id" id="tbl_emirates_id" class="form-control" type="text" tabindex="15" maxlength="20">
                        <span class="errorClass"  id="emirates_id_error" style="display:none;">Please provide correct Teacher Emirates Id.</span>
                   </div>
                  
                  </li> 
                  <li>
                    <div>
                      <label>Email</label>
                      <input name="email" id="email"class="form-control" type="text" tabindex="16" maxlength="30">
                      <span class="errorClass"  id="email_error" style="display:none;">Invalid Email Id.</span>
                    </div>
                 </li>
                 
           
                  
                  <li>
                   <label>Picture</label></br>
                   <span style="float:left;"><input type="file" name="myfile" id="myfile" class="form-control" tabindex="9" /></span><span style="float:left;" >&nbsp;&nbsp;<button id="upload" style="height:40px; width:100px;">Upload</button></span>
        		   <div id="img_div" style=" float:right;display:none; margin-right:10px; width:100px; height:100px; "></div>
                  </li> 
                  
              
                </ul>
              </div>
              <div class="col-md-6">
              <h3>&nbsp;</h3>
                <ul>
                  <li>
                    <label>School*</label>
                    <select name="tbl_school_id" id="tbl_school_id" tabindex="10" class="form-control" onChange="get_school_class_ajax()">
                                        <option value="">Select School</option>
                                      	<?php for ($c=0; $c<count($list_schools); $c++) { ?>
                                      	<option  value="<?=$list_schools[$c]['tbl_school_id']?>" ><?=$list_schools[$c]['school_name']?></option>
                                        <?php } ?>
                        </select>
                    <span class="errorClass"  id="school_error" style="display:none;">Please select School.</span>
                  </li>
                 
                  <div id="divClass">  
                 
                  </div>
                 
                  <li> 
                    <div>
                      <label>Username* (Minimum 6 Characters)</label>
                      <input name="parent_user_id" id="parent_user_id" class="form-control" type="text" tabindex="17" maxlength="30">
                      <span class="errorClass"  id="parent_user_id_error" style="display:none;">Please enter valid Username.</span>
                    </div>
                 </li>
                 
                 
                  <li>
                    <div>
                      <label>Password* (Minimum 6 Characters)</label>
                      <input name="parent_password" id="parent_password" class="form-control" type="password" tabindex="18" maxlength="15">
                      <span class="errorClass"  id="password_error" style="display:none;">Please enter valid Password.</span>
                    </div>
                 </li>
                 
                 
                  <li>
                    <div>
                      <label>Confirm Password* </label>
                      <input name="confirm_password" id="confirm_password" class="form-control" type="password" tabindex="19" maxlength="15">
                      <span class="errorClass"  id="confirm_password_error" style="display:none;">Please enter correct confirm password.</span>
                    </div>
                 </li>
                 
                </ul>
              </div>
              <div class="row">
                <div class="col-md-12 button_sbmit" style="text-align:right;">
                  <button type="button" class="btn btn-primary" onclick="ajax_validate()" style="font-size:15px  !important; height:30px !important; padding:0px;" tabindex="20">Submit</button>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane" id="login">
          	<!--<div class="regstr_form">
            	<div class="col-md-6">
                
                <ul>
                  <li>
                    <label>User Name*</label>
                    <input name="name" id="name" class="form-control" type="text">
                  </li>
                  <li>
                    <label>Password*</label>
                    <input  name="name" id="name" class="form-control" type="password">
                  </li>
                </ul>
                <div class="row">
                <div class="col-md-12 button_sbmit">
                  <button type="button" class="btn btn-primary" onclick="create_comments()">Submit</button>
                </div>
              </div>
              </div>
            </div>-->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script language="javascript">
            $(document).ready(function (e) {
                $('#upload').on('click', function () {
                    var file_data = $('#myfile').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('myfile', file_data);
					form_data.append('module_name', 'teacher');
					form_data.append('item_id', '<?=$teacher_id_enc?>');
					form_data.append('upload_from', 'web');
					form_data.append('is_ajax', 'true');
					
                    $.ajax({
                        url: '<?=HOST_URL?>/<?=LAN_SEL?>/file_mgmt/upload_the_file', // point to server-side controller method
                        dataType: 'text', // what to expect back from the server
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function (response) {
							var dataResponse = jQuery.parseJSON( response);
							var imgsrc= '<img src="'+dataResponse.url+'"   style="border:1px solid #999;height:100px;width:100px;" />';
							$('#img_div').show();
                            $('#img_div').html(imgsrc); // display success response from the server
                        },
                        error: function (response) {
                            $('#msg').html(response); // display error response from the server
                        }
                    });
                });
            });




var host = '<?=HOST_URL?>';
function get_school_class_ajax() {
	
		//show_loading();
		var tbl_school_id = $('#tbl_school_id').val();
		var xmlHttp, rnd, url, search_param, ajax_timer;
		rnd = Math.floor(Math.random()*11);
		try{		
			xmlHttp = new XMLHttpRequest(); 
		}catch(e) {
			try{
				xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
			}catch(e) {
				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
				hide_loading();
			}
		}

		//AJAX response
		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
				//ajax_timer.stop();
				var data = xmlHttp.responseText;
				$("#divClass").html(data);
				//$("#tbl_student_dropdown").multiselect('refresh');
				return;
			}
		}

      /*  if(class_ids=="all")
		{
			$("#all_student").hide();
		}*/
		/*ajax_timer = $.timer(function() {
			xmlHttp.abort();
			alert(connectivity_msg);
			ajax_timer.stop();
		},connectivity_timeout_time,true);*/

		//Sending AJAX request
		url = "<?=HOST_URL?>/<?=LAN_SEL?>/school_web/classes_subject_against_school/tbl_school_id/"+tbl_school_id+"/rnd/"+rnd;
		xmlHttp.open("POST",url,true);
		xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlHttp.send("rnd="+rnd);
	}


   //add student
   /* || validate_picture() == false*/ /* ||  validate_class() == false */
	function ajax_validate() {
		if (validate_first_name() == false || validate_last_name() == false || validate_dob() == false || validate_emirates_id() == false || validate_school() == false   
		 || isMobile() == false || validate_email() == false || validate_user_id() == false || validate_password() == false || validate_confirm_password() == false 
		|| isPasswordSame()==false || validate_exist_teacher()==false ) 
		{
			return false;
		} 
		else {
			
			return true;
		}
	}
	
	
	function validate_exist_teacher() {
		$("#pre-loader").show();
		
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/is_exist_teacher",
			data: {
				teacher_id_enc: "<?=$teacher_id_enc?>",
				emirates_id: $('#tbl_emirates_id').val(),
				tbl_school_id: $('#tbl_school_id').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
					refresh_page = "N";
					alert("Teacher is already exists.");
					$("#pre-loader").hide();
					return false;
				}else{
					validate_exist_teacher_userid();
				}
			
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	

    function validate_exist_teacher_userid()
   {
	   $.ajax({
				type: "POST",
				url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/is_exist_teacher_user_id",
				dataType: "text",
				data: {
					 teacher_id_enc  		: '<?=$teacher_id_enc?>',
					 user_id 		        : $("#parent_user_id").val(),
					is_ajax: true
				},
				success: function(data) {
					    var temp = new String();
						temp = data;
						temp = temp.trim();
						if (temp=='Y') {
							alert("User Id is already exist. please try another one.");
							$("#pre-loader").hide();
						}else{
						   ajax_create();
						}
					
					},
					error: function() {
						$("#pre-loader").hide();
					}, 
					complete: function() {
						$("#pre-loader").hide();
					}
				});
   }
	
	
	function ajax_create()
	{
		
		$.ajax({
				type: "POST",
				url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/teacher/add_teacher",
				data: {
					teacher_id_enc: "<?=$teacher_id_enc?>",
					first_name    : $('#first_name').val(),
					last_name     : $('#last_name').val(),
					first_name_ar : $('#first_name').val(),
					last_name_ar  : $('#last_name').val(),
					dob_month     : $('#dob_month').val(),
					dob_day       : $('#dob_day').val(),
					dob_year      : $('#dob_year').val(),
					gender        : $('input[name=gender]:checked').val(),
					mobile        : $('#mobile').val(),
					email         : $('#email').val(),
					emirates_id   : $('#tbl_emirates_id').val(),
					country       : $('#country').val(),
					tbl_academic_year_id : '',
					tbl_class_id           : $('#tbl_class_id').val(),
					tbl_school_id          : $('#tbl_school_id').val(),
					tbl_school_roles_id    : $('#tbl_school_roles_id').val(),
					tbl_subject_id         : $('#tbl_subject_id').val(),
					tbl_class_teacher_id   : $('#tbl_class_teacher_id').val(),
					user_id                : $('#parent_user_id').val(),
					password               : $('#parent_password').val(),
					is_active			   : 'N',
					is_ajax: true
				},
				success: function(data) {
					var temp = new String();
					temp = data;
					temp = temp.trim();
					if (temp=='Y') {
						alert("Teacher registered successfully. Registration under the school administrator approval. ");
						$("#pre-loader").hide();
						location.reload();
					}else{
						
						refresh_page = "N";
						alert("Teacher registration failed, Please try again.", 'red');
						$("#pre-loader").hide();
					}
					
				},
				error: function() {
					$("#pre-loader").hide();
				}, 
				complete: function() {
					$("#pre-loader").hide();
				}
			});
	
	}
 
   /****************************************************/
   /****************************************************/

   function validate_first_name() {
		var regExp = / /g;
		var str = $("#first_name").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
		    $("#first_name_error").show();
			$("#first_name").val('');
			$("#first_name").focus();
		return false;
		}
	    $("#first_name_error").hide();
	}
	
	function validate_last_name() {
		var regExp = / /g;
		var str = $("#last_name").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			$("#last_name_error").show();
			$("#last_name").val('');
			$("#last_name").focus();
		return false;
		}
	  $("#last_name_error").hide();
	}


	function validate_dob() {
		var month_index  = $("#dob_month").val();
		var day_index	 = $("#dob_day").val();
		var year_index 	 = $("#dob_year").val();
		if (month_index == "" && day_index == "" && year_index =="") {
			return true;
		}else if(month_index == "" || day_index == "" || year_index =="") {
			$("#dob_error").show();
			/*my_alert("Please select Date of Birth.")*/
			return false		
		}
	  $("#dob_error").hide();
	}
	
		
	
    
   function validate_emirates_id() {
		$("#emirates_id_error").hide(); 
		var regExp = / /g;
		var str = $("#tbl_emirates_id").val(); 
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			  $("#emirates_id_error").show();
			  return false;  
		}

		var emrno = /^\(?([0-9]{3})\)?[-]?([0-9]{4})[-]?([0-9]{7})[-]?([0-9]{1})$/;  
		if(str.match(emrno))  
		{  
			   return true;    
		}  
		else  
		{  
		   $("#emirates_id_error").show();
		   return false;  
		 } 
      $("#emirates_id_error").hide(); 
	}
	
	
	
	 function validate_school() {
		$("#school_error").hide(); 
		var regExp = / /g;
		var str = $("#tbl_school_id").val(); 
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			  $("#school_error").show();
			  return false;  
		}
      $("#school_error").hide(); 
	  return true;   
	}
	
	
	 function validate_class() {
		$("#class_error").hide(); 
		var regExp = / /g;
		var str = $("#tbl_class_id").val(); 
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			  $("#class_error").show();
			  return false;  
		}
      $("#class_error").hide(); 
	  return true;   
	}
	
	
	 function validate_first_name_parent() {
		var regExp = / /g;
		var str = $("#first_name_parent").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			$("#first_name_parent_error").show(); 
			$("#first_name_parent").val('');
			$("#first_name_parent").focus();
		return false;
		}
		$("#first_name_parent_error").hide();
		return true; 
	
	}
	
	function validate_last_name_parent() {
		var regExp = / /g;
		var str = $("#last_name_parent").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			$("#last_name_parent_error").show(); 
			$("#last_name_parent").val('');
			$("#last_name_parent").focus();
		return false;
		}
		$("#last_name_parent_error").hide(); 
	
	}
	
	function validate_parent_emirates_id() {
		var regExp = / /g;
		var str = $("#emirates_id_father").val(); 
		str = str.replace(regExp,'');
		if (str.length <= 0) {
		     $("#emirates_id_father_error").show();
			return false;
		}

		  var emrno = /^\(?([0-9]{3})\)?[-]?([0-9]{4})[-]?([0-9]{7})[-]?([0-9]{1})$/;  
		  if(str.match(emrno))  
		  {  
			   $("#emirates_id_father_error").hide();
			   return true;    
		  }  
		  else  
		  {  
			  $("#emirates_id_father_error").show();
			  /* my_alert("Please provide correct Father Emirates Id");  */
			   return false;  
		 } 
	    $("#emirates_id_father_error").hide();
	}

	
	function isMobile() {
		
		var strPhone = $.trim($("#mobile").val());
		if($.trim(strPhone)== "") {
			$("#mobile_error").hide();
			return true;
		}else{
				if (strPhone.length < 12 || strPhone.length > 12) {
					$("#mobile_error").show();
					//my_alert("Please enter valid mobile number.");
					$("#mobile").focus();
					return false;
			    }

				for (var i = 0; i < strPhone.length; i++) {
				var ch = strPhone.substring(i, i + 1);
					if  (ch < "0" || "9" < ch) {
						/*my_alert("The mobile number in digits only, Please re-enter your valid mobile number");*/
						$("#mobile_error").show();
						$("#mobile").focus();
						return false;
					}
				}
		}
	 $("#mobile_error").hide();
	 return true;
	}


    function validate_email() {
		var regExp = / /g;
		var str = $("#email").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
		     return true;	
		}
		if (!isNaN(str)) {
			/*my_alert("Invalid Email Id.");*/
			$("#email_error").show();
			$("#email").focus();
			$("#email").select();
			return false;
		}

		if(str.indexOf('@', 0) == -1) {
			/*my_alert("Invalid Email Id.");*/
			$("#email_error").show();
			$("#email").focus();
			$("#email").select();
			return false;
		}
     $("#email_error").hide();
	 return true;
	}

	
	function validate_user_id() {
		var str =  $.trim($("#parent_user_id").val());
		if ( str=="" ) {
			$("#parent_user_id_error").show();
			//my_alert("The User ID is blank. Please write User ID.");
			$("#parent_user_id").focus();
			return false;
		}
		if (str.length < 6 ) {
			//my_alert("The User ID should be greater than 5 characters.");
			$("#parent_user_id").focus();
			return false;
		}
		/*if (!isNaN(str)) {
			my_alert("The User ID have only letters & digits, Please re-enter User ID");
			$("#parent_user_id").focus();
			return false;
		}*/
	    $("#parent_user_id_error").hide();
	   return true;
	 }
	 
    /* Password */
	function validate_password() {
		var str = $('#parent_password').val(); 
		if (str == "") {
			$("#password_error").show();
			//my_alert("The Password field is blank. Please enter Password.");
			$('#parent_password').focus();
			return false;
		}
		if($('#password').val()!="") {
			var str = $('#password').val();
			var regExp = / /g;
			var tmp = $('#password').val();
			tmp = tmp.replace(regExp,'');
			if (tmp.length <= 0) {
				$("#password_error").show();
				$('#parent_password').focus();
			return false;
			}	
		}
		if (str.length < 6) {
			$("#password_error").show();
			/*my_alert("The Password should be greater than 5 Characters.");*/
			$('#parent_password').focus();
			$('#parent_password').select();
			return false;
		}
		
      $("#password_error").hide();
	return true;
	}


	/* Retype Password */
	function validate_confirm_password() {
		var str = $('#confirm_password').val();
		if (str == "") {
			$('#confirm_password_error').show();
			//my_alert("The Confirm Password field is blank. Please Retype Password.");
			$('#confirm_password').focus();
			return false;
		}
		if($('#confirm_password').val()!="") {
				var str = $('#confirm_password').val();
				var regExp = / /g;
				var tmp = $('#confirm_password').val();
				tmp = tmp.replace(regExp,'');
				if (tmp.length <= 0) {
					//my_alert("Enter valid Confirm Password.");
					$('#confirm_password_error').show();
					$('#confirm_password').focus();
					return false;
				}	
			
		}
	  $('#confirm_password_error').hide();
	return true;
	}

	/* Check both password */
	function isPasswordSame() {
		var str1 = $('#parent_password').val();
		var str2 = $('#confirm_password').val();
		if (str1 != str2) {
			//my_alert("Password mismatch, Please Retype same Passwords in both fields.");
			$('#confirm_password_error').show();
			$('#confirm_password').focus();
			return false;
		}
	$('#confirm_password_error').hide();
	return true;
	}

</script>



<!--tab end--> 
<?php include("footer.php"); ?>