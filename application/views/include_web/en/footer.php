<!--Footer-->
<div class="container-fluid bg_color_footer">
  <div class="row">
    <div class="col-md-8 links">
      <ul>
        <li><a href="#">Home</a></li>
        <li><a href="#">Resources</a></li>
        <li><a href="#">Features</a></li>
        <li><a href="#">Partners</a></li>
        <li><a href="#">Registration</a></li>
        <li><a href="#">Contact us</a></li>
        <li><a href="#">Feedback</a></li>
      </ul>
    </div>
    <div class="col-md-4 subscribe_W">
      <h4>Subscribe to our newsletter.</h4>
      <div class="subscribe">
        <input type="text" placeholder="Enter your Email Address" class="sub_field" />
        <button class="btn_ft" type="submit">Submit</button>
      </div>
      <div class="social_w"> <a href="#"><img src="<?=IMG_WEB_PATH?>/facebook.png" alt="image" /></a> <a href="#"><img src="<?=IMG_WEB_PATH?>/twitter.png" alt="image" /></a> <a href="#"><img src="<?=IMG_WEB_PATH?>/youtube.png" alt="image" /></a> <a href="#"><img src="<?=IMG_WEB_PATH?>/in.png" alt="image" /></a> <a href="#"><img src="<?=IMG_WEB_PATH?>/google_p.png" alt="image" /></a> </div>
    </div>
  </div>
</div>
<div class="container-fluid copyright">
  <div class="row">
    <div class="col-md-12">
      <p>Copyright © All Rights Reserved.</p>
    </div>
  </div>
</div>

<!-- jQuery --> 
<script src="<?=JS_WEB_PATH?>/jquery.js"></script> 

<!-- Bootstrap Core JavaScript --> 
<script src="<?=JS_WEB_PATH?>/bootstrap.min.js"></script>

<div id="pre-loader"></div>
 <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-102262704-1', 'auto');
  ga('send', 'pageview');

</script> 
</body>
</html>
