<?php include("header.php");  
?>
<!--About us-->
<div class="container-fluid inner_title">
  <div class="row">
    <div class="col-md-12">
      <h2>Contact Us</h2>
    </div>
  </div>
</div>
<style>
.errorClass{
	color:red; 
	position:absolute;	
	font-size:12px;
}
</style>
<!--About us End-->
<div class="container-fluid inner contact_form">
  <div class="row">
    <div class="col-md-6">
      <ul>
        <li>
          <label>Your Name*</label>
          <input name="name" id="name" class="form-control" type="text" tabindex="1">
           <span class="errorClass" id="name_error" style="display:none;">The Name is blank. Please write Name.</span>
        </li>
        <li>
          <label>Phone*</label><br>
           
          <span style="position:absolute; clear:both; padding-left:20px; padding-top:10px;"> +</span> <input name="phone" id="phone" class="form-control" type="text" tabindex="2" style="padding-left:30px;">
           <span class="errorClass" id="phone_error" style="display:none;">The Phone is blank. Please write Phone Number.</span>
        </li>
        <li>
          <label>Email*</label>
          <input name="email_id" id="email_id" class="form-control" type="text" tabindex="3">
           <span class="errorClass" id="email_error" style="display:none;">The Email Id is blank. Please write  Email Id.</span>
        </li>
        <li>
          <label>Company</label>
          <input name="company" id="company" class="form-control" type="text" tabindex="4">
           
        </li>
        <li>
          <label>Subject</label>
          <input name="subject" id="subject" class="form-control" type="text" tabindex="5">
        </li>
        <li>
          <label>Your Question/Comments</label>
          <textarea  name="enquiry" id="enquiry"  class="form-control text_area" tabindex="6"></textarea>
           <span class="errorClass" id="enquiry_error" style="display:none;">The Question/Comments is blank. Please write Question/Comments.</span>
        </li>
      </ul>
      <button type="button" class="btn btn-primary" onclick="ajax_validate()" tabindex="7">Submit</button>
    </div>
    <div class="col-md-6"> <div class="adress"><strong>Timeline Technology Solutions</strong>
      <div>
        <p>P.O.Box: 96641<br>
          Dubai U.A.E. </p>
      </div>
      <div></div>
      <div class="glyphicon glyphicon-earphone"></div>
      <span class="color_text">+ 97142697600</span>&nbsp;&nbsp;
      <div class="glyphicon glyphicon-envelope"></div>
      <span class="color_text"><a href="">info@timeline.ae</a></span>
      </div>
      <div> 
<!--        <iframe name="iframe" id="iframe"   src="http://timeline.ae/sifco/map.php?lat=25.2529755&amp;long=55.3304761" scrolling="no" frameborder="0" height="195" width="100%"></iframe>
-->       <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d57736.81063324813!2d55.33474600000001!3d25.25205499999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x149dc9480acb03a9!2sCity+Avenue+Building!5e0!3m2!1sen!2sae!4v1497350353250" height="195" width="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
    <div class="adress"><strong>Aqdar</strong>
      <div>
        <p>P.O.Box: 131189<br>
         Abu Dhabi U.A.E</p>
      </div>
      <div></div>
      <div class="glyphicon glyphicon-earphone"></div>
      <span class="color_text"> + 97126431797</span>&nbsp;&nbsp;
      <div class="glyphicon glyphicon-envelope"></div>
      <span class="color_text"><a href="">info@kpoint.ae</a></span>
      </div>
      <div>
        <iframe name="iframe" id="iframe"  src="http://timeline.ae/sifco/map.php?lat=15.248572&amp;long=45.321294" scrolling="no" frameborder="0" height="195" width="100%"></iframe>
      </div>
    </div>
    
  </div>
  <div class="row">
    <div class="col-md-12 button_sbmit">
      
    </div>
  </div>
</div>
</div>
<script language="javascript">

	function ajax_validate() {
		if (validate_name() == false || validate_phone() == false || validate_email() == false || validate_enquiry() == false ) 
		{
			return false;
		} 
		else {
			
			ajax_submit();
		}
	}
	
     function validate_name() {
		var regExp = / /g;
		var str = $("#name").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
		    $("#name_error").show();
			$("#name").val('');
			$("#name").focus();
		return false;
		}
	    $("#name_error").hide();
	}

	function validate_phone() {
		
		var strPhone = $.trim($("#phone").val());
		if($.trim(strPhone)== "") {
			$("#phone_error").show();
			return false;
		}else{
				if (strPhone.length < 12 || strPhone.length > 12) {
					$("#phone_error").show();
					//my_alert("Please enter valid mobile number.");
					$("#phone").focus();
					return false;
			    }

				for (var i = 0; i < strPhone.length; i++) {
				var ch = strPhone.substring(i, i + 1);
					if  (ch < "0" || "9" < ch) {
						/*my_alert("The mobile number in digits only, Please re-enter your valid mobile number");*/
						$("#phone_error").show();
						$("#phone").focus();
						return false;
					}
				}
		}
	 $("#phone_error").hide();
	 return true;
	}


    function validate_email() {
		var regExp = / /g;
		var str = $("#email_id").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			 $("#email_error").show();
			 $("#email_id").focus();
		     return false;	
		}
		if (!isNaN(str)) {
			$("#email_error").show();
			$("#email_id").focus();
			$("#email_id").select();
			return false;
		}

		if(str.indexOf('@', 0) == -1) {
			$("#email_error").show();
			$("#email_id").focus();
			$("#email_id").select();
			return false;
		}
     $("#email_error").hide();
	 return true;
	}

	
	function validate_enquiry() {
		var regExp = / /g;
		var str = $("#enquiry").val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			$("#enquiry_error").show();
		     return false;	
		}
		if (str.length < 6 ) {
			$("#enquiry_error").show();
			$("#enquiry").focus();
			return false;
		}
	
	    $("#enquiry_error").hide();
	   return true;
	 }
	 

	function ajax_submit()
	{
		
		$.ajax({
				type: "POST",
				url: "<?=HOST_URL?>/<?=LAN_SEL?>/school_web/contact_us/",
				data: {
					name    		: $('#name').val(),
					phone        	: $('#phone').val(),
					email_id        : $('#email_id').val(),
					company         : $('#company').val(),
					subject 		: $('#subject').val(),
					enquiry         : $('#enquiry').val(),
					is_ajax: true
				},
				success: function(data) {
					var temp = new String();
					temp = data;
					temp = temp.trim();
					if (temp=='Y') {
						alert("Your Question/Comments sent successfully. We will contact you later.");
						$("#pre-loader").hide();
						location.reload();
					}else{
						
						refresh_page = "N";
						alert("Your Question/Comments couldn't sent.");
						$("#pre-loader").hide();
					}
					
				},
				error: function() {
					$("#pre-loader").hide();
				}, 
				complete: function() {
					$("#pre-loader").hide();
				}
			});
	
	}
 
   /****************************************************/
   /****************************************************/

  
</script>

<!--Posts-->

<!--Posts end--> 
<?php include("footer.php");  ?>
