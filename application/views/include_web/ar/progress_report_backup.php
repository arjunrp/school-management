<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>Responsive Resume/CV Template for Developers</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Responsive HTML5 Resume/CV Template for Developers">
    <meta name="author" content="Xiaoying Riley at 3rd Wave Media">    
    <link rel="shortcut icon" href="favicon.ico">  
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,400italic,300italic,300,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <!-- Global CSS -->
  <link href="<?=CSS_WEB_PATH?>/bootstrap.min.css" type="text/css" rel="stylesheet" />

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    
    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="<?=CSS_WEB_PATH?>/progress_report/styles.css">
    <link rel="stylesheet" href="<?=HOST_URL?>/assets/admin/plugins/morris/morris.css">
    <link rel="stylesheet" href="<?=HOST_URL?>/assets/admin/dist/css/AdminLTE.min.css"> 


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head> 

<body>
<style>
.img-circle {
    border-radius: 50%;
}
.score_box {
    width: 40px;
    height: 40px;
    border-radius: 50px;
    text-align: center;
    padding-top: 8px;
    color: #FFFFFF;
}
</style>
<?php  				$name          		    = $student_obj[0]['first_name']." ".$student_obj[0]['last_name'];
					$name_ar       			= $student_obj[0]['first_name_ar']." ".$student_obj[0]['last_name_ar'];
					$mobile        		    = $student_obj[0]['mobile'];
		            $dob_month     		    = $student_obj[0]['dob_month'];
		            $dob_day       			= $student_obj[0]["dob_day"];
		            $dob_year               = $student_obj[0]["dob_year"];
		            $gender                 = ucfirst($student_obj[0]["gender"]);
					
		            $email                  = $student_obj[0]["email"];
		            $country                = $student_obj[0]["country"];
		            $tbl_class_id           = $student_obj[0]["tbl_class_id"];
		            $picture                = $student_obj[0]["file_name_updated"];
		            $added_date             = $student_obj[0]["added_date"];
		            $is_active              = $student_obj[0]["is_active"];
					$student_emirates_id 	= $student_obj[0]["student_emirates_id"];
					$student_user_id 		= $student_obj[0]["student_user_id"];
					$student_pass_code 		= base64_decode($student_obj[0]["student_pass_code"]);
					
		            $emirates_id_father     = $student_obj[0]["emirates_id_father"];
		            $emirates_id_mother     = $student_obj[0]["emirates_id_mother"];
                    $parent_name             = $student_obj[0]["parent_first_name"]." ".$student_obj[0]["parent_last_name"];
		            $parent_name_ar          = $student_obj[0]["parent_first_name_ar"]." ".$student_obj[0]["parent_last_name_ar"];
		            $parent_gender 		   = ucfirst($student_obj[0]["parent_gender"]);
					if($parent_gender=="Male")
					{
						$parent_gender = "Father";
					}else if($parent_gender=="Female"){
						$parent_gender = "Mother";
					}
		            $parent_user_id 		  = $student_obj[0]["parent_user_id"];
					$parent_pass_code 		= base64_decode($student_obj[0]["parent_pass_code"]);
					
					$parent_emirates_id 	  = $student_obj[0]["parent_emirates_id"];
					$parent_mobile 	       = $student_obj[0]["parent_mobile"];
					$parent_email 	        = $student_obj[0]["parent_email"];
					
					$school_type 	         = $student_obj[0]["school_type"];
					$school_type_ar 	      = $student_obj[0]["school_type_ar"];
					
					if($student_obj[0]["parent_dob_year"]<>""){
						$parent_dob 	          = $student_obj[0]["parent_dob_year"]."-".$student_obj[0]["parent_dob_month"]."-".$student_obj[0]["parent_dob_day"];
						$parent_dob             = date('M d, Y',strtotime($parent_dob));
					}else{
						$parent_dob              =  "NA";
					}
		            $parent_password 		 = base64_decode($student_obj[0]["parent_password"]);

		            $class_name              = $student_obj[0]["class_name"]." ".$student_obj[0]["section_name"];
		            $class_name_ar           = $student_obj[0]["class_name_ar"]." ".$student_obj[0]["section_name_ar"];

                    if($student_obj[0]["dob_year"]<>""){
						$dob_date                =  $dob_year."-".$dob_month."-".$dob_day;
                    	$dob_date                = date('M d, Y',strtotime($dob_date));
					}else{
						$dob_date                = "NA";
					}

					if($picture<>"")
					{
						$picture_path = IMG_PATH_STUDENT."/".$picture;
					}else{
						$picture_path = IMG_PATH_STUDENT."/no_img.png";
					}
					?>
    <div class="wrapper">
        <div class="sidebar-wrapper">
            <div class="profile-container">
                <img class="profile img-circle" src="<?=$picture_path?>" alt="" height="100" width="100" />
                <h2 class="name container-block-title"><?=ucfirst($name)?></h1>
                <h3 class="tagline"><?=$class_name?></h3>
            </div><!--//profile-container-->
            
            <div class="contact-container container-block">
                <ul class="list-unstyled contact-list">
                    <?php if($email<>""){ ?>
                    <li class="email"><i class="fa fa-envelope"></i><a href="mailto: <?=$email?>"><?=$email?></a></li>
                    <?php } ?>
                    <?php if($mobile<>""){ ?>
                    <li class="phone"><i class="fa fa-phone"></i><a href="tel:<?=$mobile?>"><?=$mobile?></a></li>
                     <?php } ?>
                    <li>DOB:&nbsp;<?=$dob_date?></li> 
                    <li>Gender:&nbsp;<?=$gender?></li> 
                    <li>Emirates Id:&nbsp;<?=$student_emirates_id?></li> 
                    <li>Nationality:&nbsp;<?=$country?></li> 
                     
                     
                    <li class="website"><i class="fa fa-globe"></i><a href="http://themes.3rdwavemedia.com/website-templates/free-responsive-website-template-for-developers/" target="_blank">portfoliosite.com</a></li>
                    <li class="linkedin"><i class="fa fa-linkedin"></i><a href="#" target="_blank">linkedin.com/in/alandoe</a></li>
                    <li class="github"><i class="fa fa-github"></i><a href="#" target="_blank">github.com/username</a></li>
                    <li class="twitter"><i class="fa fa-twitter"></i><a href="https://twitter.com/3rdwave_themes" target="_blank">@twittername</a></li>
                </ul>
            </div><!--//contact-container-->
            <div class="education-container container-block">
                <h2 class="container-block-title">Education</h2>
                <div class="item">
                    <h4 class="degree">MSc in Computer Science</h4>
                    <h5 class="meta">University of London</h5>
                    <div class="time">2011 - 2012</div>
                </div><!--//item-->
                <div class="item">
                    <h4 class="degree">BSc in Applied Mathematics</h4>
                    <h5 class="meta">Bristol University</h5>
                    <div class="time">2007 - 2011</div>
                </div><!--//item-->
            </div><!--//education-container-->
            
            <div class="languages-container container-block">
                <h2 class="container-block-title">Languages</h2>
                <ul class="list-unstyled interests-list">
                    <li>English <span class="lang-desc">(Native)</span></li>
                    <li>French <span class="lang-desc">(Professional)</span></li>
                    <li>Spanish <span class="lang-desc">(Professional)</span></li>
                </ul>
            </div><!--//interests-->
            
            <div class="interests-container container-block">
                <h2 class="container-block-title">Interests</h2>
                <ul class="list-unstyled interests-list">
                    <li>Climbing</li>
                    <li>Snowboarding</li>
                    <li>Cooking</li>
                </ul>
            </div><!--//interests-->
            
        </div><!--//sidebar-wrapper-->
        
        <div class="main-wrapper">
            
          <!--  <section class="section summary-section">
                <h2 class="section-title"><i class="fa fa-user"></i>Overview</h2>
                <div class="summary">
                    <p>Summarise your career here lorem ipsum dolor sit amet, consectetuer adipiscing elit. You can <a href="http://themes.3rdwavemedia.com/website-templates/orbit-free-resume-cv-template-for-developers/" target="_blank">download this free resume/CV template here</a>. Aenean commodo ligula eget dolor aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu.</p>
                </div>
            </section>-->
        
          <?php $colorCode = array("#f56954","#00a65a","#f39c12","#00c0ef","#3c8dbc","#d2d6de","#f56954","#00a65a","#f39c12","#00c0ef","#3c8dbc","#d2d6de"); ?>
        
        
         <?php  if(!empty($performance_activities)) { ?>
            <section class="skills-section section">
                <h2 class="section-title"><i class="fa fa-rocket"></i>Skills &amp; Proficiency</h2>
                <div class="skillset">        
                  <?php for($p=0;$p<count($performance_activities);$p++){ ?> 
                    <div class="item">
                        <h3 class="level-title"><?php echo $performance_activities[$p]['title']; ?></h3>
                        <div class="level-bar">
                            <div class="level-bar-inner" style="background-color:<?=$colorCode[$p]?>;" data-level="<?php echo $performance_activities[$p]['performance_value']; ?>%" >
							<span style="padding-left:100%;  position:relative; color:<?=$colorCode[$p]?>;" > <?php echo $performance_activities[$p]['performance_value']; ?>%</span>  <!--color:#F60;-->
                            </div> 
                        </div><!--//level-bar-->                                 
                    </div><!--//item-->
                    <?php } ?>
                </div>  
            </section><!--//skills-section-->
         <?php } ?>   
         
         
           <div class="box box-primary">
            <div class="box-header with-border">
               <?php if(LAN_SEL=="ar"){?> 
                    <h3 class="box-title">  الإستفسارات والردود </h3>
               <?php }else{ ?>
                     <h3 class="box-title"><!--Area Chart - -->Skills &amp; Proficiency vs Months</h3>
              <?php } ?>
              <div class="box-tools pull-right">
               <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
              </div>
            </div>
            <div class="box-body">
                <canvas id="areaChart" style="height:250px"></canvas>
            </div>
            <!-- /.box-body -->
         </div>
         
        
      
            
            <?php  if(!empty($rs_student_attendance)) { ?>
            <section class="skills-section section">
                <h2 class="section-title"><i class="fa fa-calendar"></i>Attendance</h2>
                <div class="skillset">        
                     
                     
                     
                     <div style="float:left; width:40%" >
                        <div  class="item" style="float:left;">
						<h3 style="font-size:14px;">Total Working Days</h3>
						</div>
                         <div style="float:left; padding:10px;">
                            <div class="score_box" style="background-color:#2d7788;"><?=$rs_student_attendance['total_count']; ?></div>
                         </div><!--//level-bar-->       
                    </div><!--//item-->
                    
                    <div style="float:left; width:30%">
                        <div class="item"  style="float:left;">
						<h3 style="font-size:14px;" >Total Present</h3>
						</div>
                        <div style="float:left; padding:10px;">
                            <div class="score_box" style="background-color:#0a7429;"><?=$rs_student_attendance['present_count']; ?></div>
                         </div> <!--//level-bar-->       
                    </div>
                    
                    <div style="float:left; width:30%">
                        <div style="float:left;">
						<h3 style="font-size:14px;" >Total Absent</h3>
						</div>
                        <div style="float:left; padding:10px;">
                             <div class="score_box" style="background-color:#fa2307;"><?=$rs_student_attendance['absent_count']; ?></div>
                         </div> <!--//level-bar-->       
                    </div>
                </div>  
                <div style="clear:both;"></div>
            </section><!--//skills-section-->
         <?php } ?> 
         
          <div class="box box-primary">
          <div class="box-header with-border">
              <h3 class="box-title">Attendance vs  Months</h3>

              <div class="box-tools pull-right">
             <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
              </div>
            </div>
            <div class="box-body">
                <canvas id="barChart" style="height: 186px; width: 645px;" height="186" width="645"></canvas>
            </div>
          </div>
            
         
           
            
         <?php  if(!empty($rs_student_point)) { ?>
            <section class="skills-section section">
                <h2 class="section-title"><i class="fa fa-star-o"></i>Points Earned</h2>
                <div class="skillset">        
                  <?php for($s=0;$s<count($rs_student_point);$s++){ ?> 
                     <div class="item">
                        <div width="30%" style="float:left; padding-right:30px;">
						<div class="score_box" style="background-color:#2d7788;"><?=$rs_student_point[$s]["student_point"]?></div>
						</div>
                        <div width="70%" style="float:left; padding-top:10px;">
							<?php echo $rs_student_point[$s]['comments_student']; ?>
                         </div> <!--//level-bar-->       
                    </div><!--//item-->
                    <?php } ?>
                </div>  
            </section><!--//skills-section-->
         <?php } ?>   
            
             <div class="box box-primary">
            <div class="box-header with-border">
               <?php if(LAN_SEL=="ar"){?> 
                    <h3 class="box-title">  الإستفسارات والردود </h3>
               <?php }else{ ?>
                     <h3 class="box-title">Area Chart - Earned Points vs Months</h3>
              <?php } ?>
              <div class="box-tools pull-right">
               <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
              </div>
            </div>
            <div class="box-body">
                <canvas id="areaChart2" style="height:250px"></canvas>
            </div>
            <!-- /.box-body -->
         </div>
         
         
          <div class="box box-primary">
            <div class="box-header with-border">
               <?php if(LAN_SEL=="ar"){?> 
                    <h3 class="box-title">  الإستفسارات والردود </h3>
               <?php }else{ ?>
                     <h3 class="box-title">Line Chart - Earned Points vs Months</h3>
              <?php } ?>
              <div class="box-tools pull-right">
               <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
              </div>
            </div>
            <div class="box-body">
                <canvas id="lineChart" style="height:250px"></canvas>
            </div>
            <!-- /.box-body -->
         </div>
         
         
           <div class="box box-primary">
          <div class="box-header with-border">
              <h3 class="box-title">Bar Chart - Earned Points vs Months</h3>

              <div class="box-tools pull-right">
             <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
              </div>
            </div>
            <div class="box-body">
                <canvas id="barChart2" style="height: 186px; width: 645px;" height="186" width="645"></canvas>
            </div>
          </div>
         
         
            
             <?php  if(!empty($cards)) { ?>
            <section class="skills-section section">
                <h2 class="section-title"><i class="fa fa-file "></i>Cards &amp; Issued</h2>
                <div class="skillset">        
                  <?php for($c=0;$c<count($cards);$c++){ ?> 
                     <div class="item">
                        <div width="30%" style="float:left; padding-right:30px;">
						<div class="score_box" style="background-color:<?=$cards[$c]["color"]?>"><?=abs($cards[$c]["score"])?></div>
						</div>
                        <div width="70%" style="float:left; padding-top:10px;">
							<?php echo $cards[$c]['title']; ?>
                         </div> <!--//level-bar-->       
                    </div><!--//item-->
                    <?php } ?>
                </div>  
            </section><!--//skills-section-->
         <?php } ?>   
              
   
            
           <!-- <i class="fa fa-briefcase"></i>-->
           <!-- <i class="fa fa-archive"></i> -->
         
       
           
        </div><!--//main-body-->
    </div>
 
  <!--  <footer class="footer">
        <div class="text-center">
                <small class="copyright">Designed with <i class="fa fa-heart"></i> by <a href="http://themes.3rdwavemedia.com" target="_blank">Xiaoying Riley</a> for developers</small>
        </div>
    </footer>-->
 
    <!-- Javascript -->          
    <script type="text/javascript" src="<?=JS_WEB_PATH?>/plugins/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="<?=JS_WEB_PATH?>/plugins/bootstrap/js/bootstrap.min.js"></script>    
    <script type="text/javascript" src="<?=JS_WEB_PATH?>/plugins/main.js"></script>    
    <!-- Charts -->
    <script src="<?=HOST_URL?>/assets/admin/plugins/chartjs/Chart.min.js"></script>
    <script>
 	 $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

     //-------------
    //- BAR CHART -
    //-------------
	
	
	
		var areaChartData1 = {
      labels: ["<?=$months['11']?>", "<?=$months['10']?>", "<?=$months['9']?>", "<?=$months['8']?>", "<?=$months['7']?>", "<?=$months['6']?>", "<?=$months['5']?>", "<?=$months['4']?>", "<?=$months['3']?>", "<?=$months['2']?>", "<?=$months['1']?>", "<?=$months['0']?>"],
      datasets: [
			 <?php if($rs_student_attendance_graph[11]['total_count']=='1000'){ $total_month_11 = ''; }else{ $total_month_11= $rs_student_attendance_graph[11]['total_count']; } 
		      if($rs_student_attendance_graph[10]['total_count']=='1000'){ $total_month_10 = ''; }else{ $total_month_10 	= $rs_student_attendance_graph[10]['total_count']; }  
			  if($rs_student_attendance_graph[9]['total_count']=='1000'){ $total_month_9   = ''; }else{ $total_month_9 	    = $rs_student_attendance_graph[9]['total_count']; }  
			  if($rs_student_attendance_graph[8]['total_count']=='1000'){ $total_month_8 = ''; }else{ $total_month_8 		= $rs_student_attendance_graph[8]['total_count']; }  
			  if($rs_student_attendance_graph[7]['total_count']=='1000'){ $total_month_7 = ''; }else{ $total_month_7 		= $rs_student_attendance_graph[7]['total_count']; }  
			  if($rs_student_attendance_graph[6]['total_count']=='1000'){ $total_month_6 = ''; }else{ $total_month_6 		= $rs_student_attendance_graph[6]['total_count']; }  
			  if($rs_student_attendance_graph[5]['total_count']=='1000'){ $total_month_5 = ''; }else{ $total_month_5 		= $rs_student_attendance_graph[5]['total_count']; }  
			  if($rs_student_attendance_graph[4]['total_count']=='1000'){ $total_month_4 = ''; }else{ $total_month_4		= $rs_student_attendance_graph[4]['total_count']; }  
			  if($rs_student_attendance_graph[3]['total_count']=='1000'){ $total_month_3 = ''; }else{ $total_month_3 		= $rs_student_attendance_graph[3]['total_count']; }  
			  if($rs_student_attendance_graph[2]['total_count']=='1000'){ $total_month_2 = ''; }else{ $total_month_2 		= $rs_student_attendance_graph[2]['total_count']; }  
			  if($rs_student_attendance_graph[1]['total_count']=='1000'){ $total_month_1 = ''; }else{ $total_month_1 		= $rs_student_attendance_graph[1]['total_count']; }  
			  if($rs_student_attendance_graph[0]['total_count']=='1000'){ $total_month_0 = ''; }else{ $total_month_0 		= $rs_student_attendance_graph[0]['total_count']; }  
			  
			  if($rs_student_attendance_graph[11]['present_count']=='1000'){ $present_month_11 = ''; }else{ $present_month_11   = $rs_student_attendance_graph[11]['present_count']; } 
		      if($rs_student_attendance_graph[10]['present_count']=='1000'){ $present_month_10 = ''; }else{ $present_month_10 	= $rs_student_attendance_graph[10]['present_count']; }  
			  if($rs_student_attendance_graph[9]['present_count']=='1000'){ $present_month_9   = ''; }else{ $present_month_9 	= $rs_student_attendance_graph[9]['present_count']; }  
			  if($rs_student_attendance_graph[8]['present_count']=='1000'){ $present_month_8 = ''; }else{ $present_month_8 		= $rs_student_attendance_graph[8]['present_count']; }  
			  if($rs_student_attendance_graph[7]['present_count']=='1000'){ $present_month_7 = ''; }else{ $present_month_7 		= $rs_student_attendance_graph[7]['present_count']; }  
			  if($rs_student_attendance_graph[6]['present_count']=='1000'){ $present_month_6 = ''; }else{ $present_month_6 		= $rs_student_attendance_graph[6]['present_count']; }  
			  if($rs_student_attendance_graph[5]['present_count']=='1000'){ $present_month_5 = ''; }else{ $present_month_5 		= $rs_student_attendance_graph[5]['present_count']; }  
			  if($rs_student_attendance_graph[4]['present_count']=='1000'){ $present_month_4 = ''; }else{ $present_month_4		= $rs_student_attendance_graph[4]['present_count']; }  
			  if($rs_student_attendance_graph[3]['present_count']=='1000'){ $present_month_3 = ''; }else{ $present_month_3 		= $rs_student_attendance_graph[3]['present_count']; }  
			  if($rs_student_attendance_graph[2]['present_count']=='1000'){ $present_month_2 = ''; }else{ $present_month_2 		= $rs_student_attendance_graph[2]['present_count']; }  
			  if($rs_student_attendance_graph[1]['present_count']=='1000'){ $present_month_1 = ''; }else{ $present_month_1 		= $rs_student_attendance_graph[1]['present_count']; }  
			  if($rs_student_attendance_graph[0]['present_count']=='1000'){ $present_month_0 = ''; }else{ $present_month_0 		= $rs_student_attendance_graph[0]['present_count']; }  
			  
			    if($rs_student_attendance_graph[11]['absent_count']=='1000'){ $absent_month_11 = ''; }else{ $absent_month_11    = $rs_student_attendance_graph[11]['absent_count']; } 
		      if($rs_student_attendance_graph[10]['absent_count']=='1000'){ $absent_month_10 = ''; }else{ $absent_month_10 	    = $rs_student_attendance_graph[10]['absent_count']; }  
			  if($rs_student_attendance_graph[9]['absent_count']=='1000'){ $absent_month_9   = ''; }else{ $absent_month_9 	    = $rs_student_attendance_graph[9]['absent_count']; }  
			  if($rs_student_attendance_graph[8]['absent_count']=='1000'){ $absent_month_8 = ''; }else{ $absent_month_8 		= $rs_student_attendance_graph[8]['absent_count']; }  
			  if($rs_student_attendance_graph[7]['absent_count']=='1000'){ $absent_month_7 = ''; }else{ $absent_month_7 		= $rs_student_attendance_graph[7]['absent_count']; }  
			  if($rs_student_attendance_graph[6]['absent_count']=='1000'){ $absent_month_6 = ''; }else{ $absent_month_6 		= $rs_student_attendance_graph[6]['absent_count']; }  
			  if($rs_student_attendance_graph[5]['absent_count']=='1000'){ $absent_month_5 = ''; }else{ $absent_month_5 		= $rs_student_attendance_graph[5]['absent_count']; }  
			  if($rs_student_attendance_graph[4]['absent_count']=='1000'){ $absent_month_4 = ''; }else{ $absent_month_4		    = $rs_student_attendance_graph[4]['absent_count']; }  
			  if($rs_student_attendance_graph[3]['absent_count']=='1000'){ $absent_month_3 = ''; }else{ $absent_month_3 		= $rs_student_attendance_graph[3]['absent_count']; }  
			  if($rs_student_attendance_graph[2]['absent_count']=='1000'){ $absent_month_2 = ''; }else{ $absent_month_2 		= $rs_student_attendance_graph[2]['absent_count']; }  
			  if($rs_student_attendance_graph[1]['absent_count']=='1000'){ $absent_month_1 = ''; }else{ $absent_month_1 		= $rs_student_attendance_graph[1]['absent_count']; }  
			  if($rs_student_attendance_graph[0]['absent_count']=='1000'){ $absent_month_0 = ''; }else{ $absent_month_0 		= $rs_student_attendance_graph[0]['absent_count']; }  
			  
		
		  ?>
		{
		  label: "Total Working Days",
          fillColor: "#2d7788",
          strokeColor: "#2d7788",
          pointColor: "#2d7788",
          pointStrokeColor: "#2d7788",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "#2d7788",
          data: [<?=$total_month_11?>,
		        <?=$total_month_10?>,
				<?=$total_month_9?>,
				<?=$total_month_8?>,
				<?=$total_month_7?>,
				<?=$total_month_6?>,
				<?=$total_month_5?>,
				<?=$total_month_4?>,
				<?=$total_month_3?>,
				<?=$total_month_2?>,
				<?=$total_month_1?>,
				<?=$total_month_0?>]
        },
		{
		  label: "Total Presents",
          fillColor: "#0a7429",
          strokeColor: "#0a7429",
          pointColor: "#0a7429",
          pointStrokeColor: "#0a7429",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "#0a7429",
          data: [<?=$present_month_11?>,
		        <?=$present_month_10?>,
				<?=$present_month_9?>,
				<?=$present_month_8?>,
				<?=$present_month_7?>,
				<?=$present_month_6?>,
				<?=$present_month_5?>,
				<?=$present_month_4?>,
				<?=$present_month_3?>,
				<?=$present_month_2?>,
				<?=$present_month_1?>,
				<?=$present_month_0?>]
        },
		{
		  label: "Total Absents",
          fillColor: "#fa2307",
          strokeColor: "#fa2307",
          pointColor: "#fa2307",
          pointStrokeColor: "#fa2307",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "#fa2307",
          data: [<?=$absent_month_11?>,
		        <?=$absent_month_10?>,
				<?=$absent_month_9?>,
				<?=$absent_month_8?>,
				<?=$absent_month_7?>,
				<?=$absent_month_6?>,
				<?=$absent_month_5?>,
				<?=$absent_month_4?>,
				<?=$absent_month_3?>,
				<?=$absent_month_2?>,
				<?=$absent_month_1?>,
				<?=$absent_month_0?>]
        }
      ]
    };

	
    var barChartCanvas = $("#barChart").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas);
    var barChartData = areaChartData1;
    barChartData.datasets[1].fillColor = "#00a65a";
    barChartData.datasets[1].strokeColor = "#00a65a";
    barChartData.datasets[1].pointColor = "#00a65a";
    var barChartOptions = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: true,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - If there is a stroke on each bar
      barShowStroke: true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth: 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing: 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing: 1,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to make the chart responsive
      responsive: true,
      maintainAspectRatio: true
    };

    barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions);
	
	
	<?php /*?>	var areaChartData1 = {
      labels: ["<?=$months['11']?>", "<?=$months['10']?>", "<?=$months['9']?>", "<?=$months['8']?>", "<?=$months['7']?>", "<?=$months['6']?>", "<?=$months['5']?>", "<?=$months['4']?>", "<?=$months['3']?>", "<?=$months['2']?>", "<?=$months['1']?>", "<?=$months['0']?>"],
      datasets: [
        <?php for($m=0;$m<count($performance_graph);$m++){ 
		      $performance_array = array();
		      $performance_array = $performance_graph[$m]['performance_data'];
			  
			  if($performance_array[11]['performance_value']=='1000'){ $performance_value_11 = ''; }else{ $performance_value_11 = $performance_array[11]['performance_value']; } 
		      if($performance_array[10]['performance_value']=='1000'){ $performance_value_10 = ''; }else{ $performance_value_10 = $performance_array[10]['performance_value']; }  
			  if($performance_array[9]['performance_value']=='1000'){ $performance_value_9   = ''; }else{ $performance_value_9 = $performance_array[9]['performance_value']; }  
			  if($performance_array[8]['performance_value']=='1000'){ $performance_value_8 = ''; }else{ $performance_value_8 = $performance_array[8]['performance_value']; }  
			  if($performance_array[7]['performance_value']=='1000'){ $performance_value_7 = ''; }else{ $performance_value_7 = $performance_array[7]['performance_value']; }  
			  if($performance_array[6]['performance_value']=='1000'){ $performance_value_6 = ''; }else{ $performance_value_6 = $performance_array[6]['performance_value']; }  
			  if($performance_array[5]['performance_value']=='1000'){ $performance_value_5 = ''; }else{ $performance_value_5 = $performance_array[5]['performance_value']; }  
			  if($performance_array[4]['performance_value']=='1000'){ $performance_value_4 = ''; }else{ $performance_value_4 = $performance_array[4]['performance_value']; }  
			  if($performance_array[3]['performance_value']=='1000'){ $performance_value_3 = ''; }else{ $performance_value_3 = $performance_array[3]['performance_value']; }  
			  if($performance_array[2]['performance_value']=='1000'){ $performance_value_2 = ''; }else{ $performance_value_2 = $performance_array[2]['performance_value']; }  
			  if($performance_array[1]['performance_value']=='1000'){ $performance_value_1 = ''; }else{ $performance_value_1 = $performance_array[1]['performance_value']; }  
			  if($performance_array[0]['performance_value']=='1000'){ $performance_value_0 = ''; }else{ $performance_value_0 = $performance_array[0]['performance_value']; }  
		
		  ?>
		{
		  label: "<?=$performance_graph[$m]['topic_en']?>",
          fillColor: "<?=$colorCode[$m]?>",
          strokeColor: "<?=$colorCode[$m]?>",
          pointColor: "<?=$colorCode[$m]?>",
          pointStrokeColor: "<?=$colorCode[$m]?>",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "<?=$colorCode[$m]?>",
          data: [<?=$performance_value_11?>,
		        <?=$performance_value_10?>,
				<?=$performance_value_9?>,
				<?=$performance_value_8?>,
				<?=$performance_value_7?>,
				<?=$performance_value_6?>,
				<?=$performance_value_5?>,
				<?=$performance_value_4?>,
				<?=$performance_value_3?>,
				<?=$performance_value_2?>,
				<?=$performance_value_1?>,
				<?=$performance_value_0?>]
        }
		<?php if($m<>count($performance_graph)-1){?>
		,
		<?php } 
		} ?>
      ]
    };

	
    var barChartCanvas = $("#barChart").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas);
    var barChartData = areaChartData1;
    barChartData.datasets[1].fillColor = "#00a65a";
    barChartData.datasets[1].strokeColor = "#00a65a";
    barChartData.datasets[1].pointColor = "#00a65a";
    var barChartOptions = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: true,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - If there is a stroke on each bar
      barShowStroke: true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth: 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing: 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing: 1,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to make the chart responsive
      responsive: true,
      maintainAspectRatio: true
    };

    barChartOptions.datasetFill = false;
    barChart.Bar(barChartData, barChartOptions);<?php */?>




    //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var areaChart = new Chart(areaChartCanvas);
  

	var areaChartData = {
      labels: ["<?=$months['11']?>", "<?=$months['10']?>", "<?=$months['9']?>", "<?=$months['8']?>", "<?=$months['7']?>", "<?=$months['6']?>", "<?=$months['5']?>", "<?=$months['4']?>", "<?=$months['3']?>", "<?=$months['2']?>", "<?=$months['1']?>", "<?=$months['0']?>"],
      datasets: [
        <?php for($m=0;$m<count($performance_graph);$m++){ 
		      $performance_array = array();
		      $performance_array = $performance_graph[$m]['performance_data'];
			  
			  if($performance_array[11]['performance_value']=='1000'){ $performance_value_11 = ''; }else{ $performance_value_11 = $performance_array[11]['performance_value']; } 
		      if($performance_array[10]['performance_value']=='1000'){ $performance_value_10 = ''; }else{ $performance_value_10 = $performance_array[10]['performance_value']; }  
			  if($performance_array[9]['performance_value']=='1000'){ $performance_value_9   = ''; }else{ $performance_value_9 = $performance_array[9]['performance_value']; }  
			  if($performance_array[8]['performance_value']=='1000'){ $performance_value_8 = ''; }else{ $performance_value_8 = $performance_array[8]['performance_value']; }  
			  if($performance_array[7]['performance_value']=='1000'){ $performance_value_7 = ''; }else{ $performance_value_7 = $performance_array[7]['performance_value']; }  
			  if($performance_array[6]['performance_value']=='1000'){ $performance_value_6 = ''; }else{ $performance_value_6 = $performance_array[6]['performance_value']; }  
			  if($performance_array[5]['performance_value']=='1000'){ $performance_value_5 = ''; }else{ $performance_value_5 = $performance_array[5]['performance_value']; }  
			  if($performance_array[4]['performance_value']=='1000'){ $performance_value_4 = ''; }else{ $performance_value_4 = $performance_array[4]['performance_value']; }  
			  if($performance_array[3]['performance_value']=='1000'){ $performance_value_3 = ''; }else{ $performance_value_3 = $performance_array[3]['performance_value']; }  
			  if($performance_array[2]['performance_value']=='1000'){ $performance_value_2 = ''; }else{ $performance_value_2 = $performance_array[2]['performance_value']; }  
			  if($performance_array[1]['performance_value']=='1000'){ $performance_value_1 = ''; }else{ $performance_value_1 = $performance_array[1]['performance_value']; }  
			  if($performance_array[0]['performance_value']=='1000'){ $performance_value_0 = ''; }else{ $performance_value_0 = $performance_array[0]['performance_value']; }  
		
		  ?>
		{
		  label: "<?=$performance_graph[$m]['topic_en']?>",
          fillColor: "<?=$colorCode[$m]?>",
          strokeColor: "<?=$colorCode[$m]?>",
          pointColor: "<?=$colorCode[$m]?>",
          pointStrokeColor: "<?=$colorCode[$m]?>",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "<?=$colorCode[$m]?>",
          data: [<?=$performance_value_11?>,
		        <?=$performance_value_10?>,
				<?=$performance_value_9?>,
				<?=$performance_value_8?>,
				<?=$performance_value_7?>,
				<?=$performance_value_6?>,
				<?=$performance_value_5?>,
				<?=$performance_value_4?>,
				<?=$performance_value_3?>,
				<?=$performance_value_2?>,
				<?=$performance_value_1?>,
				<?=$performance_value_0?>]
        }
		<?php if($m<>count($performance_graph)-1){?>
		,
		<?php } 
		} ?>
      ]
    };
	
	
	

    var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: false,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - Whether the line is curved between points
      bezierCurve: true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension: 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot: false,
      //Number - Radius of each point dot in pixels
      pointDotRadius: 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth: 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius: 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke: true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth: 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true
    };

    //Create the line chart
    areaChart.Line(areaChartData, areaChartOptions);


      // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas = $("#areaChart2").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var areaChart = new Chart(areaChartCanvas);
  

	var areaChartData2 = {
      labels: ["<?=$months['11']?>", "<?=$months['10']?>", "<?=$months['9']?>", "<?=$months['8']?>", "<?=$months['7']?>", "<?=$months['6']?>", "<?=$months['5']?>", "<?=$months['4']?>", "<?=$months['3']?>", "<?=$months['2']?>", "<?=$months['1']?>", "<?=$months['0']?>"],
      datasets: [
        <?php for($m=0;$m<count($rs_student_point_graph);$m++){ 
		      $point_array = array();
		      $point_array = $rs_student_point_graph[$m]['points_data'];
			  
			  if($point_array[11]['total_point']=='1000'){ $total_point_11 = ''; }else{ $total_point_11 = $point_array[11]['total_point']; } 
		      if($point_array[10]['total_point']=='1000'){ $total_point_10 = ''; }else{ $total_point_10 = $point_array[10]['total_point']; }  
			  if($point_array[9]['total_point']=='1000'){ $total_point_9   = ''; }else{ $total_point_9 = $point_array[9]['total_point']; }  
			  if($point_array[8]['total_point']=='1000'){ $total_point_8 = ''; }else{ $total_point_8 = $point_array[8]['total_point']; }  
			  if($point_array[7]['total_point']=='1000'){ $total_point_7 = ''; }else{ $total_point_7 = $point_array[7]['total_point']; }  
			  if($point_array[6]['total_point']=='1000'){ $total_point_6 = ''; }else{ $total_point_6 = $point_array[6]['total_point']; }  
			  if($point_array[5]['total_point']=='1000'){ $total_point_5 = ''; }else{ $total_point_5 = $point_array[5]['total_point']; }  
			  if($point_array[4]['total_point']=='1000'){ $total_point_4 = ''; }else{ $total_point_4 = $point_array[4]['total_point']; }  
			  if($point_array[3]['total_point']=='1000'){ $total_point_3 = ''; }else{ $total_point_3 = $point_array[3]['total_point']; }  
			  if($point_array[2]['total_point']=='1000'){ $total_point_2 = ''; }else{ $total_point_2 = $point_array[2]['total_point']; }  
			  if($point_array[1]['total_point']=='1000'){ $total_point_1 = ''; }else{ $total_point_1 = $point_array[1]['total_point']; }  
			  if($point_array[0]['total_point']=='1000'){ $total_point_0 = ''; }else{ $total_point_0 = $point_array[0]['total_point']; }  
		
		  ?>
		{
		  label: "<?=$rs_student_point_graph[$m]['comments_student']?>",
          fillColor: "<?=$colorCode[$m]?>",
          strokeColor: "<?=$colorCode[$m]?>",
          pointColor: "<?=$colorCode[$m]?>",
          pointStrokeColor: "<?=$colorCode[$m]?>",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "<?=$colorCode[$m]?>",
          data: [<?=$total_point_11?>,
		        <?=$total_point_10?>,
				<?=$total_point_9?>,
				<?=$total_point_8?>,
				<?=$total_point_7?>,
				<?=$total_point_6?>,
				<?=$total_point_5?>,
				<?=$total_point_4?>,
				<?=$total_point_3?>,
				<?=$total_point_2?>,
				<?=$total_point_1?>,
				<?=$total_point_0?>]
        }
		<?php if($m<>count($rs_student_point_graph)-1){?>
		,
		<?php } 
		} ?>
      ]
    };
	
	
	

    var areaChartOptions2 = {
      //Boolean - If we should show the scale at all
      showScale: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: false,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - Whether the line is curved between points
      bezierCurve: true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension: 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot: false,
      //Number - Radius of each point dot in pixels
      pointDotRadius: 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth: 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius: 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke: true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth: 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true
    };

    //Create the line chart
    areaChart.Line(areaChartData2, areaChartOptions2);


    var barChartCanvas = $("#barChart2").get(0).getContext("2d");
    var barChart = new Chart(barChartCanvas);
    var barChartData = areaChartData2;
    barChartData.datasets[1].fillColor = "#00a65a";
    barChartData.datasets[1].strokeColor = "#00a65a";
    barChartData.datasets[1].pointColor = "#00a65a";
    var barChartOptions = {
      //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
      scaleBeginAtZero: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: true,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - If there is a stroke on each bar
      barShowStroke: true,
      //Number - Pixel width of the bar stroke
      barStrokeWidth: 2,
      //Number - Spacing between each of the X value sets
      barValueSpacing: 5,
      //Number - Spacing between data sets within X values
      barDatasetSpacing: 1,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to make the chart responsive
      responsive: true,
      maintainAspectRatio: true
    };

    barChartOptions.datasetFill = false;
    barChart.Bar(areaChartData2, areaChartOptions2);
	









    //-------------
    //- LINE CHART -
    //--------------
    var lineChartCanvas = $("#lineChart").get(0).getContext("2d");
    var lineChart = new Chart(lineChartCanvas);
    var lineChartOptions = areaChartOptions2;
    lineChartOptions.datasetFill = false;
    lineChart.Line(areaChartData2, lineChartOptions);

    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
    var pieChart = new Chart(pieChartCanvas);
    var PieData = [
      {
        value: 700,
        color: "#f56954",
        highlight: "#f56954",
        label: "Chrome"

      },
      {
        value: 500,
        color: "#00a65a",
        highlight: "#00a65a",
        label: "IE"
      },
      {
        value: 400,
        color: "#f39c12",
        highlight: "#f39c12",
        label: "FireFox"
      },
      {
        value: 600,
        color: "#00c0ef",
        highlight: "#00c0ef",
        label: "Safari"
      },
      {
        value: 300,
        color: "#3c8dbc",
        highlight: "#3c8dbc",
        label: "Opera"
      },
      {
        value: 100,
        color: "#d2d6de",
        highlight: "#d2d6de",
        label: "Navigator"
      }
    ];
    var pieOptions = {
      //Boolean - Whether we should show a stroke on each segment
      segmentShowStroke: true,
      //String - The colour of each segment stroke
      segmentStrokeColor: "#fff",
      //Number - The width of each segment stroke
      segmentStrokeWidth: 2,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps: 100,
      //String - Animation easing effect
      animationEasing: "easeOutBounce",
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate: true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale: false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    };
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions);

   
  });
  </script>
</body>
</html> 

