<?php

/**
 * @desc   	  	Help Controller
 *
 * @category   	Controller
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Help extends CI_Controller {


	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
    function index() {
		//Do nothing
	}


	/**
	* @desc    Image Gallery
	*
	* @param   none
	* @access  default
	*/
    function help_file() {
		
		$user_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		
		$data["user_id"] = $user_id;
		$data["role"] = $role;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] = $school_id;
		
		$this->load->model('model_help');
		$data_rs = $this->model_help->get_help_page($school_id);
		if (count($data_rs) <=0) {
			$arr["code"] = "200";
			echo json_encode($arr);
			return;
		}
		$data["data_rs"] = $data_rs;
		$data["page"] = "view_help_file";
		$this->load->view('view_template',$data);	 
	}
	
	function schoolapp_intro(){
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		if($lan=="ar")
			$data["intro_url"]  = HOST_URL."/introduction_ar.html";
		else
			$data["intro_url"]  = HOST_URL."/introduction.html";
			
	    $response = "Success";
		$errorMsg = "Site introduction listed successfully";
		$response_array = array('response' => $response, 'result' => $data, 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
	}
}
?>

