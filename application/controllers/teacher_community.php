<?php

/**
 * @desc   	  	Teacher_community Controller
 * @category   	Controller
 * @version    	0.1
 */
class Teacher_community extends CI_Controller {

	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
    function index() {
		//Do nothing
	}
	
	/************************* Teacher Community Group ****************************/
     function teacher_message_community() {
		 
		$lan 				= $_REQUEST["lan"];
		$device 			= $_REQUEST["device"];
		$device_uid 		= $_REQUEST["device_uid"];
		$tbl_school_id  	= $_REQUEST["tbl_school_id"]; 
		$tbl_teacher_id  	= $_REQUEST["tbl_teacher_id"];
				
		$this->load->model("model_teachers");
		$this->load->model("model_teacher_community");
		$this->load->model("model_message");
		
		$rs_all_teachers     = $this->model_teachers->get_list_teachers($tbl_school_id,'','Y',$tbl_teacher_id);	
		for($d=0;$d<count($rs_all_teachers);$d++)
		{
			if($rs_all_teachers[$d]['file_name_updated']<>"")
				$rs_all_teachers[$d]['file_name_updated'] = IMG_PATH_TEACHER."/".$rs_all_teachers[$d]['file_name_updated'];
			else
				$rs_all_teachers[$d]['file_name_updated'] = "";
		}
		
		$rs_all_groups       = $this->model_teacher_community->list_my_teachers_group($tbl_school_id, $tbl_teacher_id);
			
		for($m=0;$m<count($rs_all_groups); $m++)
		{
			$teachers_list  = $this->model_message->get_teachers_list_in_group($rs_all_groups[$m]['tbl_teacher_group_id'],$tbl_school_id);
			$rs_all_groups[$m]['cntTeacher']      = count($teachers_list);
			
			for($d=0;$d<count($teachers_list);$d++)
			{
				if($teachers_list[$d]['file_name_updated']<>"")
					$teachers_list[$d]['file_name_updated'] = IMG_PATH_TEACHER."/".$teachers_list[$d]['file_name_updated'];
				else
					$teachers_list[$d]['file_name_updated'] = "";
			}
			$rs_all_groups[$m]['teachers_list']   = $teachers_list;
		}
		$data['teachers_list']     =  $rs_all_teachers;
		$data['teacher_community'] =  $rs_all_groups;
		
		$response = "Success";
		$errorMsg = "Teachers List / Community  successfully";
		$response_array = array('response' => $response, 'result' => $data, 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
	 }
	 
	 // Show Messages - Teachers Community / Teacher -  Teacher Private Messages
  	function get_teacher_community_message()
  	{
		$lan 					= $_REQUEST["lan"];
		$device 				= $_REQUEST["device"];
		$device_uid 			= $_REQUEST["device_uid"];
		$logged_teacher_id    	= $_REQUEST["logged_teacher_id"];
		$colleague_teacher_id  	= $_REQUEST["colleague_teacher_id"];
		$tbl_school_id 			= $_REQUEST["tbl_school_id"];
		$message_mode 			= $_REQUEST["message_mode"];  // G - Group ,  P - Private
		$tbl_teacher_group_id 	= $_REQUEST["tbl_teacher_group_id"];

		$this->load->model('model_teacher_community');
		$this->load->model('model_teachers');
		$this->load->model('model_parents');
		$this->load->model('model_message');

		$listMessage = $this->model_teacher_community->get_teacher_community_messages($logged_teacher_id, $colleague_teacher_id, $tbl_school_id, $message_mode, $tbl_teacher_group_id);
		$data = array();
		for($i=0;$i<count($listMessage);$i++)
		{
					
					$teacher_obj = $this->model_teachers->get_teacher_details($listMessage[$i]['message_from']);
					if($lan=="ar"){
						$first_name_teacher = $teacher_obj[0]['first_name_ar'];
						$last_name_teacher  = $teacher_obj[0]['last_name_ar'];
					}else{
						$first_name_teacher = $teacher_obj[0]['first_name'];
						$last_name_teacher  = $teacher_obj[0]['last_name'];
					}

					$listMessage[$i]['name'] = $first_name_teacher." ".$last_name_teacher;  //from
					$listMessage[$i]['tbl_teacher_id'] = $teacher_obj[0]['tbl_teacher_id'];
					
			$file_name_updated = "";
			$file_name_updated_audio ="";
			$data_image_rs = "";
			$data_audio_rs = "";

			if($listMessage[$i]['tbl_item_id']<>""){
				$data_image_rs =  $this->model_message->get_message_image($listMessage[$i]['tbl_item_id']);	
				//print_r($data_image_rs);
				$file_name_updated = $data_image_rs[0]['file_name_updated'];
				$file_name_updated_thumb = $data_image_rs[0]['file_name_updated_thumb'];
				$data_audio_rs =  $this->model_message->get_message_audio($listMessage[$i]['tbl_item_id']);	
				$file_name_updated_audio = $data_audio_rs[0]['file_name_updated'];
			}

			if (trim($file_name_updated) != "") {
			$data[$i]["message_img"] = HOST_URL."/admin/uploads/".$file_name_updated;
			$data[$i]["message_img_thumb"] = HOST_URL."/admin/uploads/".$file_name_updated_thumb;
			} else {
				$data[$i]["message_img"] = "";
				$data[$i]["message_img_thumb"] = "";
			}

			if (trim($file_name_updated_audio) != "") {
				$data[$i]["message_audio"] = HOST_URL."/admin/uploads/".$file_name_updated_audio;
			} else {
				$data[$i]["message_audio"] = "";
			}

			$data[$i]['name'] 			=  	$listMessage[$i]['name'];
			$data[$i]['message'] 		=  	isset($listMessage[$i]['message'])? $listMessage[$i]['message']:'' ;
			$data[$i]['teacher_id']     =   $listMessage[$i]['tbl_teacher_id'];
			$data[$i]['date'] 			= 	date("H:i - d/m/Y",strtotime($listMessage[$i]["added_date"]));
		}

		$response = "Success";
		$errorMsg = "Teacher community message listed successfully";
		$response_array = array('response' => $response, 'messages' => $data, 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
	}
	  
	 
	//Save Teacher Community Message
	function save_teacher_community_message()
	 {
		$lan 				= $_REQUEST["lan"];
		$device 			= $_REQUEST["device"];
		$device_uid 		= $_REQUEST["device_uid"];
		$message_from 	    = $_REQUEST["message_from"];
		$message_to 		= $_REQUEST["message_to"];
		$message_type  	    = $_REQUEST["message_type"];
		$tbl_message 		= $_REQUEST["tbl_message"];
		$tbl_item_id    	= $_REQUEST["tbl_item_id"];
		$message_mode 		= $_REQUEST["message_mode"];  // G - Group , P - Private
		$tbl_teacher_group_id 	= $_REQUEST["tbl_teacher_group_id"];
		$tbl_school_id 		= $_REQUEST["tbl_school_id"];
		$this->load->model('model_teacher_community');
		$this->load->model('model_teachers');
		$this->load->model('model_parents');
		$saveMessage = $this->model_teacher_community->save_teacher_community_message($message_from, $message_to, $message_type, $tbl_message, $tbl_school_id, $tbl_item_id, $message_mode, $tbl_teacher_group_id);
			
			$teacher_obj = $this->model_teachers->get_teacher_details($message_from);
			if($lan=="ar"){
				$first_name_teacher = $teacher_obj[0]['first_name_ar'];
				$last_name_teacher  = $teacher_obj[0]['last_name_ar'];
			}else{
				$first_name_teacher = $teacher_obj[0]['first_name'];
				$last_name_teacher  = $teacher_obj[0]['last_name'];
			}
			$tacher_name = $first_name_teacher." ".$last_name_teacher;  //from teacher
		
			$teacher_to_obj = $this->model_teachers->get_teacher_details($message_to); //to teacher
			if($lan=="ar"){
					$first_name_to_teacher = $teacher_to_obj[0]['first_name_ar'];
					$last_name_to_teacher  = $teacher_to_obj[0]['last_name_ar'];
			}else{
					$first_name_to_teacher = $teacher_to_obj[0]['first_name'];
					$last_name_to_teacher  = $teacher_to_obj[0]['last_name'];
			}
				
			$teacher_to_name = $first_name_to_teacher." ".$last_name_to_teacher;
			$tbl_userId	=	$teacher_to_obj[0]['tbl_teacher_id'];
	
	
			$this->load->model('model_user_notify_token');
			$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_userId);
			for ($b=0; $b<count($data_tkns); $b++) {										
				//Send push message
				if (trim(ENABLE_PUSH) == "Y") {
						
					$message = 'Dear Teacher, please check message in your application inbox from '.$tacher_name;
					$token   = $data_tkns[$b]["token"];
					$device  = $data_tkns[$b]["device"];
					$this->model_user_notify_token->send_notification($token , $message, $device);//echo $url;
				}//if (trim(ENABLE_PUSH) == "Y")
			}
		$response = "Success";
		$errorMsg = "Teacher community message sent successfully";
		$response_array = array('response' => $response, 'result' => '', 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
	}
	
	
	// Teacher Community or Teacher - Teacher Private Message
	function update_message_read_status()
	{
		$tbl_teacher_group_id   = isset($_REQUEST['tbl_teacher_group_id'])?  $_REQUEST['tbl_teacher_group_id']:'';
		$message_from     		= isset($_REQUEST['message_from'])?  $_REQUEST['message_from']:'';
		$message_to      		= isset($_REQUEST['message_to'])?  $_REQUEST['message_to']:'';
		$tbl_school_id      	= isset($_REQUEST['tbl_school_id'])?  $_REQUEST['tbl_school_id']:'';
		$message_mode      		= isset($_REQUEST['message_mode'])?  $_REQUEST['message_mode']:'';
		
		$this->load->model("model_teacher_community");		
		$updateReadStatus = $this->model_teacher_community->updateMessageReadStatus($tbl_teacher_group_id, $message_from, $message_to, $tbl_school_id, $message_mode);
		$response = "Success";
		$errorMsg = "Read Status Updated";
		$data['tbl_school_id'] = $tbl_school_id;
		$response_array = array('response' => $response, 'result' => $data, 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
		
	}
	
	
	/************************* End Teacher Community Group ************************/
 
}
?>

