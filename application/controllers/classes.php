<?php
/**
* @desc   	  	Classes Controller
*
* @category   	Controller
* @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
* @version    	0.1
*/



class Classes extends CI_Controller {
	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
	function index() {
		//Do nothing
	}
	
	
	/**
	* @desc    Function to get school class names list
	*
	* @param   none
	* @access  default
	*/
	function class_list() {
		
		$user_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		$module_id = $_REQUEST["module_id"];
		
		$data["user_id"] = $user_id;
		$data["role"] = $role;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] = $school_id;
		$data["module_id"] = $module_id;
		
		$tbl_teacher_id = $user_id;
		
		
		$this->load->model('model_classes');
		$data_rs = $this->model_classes->get_class_list($school_id);
		$classes = array();
		//if ($module_id <> "cdrpt") {
			$all_classes_against_teacher_rs = $this->model_classes->get_all_classes_against_teacher($tbl_teacher_id);
			for($k=0;$k<count($all_classes_against_teacher_rs);$k++)
			{
				$classes[$k] = $all_classes_against_teacher_rs[$k]['tbl_class_id'];
			}
			
			for($m=0;$m<count($data_rs);$m++)
			{
			   $classId = 	$data_rs[$m]['tbl_class_id'];
			   if (in_array($classId, $classes)) {
					$data_rs[$m]['color'] = "1dee3a";
				}else{
					$data_rs[$m]['color'] = "";
				}
				
			}
		//}
		
		function sortByOrder($a, $b) {
				return $b['color'] 	- $a['color'];
		}
		
		//usort($data_rs, 'sortByOrder');
		
		if (count($data_rs) <=0) {
			$arr["code"] = "200";
			echo json_encode($arr);
			return;
		}
		
		// We need class list for daily reports sections
		if ($module_id == "cdrpt") {
			$data["page"] = "view_class_list_cdrpt";
		}else {
			$data["page"] = "view_class_list";
		}
		
		//Teacher admin
		if (trim($_POST["is_admin"]) == "Y") {
			$data['data_rs'] = $data_rs;
			$this->load->view('admin/view_teacher_class_list',$data);
			return;
		}
		
		//echo $data['page'];
		$data['data_rs'] = $data_rs;
		$this->load->view('view_template',$data);
	}
	
	
	/**
	* @desc    Function to get all classess against teacher
	*
	* @param   none
	* @access  default
	*/
	//http://192.168.2.13/aqdar/classes/get_all_classes_against_teacher/en/c5f5986a88f3a94/0
	function get_all_classes_against_teacher() {
		
		$param_array = $this->uri->ruri_to_assoc(3);
		$param_array = array_keys($param_array);
		$lan = urldecode($param_array[0]);
		$data['lan'] = $lan;
		
		$param_array = $this->uri->ruri_to_assoc(4);
		$param_array = array_keys($param_array);
		$tbl_teacher_id = urldecode($param_array[0]);
		$data['tbl_teacher_id'] = $tbl_teacher_id;
		
		$this->load->model('model_classes');
		$all_classes_against_teacher_rs = $this->model_classes->get_all_classes_against_teacher($tbl_teacher_id);
		$data['all_classes_against_teacher_rs'] = $all_classes_against_teacher_rs;
		
		$data['page'] = "view_all_classes_against_teacher_ddm";
		
		//echo $data['page'];
		$this->load->view('view_template',$data);
	}
	
	
	function get_all_teachers_against_class() {
		$user_id 			= $_REQUEST["user_id"];
		$role				= $_REQUEST["role"];
		$lan 				= $_REQUEST["lan"];
		$device 			= $_REQUEST["device"];
		$device_uid 		= $_REQUEST["device_uid"];
		$school_id 			= $_REQUEST["school_id"];
		$tbl_student_id 	= $_REQUEST["tbl_student_id"];
		$this->load->model('model_classes');
		$this->load->model('model_teachers');
		$this->load->model('model_student');
		$rs_student 	= $this->model_student->get_student_obj($tbl_student_id);	
		$tbl_class_id 	= $rs_student[0]['tbl_class_id'];
		
		$rs_teachers = $this->model_classes->get_all_teachers_in_class_bysort($tbl_class_id,$lan);	
		
		$data = array();
		for ($i=0; $i<count($rs_teachers); $i++) {
					if($lan=="ar")
					{
						$data[$i]['teacher_id']		=  $rs_teachers[$i]['tbl_teacher_id'];
						$data[$i]['first_name']		=  $rs_teachers[$i]['first_name_ar'];
						$data[$i]['last_name']		=  $rs_teachers[$i]['last_name_ar'];
						if($rs_teachers[$i]['file_name_updated']<>""){
							$data[$i]['img_url']	    =  IMG_PATH_TEACHER."/".$rs_teachers[$i]['file_name_updated'];
						}else{
							$data[$i]['img_url']	    =  '';
						}
			}else{
				$data[$i]['teacher_id']		=  $rs_teachers[$i]['tbl_teacher_id'];
						$data[$i]['first_name']	    =  $rs_teachers[$i]['first_name'];
						$data[$i]['last_name']	    =  $rs_teachers[$i]['last_name'];
						 if($rs_teachers[$i]['file_name_updated']<>""){
							$data[$i]['img_url']	    =  IMG_PATH_TEACHER."/".$rs_teachers[$i]['file_name_updated'];
						}else{
							$data[$i]['img_url']	    =  '';
						}
					}
		}
		
		/*$rs_teachers = $this->model_classes->get_all_teachers_in_class($tbl_class_id);	
		
				$data = array();
		for ($i=0; $i<count($rs_teachers); $i++) {
			$tbl_teacher_id = $rs_teachers[$i]['tbl_teacher_id'];
			$teacher_obj = $this->model_teachers->get_teachers_obj($tbl_teacher_id);
			$first_name_teacher = $teacher_obj[0]['first_name'];
			$last_name_teacher = $teacher_obj[0]['last_name'];
			if($lan=="ar")
			{
				$data[$i]['teacher_id']	=  $tbl_teacher_id;
				$data[$i]['first_name']		=  $teacher_obj[0]['first_name_ar'];
				$data[$i]['last_name']		=  $teacher_obj[0]['last_name_ar'];
				$data[$i]['img_url']	    =  IMG_PATH_TEACHER."/".$teacher_obj[0]['file_name_updated'];
			}else{
				$data[$i]['teacher_id']	=  $tbl_teacher_id;
				$data[$i]['first_name']	    =  $teacher_obj[0]['first_name'];
				$data[$i]['last_name']	    =  $teacher_obj[0]['last_name'];
				$data[$i]['img_url']	    =  IMG_PATH_TEACHER."/".$teacher_obj[0]['file_name_updated'];
			}
		}*/
		
			   
		$response = "Success";
		$errorMsg = "Teachers listed successfully";
		$response_array = array('response' => $response, 'result' => $data, 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
		
	}
}
?>
