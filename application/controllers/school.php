<?php
/**
 * @desc   	  	School Controller
 * @category   	Controller
 * @author     	Shanavas PK
 * @version    	0.1
 */
class School extends CI_Controller {
	/**
	* @desc    Default function for the Controller
	* @param   none
	* @access  default
	*/
    function index() {
	}



    /**
	* @desc    Show all schools
	* @param   none
	* @access  default
	*/
    function all_schools() {
		$data['page'] = "view_all_schools";
		$data['menu'] = "schools";
        $data['sub_menu'] = "schools_list";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "school_name";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		//$offset = ;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "school_name";
					 break;
				}
				default: {
					$sort_name = "school_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
	
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_school");
		
		$is_active = "Y";
		
		$rs_all_schools      = $this->model_school->list_all_schools($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id);
		$total_schools       = $this->model_school->get_total_schools($q, $is_active, $tbl_school_id);
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/school/all_schools";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_schools;
		$config['per_page'] = TBL_SCHOOL_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_SCHOOL_PAGING >= $total_schools) {
			$range = $total_schools;
		} else {
			$range = $offset+TBL_SCHOOL_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_schools schools</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_schools']	= $rs_all_schools;
		$data['total_schools'] = $total_schools;
		
		$is_ajax = true;
		if ($is_ajax == true) {
			$this->load->view('include/'.LAN_SEL."/view_all_schools_ajax.php",$data);	
		}
	}
}
?>