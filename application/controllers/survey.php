<?php

/**
 * @desc   	  	Survey Controller
 * @category   	Controller
 * @version    	0.1
 */

class Survey extends CI_Controller {
	
	/**
	* @desc Default constructor for the Controller
	* @access default
	*/

    public function __construct() {
		parent::__construct();
    }

	/**
	* @desc    Default function for the Controller
	* @param   none
	* @access  default
	*/

    function load_survey() {

		$user_id 	= $_REQUEST["user_id"];
		$role 		= $_REQUEST["role"];
		$lan 		= $_REQUEST["lan"];
		$device 	= $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id  = $_REQUEST["school_id"];

		$data["user_id"] 	= $user_id;
		$data["role"] 		= $role;
		$data["lan"] 		= $lan;
		$data["device"] 	= $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] 	= $school_id;

		$questions_per_page = 1000;
		$survey_page_no 	= 0;



		$this->load->model('model_survey');
	
		$data_questions = $this->model_survey->get_survey_questions($questions_per_page, $survey_page_no, $school_id);
		
		$tbl_survey_questions_id = $data_questions[0]['tbl_survey_questions_id'];
			
		$already_attend   = $this->model_survey->is_already_attended($user_id,$tbl_survey_questions_id);
				
		if($already_attend=="Y")
		{
			$data["data_questions"] = array();
		}else{
			$data["data_questions"] = $data_questions;
		}

		$data["page"] = "view_load_survey";
		$this->load->view('view_template',$data);
	}


	/**
	* @desc    Function to load first survey page
	* @param   none
	* @access  default
	*/

    function save_survey_ajax() {
		$user_id 	= $_REQUEST["user_id"];
		$role 		= $_REQUEST["role"];
		$lan 		= $_REQUEST["lan"];
		$device 	= $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id	= $_REQUEST["school_id"];
		

		$data["user_id"] 	= $user_id;
		$data["role"] 		= $role;
		$data["lan"] 		= $lan;
		$data["device"] 	= $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] 	= $school_id;

		$tbl_parent_id 		= $user_id;
		$survey_options_str = $_REQUEST["survey_options_str"];

		$this->load->model('model_survey');
		$this->model_survey->save_survey_options($survey_options_str, $tbl_parent_id, $school_id);
		$arr["code"] = "200";
		echo json_encode($arr);
	}

}

?>