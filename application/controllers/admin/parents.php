<?php
/**
 * @desc   	  	Parent Controller
 * @category   	Controller
 * @author     	Shanavas PK
 * @version    	0.1
 */

class Parents extends CI_Controller {

	/**
	* @desc    Default function for the Controller
	* @param   none
	* @access  default
	*/

    function index() {
		//Do nothing
	}


	/**
	* @desc    Show all parents
	* @param   none
	* @access  default
	*/
    function all_parents() {
		$data['page'] = "view_all_parents";
		$data['menu'] = "members";
        $data['sub_menu'] = "parents";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$is_approved = "";
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "id";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('is_approved',$param_array)) {
			$is_approved = $param_array['is_approved'];
			$data["is_approved"] = $param_array['is_approved'];
		}
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name";
					 break;
				}
				default: {
					$sort_name = "first_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_parent_id',$param_array)) {
			$tbl_parent_id = $param_array['tbl_parent_id'];
			$data['tbl_sel_parent_id'] = $tbl_parent_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
		
	    $this->load->model("model_parents");
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			$countriesObj          = $this->model_student->get_country_list('Y');
		    $data['countries_list']= $countriesObj;
			
			$academicObj          = $this->model_student->get_academic_year('Y',$tbl_school_id);
		    $data['academic_list']= $academicObj;
			
			$classesObj 		   = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
			$data['classes_list'] = $classesObj;
			
			$rs_all_parents      = $this->model_parents->get_all_parents($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_class_id, $is_approved);
			$total_parents       = $this->model_parents->get_total_parents($q, $is_active, $tbl_school_id,$tbl_class_id, $is_approved);
			$total_parents       = 0;
			for($g=0;$g<count($rs_all_parents);$g++)
			{
				$tbl_parent_id       = $rs_all_parents[$g]['tbl_parent_id'];
				$rs_all_children     = $this->model_parents->get_my_children($tbl_parent_id,$tbl_school_id);
				$rs_all_parents[$g]['rs_all_children'] = $rs_all_children;
				
			}
			
			
		}else{
			$rs_all_parents     = array();
			
			$rs_all_children    = array();
		}
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/parents/all_parents";
		if (isset($is_approved) && trim($is_approved)!="") {
			$page_url .= "/is_approved/".rawurlencode($is_approved);
		}
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_class_id) && trim($tbl_class_id)!="") {
			$page_url .= "/tbl_class_id/".rawurlencode($tbl_class_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_parents;
		$config['per_page'] = TBL_PARENT_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_PARENT_PAGING >= $total_parents) {
			$range = $total_parents;
		} else {
			$range = $offset+TBL_PARENT_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_parents parents</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_parents']	     = $rs_all_parents;
		$data['total_parents'] 	      = $total_parents;
		$data['rs_all_children'] 	    = $rs_all_children;

		$this->load->view('admin/view_template',$data);
	}
	
	 /**
	* @desc    Activate parent
	* @param   String 
	* @access  default
	*/
    function activateParent() {
		if(isset($_POST) && count($_POST) != 0) {
			$parent_id_enc = trim($_POST["parent_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_parents");		
		$this->model_parents->activate_parent($parent_id_enc,$tbl_school_id);
	}

	/**
	* @desc    Deactivate parent
	* @access  default
	*/
    function deactivateParent() {
		if(isset($_POST) && count($_POST) != 0) {
			$parent_id_enc = trim($_POST["parent_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_parents");		
		$this->model_parents->deactivate_parent($parent_id_enc,$tbl_school_id);
	}

   /**
	* @desc    Delete parent
	* @param   POST array
	* @access  default
	*/
	function deleteParent() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['parent_id_enc']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('parent_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_parents");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_parents->delete_parent($str[$i],$tbl_school_id);
		}
	}
	
	
	function is_exist_parent_user_id() {
		if ($_POST) {
			$tbl_parent_id              = $_POST['parent_id_enc'];	
			$parent_user_id             = $_POST['parent_user_id'];
	        $emirates_id_parent         = $_POST['emirates_id_parent'];	

		}
		$this->load->model("model_parents");		
		$results = $this->model_parents->is_exist_parent_user_id($tbl_parent_id, $parent_user_id, $emirates_id_parent);
		if(count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
	
	//edit functionality
	/**
	* @desc    Edit Parents
	* @param   none
	* @access  default
	*/
    function edit_parent() {
		$data['page'] = "view_all_parents";
		$data['menu'] = "members";
        $data['sub_menu'] = "parents";
		
		$data['mid'] = "3";
		$this->load->model("model_parents");
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_teacher_id = 0;
		if (array_key_exists('parent_id_enc',$param_array)) {
			$tbl_parent_id = $param_array['parent_id_enc'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		
		//Parent details
		$parent_obj 				= $this->model_parents->get_parent_obj($tbl_parent_id);
		$data['parent_obj'] 		= $parent_obj;
		$this->load->view('admin/view_template', $data);
	}

    //edit parents - for parent side
    
    function is_exist_parent_details() {
		if ($_POST) {
			$tbl_parent_id              = $_POST['parent_id_enc'];		
			$emirates_id                = $_POST['emirates_id'];
		}
		$this->load->model("model_parents");		
		$results = $this->model_parents->is_exist_parent_details($tbl_parent_id, $emirates_id);
		if(count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
	
    function save_parent_changes() {
   
    	if ($_POST) {
			$tbl_parent_id         	 = $_POST['tbl_parent_id'];		
			$first_name             	= $_POST['first_name'];
			$last_name              	 = $_POST['last_name'];
			$first_name_ar             = $_POST['first_name_ar'];
			$last_name_ar         	  = $_POST['last_name_ar'];		
			$dob_month                 = $_POST['dob_month'];
			$dob_day                   = $_POST['dob_day'];
			$dob_year                  = $_POST['dob_year'];
			$gender                    = $_POST['gender'];		
			$mobile                    = $_POST['mobile'];
			$email                     = $_POST['email'];
			$emirates_id               = $_POST['emirates_id'];		
			$user_id                   = $_POST['user_id'];
			$password                  = $_POST['password'];

		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_parents");	
	  
	    $resData = $this->model_parents->update_parent($tbl_parent_id, $first_name, $last_name, $first_name_ar, $last_name_ar, $dob_month, $dob_day, $dob_year, $gender, $mobile,
	    $email, $emirates_id, $user_id, $password, $tbl_school_id);
	  
	   if($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
		
	}
	/************************************************************/

       // GENERATE PARENT ID CARDS 
	
	  function generate_parent_id() {
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "first_name";
		$sort_by = "ASC";
		
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name";
					 break;
				}
				default: {
					$sort_name = "first_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_parent_id',$param_array)) {
			$tbl_parent_id = $param_array['tbl_parent_id'];
			$data['tbl_sel_parent_id'] = $tbl_parent_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
	    $this->load->model("model_parents");
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			
			$rs_all_parents      = $this->model_parents->get_all_parents($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_class_id);
			$total_parents       = $this->model_parents->get_total_parents($q, $is_active, $tbl_school_id,$tbl_class_id);
			for($g=0;$g<count($rs_all_parents);$g++)
			{
				$tbl_parent_id       = $rs_all_parents[$g]['tbl_parent_id'];
				$rs_all_children     = $this->model_parents->get_my_children($tbl_parent_id,$tbl_school_id);
				$rs_all_parents[$g]['rs_all_children'] = $rs_all_children;
				
			}
			
			
		}else{
			$rs_all_parents     = array();
			$total_parents      = array();
			$rs_all_children    = array();
		}

		
		$data['rs_all_parents']	     = $rs_all_parents;
		$data['total_parents'] 	      = $total_parents;
		$data['rs_all_children'] 	    = $rs_all_children;

		$this->load->view('admin/pdf_parent_id_cards',$data);
	}
	
	
	// END GENERATE PARENT IDS
 
    function list_parents_against_class() {
	
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		
	    $tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
		}
        
		$data["tbl_school_id"] = $tbl_school_id;
		$data["tbl_class_id"] = $tbl_class_id;

		$classes = explode(",",$tbl_class_id);
		$this->load->model('model_student');
		$this->load->model('model_parents');
		$this->load->model('model_classes');
		if(count($classes)>0)
		{  
		  
		   $m=0;
		   for($x=0;$x<count($classes);$x++)
		   {
				$tbl_class_id = $classes[$x];
				$all_students_against_class_rs = $this->model_student->list_all_students_against_class($tbl_class_id);
				for($a=0;$a<count($all_students_against_class_rs);$a++)
				{
					$parentList = array();
					$tbl_student_id = $all_students_against_class_rs[$a]['tbl_student_id'];
					$tbl_parent_id = $this->model_parents->get_parent_of_student($tbl_student_id);
					$parentList = $this->model_parents->get_parent_obj($tbl_parent_id);
					if(!empty($parentList)){
						$all_parents_students_against_class_rs[$m]['tbl_parent_id'] 	= $parentList[0]['tbl_parent_id'];
						$all_parents_students_against_class_rs[$m]['first_name']   		= $parentList[0]['first_name'];
						$all_parents_students_against_class_rs[$m]['last_name']   		= $parentList[0]['last_name'];
						$all_parents_students_against_class_rs[$m]['first_name_ar']     = $parentList[0]['first_name_ar'];
						$all_parents_students_against_class_rs[$m]['last_name_ar']      = $parentList[0]['last_name_ar'];
						$all_parents_students_against_class_rs[$m]['tbl_class_id']      = $tbl_class_id;
						$class_details = $this->model_classes->getClassInfo($tbl_class_id);
						$tbl_section_id 	= $class_details[0]['tbl_section_id'];
						$data_se = $this->model_classes->getClassSectionInfo($tbl_section_id);		
						$section_name 		=  $data_se[0]["section_name"];
						$section_name_ar 	=  $data_se[0]["section_name_ar"];
						$all_parents_students_against_class_rs[$m]['class_name']      	= $class_details[0]['class_name']." ".$section_name;
						$all_parents_students_against_class_rs[$m]['class_name_ar']     = $class_details[0]['class_name_ar']." ".$section_name_ar;
						$m=$m+1;
					}
				}
		   }
		}
		$data['list_parents_against_class'] = $all_parents_students_against_class_rs;
		$data['page'] = "view_parents_against_class";
		/*$this->load->view('view_template',$data);*/
		$this->load->view('admin/view_parents_against_class',$data);
	}


	/**
	* @desc    Approve parent
	* @param   none
	* @access  default
	*/
	function approve_parent() {
		$tbl_parent_id = $_POST["parent_id_enc"];
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		
		$this->load->model("model_parents");
		$this->load->model("model_student");
		
		//Approve student of parent as well
		$rs_student = $this->model_parents->get_students_of_parent($tbl_parent_id);
		for ($i=0; $i<count($rs_student); $i++) {
			$tbl_student_id = $rs_student[$i]["tbl_student_id"];
			
			//This will approve both parent and student
			$this->model_student->approve_student_parent($tbl_student_id, $tbl_parent_id, $tbl_school_id);
		}
		
		
	}


}



?>

