<?php

/**
 * @desc   	  	Ministry Parenting Controller
 *
 * @category   	Controller
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Ministry_parenting extends CI_Controller {


	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
    function index() {
		$this->all_ministry_parentings();
	}


	/**
	* @desc    Activate parenting
	*
	* @param   String tbl_parenting_id
	* @access  default
	*/
    function activate() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_parenting_id = trim($_POST["parenting_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		
		$this->load->model("model_ministry_parenting");		
		$this->model_ministry_parenting->activate($tbl_parenting_id);
	}


	/**
	* @desc    Deactivate parenting
	*
	* @param   String tbl_parenting_id
	* @access  default
	*/
    function deactivate() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_parenting_id = trim($_POST["parenting_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		
		$this->load->model("model_ministry_parenting");		
		$this->model_ministry_parenting->deactivate($tbl_parenting_id);
	}


	/**
	* @desc    Delete parenting
	*
	* @param   CSV tbl_parenting_id
	* @access  default
	*/
    function delete() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['parenting_id_enc']; //e.g." parenting_id_enc=f31981d6285a6a958f754774013472f7&parenting_id_enc=d99f9fee0ed779292475c&parenting_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('parenting_id_checkbox_enc=', '', $str);
			$str = explode("&", $str);

			$is_ajax = trim($_POST["is_ajax"]);
		} 
		
		$this->load->model("model_ministry_parenting");		
		
		for ($i=0; $i<count($str); $i++) {
			$this->model_ministry_parenting->delete($str[$i]);
		}
	}


	/**
	* @desc    Show all ministry parenting categorys
	*
	* @param   none
	* @access  default
	*/
    function all_ministry_parentings($MSG) {
		$data['page'] = "view_ministry_parenting";
		$data['menu'] = "parenting";
		$data['MSG'] = $MSG;

		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		$sort_name = "parenting_title_en";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_ministry_parentings = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "parenting_title_en";
					 break;
				}
				default: {
					$sort_name = "parenting_title_en";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_ministry_parenting");
		
		$is_active = "";
		$rs_all_ministry_parentings = $this->model_ministry_parenting->get_all_ministry_parentings($sort_name, $sort_by, $offset, $q, $is_active);
		$total_ministry_parentings = $this->model_ministry_parenting->get_total_ministry_parentings($q, $is_active);

		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/ministry_parenting/all_ministry_parentings";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_ministry_parentings;
		$config['per_page'] = TBL_PARENTING_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_PARENTING_PAGING >= $total_ministry_parentings) {
			$range = $total_ministry_parentings;
		} else {
			$range = $offset+TBL_PARENTING_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_ministry_parentings record(s)</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_ministry_parentings'] = $rs_all_ministry_parentings;
		$data['total_ministry_parentings'] = $total_ministry_parentings;

		//Load categories
		$this->load->model("model_ministry_parenting_category");
		$rs_parenting_categorys = $this->model_ministry_parenting_category->get_all_ministry_parenting_categorys("title_en", "ASC", "", "", "Y");
		$data['rs_parenting_categorys'] = $rs_parenting_categorys;

		$this->load->view('admin/view_ministry_template',$data);
	}


	/**
	* @desc    Check if parenting already exists
	*
	* @param   POST array
	* @access  default
	*/
    function is_exist() {
		if ($_POST) {
			$tbl_parenting_id = $_POST['parenting_id_enc'];		
			$parenting_title_en = $_POST['parenting_title_en'];
			$parenting_title_ar = $_POST['parenting_title_ar'];
		}
		$this->load->model("model_ministry_parenting");		
		$results = $this->model_ministry_parenting->is_exist($tbl_parenting_id, $parenting_title_en, $parenting_title_ar);

		if (count($results)>0) {
			echo "*Y*";
		} else {
			echo "*N*";
		}
	}


	/**
	* @desc    Create parenting
	*
	* @param   POST array
	* @access  default
	*/
    function create_ministry_parenting() {
		if ($_POST) {
			$tbl_parenting_id = $_POST['parenting_id_enc'];		
			$tbl_parenting_cat_id = $_POST['parenting_cat_id_enc'];		
			$parenting_title_en = $_POST['parenting_title_en'];
			$parenting_title_ar = $_POST['parenting_title_ar'];
			$parenting_type = $_POST['parenting_type'];
			$parenting_text_en = $_POST['parenting_text_en_hidden'];//using hidden because of WYSIWYG
			$parenting_text_ar = $_POST['parenting_text_ar_hidden'];//using hidden because of WYSIWYG
		}
		
		if (trim($_FILES["parenting_logo"]["name"]) != "") {
			
			$parenting_logo = $_FILES["parenting_logo"]["name"];
			
			$Ext = strchr($parenting_logo,".");
			$Ext = strtolower($Ext);
			$parenting_logo_3d_ = md5(uniqid(rand())); 
			$parenting_logo_3d_en = $parenting_logo_3d_."".$Ext;
			$parenting_logo_temp = $_FILES["parenting_logo"]["tmp_name"];
			$copy = file_copy("$parenting_logo_temp", UPLOAD_LOGO_IMG_PATH."/", "$parenting_logo_3d_en", 1, 1);
		}
		
		$parenting_logo = $parenting_logo_3d_en;
		
		$this->load->model("model_ministry_parenting");		
		$this->model_ministry_parenting->create_ministry_parenting($tbl_parenting_id, $tbl_parenting_cat_id, $parenting_title_en, $parenting_title_ar, $parenting_type, $parenting_logo, $parenting_text_en, $parenting_text_ar);
		
		$MSG = "Information saved successfully.";
		
		$this->all_ministry_parentings($MSG);
	}
	


	/**
	* @desc    Save changes
	*
	* @param   POST array
	* @access  default
	*/
    function save_changes() {
		if ($_POST) {
			$tbl_parenting_id = $_POST['parenting_id_enc'];		
			$tbl_parenting_cat_id = $_POST['parenting_cat_id_enc'];		
			$parenting_title_en = $_POST['parenting_title_en'];
			$parenting_title_ar = $_POST['parenting_title_ar'];
			$parenting_type = $_POST['parenting_type'];
			$parenting_text_en = $_POST['parenting_text_en_hidden'];//using hidden because of WYSIWYG
			$parenting_text_ar = $_POST['parenting_text_ar_hidden'];//using hidden because of WYSIWYG
		}
		
		$parenting_logo = "";
		if (trim($_FILES["parenting_logo"]["name"]) != "") {
			
			$parenting_logo = $_FILES["parenting_logo"]["name"];
			
			$Ext = strchr($parenting_logo,".");
			$Ext = strtolower($Ext);
			$parenting_logo_3d_ = md5(uniqid(rand())); 
			$parenting_logo_3d_en = $parenting_logo_3d_."".$Ext;
			$parenting_logo_temp = $_FILES["parenting_logo"]["tmp_name"];
			$copy = file_copy("$parenting_logo_temp", UPLOAD_LOGO_IMG_PATH."/", "$parenting_logo_3d_en", 1, 1);

			$parenting_logo = $parenting_logo_3d_en;
		}
		
		$this->load->model("model_ministry_parenting");		
		$this->model_ministry_parenting->save_changes($tbl_parenting_id, $tbl_parenting_cat_id, $parenting_title_en, $parenting_title_ar, $parenting_type, $parenting_logo, $parenting_text_en, $parenting_text_ar);
		
		$MSG = "Changes saved successfully.";
		
		$this->all_ministry_parentings($MSG);
	}


	/**
	* @desc    Edit job ministry parenting categorys
	*
	* @param   none
	* @access  default
	*/
    function edit_ministry_parenting() {
		$data['page'] = "view_ministry_parenting";
		$data['menu'] = "parenting";
		$data['mid'] = "3";

		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_parenting_id = 0;
					
		if (array_key_exists('parenting_id_enc',$param_array)) {
			$tbl_parenting_id = $param_array['parenting_id_enc'];
		}	 
		
		//Ministry_parenting details
		$this->load->model("model_ministry_parenting");		
		$ministry_parenting_obj = $this->model_ministry_parenting->get_ministry_parenting_obj($tbl_parenting_id);
		$data['ministry_parenting_obj'] = $ministry_parenting_obj;

		//Load categories
		$this->load->model("model_ministry_parenting_category");
		$rs_parenting_categorys = $this->model_ministry_parenting_category->get_all_ministry_parenting_categorys("title_en", "ASC", "", "", "Y");
		$data['rs_parenting_categorys'] = $rs_parenting_categorys;
		
		//Video
		$tbl_uploads_id = $ministry_parenting_obj['tbl_parenting_id'];
		$video_url = "";
		$this->load->model("model_file_mgmt");		
		$file_mgmt_obj = $this->model_file_mgmt->get_file_mgmt_obj($tbl_uploads_id);
		
		if (count($file_mgmt_obj)>0) {
			$tbl_uploads_id = $file_mgmt_obj['tbl_uploads_id'];	
			$video_url = HOST_URL."/admin/uploads/".$file_mgmt_obj['file_name_updated'];	

			$data['tbl_uploads_id'] = $tbl_uploads_id;
			$data['video_url'] = $video_url;
		}
		
		$this->load->view('admin/view_ministry_template', $data);
	}


	/**
	* @desc    Get all ministry parenting categorys
	*
	* @param   none
	* @access  default
	*/
    function ajax_all_ministry_parentings() {
		$data['page'] = "ajax_all_ministry_parentings";

		$lan = $_POST['lan'];
		$data['lan'] = $lan;
		
		$this->load->model("model_ministry_parenting");		
		$rs_ministry_parenting = $this->model_ministry_parenting->get_all_ministry_parentings('parenting_title_en', 'ASC', '', '', 'Y');
		
		$data['rs_ministry_parenting'] = $rs_ministry_parenting;
		$this->load->view('admin/ajax_all_ministry_parentings', $data);
	}


	/**
	* @desc    Function to delete image
	*
	* @param   none
	* @access  default
	*/
	function ajax_delete_image() {
		$tbl_parenting_id = $_POST['parenting_id_enc'];
		
		$this->load->model("model_ministry_parenting");		
		$this->model_ministry_parenting->delete_image($tbl_parenting_id);
	}
	
}
?>