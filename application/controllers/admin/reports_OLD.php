<?php
/**
 * @desc   	  	Reports Controller
 * @category   	Controller
 * @author     	Shanavas .PK
 * @version    	0.1
 */

class Reports extends CI_Controller {
	/**
	* @desc Default constructor for the Controller
	* @access default
	*/

    function index() {
	}

	/**
	* @desc    Students Cards Reports
	* @param   none
	* @access  default
	*/
  
    function school_reports()
	{
		$data['page'] = "view_cards_report";
		$data['menu'] = "reports";
        $data['sub_menu'] = "cards_report";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "first_name";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name";
					 break;
				}
				default: {
					$sort_name = "first_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			if($param_array['tbl_student_id']<>'undefined')
			{
				$tbl_student_id = $param_array['tbl_student_id'];
				$data['tbl_sel_student_id'] = $tbl_student_id;
			}
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$tbl_semester_id = "";
		if (array_key_exists('tbl_semester_id',$param_array)) {
			$tbl_semester_id = $param_array['tbl_semester_id'];
			$data['tbl_sel_semester_id'] = $tbl_semester_id;
		}
		
		$tbl_teacher_id = "";
		if (array_key_exists('tbl_teacher_id',$param_array)) {
			$tbl_teacher_id = $param_array['tbl_teacher_id'];
			$data['tbl_sel_teacher_id'] = $tbl_teacher_id;
		}
		
		$tbl_academic_year = "";
		if (array_key_exists('tbl_academic_year',$param_array)) {
			$tbl_academic_year = $param_array['tbl_academic_year'];
			$data['tbl_sel_academic_year'] = $tbl_academic_year;
		}
		
	/*	$tbl_start_date = "";
		if (array_key_exists('tbl_start_date',$param_array)) {
			$tbl_start_date = $param_array['tbl_start_date'];
			$data['tbl_sel_start_date'] = $tbl_start_date;
		}
		
		$tbl_end_date = "";
		if (array_key_exists('tbl_end_date',$param_array)) {
			$tbl_end_date = $param_array['tbl_end_date'];
			$data['tbl_sel_end_date'] = $tbl_end_date;
		}
		
		$tbl_gender = "";
		if (array_key_exists('tbl_gender',$param_array)) {
			$tbl_gender = $param_array['tbl_gender'];
			$data['tbl_sel_gender'] = $tbl_gender;
		}
		
		$tbl_country = "";
		if (array_key_exists('tbl_country',$param_array)) {
			$tbl_country = $param_array['tbl_country'];
			$data['tbl_sel_country'] = $tbl_country;
		}*/
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		$this->load->model("model_teachers");
		$this->load->model("model_classes");
		$this->load->model("model_report");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			/*if($academic_year_id=="" && $tbl_semester_id=="")
			{
				$current_date             = date("Y-m-d");
				$rs_current_semester      = $this->model_classes->get_current_semester($tbl_school_id);
				$tbl_sel_academic_year    = $rs_current_semester[0]['tbl_academic_year_id'];
				$tbl_sel_semester_id      = $rs_current_semester[0]['tbl_semester_id'];
				$data['tbl_sel_semester_id']    = $tbl_sel_semester_id;
				$data['tbl_sel_academic_year']  = $tbl_sel_academic_year;
				
			}*/
						
			$countriesObj          = $this->model_student->get_country_list('Y');
		    $data['countries_list']= $countriesObj;
			
			$academicObj          = $this->model_student->get_academic_year('Y',$tbl_school_id);
		    $data['academic_list']= $academicObj;
			
			if($tbl_academic_year=="")
			{
				$academicYearObj          = $this->model_student->get_current_academic_year($tbl_school_id);
		        $tbl_academic_year        = $academicYearObj[0]['tbl_academic_year_id'];
				//$tbl_academic_year = "e0c81b9954";
				$data['tbl_sel_academic_year']= $tbl_academic_year;
			}
			
			
			
			
			$classesObj 		   = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
			$data['classes_list'] = $classesObj;
			
			$rs_all_teachers         = $this->model_teachers->get_list_teachers($tbl_school_id,$tbl_class_id,'Y');
			$data['rs_all_teachers'] = $rs_all_teachers;
			
			$rs_all_semesters         = $this->model_classes->get_academic_semesters($tbl_school_id,$tbl_academic_year);
			$data['rs_all_semesters'] = $rs_all_semesters;
			
			$student_record= $this->model_student->get_all_students_against_class($tbl_class_id);
			$data['rs_all_students'] = $student_record;
			
			$rs_student_cards      = $this->model_report->get_student_cards_report($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id, $tbl_academic_year, $tbl_semester_id, $tbl_teacher_id, $tbl_student_id,$tbl_class_id );
			$total_student_cards   = $this->model_report->get_total_student_cards_report($q, $is_active, $tbl_school_id, $tbl_academic_year, $tbl_semester_id, $tbl_teacher_id, $tbl_student_id, $tbl_class_id);
		}else{
			$rs_student_cards      = array();
			$total_student_cards   = array();
			
		}
		
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/student/school_reports";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		
		if (isset($tbl_teacher_id) && trim($tbl_teacher_id)!="") {
			$page_url .= "/tbl_teacher_id/".rawurlencode($tbl_teacher_id);
		}
		if (isset($tbl_academic_year) && trim($tbl_academic_year)!="") {
			$page_url .= "/tbl_academic_year/".rawurlencode($tbl_academic_year);
		}
		
		if (isset($tbl_semester_id) && trim($tbl_semester_id)!="") {
			$page_url .= "/tbl_semester_id/".rawurlencode($tbl_semester_id);
		}
		if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
			$page_url .= "/tbl_student_id/".rawurlencode($tbl_student_id);
		}
	
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_student_cards;
		$config['per_page'] = TBL_CARD_CATEGORY_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_CARD_CATEGORY_PAGING >= $total_student_cards) {
			$range = $total_student_cards;
		} else {
			$range = $offset+TBL_CARD_CATEGORY_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_student_cards student cards</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_student_cards']	  = $rs_student_cards;
		$data['total_student_cards']  = $total_student_cards;
		
		$this->load->view('admin/view_template',$data);
	}
	
	
	 function cards_reports_detailed()
	{
		$data['page'] = "view_cards_report_detailed";
		$data['menu'] = "reports";
        $data['sub_menu'] = "cards_report";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "first_name";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name";
					 break;
				}
				default: {
					$sort_name = "first_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			if($param_array['tbl_student_id']<>'undefined')
			{
				$tbl_student_id = $param_array['tbl_student_id'];
				$data['tbl_sel_student_id'] = $tbl_student_id;
			}
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$tbl_semester_id = "";
		if (array_key_exists('tbl_semester_id',$param_array)) {
			$tbl_semester_id = $param_array['tbl_semester_id'];
			$data['tbl_sel_semester_id'] = $tbl_semester_id;
		}
		
		$tbl_teacher_id = "";
		if (array_key_exists('tbl_teacher_id',$param_array)) {
			$tbl_teacher_id = $param_array['tbl_teacher_id'];
			$data['tbl_sel_teacher_id'] = $tbl_teacher_id;
		}
		
		$tbl_academic_year = "";
		if (array_key_exists('tbl_academic_year',$param_array)) {
			$tbl_academic_year = $param_array['tbl_academic_year'];
			$data['tbl_sel_academic_year'] = $tbl_academic_year;
		}
		
		if (array_key_exists('card_type_id_enc',$param_array)) {
			$card_type_id = $param_array['card_type_id_enc'];
			$data['tbl_sel_card_type_id'] = $card_type_id;
		}
		
		
	/*	$tbl_start_date = "";
		if (array_key_exists('tbl_start_date',$param_array)) {
			$tbl_start_date = $param_array['tbl_start_date'];
			$data['tbl_sel_start_date'] = $tbl_start_date;
		}
		
		$tbl_end_date = "";
		if (array_key_exists('tbl_end_date',$param_array)) {
			$tbl_end_date = $param_array['tbl_end_date'];
			$data['tbl_sel_end_date'] = $tbl_end_date;
		}
		
		$tbl_gender = "";
		if (array_key_exists('tbl_gender',$param_array)) {
			$tbl_gender = $param_array['tbl_gender'];
			$data['tbl_sel_gender'] = $tbl_gender;
		}
		
		$tbl_country = "";
		if (array_key_exists('tbl_country',$param_array)) {
			$tbl_country = $param_array['tbl_country'];
			$data['tbl_sel_country'] = $tbl_country;
		}*/
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		$this->load->model("model_teachers");
		$this->load->model("model_classes");
		$this->load->model("model_report");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			/*if($academic_year_id=="" && $tbl_semester_id=="")
			{
				$current_date             = date("Y-m-d");
				$rs_current_semester      = $this->model_classes->get_current_semester($tbl_school_id);
				$tbl_sel_academic_year    = $rs_current_semester[0]['tbl_academic_year_id'];
				$tbl_sel_semester_id      = $rs_current_semester[0]['tbl_semester_id'];

				$data['tbl_sel_semester_id']    = $tbl_sel_semester_id;
				$data['tbl_sel_academic_year']  = $tbl_sel_academic_year;
				
			}*/
						
			$countriesObj          = $this->model_student->get_country_list('Y');
		    $data['countries_list']= $countriesObj;
			
			$academicObj          = $this->model_student->get_academic_year('Y',$tbl_school_id);
		    $data['academic_list']= $academicObj;
			
			
			$classesObj 		   = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
			$data['classes_list'] = $classesObj;
			
			$rs_all_teachers         = $this->model_teachers->get_list_teachers($tbl_school_id,$tbl_class_id,'Y');
			$data['rs_all_teachers'] = $rs_all_teachers;
			
			$rs_all_semesters         = $this->model_classes->get_academic_semesters($tbl_school_id,$tbl_sel_academic_year);
			$data['rs_all_semesters'] = $rs_all_semesters;
			
			$student_record= $this->model_student->get_all_students_against_class($tbl_class_id);
			$data['rs_all_students'] = $student_record;
			
			$rs_student_cards      = $this->model_report->get_student_cards_report_detailed($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id, $tbl_academic_year, $tbl_semester_id, $tbl_teacher_id, $tbl_student_id,$tbl_class_id, $card_type_id );
			$total_student_cards   = $this->model_report->get_total_student_cards_report_detailed($q, $is_active, $tbl_school_id, $tbl_academic_year, $tbl_semester_id, $tbl_teacher_id, $tbl_student_id, $tbl_class_id, $card_type_id);
		}else{
			$rs_student_cards      = array();
			$total_student_cards   = array();
			
		}
		
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/reports/cards_reports_detailed";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		
		if (isset($tbl_teacher_id) && trim($tbl_teacher_id)!="") {
			$page_url .= "/tbl_teacher_id/".rawurlencode($tbl_teacher_id);
		}
		if (isset($tbl_academic_year) && trim($tbl_academic_year)!="") {
			$page_url .= "/tbl_academic_year/".rawurlencode($tbl_academic_year);
		}
		
		if (isset($tbl_semester_id) && trim($tbl_semester_id)!="") {
			$page_url .= "/tbl_semester_id/".rawurlencode($tbl_semester_id);
		}
		if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
			$page_url .= "/tbl_student_id/".rawurlencode($tbl_student_id);
		}
		if (isset($card_type_id) && trim($card_type_id)!="") {
			$page_url .= "/card_type_id_enc/".rawurlencode($card_type_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_student_cards;
		$config['per_page'] = TBL_CARD_CATEGORY_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_CARD_CATEGORY_PAGING >= $total_student_cards) {
			$range = $total_student_cards;
		} else {
			$range = $offset+TBL_CARD_CATEGORY_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_student_cards student cards</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_student_cards']	  = $rs_student_cards;
		$data['total_student_cards']  = $total_student_cards;
		
		$this->load->view('admin/view_template',$data);
	}
	
  
 
	
}



?>







