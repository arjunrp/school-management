<?php

/**
 * @desc   	  	Home Controller
 *
 * @category   	Controller
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Home extends CI_Controller {


	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
    function index() {
		$data["user_type_str"] = "admin";
		$this->load->view("admin/iframe_index", $data);
	}


	/**
	* @desc    Welcome page
	* @param   none
	* @access  default
	*/
    function welcome() {
		
		
		//echo $data["cnt_not_approved"];
		
		$data = array();
		 if($_SESSION['aqdar_smartcare']['user_type_sess'] == "ministry")
		 {   
			 $data['page'] = "welcome_ministry";
			/* $month = array(); 
			 $this->load->model('model_student');
			 $this->load->model('model_teachers');
			 $tbl_school_id = isset($_SESSION['aqdar_smartcare']['tbl_school_id_sess'])? $_SESSION['aqdar_smartcare']['tbl_school_id_sess']:'';
			 $total_student_school = $this->model_student->get_total_students_against_school($tbl_school_id);
			 $total_teacher_school = $this->model_teachers->get_total_teachers_against_school($tbl_school_id);
			 $data['total_teacher_school']        = $total_teacher_school;
		     $data['total_student_school']        = $total_student_school;*/
			 $this->load->view("admin/view_ministry_template", $data);
		 }
		 else if($_SESSION['aqdar_smartcare']['user_type_sess'] == "timeline")
		 {
			
			 $data['page'] = "welcome_timeline";
			 $month = array(); 
			 $this->load->model('model_student');
			 $this->load->model('model_teachers');
			 $tbl_school_id = isset($_SESSION['aqdar_smartcare']['tbl_school_id_sess'])? $_SESSION['aqdar_smartcare']['tbl_school_id_sess']:'';
			 $total_student_school = $this->model_student->get_total_students_against_school($tbl_school_id);
			 $total_teacher_school = $this->model_teachers->get_total_teachers_against_school($tbl_school_id);
			 $data['total_teacher_school']        = $total_teacher_school;
		     $data['total_student_school']        = $total_student_school;
			 $this->load->view("admin/view_timeline_template", $data);
		 }
		 else{
			 
			 $data['page'] = "welcome";
			 $month = array(); 
			 $this->load->model('model_student');
			 $this->load->model('model_teachers');
			 $tbl_school_id = isset($_SESSION['aqdar_smartcare']['tbl_school_id_sess'])? $_SESSION['aqdar_smartcare']['tbl_school_id_sess']:'';
			 $total_student_school = $this->model_student->get_total_students_against_school($tbl_school_id);
			 $total_teacher_school = $this->model_teachers->get_total_teachers_against_school($tbl_school_id);
			 $data['total_teacher_school']        = $total_teacher_school;
		     $data['total_student_school']        = $total_student_school;
			 
			 // Get total student count for not approved studetns
			$q = "";
			$is_active = "Y";
			$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
			$tbl_class_search_id = "";
			$is_approved = "N";
			$data["cnt_not_approved"] = $this->model_student->get_total_students($q, $is_active, $tbl_school_id,$tbl_class_search_id, "", $is_approved);
		
			 $this->load->view("admin/view_template", $data);
		 }
	
	}


/*********************************************************************************************************/
	/**
	* @desc    User listing
	*
	* @param   none
	* @access  default
	*/
    function user_listing() {
		$data['page'] = "user_listing";
		$this->load->view("admin/view_ministry_template", $data);
	}


	/**
	* @desc    Test
	*
	* @param   none
	* @access  default
	*/
    function test() {
		$data['page'] = "test";
		$this->load->view("admin/test", $data);
	}


	/**
	* @desc    Welcome page for teacher
	*
	* @param   none
	* @access  default
	*/
    function teacher() {
		$this->load->model("model_message");
		$data["data_messages"] = $this->model_message->get_all_teacher_messages($_SESSION['aqdar_smartcare']['tbl_teacher_id_sess']);
		
		$data['page'] = "teacher_welcome";
		$this->load->view("admin/view_teacher_template", $data);
	}
	
}
?>

