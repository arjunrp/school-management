<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Books extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() // to remember use public
    {
        parent::__construct(); 
        $this->load->helper('url');
    }
	
	public function index()
	{
		//print_r($_SESSION); exit;
		$data['page']     = "view_books_list";
		$data['menu']     = "school_library";
        $data['sub_menu'] = "library_books";
		
		 if (isset($_SESSION) && trim($_SESSION['tbl_admin_id_sess']) == "") {
			header("Location: ".HOST_URL."/index/login/");
			exit;
		}else{
			
			$this->load->model('model_book');
			$this->load->model('model_class_sessions');
			$this->load->model('model_category');
				  
			$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  	
		    $classesObj = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
				  
		    $sort_by    = "ASC";
		    $sort_name  = "title_en";
			$offset     = 0;
			$total_books= 0;
			$q          = "";
		    $tbl_category_id = "";
			$page = "";
			$list      = "Y";
		
			$list_category = $this->model_category->get_all_book_categories($sort_name, $sort_by, $offset, $q, $list, 'Y');

				   
		   $param_array = $this->uri->uri_to_assoc(3);
			if (array_key_exists('page',$param_array)) {
				$page = $param_array['page'];
			}	
		   if (array_key_exists('offset',$param_array)) {
				$offset = $param_array['offset'];
				if (trim($offset) == "") {
					$offset = 0;	
				}
			}	
			if (array_key_exists('sort_name',$param_array)) {
				$sort_name = $param_array['sort_name'];
				
				switch($sort_name) {
					case("R"): {
						 $sort_name = "id";
						 break;
					}
					case("TR"): {
						 $sort_name = "id";
						 break;
					}
					default: {
						$sort_name = "title_en";
					}					
				}
			}	 
			if (array_key_exists('sort_by',$param_array)) {
				$sort_by = $param_array['sort_by'];
			}else{
				$sort_by = "ASC";
			}
		
			if (array_key_exists('q',$param_array)) {
				$q = trim(urldecode($param_array['q']));
			}
			
			 if (array_key_exists('tbl_category_id',$param_array)) {
				$tbl_category_id = $param_array['tbl_category_id'];
			}
			
			$list_books  = $this->model_book->get_all_books($sort_name, $sort_by, $offset, $q,$tbl_category_id,'');
			$total_books = $this->model_book->get_total_books($q,$tbl_category_id,'');
	
		 //PAGINATION CLASS
			$page_url = HOST_URL."/".LAN_SEL."/admin/books/index/";
			if ($page<>"") {
				$page_url .= "/page/".$page;
			}
			if ($q<>"") {
				$page_url .= "/q/".$q;
			}
			if ($sort_name<>"") {
				$page_url .= "/sort_name/".$sort_name;
			}
			if ($sort_by<>"") {
				$page_url .= "/sort_by/".$sort_by;
			}
			$page_url .= "/offset";
		
		
			$this->load->library('pagination');
			$config['base_url'] = $page_url;
			$config['total_rows'] = $total_books;
			$config['per_page'] = PAGING_TBL_BOOKS;//constant
			$config['uri_segment'] = $this->uri->total_segments();
			$config['num_links'] = 5;
	
			$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #ccc; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
			$config['next_link_disable'] = '';
	
			$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #ccc; padding:5px; background-color:#eeeeee; margin-right:3px '><<Prev</span>&nbsp;&nbsp;";
			$config['prev_link_disable'] = "";		
				
			$config['first_link'] = "";
			$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
			$config['first_tag_close'] = '</span>';
			
			$config['last_link'] = "";
			$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
			$config['last_tag_close'] = '</span>';
	
			$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #ccc; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
			$config['cur_tag_close'] = "&nbsp;</span>";
			
			$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #ccc; padding:5px; background-color:#eeeeee; margin-right:3px'>";
			$config['num_tag_close'] = "&nbsp;</span>";
	
			$this->pagination->initialize($config);
			$start = $offset + 1;
			$range = "";
			if ($offset+PAGING_TBL_BOOKS >= $total_books) {
				$range = $total_books;
			} else {
				$range = $offset+PAGING_TBL_BOOKS;
			}
	
			$paging_string = "$start - $range <font color='#000'>of $total_books books</font>";
		
		$data['offset'] = $offset;
		$data['q'] = $q;
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		$data['total_books'] = $total_books;
		$data['list_books'] = $list_books;
		$data['search_data'] = $q;
		$data['classes_list'] = $classesObj;
		$data['list_category'] = $list_category;
		$this->load->view('admin/view_template', $data);
		}
	}
	
	public function add_book()
	{
	    $data['message'] = "";
		$offset          = 0;
		$data['page']     = "view_books_list";
		$data['menu']     = "school_library";
        $data['sub_menu'] = "library_books";
		
		if (isset($_SESSION) && trim($_SESSION['tbl_admin_id_sess']) == "") {
			header("Location: ".HOST_URL."/index/login/");
			exit;
		}else{
			$this->load->model('model_book');
				if(isset($_POST['title_en']) || isset($_POST['title_ar']) )
				{
					$tbl_book_id          = $_POST['tbl_book_id'];
					$getBookInfo = $this->model_book->getBookInfo($tbl_book_id);
					if(count($getBookInfo)==0)
					{
						$tbl_book_category_id  = $_POST['tbl_book_category_id'];
						$tbl_language_id       = $_POST['tbl_language_id'];
						$title_en 			   = addslashes($_POST['title_en']);
						$title_ar 			   = addslashes($_POST['title_ar']);
						$author_name_en 	   = addslashes($_POST['author_name_en']);
						$author_name_ar        = addslashes($_POST['author_name_ar']);
						$isbn 			       = $_POST['isbn'];
						$description_en        = addslashes($_POST['description_en']);
						$description_ar        = addslashes($_POST['description_ar']);
						$tbl_class_id          = $_POST['tbl_class_id'];
						$tbl_school_id         = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  	
						$save_book = $this->model_book->save_book($tbl_book_id,$tbl_book_category_id, $tbl_language_id, $title_en, $title_ar, $author_name_en, $author_name_ar, $isbn, $description_en, $description_ar,$tbl_class_id);
						if($save_book=="X")
						{
							echo "X";
							exit;
						}else{
							$save_file = $this->model_book->add_file_to_book($tbl_book_id);
							
							$assign_book = $this->model_book->assign_book_to_class($tbl_book_id,$tbl_class_id,$tbl_school_id);
							
							echo "Y";
							exit;
						}
					}
				}else{
					echo "N";
					exit;
				}
			}
			$this->load->model('model_category');
			$sort_name = "";
			$sort_by   = "";
			$offset    = ""; 
			$q		 = "";
			$list      = "Y";
		
			//$is_email_exists = $this->model_user->validate_user_exists($emiratesId);
			$list_category = $this->model_category->get_all_book_categories($sort_name, $sort_by, $offset, $q, $list, 'Y');
			$data['list_category'] = $list_category;
			$this->load->view('admin/view_template', $data);
	}
	
	
	/***************** Book Category ********************/
	public function list_category()
	{
		
		$data['page']     = "view_book_category";
		$data['menu']     = "school_library";
        $data['sub_menu'] = "library_books";
		
		if (isset($_SESSION) && trim($_SESSION['tbl_admin_id_sess']) == "") {
			header("Location: ".HOST_URL."/index/login/");
			exit;
		}else{
			  $this->load->model('model_category');
			  $this->load->model('model_book');
			  $sort_by    = "ASC";
		      $sort_name  = "category_name_en";
		      $offset     = 0;
		      $total_book_categories= 0;
			  $q          = "";
			  $page = "";
			   
			   
			   $param_array = $this->uri->uri_to_assoc(3);
			    if (array_key_exists('page',$param_array)) {
					$page = $param_array['page'];
				}	
			   if (array_key_exists('offset',$param_array)) {
					$offset = $param_array['offset'];
					if (trim($offset) == "") {
						$offset = 0;	
					}
				}	
				if (array_key_exists('sort_name',$param_array)) {
					$sort_name_param = $param_array['sort_name'];
					
					switch($sort_name) {
						case("A"): {
							$sort_name = "category_name_en";
							 break;
						}
						default: {
							$sort_name = "category_name_en";
						}					
					}
				}	 
				if (array_key_exists('sort_by',$param_array)) {
					$sort_by = $param_array['sort_by'];
				}	 
			
			    if (array_key_exists('q',$param_array)) {
					$q = $param_array['q'];
				}
			
				$list_category         = $this->model_category->get_all_book_categories($sort_name, $sort_by, $offset, $q);
				for($m=0;$m<count($list_category);$m++)
				{
					$tbl_book_category_id			=	$list_category[$m]['tbl_book_category_id'];
					$cntBook 	             		 =	$this->model_book->get_total_books('',$tbl_book_category_id,'');
					$list_category[$m]['cntBook']    = $cntBook;
				}
		   	    $total_book_categories = $this->model_category->get_total_book_categories($q);
				
		
			    //PAGINATION CLASS
				$page_url = HOST_URL."/".LAN_SEL."/admin/books/list_category/";
				if ($page<>"") {
					$page_url .= "/page/".$page;
				}
				if ($q<>"") {
					$page_url .= "/q/".$q;
				}
				if ($sort_name<>"") {
					$page_url .= "/sort_name/".$sort_name;
				}
				if ($sort_by<>"") {
					$page_url .= "/sort_by/".$sort_by;
				}
				$page_url .= "/offset";
			
			
				$this->load->library('pagination');
				$config['base_url'] = $page_url;
				$config['total_rows'] = $total_book_categories;
				$config['per_page'] = PAGING_TBL_BOOKS_CATEGORY;//constant
				$config['uri_segment'] = $this->uri->total_segments();
				$config['num_links'] = 5;
		
				$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #ccc; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
				$config['next_link_disable'] = '';
		
				$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #ccc; padding:5px; background-color:#eeeeee; margin-right:3px '><<Prev</span>&nbsp;&nbsp;";
				$config['prev_link_disable'] = "";		
					
				$config['first_link'] = "";
				$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
				$config['first_tag_close'] = '</span>';
				
				$config['last_link'] = "";
				$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
				$config['last_tag_close'] = '</span>';
		
				$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #ccc; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
				$config['cur_tag_close'] = "&nbsp;</span>";
				
				$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #ccc; padding:5px; background-color:#eeeeee; margin-right:3px'>";
				$config['num_tag_close'] = "&nbsp;</span>";
		
				$this->pagination->initialize($config);
				$start = $offset + 1;
				$range = "";
				if ($offset+PAGING_TBL_BOOKS_CATEGORY >= $total_book_categories) {
					$range = $total_book_categories;
				} else {
					$range = $offset+PAGING_TBL_BOOKS_CATEGORY;
				}
		
				$paging_string = "$start - $range <font color='#000'>of $total_book_categories categories</font>";
		
			$data['offset'] = $offset;
			$data['paging_string'] = $paging_string;
			$data['start'] = $start;
			$data['total_book_categories'] = $total_book_categories;
			$data['rs_all_categories'] = $list_category;
			$this->load->view('admin/view_template', $data);
		}
	}
	
	
	public function add_category()
	{
		 $data['message'] = "";
		 $data['page']     = "view_book_category";
		 $data['menu']     = "school_library";
         $data['sub_menu'] = "library_books";
		 
		 if (isset($_SESSION) && trim($_SESSION['tbl_admin_id_sess']) == "") {
			header("Location: ".HOST_URL."/index/login/");
			exit;
		}else{
			$this->load->model('model_category');
			$sort_name = "";
			$sort_by   = "";
			$offset    = "";  
			$q		 = "";
			$unique_id            = substr(md5(uniqid(rand())),0,15);
			if(isset($_POST['category_name_en']) || isset($_POST['category_name_ar']) )
			{
				$tbl_book_category_id           = isset($_POST['tbl_book_category_id'])? $_POST['tbl_book_category_id']:'' ;
				$tbl_book_category_parent_id    = isset($_POST['tbl_book_category_parent_id'])? $_POST['tbl_book_category_parent_id']:'' ;
				$category_name_en 		        = isset($_POST['category_name_en'])? $_POST['category_name_en']: '' ;
				$category_name_ar               = isset($_POST['category_name_ar'])? $_POST['category_name_ar']: '' ;
			
				$save_book_category             = $this->model_category->save_book_category($tbl_book_category_id,$tbl_book_category_parent_id, $category_name_en, $category_name_ar);
				if($save_book_category=="N")
				{
					echo "N";
				    exit;
				}else{
					echo "Y";
				    exit;
				}
			}
			echo "N";
			exit;
		}
	}
	

	
	
	function iframe_show_book_cover_pic() {
		$unique_id = $_REQUEST['unique_id'];
		$data['unique_id'] = $unique_id; 
		$this->load->view("iframe_show_book_cover_pic",$data);
	}
	
    function cover_photo_upload() {
		$this->load->model("model_book");
		$tbl_book_id = $_POST['tbl_book_id'];
		$data['unique_id']    = $tbl_book_id;
		$res = $this->model_book->upload_book_cover($_FILES,$tbl_book_id);
		$data['cover_pic']      = $res['cover_pic'];
        $this->load->view("iframe_show_book_cover_pic",$data);
		//$rs_files = $this->model_book->get_files($tbl_user_id, $tbl_company_id);
	}


   function iframe_show_book_file() {
		$unique_id = $_REQUEST['unique_id'];
		$data['unique_id'] = $unique_id; 
		$this->load->view("iframe_show_book_file",$data);
	}
	
	 function book_file_upload() {
		$this->load->model("model_book");
		$tbl_book_id = $_POST['tbl_book_id'];
		$data['unique_id']    = $tbl_book_id;
		$res = $this->model_book->upload_book_file($_FILES,$tbl_book_id);
		$data['book_file']      = $res['book_file_name'];
        $this->load->view("iframe_show_book_file",$data);
		//$rs_files = $this->model_book->get_files($tbl_user_id, $tbl_company_id);
	}
	
	 public function is_exist_book()
	{
	    
		if (isset($_SESSION) && trim($_SESSION['tbl_admin_id_sess']) == "") {
			header("Location: ".HOST_URL."/index/login/");
			exit;
		}else{
			$this->load->model('model_book');
			if(isset($_POST['title_en']) || isset($_POST['title_ar']) )
			{
				
				$bid 		   = $_POST['bid'];
				$title_en      = $_POST['title_en'];
				$title_ar      = $_POST['title_ar'];
				
				$is_exist = $this->model_book->is_exist_book($tbl_book_id, $tbl_language_id, $title_en, $title_ar);
				if($is_exist=="N")
				{
					echo "N";
				    exit;
				}else{
					echo "Y";
				    exit;
				}
			}
			
		}
	}
	
	//Edit Section
	public function edit_book()
	{
	    $data['page']     = "view_edit_book";
		$data['menu']     = "school_library";
        $data['sub_menu'] = "library_books";
		$data['mid'] = "3";
	   
	    $tbl_school_id        = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
	    $param_array = $this->uri->uri_to_assoc(3);
	    if (array_key_exists('bid',$param_array)) {
			$bid = $param_array['bid'];
		}
		$data['message'] = "";
		$offset          = 0;
		if (isset($_SESSION) && trim($_SESSION['tbl_admin_id_sess']) == "") {
			header("Location: ".HOST_URL."/index/login/");
			exit;
		}else{
			$this->load->model('model_book');
			if(isset($_POST['title_en']) || isset($_POST['title_ar']) )
			{
				
				$tbl_book_id 		  = $_POST['tbl_book_id'];
				$tbl_book_category_id = $_POST['tbl_book_category_id'];
				$tbl_language_id      = $_POST['tbl_language_id'];
				$title_en 			  = addslashes($_POST['title_en']);
				$title_ar 			  = addslashes($_POST['title_ar']);
				$author_name_en 	  = addslashes($_POST['author_name_en']);
				$author_name_ar       = addslashes($_POST['author_name_ar']);
				$isbn 			      = $_POST['isbn'];
				$description_en       = addslashes($_POST['description_en']);
				$description_ar       = addslashes($_POST['description_ar']);
				$tbl_class_id         = $_POST['tbl_class_id'];

				$is_exist = $this->model_book->is_exist_book($tbl_book_id, $tbl_language_id, $title_en, $title_ar);
				if($is_exist=="N")
				{
						$update_book = $this->model_book->update_book($tbl_book_id,$tbl_book_category_id, $tbl_language_id, $title_en, $title_ar, $author_name_en, $author_name_ar, $isbn, $description_en, $description_ar,$user_age_group,$book_point,$total_page,$book_price);
					    $update_file = $this->model_book->update_file_to_book($tbl_book_id);
						$assign_bok  = $this->model_book->assign_book_to_class($tbl_book_id,$tbl_class_id,$tbl_school_id);
						$data['message']         = "Book is updated successfully.";
					
				}else{
					$data['message']         = "Book title is already exist.";
					
				}
				
			}
			
			$this->load->model('model_category');
			$this->load->model('model_file_mgmt');
			$this->load->model('model_class_sessions');
			$sort_name = "";
			$sort_by   = "";
			$offset    = ""; 
			$q		 = "";
			$list      = "Y";
			
			$tbl_book_id 	= $bid;
			$getBookInfo 	= $this->model_book->getBookInfo($tbl_book_id);
			$list_category  = $this->model_category->get_all_book_categories($sort_name, $sort_by, $offset, $q, $list, 'Y');
			$classesObj     = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
			$assigned_classesObj = $this->model_book->get_assigned_classes($tbl_book_id,$tbl_school_id, 'Y');
			/*$package_group ="N";
		    $list_age_group = $this->model_settings->get_all_age_groups($sort_name, $sort_by, $offset, $q="",$list, 'Y', $package_group);
			$data['list_age_group'] 	= $list_age_group;*/

			$data['list_category']       = $list_category;
			$data['getBookInfo']         = $getBookInfo;
			$data['fileInfo']            = $fileObj;
			$data['classes_list']        = $classesObj;
			$data['assign_classes_obj']  = $assigned_classesObj;
			
			$this->load->view('admin/view_template', $data);
		}
	}
	
	
	
	public function book_details()
	{
		$data['page']     = "view_books_list";
		$data['menu']     = "school_library";
        $data['sub_menu'] = "library_books";
		$data['mid'] = "4";
		
	   
	    $param_array = $this->uri->uri_to_assoc(3);
	    if (array_key_exists('bid',$param_array)) {
			$bid = $param_array['bid'];
		}
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
		}
		
		$this->load->model('model_book');	
		$this->load->model('model_category');
			
		$tbl_book_id = $bid;
		$getBookInfo = $this->model_book->getBookInfo($tbl_book_id);
		
		
		$tbl_book_category_id   = $getBookInfo[0]['tbl_book_category_id'];
		$category_info = $this->model_category->getCategoryDetails($tbl_book_category_id);
		$data['category_name_en'] = $category_info[0]['category_name_en'];
		$data['category_name_ar'] = $category_info[0]['category_name_ar'];
		
		$data['getBookInfo']       = $getBookInfo;
		$data['offset']            = $offset;
		$this->load->view('admin/view_template', $data);
	}
	
	
	/** Acivate book */
  	function activate_book() {
	    $bid   = $_POST['bid'];
		$this->load->model('model_book');
		$this->model_book->activate_book($bid);
		echo "Y";			
		exit;
	}

	/** Deacivate Book */
  	function deactivate_book() {
	    $bid   = $_POST['bid'];
		$this->load->model('model_book');
		$this->model_book->deactivate_book($bid);
	    echo "Y";			
		exit;	
	}

	/** Delete book */
  	function delete_book() {
		
		$str = $_POST['book_id_enc']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
		$str_data = str_replace('book_id_enc=', '', $str);
		$str = explode("&", $str_data);
		$this->load->model('model_book');
		if(!empty($str))
		{
			for ($i=0; $i<count($str); $i++) {
				$this->model_book->delete_book($str[$i]);
			}
		}else{
			$this->model_book->delete_book($str_data);
			
		}
	    echo "Y";			
		exit;	
	}
	
	
	
	//Edit Section
	public function edit_category()
	{
	    $data['page']     = "view_book_category";
		$data['menu']     = "school_library";
        $data['sub_menu'] = "library_books";
		$data['mid'] = "3";
	   
	    $param_array = $this->uri->uri_to_assoc(3);
	    if (array_key_exists('cid',$param_array)) {
			$cid = $param_array['cid'];
		}
		$data['message'] = "";
		$offset          = 0;
		
	    if (isset($_SESSION) && trim($_SESSION['tbl_admin_id_sess']) == "") {
			header("Location: ".HOST_URL."/index/login/");
			exit;
		}else{
			
			$this->load->model('model_category');
			if(isset($_POST['category_name_en']) || isset($_POST['category_name_ar']) )
			{
				$tbl_book_category_id 		  = $_POST['book_category_id_enc'];
				$tbl_parent_category_id       = '';
				$category_name_en 			  = $_POST['category_name_en'];
				$category_name_ar 			  = $_POST['category_name_ar'];
				
				
				$is_exist = $this->model_category->is_exist_book_category($tbl_book_category_id, $category_name_en, $category_name_ar);
				if($is_exist=="N")
				{
					$update_book = $this->model_category->update_book_category($tbl_book_category_id,$tbl_parent_category_id, $category_name_en, $category_name_ar);
					$data['message']         = "Book category is updated successfully.";
				}else{
					$data['message']         = "Book category is already exist.";
					
				}
			}
			$sort_name = "";
			$sort_by   = "";
			$offset    = 0; 
			$q         = ""; 
			$this->load->model('model_category');
			$tbl_book_category_id = $cid;
			$getBookCategoryInfo = $this->model_category->getCategoryDetails($tbl_book_category_id);
			$data['book_categories']       = $getBookCategoryInfo;
			
			$list = "Y";
			$list_category = $this->model_category->get_all_book_categories($sort_name, $sort_by, $offset, $q, $list);
			$data['list_category'] = $list_category;
			$this->load->view('admin/view_template', $data);
		}
	}
	
	
	
	  public function is_exist_book_category()
	  {
	    $param_array = $this->uri->uri_to_assoc(3);
	    if (array_key_exists('cid',$param_array)) {
			$cid = $param_array['cid'];
		}
		$data['message'] = "";
		$offset          = 0;
		if (isset($_SESSION) && trim($_SESSION['tbl_admin_id_sess']) == "") {
			header("Location: ".HOST_URL."/index/login/");
			exit;
		}else{
			$this->load->model('model_category');
			if(isset($_POST['category_name_en']) || isset($_POST['category_name_ar']) )
			{
				
				$tbl_book_category_id 		 = $_POST['book_category_id_enc'];
				$tbl_parent_category_id      = $_POST['tbl_book_category_id'];
				$category_name_en 			 = $_POST['category_name_en'];
				$category_name_ar 			 = $_POST['category_name_ar'];
				$is_exist = $this->model_category->is_exist_book_category($tbl_book_category_id, $category_name_en, $category_name_ar);
				if($is_exist=="N")
				{
					echo "N";
				    exit;
				}else{
					echo "Y";
				    exit;
				}
			}
			
		}
	}
	
	
	/** Acivate book category*/
  	function activate_book_category() {
	    $cid   = $_POST['cid'];
		$this->load->model('model_category');
		$this->model_category->activate_book_category($cid);
		echo "Y";			
		exit;
	}

	/** Deacivate Book category */
  	function deactivate_book_category() {
	    $cid   = $_POST['cid'];
		$this->load->model('model_category');
		$this->model_category->deactivate_book_category($cid);
	    echo "Y";			
		exit;	
	}

	/** Delete book category */
  	function delete_book_category() {
        $cid   = $_POST['cid'];	
        $this->load->model('model_category');
		$this->model_category->delete_book_category($cid);
	    echo "Y";			
		exit;	
	}
	
	
	
	
	function iframe_show_category_pic() {
		$data['page'] = "iframe_show_book_category_pic";
		$MSG = "";
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_book_category_id = "";
		
		if (array_key_exists('tbl_book_category_id',$param_array)) {
			$tbl_book_category_id = $param_array['tbl_book_category_id'];
		} 
		$this->load->model("model_category");
		$category_obj = $this->model_category->get_book_category_picture($tbl_book_category_id);
		if(!empty($category_obj))
		{
			$data['picture'] = $category_obj[0]['picture'];
			$data['tbl_book_category_id'] = $tbl_book_category_id;
			$data['MSG'] = $MSG;
		}else{
			$data['picture'] = "";
			$data['tbl_book_category_id'] = $tbl_book_category_id;
			$data['MSG'] = $MSG;
		}
		$this->load->view("iframe_show_book_category_pic",$data);
	}
	
	function upload_book_category_pic() {
		$data['page'] = "iframe_show_book_category_pic";
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_book_category_id = trim($_POST["tbl_book_category_id"]);
		}

		$this->load->model("model_category");
		$MSG = $this->model_category->update_book_category_pic($_FILES, $tbl_book_category_id);
		$category_obj = $this->model_category->get_book_category_picture_upload($tbl_book_category_id);
		if(!empty($category_obj))
		{
			$data['picture'] = $category_obj[0]['picture'];
			$data['tbl_book_category_id'] = $tbl_book_category_id;
			$data['MSG'] = $MSG;
		}else{
			$data['picture'] = "";
			$data['tbl_book_category_id'] = $tbl_book_category_id;
			$data['MSG'] = $MSG;
		}
		$this->load->view("iframe_show_book_category_pic",$data);
		/*//Picture is changed of logged in user so we change the value in session as well.
		if (trim($tbl_user_id) == trim($_SESSION['tbl_user_id_sess'])) {
			$_SESSION['picture_sess'] = $user_obj['picture'];
		}*/
	}
	
	
	
	
	/******** Update status of Book Questions *******/
	
	/** Acivate Question */
  	function activate_question() {
	    $bid   = $_POST['bid'];
		$qid   = $_POST['qid'];
		$this->load->model('model_book');
		$this->model_book->activate_question($bid,$qid);
		echo "Y";			
		exit;
	}

	/** Deacivate Question*/
  	function deactivate_question() {
	    $bid   = $_POST['bid'];
		$qid   = $_POST['qid'];
		$this->load->model('model_book');
		$this->model_book->deactivate_question($bid,$qid);
	    echo "Y";			
		exit;	
	}

	/** Delete Question*/
  	function delete_question() {
        $bid   = $_POST['bid'];
		$qid   = $_POST['qid'];	
        $this->load->model('model_book');
		$this->model_book->delete_question($bid,$qid);
	    echo "Y";			
		exit;	
	}
	
	/***************** Update publish status of Book Reviews **********/
	/** Publish Book Reviews*/
  	function publish_review() {
	    $rid   = $_POST['rid'];
		$bid   = $_POST['bid'];
		$this->load->model('model_book');
		$this->model_book->publish_review($rid,$bid);
		echo "Y";			
		exit;
	}

	/** Unpublish Book Reviews*/
  	function unpublish_review() {
	    $rid   = $_POST['rid'];
		$bid   = $_POST['bid'];
		$this->load->model('model_book');
		$this->model_book->unpublish_review($rid,$bid);
	    echo "Y";			
		exit;	
	}
	
	// BACKEND FUNCTIONALITY
	
	
	  public function list_book_package()
	   {
			if (isset($_SESSION) && trim($_SESSION['tbl_admin_id_sess']) == "") {
				header("Location: ".HOST_URL."/index/login/");
				exit;
			}else{
			  $this->load->model('model_category');
			  $this->load->model('model_book');
			  $sort_by    = "ASC";
		      $sort_name  = "category_name_en";
		      $offset     = 0;
		      $total_book_categories= 0;
			  $q          = "";
			  $page = "";
			   
			   
			   $param_array = $this->uri->uri_to_assoc(3);
			    if (array_key_exists('page',$param_array)) {
					$page = $param_array['page'];
				}	
			   if (array_key_exists('offset',$param_array)) {
					$offset = $param_array['offset'];
					if (trim($offset) == "") {
						$offset = 0;	
					}
				}	
				if (array_key_exists('sort_name',$param_array)) {
					$sort_name_param = $param_array['sort_name'];
					
					switch($sort_name) {
						case("A"): {
							$sort_name = "package_name_en";
							 break;
						}
						default: {
							$sort_name = "package_name_en";
						}					
					}
				}	 
				if (array_key_exists('sort_by',$param_array)) {
					$sort_by = $param_array['sort_by'];
				}	 
			
			    if (array_key_exists('q',$param_array)) {
					$q = $param_array['q'];
				}
			
				$list_packages         = $this->model_category->get_all_book_packages($sort_name, $sort_by, $offset, $q);
				for($m=0;$m<count($list_packages);$m++)
				{
					$tbl_book_package_id			 =	 $list_packages[$m]['tbl_book_package_id'];
					$total_books                     =     $this->model_category->get_total_books_in_package($q,$tbl_book_package_id,'');
					$list_packages[$m]['cntBook']    =     $total_books;
				}
		   	    $total_book_packages = $this->model_category->get_total_book_packages($q);
				
				
		
			 //PAGINATION CLASS
				$page_url = HOST_URL."/books/list_book_package/";
				if ($page<>"") {
					$page_url .= "/page/".$page;
				}
				if ($q<>"") {
					$page_url .= "/q/".$q;
				}
				if ($sort_name<>"") {
					$page_url .= "/sort_name/".$sort_name;
				}
				if ($sort_by<>"") {
					$page_url .= "/sort_by/".$sort_by;
				}
				$page_url .= "/offset";
			
			
				$this->load->library('pagination');
				$config['base_url'] = $page_url;
				$config['total_rows'] = $total_book_packages;
				$config['per_page'] = PAGING_TBL_BOOKS_CATEGORY;//constant
				$config['uri_segment'] = $this->uri->total_segments();
				$config['num_links'] = 5;
		
				$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #ccc; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
				$config['next_link_disable'] = '';
		
				$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #ccc; padding:5px; background-color:#eeeeee; margin-right:3px '><<Prev</span>&nbsp;&nbsp;";
				$config['prev_link_disable'] = "";		
					
				$config['first_link'] = "";
				$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
				$config['first_tag_close'] = '</span>';
				
				$config['last_link'] = "";
				$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
				$config['last_tag_close'] = '</span>';
		
				$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #ccc; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
				$config['cur_tag_close'] = "&nbsp;</span>";
				
				$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #ccc; padding:5px; background-color:#eeeeee; margin-right:3px'>";
				$config['num_tag_close'] = "&nbsp;</span>";
		
				$this->pagination->initialize($config);
				$start = $offset + 1;
				$range = "";
				if ($offset+PAGING_TBL_BOOKS_CATEGORY >= $total_book_packages) {
					$range = $total_book_packages;
				} else {
					$range = $offset+PAGING_TBL_BOOKS_CATEGORY;
				}
		
				$paging_string = "$start - $range <font color='#FFF'>of $total_book_packages packages</font>";
			
			$data['offset'] = $offset;
			$data['paging_string'] = $paging_string;
			$data['start'] = $start;
			$data['total_book_packages'] = $total_book_packages;
			$data['list_packages']       = $list_packages;
			$this->load->view('list_book_package',$data);
		}
	}
	
	public function add_books_package()
	{
		 $data['message'] = "";
		 if (isset($_SESSION) && trim($_SESSION['tbl_admin_id_sess']) == "") {
			header("Location: ".HOST_URL."/index/login/");
			exit;
		}else{
			$this->load->model('model_category');
			$sort_name = "";
			$sort_by   = "";
			$offset    = ""; 
			$q		 = "";
			$unique_id            = substr(md5(uniqid(rand())),0,15);
			if(isset($_POST['package_name_en']) || isset($_POST['package_name_ar']) )
			{
				$tbl_book_package_id            = isset($_POST['tbl_book_package_id'])? $_POST['tbl_book_package_id']:'' ;
				$package_name_en 		        = isset($_POST['package_name_en'])? $_POST['package_name_en']: '' ;
				$package_name_ar                = isset($_POST['package_name_ar'])? $_POST['package_name_ar']: '' ;
				$book_point                     = isset($_POST['book_point'])? $_POST['book_point']: '0' ;
				$age_group                      = isset($_POST['age_group_enc'])? $_POST['age_group_enc']: '' ;
			
				$save_book_package      = $this->model_category->save_book_package($tbl_book_package_id, $package_name_en, $package_name_ar, $book_point, $age_group);
				if($save_book_package=="N")
				{
					$data['message']         = "Book package already exist.";
				}else{
					$data['message']         = "Book package added successfully.";
				}
				//return $save_book_category;
			}
			$this->load->model('model_settings');
			$sort_name = "";
			$sort_by   = "";
			$offset    = ""; 
			$q		 = "";
			$list      = "Y";
					
			$package_group ="Y";
		    $list_age_group = $this->model_settings->get_all_age_groups($sort_name, $sort_by, $offset, $q="",$list, 'Y', $package_group);
			$data['list_age_group']   = $list_age_group;
			
			$this->load->view('add_books_package',$data);
		}
	}
	
	
	
	//Edit Section
	public function edit_books_package()
	{
	    $param_array = $this->uri->uri_to_assoc(3);
	    if (array_key_exists('pid',$param_array)) {
			$pid = $param_array['pid'];
		}
		$data['message'] = "";
		$offset          = 0;
		if (isset($_SESSION) && trim($_SESSION['tbl_admin_id_sess']) == "") {
			header("Location: ".HOST_URL."/index/login/");
			exit;
		}else{
			$this->load->model('model_category');
			if(isset($_POST['package_name_en']) || isset($_POST['package_name_ar']) )
			{
				
				$tbl_book_package_id         = $_POST['tbl_book_package_id'];
				$package_name_en 			 = $_POST['package_name_en'];
				$package_name_ar 			 = $_POST['package_name_ar'];
				$book_point                  = isset($_POST['book_point'])? $_POST['book_point']: '0' ;
				$age_group                   = isset($_POST['user_age_group'])? $_POST['user_age_group']: '' ;
				$is_exist = $this->model_category->is_exist_book_package($tbl_book_package_id, $package_name_en, $package_name_ar);
				if($is_exist=="N")
				{
						$update_book = $this->model_category->update_book_package($tbl_book_package_id, $package_name_en, $package_name_ar, $book_point, $age_group);
					 
						$data['message']         = "Book package is updated successfully.";
					
				}else{
					    $data['message']         = "Book package is already exist.";
					
				}
				
			}
			$sort_name = "";
			$sort_by   = "";
			$offset    = 0; 
			$q         = ""; 
			$this->load->model('model_category');
			$tbl_book_package_id = $pid;
			$getBookPackageInfo          = $this->model_category->getPackageDetails($tbl_book_package_id);
			$data['getBookPackageInfo']  = $getBookPackageInfo;
			
			$this->load->model('model_settings');
			$sort_name = "";
			$sort_by   = "";
			$offset    = ""; 
			$q		 = "";
			$list      = "Y";
					
			$package_group ="Y";
		    $list_age_group = $this->model_settings->get_all_age_groups($sort_name, $sort_by, $offset, $q="",$list, 'Y', $package_group);
			$data['list_age_group']   = $list_age_group;
			
			$this->load->view('edit_books_package',$data);
		}
	}
	
	//add_books_package
	
	
	function iframe_show_package_pic() {
		$data['page'] = "iframe_show_book_category_pic";
		$MSG = "";
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_book_package_id = "";
		
		if (array_key_exists('tbl_book_package_id',$param_array)) {
			$tbl_book_package_id = $param_array['tbl_book_package_id'];
		} 
		$this->load->model("model_category");
		$category_obj = $this->model_category->get_book_package_picture($tbl_book_package_id);
		if(!empty($category_obj))
		{
			$data['picture'] = $category_obj[0]['picture'];
			$data['tbl_book_category_id'] = $tbl_book_package_id;
			$data['MSG'] = $MSG;
		}else{
			$data['picture'] = "";
			$data['tbl_book_category_id'] = $tbl_book_package_id;
			$data['MSG'] = $MSG;
		}
		$this->load->view("iframe_show_book_category_pic",$data);
	}
	
	function upload_book_package_pic() {
		$data['page'] = "iframe_show_book_category_pic";
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_book_package_id = trim($_POST["tbl_book_package_id"]);
		}

		$this->load->model("model_category");
		$MSG = $this->model_category->update_book_package_pic($_FILES, $tbl_book_package_id);
		$category_obj = $this->model_category->get_book_package_picture_upload($tbl_book_package_id);
		if(!empty($category_obj))
		{
			$data['picture'] = $category_obj[0]['picture'];
			$data['tbl_book_category_id'] = $tbl_book_package_id;
			$data['MSG'] = $MSG;
		}else{
			$data['picture'] = "";
			$data['tbl_book_category_id'] = $tbl_book_package_id;
			$data['MSG'] = $MSG;
		}
		$this->load->view("iframe_show_book_category_pic",$data);
		/*//Picture is changed of logged in user so we change the value in session as well.
		if (trim($tbl_user_id) == trim($_SESSION['tbl_user_id_sess'])) {
			$_SESSION['picture_sess'] = $user_obj['picture'];
		}*/
	}
	
	/** Acivate book category*/
  	function activate_book_package() {
	    $pid   = $_POST['pid'];
		$this->load->model('model_category');
		$this->model_category->activate_book_package($pid);
		echo "Y";			
		exit;
	}

	/** Deacivate Book category */
  	function deactivate_book_package() {
	    $pid   = $_POST['pid'];
		$this->load->model('model_category');
		$this->model_category->deactivate_book_package($pid);
	    echo "Y";			
		exit;	
	}

	/** Delete book category */
  	function delete_book_package() {
        $pid   = $_POST['pid'];	
        $this->load->model('model_category');
		$this->model_category->delete_book_package($pid);
	    echo "Y";			
		exit;	
	}
	
	
	
	//ASSIGN BOOKS TO PACKAGE
	
	public function assign_books_to_package()
	{
		 if (isset($_SESSION) && trim($_SESSION['tbl_admin_id_sess']) == "") {
			header("Location: ".HOST_URL."/index/login/");
			exit;
		}else{
			
			 if($_SESSION['admin_type_sess']=="SA" || $can_create_books=="Y"){
				 $this->load->model('model_book');
				 $this->load->model('model_category');
				 $this->load->model('model_settings');
				   
				  $sort_by    = "ASC";
				  $sort_name  = "title_en";
				  $offset     = 0;
				  $total_books= 0;
				  $q          = "";
				  $tbl_category_id = "";
				  $page = "";
				   
				   
				   $param_array = $this->uri->uri_to_assoc(3);
					if (array_key_exists('page',$param_array)) {
						$page = $param_array['page'];
					}	
				   if (array_key_exists('offset',$param_array)) {
						$offset = $param_array['offset'];
						if (trim($offset) == "") {
							$offset = 0;	
						}
					}	
					if (array_key_exists('sort_name',$param_array)) {
						$sort_name = $param_array['sort_name'];
						
						switch($sort_name) {
							case("R"): {
								 $sort_name = "id";
								 break;
							}
							case("TR"): {
								 $sort_name = "id";
								 break;
							}
							default: {
								$sort_name = "title_en";
							}					
						}
					}	 
					if (array_key_exists('sort_by',$param_array)) {
						$sort_by = $param_array['sort_by'];
					}else{
						$sort_by = "ASC";
					}
				
					if (array_key_exists('q',$param_array)) {
						$q = trim(urldecode($param_array['q']));
					}
					
					 if (array_key_exists('tbl_category_id',$param_array)) {
						$tbl_category_id = $param_array['tbl_category_id'];
					}
					
					 if (array_key_exists('tbl_book_package_id',$param_array)) {
						$tbl_book_package_id = $param_array['tbl_book_package_id'];
						$data['tbl_book_package_id'] = $tbl_book_package_id;
					}
					
					$package_info     = $this->model_category->get_package_info($tbl_book_package_id);
					$assigned_books   = $this->model_category->get_assigned_books_of_package($tbl_book_package_id);
					
					
					
					$list_books  = $this->model_book->get_all_books($sort_name, $sort_by, $offset, $q,$tbl_category_id,'');
					
					$sort_name = "";
					$sort_by   = "";
					$offset    = ""; 
					$q		 = "";
			        $list      = "Y";
					
					$package_group ="Y";
		            $list_age_group = $this->model_settings->get_all_age_groups($sort_name, $sort_by, $offset, $q="",$list, 'Y', $package_group);
					
					for($n=0;$n<count($list_books);$n++)
					{
						$dataRatings    = array();
						$tbl_book_id    = $list_books[$n]['tbl_book_id'];
						$dataRatings 	=	$this->model_book->getUserRatings($tbl_book_id);
						//print_r($dataRatings);
						$earnedRatings  = 0;
						$totalViews     = 0;
						$totalRatings   = 0;
						if($dataRatings[0]['cnt_view']>0)
						{	   
								$earnedRatings    = $dataRatings[0]['total_rating'];
								$totalRatings     = $dataRatings[0]['cnt_view'] * 5;
								$ratePercentage   = ($earnedRatings / $totalRatings) * 100 ;
							
						}else{
							$ratePercentage   = 0;
							
						}
						$list_books[$n]['ratePercentage'] = $ratePercentage;
						$dataViews 	=	$this->model_book->getUserViews($tbl_book_id);
						$list_books[$n]['cntViews']     = $dataViews[0]['cnt_view'];
						$list_books[$n]['cntReviews']   =	$this->model_book->get_total_book_reviews_bid('','',$tbl_book_id);
						$list_books[$n]['cntQuestions'] =	$this->model_book->getCntQuestions($tbl_book_id,'');
						
					}
					
					//print_r($list_books);
					$total_books = $this->model_book->get_total_books($q,$tbl_category_id,'');
			
				 //PAGINATION CLASS
					$page_url = HOST_URL."/books/assign_books_to_package/";
					if ($page<>"") {
						$page_url .= "/page/".$page;
					}
					if ($q<>"") {
						$page_url .= "/q/".$q;
					}
					if ($tbl_book_package_id<>"") {
						$page_url .= "/tbl_book_package_id/".$tbl_book_package_id;
					}
					if ($sort_name<>"") {
						$page_url .= "/sort_name/".$sort_name;
					}
					if ($sort_by<>"") {
						$page_url .= "/sort_by/".$sort_by;
					}
					$page_url .= "/offset";
				
				
					$this->load->library('pagination');
					$config['base_url'] = $page_url;
					$config['total_rows'] = $total_books;
					$config['per_page'] = PAGING_TBL_BOOKS;//constant
					$config['uri_segment'] = $this->uri->total_segments();
					$config['num_links'] = 5;
			
					$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #ccc; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
					$config['next_link_disable'] = '';
			
					$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #ccc; padding:5px; background-color:#eeeeee; margin-right:3px '><<Prev</span>&nbsp;&nbsp;";
					$config['prev_link_disable'] = "";		
						
					$config['first_link'] = "";
					$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
					$config['first_tag_close'] = '</span>';
					
					$config['last_link'] = "";
					$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
					$config['last_tag_close'] = '</span>';
			
					$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #ccc; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
					$config['cur_tag_close'] = "&nbsp;</span>";
					
					$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #ccc; padding:5px; background-color:#eeeeee; margin-right:3px'>";
					$config['num_tag_close'] = "&nbsp;</span>";
			
					$this->pagination->initialize($config);
					$start = $offset + 1;
					$range = "";
					if ($offset+PAGING_TBL_BOOKS >= $total_books) {
						$range = $total_books;
					} else {
						$range = $offset+PAGING_TBL_BOOKS;
					}
			
					$paging_string = "$start - $range <font color='#FFF'>of $total_books books</font>";
				
				$data['offset'] = $offset;
				$data['paging_string'] = $paging_string;
				$data['start'] = $start;
				$data['total_books']      = $total_books;
				$data['list_age_group']   = $list_age_group;
				$data['list_books']       = $list_books;
				$data['search_data']      = $q;
				$data['package_info']     = $package_info;
				$data['assigned_books']   = $assigned_books;
				
				$this->load->view('assign_books_to_package', $data);
			 }else{
			  header("Location: ".HOST_URL."/index/");
			  exit; 
				 
			 }
		}
	}
	
	
	function assignBookToPackage()
	{
		
		if(isset($_POST) && count($_POST) != 0) {
			
			$tbl_book_package_id   = $_POST['tbl_book_package_id'];
			$str = $_POST['book_id_enc']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('book_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
			
			$this->load->model("model_category");
				
			for ($i=0; $i<count($str); $i++) {
				if($str[$i]<>""){
				$this->model_category->assign_books_to_package($str[$i],$tbl_book_package_id);
				}
			}
			$assigned_books   = $this->model_category->get_assigned_books_of_package($tbl_book_package_id);
			$data        = '<div>';
			for($k=0;$k<count($assigned_books);$k++)
			{
			    $tbl_book_id                    =  $assigned_books[$k]['tbl_book_id'];
				$tbl_package_assigned_book_id   =  $assigned_books[$k]['tbl_package_assigned_book_id'];
				$book_name     =  $assigned_books[$k]['title_en']." [::] ".$assigned_books[$k]['title_ar'];
				
				
				$divId =  "div_".$tbl_package_assigned_book_id;
				$assigned_id = "'".$tbl_package_assigned_book_id."'";
						  
							
				$data .='<div style="float:left; clear:both; margin-bottom:10px;" id="'.$divId.'" >&nbsp;&nbsp;';
				$data .= '<div style="float:left;padding-left:20px;">'.$book_name.'<a style="cursor:pointer;"  onClick="delBook('.$assigned_id.');">&nbsp;&nbsp;&nbsp;<i title="Delete" aria-hidden="true" class="fa fa-trash-o"></i></a></div></div>';
			}
			$data             .= '</div>';
			echo $data;
			exit;
		
		} else{
		}
		
	}
	
	
	
	function delBookFromPackage()
	{
		$tbl_book_package_id            = $_POST['tbl_book_package_id'];
		$tbl_package_assigned_book_id   = $_POST['tbl_package_assigned_book_id'];
		$this->load->model("model_category");
		$this->model_category->del_assign_book_from_package($tbl_package_assigned_book_id,$tbl_book_package_id);
	}
	
	//END BACKEND FUNCTIONALITY
	
	// BOOKS UPLOADED BY PUBLIC - WAITING FOR APPROVAL
	public function books_for_approval()
	{
		 if (isset($_SESSION) && trim($_SESSION['tbl_admin_id_sess']) == "") {
			header("Location: ".HOST_URL."/index/login/");
			exit;
		}else{
			
			 if($_SESSION['admin_type_sess']=="SA" || $can_create_books=="Y"){
				 $this->load->model('model_book');
				   
				  $sort_by    = "ASC";
				  $sort_name  = "title_en";
				  $offset     = 0;
				  $total_books= 0;
				  $q          = "";
				  $tbl_category_id = "";
				  $page = "";
				   
				   
				   $param_array = $this->uri->uri_to_assoc(3);
					if (array_key_exists('page',$param_array)) {
						$page = $param_array['page'];
					}	
				   if (array_key_exists('offset',$param_array)) {
						$offset = $param_array['offset'];
						if (trim($offset) == "") {
							$offset = 0;	
						}
					}	
					if (array_key_exists('sort_name',$param_array)) {
						$sort_name = $param_array['sort_name'];
						
						switch($sort_name) {
							case("R"): {
								 $sort_name = "id";
								 break;
							}
							case("TR"): {
								 $sort_name = "id";
								 break;
							}
							default: {
								$sort_name = "title_en";
							}					
						}
					}	 
					if (array_key_exists('sort_by',$param_array)) {
						$sort_by = $param_array['sort_by'];
					}else{
						$sort_by = "ASC";
					}
				
					if (array_key_exists('q',$param_array)) {
						$q = trim(urldecode($param_array['q']));
					}
					
					 if (array_key_exists('tbl_category_id',$param_array)) {
						$tbl_category_id = $param_array['tbl_category_id'];
					}
					
					$list_books  = $this->model_book->get_all_books_for_approval($sort_name, $sort_by, $offset, $q,$tbl_category_id,'');
					for($n=0;$n<count($list_books);$n++)
					{
						$dataRatings    = array();
						$tbl_book_id    = $list_books[$n]['tbl_book_id'];
						$dataRatings 	=	$this->model_book->getUserRatings($tbl_book_id);
						//print_r($dataRatings);
						$earnedRatings  = 0;
						$totalViews     = 0;
						$totalRatings   = 0;
						if($dataRatings[0]['cnt_view']>0)
						{	   
								$earnedRatings    = $dataRatings[0]['total_rating'];
								$totalRatings     = $dataRatings[0]['cnt_view'] * 5;
								$ratePercentage   = ($earnedRatings / $totalRatings) * 100 ;
							
						}else{
							$ratePercentage   = 0;
							
						}
						$list_books[$n]['ratePercentage'] = $ratePercentage;
						$dataViews 	=	$this->model_book->getUserViews($tbl_book_id);
						$list_books[$n]['cntViews']     = $dataViews[0]['cnt_view'];
						$list_books[$n]['cntReviews']   =	$this->model_book->get_total_book_reviews_bid('','',$tbl_book_id);
						$list_books[$n]['cntQuestions'] =	$this->model_book->getCntQuestions($tbl_book_id,'');
						
					}
					
					//print_r($list_books);
					$total_books = $this->model_book->get_total_books_for_approval($q,$tbl_category_id,'');
			
				 //PAGINATION CLASS
					$page_url = HOST_URL."/books/index/";
					if ($page<>"") {
						$page_url .= "/page/".$page;
					}
					if ($q<>"") {
						$page_url .= "/q/".$q;
					}
					if ($sort_name<>"") {
						$page_url .= "/sort_name/".$sort_name;
					}
					if ($sort_by<>"") {
						$page_url .= "/sort_by/".$sort_by;
					}
					$page_url .= "/offset";
				
				
					$this->load->library('pagination');
					$config['base_url'] = $page_url;
					$config['total_rows'] = $total_books;
					$config['per_page'] = PAGING_TBL_BOOKS;//constant
					$config['uri_segment'] = $this->uri->total_segments();
					$config['num_links'] = 5;
			
					$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #ccc; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
					$config['next_link_disable'] = '';
			
					$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #ccc; padding:5px; background-color:#eeeeee; margin-right:3px '><<Prev</span>&nbsp;&nbsp;";
					$config['prev_link_disable'] = "";		
						
					$config['first_link'] = "";
					$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
					$config['first_tag_close'] = '</span>';
					
					$config['last_link'] = "";
					$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
					$config['last_tag_close'] = '</span>';
			
					$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #ccc; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
					$config['cur_tag_close'] = "&nbsp;</span>";
					
					$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #ccc; padding:5px; background-color:#eeeeee; margin-right:3px'>";
					$config['num_tag_close'] = "&nbsp;</span>";
			
					$this->pagination->initialize($config);
					$start = $offset + 1;
					$range = "";
					if ($offset+PAGING_TBL_BOOKS >= $total_books) {
						$range = $total_books;
					} else {
						$range = $offset+PAGING_TBL_BOOKS;
					}
			
					$paging_string = "$start - $range <font color='#FFF'>of $total_books books</font>";
				
				$data['offset'] = $offset;
				$data['paging_string'] = $paging_string;
				$data['start'] = $start;
				$data['total_books'] = $total_books;
				$data['list_books'] = $list_books;
				$data['search_data'] = $q;
				$this->load->view('books_for_approval', $data);
			 }else{
			  header("Location: ".HOST_URL."/index/");
			  exit; 
				 
			 }
		}
	}
	public function book_details_for_approval()
	{
	    $param_array = $this->uri->uri_to_assoc(3);
	    if (array_key_exists('bid',$param_array)) {
			$bid = $param_array['bid'];
		}
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
		}
		
		$this->load->model('model_book');	
		$this->load->model('model_category');
		$this->load->model('model_settings');
			
		$tbl_book_id = $bid;
		$getBookInfo = $this->model_book->getBookInfo($tbl_book_id);
		
		$dataRatings = $this->model_book->getUserRatings($tbl_book_id);
		//print_r($dataRatings);
		$earnedRatings  = 0;
		$totalViews     = 0;
		$totalRatings   = 0;
		if($dataRatings[0]['cnt_view']>0)
		{	   
			$earnedRatings    = $dataRatings[0]['total_rating'];
			$totalRatings     = $dataRatings[0]['cnt_view'] * 5;
			$ratePercentage   = ($earnedRatings / $totalRatings) * 100 ;
		}else{
			$ratePercentage   = 0;
		}
		$data['ratePercentage'] = $ratePercentage;
		$tbl_book_category_id   = $getBookInfo[0]['tbl_book_category_id'];
		$category_info = $this->model_category->getCategoryDetails($tbl_book_category_id);
		$data['category_name_en'] = $category_info[0]['category_name_en'];
		$data['category_name_ar'] = $category_info[0]['category_name_ar'];
		
		$dataViews 	             =	$this->model_book->getUserViews($tbl_book_id);
		$data['cntViews']          = $dataViews[0]['cnt_view'];
		$data['getBookInfo']       = $getBookInfo;
		$data['offset']            = $offset;
		$this->load->view('book_details_for_approval',$data);
	}
	
	/** Publish book */
  	function publish_book() {
	    $bid   = $_POST['bid'];
		$this->load->model('model_book');
		$this->load->model('model_user');
		$this->model_book->publish_book($bid);
		
		$getBookInfo   = $this->model_book->getBookInfo($bid);
		$title_en      = ucfirst($getBookInfo[0]['title_en']);
		$title_ar      = $getBookInfo[0]['title_ar'];
		$title         .= $title_en;
		if($title_ar<>"")
			$title         .= "&nbsp;'".$title_ar."'";
			
		$tbl_user_id   = $getBookInfo[0]['added_by'];
		
		$getUserInfo   = $this->model_user->get_student_obj($tbl_user_id);
		$messageBook   = 'Your book '.$title.' is published. To read please go to My Books';

		$message_from   = "BOOK & BOOK"; //super admin
		$message_to     = $tbl_user_id;
		$tbl_user_id    = $message_to;
		$message_type   = "message";
		$this->model_user->save_message($message_from, $message_to, $tbl_user_id, $message_type, $messageBook);
		
		echo "Y";			
		exit;
	}
	
	
	function delete_publish_book() {
	    $bid   = $_POST['bid'];
		$this->load->model('model_book');
		$this->model_book->delete_publish_book($bid);
		echo "Y";			
		exit;
	}
	
	//Edit Section
	public function edit_book_approval()
	{
	    $param_array = $this->uri->uri_to_assoc(3);
	    if (array_key_exists('bid',$param_array)) {
			$bid = $param_array['bid'];
		}
		$data['message'] = "";
		$offset          = 0;
		if (isset($_SESSION) && trim($_SESSION['tbl_admin_id_sess']) == "") {
			header("Location: ".HOST_URL."/index/login/");
			exit;
		}else{
			$this->load->model('model_book');
			if(isset($_POST['title_en']) || isset($_POST['title_ar']) )
			{
				
				$tbl_book_id 		  = $_POST['tbl_book_id'];
				$tbl_book_category_id = $_POST['tbl_book_category_id'];
				$tbl_language_id      = $_POST['tbl_language_id'];
				$title_en 			 = addslashes($_POST['title_en']);
				$title_ar 			 = addslashes($_POST['title_ar']);
				$author_name_en 	   = addslashes($_POST['author_name_en']);
				$author_name_ar       = addslashes($_POST['author_name_ar']);
				$isbn 			     = $_POST['isbn'];
				$description_en       = addslashes($_POST['description_en']);
				$description_ar       = addslashes($_POST['description_ar']);
				$user_age_group       = $_POST['user_age_group'];
				$book_point           = $_POST['book_point'];
				$total_page           = $_POST['total_page'];
				$book_price           = $_POST['book_price'];
				
				
				$is_exist = $this->model_book->is_exist_book($tbl_book_id, $tbl_language_id, $title_en, $title_ar);
				if($is_exist=="N")
				{
						$update_book = $this->model_book->update_book($tbl_book_id,$tbl_book_category_id, $tbl_language_id, $title_en, $title_ar, $author_name_en, $author_name_ar, $isbn, $description_en, $description_ar,$user_age_group,$book_point,$total_page,$book_price);
					    $update_file = $this->model_book->update_file_to_book($tbl_book_id);
						$data['message']         = "Book is updated successfully.";
					
				}else{
					$data['message']         = "Book title is already exist.";
					
				}
				
			}
			
			$this->load->model('model_category');
			$this->load->model('model_settings');
			$sort_name = "";
			$sort_by   = "";
			$offset    = ""; 
			$q		 = "";
			$list      = "Y";
			
			
			$tbl_book_id = $bid;
			$getBookInfo = $this->model_book->getBookInfo($tbl_book_id);
		
			//$is_email_exists = $this->model_user->validate_user_exists($emiratesId);
			$list_category = $this->model_category->get_all_book_categories($sort_name, $sort_by, $offset, $q, $list, 'Y');
			
			$package_group ="N";
		    $list_age_group = $this->model_settings->get_all_age_groups($sort_name, $sort_by, $offset, $q="",$list, 'Y', $package_group);

			$data['list_category']   	 = $list_category;
			$data['list_age_group'] 	= $list_age_group;
			$data['getBookInfo']       = $getBookInfo;
			$this->load->view('edit_book_approval',$data);
		}
	}
	
	//END PUBLIC BOOKS FOR APPROVAL
	
    // REQUEST BOOKS - BOOK ORDERS
	// BOOKS UPLOADED BY PUBLIC - WAITING FOR APPROVAL
	public function books_orders()
	{
		 if (isset($_SESSION) && trim($_SESSION['tbl_admin_id_sess']) == "") {
			header("Location: ".HOST_URL."/index/login/");
			exit;
		}else{
			
			 if($_SESSION['admin_type_sess']=="SA" || $can_create_books=="Y"){
				 $this->load->model('model_book');
				   
				  $sort_by    = "ASC";
				  $sort_name  = "added_date";
				  $offset     = 0;
				  $total_books= 0;
				  $q          = "";
				  $tbl_category_id = "";
				  $page = "";
				   
				   
				   $param_array = $this->uri->uri_to_assoc(3);
					if (array_key_exists('page',$param_array)) {
						$page = $param_array['page'];
					}	
				   if (array_key_exists('offset',$param_array)) {
						$offset = $param_array['offset'];
						if (trim($offset) == "") {
							$offset = 0;	
						}
					}	
					if (array_key_exists('sort_name',$param_array)) {
						$sort_name = $param_array['sort_name'];
						
						switch($sort_name) {
							case("R"): {
								 $sort_name = "id";
								 break;
							}
							case("TR"): {
								 $sort_name = "id";
								 break;
							}
							default: {
								$sort_name = "title_en";
							}					
						}
					}	 
					if (array_key_exists('sort_by',$param_array)) {
						$sort_by = $param_array['sort_by'];
					}else{
						$sort_by = "ASC";
					}
				
					if (array_key_exists('q',$param_array)) {
						$q = trim(urldecode($param_array['q']));
					}
					
					 if (array_key_exists('tbl_category_id',$param_array)) {
						$tbl_category_id = $param_array['tbl_category_id'];
					}
					
					$list_books  = $this->model_book->get_all_books_orders($sort_name, $sort_by, $offset, $q,$tbl_category_id,'');
					for($n=0;$n<count($list_books);$n++)
					{
						$dataRatings    = array();
						$tbl_book_id    = $list_books[$n]['tbl_book_id'];
						$dataRatings 	=	$this->model_book->getUserRatings($tbl_book_id);
						//print_r($dataRatings);
						$earnedRatings  = 0;
						$totalViews     = 0;
						$totalRatings   = 0;
						if($dataRatings[0]['cnt_view']>0)
						{	   
								$earnedRatings    = $dataRatings[0]['total_rating'];
								$totalRatings     = $dataRatings[0]['cnt_view'] * 5;
								$ratePercentage   = ($earnedRatings / $totalRatings) * 100 ;
							
						}else{
							$ratePercentage   = 0;
							
						}
						$list_books[$n]['ratePercentage'] = $ratePercentage;
						$dataViews 	=	$this->model_book->getUserViews($tbl_book_id);
						$list_books[$n]['cntViews']     = $dataViews[0]['cnt_view'];
						$list_books[$n]['cntReviews']   =	$this->model_book->get_total_book_reviews_bid('','',$tbl_book_id);
						$list_books[$n]['cntQuestions'] =	$this->model_book->getCntQuestions($tbl_book_id,'');
						
					}
					
					//print_r($list_books);
					$total_books = $this->model_book->get_total_books_orders($q,$tbl_category_id,'');
			
				 //PAGINATION CLASS
					$page_url = HOST_URL."/books/index/";
					if ($page<>"") {
						$page_url .= "/page/".$page;
					}
					if ($q<>"") {
						$page_url .= "/q/".$q;
					}
					if ($sort_name<>"") {
						$page_url .= "/sort_name/".$sort_name;
					}
					if ($sort_by<>"") {
						$page_url .= "/sort_by/".$sort_by;
					}
					$page_url .= "/offset";
				
				
					$this->load->library('pagination');
					$config['base_url'] = $page_url;
					$config['total_rows'] = $total_books;
					$config['per_page'] = PAGING_TBL_BOOKS;//constant
					$config['uri_segment'] = $this->uri->total_segments();
					$config['num_links'] = 5;
			
					$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #ccc; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
					$config['next_link_disable'] = '';
			
					$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #ccc; padding:5px; background-color:#eeeeee; margin-right:3px '><<Prev</span>&nbsp;&nbsp;";
					$config['prev_link_disable'] = "";		
						
					$config['first_link'] = "";
					$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
					$config['first_tag_close'] = '</span>';
					
					$config['last_link'] = "";
					$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
					$config['last_tag_close'] = '</span>';
			
					$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #ccc; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
					$config['cur_tag_close'] = "&nbsp;</span>";
					
					$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #ccc; padding:5px; background-color:#eeeeee; margin-right:3px'>";
					$config['num_tag_close'] = "&nbsp;</span>";
			
					$this->pagination->initialize($config);
					$start = $offset + 1;
					$range = "";
					if ($offset+PAGING_TBL_BOOKS >= $total_books) {
						$range = $total_books;
					} else {
						$range = $offset+PAGING_TBL_BOOKS;
					}
			
					$paging_string = "$start - $range <font color='#FFF'>of $total_books orders</font>";
				
				$data['offset'] = $offset;
				$data['paging_string'] = $paging_string;
				$data['start'] = $start;
				$data['total_books'] = $total_books;
				$data['list_books'] = $list_books;
				$data['search_data'] = $q;
				$this->load->view('books_orders', $data);
			 }else{
			  header("Location: ".HOST_URL."/index/");
			  exit; 
				 
			 }
		}
	}
	
	
	
	/** Request Book Delivered */
  	function delivered_book() {
	    $brid   = $_POST['brid'];
		$this->load->model('model_book');
		$this->model_book->delivered_book($brid);
		echo "Y";			
		exit;
	}
	
	
	public function book_details_of_request()
	{
	    $param_array = $this->uri->uri_to_assoc(3);
	    if (array_key_exists('bid',$param_array)) {
			$bid = $param_array['bid'];
		}
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
		}
		
		$this->load->model('model_book');	
		$this->load->model('model_category');
		$this->load->model('model_settings');
			
		$tbl_book_id = $bid;
		$getBookInfo = $this->model_book->getBookInfo($tbl_book_id);
		
		$dataRatings = $this->model_book->getUserRatings($tbl_book_id);
		//print_r($dataRatings);
		$earnedRatings  = 0;
		$totalViews     = 0;
		$totalRatings   = 0;
		if($dataRatings[0]['cnt_view']>0)
		{	   
			$earnedRatings    = $dataRatings[0]['total_rating'];
			$totalRatings     = $dataRatings[0]['cnt_view'] * 5;
			$ratePercentage   = ($earnedRatings / $totalRatings) * 100 ;
		}else{
			$ratePercentage   = 0;
		}
		$data['ratePercentage'] = $ratePercentage;
		$tbl_book_category_id   = $getBookInfo[0]['tbl_book_category_id'];
		$category_info = $this->model_category->getCategoryDetails($tbl_book_category_id);
		$data['category_name_en'] = $category_info[0]['category_name_en'];
		$data['category_name_ar'] = $category_info[0]['category_name_ar'];
		
		$dataViews 	             =	$this->model_book->getUserViews($tbl_book_id);
		$data['cntViews']          = $dataViews[0]['cnt_view'];
		$data['getBookInfo']       = $getBookInfo;
		$data['offset']            = $offset;
		$this->load->view('book_details_of_request',$data);
	}
	
	//END BOOK ORDERS
	
	
	
	
	
	
	
	
}
