<?php
/**
 * @desc   	  	Survey Controller
 * @category   	Controller
 * @author     	Shanavas
 * @version    	0.1
 */
class Survey extends CI_Controller {

	/**
	* @desc    Default function for the Controller
	* @param   none
	* @access  default
	*/
    function index() {
	}

	 /**
	* @desc    Show all survey
	* @param   none
	* @access  default
	*/
    function all_survey() {
		$data['page'] = "view_all_survey";
		$data['menu'] = "survey";
        $data['sub_menu'] = "survey";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "id";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "question_text_en";
					 break;
				}
				default: {
					$sort_name = "question_text_en";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_survey");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			$rs_all_survey      = $this->model_survey->get_all_survey($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id);
			$total_survey       = $this->model_survey->get_total_survey($q, $is_active, $tbl_school_id);
		}else{
			$rs_all_survey     = array();
			$total_survey      = array();
		}
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/survey/all_survey";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_school_type_id) && trim($tbl_student_id)!="") {
			$page_url .= "/tbl_student_id/".rawurlencode($tbl_student_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_survey;
		$config['per_page'] = TBL_SURVEY_QUESTIONS_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_SURVEY_QUESTIONS_PAGING >= $total_survey) {
			$range = $total_survey;
		} else {
			$range = $offset+TBL_SURVEY_QUESTIONS_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_survey questions</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_survey']	     = $rs_all_survey;
		$data['total_survey'] 	      = $total_survey;

		$this->load->view('admin/view_template',$data);
	}
	
	function is_exist_survey_question() {
		if ($_POST) {
			$tbl_survey_questions_id= $_POST['tbl_survey_questions_id'];		
			$question_text_en       = $_POST['question_text_en'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_survey");		
		$results = $this->model_survey->is_exist_survey_question($tbl_survey_questions_id, $question_text_en, $tbl_school_id);
		if(count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
    /**
	* @desc    Add survey question
	* @param   POST array
	* @access  default
	*/
    function add_survey_question() {
		if ($_POST) {
			$tbl_survey_questions_id = $_POST['tbl_survey_questions_id'];		
			$question_text_en        = $_POST['question_text_en'];
			$question_text_ar        = $_POST['question_text_ar'];
			$option_1_en             = $_POST['option_1_en'];
			$option_1_ar             = $_POST['option_1_ar'];
			$option_2_en             = $_POST['option_2_en'];
			$option_2_ar             = $_POST['option_2_ar'];
			$option_3_en             = $_POST['option_3_en'];
			$option_3_ar             = $_POST['option_3_ar'];
			$option_4_en             = $_POST['option_4_en'];
			$option_4_ar             = $_POST['option_4_ar'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_survey");	
		$resData = $this->model_survey->add_survey_question($tbl_survey_questions_id, $question_text_en, $question_text_ar, $option_1_en, $option_1_ar, $option_2_en, $option_2_ar, 
		                                           $option_3_en, $option_3_ar, $option_4_en, $option_4_ar, $tbl_school_id);
		if ($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}

    /**
	* @desc    Activate Survey Question
	* @param   String 
	* @access  default
	*/
    function activateSurveyQuestion() {
		if(isset($_POST) && count($_POST) != 0) {
			$survey_questions_id_enc = trim($_POST["survey_questions_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_survey");		
		$this->model_survey->activate_survey_question($survey_questions_id_enc,$tbl_school_id);
	}


	/**
	* @desc    Deactivate Survey Question
	* @param   String school_type_id_enc
	* @access  default
	*/
    function deactivateSurveyQuestion() {
		if(isset($_POST) && count($_POST) != 0) {
			$survey_questions_id_enc = trim($_POST["survey_questions_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_survey");		
		$this->model_survey->deactivate_survey_question($survey_questions_id_enc,$tbl_school_id);
	}

     /**
	* @desc    Delete Survey Question
	* @param   POST array
	* @access  default
	*/
     
	function deleteSurveyQuestion() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['survey_questions_id_enc']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('survey_questions_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_survey");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_survey->delete_survey_question($str[$i],$tbl_school_id);
		}
	}
	
	/**
	* @desc    Save changes
	* @param   POST array
	* @access  default
	*/
    function save_survey_question_changes() {
		if ($_POST) {
		    $tbl_survey_questions_id = $_POST['tbl_survey_questions_id'];		
			$question_text_en        = $_POST['question_text_en'];
			$question_text_ar        = $_POST['question_text_ar'];
			
			$option_1_id             = $_POST['option_1_id'];
			$option_1_en             = $_POST['option_1_en'];
			$option_1_ar             = $_POST['option_1_ar'];
			
			$option_2_id             = $_POST['option_2_id'];
			$option_2_en             = $_POST['option_2_en'];
			$option_2_ar             = $_POST['option_2_ar'];
			
			$option_3_id             = $_POST['option_3_id'];
			$option_3_en             = $_POST['option_3_en'];
			$option_3_ar             = $_POST['option_3_ar'];
			
			$option_4_id             = $_POST['option_4_id'];
			$option_4_en             = $_POST['option_4_en'];
			$option_4_ar             = $_POST['option_4_ar'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_survey");	
		$resData = $this->model_survey->save_survey_question_changes($tbl_survey_questions_id, $question_text_en, $question_text_ar, $option_1_id, $option_1_en, $option_1_ar, $option_2_id, $option_2_en, $option_2_ar, $option_3_id, $option_3_en, $option_3_ar, $option_4_id, $option_4_en, $option_4_ar, $tbl_school_id);
		
		if ($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}


	/**
	* @desc    Edit Survey
	* @param   none
	* @access  default
	*/
    function edit_survey_question() {
	    $data['page'] = "view_all_survey";
		$data['menu'] = "survey";
        $data['sub_menu'] = "survey";
		$data['mid'] = "3";
		$this->load->model("model_survey");
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_event_id = 0;
		if (array_key_exists('survey_questions_id_enc',$param_array)) {
			$tbl_survey_questions_id = $param_array['survey_questions_id_enc'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess']; 
		
		//Survey Question details
		$survey_question_obj        = $this->model_survey->get_survey_question_obj($tbl_survey_questions_id,$tbl_school_id);
		$survey_question_option_obj = $this->model_survey->get_survey_question_option_obj($tbl_survey_questions_id,$tbl_school_id);
		
		$data['survey_question_obj']        = $survey_question_obj;
		$data['survey_question_option_obj'] = $survey_question_option_obj;
		$this->load->view('admin/view_template', $data);
	}
	
	/**
	* @desc    Stats survey
	* @param   none
	* @access  default
	*/
	function stats_survey() {
		$data['page'] = "view_all_survey";
		$data['menu'] = "survey";
        $data['sub_menu'] = "survey";
		$data['mid'] = "4";

		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_survey_id = 0;
					
		if (array_key_exists('survey_questions_id_enc',$param_array)) {
			$tbl_survey_questions_id = $param_array['survey_questions_id_enc'];
		}	 
		
		//Survey details
		$this->load->model("model_survey");
		
		//Survey Question details
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess']; 
		$survey_question_obj        = $this->model_survey->get_survey_question_obj($tbl_survey_questions_id,$tbl_school_id);
		$survey_question_option_cnt = $this->model_survey->get_survey_question_option_cnt($tbl_survey_questions_id,$tbl_school_id);
	
	    $data['survey_question_obj']        = $survey_question_obj;
		$data['survey_question_option_obj'] = $survey_question_option_cnt;
		$this->load->view('admin/view_template', $data);
	}   

   
}
?>