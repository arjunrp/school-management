<?php
/**
 * @desc   	  	Event Controller
 * @category   	Controller
 * @author     	Shanavas PK
 * @version    	0.1
 */
class Event extends CI_Controller {
	/**
	* @desc    Default function for the Controller
	* @param   none
	* @access  default
	*/
    function index() {
	}

    /**
	* @desc    Show all events
	* @param   none
	* @access  default
	*/
    function all_events() {
		$data['page'] = "view_all_events";
		$data['menu'] = "events";
        $data['sub_menu'] = "events";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "id";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "title";
					 break;
				}
				default: {
					$sort_name = "title";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_events");
		$total_events      = 0;
		$rs_all_events     = array();
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			$rs_all_events      = $this->model_events->get_all_events($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id);
			$total_events       = $this->model_events->get_total_events($q, $is_active, $tbl_school_id);
		}
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/event/all_events";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_school_type_id) && trim($tbl_student_id)!="") {
			$page_url .= "/tbl_student_id/".rawurlencode($tbl_student_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_events;
		$config['per_page'] = TBL_EVENTS_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_EVENTS_PAGING >= $total_events) {
			$range = $total_events;
		} else {
			$range = $offset+TBL_EVENTS_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_events events</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_events']	     = $rs_all_events;
		$data['total_events'] 	      = $total_events;

		$this->load->view('admin/view_template',$data);
	}
	
	function is_exist_event() {
		if ($_POST) {
			$tbl_event_id           = $_POST['tbl_event_id'];		
			$title                  = $_POST['title'];
			$title_ar               = $_POST['title_ar'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_events");		
		$results = $this->model_events->is_exist_event($tbl_event_id, $title, $title_ar, $tbl_school_id);
		if(count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
    /**
	* @desc    Add Event
	* @param   POST array
	* @access  default
	*/
    function add_event() {
		if ($_POST) {
			$tbl_event_id        = $_POST['tbl_event_id'];		
			$title               = $_POST['title'];
			$title_ar            = $_POST['title_ar'];
			$description         = $_POST['description'];
			$description_ar      = $_POST['description_ar'];
			$start_date     	  = date("Y-m-d", strtotime($_POST['start_date']));
			$end_date     	    = date("Y-m-d", strtotime($_POST['end_date']));		
			$start_time          = $_POST['start_time'];
			
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_events");	
		$resData = $this->model_events->add_event($tbl_event_id, $title, $title_ar, $description, $description_ar, $start_date, $end_date, $start_time, $tbl_school_id);
		if (trim($resData)=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}

    /**
	* @desc    Activate event
	* @param   String 
	* @access  default
	*/
    function activateEvent() {
		if(isset($_POST) && count($_POST) != 0) {
			$event_id_enc = trim($_POST["event_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_events");		
		$this->model_events->activate_event($event_id_enc,$tbl_school_id);
	}


	/**
	* @desc    Deactivate event
	* @param   String school_type_id_enc
	* @access  default
	*/
    function deactivateEvent() {
		if(isset($_POST) && count($_POST) != 0) {
			$event_id_enc = trim($_POST["event_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_events");		
		$this->model_events->deactivate_event($event_id_enc,$tbl_school_id);
	}

     /**
	* @desc    Delete event
	* @param   POST array
	* @access  default
	*/
     
	function deleteEvent() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['event_id_enc']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('event_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_events");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_events->delete_event($str[$i],$tbl_school_id);
		}
	}
	
	/**
	* @desc    Save changes
	* @param   POST array
	* @access  default
	*/
    function save_event_changes() {
		if ($_POST) {
		    $tbl_event_id        = $_POST['tbl_event_id'];		
			$title               = $_POST['title'];
			$title_ar            = $_POST['title_ar'];
			$description         = $_POST['description'];
			$description_ar      = $_POST['description_ar'];
			$start_date     	  = date("Y-m-d", strtotime($_POST['start_date']));
			$end_date     	    = date("Y-m-d", strtotime($_POST['end_date']));		
			$start_time          = $_POST['start_time'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_events");	
		$resData = $this->model_events->save_event_changes($tbl_event_id, $title, $title_ar, $description, $description_ar, $start_date, $end_date, $start_time, $tbl_school_id);
		
		if ($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}


	/**
	* @desc    Edit event
	* @param   none
	* @access  default
	*/
    function edit_event() {
	    $data['page'] = "view_all_events";
		$data['menu'] = "events";
        $data['sub_menu'] = "events";
		$data['mid'] = "3";
		$this->load->model("model_events");
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_event_id = 0;
		if (array_key_exists('event_id_enc',$param_array)) {
			$tbl_event_id = $param_array['event_id_enc'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess']; 
		
		//Event details
		$event_obj = $this->model_events->get_event_obj($tbl_event_id,$tbl_school_id);
		$data['event_obj'] = $event_obj;
		$this->load->view('admin/view_template', $data);
	}


	/**
	* @desc    Show calendar
	* @param   none
	* @access  default
	*/
    function event_calendar() {
	    $data['page'] = "view_teacher_event_calendar";
		//$this->load->view('admin/view_teacher_template', $data);

		//$data['page'] = "teacher_welcome";
		$this->load->view("admin/view_teacher_template", $data);
	}


	
}
?>