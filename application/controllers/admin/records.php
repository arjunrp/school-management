<?php

/**
 * @desc   	  	Records Controller
 * @category   	Controller
 * @author     	Shanavas PK
 * @version    	0.1
 */
class Records extends CI_Controller {
	/**
	* @desc    Default function for the Controller
	* @param   none
	* @access  default
	*/
    function index() {
		
	}

	//LIST RECORDS FROM SCHOOL......................
    /**
	* @desc    Show all records from school
	* @param   none
	* @access  default
	*/
	
    function school_records() {
		$data['page'] = "view_school_records";
		$data['menu'] = "records";
        $data['sub_menu'] = "school_records";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "id";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
		$from = "";	
			
		if (array_key_exists('from',$param_array)) {
			$from = $param_array['from'];
			if (trim($from) == "") {
				$from = '';	
			}
		}	 		
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "id";
					 break;
				}
				default: {
					$sort_name = "id";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_parenting_school_cat_id',$param_array)) {
			$tbl_parenting_school_cat_id = $param_array['tbl_parenting_school_cat_id'];
			$data['tbl_sel_parenting_school_cat_id'] = $tbl_parenting_school_cat_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_parenting_school");
		$this->load->model("model_class_sessions");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess']; 
		$classesObj = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y'); 
		
		$categoryObj = $this->model_parenting_school->get_parenting_school_categories($tbl_school_id);
	    $data['category_list'] = $categoryObj;	
		
		if($tbl_school_id<>"")
		{  
			$rs_all_records     = $this->model_parenting_school->get_parenting_datas($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parenting_school_cat_id);
			$total_records      = $this->model_parenting_school->get_total_parenting_datas($q, $is_active, $tbl_school_id, $tbl_parenting_school_cat_id);
			for($k=0;$k<count($rs_all_records);$k++)
			{
				$tbl_parenting_school_id = $rs_all_records[$k]['tbl_parenting_school_id'];
				$total_assigned_users    = $this->model_parenting_school->getcntAssignRecords($tbl_parenting_school_id,$tbl_school_id);
				$rs_all_records[$k]['total_assigned_users'] = $total_assigned_users;
			}
			
		
		}else{
			$rs_all_records     = array();
			$total_records      = array();
		}
	
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/records/school_records";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_teacher_id) && trim($tbl_teacher_id)!="") {
			$page_url .= "/tbl_teacher_id/".rawurlencode($tbl_teacher_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_records;
		$config['per_page'] = TBL_PARENTING_SCHOOL_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_PARENTING_SCHOOL_PAGING >= $total_records) {
			$range = $total_records;
		} else {
			$range = $offset+TBL_PARENTING_SCHOOL_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_records records</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['classes_list']	      = $classesObj;
		$data['rs_teacher_groups']	  = $rs_teacher_groups;
		$data['rs_all_teachers']	  = $rs_all_teachers;
		$data['rs_all_records']	      = $rs_all_records;
		$data['total_records'] 	      = $total_records;
		$data['from'] 	              = $from;

		$this->load->view('admin/view_template',$data);
	}
	
	
	function add_school_record()
	{
		$added_by                          = $_SESSION['aqdar_smartcare']['tbl_admin_id_sess'];
		$tbl_parenting_school_id           = $_POST['tbl_parenting_school_id'];
		$tbl_parenting_school_cat_id       = $_POST['tbl_parenting_school_cat_id'];
		$category_en                       = $_POST['category_en'];
		$category_ar                       = $_POST['category_ar'];
		$parenting_title_en                = $_POST['parenting_title_en'];
		$parenting_title_ar                = $_POST['parenting_title_ar'];
		$parenting_type                    = $_POST['parenting_type'];
		$parenting_text_en                 = $_POST['parenting_text_en'];
		$parenting_text_ar                 = $_POST['parenting_text_ar'];
		$parenting_url                     = $_POST['parenting_url'];
		$is_active                         = $_POST['is_active'];
		
		$tbl_student_id                    = $_POST['tbl_student_id'];
		
		$this->load->model("model_parenting_school");
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
		if($tbl_school_id<>"")
		{  
		   if($category_en <>"")
		   {
		   		$tbl_parenting_school_cat_id    = substr(md5(uniqid(rand())),0,15);
		   		$this->model_parenting_school->add_records_category($tbl_parenting_school_cat_id,$category_en,$category_ar,$tbl_school_id); 
		   }
		   
		   $this->model_parenting_school->add_record($tbl_parenting_school_id,$tbl_parenting_school_cat_id,$parenting_title_en,$parenting_title_ar,$parenting_type,$parenting_text_en,$parenting_text_ar,
		   $parenting_url,$is_active,$tbl_school_id);
		   
		       // assign records to students
				if($tbl_student_id<>"")
				{
					$str  = $_POST['tbl_student_id']; //e.g." tbl_class_id=f31981d6285a6a958f754&tbl_class_id=d99f9fee0&tbl_class_id=a343d70666c3
					$str  = explode("&", $str); 
					$str = str_replace('student_id_enc=', '', $str);
					for($m=0; $m<count($str); $m++) {
						  if($str[$m]<>""){
							$data =  explode("*",$str[$m]); 
							$tbl_student_id_t = $data[0];
							$tbl_class_id_t  = $data[1];
							$tbl_parenting_school_assign_id = substr(md5(uniqid(rand())),0,10);
							$this->model_parenting_school->assign_records_to_student($tbl_parenting_school_assign_id,$tbl_parenting_school_id,$tbl_student_id_t,$tbl_class_id_t,$tbl_school_id);
						  }
					}
				}
				// end assign records to students
		   echo "Y"; 
		}else{
		  echo "N";	
		}
	}
	
	
    //edit functionality
	/**
	* @desc    Edit School Records
	* @param   none
	* @access  default
	*/
    function edit_school_record() {
		$data['page'] = "view_school_records";
		$data['menu'] = "records";
        $data['sub_menu'] = "school_records";
		
		$data['mid'] = "3";
		$this->load->model("model_parenting_school");

		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_teacher_id = 0;
		if (array_key_exists('tbl_parenting_school_id',$param_array)) {
			$tbl_parenting_school_id = $param_array['tbl_parenting_school_id'];
		}
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  	
		if($tbl_school_id<>"")
		{  
		   $categoryObj = $this->model_parenting_school->get_parenting_school_categories($tbl_school_id);
	       $data['category_list'] = $categoryObj;	
		  
		   $results = $this->model_parenting_school->get_school_record($tbl_parenting_school_id,$tbl_school_id);
	       $data['school_record'] = $results;  
		}
		$this->load->view('admin/view_template', $data);
	}

    
	
	function update_school_record()
	{
		$added_by                          = $_SESSION['aqdar_smartcare']['tbl_admin_id_sess'];
		$tbl_parenting_school_id           = $_POST['tbl_parenting_school_id'];
		$tbl_parenting_school_cat_id       = $_POST['tbl_parenting_school_cat_id'];
		$category_en                       = $_POST['category_en'];
		$category_ar                       = $_POST['category_ar'];
		$parenting_title_en                = $_POST['parenting_title_en'];
		$parenting_title_ar                = $_POST['parenting_title_ar'];
		$parenting_type                    = $_POST['parenting_type'];
		$parenting_text_en                 = $_POST['parenting_text_en'];
		$parenting_text_ar                 = $_POST['parenting_text_ar'];
		$parenting_url                     = $_POST['parenting_url'];
		$is_active                         = $_POST['is_active'];
		
		$this->load->model("model_parenting_school");
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
		if($tbl_school_id<>"")
		{  
		   if($category_en <>"")
		   {
		   		$tbl_parenting_school_cat_id    = substr(md5(uniqid(rand())),0,15);
		   		$this->model_parenting_school->add_records_category($tbl_parenting_school_cat_id,$category_en,$category_ar,$tbl_school_id); 
		   }
		   
		   
		   $this->model_parenting_school->update_school_record($tbl_parenting_school_id,$tbl_parenting_school_cat_id,$parenting_title_en,$parenting_title_ar,$parenting_type,$parenting_text_en,$parenting_text_ar,$parenting_url,$is_active,$tbl_school_id);
		   echo "Y"; 
		}else{
		  echo "N";	
		}
	}
	
	
	 function popup_school_record() {

		$this->load->model("model_parenting_school");
		
		$tbl_parenting_school_id	=	$_POST['tbl_parenting_school_id'];
		$tbl_school_id	            =	$_POST['tbl_school_id'];
	
	    if($tbl_school_id=="")
			$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  	
		
		if($tbl_school_id<>"")
		{  
		   $categoryObj = $this->model_parenting_school->get_parenting_school_categories($tbl_school_id);
	       $data['category_list'] = $categoryObj;	
		  
		   $results = $this->model_parenting_school->get_school_record($tbl_parenting_school_id,$tbl_school_id);
	       $data['school_record'] = $results;  
		}
		
		$this->load->view('parent/view_school_record_details.php', $data);
	}
	
	  /**
	* @desc    Activate School Record 
	* @param   String 
	* @access  default
	*/
    function activateSchoolRecord() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_parenting_school_id = trim($_POST["tbl_parenting_school_id"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_parenting_school");		
		$this->model_parenting_school->activate_school_record($tbl_parenting_school_id,$tbl_school_id);
	}

	/**
	* @desc    Deactivate School Record
	* @param   String school_type_id_enc
	* @access  default
	*/
    function deactivateSchoolRecord() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_parenting_school_id = trim($_POST["tbl_parenting_school_id"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_parenting_school");		
		$this->model_parenting_school->deactivate_school_record($tbl_parenting_school_id,$tbl_school_id);
	}

     /**
	* @desc    Delete School Record
	* @param   POST array
	* @access  default
	*/
	function deleteSchoolRecord() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['tbl_parenting_school_id']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('tbl_parenting_school_id=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_parenting_school");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_parenting_school->delete_school_record($str[$i],$tbl_school_id);
		}
	}
	

	//END ADD SCHOOL RECORDS......................
	
	
	
	//LIST SCHOOL RECORDS CATEGORY......................
    /**
	* @desc    Show all records categoryfrom school
	* @param   none
	* @access  default
	*/
	
    function parenting_categories() {
		$data['page'] = "view_school_records_category";
		$data['menu'] = "records";
        $data['sub_menu'] = "record_category";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "id";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "id";
					 break;
				}
				default: {
					$sort_name = "id";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_parenting_school_cat_id',$param_array)) {
			$tbl_parenting_school_cat_id = $param_array['tbl_parenting_school_cat_id'];
			$data['tbl_sel_parenting_school_cat_id'] = $tbl_parenting_school_cat_id;
		}
		
		
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_parenting_school");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		
	
		if($tbl_school_id<>"")
		{  
			$rs_all_categories     = $this->model_parenting_school->get_parenting_categories_list($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parenting_school_cat_id);
			$total_categories      = $this->model_parenting_school->get_total_parenting_categories_list($q, $is_active, $tbl_school_id, $tbl_parenting_school_cat_id);
		
		}else{
			$rs_all_categories     = array();
			$total_categories      = array();
		}
	
	    for($c=0; $c<count($rs_all_categories); $c++)
		{
			$tbl_parenting_school_cat_id  = $rs_all_categories[$c]['tbl_parenting_school_cat_id'];
			$total_records      = $this->model_parenting_school->get_total_parenting_datas($q, $is_active, $tbl_school_id, $tbl_parenting_school_cat_id);
			$rs_all_categories[$c]['cnt_records'] = $total_records;
		}
	
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/records/parenting_categories";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_categories;
		$config['per_page'] = TBL_PARENTING_SCHOOL_CAT_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_PARENTING_SCHOOL_CAT_PAGING >= $total_categories) {
			$range = $total_categories;
		} else {
			$range = $offset+TBL_PARENTING_SCHOOL_CAT_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_categories categories</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_categories']	     = $rs_all_categories;
		$data['total_categories'] 	      = $total_categories;

		$this->load->view('admin/view_template',$data);
	}
	
	
	function is_exist_records_category() {
		if ($_POST) {
			$tbl_parenting_school_cat_id   = $_POST['parenting_school_cat_id_enc'];		
			$title_en                      = $_POST['title_en'];
			$title_ar                      = $_POST['title_ar'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_parenting_school");		
		$results = $this->model_parenting_school->is_exist_records_category($tbl_parenting_school_cat_id, $title_en, $title_ar, $tbl_school_id);
		if (count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
	
	function add_records_category()
	{
		$added_by                      = $_SESSION['aqdar_smartcare']['tbl_admin_id_sess'];
		$tbl_parenting_school_cat_id   = $_POST['parenting_school_cat_id_enc'];		
		$title_en                      = $_POST['title_en'];
		$title_ar                      = $_POST['title_ar'];
		$is_active                     = $_POST['is_active'];
		
		$this->load->model("model_parenting_school");
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
		if($tbl_school_id<>"")
		{  
		   $this->model_parenting_school->add_records_category($tbl_parenting_school_cat_id,$title_en,$title_ar,$tbl_school_id);
		   echo "Y"; 
		}else{
		  echo "N";	
		}
	}
	
	
    //edit functionality
	/**
	* @desc    Edit School Records
	* @param   none
	* @access  default
	*/
    function edit_records_category() {
		$data['page'] = "view_school_records_category";
		$data['menu'] = "records";
        $data['sub_menu'] = "school_records";
		
		$data['mid'] = "3";
		$this->load->model("model_parenting_school");

		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_teacher_id = 0;
		if (array_key_exists('tbl_parenting_school_cat_id',$param_array)) {
			$tbl_parenting_school_cat_id = $param_array['tbl_parenting_school_cat_id'];
		}
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  	
		if($tbl_school_id<>"")
		{  
		
		  
		   $results = $this->model_parenting_school->get_school_record_category($tbl_parenting_school_cat_id,$tbl_school_id);
	       $data['record_categories'] = $results;  
		}
		$this->load->view('admin/view_template', $data);
	}

    
	
	function update_school_record_category()
	{
		$added_by                      = $_SESSION['aqdar_smartcare']['tbl_admin_id_sess'];
		$tbl_parenting_school_cat_id   = $_POST['parenting_school_cat_id_enc'];		
		$title_en                      = $_POST['title_en'];
		$title_ar                      = $_POST['title_ar'];
		$is_active                     = $_POST['is_active'];
		
		$this->load->model("model_parenting_school");
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
		if($tbl_school_id<>"")
		{  
		   $this->model_parenting_school->update_school_record_category($tbl_parenting_school_cat_id,$title_en,$title_ar,$tbl_school_id);
		   echo "Y"; 
		}else{
		  echo "N";	
		}
	}
	
	  /**
	* @desc    Activate School Record Category 
	* @param   String 
	* @access  default
	*/
    function activateRecordsCategory() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_parenting_school_cat_id = trim($_POST["parenting_school_cat_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_parenting_school");		
		$this->model_parenting_school->activate_school_record_category($tbl_parenting_school_cat_id,$tbl_school_id);
	}

	/**
	* @desc    Deactivate School Record
	* @param   String school_type_id_enc
	* @access  default
	*/
    function deactivateRecordsCategory() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_parenting_school_cat_id = trim($_POST["parenting_school_cat_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_parenting_school");		
		$this->model_parenting_school->deactivate_school_record_category($tbl_parenting_school_cat_id,$tbl_school_id);
	}

     /**
	* @desc    Delete School Record
	* @param   POST array
	* @access  default
	*/
	function deleteRecordsCategory() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['parenting_school_cat_id_enc']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('parenting_school_cat_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_parenting_school");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_parenting_school->delete_school_record_category($str[$i],$tbl_school_id);
		}
	}
	

	//END ADD SCHOOL RECORDS......................

	
	//LIST RECORDS ASSIGNED TO STUDENTS ......................
    /**
	* @desc    Show records assigned to tudents
	* @param   none
	* @access  default
	*/
	
    function list_assign_records() {
		$data['page'] = "view_assign_records";
		$data['menu'] = "records";
        $data['sub_menu'] = "assign_records";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "id";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		if (array_key_exists('add',$param_array)) {
			$add = $param_array['add'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "id";
					 break;
				}
				default: {
					$sort_name = "id";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_parenting_school_id',$param_array)) {
			$tbl_parenting_school_id = $param_array['tbl_parenting_school_id'];
			$data['tbl_sel_parenting_school_id'] = $tbl_parenting_school_id;
		}
		
		
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['add'] = $add;
		$data['offset'] = $offset;
	   
		$this->load->model("model_parenting_school");
		$this->load->model("model_class_sessions");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		
	
		if($tbl_school_id<>"")
		{  
			$classesObj = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
			$data['classes_list'] = $classesObj;
			
			$recordsObj = $this->model_parenting_school->get_school_records_list($tbl_school_id, 'Y');
			$data['recordsObj'] = $recordsObj;
			
			$rs_assigned_students     = $this->model_parenting_school->get_list_assign_records($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parenting_school_id);
			$total_assigned_students  = $this->model_parenting_school->get_total_assign_records($q, $is_active, $tbl_school_id, $tbl_parenting_school_id);
		
		}else{
			$rs_assigned_students     = array();
			$total_assigned_students  = array();
		}
	
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/records/list_assign_records";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		if (isset($tbl_parenting_school_id) && trim($tbl_parenting_school_id)!="") {
			$page_url .= "/tbl_parenting_school_id/".rawurlencode($tbl_parenting_school_id);
		}
		
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_assigned_students;
		$config['per_page'] = TBL_PARENTING_SCHOOL_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_PARENTING_SCHOOL_PAGING >= $total_assigned_students) {
			$range = $total_assigned_students;
		} else {
			$range = $offset+TBL_PARENTING_SCHOOL_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_assigned_students records</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_assigned_students']	     = $rs_assigned_students;
		$data['total_assigned_students'] 	  = $total_assigned_students;

		$this->load->view('admin/view_template',$data);
	}
	
	
	function assign_records()
	{
		$added_by                      = $_SESSION['aqdar_smartcare']['tbl_admin_id_sess'];
		$tbl_parenting_school_id       = $_POST['tbl_parenting_school_id'];		
		$tbl_student_id                = $_POST['tbl_student_id'];
		
		$this->load->model("model_parenting_school");
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
		if($tbl_school_id<>"")
		{  
		    if($tbl_student_id<>"")
			{
				$str  = $_POST['tbl_student_id']; //e.g." tbl_class_id=f31981d6285a6a958f754&tbl_class_id=d99f9fee0&tbl_class_id=a343d70666c3
				$str  = explode("&", $str); 
				$str = str_replace('student_id_enc=', '', $str);
			    for($m=0; $m<count($str); $m++) {
					  if($str[$m]<>""){
						$data =  explode("*",$str[$m]); 
						$tbl_student_id_t = $data[0];
						$tbl_class_id_t  = $data[1];
						$tbl_parenting_school_assign_id = substr(md5(uniqid(rand())),0,10);
						$this->model_parenting_school->assign_records_to_student($tbl_parenting_school_assign_id,$tbl_parenting_school_id,$tbl_student_id_t,$tbl_class_id_t,$tbl_school_id);
					  }
			    }
			  echo "Y";
			}
		}else{
		  echo "N";	
		}
	}
	
	
    //edit functionality
	/**
	* @desc    Edit School Assign Records 
	* @param   none
	* @access  default
	*/
    function edit_assign_records() {
		$data['page'] = "view_assign_records";
		$data['menu'] = "records";
        $data['sub_menu'] = "school_records";
		
		$data['mid'] = "3";
		$this->load->model("model_parenting_school");

		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_teacher_id = 0;
		if (array_key_exists('tbl_parenting_school_cat_id',$param_array)) {
			$tbl_parenting_school_cat_id = $param_array['tbl_parenting_school_cat_id'];
		}
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  	
		if($tbl_school_id<>"")
		{  
		   $results = $this->model_parenting_school->get_assigned_record_info($tbl_parenting_school_cat_id,$tbl_school_id);
	       $data['record_categories'] = $results;  
		}
		$this->load->view('admin/view_template', $data);
	}
	
	function update_assign_records()
	{
		$added_by                      = $_SESSION['aqdar_smartcare']['tbl_admin_id_sess'];
		$tbl_parenting_school_cat_id   = $_POST['parenting_school_cat_id_enc'];		
		$title_en                      = $_POST['title_en'];
		$title_ar                      = $_POST['title_ar'];
		$is_active                     = $_POST['is_active'];
		
		$this->load->model("model_parenting_school");
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
		if($tbl_school_id<>"")
		{  
		   $this->model_parenting_school->update_assign_records($tbl_parenting_school_cat_id,$title_en,$title_ar,$tbl_school_id);
		   echo "Y"; 
		}else{
		  echo "N";	
		}
	}
	
	  /**
	* @desc    Activate School Assign Records
	* @param   String 
	* @access  default
	*/
    function activateAssignRecords() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_parenting_school_assign_id = trim($_POST["tbl_parenting_school_assign_id"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_parenting_school");		
		$this->model_parenting_school->activate_assign_record($tbl_parenting_school_assign_id,$tbl_school_id);
	}

	/**
	* @desc    Deactivate Assign Records
	* @param   String school_type_id_enc
	* @access  default
	*/
    function deactivateAssignRecords() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_parenting_school_assign_id = trim($_POST["tbl_parenting_school_assign_id"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_parenting_school");		
		$this->model_parenting_school->deactivate_assign_record($tbl_parenting_school_assign_id,$tbl_school_id);
	}

    /**
	* @desc    Delete Assign Records
	* @param   POST array
	* @access  default
	*/
	function deleteAssignRecords() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['tbl_parenting_school_assign_id']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('tbl_parenting_school_assign_id=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_parenting_school");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_parenting_school->delete_assign_record($str[$i],$tbl_school_id);
		}
	}

	//END ASSIGN SCHOOL RECORDS TO STUDENTS......................
	
	/*****************************************************************/
	
	
	
	
	

	
}
?>