<?php
/**
 * @desc   	  	Student Controller
 * @category   	Controller
 * @author     	Shanavas .PK
 * @version    	0.1
 */

class Student extends CI_Controller {
	
	
	/**
	* @desc Default constructor for the Controller
	* @access default
	*/
    function index() {
	}
	/**
	* @desc    Show all students
	*
	* @param   none
	* @access  default
	*/
    function all_students() {
		$data['page'] = "view_all_students";
		$data['menu'] = "members";
        $data['sub_menu'] = "students";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$is_approved = "";
		$sort_by_click = "N";
		$sort_name_param = "I";
		
		$sort_name = "id";
		$sort_by = "DESC";
		
		/*$sort_name = "first_name, last_name";
		$sort_by = "ASC";*/
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
		
		//echo "--->".$status;
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		
		// Show only those student details where is_approved=N
		if (array_key_exists('sub_module',$param_array)) {
			$sub_module = $param_array['sub_module'];
			$data["sub_module"] = $param_array['sub_module'];
			$url = "/sub_module/students_pending";
		}
		if ($sub_module == "students_pending") {
			$is_approved = "N";
			$data["is_approved"] = $is_approved;
		}
		
		
				
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("I"): {
					$sort_name = "id";
					 break;
				}
				case("A"): {
					$sort_name = "first_name, last_name";
					 break;
				}
				default: {
					$sort_name = "first_name, last_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_class_search_id = "";
		if (array_key_exists('tbl_class_search_id',$param_array)) {
			$tbl_class_search_id = $param_array['tbl_class_search_id'];
			$data['tbl_class_search_id'] = $tbl_class_search_id;
		}
		$q = "";
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
			$data['q'] = $q;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		$total_students = 0;
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		
		if($tbl_school_id<>"")
		{  
			$countriesObj          = $this->model_student->get_country_list('Y');
		    $data['countries_list']= $countriesObj;
			
			$academicObj          = $this->model_student->get_academic_year('Y',$tbl_school_id);
		    $data['academic_list']= $academicObj;
			
			$classesObj 		   = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
			$data['classes_list'] = $classesObj;
	
			$rs_all_students      = $this->model_student->get_all_students($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_class_search_id, "", $is_approved);
			$total_students       = $this->model_student->get_total_students($q, $is_active, $tbl_school_id,$tbl_class_search_id, "", $is_approved);
			
			
		}else{
			$rs_all_students     = array();
			
		}
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/student/all_students".$url;
		if (isset($is_approved) && trim($is_approved)!="") {
			$page_url .= "/is_approved/".rawurlencode($is_approved);
		}
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
			$page_url .= "/tbl_student_id/".rawurlencode($tbl_student_id);
		}
		if (isset($tbl_class_search_id) && trim($tbl_class_search_id)!="") {
			$page_url .= "/tbl_class_search_id/".rawurlencode($tbl_class_search_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_students;
		$config['per_page'] = TBL_STUDENT_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_STUDENT_PAGING >= $total_students) {
			$range = $total_students;
		} else {
			$range = $offset+TBL_STUDENT_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_students students</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		$data['rs_all_students']	     = $rs_all_students;
		$data['total_students'] 	     = $total_students;

		$this->load->view('admin/view_template',$data);
	}
	
	
	
	function is_exist_student() {
		if ($_POST) {
			$tbl_student_id         = $_POST['student_id_enc'];		
			$first_name             = $_POST['first_name'];
			$last_name              = $_POST['last_name'];
			$tbl_class_id           = $_POST['tbl_class_id'];
			$tbl_emirates_id        = $_POST['tbl_emirates_id'];
			$tbl_school_id          = $_POST['tbl_school_id'];
		}
		if($tbl_school_id=="")
		{
			$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		}
		$this->load->model("model_student");		
		$results = $this->model_student->is_exist_student($tbl_student_id, $first_name, $last_name, $tbl_class_id, $tbl_school_id, $tbl_emirates_id);
		if(count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
    /**
	* @desc    Add Student
	* @param   POST array
	* @access  default
	*/
    function add_student() {
		if ($_POST) {
			$tbl_student_id            = $_POST['student_id_enc'];		
			$first_name                = $_POST['first_name'];
			$last_name                 = $_POST['last_name'];
			$first_name_ar             = $_POST['first_name_ar'];
			$last_name_ar         	   = $_POST['last_name_ar'];		
			$dob_month                 = $_POST['dob_month'];
			$dob_day                   = $_POST['dob_day'];
			$dob_year                  = $_POST['dob_year'];
			$gender                    = $_POST['gender'];		
			$mobile                    = $_POST['mobile'];
			$email                     = $_POST['email'];
			$tbl_emirates_id           = $_POST['tbl_emirates_id'];	
			$country                   = $_POST['country'];
			$emirates_id_father        = $_POST['emirates_id_father'];		
			$emirates_id_mother        = $_POST['emirates_id_mother'];
			$tbl_academic_year_id      = $_POST['tbl_academic_year_id'];
			$tbl_class_id              = $_POST['tbl_class_id'];
			$tbl_school_id             = $_POST['tbl_school_id'];
			
		
		}
		if($tbl_school_id==""){
			$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess']; 
		}
		
		$this->load->model("model_student");	
		$resData = $this->model_student->add_student($tbl_student_id, $first_name, $last_name, $first_name_ar, $last_name_ar, $dob_month, $dob_day, $dob_year, 
		                                             $gender, $mobile, $email, $country, $emirates_id_father, $emirates_id_mother, $tbl_academic_year_id, $tbl_class_id, $tbl_school_id,$tbl_emirates_id);
		
		if ($resData=="Y") {
			
			$tbl_parent_id                    = md5(uniqid(rand()));
			$first_name_parent                = $_POST['first_name_parent'];
			$last_name_parent              	 = $_POST['last_name_parent'];
			$first_name_parent_ar             = $_POST['first_name_parent_ar'];
			$last_name_parent_ar         	  = $_POST['last_name_parent_ar'];		
			$dob_month_parent                 = $_POST['dob_month_parent'];
			$dob_day_parent                   = $_POST['dob_day_parent'];
			$dob_year_parent                  = $_POST['dob_year_parent'];
			$gender_parent                    = $_POST['gender_parent'];		
			$mobile_parent                    = $_POST['mobile_parent'];
			$email_parent                     = $_POST['email_parent'];
			$emirates_id_parent               = $_POST['emirates_id_parent'];
			$parent_user_id                   = $_POST['parent_user_id'];
			$password                         = $_POST['password'];
		
			$this->load->model("model_parents");
			$existParent = $this->model_parents->is_exist_parent($emirates_id_parent);  //$first_name_parent,$last_name_parent,
			if(count($existParent)>0)
			{
				$tbl_parent_id           = $existParent[0]['tbl_parent_id'];
				
				$updateParent = $this->model_parents->update_parent($tbl_parent_id, $first_name_parent, $last_name_parent, $first_name_parent_ar, $last_name_parent_ar, $dob_month_parent, $dob_day_parent, $dob_year_parent, $gender_parent, $mobile_parent, $email_parent, $emirates_id_parent, $parent_user_id, $password, $tbl_school_id);
				
				$resParent = $this->model_parents->assign_parents($tbl_parent_id, $tbl_student_id, $tbl_school_id);
				
			}else{
			    $resData = $this->model_parents->add_parent($tbl_parent_id, $first_name_parent, $last_name_parent, $first_name_parent_ar, $last_name_parent_ar, $dob_month_parent, $dob_day_parent, $dob_year_parent, $gender_parent, $mobile_parent, $email_parent, $emirates_id_parent, $parent_user_id, $password, $tbl_school_id);
				
				$resParent = $this->model_parents->assign_parents($tbl_parent_id, $tbl_student_id, $tbl_school_id);
			}
			
		}
		
		if ($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
	
	
	function is_exist_parent_info()
	{
		$emirates_id_father            = $_POST['emirates_id_father'];		
		$emirates_id_mother            = $_POST['emirates_id_mother'];
		$assigned_parent_id_enc        = $_POST['assigned_parent_id_enc'];
		$this->load->model("model_parents");
		$parentInfo = $this->model_parents->is_exist_parent_info($emirates_id_father,$emirates_id_mother,$assigned_parent_id_enc);  
		echo json_encode($parentInfo);
        exit();
		//return $parentInfo;
	}

    /**
	* @desc    Activate student
	* @param   String 
	* @access  default
	*/
    function activateStudent() {
		if(isset($_POST) && count($_POST) != 0) {
			$student_id_enc = trim($_POST["student_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");		
		$this->model_student->activate_student($student_id_enc,$tbl_school_id);
	}


	/**
	* @desc    Deactivate school type
	* @param   String school_type_id_enc
	* @access  default
	*/
    function deactivateStudent() {
		if(isset($_POST) && count($_POST) != 0) {
			$student_id_enc = trim($_POST["student_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");		
		$this->model_student->deactivate_student($student_id_enc,$tbl_school_id);
	}
	
	
	/**
	* @desc    Approve student
	* @param   String school_type_id_enc
	* @access  default
	*/
    function approve_student_parent() {
		if(isset($_POST) && count($_POST) != 0) {
			$student_id_enc = trim($_POST["student_id_enc"]);
			$parent_id_enc = trim($_POST["parent_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");		
		$this->model_student->approve_student_parent($student_id_enc,$parent_id_enc,$tbl_school_id);
	}
	
	

     /**
	* @desc    Delete school type
	* @param   POST array
	* @access  default
	*/
     
	function deleteStudent() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['student_id_enc']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('student_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_student->delete_student($str[$i],$tbl_school_id);
		}
	}

    function updatePromotion() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['student_id_enc']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('student_id_enc=', '', $str);
			$str = explode("&", $str);
			$tbl_class_id_promotion       = trim($_POST["tbl_class_id_promotion"]);
			$tbl_academic_year_promotion  = trim($_POST["tbl_academic_year_promotion"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_student->update_promotion($str[$i],$tbl_class_id_promotion,$tbl_academic_year_promotion,$tbl_school_id);
		}
	}
	
	
	/**
	* @desc    Save changes
	* @param   POST array
	* @access  default
	*/
    function save_student_changes() {
		
		  
			if ($_POST) {
			$tbl_student_id         	= $_POST['student_id_enc'];		
			$first_name             	= $_POST['first_name'];
			$last_name              	 = $_POST['last_name'];
			$first_name_ar             = $_POST['first_name_ar'];
			$last_name_ar         	  = $_POST['last_name_ar'];		
			$dob_month                 = $_POST['dob_month'];
			$dob_day                   = $_POST['dob_day'];
			$dob_year                  = $_POST['dob_year'];
			$gender                    = $_POST['gender'];		
			$mobile                    = $_POST['mobile'];
			$email                     = $_POST['email'];
			$tbl_emirates_id           = $_POST['tbl_emirates_id'];	
			$country                   = $_POST['country'];
			$emirates_id_father        = $_POST['emirates_id_father'];		
			$emirates_id_mother        = $_POST['emirates_id_mother'];
			$tbl_academic_year_id      = $_POST['tbl_academic_year_id'];
			$tbl_class_id              = $_POST['tbl_class_id'];
			$is_approved              = $_POST['is_approved'];
			//echo "Here";
			//exit();
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");	
		$resData = $this->model_student->save_student_changes($tbl_student_id, $first_name, $last_name, $first_name_ar, $last_name_ar, $dob_month, $dob_day, $dob_year, 
		                                             $gender, $mobile, $email, $country, $emirates_id_father, $emirates_id_mother, $tbl_academic_year_id, $tbl_class_id, $tbl_school_id,$tbl_emirates_id, $is_approved);
		
		
		if ($resData=="Y") {
			
			$tbl_parent_id                    = md5(uniqid(rand()));
			$first_name_parent                = $_POST['first_name_parent'];
			$last_name_parent              	 = $_POST['last_name_parent'];
			$first_name_parent_ar             = $_POST['first_name_parent_ar'];
			$last_name_parent_ar         	  = $_POST['last_name_parent_ar'];		
			$dob_month_parent                 = $_POST['dob_month_parent'];
			$dob_day_parent                   = $_POST['dob_day_parent'];
			$dob_year_parent                  = $_POST['dob_year_parent'];
			$gender_parent                    = $_POST['gender_parent'];		
			$mobile_parent                    = $_POST['mobile_parent'];
			$email_parent                     = $_POST['email_parent'];
			$emirates_id_parent               = $_POST['emirates_id_parent'];
			$parent_user_id                   = $_POST['parent_user_id'];
			$password                         = $_POST['password'];
		
			$this->load->model("model_parents");
			$existParent = $this->model_parents->is_exist_parent($emirates_id_parent);  //$first_name_parent,$last_name_parent,
			if(count($existParent)>0)
			{
				$tbl_parent_id           = $existParent[0]['tbl_parent_id'];
				
				$updateParent = $this->model_parents->update_parent($tbl_parent_id, $first_name_parent, $last_name_parent, $first_name_parent_ar, $last_name_parent_ar, $dob_month_parent, $dob_day_parent, $dob_year_parent, $gender_parent, $mobile_parent, $email_parent, $emirates_id_parent, $parent_user_id, $password, $tbl_school_id, $is_approved);
				
				$resParent    = $this->model_parents->assign_parents($tbl_parent_id, $tbl_student_id, $tbl_school_id);
				
			}else{
			    $resData = $this->model_parents->add_parent($tbl_parent_id, $first_name_parent, $last_name_parent, $first_name_parent_ar, $last_name_parent_ar, $dob_month_parent, $dob_day_parent, $dob_year_parent, $gender_parent, $mobile_parent, $email_parent, $emirates_id_parent, $parent_user_id, $password, $tbl_school_id);
				
				$resParent = $this->model_parents->assign_parents($tbl_parent_id, $tbl_student_id, $tbl_school_id);
			}
			
		}
		if ($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}


	/**
	* @desc    Edit student
	* @param   none
	* @access  default
	*/
    function edit_student() {
		$data['page'] = "view_all_students";
		$data['menu'] = "members";
        $data['sub_menu'] = "students";
		$data['mid'] = "3";
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		$this->load->model("model_file_mgmt");
		
		$param_array = $this->uri->uri_to_assoc(3);
		// Show only those student details where is_approved=N
		if (array_key_exists('sub_module',$param_array)) {
			$sub_module = $param_array['sub_module'];
			$data["sub_module"] = $param_array['sub_module'];
		}
		if ($sub_module == "students_pending") {
			$is_approved = "N";
			$data["is_approved"] = $is_approved;
		}
		
		
		//GET Params
		
		$tbl_student_id = 0;
		if (array_key_exists('student_id_enc',$param_array)) {
			$tbl_student_id = $param_array['student_id_enc'];
		}
		
		
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess']; 
		
		$countriesObj          = $this->model_student->get_country_list('Y');
		$data['countries_list']= $countriesObj;
			
	    $academicObj          = $this->model_student->get_academic_year('Y',$tbl_school_id);
		$data['academic_list']= $academicObj;
		
	    $classesObj 		   = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
		$data['classes_list'] = $classesObj;	
		
		
		$data['tbl_uploads_id'] = $tbl_student_id;
		
		//Student details
		$student_obj = $this->model_student->get_student_obj($tbl_student_id);
		$data['student_obj'] = $student_obj;
		
		$assigned_parent_obj = $this->model_student->get_assigned_parent($tbl_student_id);
		$data['assigned_parent_obj'] = $assigned_parent_obj;
		
		$this->load->view('admin/view_template', $data);
	}

    
	/**
	* @desc    Student details
	* @param   none
	* @access  default
	*/
    function student_details() {
		$data['page'] = "view_student_details";
		$data['menu'] = "members";
        $data['sub_menu'] = "students";
		$data['mid'] = "4";//Details

		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_user_id = 0;
					
		if (array_key_exists('student_id_enc',$param_array)) {
			$tbl_student_id = $param_array['student_id_enc'];
			$data['tbl_student_id'] = $tbl_student_id;
		}	 
		
		//User details
		$this->load->model("model_student");	
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess']; 	
		$student_obj = $this->model_student->student_info($tbl_student_id,$tbl_school_id);
		$data['student_obj'] = $student_obj;
		
		$tbl_class_id  = $student_obj[0]['tbl_class_id'];
		$tbl_parent_id = $student_obj[0]['tbl_parent_id'];
	  
	    $points_obj = $this->model_student->student_points_details($q, $tbl_student_id,$tbl_class_id,$tbl_school_id);
		$data['points_obj'] = $points_obj;
		
		$cards_obj = $this->model_student->student_cards_details($q, $tbl_student_id,$tbl_class_id,$tbl_school_id);
		$data['cards_obj'] = $cards_obj;
		
		$rs_all_records     = $this->model_student->get_student_records($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parenting_school_cat_id,$tbl_student_id,$tbl_class_id);
        $data['records_obj'] = $rs_all_records;
		
		$rs_all_messages     = $this->model_student->list_parent_messages_of_student($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parent_id);
		$data['messages_obj'] = $rs_all_messages;
		
		$this->load->view('admin/view_template', $data);
	}
	



   /**
	* @desc    Students Behaviour Points Configuration
	* @param   none
	* @access  default
	*/
    function student_points_conf() {
		$data['page'] = "view_student_points_conf";
		$data['menu'] = "configuration";
        $data['sub_menu'] = "student_points_conf";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		//$sort_name = "point_name_en";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "point_name_en";
					 break;
				}
				default: {
					$sort_name = "point_name_en";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			$rs_behaviour_points      = $this->model_student->get_all_bevaviour_points($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id);
			$total_behaviour_points   = $this->model_student->get_total_behaviour_points($q, $is_active, $tbl_school_id);
		}else{
			$rs_behaviour_points     = array();
			$total_behaviour_points  = '';
		}
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/student/student_points_conf";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
	
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_behaviour_points;
		$config['per_page'] = TBL_CARD_CATEGORY_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_CARD_CATEGORY_PAGING >= $total_behaviour_points) {
			$range = $total_behaviour_points;
		} else {
			$range = $offset+TBL_CARD_CATEGORY_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_behaviour_points behavior points</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_behaviour_points']	 = $rs_behaviour_points;
		$data['total_behaviour_points']  = $total_behaviour_points;

		$this->load->view('admin/view_template',$data);
	}
	
	
	
	function is_exist_behavior_point() {
		if ($_POST) {
			$tbl_point_category_id  = $_POST['point_category_id_enc'];		
			$point_name_en          = $_POST['point_name_en'];
			$point_name_ar          = $_POST['point_name_ar'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");		
		$results = $this->model_student->is_exist_behavior_point($tbl_point_category_id, $point_name_en, $point_name_ar, $tbl_school_id);
		if(count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
    /**
	* @desc    Add Student
	* @param   POST array
	* @access  default
	*/
    function add_behavior_point() {
		if ($_POST) {
			
			$tbl_point_category_id    = $_POST['point_category_id_enc'];		
			$point_name_en            = $_POST['point_name_en'];
			$point_name_ar            = $_POST['point_name_ar'];
			$behaviour_point          = $_POST['behaviour_point'];
		
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");	
		$resData = $this->model_student->add_behavior_point($tbl_point_category_id,$point_name_en,$point_name_ar,$behaviour_point,$tbl_school_id);
		
		if ($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
	

    /**
	* @desc    Activate Behavior Point
	* @param   String 
	* @access  default
	*/
    function activateBehaviorPoint() {
		if(isset($_POST) && count($_POST) != 0) {
			$point_category_id_enc = trim($_POST["point_category_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");		
		$this->model_student->activateBehaviorPoint($point_category_id_enc,$tbl_school_id);
	}


	/**
	* @desc    Deactivate Behavior Point
	* @param   String school_type_id_enc
	* @access  default
	*/
    function deactivateBehaviorPoint() {
		if(isset($_POST) && count($_POST) != 0) {
			$point_category_id_enc = trim($_POST["point_category_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");		
		$this->model_student->deactivateBehaviorPoint($point_category_id_enc,$tbl_school_id);
	}

     /**
	* @desc    Delete Behavior Point
	* @param   POST array
	* @access  default
	*/
     
	function deleteBehaviorPoint() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['point_category_id_enc']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('point_category_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_student->deleteBehaviorPoint($str[$i],$tbl_school_id);
		}
	}

	
	
	/**
	* @desc    Save Behavior Point changes
	* @param   POST array
	* @access  default
	*/
    function save_behavior_point_changes() {
		if ($_POST) {
			$tbl_point_category_id    = $_POST['point_category_id_enc'];		
			$point_name_en            = $_POST['point_name_en'];
			$point_name_ar            = $_POST['point_name_ar'];
			$behaviour_point          = $_POST['behaviour_point'];
		
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");	
		$resData = $this->model_student->save_behavior_point_changes($tbl_point_category_id,$point_name_en,$point_name_ar,$behaviour_point,$tbl_school_id);
		if ($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}


	/**
	* @desc    Edit behavior point
	* @param   none
	* @access  default
	*/
    function edit_behavior_point() {
		$data['page']     = "view_student_points_conf";
		$data['menu']     = "configuration";
        $data['sub_menu'] = "student_points_conf";
		
		$data['mid'] = "3";
		$this->load->model("model_student");
	
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_student_id = 0;
		if (array_key_exists('point_category_id_enc',$param_array)) {
			$tbl_point_category_id = $param_array['point_category_id_enc'];
		}
		
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess']; 
		
		//Behaviour Point Details
		$point_obj = $this->model_student->get_bevaviour_point_info('', $tbl_point_category_id, $tbl_school_id);
		$data['point_obj'] = $point_obj;
		$this->load->view('admin/view_template', $data);
	}
	//END STUDENT BEHAVIOUR POINTS	
	
	
	//START STUDENT CARDS CONFIGURATION
	 /**
	* @desc    Students Cards Configuration
	*
	* @param   none
	* @access  default
	*/
    function student_cards_conf() {
		$data['page'] = "view_student_cards_conf";
		$data['menu'] = "configuration";
        $data['sub_menu'] = "student_cards_conf";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		//$sort_name = "point_name_en";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "category_name_en";
					 break;
				}
				default: {
					$sort_name = "category_name_en";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_classes");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			$schoolTypesObj = $this->model_classes->get_school_types($tbl_school_id, 'Y');
			$data['school_types'] = $schoolTypesObj;
			
			$rs_student_cards      = $this->model_student->get_all_student_cards($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id);
			$total_student_cards   = $this->model_student->get_total_student_cards($q, $is_active, $tbl_school_id);
		}else{
			$rs_student_cards     = array();
			$total_student_cards  = '';
		}
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/student/student_cards_conf";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
	
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_student_cards;
		$config['per_page'] = TBL_CARD_CATEGORY_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_CARD_CATEGORY_PAGING >= $total_student_cards) {
			$range = $total_student_cards;
		} else {
			$range = $offset+TBL_CARD_CATEGORY_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_student_cards student cards</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_student_cards']	 = $rs_student_cards;
		$data['total_student_cards']  = $total_student_cards;

		$this->load->view('admin/view_template',$data);
	}
	
	
	
	function is_exist_student_card() {
		if ($_POST) {
			$tbl_card_category_id      = $_POST['card_category_id_enc'];		
			$category_name_en          = $_POST['category_name_en'];
			$category_name_ar          = $_POST['category_name_ar'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");		
		$results = $this->model_student->is_exist_student_card($tbl_card_category_id, $category_name_en, $category_name_ar, $tbl_school_id);
		if(count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
    /**
	* @desc    Add student card
	* @param   POST array
	* @access  default
	*/
    function add_student_card() {
		if ($_POST) {
			
			$tbl_card_category_id        = $_POST['card_category_id_enc'];		
			$category_name_en            = $_POST['category_name_en'];
			$category_name_ar            = $_POST['category_name_ar'];
			$card_point                  = $_POST['card_point'];
			$tbl_category_id             = $_POST['tbl_category_id'];
		
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");	
		$resData = $this->model_student->add_student_card($tbl_card_category_id,$category_name_en,$category_name_ar,$card_point,$tbl_category_id,$tbl_school_id);
		
		if ($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
	

    /**
	* @desc    Activate student card
	* @param   String 
	* @access  default
	*/
    function activateStudentCard() {
		if(isset($_POST) && count($_POST) != 0) {
			$card_category_id_enc = trim($_POST["card_category_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");		
		$this->model_student->activateStudentCard($card_category_id_enc,$tbl_school_id);
	}


	/**
	* @desc    Deactivate student card
	* @param   String school_type_id_enc
	* @access  default
	*/
    function deactivateStudentCard() {
		if(isset($_POST) && count($_POST) != 0) {
			$card_category_id_enc = trim($_POST["card_category_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");		
		$this->model_student->deactivateStudentCard($card_category_id_enc,$tbl_school_id);
	}

     /**
	* @desc    Delete student card
	* @param   POST array
	* @access  default
	*/
     
	function deleteStudentCard() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['card_category_id_enc']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('card_category_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_student->deleteStudentCard($str[$i],$tbl_school_id);
		}
	}

	
	
	/**
	* @desc    Save student card changes
	* @param   POST array
	* @access  default
	*/
    function save_student_card_changes() {
		if ($_POST) {
			$tbl_card_category_id        = $_POST['card_category_id_enc'];		
			$category_name_en            = $_POST['category_name_en'];
			$category_name_ar            = $_POST['category_name_ar'];
			$tbl_category_id             = $_POST['tbl_category_id'];
		
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");	
		$resData = $this->model_student->save_student_card_changes($tbl_card_category_id,$category_name_en,$category_name_ar,$tbl_category_id,$tbl_school_id);
		if ($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}


	/**
	* @desc    Edit student card
	* @param   none
	* @access  default
	*/
    function edit_student_card() {
		$data['page'] = "view_student_cards_conf";
		$data['menu'] = "configuration";
        $data['sub_menu'] = "student_cards_conf";
		
		$data['mid'] = "3";
		$this->load->model("model_student");
	    $this->load->model("model_classes");
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_student_id = 0;
		if (array_key_exists('card_category_id_enc',$param_array)) {
			$tbl_card_category_id = $param_array['card_category_id_enc'];
		}
		
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess']; 
		
		$schoolTypesObj = $this->model_classes->get_school_types($tbl_school_id, 'Y');
		$data['school_types'] = $schoolTypesObj;
		//Behaviour Point Details
		$card_obj = $this->model_student->get_student_card_info('', $tbl_card_category_id, $tbl_school_id);
		$data['card_obj'] = $card_obj;
		$this->load->view('admin/view_template', $data);
	}
	//END STUDENT CARDS CONFIGURATION	
	
	
	//START STUDENT CARDS POINTS CONFIGURATION
	 /**
	* @desc    Students Cards Configuration
	*
	* @param   none
	* @access  default
	*/
    function student_cards_points_conf() {
		$data['page']     = "view_student_cards_points_conf";
		$data['menu']     = "configuration";
        $data['sub_menu'] = "student_cards_conf";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		//$sort_name = "point_name_en";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "range_from";
					 break;
				}
				default: {
					$sort_name = "range_from";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		
		$is_active = "";
		$totalPoint = 0;
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$totalPoint    = $this->model_student->getPointSettings($tbl_school_id);
		if($tbl_school_id<>"")
		{  
			$rs_student_cards_points      = $this->model_student->get_all_student_card_points($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id);
			$total_student_cards_points   = $this->model_student->get_total_student_card_points($q, $is_active, $tbl_school_id);
		}else{
			$rs_student_cards_points     = array();
			$total_student_cards_points  = '';
		}
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/student/student_cards_points_conf";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
	
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_student_cards_points;
		$config['per_page'] = TBL_CARD_CATEGORY_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_CARD_CATEGORY_PAGING >= $total_student_cards_points) {
			$range = $total_student_cards_points;
		} else {
			$range = $offset+TBL_CARD_CATEGORY_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_student_cards_points cards points</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;

		$data['totalPoint']	              = $totalPoint;
		$data['rs_student_cards_points']	 = $rs_student_cards_points;
		$data['total_student_cards_points']  = $total_student_cards_points;

		$this->load->view('admin/view_template',$data);
	}
	
	
	function is_exist_student_card_point() {
		if ($_POST) {
			$tbl_card_point_id         = $_POST['card_point_id_enc'];		
			$range_from                = $_POST['range_from'];
			$range_to                  = $_POST['range_to'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");		
		$results = $this->model_student->is_exist_student_card_point($tbl_card_point_id, $range_from, $range_to, $tbl_school_id);
		if(count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
    /**
	* @desc    Add student card
	* @param   POST array
	* @access  default
	*/
    function add_student_card_point() {
		if ($_POST) {
			
			$tbl_card_point_id         = $_POST['card_point_id_enc'];
			$total_points              = $_POST['total_points'];		
			$range_from                = $_POST['range_from'];
			$range_to                  = $_POST['range_to'];
			$card_point                = $_POST['card_point'];
		
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");	
	    $resData = $this->model_student->add_student_card_point($tbl_card_point_id,$total_points,$range_from,$range_to,$card_point,$tbl_school_id);
		
		if ($resData=="Y") {
			echo "Y";
		}else if ($resData=="X") {
			echo "X";
		}else {
			echo "N";
		}
	}

     /**
	* @desc    Delete student card point
	* @param   POST array
	* @access  default
	*/
	function deleteStudentCardPoint() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['card_point_id_enc']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('card_point_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_student->deleteStudentCardPoint($str[$i],$tbl_school_id);
		}
	}

	//END STUDENT CARDS POINTS CONFIGURATION	
	
	//LIST OF STUDENTS AGAINST CLASS
	function students_against_classes() {
	    //GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		//$sort_name = "point_name_en";
		$sort_by = "ASC";
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			if (trim($tbl_class_id) == "") {
				$tbl_class_id = '';	
			}
		}	
		
		if (array_key_exists('selBox',$param_array)) {
			$selBox = $param_array['selBox'];
			if (trim($selBox) == "") {
				$selBox = '';	
			}
		}	
		
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
	
		$data["tbl_school_id"]  = $tbl_school_id;
		$data["tbl_class_id"]   = $tbl_class_id;
		$data["selBox"]         = $selBox;
		$classes = explode(",",$tbl_class_id);
		$this->load->model('model_student');
		$this->load->model('model_classes');
		$student_record = array();
		if(count($classes)>0)
		{  $m=0;
		   for($x=0;$x<count($classes);$x++)
		   {
			   $tbl_class_id = $classes[$x];
			   if($tbl_class_id<>""){
				   $student_record= $this->model_student->get_all_students_against_class($tbl_class_id);
				   if($selBox=="1")
				   {
					   $option_str .= '<select name="tbl_student_id" id="tbl_student_id" class="form-control">
                                <option value="">--Select Student--</option>';
				   }
				   
				   for($a=0;$a<count($student_record);$a++)
				   {
							$tbl_student_id                                             = $student_record[$a]['tbl_student_id'];
							$all_students_against_class_rs[$m]['tbl_class_id']  = $tbl_class_id;
							$all_students_against_class_rs[$m]['first_name']    = $student_record[$a]['first_name'];
							$all_students_against_class_rs[$m]['last_name']     = $student_record[$a]['last_name'];
							$all_students_against_class_rs[$m]['first_name_ar'] = $student_record[$a]['first_name_ar'];
							$all_students_against_class_rs[$m]['last_name_ar']  = $student_record[$a]['last_name_ar'];
							$all_students_against_class_rs[$m]['file_name_updated']  = $student_record[$a]['file_name_updated'];
							
							$all_students_against_class_rs[$m]['tbl_student_id']= $tbl_student_id;
							$class_details       = $this->model_classes->getClassInfo($tbl_class_id);
							$tbl_section_id 	  = $class_details[0]['tbl_section_id'];
							$data_se             = $this->model_classes->getClassSectionInfo($tbl_section_id);		
							$section_name 		=  $data_se[0]["section_name"];
							$section_name_ar 	 =  $data_se[0]["section_name_ar"];
							$all_students_against_class_rs[$m]['class_name']    = $class_details[0]['class_name']." ".$section_name;
							$all_students_against_class_rs[$m]['class_name_ar'] = $class_details[0]['class_name_ar']." ".$section_name_ar;
							$name = $all_students_against_class_rs[$m]['first_name']." ".$all_students_against_class_rs[$m]['last_name'];
							$name_ar = $all_students_against_class_rs[$m]['first_name_ar']." ".$all_students_against_class_rs[$m]['last_name_ar'];
							
							if($selBox=="1")
						   {
							  $option_str .= '<option value="'.$tbl_student_id.'">'.$name.' [::] '.$name_ar.'</option>';
						   }
							
							$m=$m+1;
							
							
					}
					
					if($selBox=="1")
				    {
							  $option_str .= '</select>';
					}
			   }
			}
		}
		
		
		
		if($selBox=="1")
		{
			echo $option_str;
		}else{
			$data['all_students_against_class_rs'] = $all_students_against_class_rs;
			$this->load->view('admin/view_students_against_classes',$data);
		}
	}
	//END LIST OF STUDENTS AGAINST CLASS
	
	//START SEMESTERS AGAINST ACADEMIC YEAR
	function semesters_against_academicyear() {
	//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		//$sort_name = "point_name_en";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('tbl_academic_year',$param_array)) {
			$tbl_academic_year = $param_array['tbl_academic_year'];
			if (trim($tbl_academic_year) == "") {
				$tbl_academic_year = '';	
			}
		}	
	
		
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$data["tbl_school_id"]       = $tbl_school_id;
		$data["tbl_academic_year"]   = $tbl_academic_year;
	
		$this->load->model('model_classes');
		$semesters = array();
	
			   if($tbl_academic_year<>""){
				    $semesters = $this->model_classes->get_semesters_against_academicYear($tbl_academic_year,$tbl_school_id);
				    $option_str .= '<select name="tbl_semester_id" id="tbl_semester_id" class="form-control">
                              <option value="">--Select Semester--</option>' ;
				   for($a=0;$a<count($semesters);$a++)
				   {
						$tbl_semester_id   = $semesters[$a]['tbl_semester_id'];
						$title             = $semesters[$a]['title'];
						$title_ar          = $semesters[$a]['title_ar'];
						$duration          = $semesters[$a]['start_date']." - ".$semesters[$a]['end_date'];
						  
						if($tbl_sel_semester_id == $tbl_semester_id_u)
                             $selSemester = "selected";
                        else
                             $selSemester = "";
						
						$option_str .= '<option value="'.$tbl_semester_id.'">'.$title.' [::] '.$title_ar.' &nbsp; / &nbsp;'.$duration.'</option>';
					}
					$option_str .= '</select>';
			   }
		
			echo $option_str;
	}
	
	 //END SEMESTERS AGAINST ACADEMIC YEAR
	
	 //GENERATE STUDENT IDS
	  function generate_student_id() {
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		$sort_name = "first_name, last_name";
		$sort_by = "ASC";
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name, last_name";
					 break;
				}
				default: {
					$sort_name = "first_name, last_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		
		$is_active = "";
		
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			$rs_all_students      = $this->model_student->get_all_students($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_class_id);
			$total_students       = $this->model_student->get_total_students($q, $is_active, $tbl_school_id,$tbl_class_id);
		}else{
			$rs_all_students     = array();
			$total_students      = array();
		}
		$data['rs_all_students']	     = $rs_all_students;
		$data['total_students'] 	     = $total_students;
		$this->load->view('admin/pdf_student_id_cards',$data);
		
	}
	// END GENERATE STUDENT IDS
	
	// Generate Student Id Card
	 function generate_student_id_card() {
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		$sort_name = "first_name, last_name";
		$sort_by = "ASC";
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name, last_name";
					 break;
				}
				default: {
					$sort_name = "first_name, last_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			$rs_all_students      = $this->model_student->get_all_students($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_class_id);
			$total_students       = $this->model_student->get_total_students($q, $is_active, $tbl_school_id,$tbl_class_id);
		}else{
			$rs_all_students     = array();
			$total_students      = array();
		}
		$data['rs_all_students']	     = $rs_all_students;
		$data['total_students'] 	     = $total_students;
		$this->load->view('admin/print_student_id_card',$data);
		
	}
	
	//END LIST OF STUDENTS AGAINST CLASS
	
	
	
	/**
	* @desc    Add Student from mobile app
	* @param   POST array
	* @access  default
	*/
    function add_student_from_app() {
		if ($_POST) {
			$tbl_school_id = $_POST['school_id'];	
			$tbl_student_id = $_POST['student_id_enc'];		
			$first_name = $_POST['first_name'];
			$last_name = $_POST['last_name'];
			$first_name_ar = $_POST['first_name_ar'];
			$last_name_ar = $_POST['last_name_ar'];		
			$dob_month = $_POST['dob_month'];
			$dob_day = $_POST['dob_day'];
			$dob_year = $_POST['dob_year'];
			$gender = $_POST['gender'];		
			$mobile = $_POST['mobile'];
			$email = $_POST['email'];
			$tbl_emirates_id = $_POST['tbl_emirates_id'];	
			$country = $_POST['country'];
			$emirates_id_father = $_POST['emirates_id_father'];		
			$emirates_id_mother = $_POST['emirates_id_mother'];
			$tbl_academic_year_id = $_POST['tbl_academic_year_id'];
			$tbl_class_id = $_POST['tbl_class_id'];
		}
		
		$emirates_id_parent = $_POST['emirates_id_parent'];

		$this->load->model("model_student");
		$this->load->model("model_parents");
		
		
		//echo json_encode($arr);
		//exit();
		
		if ($tbl_student_id == "") {
			//echo '{"code":"200"}';	// Invalid
			$arr["code"] = "200";
			echo json_encode($arr);
			exit();	
		}
		
		if ($tbl_school_id == "") {
			$arr["code"] = "MSI";
			echo json_encode($arr);
			exit();	
		}
		
		if ($tbl_emirates_id == "") {
			$arr["code"] = "MEIDS";
			echo json_encode($arr);
			exit();	
		}
		
		if ($emirates_id_parent == "") {
			$arr["code"] = "MEIDP";
			echo json_encode($arr);
			exit();	
		}
		
		//Check if parent user id already exists
		$parent_user_id = $_POST['parent_user_id'];
		$existParentUserId = $this->model_parents->is_exist_user_id($parent_user_id);  //Check if parent emirated id exists in tbl_parents table
		if($existParentUserId[0]["id"] != "") {
			$arr["code"] = "PUIDAE";
			echo json_encode($arr);
			exit();	
		}
		
		//Check if student emirated id exists in tbl_student table
		$emirates_id_parent = $_POST['emirates_id_parent'];
		$existParent = $this->model_parents->is_exist_parent($emirates_id_parent);  //Check if parent emirated id exists in tbl_parents table
		if($existParent[0]["tbl_parent_id"] != "") {
			$arr["code"] = "PEAE";
			echo json_encode($arr);
			exit();	
		}
		
		$email_parent = $_POST['email_parent'];
		$is_exists_email = $this->model_parents->is_exists_email($email_parent);
		if ($is_exists_email == "Y"){
			$arr["code"] = "PEMAE";
			echo json_encode($arr);
			exit();	
		}
		
		//Check if student emirated id already exists in tbl_student table
		$tbl_emirates_id = $_POST['tbl_emirates_id'];
		$existStudent = $this->model_student->is_exist_student_eid($tbl_emirates_id, "");
		if($existStudent[0]["tbl_emirates_id"] != "") {
			$arr["code"] = "SEAE";
			echo json_encode($arr);
			exit();	
		}
		
		if ($emirates_id_father == "") {
			$arr["code"] = "MEIDF";			//Missing emirates ID father
			echo json_encode($arr);
			exit();	
		}
		
		//Check if tbl_student_id already exists in tbl_student table
		$existStudent = $this->model_student->is_exist_student_id($tbl_student_id);
		if($existStudent[0]["id"] != "") {
			$arr["code"] = "SIAE";				// Student ID alrady exists
			echo json_encode($arr);
			exit();	
		}
		
		$resData = $this->model_student->add_student($tbl_student_id, $first_name, $last_name, $first_name_ar, $last_name_ar, $dob_month, $dob_day, $dob_year, 
		                                             $gender, $mobile, $email, $country, $emirates_id_father, $emirates_id_mother, $tbl_academic_year_id, $tbl_class_id, $tbl_school_id,$tbl_emirates_id,"N");
		
		
		if ($resData=="Y") {
			
			$tbl_parent_id = md5(uniqid(rand()));
			$first_name_parent = $_POST['first_name_parent'];
			$last_name_parent = $_POST['last_name_parent'];
			$first_name_parent_ar = $_POST['first_name_parent_ar'];
			$last_name_parent_ar = $_POST['last_name_parent_ar'];		
			$dob_month_parent = $_POST['dob_month_parent'];
			$dob_day_parent = $_POST['dob_day_parent'];
			$dob_year_parent = $_POST['dob_year_parent'];
			$gender_parent = $_POST['gender_parent'];		
			$mobile_parent = $_POST['mobile_parent'];
			$email_parent = $_POST['email_parent'];
			$emirates_id_parent = $_POST['emirates_id_parent'];
			$parent_user_id = $_POST['parent_user_id'];
			$password = $_POST['password'];
		
			    $resData = $this->model_parents->add_parent($tbl_parent_id, $first_name_parent, $last_name_parent, $first_name_parent_ar, $last_name_parent_ar, $dob_month_parent, $dob_day_parent, $dob_year_parent, $gender_parent, $mobile_parent, $email_parent, $emirates_id_parent, $parent_user_id, $password, $tbl_school_id, "N");
				
				$resParent = $this->model_parents->assign_parents($tbl_parent_id, $tbl_student_id, $tbl_school_id);
			
		}
		
		if ($resData=="Y") {
			$arr["code"] = "Y";
			echo json_encode($arr);
		} else {
			$arr["code"] = "N";
			echo json_encode($arr);
		}
	}
	
	
	/**
	* @desc    Add Student from mobile app
	* @param   POST array
	* @access  default
	*/
    function add_only_student_from_app() {
		if ($_GET) {
			/*$user_id = $_GET["user_id"];
			$role = $_GET["role"];
			$lan = $_GET["lan"];
			$device = $_GET["device"];
			$device_uid = $_GET["device_uid"];*/
		}
		
		if ($_POST) {
			$user_id = $_POST["user_id"];
			$role = $_POST["role"];
			$lan = $_POST["lan"];
			$device = $_POST["device"];
			$device_uid = $_POST["device_uid"];
			
			$tbl_school_id = $_POST['school_id'];	
			$tbl_student_id = $_POST['student_id_enc'];		
			$first_name = $_POST['first_name'];
			$last_name = $_POST['last_name'];
			$first_name_ar = $_POST['first_name_ar'];
			$last_name_ar = $_POST['last_name_ar'];		
			$dob_month = $_POST['dob_month'];
			$dob_day = $_POST['dob_day'];
			$dob_year = $_POST['dob_year'];
			$gender = $_POST['gender'];		
			$mobile = $_POST['mobile'];
			$email = $_POST['email'];
			$tbl_emirates_id = $_POST['tbl_emirates_id'];	
			$country = $_POST['country'];
			$emirates_id_father = $_POST['emirates_id_father'];		
			$emirates_id_mother = $_POST['emirates_id_mother'];
			$tbl_academic_year_id = $_POST['tbl_academic_year_id'];
			$tbl_class_id = $_POST['tbl_class_id'];
		}
		
		$tbl_parent_id = $user_id;
		
		$this->load->model("model_student");
		$this->load->model("model_parents");
		//exit();
		
		if ($tbl_student_id == "") {
			//echo '{"code":"200"}';	// Invalid
			$arr["code"] = "200";
			echo json_encode($arr);
			exit();	
		}
		
		if ($tbl_school_id == "") {
			$arr["code"] = "MSI";
			echo json_encode($arr);
			exit();	
		}
		
		if ($tbl_emirates_id == "") {
			$arr["code"] = "MEIDS";
			echo json_encode($arr);
			exit();	
		}
		
		if ($emirates_id_father == "") {
			$arr["code"] = "MEIDF";			//Missing emirates ID father
			echo json_encode($arr);
			exit();	
		}
		
		//Check if student emirated id already exists in tbl_student table
		$tbl_emirates_id = $_POST['tbl_emirates_id'];
		$existStudent = $this->model_student->is_exist_student_eid($tbl_emirates_id, "");
		if($existStudent[0]["tbl_emirates_id"] != "") {
			$arr["code"] = "SEAE";
			echo json_encode($arr);
			exit();	
		}
		
		//Check if tbl_student_id already exists in tbl_student table
		$existStudent = $this->model_student->is_exist_student_id($tbl_student_id);
		if($existStudent[0]["id"] != "") {
			$arr["code"] = "SIAE";				// Student ID alrady exists
			echo json_encode($arr);
			exit();	
		}
		
		$resData = $this->model_student->add_student($tbl_student_id, $first_name, $last_name, $first_name_ar, $last_name_ar, $dob_month, $dob_day, $dob_year, 
		                                             $gender, $mobile, $email, $country, $emirates_id_father, $emirates_id_mother, $tbl_academic_year_id, $tbl_class_id, $tbl_school_id,$tbl_emirates_id);
		
		$resParent = $this->model_parents->assign_parents($tbl_parent_id, $tbl_student_id, $tbl_school_id);
		if ($resData=="Y") {
			$arr["code"] = "Y";
			echo json_encode($arr);
		} else {
			$arr["code"] = "N";
			echo json_encode($arr);
		}
	}
	
	
	
	//START STUDENT PERFORMANCE BASED ON ACTIVITY CONFIGURATION
	 /**
	* @desc    Students Cards Configuration
	*
	* @param   none
	* @access  default
	*/
    function student_performance_conf() {
		$data['page'] = "view_student_performance_conf";
		$data['menu'] = "configuration";
        $data['sub_menu'] = "student_performance_conf";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "topic_en";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "topic_en";
					 break;
				}
				case("B"): {
					$sort_name = "performance_category_en";
					 break;
				}
				default: {
					$sort_name = "topic_en";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_classes");
		$this->load->model("model_class_sessions");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			$schoolTopicCategoryObj = $this->model_student->get_student_topic_category('Y');
			$data['topic_categories']   = $schoolTopicCategoryObj;
			
			// add purpose
			$classesObj = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
			$data['classes_list'] = $classesObj;
		
			$rs_student_topics      = $this->model_student->get_all_student_topics($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id);
			$total_student_topics   = $this->model_student->get_total_student_topics($q, $is_active, $tbl_school_id);
		}else{
			$rs_student_topics     = array();
			$total_student_topics  = '';
		}
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/student/student_performance_conf";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
	
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_student_topics;
		$config['per_page'] = TBL_CARD_CATEGORY_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_CARD_CATEGORY_PAGING >= $total_student_topics) {
			$range = $total_student_topics;
		} else {
			$range = $offset+TBL_CARD_CATEGORY_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_student_topics student topics</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_student_topics']	 	= $rs_student_topics;
		$data['total_student_topics']  	= $total_student_topics;

		$this->load->view('admin/view_template',$data);
	}
	
	
	
	function is_exist_student_topic() {
		if ($_POST) {
			$tbl_performance_id        = $_POST['performance_id_enc'];		
			$topic_name_en             = $_POST['topic_name_en'];
			$topic_name_ar             = $_POST['topic_name_ar'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");		
		$results = $this->model_student->is_exist_student_topic($tbl_performance_id, $topic_name_en, $topic_name_ar, $tbl_school_id);
		if(count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
    /**
	* @desc    Add student topic
	* @param   POST array
	* @access  default
	*/
    function add_student_topic() {
		if ($_POST) {
			
			$tbl_performance_id          = $_POST['performance_id_enc'];		
			$topic_name_en               = $_POST['topic_name_en'];
			$topic_name_ar               = $_POST['topic_name_ar'];
			$tbl_performance_category_id = $_POST['tbl_performance_category_id'];
			$tbl_class_id                = $_POST['tbl_class_id'];
		
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");	
		$resData = $this->model_student->add_student_topic($tbl_performance_id,$topic_name_en,$topic_name_ar,$tbl_performance_category_id,$tbl_school_id,$tbl_class_id);
		
		if ($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
	

    /**
	* @desc    Activate student topic
	* @param   String 
	* @access  default
	*/
    function activateStudentTopic() {
		if(isset($_POST) && count($_POST) != 0) {
			$performance_id_enc = trim($_POST["performance_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");		
		$this->model_student->activateStudentTopic($performance_id_enc,$tbl_school_id);
	}


	/**
	* @desc    Deactivate student topic
	* @param   String performance_id_enc
	* @access  default
	*/
    function deactivateStudentTopic() {
		if(isset($_POST) && count($_POST) != 0) {
			$performance_id_enc = trim($_POST["performance_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");		
		$this->model_student->deactivateStudentTopic($performance_id_enc,$tbl_school_id);
	}

     /**
	* @desc    Delete student topic
	* @param   POST array
	* @access  default
	*/
     
	function deleteStudentTopic() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['performance_id_enc']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('performance_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_student->deleteStudentTopic($str[$i],$tbl_school_id);
		}
	}

	
	
	/**
	* @desc    Save student card changes
	* @param   POST array
	* @access  default
	*/
    function save_student_topic_changes() {
		if ($_POST) {
			$tbl_performance_id          = $_POST['performance_id_enc'];		
			$topic_name_en               = $_POST['topic_name_en'];
			$topic_name_ar               = $_POST['topic_name_ar'];
			$tbl_performance_category_id = $_POST['tbl_performance_category_id'];
			$tbl_class_id                = $_POST['tbl_class_id'];
		
		
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");	
		$resData = $this->model_student->save_student_topic_changes($tbl_performance_id,$topic_name_en,$topic_name_ar,$tbl_performance_category_id,$tbl_school_id,$tbl_class_id);
		if ($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}


	/**
	* @desc    Edit student topic
	* @param   none
	* @access  default
	*/
    function edit_student_topic() {
		$data['page'] = "view_student_performance_conf";
		$data['menu'] = "configuration";
        $data['sub_menu'] = "student_performance_conf";
		
		$data['mid'] = "3";
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_performance_id = 0;
		if (array_key_exists('performance_id_enc',$param_array)) {
			$tbl_performance_id = $param_array['performance_id_enc'];
		}
		
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess']; 
		
		$schoolTopicCategoryObj = $this->model_student->get_student_topic_category('Y');
		$data['topic_categories']   = $schoolTopicCategoryObj;
		
		// update purpose
		$classesObj = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
		$data['classes_list'] = $classesObj;
		
		$selected_class_list   = $this->model_student->get_selected_class_by_performance($tbl_performance_id,$tbl_school_id);
	
		
		for($b=0;$b<count($selected_class_list);$b++)
		   {
			   $class_array[$b]     = $selected_class_list[$b]['tbl_class_id'];
		   }
		
		
		//Behaviour Point Details
		$student_topic_obj         = $this->model_student->get_student_topic_info('', $tbl_performance_id, $tbl_school_id);
		$data['class_array']       = $class_array;
		$data['student_topic_obj'] = $student_topic_obj;
		$this->load->view('admin/view_template', $data);
	}
	//END STUDENT ACTIVITY CONFIGURATION	
	
		// Submit Student Topic/ Activity Performance
		
	function save_student_performance_rate() {
		if ($_POST) {
			$tbl_performance_student_id      	= md5(uniqid(rand()));
			$topic_val          				= $_POST['topic_val'];		
			$topic_id           				= $_POST['topic_id'];
			$tbl_student_id     				= $_POST['tbl_student_id'];
			$tbl_class_id       				= $_POST['tbl_class_id'];
			$tbl_teacher_id     				= $_POST['tbl_teacher_id'];
			$tbl_school_id      				= $_POST['tbl_school_id'];
		}
		$this->load->model("model_student");	
		$resData = $this->model_student->save_student_performance_rate($tbl_performance_student_id,$topic_id,$topic_val,$tbl_student_id,$tbl_class_id,$tbl_teacher_id,$tbl_school_id);
		if ($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}

	function save_student_progress_report() {
		if ($_POST) {
			
			$tbl_progress_report_id         = $_POST['tbl_progress_report_id'];
			$report_name          			= $_POST['report_name'];
			if($report_name<>"")
			{
				$tbl_progress_report_id     = md5(uniqid(rand()));
			}
			$tbl_progress_id      			= md5(uniqid(rand()));
			
			$report_name_ar          		= $_POST['report_name_ar'];
			$id_data          				= $_POST['id_data'];
			$sub_mark_data          		= $_POST['sub_mark_data'];
			$tot_mark_data          		= isset($_POST['tot_mark_data'])? $_POST['tot_mark_data'] : '100';
			$tbl_student_id     			= $_POST['tbl_student_id'];
			$tbl_class_id       			= $_POST['tbl_class_id'];
			$tbl_teacher_id     			= $_POST['tbl_teacher_id'];
			$tbl_school_id      			= $_POST['tbl_school_id'];
		}
		
		$this->load->model("model_student");	
		$resData = $this->model_student->save_student_progress_report($tbl_progress_id, $tbl_progress_report_id, $report_name, $report_name_ar, $id_data, $sub_mark_data,
																	  $tot_mark_data, $tbl_student_id, $tbl_class_id, $tbl_teacher_id, $tbl_school_id);
		if ($resData=="Y") {
			echo "Y";
		} else if($resData=="E") {
			echo "E";
		} else {
			echo "N";
		}
	}	
		
		
		
		
	function update_student_progress_report() {
		if ($_POST) {
			
			$tbl_progress_report_id         = $_POST['tbl_progress_report_id'];
			$report_name          			= $_POST['report_name'];
			if($report_name<>"")
			{
				$tbl_progress_report_id     = md5(uniqid(rand()));
			}
			$tbl_progress_id      			= md5(uniqid(rand()));
			
			$report_name_ar          		= $_POST['report_name_ar'];
			$id_data          				= $_POST['id_data'];
			$sub_mark_data          		= $_POST['sub_mark_data'];
			$tot_mark_data          		= isset($_POST['tot_mark_data'])? $_POST['tot_mark_data'] : '100';
			$tbl_student_id     			= $_POST['tbl_student_id'];
			$tbl_class_id       			= $_POST['tbl_class_id'];
			$tbl_teacher_id     			= $_POST['tbl_teacher_id'];
			$tbl_school_id      			= $_POST['tbl_school_id'];
		}
		
		$this->load->model("model_student");	
		$resData = $this->model_student->update_student_progress_report($tbl_progress_id, $tbl_progress_report_id, $report_name, $report_name_ar, $id_data, $sub_mark_data,
																	  $tot_mark_data, $tbl_student_id, $tbl_class_id, $tbl_teacher_id, $tbl_school_id);
		if ($resData=="Y") {
			echo "Y";
		} else if($resData=="E") {
			echo "E";
		} else {
			echo "N";
		}
	}	
	
	
	
	
	
	
	
	
}
?>







