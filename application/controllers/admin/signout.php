<?php

/**
 * @desc   	  	Signout Controller
 *
 * @category   	Controller
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Signout extends CI_Controller {


	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
    function index() {
		if (trim($_SESSION['aqdar_smartcare']['user_type_sess']) == "teacher") {
			unset($_SESSION['aqdar_smartcare']['tbl_teacher_id_sess']);
			unset($_SESSION['aqdar_smartcare']['teacher_email_sess']);
			unset($_SESSION['aqdar_smartcare']['teacher_first_name_sess']);
			unset($_SESSION['aqdar_smartcare']['teacher_last_name_sess']);
			unset($_SESSION['aqdar_smartcare']['teacher_picture_sess']);
			unset($_SESSION['aqdar_smartcare']['added_date_sess']);
			
			unset($_SESSION['aqdar_smartcare']['tbl_admin_user_id_sess']);
			unset($_SESSION['aqdar_smartcare']['tbl_admin_id_sess']);
			unset($_SESSION['aqdar_smartcare']['module_access']);
			unset($_SESSION['aqdar_smartcare']['tbl_school_id_sess']);
			unset($_SESSION['aqdar_smartcare']['admin_first_name_sess']);
			unset($_SESSION['aqdar_smartcare']['admin_last_name_sess']);
			
			unset($_SESSION['aqdar_smartcare']['admin_email_sess']);
			
			unset($_SESSION['aqdar_smartcare']);
		
			//session_destroy();
			/*header("Location: ".HOST_URL."/".LAN_SEL."/admin/teacher/login_form");*/
			header("Location: ".HOST_URL."/".LAN_SEL."/school_web");
		} else {
			unset($_SESSION['aqdar_smartcare']['tbl_admin_user_id_sess']);
			unset($_SESSION['aqdar_smartcare']['tbl_admin_id_sess']);
			unset($_SESSION['aqdar_smartcare']['module_access']);
			unset($_SESSION['aqdar_smartcare']['tbl_school_id_sess']);
			unset($_SESSION['aqdar_smartcare']['admin_first_name_sess']);
			unset($_SESSION['aqdar_smartcare']['admin_last_name_sess']);
			
			/**********************************************************/
			unset($_SESSION['aqdar_smartcare']['tbl_teacher_id_sess']);
			unset($_SESSION['aqdar_smartcare']['teacher_email_sess']);
			unset($_SESSION['aqdar_smartcare']['teacher_first_name_sess']);
			unset($_SESSION['aqdar_smartcare']['teacher_last_name_sess']);
			unset($_SESSION['aqdar_smartcare']['teacher_picture_sess']);
			unset($_SESSION['aqdar_smartcare']['added_date_sess']);
			
			/**********************************************************/
			unset($_SESSION['aqdar_smartcare']);
	
			//session_destroy();
			/*header("Location: ".HOST_URL."/".LAN_SEL."/admin/admin_user/login_form");*/
			header("Location: ".HOST_URL."/".LAN_SEL."/school_web");
		}
	}
	
}
?>