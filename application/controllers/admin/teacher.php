<?php
/**
 * @desc   	  	Teacher Controller
 * @category   	Controller
 * @author     	Shanavas PK
 * @version    	0.1
 */
class Teacher extends CI_Controller {
	/**
	* @desc    Default function for the Controller
	* @param   none
	* @access  default
	*/
    function index() {
		$data["user_type_str"] = "teacher";
		$this->load->view("admin/iframe_index", $data);
	}

	/**
	* @desc    Login page
	* @param   none
	* @access  default
	*/
    function login_form() {
		$data['page'] = "login_form";
		$data['user_type'] = "T";
		$this->load->view("admin/login_form", $data);
	}

    /**
	* @desc    Show all teachers
	* @param   none
	* @access  default
	*/
    function all_teachers() {
		$data['page'] = "view_all_teachers";
		$data['menu'] = "members";
        $data['sub_menu'] = "teachers";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "first_name";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name";
					 break;
				}
				default: {
					$sort_name = "first_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_teacher_id',$param_array)) {
			$tbl_teacher_id = $param_array['tbl_teacher_id'];
			$data['tbl_sel_teacher_id'] = $tbl_teacher_id;
		}
		
		$data['tbl_sel_class_search_id'] = "";
		$tbl_class_search_id = "";
		if (array_key_exists('tbl_class_search_id',$param_array)) {
			$tbl_class_search_id = $param_array['tbl_class_search_id'];
			$data['tbl_sel_class_search_id'] = $tbl_class_search_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		$this->load->model("model_teachers");
		$this->load->model("model_school_subject");
		$total_teachers      = 0; 
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			$countriesObj          = $this->model_student->get_country_list('Y');
		    $data['countries_list']= $countriesObj;
			
			$academicObj          = $this->model_student->get_academic_year('Y',$tbl_school_id);
		    $data['academic_list']= $academicObj;
			
			$classesObj 		   = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
			$data['classes_list']  = $classesObj;
			
			$subjectObj 		   = $this->model_school_subject->get_all_school_subjects('', '', '', '', 'Y', $tbl_school_id);
			$data['subject_list']  = $subjectObj;
			
			$rs_teacher_roles            = $this->model_teachers->get_teacher_roles('Y',$tbl_school_id);
			$data['teacher_roles_list']  = $rs_teacher_roles;
			
			$rs_all_teachers      = $this->model_teachers->list_all_teachers($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_class_search_id);
			$total_teachers       = $this->model_teachers->get_total_teachers($q, $is_active, $tbl_school_id,$tbl_class_search_id);
		}else{
			$rs_all_teachers     = array();
			
		}
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/teacher/all_teachers";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_class_id) && trim($tbl_class_id)!="") {
			$page_url .= "/tbl_class_search_id/".rawurlencode($tbl_class_search_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_teachers;
		$config['per_page'] = TBL_TEACHER_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_TEACHER_PAGING >= $total_teachers) {
			$range = $total_teachers;
		} else {
			$range = $offset+TBL_TEACHER_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_teachers teachers</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_teachers']	     = $rs_all_teachers;
		$data['total_teachers'] 	      = $total_teachers;

		$this->load->view('admin/view_template',$data);
	}
	
	 /**
	* @desc    Activate Teacher
	* @param   String 
	* @access  default
	*/
    function activateTeacher() {
		if(isset($_POST) && count($_POST) != 0) {
			$teacher_id_enc = trim($_POST["teacher_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_teachers");		
		$this->model_teachers->activate_teacher($teacher_id_enc,$tbl_school_id);
	}

	/**
	* @desc    Deactivate Teacher
	* @param   String school_type_id_enc
	* @access  default
	*/
    function deactivateTeacher() {
		if(isset($_POST) && count($_POST) != 0) {
			$teacher_id_enc = trim($_POST["teacher_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_teachers");		
		$this->model_teachers->deactivate_teacher($teacher_id_enc,$tbl_school_id);
	}

     /**
	* @desc    Delete Teacher
	* @param   POST array
	* @access  default
	*/
	function deleteTeacher() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['teacher_id_enc']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('teacher_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_teachers");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_teachers->delete_teacher($str[$i],$tbl_school_id);
		}
	}
	
	function is_exist_teacher()
	{
		if ($_POST) {
			$tbl_teacher_id          = $_POST['teacher_id_enc'];		
			$emirates_id             = $_POST['emirates_id'];
			$tbl_school_id           = $_POST['tbl_school_id'];
		}
		if($tbl_school_id<>"")
			$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		
		$this->load->model("model_teachers");		
		$results = $this->model_teachers->is_exist_teacher($tbl_teacher_id, $emirates_id, $tbl_school_id);
		if(count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
	// Teacher Details
		/**
	* @desc    Teacher details
	* @param   none
	* @access  default
	*/
    function teacher_details() {
		$data['page'] = "view_teacher_details";
		$data['menu'] = "members";
        $data['sub_menu'] = "teachers";
		$data['mid'] = "4";//Details

		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_user_id = 0;
					
		if (array_key_exists('teacher_id_enc',$param_array)) {
			$tbl_teacher_id = $param_array['teacher_id_enc'];
			$data['tbl_teacher_id'] = $tbl_teacher_id;
		}	 
		
		//User details
		$this->load->model("model_teachers");	
		$this->load->model("model_classes");
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess']; 	
		$teacher_obj = $this->model_teachers->teacher_info($tbl_teacher_id,$tbl_school_id);
		$data['teacher_obj'] = $teacher_obj;
		
		$classes_obj = $this->model_classes->list_classes_against_teacher($tbl_teacher_id,$tbl_school_id);
		$data['classes_obj'] = $classes_obj;
		
		$this->load->view('admin/view_template', $data);
	}
	


	
	
	//add teacher

	 function add_teacher() {
		if ($_POST) {
			$tbl_teacher_id            = $_POST['teacher_id_enc'];		
			$first_name                = $_POST['first_name'];
			$last_name                 = $_POST['last_name'];
			$first_name_ar             = $_POST['first_name_ar'];
			$last_name_ar         	   = $_POST['last_name_ar'];		
			$dob_month                 = $_POST['dob_month'];
			$dob_day                   = $_POST['dob_day'];
			$dob_year                  = $_POST['dob_year'];
			$gender                    = $_POST['gender'];		
			$mobile                    = $_POST['mobile'];
			$email                     = $_POST['email'];
			$country                   = $_POST['country'];
			$emirates_id               = $_POST['emirates_id'];		
			$tbl_school_roles_id       = $_POST['tbl_school_roles_id'];
			
			$strClass 				   = $_POST['tbl_class_id']; //e.g." tbl_class_id=f31981d6285a6a958f754&tbl_class_id=d99f9fee0&tbl_class_id=a343d70666c3
			$strClass 				   = explode(":", $strClass);
			
			$strSubject 			   = $_POST['tbl_subject_id']; //e.g." tbl_class_id=f31981d6285a6a958f754&tbl_class_id=d99f9fee0&tbl_class_id=a343d70666c3
			$strSubject                = explode(":", $strSubject);
			
			$tbl_class_teacher_id      = $_POST['tbl_class_teacher_id'];
			$user_id                   = $_POST['user_id'];
			$password                  = $_POST['password'];
			$tbl_school_id             = $_POST['tbl_school_id'];
			$is_active                 = isset($_POST['is_active'])? $_POST['is_active']:'N' ;

		}
		if($tbl_school_id==""){
			$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		}
		$this->load->model("model_teachers");	
		$resData = $this->model_teachers->add_teacher($tbl_teacher_id, $first_name, $last_name, $first_name_ar, $last_name_ar, $dob_month, $dob_day, $dob_year, 
		                                             $gender, $mobile, $email, $country, $emirates_id, $tbl_school_roles_id, $tbl_class_teacher_id, $user_id, $password, $tbl_school_id, $is_active);
		if ($resData=="Y") {
			$delAssignClasses = $this->model_teachers->delete_assign_classes($tbl_teacher_id, $tbl_school_id);
			for($m=0; $m<count($strClass); $m++) {
				if($strClass[$m]<>""){
					$tbl_teacher_class_id = substr(md5(uniqid(rand())),0,15);
					$resTeacher = $this->model_teachers->assign_classes($tbl_teacher_class_id, $strClass[$m], $tbl_teacher_id, $tbl_school_id);
				}
		    }
			
			$delAssignSubject = $this->model_teachers->delete_assign_subjects($tbl_teacher_id, $tbl_school_id);
			for($n=0; $n<count($strSubject); $n++) {
				if($strSubject[$n]<>""){
					$tbl_teacher_subject_id = substr(md5(uniqid(rand())),0,15);
					$resSubject = $this->model_teachers->assign_subjects($tbl_teacher_subject_id, $strSubject[$n], $tbl_teacher_id, $tbl_school_id);
				}
		    }
		}
		
	   if($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
	function is_exist_teacher_user_id() {
		if ($_POST) {
			$tbl_teacher_id              = $_POST['teacher_id_enc'];		
			$user_id                     = $_POST['user_id'];
		}
		$this->load->model("model_teachers");		
		$results = $this->model_teachers->is_exist_teacher_user_id($tbl_teacher_id, $user_id);
		if(count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
	//edit functionality
	/**
	* @desc    Edit teacher
	* @param   none
	* @access  default
	*/
    function edit_teacher() {
		$data['page'] = "view_all_teachers";
		$data['menu'] = "members";
        $data['sub_menu'] = "teachers";
		$data['mid'] = "3";
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		$this->load->model("model_teachers");
		$this->load->model("model_school_subject");
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_teacher_id = 0;
		if (array_key_exists('teacher_id_enc',$param_array)) {
			$tbl_teacher_id 		= $param_array['teacher_id_enc'];
		}
		$tbl_school_id 				= $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$data['tbl_uploads_id'] 	= $tbl_teacher_id;
		
		$countriesObj          		= $this->model_student->get_country_list('Y');
		$data['countries_list']		= $countriesObj;
			
	    $academicObj          		= $this->model_student->get_academic_year('Y',$tbl_school_id);
		$data['academic_list']		= $academicObj;
		
	    $classesObj 		   		= $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
		$data['classes_list'] 		= $classesObj;	
		 
		$rs_teacher_roles           = $this->model_teachers->get_teacher_roles('Y',$tbl_school_id);
		$data['teacher_roles_list'] = $rs_teacher_roles;
		
		
		$subjectObj 		   		= $this->model_school_subject->get_all_school_subjects('', '', '', '', 'Y', $tbl_school_id);
		$data['subject_list']  		= $subjectObj;
		
		//Teacher details
		$teacher_obj 				= $this->model_teachers->get_teacher_details($tbl_teacher_id);
		$data['teacher_obj'] 		= $teacher_obj;
		
		$assign_classes_obj 	    = $this->model_teachers->get_assign_classes($tbl_teacher_id,$tbl_school_id);
		$data['assign_classes_obj'] = $assign_classes_obj;
		
		$assign_subjects_obj 	    = $this->model_teachers->get_assign_subjects($tbl_teacher_id,$tbl_school_id);
		$data['assign_subjects_obj']= $assign_subjects_obj;
		
		
		
		$this->load->view('admin/view_template', $data);
	}


    function save_teacher_changes() {
   
    	if ($_POST) {
			$tbl_teacher_id            = $_POST['teacher_id_enc'];		
			$first_name                = $_POST['first_name'];
			$last_name                 = $_POST['last_name'];
			$first_name_ar             = $_POST['first_name_ar'];
			$last_name_ar         	   = $_POST['last_name_ar'];		
			$dob_month                 = $_POST['dob_month'];
			$dob_day                   = $_POST['dob_day'];
			$dob_year                  = $_POST['dob_year'];
			$gender                    = $_POST['gender'];		
			$mobile                    = $_POST['mobile'];
			$email                     = $_POST['email'];
			$country                   = $_POST['country'];
			$emirates_id               = $_POST['emirates_id'];		
			$tbl_school_roles_id       = $_POST['tbl_school_roles_id'];
			
			$strClass 				   = $_POST['tbl_class_id']; //e.g." tbl_class_id=f31981d6285a6a958f754&tbl_class_id=d99f9fee0&tbl_class_id=a343d70666c3
			$strClass 				   = explode(":", $strClass);
			
			$strSubject 			   = $_POST['tbl_subject_id']; //e.g." tbl_class_id=f31981d6285a6a958f754&tbl_class_id=d99f9fee0&tbl_class_id=a343d70666c3
			$strSubject                = explode(":", $strSubject);
			
			$tbl_class_teacher_id      = $_POST['tbl_class_teacher_id'];
			$user_id                   = $_POST['user_id'];
			$password                  = $_POST['password'];

		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_teachers");	
		$resData = $this->model_teachers->update_teacher($tbl_teacher_id, $first_name, $last_name, $first_name_ar, $last_name_ar, $dob_month, $dob_day, $dob_year, 
		                                             $gender, $mobile, $email, $country, $emirates_id, $tbl_school_roles_id, $tbl_class_teacher_id, $user_id, $password, $tbl_school_id);
		if ($resData=="Y") {
			$delAssignClasses = $this->model_teachers->delete_assign_classes($tbl_teacher_id, $tbl_school_id);
			for($m=0; $m<count($strClass); $m++) {
				if($strClass[$m]<>""){
					$tbl_teacher_class_id = substr(md5(uniqid(rand())),0,15);
					$resTeacher = $this->model_teachers->assign_classes($tbl_teacher_class_id, $strClass[$m], $tbl_teacher_id, $tbl_school_id);
				}
		    }
			
			$delAssignSubject = $this->model_teachers->delete_assign_subjects($tbl_teacher_id, $tbl_school_id);
			for($n=0; $n<count($strSubject); $n++) {
				if($strSubject[$n]<>""){
					$tbl_teacher_subject_id = substr(md5(uniqid(rand())),0,15);
					$resSubject = $this->model_teachers->assign_subjects($tbl_teacher_subject_id, $strSubject[$n], $tbl_teacher_id, $tbl_school_id);
				}
		    }
			
		}
		
	   if($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
		
	}
	
    // END TEACHER ADD
	
	// START TEACHERS ROLES
	
    /**
	* @desc    Show all teacher roles
	* @param   none
	* @access  default
	*/
    function school_role() {
		$data['page'] = "view_teacher_role";
		$data['menu'] = "members";
        $data['sub_menu'] = "teachers";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "role";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "role";
					 break;
				}
				default: {
					$sort_name = "role";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_category_id = "";
		if (array_key_exists('tbl_school_type_id',$param_array)) {
			$tbl_school_type_id = $param_array['tbl_school_type_id'];
			$data['tbl_sel_school_type_id'] = $tbl_school_type_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_school_roles");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			$rs_all_school_roles      = $this->model_school_roles->get_all_school_roles($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id);
			$total_school_roles       = $this->model_school_roles->get_total_school_roles($q, $is_active, $tbl_school_id);
		}else{
			$rs_all_school_roles     = array();
			$total_school_roles      = array();
		}
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/teacher/school_role";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_school_type_id) && trim($tbl_school_type_id)!="") {
			$page_url .= "/tbl_school_type_id/".rawurlencode($tbl_school_type_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_school_roles;
		$config['per_page'] = TBL_SCHOOL_ROLES_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_SCHOOL_ROLES_PAGING >= $total_school_roles) {
			$range = $total_school_roles;
		} else {
			$range = $offset+TBL_SCHOOL_ROLES_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_school_roles roles</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_school_roles']	     = $rs_all_school_roles;
		$data['total_school_roles'] 	      = $total_school_roles;

		$this->load->view('admin/view_template',$data);
	}
	
	function is_exist_role() {
		if ($_POST) {
			$tbl_school_roles_id    = $_POST['school_roles_id_enc'];		
			$role                   = $_POST['role'];
			$role_ar                = $_POST['role_ar'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_school_roles");		
		$results = $this->model_school_roles->is_exist_role($tbl_school_roles_id, $role, $role_ar, $tbl_school_id);
		if (count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
    /**
	* @desc    Create school role
	* @param   POST array
	* @access  default
	*/
    function create_school_role() {
		if ($_POST) {
			$tbl_school_roles_id= $_POST['school_roles_id_enc'];	
			$role               = $_POST['role'];
			$role_ar            = $_POST['role_ar'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_school_roles");	
		$resData = $this->model_school_roles->create_school_role($tbl_school_roles_id, $role, $role_ar, $tbl_school_id);
		if ($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}

    /**
	* @desc    Activate school role
	* @param   String school_type_id_enc
	* @access  default
	*/
    function activateRole() {
		if(isset($_POST) && count($_POST) != 0) {
			$school_roles_id_enc = trim($_POST["school_roles_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_school_roles");		
		$this->model_school_roles->activate_role($school_roles_id_enc,$tbl_school_id);
	}


	/**
	* @desc    Deactivate school role
	* @param   String school_type_id_enc
	* @access  default
	*/
    function deactivateRole() {
		if(isset($_POST) && count($_POST) != 0) {
			$school_roles_id_enc = trim($_POST["school_roles_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_school_roles");		
		$this->model_school_roles->deactivate_role($school_roles_id_enc,$tbl_school_id);
	}

     /**
	* @desc    Delete school role
	* @param   POST array
	* @access  default
	*/
     
	function deleteRole() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['school_roles_id_enc']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('school_roles_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_school_roles");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_school_roles->delete_role($str[$i],$tbl_school_id);
		}
	}
	
	/**
	* @desc    Save changes
	* @param   POST array
	* @access  default
	*/
    function save_role_changes() {
		if ($_POST) {
			$tbl_school_roles_id= $_POST['school_roles_id_enc'];	
			$role               = $_POST['role'];
			$role_ar            = $_POST['role_ar'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_school_roles");			
		$this->model_school_roles->save_role_changes($tbl_school_roles_id, $role, $role_ar, $tbl_school_id);
	}

	/**
	* @desc    Edit school role
	* @param   none
	* @access  default
	*/
    function edit_school_role() {
		$data['page'] = "view_teacher_role";
		$data['menu'] = "members";
        $data['sub_menu'] = "teachers";
		
		$data['mid'] = "3";
		$this->load->model("model_school_roles");
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_school_role_id = 0;
		if (array_key_exists('school_roles_id_enc',$param_array)) {
			$tbl_school_role_id = $param_array['school_roles_id_enc'];
		}	 
		
		//School Role details
		$school_role_obj = $this->model_school_roles->get_school_role_obj($tbl_school_role_id);
		$data['school_role_obj'] = $school_role_obj;
		$this->load->view('admin/view_template', $data);
	}

	// END TEACHERS ROLES
	
	/********************************************************************/
	

	/**
	* @desc    Validate teacher credentials and generate session
	*
	* @param   none
	* @access  default
	*/
    function ajax_validate_and_login_teacher() {
		if(isset($_POST) && count($_POST) != 0) {
			$username 	= trim($_POST["username"]);
			$password 	= trim($_POST["password"]);
			$remember_me = trim($_POST["remember_me"]);
			$is_ajax 	 = trim($_POST["is_ajax"]);
		} else {
			echo "*N*";	
		exit;
		}

		if (trim($remember_me) == "Y") {
			$year = time() + 31536000;
			setcookie('remember_me', $email, $year);	
		} else {
			if(isset($_COOKIE['remember_me'])) {
				$past = time() - 100;
				setcookie('remember_me', '', $past);
			}
		}
		
        $this->load->model('model_teachers');
        $is_teacher_status = $this->model_teachers->authenticate_t($username, $password);
		
		switch($is_teacher_status) {
			case("D"): {//User does not exist
				echo "*N*";		
				exit;
				}
			case("N"): {
				echo "*N*";		
				exit;
				}
			case("Y"): {
				
					//$teacher_obj = $this->model_teachers->get_teachers_obj("", $username);
					$tbl_teacher_id     = $this->model_teachers->get_teachers_id($username);
			        $teacher_obj        = $this->model_teachers->get_teacher_details($tbl_teacher_id);
					//Create teacher sessions
					$_SESSION['aqdar_smartcare']['tbl_teacher_id_sess']  = $teacher_obj[0]['tbl_teacher_id'];
					$_SESSION['aqdar_smartcare']['teacher_email_sess']   = $teacher_obj[0]['email'];
					$_SESSION['aqdar_smartcare']['teacher_user_id_sess'] = $teacher_obj[0]['user_id'];
					$_SESSION['aqdar_smartcare']['tbl_school_id_sess']   = $teacher_obj[0]['tbl_school_id'];
					
					$_SESSION['tbl_teacher_id_sess']  = $teacher_obj[0]['tbl_teacher_id'];
					$_SESSION['teacher_email_sess']   = $teacher_obj[0]['email'];
					$_SESSION['teacher_user_id_sess'] = $teacher_obj[0]['user_id'];
					$_SESSION['tbl_school_id_sess']   = $teacher_obj[0]['tbl_school_id'];
					
					/*
					$_SESSION['aqdar_smartcare']['teacher_first_name_en_sess'] = $teacher_obj['first_name_en'];
					$_SESSION['aqdar_smartcare']['teacher_first_name_ar_sess'] = $teacher_obj['first_name_ar'];
					$_SESSION['aqdar_smartcare']['teacher_last_name_en_sess'] = $teacher_obj['last_name_en'];
					$_SESSION['aqdar_smartcare']['teacher_last_name_ar_sess'] = $teacher_obj['last_name_ar'];
					*/
					
					$_SESSION['aqdar_smartcare']['teacher_picture_sess'] = $teacher_obj[0]['file_name_updated'];
					$_SESSION['aqdar_smartcare']['added_date_sess'] = date('M d, Y', strtotime($teacher_obj[0]['added_date']));
					$_SESSION['aqdar_smartcare']['user_type_sess'] = "teacher";
					
					$_SESSION['teacher_picture_sess'] = $teacher_obj[0]['file_name_updated'];
					$_SESSION['added_date_sess'] = date('M d, Y', strtotime($teacher_obj[0]['added_date']));
					$_SESSION['user_type_sess'] = "teacher";


					//If name is not in Arabic then it will be in English and vice versa	
					if (trim($teacher_obj['first_name_en']) == "") {
						$_SESSION['aqdar_smartcare']['teacher_first_name_sess'] = $teacher_obj[0]['first_name_ar'];	
						$_SESSION['aqdar_smartcare']['teacher_last_name_sess']  = $teacher_obj[0]['last_name_ar'];	
					} else {
						$_SESSION['aqdar_smartcare']['teacher_first_name_sess'] = $teacher_obj[0]['first_name_en'];	
						$_SESSION['aqdar_smartcare']['teacher_last_name_sess']  = $teacher_obj[0]['first_name_en'];	
					}

                    
					//Update last login time
					//$this->model_teachers->update_last_login($email);
					
				echo "*Y*";		
				exit;
				}
		}
	echo "*N*";		
	exit;
	}


	/**
	* @desc    Assign points by teacher
	*
	* @param   none
	* @access  default
	*/
	function teacher_all_classes() {
		$data["module_id"] 			= $_GET["module_id"];
		$data["issue_cards"] 		= $_GET["issue_cards"];
		$data["contact_parents"] 	= $_GET["contact_parents"];
		$data["performance_calc"] 	= $_GET["performance_calc"];
		$data["progress_report"] 	= $_GET["progress_report"];

		$data["page"] = "view_teacher_all_classes";
		$this->load->view('admin/view_teacher_template', $data);				
	}



	/**
	* @desc    Show contact parents sub form
	*
	* @param   none
	* @access  default
	*/
	function teacher_msg_list_page_web() {
		$user_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		$tbl_teacher_id_from = $_REQUEST["tbl_teacher_id_from"];
		$tbl_teacher_id_to = $_REQUEST["tbl_teacher_id_to"];
		$src_id = $_REQUEST["src_id"];					// 
		$tbl_teacher_group_id = $_REQUEST["tbl_teacher_group_id"]; 
			
		$data["user_id"] = $user_id;
		$data["role"] = $role;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] = $school_id;
		$data["tbl_teacher_id_from"] = $tbl_teacher_id_from;
		$data["tbl_teacher_id_to"] = $tbl_teacher_id_to;
		$data["tbl_teacher_group_id"] = $tbl_teacher_group_id;
		
		$tbl_school_id = $school_id;
		$tbl_teacher_id = $user_id;
		
		$this->load->model('model_message');
		$data_rs = $this->model_message->get_teacher_messages($tbl_teacher_id_from, $tbl_teacher_id_to, $tbl_teacher_group_id,$tbl_school_id);

		// Change all is_read of messages from from N to Y
		$this->model_message->update_unread_msg_count($tbl_teacher_id_from, $tbl_teacher_id_to, $tbl_school_id, $tbl_teacher_group_id);
		$data["data_rs"] = $data_rs;

		$data["page"] = "view_teacher_messages";
		$this->load->view('admin/view_teacher_template',$data);
	}
	


	/**
	* @desc    Show teachers login page
	*
	* @param   none
	* @access  default
	*/
	function teachers_roles_web() {
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["school_id"];
		$tbl_teacher_id = $_REQUEST["user_id"];
		
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		$data["tbl_teacher_id"] = $tbl_teacher_id;

		$this->load->model('model_message');
		$rs_groups = $this->model_message->get_message_groups_against_teacher($tbl_teacher_id, $tbl_school_id);
		//print_r($rs_groups);
		
		$data["rs_groups"] = $rs_groups;
		$data["page"] = "view_teacher_groups";
		$this->load->view('admin/view_teacher_template',$data);
	}
	
	// GENERATE TEACHER ID CARDS 
	
	  function generate_teacher_id() {

		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "first_name";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name";
					 break;
				}
				default: {
					$sort_name = "first_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_teacher_id',$param_array)) {
			$tbl_teacher_id = $param_array['tbl_teacher_id'];
			$data['tbl_sel_teacher_id'] = $tbl_teacher_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		$this->load->model("model_teachers");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			$rs_all_teachers      = $this->model_teachers->list_all_teachers($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_class_id);
			$total_teachers       = $this->model_teachers->get_total_teachers($q, $is_active, $tbl_school_id,$tbl_class_id);
		}else{
			$rs_all_teachers     = array();
			$total_teachers      = array();
		}
	
		$data['rs_all_teachers']	     = $rs_all_teachers;
		$data['total_teachers'] 	      = $total_teachers;

		$this->load->view('admin/pdf_teacher_id_cards',$data);
	}
	
	
	// END GENERATE PARENT IDS
	
	function performance_monitor_form() {

		$user_id 		= $_REQUEST["user_id"];
		$role 			= $_REQUEST["role"];
		$lan 			= $_REQUEST["lan"];
		$device 		= $_REQUEST["device"];
		$device_uid 	= $_REQUEST["device_uid"];
		$school_id 		= $_REQUEST["school_id"];
		$tbl_class_id 	= $_REQUEST["tbl_class_id"];
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		
			
		$tbl_semester_id 	    = $_REQUEST["tbl_semester_id"];
		$data["tbl_teacher_id"] = $user_id;
		$data["role"] 		    = $role;
		$data["lan"] 		    = $lan;
		$data["device"] 	    = $device;
		$data["device_uid"]     = $device_uid;
		$data["tbl_school_id"] 	= $school_id;
		$data["tbl_class_id"]   = $tbl_class_id;
		$data["tbl_student_id"] = $tbl_student_id;
		$this->load->model('model_student');
		$current_date           = date("Y-m-d");
			
		$data["semester_title"] = "";
	    if($tbl_semester_id=="")
		{
			$getSemesterId   = $this->model_student->get_semester_id($school_id,$current_date);
			$tbl_semester_id = $getSemesterId[0]['tbl_semester_id'];
			if($lan=="en")
				$semester_title = $getSemesterId[0]['title'];
			else
				$semester_title = $getSemesterId[0]['title_ar'];
		
		 $data["semester_title"] = $semester_title;
		}
			
		$rs_cards = $this->model_student->get_all_cards_student($tbl_student_id,$tbl_semester_id);
		$cardTypeArray = array();
		for($p=0;$p<count($rs_cards);$p++)
		
		{
			$cardTypeArray[$p] = 	$rs_cards[$p]['card_type'];
		}
		$this->load->model('model_config');
		$this->load->model('model_classes');
		$this->load->model('model_student');
		//new change
		//$data_config = $this->model_config->get_config($school_id);
		$arr_cards = array();
		
		if($tbl_class_id==""){
			$student_info = $this->model_student->get_student_obj($tbl_student_id);
			$tbl_class_id = $student_info[0]['tbl_class_id'];
		}
	
		if($tbl_class_id<>""){
			$class_info = $this->model_classes->getClassInfo($tbl_class_id);
		}
		
		
		$topic_categories = $this->model_config->get_performance_topics_new($tbl_student_id,$tbl_class_id,$school_id);
		
		
		for($m=0;$m<count($topic_categories);$m++)
		{
			$arr_cards[$m]['id'] 					= $topic_categories[$m]['tbl_performance_id'];
			$arr_cards[$m]['title'] 				= $topic_categories[$m]['topic_en'];
			$arr_cards[$m]['title_ar']  			= $topic_categories[$m]['topic_ar'];
			$arr_cards[$m]['performance_value']  	= isset($topic_categories[$m]['performance_value'])? $topic_categories[$m]['performance_value']: '0' ;
			
		}
		
		$rs_student_point 	= $this->model_student->get_all_cards_student($tbl_student_id, $tbl_semester_id);
		$total = 0;
		if(!empty($rs_student_point))
		{
			for($m=0;$m<count($rs_student_point);$m++)
			{
				$total =	$total + $rs_student_point[$m]['card_point'];
			}
		}
		// Get Student Picture
		$this->load->model('model_student');
		$file_name_updated = $this->model_student->get_student_picture($tbl_student_id);
		$this->load->model('model_teachers');
		$all_classes_against_teacher_rs = $this->model_teachers->get_all_classes_against_teacher_t($tbl_teacher_id);
		$data["file_name_updated"] = $file_name_updated;
		$data["all_classes_against_teacher_rs"] = $all_classes_against_teacher_rs;
		//$data["data_config"] = $data_config;
		$data["cards"] = $arr_cards;
		$rs_total_mark_card = $this->model_student->get_total_mark_cards($school_id);
		$total_card_mark = isset($rs_total_mark_card[0]['total_points'])? $rs_total_mark_card[0]['total_points']:'0' ;
		$data["total_mark"] 	= $total_card_mark;
		//$data["total_points"] 	= $total_card_mark + $total_points ; 
		$total_points  = $total_card_mark - abs($total);
		$data["total_points"] 	= $total_card_mark." - ".abs($total)." = ".$total_points ; 
		
		//Teacher admin
		if (trim($_POST["is_admin"]) == "Y") {
			$this->load->view('admin/view_performance_topics_form',$data);
			return;
		}	
		
		//echo json_encode($data);
		//$data["page"] = "view_issue_cards_form";
		//$this->load->view('view_template',$data);
		//end new change
	}
	
	
	function progress_report_form() {

		$user_id 		= $_REQUEST["user_id"];
		$role 			= $_REQUEST["role"];
		$lan 			= $_REQUEST["lan"];
		$device 		= $_REQUEST["device"];
		$device_uid 	= $_REQUEST["device_uid"];
		$school_id 		= $_REQUEST["school_id"];
		$tbl_class_id 	= $_REQUEST["tbl_class_id"];
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		
			
		$tbl_semester_id 	    = $_REQUEST["tbl_semester_id"];
		$data["tbl_teacher_id"] = $user_id;
		$data["role"] 		    = $role;
		$data["lan"] 		    = $lan;
		$data["device"] 	    = $device;
		$data["device_uid"]     = $device_uid;
		$data["tbl_school_id"] 	= $school_id;
		$data["tbl_class_id"]   = $tbl_class_id;
		$data["tbl_student_id"] = $tbl_student_id;
		$this->load->model('model_student');
		$current_date           = date("Y-m-d");
			
		$data["semester_title"] = "";
	    if($tbl_semester_id=="")
		{
			$getSemesterId   = $this->model_student->get_semester_id($school_id,$current_date);
			$tbl_semester_id = $getSemesterId[0]['tbl_semester_id'];
			if($lan=="en")
				$semester_title = $getSemesterId[0]['title'];
			else
				$semester_title = $getSemesterId[0]['title_ar'];
		
		 $data["semester_title"] = $semester_title;
		}
		
		$this->load->model('model_config');
		$this->load->model('model_classes');
		$this->load->model('model_student');
		$this->load->model('model_school_subject');
		
		$rs_subject 		= $this->model_school_subject->get_all_school_subjects('', '', '', $q, 'Y', $school_id);
		
		$rs_progress_report = $this->model_student->get_progress_report_list($tbl_school_id, $tbl_teacher_id);
		
		// Progress Student Report List
		
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "id";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "id";
					 break;
				}
				default: {
					$sort_name = "id";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_school_roles");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			$rs_all_progress_reports_student      = $this->model_student->get_all_progress_reports_student($sort_name, $sort_by, $offset, $q, $is_active, $tbl_class_id, $tbl_student_id, $tbl_school_id);
			$total_progress_reports_student       = $this->model_student->get_total_progress_reports_student($q, $is_active, $tbl_class_id, $tbl_student_id, $tbl_school_id);
		}else{
			$rs_all_progress_reports_student     = array();
			$total_progress_reports_student      = array();
		}
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/teacher/school_role";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_school_type_id) && trim($tbl_school_type_id)!="") {
			$page_url .= "/tbl_school_type_id/".rawurlencode($tbl_school_type_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_progress_reports_student;
		$config['per_page'] = TBL_SCHOOL_ROLES_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_SCHOOL_ROLES_PAGING >= $total_progress_reports_student) {
			$range = $total_progress_reports_student;
		} else {
			$range = $offset+TBL_SCHOOL_ROLES_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_progress_reports_student reports</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		// End Progress Student Report List
	
		//new change
		//$data_config = $this->model_config->get_config($school_id);
		$arr_cards = array();
		
		if($tbl_class_id==""){
			$student_info = $this->model_student->get_student_obj($tbl_student_id);
			$tbl_class_id = $student_info[0]['tbl_class_id'];
		}
	
		if($tbl_class_id<>""){
			$class_info = $this->model_classes->getClassInfo($tbl_class_id);
		}
	
		// Get Student Picture
		$file_name_updated = $this->model_student->get_student_picture($tbl_student_id);
	
		$this->load->model('model_teachers');
		$all_classes_against_teacher_rs 		= $this->model_teachers->get_all_classes_against_teacher_t($tbl_teacher_id);
		$data["file_name_updated"] 				= $file_name_updated;
		$data["all_classes_against_teacher_rs"] = $all_classes_against_teacher_rs;
		$data["rs_subject"] 					= $rs_subject;
		$data["rs_progress_report"] 			= $rs_progress_report;  // category

        $data["progress_reports_student"] 		= $rs_all_progress_reports_student;   
		$data["total_progress_reports_student"] = $total_progress_reports_student;   
       
	    
		//Teacher admin
		if (trim($_POST["is_admin"]) == "Y") {
			$this->load->view('admin/view_progress_report_form',$data);
			return;
		}	
		
	}
	
	
	function view_progress_report() {
		$data['page']     = "view_progress_report";
		$data['menu']     = "progress_report";
        $data['sub_menu'] = "progress_report";
		
		$this->load->model("model_student");
		$tbl_progress_report_id  = $_POST['tbl_progress_report_id'];	
		$tbl_progress_id         = $_POST['tbl_progress_id'];	
		$tbl_school_id           = $_POST['tbl_school_id'];	
		$tbl_student_id          = $_POST['tbl_student_id'];	
		$is_active = "";
		if($tbl_school_id<>"")
		{  
		   $progress_report_details          = $this->model_student->get_progress_report_details($tbl_progress_report_id, $tbl_progress_id, $tbl_class_id, $tbl_student_id, $tbl_school_id);
	       $data['progress_report_details'] = $progress_report_details;  
		}
		
		$this->load->view('parent/view_progress_report_details.php', $data);
	}
	
	
 
	
}
?>