<?php

/**
 * @desc   	  	Message Controller
 * @category   	Controller
 * @author     	Shanavas PK
 * @version    	0.1
 */
class Message extends CI_Controller {
	/**
	* @desc    Default function for the Controller
	* @param   none
	* @access  default
	*/
    function index() {
		
	}

	//SEND MESSAGE TO TEACHERS......................
    /**
	* @desc    Show all school message to teachers
	* @param   none
	* @access  default
	*/
	
    function message_to_teachers() {
		$data['page'] = "view_message_to_teachers";
		$data['menu'] = "messages";
        $data['sub_menu'] = "message_to_teachers";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "id";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "id";
					 break;
				}
				default: {
					$sort_name = "id";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_teacher_id',$param_array)) {
			$tbl_teacher_id = $param_array['tbl_teacher_id'];
			$data['tbl_sel_teacher_id'] = $tbl_teacher_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_message");
		$this->load->model("model_teachers");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  	
		if($tbl_school_id<>"")
		{  
			$rs_all_messages     = $this->model_message->list_teacher_messages_from_school($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_teacher_id);
			$total_messages      = $this->model_message->get_total_teacher_messages_from_school($q, $is_active, $tbl_school_id,$tbl_teacher_id);
			for($m=0;$m<count($rs_all_messages); $m++)
			{
				$cntTeacher                 = $this->model_message->get_teacher_cnt_msg_from_school($rs_all_messages[$m]['message'],$tbl_school_id);
				$cntSentNotification        = $this->model_message->get_sent_notification_cnt_msg_from_school($rs_all_messages[$m]['message'],$tbl_school_id);
				$notification_teachers_list = $this->model_message->get_teachers_list_msg_from_school($rs_all_messages[$m]['message'],$tbl_school_id);
				$rs_all_messages[$m]['cntTeacher']                  = $cntTeacher;
				$rs_all_messages[$m]['cntSentNotification']         = $cntSentNotification;
				$rs_all_messages[$m]['notification_teachers_list']  = $notification_teachers_list;
			}
			$rs_all_teachers        = $this->model_teachers->get_list_teachers($tbl_school_id,$tbl_class_id,'Y');
			$rs_teacher_groups      = $this->model_teachers->get_list_teacher_group($tbl_school_id,'Y');
			
		}else{
			$rs_all_teachers     = array();
			$rs_all_messages     = array();
			$total_messages      = array();
		}
	
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/message/message_to_teachers";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_teacher_id) && trim($tbl_teacher_id)!="") {
			$page_url .= "/tbl_teacher_id/".rawurlencode($tbl_teacher_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_messages;
		$config['per_page'] = TBL_MESSAGE_TEACHER_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_MESSAGE_TEACHER_PAGING >= $total_messages) {
			$range = $total_messages;
		} else {
			$range = $offset+TBL_MESSAGE_TEACHER_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_messages messages</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_teacher_groups']	   = $rs_teacher_groups;
		$data['rs_all_teachers']	     = $rs_all_teachers;
		$data['rs_all_messages']	     = $rs_all_messages;
		$data['total_messages'] 	      = $total_messages;

		$this->load->view('admin/view_template',$data);
	}
	
	
	function send_message_to_teacher()
	{
		$message_from      = $_SESSION['aqdar_smartcare']['tbl_admin_id_sess'];
		$message_from_type = "A"; //Admin User
		$message           = $_POST['message'];
		$this->load->model("model_message");
		$tbl_teacher_group_id = $_POST['tbl_teacher_group_id'];
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
		if($tbl_school_id<>"")
		{  
			if($tbl_teacher_group_id<>"")
			{
				$this->model_message->send_group_message_teachers($tbl_teacher_group_id,$message_from,$message_from_type,$message,$tbl_school_id);
				
				$this->model_message->send_grp_msg_notification($tbl_teacher_group_id,$message_from,$message_from_type,$message,$tbl_school_id);
				
				
			}else{
				$str  = $_POST['tbl_teacher_id']; //e.g." tbl_class_id=f31981d6285a6a958f754&tbl_class_id=d99f9fee0&tbl_class_id=a343d70666c3
				$this->model_message->send_to_teachers_message($str,$message_from,$message_from_type,$message,$tbl_school_id);
				$this->model_message->send_msg_notification($str,$message_from,$message_from_type,$message,$tbl_school_id);
			}
		   echo "Y"; 
		}else{
		  echo "N";	
		}
	}
	
	  /**
	* @desc    Activate Teacher Message
	* @param   String 
	* @access  default
	*/
    function activateTeacherMessage() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_message_group_id = trim($_POST["tbl_message_group_id"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_message");		
		$this->model_message->activate_teacher_message($tbl_message_group_id,$tbl_school_id);
	}

	/**
	* @desc    Deactivate Teacher Message
	* @param   String school_type_id_enc
	* @access  default
	*/
    function deactivateTeacherMessage() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_message_group_id = trim($_POST["tbl_message_group_id"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_message");		
		$this->model_message->deactivate_teacher_message($tbl_message_group_id,$tbl_school_id);
	}

     /**
	* @desc    Delete Teacher Message
	* @param   POST array
	* @access  default
	*/
	function deleteTeacherMessage() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['tbl_message_group_id']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('tbl_message_group_id=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_message");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_message->delete_teacher_message($str[$i],$tbl_school_id);
		}
	}
	
	//edit functionality
	/**
	* @desc    Edit teacher
	* @param   none
	* @access  default
	*/
    function edit_message_to_teacher() {
		$data['page'] = "view_message_to_teachers";
		$data['menu'] = "messages";
        $data['sub_menu'] = "message_to_teachers";
		$data['mid'] = "3";
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		$this->load->model("model_teachers");
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_teacher_id = 0;
		if (array_key_exists('tbl_message_group_id',$param_array)) {
			$tbl_message_group_id = $param_array['tbl_message_group_id'];
		}
		
		$this->load->model("model_message");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  	
		if($tbl_school_id<>"")
		{  
		   $results = $this->model_message->get_school_message_to_teacher($tbl_message_group_id,$tbl_school_id);
	       $data['school_message'] = $results;  
		}
		
		$this->load->view('admin/view_template', $data);
	}

    
	
	function update_message_to_teacher()
	{
		$message              = $_POST['message'];
		$this->load->model("model_message");
		$tbl_message_group_id = $_POST['tbl_message_group_id'];
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
		if($tbl_school_id<>"")
		{  
		  $this->model_message->update_message_to_teacher($tbl_message_group_id,$message,$tbl_school_id);
		   echo "Y"; 
		}else{
		  echo "N";	
		}
	}
	
	//END SEND MESSAGE TO TEACHERS......................
	
	//SEND MESSAGE TO PARENTS......................
	/**
	* @desc    Show all school message to parents
	* @param   none
	* @access  default
	*/
    function message_to_parents() {
		$data['page'] = "view_message_to_parents";
		$data['menu'] = "messages";
        $data['sub_menu'] = "message_to_parents";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "id";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "id";
					 break;
				}
				default: {
					$sort_name = "id";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_teacher_id',$param_array)) {
			$tbl_teacher_id = $param_array['tbl_teacher_id'];
			$data['tbl_sel_teacher_id'] = $tbl_teacher_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_message");
        $this->load->model("model_class_sessions");		
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  	
		if($tbl_school_id<>"")
		{  
			$classesObj = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
			$data['classes_list'] = $classesObj;
			
			$rs_all_messages     = $this->model_message->list_parents_messages_from_school($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_teacher_id);
			$total_messages      = $this->model_message->get_total_parents_messages_from_school($q, $is_active, $tbl_school_id,$tbl_teacher_id);
			
			for($m=0;$m<count($rs_all_messages); $m++)
			{
				$cntParents                 = $this->model_message->get_parent_cnt_msg_from_school($rs_all_messages[$m]['tbl_message_group_id'],$tbl_school_id);
				$notification_classes_list  = $this->model_message->get_class_list_parent_msg_from_school($rs_all_messages[$m]['tbl_message_group_id'],$tbl_school_id);
				$rs_all_messages[$m]['cntParents']   = $cntParents;
				$rs_all_messages[$m]['notification_classes_list']  = $notification_classes_list;
				
			}
			
		}else{
			$rs_all_messages     = array();
			$total_messages      = array();
		}
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/message/message_to_parents";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_teacher_id) && trim($tbl_teacher_id)!="") {
			$page_url .= "/tbl_teacher_id/".rawurlencode($tbl_teacher_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_messages;
		$config['per_page'] = TBL_MESSAGE_PARENT_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_MESSAGE_PARENT_PAGING >= $total_messages) {
			$range = $total_messages;
		} else {
			$range = $offset+TBL_MESSAGE_PARENT_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_messages messages</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_messages']	     = $rs_all_messages;
		$data['total_messages'] 	      = $total_messages;

		$this->load->view('admin/view_template',$data);
	}
	
	
	function send_message_to_parent()
	{
		$message_from      = $_SESSION['aqdar_smartcare']['tbl_admin_id_sess'];
		$message_from_type = "A"; //Admin User
		$message           = $_POST['message'];
		$this->load->model("model_message");
		$this->load->model("model_parents");
		$tbl_class_id     	= $_POST['tbl_class_id'];
		$tbl_school_id 		= $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
		$tbl_student_id		=  $_POST['tbl_student_id'];
		if($tbl_school_id<>"")
		{  
			if($tbl_student_id<>"")
			{
				$str  = $_POST['tbl_student_id']; //e.g." tbl_class_id=f31981d6285a6a958f754&tbl_class_id=d99f9fee0&tbl_class_id=a343d70666c3
				$str  = explode("&", $str); 
				$str = str_replace('student_id_enc=', '', $str);
			    for($m=0; $m<count($str); $m++) {
					  if($str[$m]<>""){
						$data =  explode("*",$str[$m]); 
						$tbl_student_id_t = $data[0];
						$tbl_class_id_t  = $data[1];
						$tbl_parenting_school_assign_id = substr(md5(uniqid(rand())),0,10);
						$my_parent       = $this->model_parents->get_my_parent_id($tbl_student_id_t);
						if(count($my_parent)>0)
						{
							$this->model_message->send_to_parents_message($my_parent,$message_from,$message_from_type,$message,$tbl_school_id,$tbl_class_id_t,$tbl_message_group_id);
						}
					  }
			    }
			  echo "Y";
			}else if($tbl_class_id<>"")
			{
				$str  = $_POST['tbl_class_id']; //e.g." tbl_class_id=f31981d6285a6a958f754&tbl_class_id=d99f9fee0&tbl_class_id=a343d70666c3
				$tbl_message_group_id = substr(md5(uniqid(rand())),0,10);
			    $str  = explode("&", $str);
			    for($m=0; $m<count($str); $m++) {
				  if($str[$m]<>""){
					$tbl_class_id_t = $str[$m];
					$tbl_message_parent_id = substr(md5(uniqid(rand())),0,10);
					$parent_list = $this->model_parents->get_parents_based_on_class($tbl_school_id, $tbl_class_id_t);
					$this->model_message->send_to_parents_message($parent_list,$message_from,$message_from_type,$message,$tbl_school_id,$tbl_class_id_t,$tbl_message_group_id);
					//$this->model_message->send_parent_msg_notification($parent_list,$message_from,$message_from_type,$message,$tbl_school_id);
				  }
				}
			}
		   echo "Y"; 
		}else{
		  echo "N";	
		}
	}
	
	  /**
	* @desc    Activate Teacher Message
	* @param   String 
	* @access  default
	*/
    function activateParentMessage() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_message_group_id = trim($_POST["tbl_message_group_id"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_message");		
		$this->model_message->activate_teacher_message($tbl_message_group_id,$tbl_school_id);
	}

	/**
	* @desc    Deactivate Teacher Message
	* @param   String school_type_id_enc
	* @access  default
	*/
    function deactivateParentMessage() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_message_group_id = trim($_POST["tbl_message_group_id"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_message");		
		$this->model_message->deactivate_teacher_message($tbl_message_group_id,$tbl_school_id);
	}

     /**
	* @desc    Delete Teacher Message
	* @param   POST array
	* @access  default
	*/
	function deleteParentMessage() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['tbl_message_group_id']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('tbl_message_group_id=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_message");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_message->delete_parent_message($str[$i],$tbl_school_id);
		}
	}
	
	//edit functionality
	/**
	* @desc    Edit teacher
	* @param   none
	* @access  default
	*/
    function edit_message_to_parents() {
		$data['page'] = "view_message_to_parents";
		$data['menu'] = "messages";
        $data['sub_menu'] = "message_to_parents";
		$data['mid'] = "3";
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		$this->load->model("model_teachers");
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_teacher_id = 0;
		if (array_key_exists('tbl_message_group_id',$param_array)) {
			$tbl_message_group_id = $param_array['tbl_message_group_id'];
		}
		
		$this->load->model("model_message");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  	
		if($tbl_school_id<>"")
		{  
		   $results = $this->model_message->get_school_message_to_parent($tbl_message_group_id,$tbl_school_id);
	       $data['school_message'] = $results;  
		}
		
		$this->load->view('admin/view_template', $data);
	}

    
	
	function update_message_to_parent()
	{
		$message              = $_POST['message'];
		$this->load->model("model_message");
		$tbl_message_group_id = $_POST['tbl_message_group_id'];
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
		if($tbl_school_id<>"")
		{  
		  $this->model_message->update_message_to_parent($tbl_message_group_id,$message,$tbl_school_id);
		   echo "Y"; 
		}else{
		  echo "N";	
		}
	}
	
	
	//END SEND MESSAGE TO PARENTS......................
	
	
	// TEACHER MESSAGE GROUPS -  TEACHERS COMMUNITY
    /**
	* @desc    Show all school message - teachers community
	* @param   none
	* @access  default
	*/
	
    function teachers_group() {
		$data['page'] = "view_teachers_group";
		$data['menu'] = "configuration";
        $data['sub_menu'] = "teacher_group";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "id";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "id";
					 break;
				}
				default: {
					$sort_name = "id";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_teacher_id',$param_array)) {
			$tbl_teacher_id = $param_array['tbl_teacher_id'];
			$data['tbl_sel_teacher_id'] = $tbl_teacher_id;
		}
		
	
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_message");
		$this->load->model("model_teachers");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  	
		if($tbl_school_id<>"")
		{  
			// add purpose
			$rs_all_teachers        = $this->model_teachers->get_list_teachers($tbl_school_id,$tbl_class_id,'Y');
			
			//list purpose
			$rs_all_groups     = $this->model_message->list_teachers_group($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_teacher_id);
			$total_groups      = $this->model_message->get_total_teachers_group($q, $is_active, $tbl_school_id,$tbl_teacher_id);
			
			for($m=0;$m<count($rs_all_groups); $m++)
			{
				$teachers_list  = $this->model_message->get_teachers_list_in_group($rs_all_groups[$m]['tbl_teacher_group_id'],$tbl_school_id);
				$rs_all_groups[$m]['cntTeacher']      = count($teachers_list);
				$rs_all_groups[$m]['teachers_list']   = $teachers_list;
			}
			
		}else{
			$rs_all_teachers     = array();
			$rs_all_groups     = array();
			$total_groups      = array();
		}
	
	
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/message/teachers_group";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_teacher_id) && trim($tbl_teacher_id)!="") {
			$page_url .= "/tbl_teacher_id/".rawurlencode($tbl_teacher_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_groups;
		$config['per_page'] = TBL_TEACHER_GROUP_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_TEACHER_GROUP_PAGING >= $total_groups) {
			$range = $total_groups;
		} else {
			$range = $offset+TBL_TEACHER_GROUP_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_groups Teacher Groups</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_groups']	       = $rs_all_groups;
		$data['total_groups'] 	        = $total_groups;
		$data['rs_all_teachers']	     = $rs_all_teachers;
		$data['rs_all_messages']	     = $rs_all_messages;
		

		$this->load->view('admin/view_template',$data);
	}
	
	
	function save_teacher_group()
	{
		$this->load->model("model_message");
		$group_name_en        = $_POST['group_name_en'];
		$group_name_ar        = $_POST['group_name_ar'];
		$tbl_teacher_id       = $_POST['tbl_teacher_id'];
		$tbl_teacher_group_id = $_POST['tbl_teacher_group_id'];
		$tbl_school_id        = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
		
		$is_exist = $this->model_message->is_exist_teacher_group($tbl_teacher_group_id,$group_name_en,$tbl_school_id);
		if(count($is_exist)>0)
		{
		  echo "X";
		}else{
			
			if($tbl_school_id<>"")
			{  
				if($tbl_teacher_group_id<>"")
				{
					$this->model_message->save_teacher_group($tbl_teacher_group_id,$group_name_en,$group_name_ar,$tbl_teacher_id,$tbl_school_id);
				}
			   echo "Y"; 
			}else{
			  echo "N";	
			}
		}
	}
	
	  /**
	* @desc    Activate Teacher Group
	* @param   String 
	* @access  default
	*/
    function activateTeacherGroup() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_teacher_group_id = trim($_POST["tbl_teacher_group_id"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_message");		
		$this->model_message->activate_teacher_group($tbl_teacher_group_id,$tbl_school_id);
	}

	/**
	* @desc    Deactivate Teacher Group
	* @param   String school_type_id_enc
	* @access  default
	*/
    function deactivateTeacherGroup() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_teacher_group_id = trim($_POST["tbl_teacher_group_id"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_message");		
		$this->model_message->deactivate_teacher_group($tbl_teacher_group_id,$tbl_school_id);
	}

    /**
	* @desc    Delete Teacher Group
	* @param   POST array
	* @access  default
	*/
	function deleteTeacherGroup() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['tbl_teacher_group_id'];    
			 //e.g."school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('tbl_teacher_group_id=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_message");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_message->delete_teacher_group($str[$i],$tbl_school_id);
		}
	}
	
	//edit functionality
	/**
	* @desc    Edit teacher
	* @param   none
	* @access  default
	*/
    function edit_teacher_group() {
		$data['page']     = "view_teachers_group";
		$data['menu']     = "configuration";
        $data['sub_menu'] = "teacher_group";
		
		
		$data['mid'] = "3";
		$this->load->model("model_message");
		$this->load->model("model_teachers");
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_teacher_id = 0;
		if (array_key_exists('tbl_teacher_group_id',$param_array)) {
			$tbl_teacher_group_id = $param_array['tbl_teacher_group_id'];
		}
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  	
		$rs_all_teachers        = $this->model_teachers->get_list_teachers($tbl_school_id,$tbl_class_id,'Y');
		if($tbl_school_id<>"")
		{  
		   $results        = $this->model_message->get_teacher_group_info($tbl_teacher_group_id,$tbl_school_id);
		   $teachers_list  = $this->model_message->get_teachers_list_in_group($tbl_teacher_group_id,$tbl_school_id);
		   for($b=0;$b<count($teachers_list);$b++)
		   {
			   $assign_teachers_list[$b] = $teachers_list[$b]['tbl_teacher_id'];
		   }
		   
		   $data['assign_teachers_list']     = $assign_teachers_list;
	       $data['group_info']               = $results; 
		    
		}
		$data['rs_all_teachers']  = $rs_all_teachers;  
		$this->load->view('admin/view_template', $data);
	}

    
	// Update Teacher Group
	function update_teacher_group()
	{
		$this->load->model("model_message");
		$group_name_en        = $_POST['group_name_en'];
		$group_name_ar        = $_POST['group_name_ar'];
		$tbl_teacher_id       = $_POST['tbl_teacher_id'];
		$tbl_teacher_group_id = $_POST['tbl_teacher_group_id'];
		$tbl_school_id        = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
		$is_exist = $this->model_message->is_exist_teacher_group($tbl_teacher_group_id,$group_name_en,$tbl_school_id);
		if(count($is_exist)>0)
		{
		  echo "X";
		}else{
			if($tbl_school_id<>"")
			{  
			  $this->model_message->update_teacher_group($tbl_teacher_group_id,$group_name_en,$group_name_ar,$tbl_teacher_id,$tbl_school_id);
			   echo "Y"; 
			}else{
			  echo "N";	
			}
		}
	}
	
	//END TEACHER MESSAGE GROUPS -  TEACHERS COMMUNITY......................
	
	// TEACHER MESSAGE GROUPS -  TEACHERS COMMUNITY
    /**
	* @desc    Show all school message - teachers community
	* @param   none
	* @access  default
	*/
	
    function parents_group() {
		$data['page'] = "view_parents_group";
		$data['menu'] = "configuration";
        $data['sub_menu'] = "parent_group";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "id";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "id";
					 break;
				}
				default: {
					$sort_name = "id";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_parent_id',$param_array)) {
			$tbl_parent_id = $param_array['tbl_parent_id'];
			$data['tbl_sel_parent_id'] = $tbl_parent_id;
		}
		
	
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_message");
		$this->load->model("model_class_sessions");
		$this->load->model("model_teachers");
		$this->load->model("model_parents");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  	
		if($tbl_school_id<>"")
		{  
			// add purpose
			$classesObj = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
			$data['classes_list'] = $classesObj;
			
			//list purpose
			$rs_all_groups     = $this->model_message->list_parents_group($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parent_id);
			$total_groups      = $this->model_message->get_total_parents_group($q, $is_active, $tbl_school_id,$tbl_parent_id);
			
			for($m=0;$m<count($rs_all_groups); $m++)
			{
				$parents_list  = $this->model_message->get_parents_list_in_group($rs_all_groups[$m]['tbl_parent_group_id'],$tbl_school_id);
				$rs_all_groups[$m]['cntParents']      = count($parents_list);
				$rs_all_groups[$m]['parents_list']    = $parents_list;
			}
			
		}else{
			$data['classes_list'] = array();
			$rs_all_groups     = array();
			$total_groups      = array();
		}
	
	
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/message/parents_group";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_parent_id) && trim($tbl_parent_id)!="") {
			$page_url .= "/tbl_parent_id/".rawurlencode($tbl_parent_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_groups;
		$config['per_page'] = TBL_PARENT_GROUP_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_PARENT_GROUP_PAGING >= $total_groups) {
			$range = $total_groups;
		} else {
			$range = $offset+TBL_PARENT_GROUP_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_groups Parents Groups</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_groups']	       = $rs_all_groups;
		$data['total_groups'] 	        = $total_groups;
		$data['rs_all_teachers']	     = $rs_all_teachers;
		$data['rs_all_messages']	     = $rs_all_messages;
		

		$this->load->view('admin/view_template',$data);
	}
	
	
	function save_parent_group()
	{
		$this->load->model("model_message");
		$group_name_en        = $_POST['group_name_en'];
		$group_name_ar        = $_POST['group_name_ar'];
		$tbl_parent_id        = $_POST['tbl_parent_id'];
		$tbl_parent_group_id  = $_POST['tbl_parent_group_id'];
		$tbl_school_id        = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
		
		$is_exist = $this->model_message->is_exist_parent_group($tbl_parent_group_id,$group_name_en,$tbl_school_id);
		if(count($is_exist)>0)
		{
		  echo "X";
		}else{
			
			if($tbl_school_id<>"")
			{  
				if($tbl_parent_group_id<>"")
				{
					$this->model_message->save_parent_group($tbl_parent_group_id,$group_name_en,$group_name_ar,$tbl_parent_id,$tbl_school_id);
				}
			   echo "Y"; 
			}else{
			  echo "N";	
			}
		}
	}
	
	  /**
	* @desc    Activate Parent Group
	* @param   String 
	* @access  default
	*/
    function activateParentGroup() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_parent_group_id = trim($_POST["tbl_parent_group_id"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_message");		
		$this->model_message->activate_parent_group($tbl_parent_group_id,$tbl_school_id);
	}

	/**
	* @desc    Deactivate Parents Group
	* @param   String school_type_id_enc
	* @access  default
	*/
    function deactivateParentGroup() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_parent_group_id = trim($_POST["tbl_parent_group_id"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_message");		
		$this->model_message->deactivate_parent_group($tbl_parent_group_id,$tbl_school_id);
	}

    /**
	* @desc    Delete Parents Group
	* @param   POST array
	* @access  default
	*/
	function deleteParentGroup() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['tbl_parent_group_id'];    
			 //e.g."school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('tbl_parent_group_id=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_message");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_message->delete_parent_group($str[$i],$tbl_school_id);
		}
	}
	
	//edit functionality
	/**
	* @desc    Edit parent_group
	* @param   none
	* @access  default
	*/
    function edit_parent_group() {
		$data['page'] = "view_parents_group";
		$data['menu'] = "configuration";
        $data['sub_menu'] = "parent_group";
		
		
		$data['mid'] = "3";
		$this->load->model("model_message");
		$this->load->model("model_class_sessions");
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_teacher_id = 0;
		if (array_key_exists('tbl_parent_group_id',$param_array)) {
			$tbl_parent_group_id = $param_array['tbl_parent_group_id'];
		}
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  	
		
		$classesObj = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
		$data['classes_list'] = $classesObj;
		if($tbl_school_id<>"")
		{  
		   $results               = $this->model_message->get_parent_group_info($tbl_parent_group_id,$tbl_school_id);
		   $selected_class_list   = $this->model_message->get_selected_class_in_group($tbl_parent_group_id,$tbl_school_id);
		   for($c=0;$c<count($selected_class_list);$c++)
		   {
			   if($c<>0)
			   {
				  $class_array .= ",";
			   }
			   $class_array .= "'".$selected_class_list[$c]['tbl_class_id']."'";
		   }
		   if($class_array=="")
		   {
			   $class_array = "'UNDEFINED'";
		   }
		   
		   $all_parents_list      = $this->model_message->getParentsBasedOnClass($class_array,$tbl_school_id);
		   
		   $parents_list          = $this->model_message->get_parents_list_in_group($tbl_parent_group_id,$tbl_school_id);
		   for($b=0;$b<count($parents_list);$b++)
		   {
			   $assign_parents_list[$b] = $parents_list[$b]['tbl_parent_id'];
		   }
		   $data['all_class_parent_list']   = $all_parents_list;
		   $data['selected_class_list']     = $selected_class_list;
		   $data['assign_parents_list']     = $assign_parents_list;
	       $data['group_info']              = $results; 
		    
		}
		$this->load->view('admin/view_template', $data);
	}

    
	// Update Parents Forum Group
	function update_parent_group()
	{
		$this->load->model("model_message");
		$group_name_en        = $_POST['group_name_en'];
		$group_name_ar        = $_POST['group_name_ar'];
		$tbl_parent_id        = $_POST['tbl_parent_id'];
		$tbl_parent_group_id  = $_POST['tbl_parent_group_id'];
		$tbl_school_id        = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
		$is_exist = $this->model_message->is_exist_parent_group($tbl_parent_group_id,$group_name_en,$tbl_school_id);
		if(count($is_exist)>0)
		{
		  echo "X";
		}else{
			if($tbl_school_id<>"")
			{  
			  $this->model_message->update_parent_group($tbl_parent_group_id,$group_name_en,$group_name_ar,$tbl_parent_id,$tbl_school_id);
			   echo "Y"; 
			}else{
			  echo "N";	
			}
		}
	}
	
	//END PARENTS FORUM GROUP
	
	
	//LIST MESSAGE FROM PARENTS TO SCHOOL - CONTACT SCHOOL......................
	/**
	* @desc    Show all school message to parents
	* @param   none
	* @access  default
	*/
    function message_from_parents() {
		$data['page'] = "view_message_from_parents";
		$data['menu'] = "messages";
        $data['sub_menu'] = "message_from_parents";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "id";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "id";
					 break;
				}
				default: {
					$sort_name = "id";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_teacher_id',$param_array)) {
			$tbl_teacher_id = $param_array['tbl_teacher_id'];
			$data['tbl_sel_teacher_id'] = $tbl_teacher_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_message");
        $this->load->model("model_class_sessions");		
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  	
		if($tbl_school_id<>"")
		{  
			$rs_all_messages     = $this->model_message->list_parents_messages_to_school($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_teacher_id);
			$total_messages      = $this->model_message->get_total_parents_messages_to_school($q, $is_active, $tbl_school_id,$tbl_teacher_id);
			
		}else{
			$rs_all_messages     = array();
			$total_messages      = array();
		}
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/message/message_from_parents";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_teacher_id) && trim($tbl_teacher_id)!="") {
			$page_url .= "/tbl_teacher_id/".rawurlencode($tbl_teacher_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_messages;
		$config['per_page'] = TBL_SCHOOL_CONTACTS_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_SCHOOL_CONTACTS_PAGING >= $total_messages) {
			$range = $total_messages;
		} else {
			$range = $offset+TBL_SCHOOL_CONTACTS_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_messages messages</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_messages']	     = $rs_all_messages;
		$data['total_messages'] 	      = $total_messages;

		$this->load->view('admin/view_template',$data);
	}
	
	
	
	//MINISTRY MESSAGE SECTION 
	
	//SEND MESSAGE TO TEACHERS......................
    /**
	* @desc    Show all school message to teachers
	* @param   none
	* @access  default
	*/
	
    function ministry_message_to_teachers() {
		$data['page']     = "view_ministry_message_to_teachers";
		$data['menu']     = "messages";
        $data['sub_menu'] = "message_to_teachers";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "id";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "id";
					 break;
				}
				default: {
					$sort_name = "id";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_teacher_id',$param_array)) {
			$tbl_teacher_id = $param_array['tbl_teacher_id'];
			$data['tbl_sel_teacher_id'] = $tbl_teacher_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_notification");
		$this->load->model("model_school");
		$this->load->model("model_student");
		
		$is_active = "";
		
		$rs_all_schools      = $this->model_school->get_all_schools();
		$rs_all_country      = $this->model_student->get_country_list('Y');
		
		/*print_r($rs_all_schools);
		echo "<pre>";
		print_r($rs_all_country);
		*/
		$rs_all_messages     = $this->model_notification->list_ministry_message_to_teachers($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_teacher_id);
		$total_messages      = $this->model_notification->get_total_ministry_message_to_teachers($q, $is_active, $tbl_school_id,$tbl_teacher_id);
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/message/ministry_message_to_teachers";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_teacher_id) && trim($tbl_teacher_id)!="") {
			$page_url .= "/tbl_teacher_id/".rawurlencode($tbl_teacher_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_messages;
		$config['per_page'] = TBL_MESSAGE_TEACHER_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_MESSAGE_TEACHER_PAGING >= $total_messages) {
			$range = $total_messages;
		} else {
			$range = $offset+TBL_MESSAGE_TEACHER_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_messages messages</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_country']	      = $rs_all_country;
		$data['rs_all_schools']	      = $rs_all_schools;
		$data['rs_all_messages']	     = $rs_all_messages;
		$data['total_messages'] 	      = $total_messages;

		$this->load->view('admin/view_ministry_template',$data);
	}
	
	
	function send_ministry_message_to_teacher()
	{
		$message_from      = $_SESSION['aqdar_smartcare']['tbl_admin_id_sess'];
		$message_from_type = "A"; //Admin User
		$this->load->model("model_notification");
		
		$tbl_notification_rpt_id = $_POST['tbl_notification_rpt_id'];
		$message                 = $_POST['message'];
		$tbl_school_id           = $_POST['tbl_school_id'];
		$tbl_country_id          = $_POST['tbl_country_id'];
		$gender_male             = $_POST['gender_male'];
		$gender_female           = $_POST['gender_female'];
		$public_school           = $_POST['public_school'];
		$private_school          = $_POST['private_school'];
		
		if($tbl_school_id<>"")
		{  
		   $this->model_notification->send_ministry_message_to_teacher($tbl_notification_rpt_id,$message,$tbl_school_id,$tbl_country_id,$gender_male,$gender_female,$public_school,$private_school);
		   echo "Y"; 
		}else{
		  echo "N";	
		}
	}
	
	  /**
	* @desc    Activate Teacher Message
	* @param   String 
	* @access  default
	*/
    function activateMinistryTeacherMessage() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_message_group_id = trim($_POST["tbl_message_group_id"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_message");		
		$this->model_message->activate_teacher_message($tbl_message_group_id,$tbl_school_id);
	}

	/**
	* @desc    Deactivate Teacher Message
	* @param   String school_type_id_enc
	* @access  default
	*/
    function deactivateMinistryTeacherMessage() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_message_group_id = trim($_POST["tbl_message_group_id"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_message");		
		$this->model_message->deactivate_teacher_message($tbl_message_group_id,$tbl_school_id);
	}

     /**
	* @desc    Delete Teacher Message
	* @param   POST array
	* @access  default
	*/
	function deleteMinistryTeacherMessage() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['tbl_message_group_id']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('tbl_message_group_id=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_message");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_message->delete_teacher_message($str[$i],$tbl_school_id);
		}
	}
	
	//edit functionality
	/**
	* @desc    Edit teacher
	* @param   none
	* @access  default
	*/
    function edit_ministry_message_to_teacher() {
		$data['page'] = "view_ministry_message_to_teachers";
		$data['menu'] = "messages";
        $data['sub_menu'] = "message_to_teachers";
		$data['mid'] = "3";
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		$this->load->model("model_teachers");
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_teacher_id = 0;
		if (array_key_exists('tbl_message_group_id',$param_array)) {
			$tbl_message_group_id = $param_array['tbl_message_group_id'];
		}
		
		$this->load->model("model_message");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  	
		if($tbl_school_id<>"")
		{  
		   $results = $this->model_message->get_school_message_to_teacher($tbl_message_group_id,$tbl_school_id);
	       $data['school_message'] = $results;  
		}
		
		$this->load->view('admin/view_ministry_template', $data);
	}

    
	
	function update_ministry_message_to_teacher()
	{
		$message              = $_POST['message'];
		$this->load->model("model_message");
		$tbl_message_group_id = $_POST['tbl_message_group_id'];
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
		if($tbl_school_id<>"")
		{  
		  $this->model_message->update_message_to_teacher($tbl_message_group_id,$message,$tbl_school_id);
		   echo "Y"; 
		}else{
		  echo "N";	
		}
	}
	
	//END SEND MESSAGE TO TEACHERS......................
	
	//END MINISTRY MESSAGE SECTION
	
	// TIMELINE MESSAGE - LISTING OF USER MESSAGES
	
	 function website_feedback() {
		$data['page']     = "view_website_feedback";
		$data['menu']     = "messages";
        $data['sub_menu'] = "website_feedback";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "id";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "id";
					 break;
				}
				default: {
					$sort_name = "id";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
	
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_message");
        $this->load->model("model_class_sessions");		
		
		$is_active = "";
		$rs_all_messages     = $this->model_message->list_website_feedback($sort_name, $sort_by, $offset, $q, $is_active);
		$total_messages      = $this->model_message->get_total_website_feedback($q, $is_active);
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/message/website_feedback";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_messages;
		$config['per_page'] = TBL_SCHOOL_CONTACTS_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_SCHOOL_CONTACTS_PAGING >= $total_messages) {
			$range = $total_messages;
		} else {
			$range = $offset+TBL_SCHOOL_CONTACTS_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_messages messages</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_messages']	     = $rs_all_messages;
		$data['total_messages'] 	      = $total_messages;

		$this->load->view('admin/view_timeline_template',$data);
	}
	
	
	function feedback_details()
	{
		$data['page']     = "view_website_feedback";
		$data['menu']     = "messages";
        $data['sub_menu'] = "website_feedback";
		$data['mid'] = "4";//Details

		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_user_id = 0;
					
		if (array_key_exists('feedback_id_enc',$param_array)) {
			$tbl_feedback_id = $param_array['feedback_id_enc'];
			$data['tbl_feedback_id'] = $tbl_feedback_id;
		}	 
		$this->load->model("model_message");		
		//School details
		$feedback_obj 				= $this->model_message->get_feedbackObj($tbl_feedback_id);
		$data['feedback_obj'] 		= $feedback_obj;
		
		$update_feedback 			= $this->model_message->update_feedbackStatus($tbl_feedback_id);
		
		$this->load->view('admin/view_timeline_template', $data);	
		
	}
	
	
	
   // Start Manage Students Group	
	
		
    function students_group() {
		$data['page'] = "view_students_group";
		$data['menu'] = "configuration";
        $data['sub_menu'] = "student_group";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "id";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "id";
					 break;
				}
				default: {
					$sort_name = "id";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
	
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_message");
		$this->load->model("model_class_sessions");
		$this->load->model("model_teachers");
		$this->load->model("model_parents");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  	
		if($tbl_school_id<>"")
		{  
			// add purpose
			$classesObj = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
			$data['classes_list'] = $classesObj;
			
			//list purpose
			$rs_all_groups     = $this->model_message->list_students_group($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_student_id);
			$total_groups      = $this->model_message->get_total_students_group($q, $is_active, $tbl_school_id,$tbl_student_id);
			
			for($m=0;$m<count($rs_all_groups); $m++)
			{
				$students_list  = $this->model_message->get_students_list_in_group($rs_all_groups[$m]['tbl_student_group_id'],$tbl_school_id);
				$rs_all_groups[$m]['cntStudents']      = count($students_list);
				$rs_all_groups[$m]['students_list']     = $students_list;
			}
			
		}else{
			$data['classes_list'] = array();
			$rs_all_groups     = array();
			$total_groups      = array();
		}
	
	
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/message/students_group";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_parent_id) && trim($tbl_parent_id)!="") {
			$page_url .= "/tbl_parent_id/".rawurlencode($tbl_parent_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_groups;
		$config['per_page'] = TBL_STUDENT_GROUP_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_STUDENT_GROUP_PAGING >= $total_groups) {
			$range = $total_groups;
		} else {
			$range = $offset+TBL_STUDENT_GROUP_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_groups Students Groups</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_groups']	       = $rs_all_groups;
		$data['total_groups'] 	        = $total_groups;
		$this->load->view('admin/view_template',$data);
	}
	
	
	function save_student_group()
	{
		$this->load->model("model_message");
		$group_name_en        = $_POST['group_name_en'];
		$group_name_ar        = $_POST['group_name_ar'];
		$group_for            = $_POST['group_for'];
		$tbl_student_id       = $_POST['tbl_student_id'];
		$tbl_student_group_id = $_POST['tbl_student_group_id'];
		$tbl_school_id        = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
		
		$is_exist = $this->model_message->is_exist_student_group($tbl_student_group_id, $group_name_en, $tbl_school_id);
		if(count($is_exist)>0)
		{
		  echo "X";
		}else{
			
			if($tbl_school_id<>"")
			{  
				if($tbl_student_group_id<>"")
				{
					$this->model_message->save_student_group($tbl_student_group_id, $group_name_en, $group_name_ar, $tbl_student_id, $tbl_school_id, $group_for);
				}
			   echo "Y"; 
			}else{
			  echo "N";	
			}
		}
	}
	
	  /**
	* @desc    Activate Parent Group
	* @param   String 
	* @access  default
	*/
    function activateStudentGroup() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_student_group_id = trim($_POST["tbl_student_group_id"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_message");		
		$this->model_message->activate_student_group($tbl_student_group_id,$tbl_school_id);
	}

	/**
	* @desc    Deactivate Parents Group
	* @param   String school_type_id_enc
	* @access  default
	*/
    function deactivateStudentGroup() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_student_group_id = trim($_POST["tbl_student_group_id"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_message");		
		$this->model_message->deactivate_student_group($tbl_student_group_id,$tbl_school_id);
	}

    /**
	* @desc    Delete Parents Group
	* @param   POST array
	* @access  default
	*/
	function deleteStudentGroup() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['tbl_student_group_id'];    
			 //e.g."school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('tbl_student_group_id=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_message");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_message->delete_student_group($str[$i],$tbl_school_id);
		}
	}
	
	//edit functionality
	/**
	* @desc    Edit student_group
	* @param   none
	* @access  default
	*/
    function edit_student_group() {
		$data['page'] = "view_students_group";
		$data['menu'] = "configuration";
        $data['sub_menu'] = "student_group";
		
		
		$data['mid'] = "3";
		$this->load->model("model_message");
		$this->load->model("model_class_sessions");
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_teacher_id = 0;
		if (array_key_exists('tbl_student_group_id',$param_array)) {
			$tbl_student_group_id = $param_array['tbl_student_group_id'];
		}
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  	
		
		$classesObj = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
		$data['classes_list'] = $classesObj;
		if($tbl_school_id<>"")
		{  
		   $results               = $this->model_message->get_student_group_info($tbl_student_group_id,$tbl_school_id);
		   $selected_class_list   = $this->model_message->get_selected_student_class_in_group($tbl_student_group_id,$tbl_school_id);
		   for($c=0;$c<count($selected_class_list);$c++)
		   {
			   if($c<>0)
			   {
				  $class_array .= ",";
			   }
			   $class_array .= "'".$selected_class_list[$c]['tbl_class_id']."'";
		   }
		   if($class_array=="")
		   {
			   $class_array = "'UNDEFINED'";
		   }
		   
		   $all_students_list      = $this->model_message->getStudentsBasedOnClass($class_array,$tbl_school_id);
		   
		   $students_list          = $this->model_message->get_students_list_in_group($tbl_student_group_id,$tbl_school_id);
		   for($b=0;$b<count($students_list);$b++)
		   {
			   $assign_students_list[$b]     = $students_list[$b]['tbl_student_id'];
		   }
		   $data['all_class_student_list']   = $all_students_list;
		   $data['selected_class_list']      = $selected_class_list;
		   $data['assign_students_list']     = $assign_students_list;
	       $data['group_info']               = $results; 
		    
		}
		$this->load->view('admin/view_template', $data);
	}

    
	// Update Student Group
	function update_student_group()
	{
		$this->load->model("model_message");
		$group_name_en        	= $_POST['group_name_en'];
		$group_name_ar        	= $_POST['group_name_ar'];
		$group_for              = $_POST['group_for'];
		$tbl_student_id        	= $_POST['tbl_student_id'];
		$tbl_student_group_id  	= $_POST['tbl_student_group_id'];
		$tbl_school_id        	= $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
		$is_exist = $this->model_message->is_exist_student_group($tbl_student_group_id,$group_name_en,$tbl_school_id);
		if(count($is_exist)>0)
		{
		  echo "X";
		}else{
			if($tbl_school_id<>"")
			{  
			  $this->model_message->update_student_group($tbl_student_group_id, $group_name_en, $group_name_ar, $tbl_student_id, $tbl_school_id, $group_for );
			   echo "Y"; 
			}else{
			  echo "N";	
			}
		}
	}
	
	
	
	
	
}
?>