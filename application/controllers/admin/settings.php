<?php

/**
 * @desc   	  	Home Controller
 *
 * @category   	Controller
 * @author     	Shanavas.PK
 * @version    	0.1
 */
class Settings extends CI_Controller {


	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
    function index() {
		
	}


	/**
	* @desc    Edit School Contact
	* @param   none
	* @access  default
	*/
    function school_contact() {
		$data['page'] = "view_school_contact";
		$data['menu'] = "configuration";
        $data['sub_menu'] = "school_contact";
		
		$data['mid'] = "3";
		$this->load->model("model_config");
		//GET Params
	
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$rs_school_contact    = $this->model_config->get_school_contact($tbl_school_id);
		$data['rs_school_contact'] = $rs_school_contact;
		$this->load->view('admin/view_template', $data);
	}
	
		/**
	* @desc    Save changes
	* @param   POST array
	* @access  default
	*/
    function save_school_contact() {
		if ($_POST) {
			$latitude            	= $_POST['latitude'];
			$longitude     	     	= $_POST['longitude'];
			$email               	= $_POST['email'];
			$phone     	         	= $_POST['phone'];
			$mobile                	= $_POST['mobile'];
			$fax     	           	= $_POST['fax'];
			$website            	= $_POST['website'];
			$payment_link     	    = $_POST['payment_link'];
			$facebook               = $_POST['facebook'];
			$google     	        = $_POST['google'];
			$youtube                = $_POST['youtube'];
			$twitter     	        = $_POST['twitter'];
			$linkedin     	        = $_POST['linkedin'];
		}
		$tbl_school_contacts_id = md5(uniqid(rand()));
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_config");			
		$this->model_config->save_school_contact($tbl_school_contacts_id, $latitude, $longitude, $email, $phone, $mobile, $fax, $tbl_school_id, $website, $payment_link, $facebook, $google, 
		                                         $youtube, $twitter, $linkedin);
		echo "Y";
	}


	
	
}
?>