<?php
/**
 * @desc   	  	Forum Controller
 * @category   	Controller
 * @author     	Shanavas PK
 * @version    	0.1
 */
class Forum extends CI_Controller {
	/**
	* @desc    Default function for the Controller
	* @param   none
	* @access  default
	*/
    function index() {
	}

    /**
	* @desc    Show all topics
	* @param   none
	* @access  default
	*/
    function all_topics() {
		$data['page'] = "view_all_topics";
		$data['menu'] = "forum";
        $data['sub_menu'] = "forum";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "id";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "forum_topic";
					 break;
				}
				default: {
					$sort_name = "forum_topic";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_forum");
		$this->load->model("model_parents");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			$rs_parents_groups  = $this->model_parents->get_list_parents_group($tbl_school_id,'Y');
			$data['rs_parents_groups'] = $rs_parents_groups;
			$rs_all_topics      = $this->model_forum->get_all_topics($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id);
			$total_topics       = $this->model_forum->get_total_topics($q, $is_active, $tbl_school_id);
			for($k=0;$k<count($rs_all_topics);$k++)
			{
				$group_parents = "";
				$tbl_parent_group_id = $rs_all_topics[$k]['tbl_parent_group_id'];
				
				$group_list = $this->model_forum->get_parent_group_list_of_topic($tbl_parent_group_id);
				for($d=0;$d<count($group_list);$d++)
				{
				   $group_parents .=	$group_list[$d]['group_name_en']."&nbsp;[::]&nbsp;".$group_list[$d]['group_name_ar']."<br/>";
				}
				$rs_all_topics[$k]['group_parents'] = $group_parents;
			}
			
			
		}else{
			$rs_all_topics     = array();
			$total_topics      = array();
		}
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/forum/all_topics";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_school_type_id) && trim($tbl_student_id)!="") {
			$page_url .= "/tbl_student_id/".rawurlencode($tbl_student_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_topics;
		$config['per_page'] = TBL_FORUM_TOPICS_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_FORUM_TOPICS_PAGING >= $total_topics) {
			$range = $total_topics;
		} else {
			$range = $offset+TBL_FORUM_TOPICS_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_topics topics</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_topics']	     = $rs_all_topics;
		$data['total_topics'] 	      = $total_topics;

		$this->load->view('admin/view_template',$data);
	}
	
	function is_exist_topic() {
		if ($_POST) {
			$tbl_parent_forum_id          = $_POST['tbl_parent_forum_id'];		
			$forum_topic                  = $_POST['forum_topic'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_forum");		
		$results = $this->model_forum->is_exist_topic($tbl_parent_forum_id, $forum_topic, $tbl_school_id);
		if(count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
    /**
	* @desc    Add Forum Topic
	* @param   POST array
	* @access  default
	*/
    function add_forum_topic() {
		if ($_POST) {
			$tbl_parent_forum_id          = $_POST['tbl_parent_forum_id'];		
			$forum_topic                  = $_POST['forum_topic'];
			$str                          = $_POST['tbl_parent_group_id'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_forum");	
		$resData = $this->model_forum->add_forum_topic($tbl_parent_forum_id, $forum_topic, $str, $tbl_school_id);
		if ($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}

    /**
	* @desc    Activate topic
	* @param   String 
	* @access  default
	*/
    function activateTopic() {
		if(isset($_POST) && count($_POST) != 0) {
			$parent_forum_id_enc = trim($_POST["parent_forum_id_enc"]);
			$is_ajax             = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_forum");		
		$this->model_forum->activate_topic($parent_forum_id_enc,$tbl_school_id);
	}


	/**
	* @desc    Deactivate topic
	* @param   String parent_forum_id_enc
	* @access  default
	*/
    function deactivateTopic() {
		if(isset($_POST) && count($_POST) != 0) {
			$parent_forum_id_enc = trim($_POST["parent_forum_id_enc"]);
			$is_ajax             = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_forum");		
		$this->model_forum->deactivate_topic($parent_forum_id_enc,$tbl_school_id);
	}

     /**
	* @desc    Delete topic
	* @param   POST array
	* @access  default
	*/
     
	function deleteTopic() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['parent_forum_id_enc']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('parent_forum_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_forum");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_forum->delete_topic($str[$i],$tbl_school_id);
		}
	}
	
	/**
	* @desc    Save changes
	* @param   POST array
	* @access  default
	*/
    function update_forum_topic() {
		if ($_POST) {
			$tbl_parent_forum_id          = $_POST['tbl_parent_forum_id'];		
			$forum_topic                  = $_POST['forum_topic'];
			$str                          = $_POST['tbl_parent_group_id'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_forum");	
		$resData = $this->model_forum->save_topic_changes($tbl_parent_forum_id, $forum_topic, $str, $tbl_school_id);
		
		if ($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}


	/**
	* @desc    Edit forum topic
	* @param   none
	* @access  default
	*/
    function edit_forum_topic() {
	    $data['page'] = "view_all_topics";
		$data['menu'] = "forum";
        $data['sub_menu'] = "forum";
		$data['mid'] = "3";
		$this->load->model("model_forum");
		$this->load->model("model_parents");
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		if (array_key_exists('parent_forum_id_enc',$param_array)) {
			$parent_forum_id_enc = $param_array['parent_forum_id_enc'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess']; 
		
		$rs_parents_groups  = $this->model_parents->get_list_parents_group($tbl_school_id,'Y');
		$data['rs_parents_groups'] = $rs_parents_groups;
			
		//Topic details
		$topic_obj = $this->model_forum->get_topic_obj($parent_forum_id_enc,$tbl_school_id);
		$data['topic_obj'] = $topic_obj;
		$this->load->view('admin/view_template', $data);
	}
	
	
	// FORUM COMMENTS
	/**
	* @desc    Show all topics
	* @param   none
	* @access  default
	*/
    function all_topic_comments() {
		$data['page'] = "view_all_comments";
		$data['menu'] = "forum";
        $data['sub_menu'] = "forum";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "id";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "forum_comment";
					 break;
				}
				default: {
					$sort_name = "forum_comment";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
	
		$tbl_parent_forum_id = "";
		if (array_key_exists('tbl_parent_forum_id',$param_array)) {
			$tbl_parent_forum_id = $param_array['tbl_parent_forum_id'];
			$data['tbl_sel_parent_forum_id'] = $tbl_parent_forum_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_forum");
		$this->load->model("model_parents");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
		    $rs_topic_obj         = $this->model_forum->get_topic_obj($tbl_parent_forum_id,$tbl_school_id);
			$data['rs_topic_obj']= $rs_topic_obj;
			$rs_all_comments      = $this->model_forum->get_all_comments($sort_name, $sort_by, $offset, $q, $is_active, $tbl_parent_forum_id, $tbl_school_id);
			$total_comments       = $this->model_forum->get_total_comments($q, $is_active, $tbl_parent_forum_id, $tbl_school_id);
		
			
		}else{
			$rs_all_comments     = array();
			$total_comments      = array();
		}
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/forum/all_topic_comments";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_parent_forum_id) && trim($tbl_parent_forum_id)!="") {
			$page_url .= "/tbl_parent_forum_id/".rawurlencode($tbl_parent_forum_id);
		}
		
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_comments;
		$config['per_page'] = TBL_FORUM_COMMENTS_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_FORUM_COMMENTS_PAGING >= $total_comments) {
			$range = $total_comments;
		} else {
			$range = $offset+TBL_FORUM_COMMENTS_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_comments comments</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_comments']	     = $rs_all_comments;
		$data['total_comments'] 	      = $total_comments;

		$this->load->view('admin/view_template',$data);
	}
	
	

    /**
	* @desc    Activate comment
	* @param   String 
	* @access  default
	*/
    function activateComment() {
		if(isset($_POST) && count($_POST) != 0) {
			$parent_comment_id_enc = trim($_POST["parent_comment_id_enc"]);
			$is_ajax             = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_forum");		
		$this->model_forum->activate_comment($parent_comment_id_enc,$tbl_school_id);
	}


	/**
	* @desc    Deactivate comment
	* @param   String parent_forum_id_enc
	* @access  default
	*/
    function deactivateComment() {
		if(isset($_POST) && count($_POST) != 0) {
			$parent_comment_id_enc = trim($_POST["parent_comment_id_enc"]);
			$is_ajax             = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_forum");		
		$this->model_forum->deactivate_comment($parent_comment_id_enc,$tbl_school_id);
	}

     /**
	* @desc    Delete comment
	* @param   POST array
	* @access  default
	*/
     
	function deleteComment() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['parent_comment_id_enc']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('parent_comment_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_forum");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_forum->delete_comment($str[$i],$tbl_school_id);
		}
	}
	
	
	//END FORUM COMMENTS
	
	// STUDENTS FORUM
	
	 /**
	* @desc    Show all topics
	* @param   none
	* @access  default
	*/
    function all_student_forum_topics() {
		$data['page'] = "view_all_student_forum_topics";
		$data['menu'] = "forum_student";
        $data['sub_menu'] = "forum_student";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "id";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "forum_topic";
					 break;
				}
				default: {
					$sort_name = "forum_topic";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_forum");
		$this->load->model("model_student");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			$rs_students_groups  = $this->model_student->get_list_students_group($tbl_school_id,'Y');
			
			$data['rs_students_groups'] = $rs_students_groups;
			$rs_all_topics      = $this->model_forum->get_all_student_topics($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id);
			$total_topics       = $this->model_forum->get_total_student_topics($q, $is_active, $tbl_school_id);
		
			for($k=0;$k<count($rs_all_topics);$k++)
			{
				$group_students = "";
				$tbl_student_group_id = $rs_all_topics[$k]['tbl_student_group_id'];
				
				$group_list = $this->model_forum->get_student_group_list_of_topic($tbl_student_group_id);
				for($d=0;$d<count($group_list);$d++)
				{
				   $group_students .=	$group_list[$d]['group_name_en']."&nbsp;[::]&nbsp;".$group_list[$d]['group_name_ar']."<br/>";
				}
				$rs_all_topics[$k]['group_students'] = $group_students;
			}
			
			
		}else{
			$rs_all_topics     = array();
			$total_topics      = array();
		}
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/forum/all_student_forum_topics";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_school_type_id) && trim($tbl_student_id)!="") {
			$page_url .= "/tbl_student_id/".rawurlencode($tbl_student_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_topics;
		$config['per_page'] = TBL_FORUM_TOPICS_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_FORUM_TOPICS_PAGING >= $total_topics) {
			$range = $total_topics;
		} else {
			$range = $offset+TBL_FORUM_TOPICS_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_topics topics</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_topics']	     = $rs_all_topics;
		$data['total_topics'] 	      = $total_topics;

		$this->load->view('admin/view_template',$data);
	}
	
	function is_exist_student_topic() {
		if ($_POST) {
			$tbl_student_forum_id          = $_POST['tbl_student_forum_id'];		
			$forum_topic                  = $_POST['forum_topic'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_forum");		
		$results = $this->model_forum->is_exist_student_topic($tbl_student_forum_id, $forum_topic, $tbl_school_id);
		if(count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
    /**
	* @desc    Add Forum Topic
	* @param   POST array
	* @access  default
	*/
    function add_forum_student_topic() {
		if ($_POST) {
			$tbl_student_forum_id          = $_POST['tbl_student_forum_id'];		
			$forum_topic                   = $_POST['forum_topic'];
			$str                           = $_POST['tbl_student_group_id'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_forum");	
		$resData = $this->model_forum->add_forum_student_topic($tbl_student_forum_id, $forum_topic, $str, $tbl_school_id);
		if ($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}

    /**
	* @desc    Activate topic
	* @param   String 
	* @access  default
	*/
    function activateStudentTopic() {
		if(isset($_POST) && count($_POST) != 0) {
			$student_forum_id_enc = trim($_POST["student_forum_id_enc"]);
			$is_ajax             = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_forum");		
		$this->model_forum->activate_student_topic($student_forum_id_enc,$tbl_school_id);
	}


	/**
	* @desc    Deactivate topic
	* @param   String parent_forum_id_enc
	* @access  default
	*/
    function deactivateStudentTopic() {
		if(isset($_POST) && count($_POST) != 0) {
			$student_forum_id_enc = trim($_POST["student_forum_id_enc"]);
			$is_ajax              = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_forum");		
		$this->model_forum->deactivate_student_topic($student_forum_id_enc,$tbl_school_id);
	}

     /**
	* @desc    Delete topic
	* @param   POST array
	* @access  default
	*/
     
	function deleteStudentTopic() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['student_forum_id_enc']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('student_forum_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_forum");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_forum->delete_student_topic($str[$i],$tbl_school_id);
		}
	}
	
	/**
	* @desc    Save changes
	* @param   POST array
	* @access  default
	*/
    function update_forum_student_topic() {
		if ($_POST) {
			$tbl_student_forum_id         = $_POST['tbl_student_forum_id'];		
			$forum_topic                  = $_POST['forum_topic'];
			$str                          = $_POST['tbl_student_group_id'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_forum");	
		$resData = $this->model_forum->save_student_topic_changes($tbl_student_forum_id, $forum_topic, $str, $tbl_school_id);
		
		if ($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}


	/**
	* @desc    Edit forum topic
	* @param   none
	* @access  default
	*/
    function edit_forum_student_topic() {
	    $data['page'] = "view_all_student_forum_topics";
		$data['menu'] = "forum_student";
        $data['sub_menu'] = "forum_student";
		$data['mid'] = "3";
		$this->load->model("model_forum");
		$this->load->model("model_student");
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		if (array_key_exists('student_forum_id_enc',$param_array)) {
			$student_forum_id_enc = $param_array['student_forum_id_enc'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess']; 
		
		$rs_students_groups  = $this->model_student->get_list_students_group($tbl_school_id,'Y');
		$data['rs_students_groups'] = $rs_students_groups;
		//Topic details
		$topic_obj = $this->model_forum->get_student_topic_obj($student_forum_id_enc,$tbl_school_id);
		$data['topic_obj'] = $topic_obj;
		$this->load->view('admin/view_template', $data);
	}
	
	
	// FORUM COMMENTS
	/**
	* @desc    Show all topics
	* @param   none
	* @access  default
	*/
    function all_topic_student_comments() {
		$data['page'] = "view_all_student_forum_comments";
		$data['menu'] = "forum_student";
        $data['sub_menu'] = "forum_student";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "id";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "forum_comment";
					 break;
				}
				default: {
					$sort_name = "forum_comment";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
	
		$tbl_student_forum_id = "";
		if (array_key_exists('tbl_student_forum_id',$param_array)) {
			$tbl_student_forum_id = $param_array['tbl_student_forum_id'];
			$data['tbl_sel_student_forum_id'] = $tbl_student_forum_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_forum");
		$this->load->model("model_student");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
		    $rs_topic_obj         = $this->model_forum->get_student_topic_obj($tbl_student_forum_id,$tbl_school_id);
			$data['rs_topic_obj']= $rs_topic_obj;
			$rs_all_comments      = $this->model_forum->get_all_student_comments($sort_name, $sort_by, $offset, $q, $is_active, $tbl_student_forum_id, $tbl_school_id);
			$total_comments       = $this->model_forum->get_total_student_comments($q, $is_active, $tbl_student_forum_id, $tbl_school_id);
		
			
		}else{
			$rs_all_comments     = array();
			$total_comments      = array();
		}
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/forum/all_topic_student_comments";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_student_forum_id) && trim($tbl_student_forum_id)!="") {
			$page_url .= "/tbl_student_forum_id/".rawurlencode($tbl_student_forum_id);
		}
		
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_comments;
		$config['per_page'] = TBL_FORUM_COMMENTS_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_FORUM_COMMENTS_PAGING >= $total_comments) {
			$range = $total_comments;
		} else {
			$range = $offset+TBL_FORUM_COMMENTS_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_comments comments</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_comments']	     = $rs_all_comments;
		$data['total_comments'] 	      = $total_comments;

		$this->load->view('admin/view_template',$data);
	}

    /**
	* @desc    Activate comment
	* @param   String 
	* @access  default
	*/
    function activateStudentComment() {
		if(isset($_POST) && count($_POST) != 0) {
			$student_comment_id_enc = trim($_POST["student_comment_id_enc"]);
			$is_ajax             = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_forum");		
		$this->model_forum->activate_student_comment($student_comment_id_enc,$tbl_school_id);
	}

	/**
	* @desc    Deactivate comment
	* @param   String parent_forum_id_enc
	* @access  default
	*/
    function deactivateStudentComment() {
		if(isset($_POST) && count($_POST) != 0) {
			$student_comment_id_enc = trim($_POST["student_comment_id_enc"]);
			$is_ajax             = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_forum");		
		$this->model_forum->deactivate_student_comment($student_comment_id_enc,$tbl_school_id);
	}

     /**
	* @desc    Delete comment
	* @param   POST array
	* @access  default
	*/
     
	function deleteStudentComment() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['student_comment_id_enc']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('student_comment_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_forum");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_forum->delete_student_comment($str[$i],$tbl_school_id);
		}
	}
	//END STUDENTS FORUM
	
	
	
	


	
}
?>