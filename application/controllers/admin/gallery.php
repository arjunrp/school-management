<?php

/**
 * @desc   	  	Gallery Controller
 * @category   	Controller
 * @author     	Shanavas PK
 * @version    	0.1
 */
class Gallery extends CI_Controller {
	/**
	* @desc    Default function for the Controller
	* @param   none
	* @access  default
	*/
    function index() {
		
	}

	//LIST SCHOOL GALLERY FROM SCHOOL......................
    /**
	* @desc    Show all gallery from school
	* @param   none
	* @access  default
	*/
	
    function all_galleries() {
		$data['page'] = "view_school_gallery";
		$data['menu'] = "gallery";
        $data['sub_menu'] = "school_gallery";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "id";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "id";
					 break;
				}
				default: {
					$sort_name = "id";
				}					
			}
		}
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		if (array_key_exists('tbl_gallery_category_id',$param_array)) {
			$tbl_gallery_category_id = $param_array['tbl_gallery_category_id'];
			$data['tbl_sel_gallery_category_id'] = $tbl_gallery_category_id;
		}

		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_gallery");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess']; 
		$data['tbl_school_id'] = $tbl_school_id;	 
		
		$categoryObj = $this->model_gallery->get_gallery_categories($tbl_school_id);
	    $data['category_list'] = $categoryObj;	
		
		if($tbl_school_id<>"")
		{  
			$rs_all_gallery     = $this->model_gallery->get_school_gallery($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_gallery_category_id);
			$total_gallery      = $this->model_gallery->get_total_school_gallery($q, $is_active, $tbl_school_id, $tbl_gallery_category_id);
		
		}else{
			$rs_all_gallery     = array();
			$total_gallery      = array();
		}
		
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/gallery/all_galleries";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_teacher_id) && trim($tbl_teacher_id)!="") {
			$page_url .= "/tbl_teacher_id/".rawurlencode($tbl_teacher_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url']    = $page_url;
		$config['total_rows']  = $total_gallery;
		$config['per_page']    = TBL_IMAGE_GALLERY_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_IMAGE_GALLERY_PAGING >= $total_gallery) {
			$range = $total_gallery;
		} else {
			$range = $offset+TBL_IMAGE_GALLERY_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_gallery records</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_gallery']	     = $rs_all_gallery;
		$data['total_gallery'] 	      = $total_gallery;

		$this->load->view('admin/view_template',$data);
	}
	
	
	function add_school_gallery()
	{
		
		$tbl_image_gallery_id          = $_POST['tbl_image_gallery_id'];
		$tbl_gallery_category_id       = $_POST['tbl_gallery_category_id'];
		$is_active                     = $_POST['is_active'];
		
		$this->load->model("model_gallery");
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
		if($tbl_school_id<>"")
		{  
		   $this->model_gallery->add_school_gallery($tbl_image_gallery_id,$tbl_gallery_category_id,$is_active,$tbl_school_id);
		   echo "Y"; 
		}else{
		  echo "N";	
		}
	}
	
	// Exist images
	function exist_gallery() {
		if ($_POST) {
			$tbl_image_gallery_id   = $_POST['tbl_image_gallery_id'];		
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_gallery");		
		$results = $this->model_gallery->exist_gallery($tbl_image_gallery_id, $tbl_school_id);
		if (count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
	
	
    //edit functionality
	/**
	* @desc    Edit School Gallery
	* @param   none
	* @access  default
	*/
    function edit_gallery() {
		$data['page'] = "view_school_gallery";
		$data['menu'] = "gallery";
        $data['sub_menu'] = "school_gallery";
		
		$data['mid'] = "3";
		$this->load->model("model_gallery");

		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_teacher_id = 0;
		if (array_key_exists('tbl_image_gallery_id',$param_array)) {
			$tbl_image_gallery_id = $param_array['tbl_image_gallery_id'];
		}
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
		$categoryObj = $this->model_gallery->get_gallery_categories($tbl_school_id);
	    $data['category_list'] = $categoryObj;	
		  	
		if($tbl_school_id<>"")
		{  
		   $results = $this->model_gallery->get_gallery_info($tbl_image_gallery_id,$tbl_school_id);
	       $data['gallery_data'] = $results;  
		}
		$this->load->view('admin/view_template', $data);
	}
	
	  /**
	* @desc    Activate Gallery
	* @param   String 
	* @access  default
	*/
    function activateGallery() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_image_gallery_id = trim($_POST["tbl_image_gallery_id"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_gallery");		
		$this->model_gallery->activate_gallery($tbl_image_gallery_id,$tbl_school_id);
	}

	/**
	* @desc    Deactivate Gallery
	* @param   String school_type_id_enc
	* @access  default
	*/
    function deactivateGallery() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_image_gallery_id = trim($_POST["tbl_image_gallery_id"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_gallery");		
		$this->model_gallery->deactivate_gallery($tbl_image_gallery_id,$tbl_school_id);
	}

     /**
	* @desc    Delete School Record
	* @param   POST array
	* @access  default
	*/
	function deleteGallery() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['tbl_image_gallery_id']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('tbl_image_gallery_id=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_gallery");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_gallery->delete_gallery($str[$i],$tbl_school_id);
		}
	}
	

	//END SCHOOL GALLERIES......................
	
	
	
	//LIST SCHOOL GALLERIES CATEGORY......................
    /**
	* @desc    Show all gallery categories 
	* @param   none
	* @access  default
	*/
    function gallery_categories_list() {
		$data['page'] = "view_school_gallery_category";
		$data['menu'] = "gallery";
        $data['sub_menu'] = "school_gallery";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "id";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "id";
					 break;
				}
				default: {
					$sort_name = "id";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_gallery_category_id',$param_array)) {
			$tbl_gallery_category_id = $param_array['tbl_gallery_category_id'];
			$data['tbl_sel_gallery_category_id'] = $tbl_gallery_category_id;
		}
		
		
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_gallery");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		
	
		if($tbl_school_id<>"")
		{  
			$rs_all_categories     = $this->model_gallery->get_gallery_categories_list($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_gallery_category_id);
			$total_categories      = $this->model_gallery->get_total_gallery_categories_list($q, $is_active, $tbl_school_id, $tbl_gallery_category_id);
		
		}else{
			$rs_all_categories     = array();
			$total_categories      = array();
		}
	
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/gallery/gallery_categories_list";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_categories;
		$config['per_page'] = TBL_GALLERY_CATEGORY_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_GALLERY_CATEGORY_PAGING >= $total_categories) {
			$range = $total_categories;
		} else {
			$range = $offset+TBL_GALLERY_CATEGORY_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_categories categories</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_categories']	     = $rs_all_categories;
		$data['total_categories'] 	      = $total_categories;

		$this->load->view('admin/view_template',$data);
	}
	
	
	function is_exist_gallery_category() {
		if ($_POST) {
			$tbl_gallery_category_id       = $_POST['gallery_category_id_enc'];		
			$category_name_en              = $_POST['category_name_en'];
			$category_name_ar              = $_POST['category_name_ar'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_gallery");		
		$results = $this->model_gallery->is_exist_gallery_category($tbl_gallery_category_id, $category_name_en, $category_name_ar, $tbl_school_id);
		if (count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
	
	function add_gallery_category()
	{
		$added_by                      = $_SESSION['aqdar_smartcare']['tbl_admin_id_sess'];
		$tbl_gallery_category_id       = $_POST['gallery_category_id_enc'];		
		$category_name_en              = $_POST['category_name_en'];
		$category_name_ar              = $_POST['category_name_ar'];
		$is_active                     = $_POST['is_active'];
		
		$this->load->model("model_gallery");
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
		if($tbl_school_id<>"")
		{  
		   $this->model_gallery->add_gallery_category($tbl_gallery_category_id,$category_name_en,$category_name_ar,$tbl_school_id);
		   echo "Y"; 
		}else{
		  echo "N";	
		}
	}
	
	
    //edit functionality
	/**
	* @desc    Edit School Records
	* @param   none
	* @access  default
	*/
    function edit_gallery_category() {
		$data['page'] = "view_school_gallery_category";
		$data['menu'] = "gallery";
        $data['sub_menu'] = "school_gallery";
		
		$data['mid'] = "3";
		$this->load->model("model_gallery");

		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		if (array_key_exists('tbl_gallery_category_id',$param_array)) {
			$tbl_gallery_category_id = $param_array['tbl_gallery_category_id'];
		}
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  	
		if($tbl_school_id<>"")
		{  
		   $results = $this->model_gallery->get_school_gallery_category($tbl_gallery_category_id,$tbl_school_id);
	       $data['gallery_categories'] = $results;  
		}
		$this->load->view('admin/view_template', $data);
	}

    
	
	function update_school_gallery_category()
	{
		$added_by                      = $_SESSION['aqdar_smartcare']['tbl_admin_id_sess'];
		$tbl_gallery_category_id       = $_POST['gallery_category_id_enc'];		
		$category_name_en              = $_POST['category_name_en'];
		$category_name_ar              = $_POST['category_name_ar'];
		
		$this->load->model("model_gallery");
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
		if($tbl_school_id<>"")
		{  
		   $this->model_gallery->update_school_gallery_category($tbl_gallery_category_id,$category_name_en,$category_name_ar,$tbl_school_id);
		   echo "Y"; 
		}else{
		  echo "N";	
		}
	}
	
	  /**
	* @desc    Activate School Record Category 
	* @param   String 
	* @access  default
	*/
    function activateGalleryCategory() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_gallery_category_id = trim($_POST["gallery_category_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_gallery");		
		$this->model_gallery->activate_school_gallery_category($tbl_gallery_category_id,$tbl_school_id);
	}

	/**
	* @desc    Deactivate School Record
	* @param   String school_type_id_enc
	* @access  default
	*/
    function deactivateGalleryCategory() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_gallery_category_id = trim($_POST["gallery_category_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_gallery");		
		$this->model_gallery->deactivate_school_gallery_category($tbl_gallery_category_id,$tbl_school_id);
	}

     /**
	* @desc    Delete School Record
	* @param   POST array
	* @access  default
	*/
	function deleteGalleryCategory() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['gallery_category_id_enc']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('gallery_category_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_gallery");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_gallery->delete_school_gallery_category($str[$i],$tbl_school_id);
		}
	}
	

    /**
	* @desc    Delete category image
	* @param   POST array
	* @access  default
	*/
	function delete_gallery_category_image() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_gallery_category_id = $_POST['gallery_category_id_enc'];
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$this->load->model("model_gallery");
		$this->model_gallery->delete_gallery_category_image($tbl_gallery_category_id);	
	}
	
	
	 function updateSortOrder()
	{
		$sectionids = $_POST['sectionsid'];
        $this->load->model("model_gallery");		
		$results = $this->model_gallery->update_gallery_category_order($sectionids);
		if($results=="1")
		   return "success";
		else
		   return "failure";
	}
	
     // Admin Gallery Image list and view
     function view_school_gallery_list() {
		$data['page'] = "view_school_gallery_details";
		$data['menu'] = "gallery";
        $data['sub_menu'] = "gallery";
		
		$this->load->model("model_parenting_school");

		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		
		
		$tbl_gallery_category_id = "";
		if (array_key_exists('tbl_gallery_category_id',$param_array)) {
			$tbl_gallery_category_id = $param_array['tbl_gallery_category_id'];
			$data['tbl_gallery_category_id'] = $tbl_gallery_category_id;
		}
		
		$tbl_school_id = "";
		if (array_key_exists('tbl_school_id',$param_array)) {
			$tbl_school_id = $param_array['tbl_school_id'];
			$data['tbl_school_id'] = $tbl_school_id;
		}
		
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];
			$data['tbl_student_id'] = $tbl_student_id;
		}
		
	
		$this->load->model('model_gallery');
		$data_category = $this->model_gallery->get_gallery_category_name($tbl_gallery_category_id);
		$data_gallery_images = $this->model_gallery->get_gallery_category_images($tbl_gallery_category_id);
		
		$data['data_category'] = $data_category;
		$data['data_gallery_images'] = $data_gallery_images;
		
		$this->load->view('admin/view_template', $data);
	}
	
	//END ADD SCHOOL GALLERY CATEGORY......................

	
	/*****************************************************************/
	
	
	
	
	

	
}
?>