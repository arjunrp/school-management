<?php
/**
 * @desc   	  	School Controller
 * @category   	Controller
 * @author     	Shanavas PK
 * @version    	0.1
 */
class School extends CI_Controller {
	/**
	* @desc    Default function for the Controller
	* @param   none
	* @access  default
	*/
    function index() {
	}



    /**
	* @desc    Show all schools
	* @param   none
	* @access  default
	*/
    function all_schools() {
		$data['page'] = "view_all_schools";
		$data['menu'] = "schools";
        $data['sub_menu'] = "schools_list";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "I";
		
		$sort_name = "id";
		$sort_by = "DESC";
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "school_name";
					 break;
				}
				case("I"): {
					$sort_name = "id";
					 break;
				}
				default: {
					$sort_name = "school_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
	
	
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_school");
		
		$is_active = "";
		$rs_all_schools      = $this->model_school->list_all_schools($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id);
		$total_schools       = $this->model_school->get_total_schools($q, $is_active, $tbl_school_id);
		
		
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/school/all_schools";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_schools;
		$config['per_page'] = TBL_SCHOOL_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_SCHOOL_PAGING >= $total_schools) {
			$range = $total_schools;
		} else {
			$range = $offset+TBL_SCHOOL_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_schools schools</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_schools']	     = $rs_all_schools;
		$data['total_schools'] 	         = $total_schools;

		$this->load->view('admin/view_timeline_template',$data);
	}
	
	 /**
	* @desc    Activate School
	* @param   String 
	* @access  default
	*/
    function activateSchool() {
		if(isset($_POST) && count($_POST) != 0) {
			$school_id_enc = trim($_POST["school_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$this->load->model("model_school");		
		$this->model_school->activate_school($school_id_enc);
	}

	/**
	* @desc    Deactivate School
	* @access  default
	*/
    function deactivateSchool() {
		if(isset($_POST) && count($_POST) != 0) {
			$school_id_enc = trim($_POST["school_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$this->load->model("model_school");		
		$this->model_school->deactivate_school($school_id_enc);
	}

     /**
	* @desc    Delete School
	* @param   POST array
	* @access  default
	*/
	function deleteSchool() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['school_id_enc']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('school_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$this->load->model("model_school");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_school->delete_school($str[$i]);
		}
	}
	
	function is_exist_school()
	{
		if ($_POST) {
			$tbl_school_id          = $_POST['school_id_enc'];		
			$school_name             = $_POST['school_name'];
		}
		$this->load->model("model_school");		
		$results = $this->model_school->is_exist_school($tbl_school_id, $school_name);
		if(count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
	// Teacher Details
		/**
	* @desc    Teacher details
	* @param   none
	* @access  default
	*/
    function school_details() {
		$data['page'] = "view_all_schools";
		$data['menu'] = "schools";
        $data['sub_menu'] = "schools_list";
		$data['mid'] = "4";//Details

		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_user_id = 0;
					
		if (array_key_exists('school_id_enc',$param_array)) {
			$tbl_school_id = $param_array['school_id_enc'];
			$data['tbl_school_id'] = $tbl_school_id;
		}	 
		$this->load->model("model_school");		
		$this->load->model("model_admin");		
		//School details
		$school_obj 				= $this->model_school->get_schoolObj($tbl_school_id);
		$data['school_obj'] 		= $school_obj;
		
		$school_admin_obj 	        = $this->model_admin->getSchoolAdmin($tbl_school_id,'SA','S');
		$data['school_admin_obj']   = $school_admin_obj;
		
		$this->load->view('admin/view_timeline_template', $data);
	}
	


	
	
	//add school

	 function add_school() {
		if ($_POST) {
			$tbl_school_id             = $_POST['school_id_enc'];	
			$school_name               = $_POST['school_name'];
			$school_name_ar            = $_POST['school_name_ar'];
			$school_type               = $_POST['school_type'];
			$email         	           = $_POST['email'];		
			$contact_person            = $_POST['contact_person'];
			$phone                     = $_POST['phone'];
			$mobile                    = $_POST['mobile'];
			$fax                       = $_POST['fax'];
			$address                   = $_POST['address'];
			$color_code                = isset($_POST['color_code'])? $_POST['color_code']:'#fff' ;
			$is_active                 = $_POST['is_active'];
				
			$first_name                = $_POST['first_name'];
			$last_name                 = $_POST['last_name'];
			
			$user_id                   = $_POST['user_id'];
			$password                  = $_POST['password'];

		}
		$this->load->model("model_school");	
		$this->load->model("model_admin");	
		$resData = $this->model_school->add_school($tbl_school_id, $school_name, $school_name_ar, $school_type, $email, $contact_person, $phone, $mobile, $fax, $address, $color_code, $is_active); 
		if ($resData=="Y") {
			$tbl_admin_user_id = substr(md5(uniqid(rand())),0,15);
			$resAdminUser        = $this->model_admin->create_school_admin($tbl_admin_user_id, $first_name, $last_name, '', 'male', $mobile, $email, $user_id, $password, $tbl_school_id);
		}
	   if($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
	function is_exist_school_user_id() {
		if ($_POST) {
			$tbl_school_id               = $_POST['school_id_enc'];		
			$user_id                     = $_POST['user_id'];
		}
		$this->load->model("model_school");		
		$results = $this->model_school->is_exist_school_user_id($tbl_school_id, $user_id);
		if(count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
	//edit functionality
	/**
	* @desc    Edit school
	* @param   none
	* @access  default
	*/
    function edit_school() {
		$data['page'] = "view_all_schools";
		$data['menu'] = "schools";
        $data['sub_menu'] = "schools_list";
		$data['mid'] = "3";
		$this->load->model("model_school");
		$this->load->model("model_admin");
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_teacher_id = 0;
		if (array_key_exists('school_id_enc',$param_array)) {
			$tbl_school_id = $param_array['school_id_enc'];
		}
		$data['tbl_uploads_id'] = $tbl_school_id;
	
		//School details
		$school_obj 				= $this->model_school->get_schoolObj($tbl_school_id);
		$data['school_obj'] 		= $school_obj;
		
		$school_admin_obj 	        = $this->model_admin->getSchoolAdmin($tbl_school_id,'SA','S');
		$data['school_admin_obj']   = $school_admin_obj;
		
		$this->load->view('admin/view_timeline_template', $data);
	}


    function save_school_changes() {
   
    	if ($_POST) {
			$tbl_school_id             = $_POST['school_id_enc'];
			$tbl_admin_id              = $_POST['admin_id_enc'];
				
			$school_name               = $_POST['school_name'];
			$school_name_ar            = $_POST['school_name_ar'];
			$school_type               = $_POST['school_type'];
			$email         	           = $_POST['email'];		
			$contact_person            = $_POST['contact_person'];
			$phone                     = $_POST['phone'];
			$mobile                    = $_POST['mobile'];
			$fax                       = $_POST['fax'];
			$address                   = $_POST['address'];
			$color_code                = isset($_POST['color_code'])? $_POST['color_code']:'#fff' ;
			$is_active                 = $_POST['is_active'];
				
			$first_name                = $_POST['first_name'];
			$last_name                 = $_POST['last_name'];
			
			$user_id                   = $_POST['user_id'];
			$password                  = $_POST['password'];

		}
		
		$this->load->model("model_school");	
		$this->load->model("model_admin");	
		$resData = $this->model_school->update_school($tbl_school_id, $school_name, $school_name_ar, $school_type, $email, $contact_person, $phone, $mobile, $fax, $address, $color_code, $is_active); 
		if ($resData=="Y") {
			$tbl_admin_user_id = substr(md5(uniqid(rand())),0,15);
			$resAdminUser        = $this->model_admin->update_school_admin($tbl_admin_id, $first_name, $last_name, '', 'male', $mobile, $email, $user_id, $password, $tbl_school_id);
		}
	   if($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
		
		
		$this->load->model("model_teachers");	
		$resData = $this->model_teachers->update_teacher($tbl_teacher_id, $first_name, $last_name, $first_name_ar, $last_name_ar, $dob_month, $dob_day, $dob_year, 
		                                             $gender, $mobile, $email, $country, $emirates_id, $tbl_school_roles_id, $tbl_class_teacher_id, $user_id, $password, $tbl_school_id);
		if ($resData=="Y") {
			$delAssignClasses = $this->model_teachers->delete_assign_classes($tbl_teacher_id, $tbl_school_id);
			for($m=0; $m<count($str); $m++) {
				if($str[$m]<>""){
					$tbl_teacher_class_id = substr(md5(uniqid(rand())),0,15);
					$resTeacher = $this->model_teachers->assign_classes($tbl_teacher_class_id, $str[$m], $tbl_teacher_id, $tbl_school_id);
				}
		    }
		}
		
	   if($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
		
	}
	
	function is_exist_edit_school()
	{
		if ($_POST) {
			$tbl_school_id           = $_POST['school_id_enc'];		
			$school_name             = $_POST['school_name'];
		}
		$this->load->model("model_school");		
		$results = $this->model_school->is_exist_edit_school($tbl_school_id, $school_name);
		if(count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
	
	function is_exist_edit_school_user_id() {
		if ($_POST) {
			$tbl_school_id               = $_POST['school_id_enc'];		
			$user_id                     = $_POST['user_id'];
		}
		$this->load->model("model_school");		
		$results = $this->model_school->is_exist_edit_school_user_id($tbl_school_id, $user_id);
		if(count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
    // END UPDATE SCHOOL
	
	
	/**
	* @desc    Show all school subjects
	* @param   none
	* @access  default
	*/
    function school_subject() {
		$data['page'] = "view_subject";
		$data['menu'] = "configuration";
        $data['sub_menu'] = "subject";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "subject";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "subject";
					 break;
				}
				default: {
					$sort_name = "subject";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_category_id = "";
		if (array_key_exists('tbl_school_type_id',$param_array)) {
			$tbl_school_type_id = $param_array['tbl_school_type_id'];
			$data['tbl_sel_school_type_id'] = $tbl_school_type_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_school_subject");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			$rs_all_subjects      = $this->model_school_subject->get_all_school_subjects($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id);
			$total_subjects       = $this->model_school_subject->get_total_school_subjects($q, $is_active, $tbl_school_id);
		}else{
			$rs_all_subjects     = array();
			$total_subjects      = array();
		}
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/school/school_subject";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_school_type_id) && trim($tbl_school_type_id)!="") {
			$page_url .= "/tbl_school_type_id/".rawurlencode($tbl_school_type_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_subjects;
		$config['per_page'] = TBL_SCHOOL_SUBJECT_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_SCHOOL_SUBJECT_PAGING >= $total_subjects) {
			$range = $total_subjects;
		} else {
			$range = $offset+TBL_SCHOOL_SUBJECT_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_subjects subjects</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_subjects']	     = $rs_all_subjects;
		$data['total_subjects'] 	     = $total_subjects;

		$this->load->view('admin/view_template',$data);
	}
	
	function is_exist_subject() {
		if ($_POST) {
			$tbl_subject_id         = $_POST['subject_id_enc'];		
			$subject                = $_POST['subject'];
			$subject_ar             = $_POST['subject_ar'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_school_subject");		
		$results = $this->model_school_subject->is_exist_subject($tbl_subject_id, $subject, $subject_ar, $tbl_school_id);
		if (count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
    /**
	* @desc    Create school role
	* @param   POST array
	* @access  default
	*/
    function create_school_subject() {
		if ($_POST) {
			$tbl_subject_id         = $_POST['subject_id_enc'];	
			$subject                = $_POST['subject'];
			$subject_ar             = $_POST['subject_ar'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_school_subject");	
		$resData = $this->model_school_subject->create_school_subject($tbl_subject_id, $subject, $subject_ar, $tbl_school_id);
		if ($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}

    /**
	* @desc    Activate school role
	* @param   String school_type_id_enc
	* @access  default
	*/
    function activateSubject() {
		if(isset($_POST) && count($_POST) != 0) {
			$subject_id_enc = trim($_POST["subject_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_school_subject");		
		$this->model_school_subject->activate_subject($subject_id_enc,$tbl_school_id);
	}


	/**
	* @desc    Deactivate school role
	* @param   String school_type_id_enc
	* @access  default
	*/
    function deactivateSubject() {
		if(isset($_POST) && count($_POST) != 0) {
			$subject_id_enc = trim($_POST["subject_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_school_subject");		
		$this->model_school_subject->deactivate_subject($subject_id_enc,$tbl_school_id);
	}

     /**
	* @desc    Delete school role
	* @param   POST array
	* @access  default
	*/
     
	function deleteSubject() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['subject_id_enc']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('subject_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_school_subject");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_school_subject->delete_subject($str[$i],$tbl_school_id);
		}
	}
	
	/**
	* @desc    Save changes
	* @param   POST array
	* @access  default
	*/
    function save_subject_changes() {
		if ($_POST) {
			$subject_id_enc= $_POST['subject_id_enc'];	
			$subject               = $_POST['subject'];
			$subject_ar            = $_POST['subject_ar'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_school_subject");			
		$this->model_school_subject->save_subject_changes($subject_id_enc, $subject, $subject_ar, $tbl_school_id);
	}

	/**
	* @desc    Edit school subject
	* @param   none
	* @access  default
	*/
    function edit_school_subject() {
		$data['page'] = "view_subject";
		$data['menu'] = "configuration";
        $data['sub_menu'] = "subject";
		
		$data['mid'] = "3";
		$this->load->model("model_school_subject");
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_subject_id = 0;
		if (array_key_exists('subject_id_enc',$param_array)) {
			$tbl_subject_id = $param_array['subject_id_enc'];
		}	 
		
		//School Subject details
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$school_subject_obj = $this->model_school_subject->get_school_subject_obj($tbl_subject_id,$tbl_school_id);
		$data['school_subject_obj'] = $school_subject_obj;
		$this->load->view('admin/view_template', $data);
	}
	
	// END SCHOOL SUBJECT
	
	
	
	
	
	
	
	
}
?>