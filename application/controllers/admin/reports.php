<?php
/**
 * @desc   	  	Reports Controller
 * @category   	Controller
 * @author     	Shanavas .PK
 * @version    	0.1
 */

class Reports extends CI_Controller {
	/**
	* @desc Default constructor for the Controller
	* @access default
	*/

    function index() {
	}

	/**
	* @desc    Show all students
	* @param   none
	* @access  default
	*/
  
    function school_reports()
	{
		$data['page'] = "view_cards_report";
		$data['menu'] = "reports";
        $data['sub_menu'] = "cards_report";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "first_name";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		
		
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name";
					 break;
				}
				default: {
					$sort_name = "first_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];                               
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$tbl_semester_id = "";
		if (array_key_exists('tbl_semester_id',$param_array)) {
			$tbl_semester_id = $param_array['tbl_semester_id'];
			$data['tbl_sel_semester_id'] = $tbl_semester_id;
		}
		
		$tbl_teacher_id = "";
		if (array_key_exists('tbl_teacher_id',$param_array)) {
			$tbl_teacher_id = $param_array['tbl_teacher_id'];
			$data['tbl_sel_teacher_id'] = $tbl_teacher_id;
		}
		
		$tbl_academic_year = "";
		if (array_key_exists('tbl_academic_year',$param_array)) {
			$tbl_academic_year = $param_array['tbl_academic_year'];
			$data['tbl_sel_academic_year'] = $tbl_academic_year;
		}
		
		$tbl_gender = "";
		if (array_key_exists('tbl_gender',$param_array)) {
			$tbl_gender = $param_array['tbl_gender'];
			$data['tbl_sel_gender'] = $tbl_gender;
		}
		
		$tbl_country = "";
		if (array_key_exists('tbl_country_id',$param_array)) {
			$tbl_country_id = $param_array['tbl_country_id'];
			$data['tbl_sel_country'] = $tbl_country_id;
		}
		
		$card_type = "";
		if (array_key_exists('card_type',$param_array)) {
			$card_type = $param_array['card_type'];
			$data['tbl_sel_card_type'] = $card_type;
		}
		
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		$this->load->model("model_teachers");
		$this->load->model("model_classes");
		$this->load->model("model_report");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$user_panel   = $_SESSION['aqdar_smartcare']['user_type_sess'];  
		
		
	
		
		if($tbl_school_id<>"")
		{  
			
			$cardsTotalObj = $this->model_report->total_school_card_reports($q="", $tbl_category_id, $is_active='', $tbl_school_id, $tbl_gender, $tbl_country_id, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id, $card_type);
			
			$cardsObj = $this->model_report->school_card_reports($sort_name, $sort_by, $offset, $q="", $tbl_category_id, $is_active='', $tbl_school_id, $tbl_gender, $tbl_country_id, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id, $card_type);
			
			
			if($academic_year_id=="" && $tbl_semester_id=="")
			{
				$current_date             = date("Y-m-d");
				$rs_current_semester      = $this->model_classes->get_current_semester($tbl_school_id);
				$tbl_sel_academic_year    = $rs_current_semester[0]['tbl_academic_year_id'];
				$tbl_sel_semester_id      = $rs_current_semester[0]['tbl_semester_id'];
				//$data['tbl_sel_semester_id']    = $tbl_sel_semester_id;
				$data['tbl_sel_semester_id']    = '';
				$data['tbl_sel_academic_year']  = $tbl_sel_academic_year;
				
			}
						
			$countriesObj          = $this->model_student->get_country_list('Y');
		    $data['countries_list']= $countriesObj;
			
			$cardsListObj = $this->model_report->all_school_cards($tbl_category_id, '', $tbl_school_id);
			$data['cards_list']= $cardsListObj;
			
			$academicObj          = $this->model_student->get_academic_year('Y',$tbl_school_id);
		    $data['academic_list']= $academicObj;
			
			
			$classesObj 		   = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
			$data['classes_list'] = $classesObj;
			
			$rs_all_teachers         = $this->model_teachers->get_list_teachers($tbl_school_id,$tbl_class_id,'Y');
			$data['rs_all_teachers'] = $rs_all_teachers;
			
			if($tbl_class_id<>"")
			{
				$students_list = $this->model_student->get_all_students_against_class($tbl_class_id);
				$data['rs_all_students'] = $students_list;
			}else{
				$data['rs_all_students'] =  array();
			}
			
			$rs_all_semesters         = $this->model_classes->get_academic_semesters($tbl_school_id,$tbl_sel_academic_year);
			$data['rs_all_semesters'] = $rs_all_semesters;
		}
		
		$data['cardsTotalObj'] =  $cardsTotalObj;
		$data['rs_all_cards']  =  $cardsObj;
		
	    if($user_panel =="teacher")
			$this->load->view('admin/view_teacher_template',$data);
		else
			$this->load->view('admin/view_template',$data);
	}
	// End Card Summary
	
	// cards report detailed
	 function card_reports_list()
	{
		$data['page'] = "view_cards_report_detailed";
		$data['menu'] = "reports";
        $data['sub_menu'] = "cards_report";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "first_name";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name";
					 break;
				}
				default: {
					$sort_name = "first_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];                               
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$tbl_semester_id = "";
		if (array_key_exists('tbl_semester_id',$param_array)) {
			$tbl_semester_id = $param_array['tbl_semester_id'];
			$data['tbl_sel_semester_id'] = $tbl_semester_id;
		}
		
		$tbl_teacher_id = "";
		if (array_key_exists('tbl_teacher_id',$param_array)) {
			$tbl_teacher_id = $param_array['tbl_teacher_id'];
			$data['tbl_sel_teacher_id'] = $tbl_teacher_id;
		}
		
		$tbl_academic_year = "";
		if (array_key_exists('tbl_academic_year',$param_array)) {
			$tbl_academic_year = $param_array['tbl_academic_year'];
			$data['tbl_sel_academic_year'] = $tbl_academic_year;
		}
		
		$card_type = "";
		if (array_key_exists('card_type',$param_array)) {
			$card_type = $param_array['card_type'];
			$data['tbl_sel_card_type'] = $card_type;
		}
		$tbl_gender = "";
		if (array_key_exists('gender',$param_array)) {
			$tbl_gender = $param_array['gender'];
			$data['tbl_sel_gender'] = $tbl_gender;
		}
		
		$tbl_country = "";
		if (array_key_exists('tbl_country_id',$param_array)) {
			$tbl_country_id = $param_array['tbl_country_id'];
			$data['tbl_sel_country'] = $tbl_country_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		$this->load->model("model_teachers");
		$this->load->model("model_classes");
		$this->load->model("model_report");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			
			$cardsDetailedTotalObj = $this->model_report->total_school_card_reports_detailed($q="", $card_type, $is_active='', $tbl_school_id, $tbl_gender, $tbl_country_id, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id);
			
			$cardsDetailedObj = $this->model_report->school_card_reports_detailed($sort_name, $sort_by, $offset, $q="", $card_type, $is_active='', $tbl_school_id, $tbl_gender, $tbl_country_id, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id);
								 
				if($academic_year_id=="" && $tbl_semester_id=="")
			{
				$current_date             = date("Y-m-d");
				$rs_current_semester      = $this->model_classes->get_current_semester($tbl_school_id);
				$tbl_sel_academic_year    = $rs_current_semester[0]['tbl_academic_year_id'];
				$tbl_sel_semester_id      = $rs_current_semester[0]['tbl_semester_id'];
				/*$data['tbl_sel_semester_id']    = $tbl_sel_semester_id;*/
				$data['tbl_sel_semester_id']    = '';
				$data['tbl_sel_academic_year']  = $tbl_sel_academic_year;
				
			}
						
			$countriesObj          = $this->model_student->get_country_list('Y');
		    $data['countries_list']= $countriesObj;
			
			$academicObj          = $this->model_student->get_academic_year('Y',$tbl_school_id);
		    $data['academic_list']= $academicObj;
			
			$cardsListObj = $this->model_report->all_school_cards($tbl_category_id, '', $tbl_school_id);
			$data['cards_list']= $cardsListObj;
			
			$classesObj 		   = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
			$data['classes_list'] = $classesObj;
			
			$rs_all_teachers         = $this->model_teachers->get_list_teachers($tbl_school_id,$tbl_class_id,'Y');
			$data['rs_all_teachers'] = $rs_all_teachers;
			
			if($tbl_class_id<>"")
			{
				$students_list = $this->model_student->get_all_students_against_class($tbl_class_id);
				$data['rs_all_students'] = $students_list;
			}else{
				$data['rs_all_students'] =  array();
			}
			
			$rs_all_semesters         = $this->model_classes->get_academic_semesters($tbl_school_id,$tbl_sel_academic_year);
			$data['rs_all_semesters'] = $rs_all_semesters;					 
		
		}
		
		
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/reports/card_reports_list";
		
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
			$page_url .= "/tbl_student_id/".rawurlencode($tbl_student_id);
		}
		if (isset($tbl_class_id) && trim($tbl_class_id)!="") {
			$page_url .= "/tbl_class_id/".rawurlencode($tbl_class_id);
		}
		if (isset($tbl_semester_id) && trim($tbl_semester_id)!="") {
			$page_url .= "/tbl_semester_id/".rawurlencode($tbl_semester_id);
		}
		if (isset($tbl_teacher_id) && trim($tbl_teacher_id)!="") {
			$page_url .= "/tbl_teacher_id/".rawurlencode($tbl_teacher_id);
		}
		if (isset($tbl_academic_year) && trim($tbl_academic_year)!="") {
			$page_url .= "/tbl_academic_year/".rawurlencode($tbl_academic_year);
		}
		
		if (isset($tbl_gender) && trim($tbl_gender)!="") {
			$page_url .= "/gender/".rawurlencode($tbl_gender);
		}
		
		if (isset($tbl_country_id) && trim($tbl_country_id)!="") {
			$page_url .= "/tbl_country_id/".rawurlencode($tbl_country_id);
		}
		if ($card_type!="") {
			$page_url .= "/card_type/".$card_type;
		}
	
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		$this->load->library('pagination');
		$config['base_url']   = $page_url;
		$config['total_rows'] = $cardsDetailedTotalObj;
		$config['per_page'] = TBL_TEACHER_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_TEACHER_PAGING >= $cardsDetailedTotalObj) {
			$range = $cardsDetailedTotalObj;
		} else {
			$range = $offset+TBL_TEACHER_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $cardsDetailedTotalObj cards points</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		$data['cardsDetailedTotalObj'] =  $cardsDetailedTotalObj;
		$data['rs_all_card_details']   =  $cardsDetailedObj;

        $user_panel   = $_SESSION['aqdar_smartcare']['user_type_sess'];  

 		if($user_panel =="teacher")
			$this->load->view('admin/view_teacher_template',$data);
		else
			$this->load->view('admin/view_template',$data);
		
	}
	
	// End Cards Report Detailed
   /********************************************************************************/
    // Student Points Report
	  function school_point_reports()
	{
		$data['page'] = "view_points_report";
		$data['menu'] = "reports";
        $data['sub_menu'] = "points_report";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "first_name";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name";
					 break;
				}
				default: {
					$sort_name = "first_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];                               
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$tbl_semester_id = "";
		if (array_key_exists('tbl_semester_id',$param_array)) {
			$tbl_semester_id = $param_array['tbl_semester_id'];
			$data['tbl_sel_semester_id'] = $tbl_semester_id;
		}
		
		$tbl_teacher_id = "";
		if (array_key_exists('tbl_teacher_id',$param_array)) {
			$tbl_teacher_id = $param_array['tbl_teacher_id'];
			$data['tbl_sel_teacher_id'] = $tbl_teacher_id;
		}
		
		$tbl_academic_year = "";
		if (array_key_exists('tbl_academic_year',$param_array)) {
			$tbl_academic_year = $param_array['tbl_academic_year'];
			$data['tbl_sel_academic_year'] = $tbl_academic_year;
		}
		
		$tbl_gender = "";
		if (array_key_exists('tbl_gender',$param_array)) {
			$tbl_gender = $param_array['tbl_gender'];
			$data['tbl_sel_gender'] = $tbl_gender;
		}
		
		$tbl_country = "";
		if (array_key_exists('tbl_country_id',$param_array)) {
			$tbl_country_id = $param_array['tbl_country_id'];
			$data['tbl_sel_country'] = $tbl_country_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		$this->load->model("model_teachers");
		$this->load->model("model_classes");
		$this->load->model("model_report");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			
			$pointsTotalObj = $this->model_report->total_school_point_reports($q="", $tbl_category_id, $is_active='', $tbl_school_id, $tbl_gender, $tbl_country_id, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id);
			
			$pointsObj = $this->model_report->school_point_reports($sort_name, $sort_by, $offset, $q="", $tbl_category_id, $is_active='', $tbl_school_id, $tbl_gender, $tbl_country_id, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id);
			
			
			if($academic_year_id=="" && $tbl_semester_id=="")
			{
				$current_date             = date("Y-m-d");
				$rs_current_semester      = $this->model_classes->get_current_semester($tbl_school_id);
				$tbl_sel_academic_year    = $rs_current_semester[0]['tbl_academic_year_id'];
				$tbl_sel_semester_id      = $rs_current_semester[0]['tbl_semester_id'];
				//$data['tbl_sel_semester_id']    = $tbl_sel_semester_id;
				$data['tbl_sel_semester_id']    = '';
				$data['tbl_sel_academic_year']  = $tbl_sel_academic_year;
				
			}
						
			$countriesObj          = $this->model_student->get_country_list('Y');
		    $data['countries_list']= $countriesObj;
			
			$academicObj          = $this->model_student->get_academic_year('Y',$tbl_school_id);
		    $data['academic_list']= $academicObj;
			
			
			$classesObj 		   = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
			$data['classes_list'] = $classesObj;
			
			$rs_all_teachers         = $this->model_teachers->get_list_teachers($tbl_school_id,$tbl_class_id,'Y');
			$data['rs_all_teachers'] = $rs_all_teachers;
			
			if($tbl_class_id<>"")
			{
				$students_list = $this->model_student->get_all_students_against_class($tbl_class_id);
				$data['rs_all_students'] = $students_list;
			}else{
				$data['rs_all_students'] =  array();
			}
			
			$rs_all_semesters         = $this->model_classes->get_academic_semesters($tbl_school_id,$tbl_sel_academic_year);
			$data['rs_all_semesters'] = $rs_all_semesters;
		}
		
		$data['pointsTotalObj'] =  $pointsTotalObj;
		$data['rs_all_points'] =  $pointsObj;
		   
		$user_panel   = $_SESSION['aqdar_smartcare']['user_type_sess'];  

 		if($user_panel =="teacher")
			$this->load->view('admin/view_teacher_template',$data);
		else
			$this->load->view('admin/view_template',$data);
	}
	// End Point Summary
	
	// point reports detailed
	
	 function point_reports_list()
	{
		$data['page'] = "view_points_report_detailed";
		$data['menu'] = "reports";
        $data['sub_menu'] = "points_report";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "first_name";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name";
					 break;
				}
				default: {
					$sort_name = "first_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];                               
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$tbl_semester_id = "";
		if (array_key_exists('tbl_semester_id',$param_array)) {
			$tbl_semester_id = $param_array['tbl_semester_id'];
			$data['tbl_sel_semester_id'] = $tbl_semester_id;
		}
		
		$tbl_teacher_id = "";
		if (array_key_exists('tbl_teacher_id',$param_array)) {
			$tbl_teacher_id = $param_array['tbl_teacher_id'];
			$data['tbl_sel_teacher_id'] = $tbl_teacher_id;
		}
		
		$tbl_academic_year = "";
		if (array_key_exists('tbl_academic_year',$param_array)) {
			$tbl_academic_year = $param_array['tbl_academic_year'];
			$data['tbl_sel_academic_year'] = $tbl_academic_year;
		}
		
		$card_type = "";
		if (array_key_exists('tbl_point_category_id',$param_array)) {
			$tbl_point_category_id = $param_array['tbl_point_category_id'];
			$data['tbl_sel_point_category_id'] = $tbl_point_category_id;
		}
		$tbl_gender = "";
		if (array_key_exists('gender',$param_array)) {
			$tbl_gender = $param_array['gender'];
			$data['tbl_sel_gender'] = $tbl_gender;
		}
		
		$tbl_country = "";
		if (array_key_exists('tbl_country_id',$param_array)) {
			$tbl_country_id = $param_array['tbl_country_id'];
			$data['tbl_sel_country'] = $tbl_country_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		$this->load->model("model_teachers");
		$this->load->model("model_classes");
		$this->load->model("model_report");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			
		
			$pointsDetailedObj = $this->model_report->school_point_reports_detailed($sort_name, $sort_by, $offset, $q="", $tbl_point_category_id, $is_active='', $tbl_school_id, $tbl_gender, $tbl_country_id, $tbl_class_id, $tbl_student_id, $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id);
								 
			$pointsDetailedTotalObj = $this->model_report->total_school_point_reports_detailed($q="", $tbl_point_category_id, $is_active='', $tbl_school_id, $tbl_gender, $tbl_country_id, $tbl_class_id, $tbl_student_id, $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id);
								 
			
			if($academic_year_id=="" && $tbl_semester_id=="")
			{
				$current_date             = date("Y-m-d");
				$rs_current_semester      = $this->model_classes->get_current_semester($tbl_school_id);
				$tbl_sel_academic_year    = $rs_current_semester[0]['tbl_academic_year_id'];
				$tbl_sel_semester_id      = $rs_current_semester[0]['tbl_semester_id'];
				/*$data['tbl_sel_semester_id']    = $tbl_sel_semester_id;*/
				$data['tbl_sel_semester_id']    = '';
				$data['tbl_sel_academic_year']  = $tbl_sel_academic_year;
				
			}
						
			$countriesObj          = $this->model_student->get_country_list('Y');
		    $data['countries_list']= $countriesObj;
			
			$academicObj          = $this->model_student->get_academic_year('Y',$tbl_school_id);
		    $data['academic_list']= $academicObj;
			
			
			$classesObj 		   = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
			$data['classes_list'] = $classesObj;
			
			$rs_all_teachers         = $this->model_teachers->get_list_teachers($tbl_school_id,$tbl_class_id,'Y');
			$data['rs_all_teachers'] = $rs_all_teachers;
			
			if($tbl_class_id<>"")
			{
				$students_list = $this->model_student->get_all_students_against_class($tbl_class_id);
				$data['rs_all_students'] = $students_list;
			}else{
				$data['rs_all_students'] =  array();
			}
			
			$rs_all_semesters         = $this->model_classes->get_academic_semesters($tbl_school_id,$tbl_sel_academic_year);
			$data['rs_all_semesters'] = $rs_all_semesters;					 
		
		}
		
		
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/reports/point_reports_list";
		
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
			$page_url .= "/tbl_student_id/".rawurlencode($tbl_student_id);
		}
		if (isset($tbl_class_id) && trim($tbl_class_id)!="") {
			$page_url .= "/tbl_class_id/".rawurlencode($tbl_class_id);
		}
		if (isset($tbl_semester_id) && trim($tbl_semester_id)!="") {
			$page_url .= "/tbl_semester_id/".rawurlencode($tbl_semester_id);
		}
		if (isset($tbl_teacher_id) && trim($tbl_teacher_id)!="") {
			$page_url .= "/tbl_teacher_id/".rawurlencode($tbl_teacher_id);
		}
		if (isset($tbl_academic_year) && trim($tbl_academic_year)!="") {
			$page_url .= "/tbl_academic_year/".rawurlencode($tbl_academic_year);
		}
		
		if (isset($tbl_gender) && trim($tbl_gender)!="") {
			$page_url .= "/gender/".rawurlencode($tbl_gender);
		}
		
		if (isset($tbl_country_id) && trim($tbl_country_id)!="") {
			$page_url .= "/tbl_country_id/".rawurlencode($tbl_country_id);
		}
		if ($tbl_point_category_id!="") {
			$page_url .= "/tbl_point_category_id/".$tbl_point_category_id;
		}
	
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		$this->load->library('pagination');
		$config['base_url']   = $page_url;
		$config['total_rows'] = $pointsDetailedTotalObj;
		$config['per_page'] = TBL_TEACHER_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_TEACHER_PAGING >= $pointsDetailedTotalObj) {
			$range = $pointsDetailedTotalObj;
		} else {
			$range = $offset+TBL_TEACHER_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $pointsDetailedTotalObj behaviour points</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		$data['pointsDetailedTotalObj'] =  $pointsDetailedTotalObj;
		$data['rs_all_point_details']   =  $pointsDetailedObj;
		   
		   
		$user_panel   = $_SESSION['aqdar_smartcare']['user_type_sess'];  

 		if($user_panel =="teacher")
			$this->load->view('admin/view_teacher_template',$data);
		else
			$this->load->view('admin/view_template',$data);
		
	}
	
	//End Student Point Reports
  
    // Start School Attendance Reports - Daily
	
    function attendance_days_reports()
	{ 
		$data['page'] = "view_attendance_days_report";
		$data['menu'] = "reports";
        $data['sub_menu'] = "attendance_report";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "first_name";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name";
					 break;
				}
				default: {
					$sort_name = "first_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];                               
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$tbl_semester_id = "";
		if (array_key_exists('tbl_semester_id',$param_array)) {
			$tbl_semester_id = $param_array['tbl_semester_id'];
			$data['tbl_sel_semester_id'] = $tbl_semester_id;
		}
		
		$tbl_teacher_id = "";
		if (array_key_exists('tbl_teacher_id',$param_array)) {
			$tbl_teacher_id = $param_array['tbl_teacher_id'];
			$data['tbl_sel_teacher_id'] = $tbl_teacher_id;
		}
		
		$tbl_academic_year = "";
		if (array_key_exists('tbl_academic_year',$param_array)) {
			$tbl_academic_year = $param_array['tbl_academic_year'];
			$data['tbl_sel_academic_year'] = $tbl_academic_year;
		}
		
		$tbl_gender = "";
		if (array_key_exists('tbl_gender',$param_array)) {
			$tbl_gender = $param_array['tbl_gender'];
			$data['tbl_sel_gender'] = $tbl_gender;
		}
		
		$tbl_country = "";
		if (array_key_exists('attendance_date',$param_array)) {
			$attendance_date = $param_array['attendance_date'];
			$dateArray       = explode("-",$attendance_date);   
			$data['attendance_date'] = $dateArray[1]."/".$dateArray[2]."/".$dateArray[0];
		}
		
		
		$data['search_btn'] = "";
		if (array_key_exists('search_btn',$param_array)) {
			$search_btn = $param_array['search_btn'];
			$data['search_btn'] = $search_btn;
		}
		
		/*if($attendance_date=="")
		{
			$attendance_date =  date("Y-m-d");
			$dateArray       = explode("-",$attendance_date);   
			$data['attendance_date'] = $dateArray[1]."/".$dateArray[2]."/".$dateArray[0];
		}*/
		
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		$this->load->model("model_teachers");
		$this->load->model("model_classes");
		$this->load->model("model_report");
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		
		$start_date = "";
		$end_date   = "";
		 
		if($tbl_academic_year<>"")
		{
			if($tbl_semester_id<>"")
			{
				$semester_info = $this->model_report->get_semester_duration($tbl_semester_id,$tbl_school_id);
				$start_date	   =	$semester_info[0]['start_date'];
				$end_date	   =	$semester_info[0]['end_date'];
				
			}else{
				$semester_info = $this->model_report->get_academic_duration($tbl_semester_id, $academic_year_id,$tbl_school_id);
				$start_date	   =	$semester_info[0]['start_date'];
				$end_date	   =	$semester_info[0]['end_date'];
			}
			
		}
		
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		
		
		$is_active = "";
		
		if($tbl_school_id<>"")
		{  
			$attendanceObj = $this->model_report->school_attendance_date_reports($sort_name, $sort_by, $offset, $q="", $tbl_category_id, $is_active='', $tbl_school_id, $tbl_gender, $attendance_date, $tbl_class_id, $tbl_student_id, $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id, $start_date, $end_date);
			
			$attendanceTotalObj = $this->model_report->total_school_attendance_date_reports($q="", $tbl_category_id, $is_active='', $tbl_school_id, $tbl_gender, $attendance_date, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id, $start_date, $end_date);
			
		
			
			if($academic_year_id=="" && $tbl_semester_id=="")
			{
				$current_date             = date("Y-m-d");
				$rs_current_semester      = $this->model_classes->get_current_semester($tbl_school_id);
				$tbl_sel_academic_year    = $rs_current_semester[0]['tbl_academic_year_id'];
				$tbl_sel_semester_id      = $rs_current_semester[0]['tbl_semester_id'];
				//$data['tbl_sel_semester_id']    = $tbl_sel_semester_id;
				$data['tbl_sel_semester_id']    = '';
				$data['tbl_sel_academic_year']  = $tbl_sel_academic_year;
				
			}
						
			$countriesObj          = $this->model_student->get_country_list('Y');
		    $data['countries_list']= $countriesObj;
			
			$academicObj          = $this->model_student->get_academic_year('Y',$tbl_school_id);
		    $data['academic_list']= $academicObj;
			
			
			$classesObj 		   = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
			$data['classes_list'] = $classesObj;
			
			$rs_all_teachers         = $this->model_teachers->get_list_teachers($tbl_school_id,$tbl_class_id,'Y');
			$data['rs_all_teachers'] = $rs_all_teachers;
			
			if($tbl_class_id<>"")
			{
				$students_list = $this->model_student->get_all_students_against_class($tbl_class_id);
				$data['rs_all_students'] = $students_list;
			}else{
				$data['rs_all_students'] =  array();
			}
			
			$rs_all_semesters         = $this->model_classes->get_academic_semesters($tbl_school_id,$tbl_sel_academic_year);
			$data['rs_all_semesters'] = $rs_all_semesters;
		}
	
			
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/reports/attendance_days_reports";
		
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
			$page_url .= "/tbl_student_id/".rawurlencode($tbl_student_id);
		}
		if (isset($tbl_class_id) && trim($tbl_class_id)!="") {
			$page_url .= "/tbl_class_id/".rawurlencode($tbl_class_id);
		}
		if (isset($tbl_semester_id) && trim($tbl_semester_id)!="") {
			$page_url .= "/tbl_semester_id/".rawurlencode($tbl_semester_id);
		}
		if (isset($tbl_teacher_id) && trim($tbl_teacher_id)!="") {
			$page_url .= "/tbl_teacher_id/".rawurlencode($tbl_teacher_id);
		}
		
		if (isset($tbl_gender) && trim($tbl_gender)!="") {
			$page_url .= "/gender/".rawurlencode($tbl_gender);
		}
		
		if (isset($tbl_academic_year) && trim($tbl_academic_year)!="") {
			$page_url .= "/tbl_academic_year/".rawurlencode($tbl_academic_year);
		}
	
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		
		if (isset($search_btn) && trim($search_btn)!="") {
			$page_url .= "/search_btn/".rawurlencode($search_btn);
		}
		$page_url .= "/offset";
		$this->load->library('pagination');
		$config['base_url']   = $page_url;
		$config['total_rows'] = $attendanceTotalObj;
		$config['per_page'] = PAGING_TBL_DATE_ATTENDANCE;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+PAGING_TBL_DATE_ATTENDANCE >= $attendanceTotalObj) {
			$range = $attendanceTotalObj;
		} else {
			$range = $offset+PAGING_TBL_DATE_ATTENDANCE;
		}

		$paging_string = "$start - $range <font color='#333'>of $attendanceTotalObj Attendances</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['attendanceTotalObj'] =  $attendanceTotalObj;
		$data['rs_all_attendance']  =  $attendanceObj;
		
	    $user_panel   = $_SESSION['aqdar_smartcare']['user_type_sess'];  

 		if($user_panel =="teacher")
			$this->load->view('admin/view_teacher_template',$data);
		else
			$this->load->view('admin/view_template',$data);
	}
	
	
	function view_attendance_report() {
		$data['page']     = "view_attendance_report";
		$data['menu']     = "reports";
        $data['sub_menu'] = "attendance_report";
		
		$this->load->model("model_report");
		$tbl_class_id  			 	= $_POST['tbl_class_id'];	
		$attendance_date         	= $_POST['attendance_date'];	
		$tbl_class_sessions_id      = $_POST['tbl_class_sessions_id'];
		$tbl_school_id              = $_POST['tbl_school_id'];	
		$tbl_student_id             = $_POST['tbl_student_id'];	
		$is_active = "";
		$attendance_report_details   = $this->model_report->get_attendance_report_details($attendance_date, $tbl_class_sessions_id, $tbl_class_id, $tbl_student_id, $tbl_school_id);
	   
	    $data['attendance_report_details'] = $attendance_report_details;  
		$this->load->view('admin/view_attendance_report_details.php', $data);
	}
	
	
	// End Attendance Daily Summary
	
	
	
	 function school_attendance_reports()
	{
		$data['page'] = "view_attendance_report";
		$data['menu'] = "reports";
        $data['sub_menu'] = "attendance_report";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "first_name";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name";
					 break;
				}
				default: {
					$sort_name = "first_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];                               
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$tbl_semester_id = "";
		if (array_key_exists('tbl_semester_id',$param_array)) {
			$tbl_semester_id = $param_array['tbl_semester_id'];
			$data['tbl_sel_semester_id'] = $tbl_semester_id;
		}
		
		$tbl_teacher_id = "";
		if (array_key_exists('tbl_teacher_id',$param_array)) {
			$tbl_teacher_id = $param_array['tbl_teacher_id'];
			$data['tbl_sel_teacher_id'] = $tbl_teacher_id;
		}
		
		$tbl_academic_year = "";
		if (array_key_exists('tbl_academic_year',$param_array)) {
			$tbl_academic_year = $param_array['tbl_academic_year'];
			$data['tbl_sel_academic_year'] = $tbl_academic_year;
		}
		
		$tbl_gender = "";
		if (array_key_exists('tbl_gender',$param_array)) {
			$tbl_gender = $param_array['tbl_gender'];
			$data['tbl_sel_gender'] = $tbl_gender;
		}
		
		$tbl_country = "";
		if (array_key_exists('attendance_date',$param_array)) {
			$attendance_date = $param_array['attendance_date'];
			$dateArray       = explode("-",$attendance_date);   
			$data['attendance_date'] = $dateArray[1]."/".$dateArray[2]."/".$dateArray[0];
		}
		if($attendance_date=="")
		{
			$attendance_date =  date("Y-m-d");
			$dateArray       = explode("-",$attendance_date);   
			$data['attendance_date'] = $dateArray[1]."/".$dateArray[2]."/".$dateArray[0];
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		$this->load->model("model_teachers");
		$this->load->model("model_classes");
		$this->load->model("model_report");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			$attendanceObj = $this->model_report->school_attendance_reports($sort_name, $sort_by, $offset, $q="", $tbl_category_id, $is_active='', $tbl_school_id, $tbl_gender, $attendance_date, $tbl_class_id, $tbl_student_id, $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id);
			
			$attendanceTotalObj = $this->model_report->total_school_attendance_reports($q="", $tbl_category_id, $is_active='', $tbl_school_id, $tbl_gender, $attendance_date, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id);
			
			
			
			if($academic_year_id=="" && $tbl_semester_id=="")
			{
				$current_date             = date("Y-m-d");
				$rs_current_semester      = $this->model_classes->get_current_semester($tbl_school_id);
				$tbl_sel_academic_year    = $rs_current_semester[0]['tbl_academic_year_id'];
				$tbl_sel_semester_id      = $rs_current_semester[0]['tbl_semester_id'];
				//$data['tbl_sel_semester_id']    = $tbl_sel_semester_id;
				$data['tbl_sel_semester_id']    = '';
				$data['tbl_sel_academic_year']  = $tbl_sel_academic_year;
				
			}
						
			$countriesObj          = $this->model_student->get_country_list('Y');
		    $data['countries_list']= $countriesObj;
			
			$academicObj          = $this->model_student->get_academic_year('Y',$tbl_school_id);
		    $data['academic_list']= $academicObj;
			
			
			$classesObj 		   = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
			$data['classes_list'] = $classesObj;
			
			$rs_all_teachers         = $this->model_teachers->get_list_teachers($tbl_school_id,$tbl_class_id,'Y');
			$data['rs_all_teachers'] = $rs_all_teachers;
			
			if($tbl_class_id<>"")
			{
				$students_list = $this->model_student->get_all_students_against_class($tbl_class_id);
				$data['rs_all_students'] = $students_list;
			}else{
				$data['rs_all_students'] =  array();
			}
			
			$rs_all_semesters         = $this->model_classes->get_academic_semesters($tbl_school_id,$tbl_sel_academic_year);
			$data['rs_all_semesters'] = $rs_all_semesters;
		}
	
			
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/reports/school_attendance_reports";
		
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
			$page_url .= "/tbl_student_id/".rawurlencode($tbl_student_id);
		}
		if (isset($tbl_class_id) && trim($tbl_class_id)!="") {
			$page_url .= "/tbl_class_id/".rawurlencode($tbl_class_id);
		}
		if (isset($tbl_semester_id) && trim($tbl_semester_id)!="") {
			$page_url .= "/tbl_semester_id/".rawurlencode($tbl_semester_id);
		}
		if (isset($tbl_teacher_id) && trim($tbl_teacher_id)!="") {
			$page_url .= "/tbl_teacher_id/".rawurlencode($tbl_teacher_id);
		}
		
		if (isset($tbl_gender) && trim($tbl_gender)!="") {
			$page_url .= "/gender/".rawurlencode($tbl_gender);
		}
		
		if (isset($tbl_academic_year) && trim($tbl_academic_year)!="") {
			$page_url .= "/tbl_academic_year/".rawurlencode($tbl_academic_year);
		}
	
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		$this->load->library('pagination');
		$config['base_url']   = $page_url;
		$config['total_rows'] = $attendanceTotalObj;
		$config['per_page'] = TBL_TEACHER_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_TEACHER_PAGING >= $attendanceTotalObj) {
			$range = $attendanceTotalObj;
		} else {
			$range = $offset+TBL_TEACHER_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $attendanceTotalObj Classes</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['attendanceTotalObj'] =  $attendanceTotalObj;
		$data['rs_all_attendance']  =  $attendanceObj;
		
	    $user_panel   = $_SESSION['aqdar_smartcare']['user_type_sess'];  

 		if($user_panel =="teacher")
			$this->load->view('admin/view_teacher_template',$data);
		else
			$this->load->view('admin/view_template',$data);
	}
	// End Attendance Daily Summary
	
	// Daily Attendance Detailed -  Daily Report
	 function attendance_reports_list()
	{
		$data['page'] = "view_attendance_report_detailed";
		$data['menu'] = "reports";
        $data['sub_menu'] = "attendance_report";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "first_name";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name";
					 break;
				}
				default: {
					$sort_name = "first_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];                               
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$tbl_semester_id = "";
		if (array_key_exists('tbl_semester_id',$param_array)) {
			$tbl_semester_id = $param_array['tbl_semester_id'];
			$data['tbl_sel_semester_id'] = $tbl_semester_id;
		}
		
		$tbl_teacher_id = "";
		if (array_key_exists('tbl_teacher_id',$param_array)) {
			$tbl_teacher_id = $param_array['tbl_teacher_id'];
			$data['tbl_sel_teacher_id'] = $tbl_teacher_id;
		}

		$tbl_gender = "";
		if (array_key_exists('gender',$param_array)) {
			$tbl_gender = $param_array['gender'];
			$data['tbl_sel_gender'] = $tbl_gender;
		}
		
		if (array_key_exists('attendance_date',$param_array)) {
			$attendance_date = $param_array['attendance_date'];
			$dateArray       = explode("-",$attendance_date);   
			$data['attendance_date'] = $dateArray[1]."/".$dateArray[2]."/".$dateArray[0];
		}
		if($attendance_date=="")
		{
			$attendance_date =  date("Y-m-d");
			$dateArray       = explode("-",$attendance_date);   
			$data['attendance_date'] = $dateArray[1]."/".$dateArray[2]."/".$dateArray[0];
		}
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		$this->load->model("model_teachers");
		$this->load->model("model_classes");
		$this->load->model("model_report");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			
		
			$attendanceDetailedObj = $this->model_report->school_attendance_reports_detailed($sort_name, $sort_by, $offset, $q="", $tbl_point_category_id, $is_active='', $tbl_school_id, $tbl_gender, $tbl_country_id, $tbl_class_id, $tbl_student_id, $tbl_semester_id, $attendance_date, $tbl_teacher_id);
								 
			$attendanceDetailedTotalObj = $this->model_report->total_school_attendance_reports_detailed($q="", $tbl_point_category_id, $is_active='', $tbl_school_id, $tbl_gender, $tbl_country_id, $tbl_class_id, $tbl_student_id, $tbl_semester_id, $attendance_date, $tbl_teacher_id);
								 
			
			if($academic_year_id=="" && $tbl_semester_id=="")
			{
				$current_date             = date("Y-m-d");
				$rs_current_semester      = $this->model_classes->get_current_semester($tbl_school_id);
				$tbl_sel_academic_year    = $rs_current_semester[0]['tbl_academic_year_id'];
				$tbl_sel_semester_id      = $rs_current_semester[0]['tbl_semester_id'];
				/*$data['tbl_sel_semester_id']    = $tbl_sel_semester_id;*/
				$data['tbl_sel_semester_id']    = '';
				$data['tbl_sel_academic_year']  = $tbl_sel_academic_year;
				
			}
						
			$countriesObj          = $this->model_student->get_country_list('Y');
		    $data['countries_list']= $countriesObj;
			
			$academicObj          = $this->model_student->get_academic_year('Y',$tbl_school_id);
		    $data['academic_list']= $academicObj;
			
			
			$classesObj 		   = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
			$data['classes_list'] = $classesObj;
			
			$rs_all_teachers         = $this->model_teachers->get_list_teachers($tbl_school_id,$tbl_class_id,'Y');
			$data['rs_all_teachers'] = $rs_all_teachers;
			
			if($tbl_class_id<>"")
			{
				$students_list = $this->model_student->get_all_students_against_class($tbl_class_id);
				$data['rs_all_students'] = $students_list;
			}else{
				$data['rs_all_students'] =  array();
			}
			
			$rs_all_semesters         = $this->model_classes->get_academic_semesters($tbl_school_id,$tbl_sel_academic_year);
			$data['rs_all_semesters'] = $rs_all_semesters;					 
		
		}
		
		
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/reports/attendance_reports_list";
		
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
			$page_url .= "/tbl_student_id/".rawurlencode($tbl_student_id);
		}
		if (isset($tbl_class_id) && trim($tbl_class_id)!="") {
			$page_url .= "/tbl_class_id/".rawurlencode($tbl_class_id);
		}
		if (isset($tbl_semester_id) && trim($tbl_semester_id)!="") {
			$page_url .= "/tbl_semester_id/".rawurlencode($tbl_semester_id);
		}
		if (isset($tbl_teacher_id) && trim($tbl_teacher_id)!="") {
			$page_url .= "/tbl_teacher_id/".rawurlencode($tbl_teacher_id);
		}
		
		if (isset($tbl_gender) && trim($tbl_gender)!="") {
			$page_url .= "/gender/".rawurlencode($tbl_gender);
		}
		
		if ($attendance_date!="") {
			$page_url .= "/attendance_date/".$attendance_date;
		}
	
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		$this->load->library('pagination');
		$config['base_url']   = $page_url;
		$config['total_rows'] = $attendanceDetailedTotalObj;
		$config['per_page'] = TBL_TEACHER_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_TEACHER_PAGING >= $attendanceDetailedTotalObj) {
			$range = $attendanceDetailedTotalObj;
		} else {
			$range = $offset+TBL_TEACHER_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $attendanceDetailedTotalObj Students</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		$data['attendanceDetailedTotalObj'] =  $attendanceDetailedTotalObj;
		$data['rs_all_attendance_details']   =  $attendanceDetailedObj;
		
		$user_panel   = $_SESSION['aqdar_smartcare']['user_type_sess'];  

 		if($user_panel =="teacher")
			$this->load->view('admin/view_teacher_template',$data);
		else
			$this->load->view('admin/view_template',$data);
		
	}
	// End Daily Attendance Detailed 
	
	// Print Report
	
	// Generate Student Card Summary Report
	 function generate_card_summary_report() {
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "first_name";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name";
					 break;
				}
				default: {
					$sort_name = "first_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];                               
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$tbl_semester_id = "";
		if (array_key_exists('tbl_semester_id',$param_array)) {
			$tbl_semester_id = $param_array['tbl_semester_id'];
			$data['tbl_sel_semester_id'] = $tbl_semester_id;
		}
		
		$tbl_teacher_id = "";
		if (array_key_exists('tbl_teacher_id',$param_array)) {
			$tbl_teacher_id = $param_array['tbl_teacher_id'];
			$data['tbl_sel_teacher_id'] = $tbl_teacher_id;
		}
		
		$tbl_academic_year = "";
		if (array_key_exists('tbl_academic_year',$param_array)) {
			$tbl_academic_year = $param_array['tbl_academic_year'];
			$data['tbl_sel_academic_year'] = $tbl_academic_year;
		}
		
		$card_type = "";
		if (array_key_exists('card_type',$param_array)) {
			$card_type = $param_array['card_type'];
			$data['tbl_sel_card_type'] = $card_type;
		}
		$tbl_gender = "";
		if (array_key_exists('gender',$param_array)) {
			$tbl_gender = $param_array['gender'];
			$data['tbl_sel_gender'] = $tbl_gender;
		}
		
		$tbl_country = "";
		if (array_key_exists('tbl_country_id',$param_array)) {
			$tbl_country_id = $param_array['tbl_country_id'];
			$data['tbl_sel_country'] = $tbl_country_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		$this->load->model("model_teachers");
		$this->load->model("model_classes");
		$this->load->model("model_report");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
		   $cardsDetailedTotalObj = $this->model_report->total_school_card_reports($q="", $tbl_category_id, $is_active='', $tbl_school_id, $tbl_gender, $tbl_country_id, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id, $card_type);
			
		   $cardsDetailedObj      = $this->model_report->school_card_reports($sort_name, $sort_by, $offset, $q="", $tbl_category_id, $is_active='', $tbl_school_id, $tbl_gender, $tbl_country_id, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id, $card_type);
		}
	
	    //print_r($cardsDetailedObj); exit;
		$data['cardsDetailedTotalObj'] =  $cardsDetailedTotalObj;
		$data['rs_all_card_details']   =  $cardsDetailedObj;
		
		$this->load->view('admin/pdf_student_cards_summary_report',$data);
		
	}
	
	// Generate Student Card Detailed Report
	 function generate_card_report() {
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "first_name";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name";
					 break;
				}
				default: {
					$sort_name = "first_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];                               
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$tbl_semester_id = "";
		if (array_key_exists('tbl_semester_id',$param_array)) {
			$tbl_semester_id = $param_array['tbl_semester_id'];
			$data['tbl_sel_semester_id'] = $tbl_semester_id;
		}
		
		$tbl_teacher_id = "";
		if (array_key_exists('tbl_teacher_id',$param_array)) {
			$tbl_teacher_id = $param_array['tbl_teacher_id'];
			$data['tbl_sel_teacher_id'] = $tbl_teacher_id;
		}
		
		$tbl_academic_year = "";
		if (array_key_exists('tbl_academic_year',$param_array)) {
			$tbl_academic_year = $param_array['tbl_academic_year'];
			$data['tbl_sel_academic_year'] = $tbl_academic_year;
		}
		
		$card_type = "";
		if (array_key_exists('card_type',$param_array)) {
			$card_type = $param_array['card_type'];
			$data['tbl_sel_card_type'] = $card_type;
		}
		$tbl_gender = "";
		if (array_key_exists('gender',$param_array)) {
			$tbl_gender = $param_array['gender'];
			$data['tbl_sel_gender'] = $tbl_gender;
		}
		
		$tbl_country = "";
		if (array_key_exists('tbl_country_id',$param_array)) {
			$tbl_country_id = $param_array['tbl_country_id'];
			$data['tbl_sel_country'] = $tbl_country_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		$this->load->model("model_teachers");
		$this->load->model("model_classes");
		$this->load->model("model_report");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			$cardsDetailedTotalObj = $this->model_report->total_school_card_reports_detailed($q="", $card_type, $is_active='', $tbl_school_id, $tbl_gender, $tbl_country_id, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id);
			
			$cardsDetailedObj = $this->model_report->school_card_reports_detailed($sort_name, $sort_by, $offset, $q="", $card_type, $is_active='', $tbl_school_id, $tbl_gender, $tbl_country_id, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id);
	
		}
	
		$data['cardsDetailedTotalObj'] =  $cardsDetailedTotalObj;
		$data['rs_all_card_details']   =  $cardsDetailedObj;
		$this->load->view('admin/pdf_student_cards_report',$data);
		
	}
	// End School Attendance Reports
    
	// Start Progress Report
	 function student_performance_report()
	{
		$data['page'] = "view_performance_report";
		$data['menu'] = "reports";
        $data['sub_menu'] = "student_performance_report";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "first_name";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name";
					 break;
				}
				default: {
					$sort_name = "first_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];                               
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$tbl_semester_id = "";
		if (array_key_exists('tbl_semester_id',$param_array)) {
			$tbl_semester_id = $param_array['tbl_semester_id'];
			$data['tbl_sel_semester_id'] = $tbl_semester_id;
		}
		
		$tbl_teacher_id = "";
		if (array_key_exists('tbl_teacher_id',$param_array)) {
			$tbl_teacher_id = $param_array['tbl_teacher_id'];
			$data['tbl_sel_teacher_id'] = $tbl_teacher_id;
		}
		
		$tbl_academic_year = "";
		if (array_key_exists('tbl_academic_year',$param_array)) {
			$tbl_academic_year = $param_array['tbl_academic_year'];
			$data['tbl_sel_academic_year'] = $tbl_academic_year;
		}
		
	
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		$this->load->model("model_teachers");
		$this->load->model("model_classes");
		$this->load->model("model_report");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
	
		$countriesObj          = $this->model_student->get_country_list('Y');
		$data['countries_list']= $countriesObj;
			
		$academicObj          = $this->model_student->get_academic_year('Y',$tbl_school_id);
		$data['academic_list']= $academicObj;
			
			
		$classesObj 		   = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
		$data['classes_list'] = $classesObj;
			
		$rs_all_semesters         = $this->model_classes->get_academic_semesters($tbl_school_id,$tbl_sel_academic_year);
		$data['rs_all_semesters'] = $rs_all_semesters;	
		
		if($tbl_class_id<>"")
		{
			$students_list = $this->model_student->get_all_students_against_class($tbl_class_id);
			$data['rs_all_students'] = $students_list;
		}else{
			$data['rs_all_students'] =  array();
		}
		
		if($tbl_student_id <>"")
		{
			$lan  		  = 'en';
			$data['lan']  = $lan;
		
			$this->load->model("model_student");
			$this->load->model('model_school');
			
			//GET Params
			$param_array = $this->uri->uri_to_assoc(2);
						
			if (array_key_exists('student_id_enc',$param_array)) {
				$tbl_student_id = $param_array['student_id_enc'];
				$data['tbl_student_id'] = $tbl_student_id;
			}	 
			
			//User details
			$this->load->model("model_student");
			$this->load->model("model_config");	
			$this->load->model("model_classes");	
				
			$tbl_school_id = ""; 	
			$student_obj = $this->model_student->student_info($tbl_student_id,$tbl_school_id);
			$data['student_obj'] = $student_obj;
			
			$tbl_class_id  = $student_obj[0]['tbl_class_id'];
			$tbl_parent_id = $student_obj[0]['tbl_parent_id'];
			$tbl_school_id = $student_obj[0]['tbl_school_id'];
			
			
			
			//PERFORMANCE GRAPH PRESENTATION
			$range                  = "year";
			$curr_date              =  date("Y-m-d");
			$curr_month             =  date("m");
			$curr_year              =  date("Y");
			if($range<>"")
			{
					 if($range == "week")
					 {
						 $to_date      =  $curr_date;
						 $startdateStr = strtotime("-1 week", strtotime($curr_date));
						 $from_date = date("Y-m-d",$startdateStr);
					 }
					 else if($range == "month")
					 {
						 $from_date      =  $curr_year."-".$curr_month."-01";
						 $to_date        =  $curr_year."-".$curr_month."-31";
						
					 }
					 else if($range == "year")
					 {
						  $to_date_range      =  $curr_date;
						  $startdateStr = strtotime("-11 months", strtotime($curr_date));
						  $from_date_range    = date("Y-m-d",$startdateStr);
						  $from_m		      = date("m",  strtotime($from_date_range));
						  $from_y		      = date("Y",  strtotime($from_date_range));
						  $to_m		          = date("m",  strtotime($to_date_range));
						  $to_y		          = date("Y",  strtotime($to_date_range));
						  
						  $from_date          = $from_y."-".$from_m."-01";
						  $to_date            = $to_y."-".$to_m."-31";
					 }
		
				$getSemesterId = $this->model_student->get_semester_id($tbl_school_id,$current_date);
				$tbl_semester_id = $getSemesterId[0]['tbl_semester_id'];
				$months = array();
			
				$sel_month =	 date("F");
				$months[0]	   =	 $sel_month;
		   
		   		for ($i = 1; $i < 12; $i++) {
					$months[$i] = date("F", strtotime( date( 'Y-m-01' )." -$i months"));
				}
				$data['months'] 		        = $months;
				
			
				$performance_graph 				= $this->model_config->get_performance_graphs($tbl_student_id,$tbl_class_id,$tbl_school_id, 'Y',$tbl_performance_id,$from_date,$to_date,$range);
				$data['performance_graph'] 		= $performance_graph;
		   }
			
			
			//PERFORMANCE ACTIVITIES
			$topic_categories = $this->model_config->get_performance_topics_new($tbl_student_id,$tbl_class_id,$tbl_school_id);
			$performance_activities = array();
			
			for($m=0;$m<count($topic_categories);$m++)
			{
				$performance_activities[$m]['id'] 					= $topic_categories[$m]['tbl_performance_id'];
				$performance_activities[$m]['title'] 				= $topic_categories[$m]['topic_en'];
				$performance_activities[$m]['title_ar']  			= $topic_categories[$m]['topic_ar'];
				$performance_activities[$m]['performance_value']  	= isset($topic_categories[$m]['performance_value'])? $topic_categories[$m]['performance_value']: '0' ;
				
			}
			$data['performance_activities'] = $performance_activities;
			
			
			//CARDS
			$current_date = date("Y-m-d");
				
			$data["semester_title"] = "";
			if($tbl_semester_id=="")
			{
				$getSemesterId   = $this->model_student->get_semester_id($tbl_school_id,$current_date);
				$tbl_semester_id = $getSemesterId[0]['tbl_semester_id'];
				if($lan=="en")
					$semester_title = $getSemesterId[0]['title'];
				else
					$semester_title = $getSemesterId[0]['title_ar'];
			
			 $data["semester_title"] = $semester_title;
			}
			$tbl_semester_id = "";
			
			
			
			$rs_cards = $this->model_student->get_all_cards_student($tbl_student_id,$tbl_semester_id);
			$cardTypeArray = array();
			for($p=0;$p<count($rs_cards);$p++)
			{
				$cardTypeArray[$p] = 	$rs_cards[$p]['card_type'];
			}
			
			
			$arr_cards = array();
			if($tbl_class_id<>""){
				$class_info = $this->model_classes->getClassInfo($tbl_class_id);
				$tbl_school_type_id = $class_info[0]['tbl_school_type_id'];
			}
			$card_categories = $this->model_config->get_cards_categories_new($tbl_school_id,$tbl_school_type_id,$lan);
			
			for($m=0;$m<count($card_categories);$m++)
			{
				$arr_cards[$m]['id'] 		= $card_categories[$m]['tbl_card_category_id'];
				$arr_cards[$m]['title'] 	= $card_categories[$m]['category_name_en'];
				$arr_cards[$m]['title_ar']  = $card_categories[$m]['category_name_ar'];
				/*$arr_cards[$m]['score'] 	= $card_categories[$m]['card_point'];*/
				$issuedPoint = 0;
				if(in_array($arr_cards[$m]['id'], $cardTypeArray))
				{
					 $rs_cards_semester = $this->model_student->get_cards_semester($tbl_student_id, $tbl_semester_id, $tbl_school_id, $arr_cards[$m]['id']);
						for ($i=0; $i<count($rs_cards_semester); $i++) {
							$cnt 				= $rs_cards_semester[$i]['cnt'];
							$card_type 			= $rs_cards_semester[$i]['card_type'];
							$card_issue_type    = $rs_cards_semester[$i]['card_issue_type'];
							if($card_issue_type=="issue"){
								$cardData[$card_type]['issue'] = $cnt;
								$cardData[$card_type]['card_type'] = $card_type;
							}else{
								$cardData[$card_type]['cancel'] = $cnt;
								$cardData[$card_type]['card_type'] = $card_type;
							}
							
						}
						$cardCntIssued  = 0;
						foreach($cardData as $key=>$value)
						{ 
							$cardPoint	    = 0;
							$issueCntCard 	= isset($value['issue'])? $value['issue'] : '0';
							$cancelCntCard 	= isset($value['cancel'])? $value['cancel'] : '0';
							$cardCntIssued  = $issueCntCard - $cancelCntCard;
						}
						//echo $cardCntIssued;
						
						$cardCntIssued = abs($cardCntIssued); 
						if(abs($cardCntIssued)<=0){
							$arr_cards[$m]['image'] = "N";
							$arr_cards[$m]['color'] =  "#0a7429";
							$arr_cards[$m]['score']  = "".abs($cardCntIssued);
						}else if($cardCntIssued==1){
							$arr_cards[$m]['image'] = "N";
							$arr_cards[$m]['color'] 	=  "#faf707";
							$arr_cards[$m]['score']  = "-".abs($cardCntIssued);
						}else if($cardCntIssued >=2){
							$arr_cards[$m]['image'] = "N";
							$arr_cards[$m]['color'] 	=  "#fa2307";
							$cardCntIssued = $cardCntIssued - 1;
							$arr_cards[$m]['score']  = "".abs($cardCntIssued);
						}
				}else{
					
					$arr_cards[$m]['score'] 	= "0";
					$arr_cards[$m]['image'] = "N";
					$arr_cards[$m]['color'] 	=  "#0a7429";
				}
			}
			$data["cards"] 				= $arr_cards;
			
			//Points
			$rs_student_point 	= $this->model_student->get_overview_student_points_detail($tbl_student_id,$tbl_class_id,$tbl_semester_id,$tbl_school_id);
			$data["rs_student_point"] 	= $rs_student_point;
			
			$totalPoint = 0;
			for($v=0;$v<count($rs_student_point);$v++)
			{
				$totalPoint = $totalPoint + $rs_student_point[$v]['student_point'];
			}
			$data["rs_student_total_point"] 	=   $totalPoint;
			
			$rs_student_point_graph 			= $this->model_student->get_student_points_graph($tbl_student_id,$tbl_class_id,$tbl_semester_id,$tbl_school_id, 'Y',$from_date,$to_date,$range);
			$data["rs_student_point_graph"] 	= $rs_student_point_graph;
			
			// Attendance 
			$rs_student_attendance 	    	= $this->model_student->get_total_student_attendance($tbl_student_id,$tbl_class_id,$tbl_school_id, 'Y',$from_date,$to_date,$range);
			$data["rs_student_attendance"] 	= $rs_student_attendance;
			
			// Attendance Graph
			$rs_student_attendance_graph 	    	= $this->model_student->get_student_attendance_graph($tbl_student_id,$tbl_class_id,$tbl_school_id, 'Y',$from_date,$to_date,$range);
			$data["rs_student_attendance_graph"] 	= $rs_student_attendance_graph;
		  
	   }
	   
	    $user_panel   = $_SESSION['aqdar_smartcare']['user_type_sess'];  

 		if($user_panel =="teacher")
			$this->load->view('admin/view_teacher_template',$data);
		else
			$this->load->view('admin/view_template',$data); 
	}
	
	
	// Start Student Progress Report
	 function student_progress_report()
	{
		$data['page'] = "view_progress_report";
		$data['menu'] = "reports";
        $data['sub_menu'] = "student_progress_report";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "first_name";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name";
					 break;
				}
				default: {
					$sort_name = "first_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];                               
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$tbl_semester_id = "";
		if (array_key_exists('tbl_semester_id',$param_array)) {
			$tbl_semester_id = $param_array['tbl_semester_id'];
			$data['tbl_sel_semester_id'] = $tbl_semester_id;
		}
		
		$tbl_teacher_id = "";
		if (array_key_exists('tbl_teacher_id',$param_array)) {
			$tbl_teacher_id = $param_array['tbl_teacher_id'];
			$data['tbl_sel_teacher_id'] = $tbl_teacher_id;
		}
		
		$tbl_academic_year = "";
		if (array_key_exists('tbl_academic_year',$param_array)) {
			$tbl_academic_year = $param_array['tbl_academic_year'];
			$data['tbl_sel_academic_year'] = $tbl_academic_year;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		$this->load->model("model_teachers");
		$this->load->model("model_classes");
		$this->load->model("model_report");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
	
		$countriesObj          = $this->model_student->get_country_list('Y');
		$data['countries_list']= $countriesObj;
			
		$academicObj          = $this->model_student->get_academic_year('Y',$tbl_school_id);
		$data['academic_list']= $academicObj;
			
			
		$classesObj 		   = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
		$data['classes_list'] = $classesObj;
			
		$rs_all_semesters         = $this->model_classes->get_academic_semesters($tbl_school_id,$tbl_sel_academic_year);
		$data['rs_all_semesters'] = $rs_all_semesters;	
		
		if($tbl_class_id<>"")
		{
			$students_list = $this->model_student->get_all_students_against_class($tbl_class_id);
			$data['rs_all_students'] = $students_list;
		}else{
			$data['rs_all_students'] =  array();
		}
		
		
			$this->load->model('model_student');
			$current_date           = date("Y-m-d");
				
			$data["semester_title"] = "";
			if($tbl_semester_id=="")
			{
				$getSemesterId   = $this->model_student->get_semester_id($school_id,$current_date);
				$tbl_semester_id = $getSemesterId[0]['tbl_semester_id'];
				if($lan=="en")
					$semester_title = $getSemesterId[0]['title'];
				else
					$semester_title = $getSemesterId[0]['title_ar'];
			
			 $data["semester_title"] = $semester_title;
			}
			
			$tbl_teacher_id = "";
			$this->load->model('model_config');
			$this->load->model('model_classes');
			$this->load->model('model_student');
			$this->load->model('model_school_subject');
			
			$rs_subject 		= $this->model_school_subject->get_all_school_subjects('', '', '', $q, 'Y', $tbl_school_id);
			
			$rs_progress_report = $this->model_student->get_progress_report_list($tbl_school_id, $tbl_teacher_id);
			// Progress Student Report List
		
			$param_array = $this->uri->uri_to_assoc(3);
			$q = "";
			$sort_by_click = "N";
			$sort_name_param = "A";
			
			$sort_name = "id";
			$sort_by = "DESC";
			
			$icon_sort = "icon_bottom.jpg";
			$offset = 0;
			$total_categories = 0;
						
			if (array_key_exists('offset',$param_array)) {
				$offset = $param_array['offset'];
				if (trim($offset) == "") {
					$offset = 0;	
				}
			}	 
			
			if (array_key_exists('tbl_student_id',$param_array)) {
				$tbl_student_id = $param_array['tbl_student_id'];
			}
		
			
			if (array_key_exists('sort_by_click',$param_array)) {
				$sort_by_click = $param_array['sort_by_click'];
			}	 
			if (array_key_exists('sort_name',$param_array)) {
				$sort_name_param = $param_array['sort_name'];
				
				switch($sort_name_param) {
					case("A"): {
						$sort_name = "id";
						 break;
					}
					default: {
						$sort_name = "id";
					}					
				}
			}	 
			
			if (array_key_exists('sort_by',$param_array)) {
				$sort_by = $param_array['sort_by'];
			}	 
			
			if (trim($sort_by_click) == "Y") { 
				if (trim($sort_by) == "ASC") {
					$sort_by = "DESC";
				} else if (trim($sort_by) == "DESC") {
					$sort_by = "ASC";
				}
			}
			
			$data['tbl_sel_student_id']  = $tbl_student_id; 
			$data['sort_by_click']   = $sort_by_click;			
			$data['sort_name']       = $sort_name;			
			$data['sort_name_param'] = $sort_name_param;			
			$data['sort_by']         = $sort_by;			
			$data['icon_sort']       = $icon_sort;			
			
			$data['q']      = $q;
			$data['offset'] = $offset;
		   
			$this->load->model("model_school_roles");
			
			$is_active = "";
			$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
			
			
			if($tbl_school_id<>"" && $tbl_student_id <>"")
			{  
				$rs_all_progress_reports_student      = $this->model_student->get_all_progress_reports_student($sort_name, $sort_by, $offset, $q, $is_active, $tbl_class_id, $tbl_student_id, $tbl_school_id);
				$total_progress_reports_student       = $this->model_student->get_total_progress_reports_student($q, $is_active, $tbl_class_id, $tbl_student_id, $tbl_school_id);
			}else{
				$rs_all_progress_reports_student     = array();
				$total_progress_reports_student      = 0;
			}
		
			//PAGINATION CLASS
			$page_url = HOST_URL."/".LAN_SEL."/admin/reports/student_progress_report";
			if (isset($q) && trim($q)!="") {
				$page_url .= "/q/".rawurlencode($q);
			}
			if (isset($tbl_school_type_id) && trim($tbl_school_type_id)!="") {
				$page_url .= "/tbl_school_type_id/".rawurlencode($tbl_school_type_id);
			}
			if (isset($sort_name) && trim($sort_name)!="") {
				$page_url .= "/sort_name/".rawurlencode($sort_name_param);
			}
			if (isset($sort_by) && trim($sort_by)!="") {
				$page_url .= "/sort_by/".rawurlencode($sort_by);
			}
			$page_url .= "/offset";
			
			$this->load->library('pagination');
			$config['base_url'] = $page_url;
			$config['total_rows'] = $total_progress_reports_student;
			$config['per_page'] = TBL_SCHOOL_ROLES_PAGING;//constant
			$config['uri_segment'] = $this->uri->total_segments();
			$config['num_links'] = 5;
	
			$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
			$config['next_link_disable'] = '';
	
			$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
			$config['prev_link_disable'] = "";		
				
			$config['first_link'] = "";
			$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
			$config['first_tag_close'] = '</span>';
			
			$config['last_link'] = "";
			$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
			$config['last_tag_close'] = '</span>';
	
			$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
			$config['cur_tag_close'] = "&nbsp;</span>";
			
			$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
			$config['num_tag_close'] = "&nbsp;</span>";
	
			$this->pagination->initialize($config);
			$start = $offset + 1;
			$range = "";
			if ($offset+TBL_SCHOOL_ROLES_PAGING >= $total_progress_reports_student) {
				$range = $total_progress_reports_student;
			} else {
				$range = $offset+TBL_SCHOOL_ROLES_PAGING;
			}
	
			$paging_string = "$start - $range <font color='#333'>of $total_progress_reports_student reports</font>&nbsp;";
			$data['paging_string'] = $paging_string;
			$data['start']         = $start;
			
			// End Progress Student Report List
		
			//new change
			//$data_config = $this->model_config->get_config($school_id);
			
			if($tbl_class_id<>""){
				$class_info = $this->model_classes->getClassInfo($tbl_class_id);
				$student_info = $this->model_student->get_student_obj($tbl_student_id);
			}
		
		
			// Get Student Picture
			$file_name_updated = $this->model_student->get_student_picture($tbl_student_id);
		
			$this->load->model('model_teachers');
			$all_classes_against_teacher_rs 		= $this->model_teachers->get_all_classes_against_teacher_t($tbl_teacher_id);
			$data["file_name_updated"] 				= $file_name_updated;
			$data["all_classes_against_teacher_rs"] = $all_classes_against_teacher_rs;
			$data["rs_subject"] 					= $rs_subject;
			$data["rs_progress_report"] 			= $rs_progress_report;  // category
	
			$data["progress_reports_student"] 		= $rs_all_progress_reports_student;   
			$data["total_progress_reports_student"] = $total_progress_reports_student;  
			
			$data["student_info"] 					= $student_info;
			$data["class_info"] 					= $class_info; 
			
		   
		$user_panel   = $_SESSION['aqdar_smartcare']['user_type_sess'];  

 		if($user_panel =="teacher")
			$this->load->view('admin/view_teacher_template',$data);
		else
			$this->load->view('admin/view_template',$data);
		
	}
	
 
	
}



?>







