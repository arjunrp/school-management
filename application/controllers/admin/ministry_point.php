<?php

/**
 * @desc   	  	Ministry point Controller
 * @category   	Controller
 * @version    	0.1
 */
class Ministry_point extends CI_Controller {


	/**
	* @desc    Default function for the Controller
	* @param   none
	* @access  default
	*/
    function index() {
	}


    // Ministry points confuiguration
	
   /**
	* @desc    Students Behaviour Points Configuration
	* @param   none
	* @access  default
	*/
    function student_points_conf_ministry() {
		$data['page'] = "view_student_points_conf_ministry";
		$data['menu'] = "configuration";
        $data['sub_menu'] = "student_points_conf";
		
		$mode   ="ministry";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		//$sort_name = "point_name_en";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "point_name_en";
					 break;
				}
				default: {
					$sort_name = "point_name_en";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		
		$is_active = "";
		$rs_behaviour_points     = array();
		$total_behaviour_points  = '';
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$rs_behaviour_points      = $this->model_student->get_all_bevaviour_points($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id, $mode);
		$total_behaviour_points   = $this->model_student->get_total_behaviour_points($q, $is_active, $tbl_school_id, $mode);
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/ministry_point/student_points_conf_ministry";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
	
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_behaviour_points;
		$config['per_page'] = TBL_CARD_CATEGORY_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_CARD_CATEGORY_PAGING >= $total_behaviour_points) {
			$range = $total_behaviour_points;
		} else {
			$range = $offset+TBL_CARD_CATEGORY_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_behaviour_points behavior points</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_behaviour_points']	 = $rs_behaviour_points;
		$data['total_behaviour_points']  = $total_behaviour_points;

		$this->load->view('admin/view_ministry_template',$data);
	}
	
	
	
	function is_exist_behavior_point_ministry() {
		if ($_POST) {
			$tbl_point_category_id  = $_POST['point_category_id_enc'];		
			$point_name_en          = $_POST['point_name_en'];
			$point_name_ar          = $_POST['point_name_ar'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");		
		$results = $this->model_student->is_exist_behavior_point($tbl_point_category_id, $point_name_en, $point_name_ar, $tbl_school_id);
		if(count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
    /**
	* @desc    Add Student
	* @param   POST array
	* @access  default
	*/
    function add_behavior_point_ministry() {
		if ($_POST) {
			$tbl_point_category_id    = $_POST['point_category_id_enc'];		
			$point_name_en            = $_POST['point_name_en'];
			$point_name_ar            = $_POST['point_name_ar'];
			$behaviour_point          = $_POST['behaviour_point'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");	
		$resData = $this->model_student->add_behavior_point($tbl_point_category_id,$point_name_en,$point_name_ar,$behaviour_point,$tbl_school_id);
		if ($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
	

    /**
	* @desc    Activate Behavior Point
	* @param   String 
	* @access  default
	*/
    function activateBehaviorPointMinistry() {
		if(isset($_POST) && count($_POST) != 0) {
			$point_category_id_enc = trim($_POST["point_category_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");		
		$this->model_student->activateBehaviorPoint($point_category_id_enc,$tbl_school_id);
	}


	/**
	* @desc    Deactivate Behavior Point
	* @param   String school_type_id_enc
	* @access  default
	*/
    function deactivateBehaviorPointMinistry() {
		if(isset($_POST) && count($_POST) != 0) {
			$point_category_id_enc = trim($_POST["point_category_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");		
		$this->model_student->deactivateBehaviorPoint($point_category_id_enc,$tbl_school_id);
	}

     /**
	* @desc    Delete Behavior Point
	* @param   POST array
	* @access  default
	*/
     
	function deleteBehaviorPointMinistry() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['point_category_id_enc']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('point_category_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_student->deleteBehaviorPoint($str[$i],$tbl_school_id);
		}
	}

	
	
	/**
	* @desc    Save Behavior Point changes
	* @param   POST array
	* @access  default
	*/
    function save_behavior_point_changes_ministry() {
		if ($_POST) {
			$tbl_point_category_id    = $_POST['point_category_id_enc'];		
			$point_name_en            = $_POST['point_name_en'];
			$point_name_ar            = $_POST['point_name_ar'];
			$behaviour_point          = $_POST['behaviour_point'];
		
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_student");	
		$resData = $this->model_student->save_behavior_point_changes($tbl_point_category_id,$point_name_en,$point_name_ar,$behaviour_point,$tbl_school_id);
		if ($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}


	/**
	* @desc    Edit behavior point
	* @param   none
	* @access  default
	*/
    function edit_behavior_point_ministry() {
		$data['page']     = "view_student_points_conf_ministry";
		$data['menu']     = "configuration";
        $data['sub_menu'] = "student_points_conf";
		
		$data['mid'] = "3";
		$this->load->model("model_student");
	
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_student_id = 0;
		if (array_key_exists('point_category_id_enc',$param_array)) {
			$tbl_point_category_id = $param_array['point_category_id_enc'];
		}
		
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess']; 
		
		//Behaviour Point Details
		$point_obj = $this->model_student->get_bevaviour_point_info('', $tbl_point_category_id, $tbl_school_id);
		$data['point_obj'] = $point_obj;
		$this->load->view('admin/view_ministry_template', $data);
	}
	//END STUDENT BEHAVIOUR POINTS	
	
}
?>