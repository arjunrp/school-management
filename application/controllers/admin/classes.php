<?php
/**
 * @desc   	  	Class Controller
 * @category   	Controller
 * @author     	Shanavas PK
 * @version    	0.1
 */
class Classes extends CI_Controller {

	/**
	* @desc    Default function for the Controller
	* @param   none
	* @access  default
	*/
    function index() {
	}
	
	
	/**
	* @desc    Show all school types
	*
	* @param   none
	* @access  default
	*/
    function school_types() {
		$data['page'] = "view_school_type";
		$data['menu'] = "classes";
        $data['sub_menu'] = "class_types";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "school_type";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "school_type";
					 break;
				}
				default: {
					$sort_name = "school_type";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_category_id = "";
		if (array_key_exists('tbl_school_type_id',$param_array)) {
			$tbl_school_type_id = $param_array['tbl_school_type_id'];
			$data['tbl_sel_school_type_id'] = $tbl_school_type_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_classes");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			$rs_all_school_types      = $this->model_classes->get_all_school_types($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id);
			$total_school_types       = $this->model_classes->get_total_school_types($q, $is_active, $tbl_school_id);
		}else{
			$rs_all_school_types     = array();
			$total_school_types      = array();
		}
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/classes/school_types";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_school_type_id) && trim($tbl_school_type_id)!="") {
			$page_url .= "/tbl_school_type_id/".rawurlencode($tbl_school_type_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_school_types;
		$config['per_page'] = TBL_SCHOOL_TYPE_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_SCHOOL_TYPE_PAGING >= $total_school_types) {
			$range = $total_school_types;
		} else {
			$range = $offset+TBL_SCHOOL_TYPE_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_school_types class types</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_school_types']	     = $rs_all_school_types;
		$data['total_school_types'] 	      = $total_school_types;

		$this->load->view('admin/view_template',$data);
	}
	
	function is_exist_type() {
		if ($_POST) {
			$tbl_school_type_id     = $_POST['school_type_id_enc'];		
			$school_type            = $_POST['school_type'];
			$school_type_ar         = $_POST['school_type_ar'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");		
		$results = $this->model_classes->is_exist_type($tbl_school_type_id, $school_type, $school_type_ar, $tbl_school_id);
		if (count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
    /**
	* @desc    Create school type
	* @param   POST array
	* @access  default
	*/
    function create_school_type() {
		if ($_POST) {
			$tbl_school_type_id        = $_POST['school_type_id_enc'];	
			$school_type               = $_POST['school_type'];
			$school_type_ar            = $_POST['school_type_ar'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");	
		$resData = $this->model_classes->create_school_type($tbl_school_type_id, $school_type, $school_type_ar, $tbl_school_id);
		if ($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}

    /**
	* @desc    Activate school type
	* @param   String school_type_id_enc
	* @access  default
	*/
    function activateType() {
		if(isset($_POST) && count($_POST) != 0) {
			$school_type_id_enc = trim($_POST["school_type_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");		
		$this->model_classes->activate_type($school_type_id_enc,$tbl_school_id);
	}


	/**
	* @desc    Deactivate school type
	* @param   String school_type_id_enc
	* @access  default
	*/
    function deactivateType() {
		if(isset($_POST) && count($_POST) != 0) {
			$school_type_id_enc = trim($_POST["school_type_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");		
		$this->model_classes->deactivate_type($school_type_id_enc,$tbl_school_id);
	}

     /**
	* @desc    Delete school type
	* @param   POST array
	* @access  default
	*/
     
	function deleteType() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['school_type_id_enc']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('school_type_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_classes->delete_type($str[$i],$tbl_school_id);
		}
	}

	
	
	/**
	* @desc    Save changes
	* @param   POST array
	* @access  default
	*/
    function save_type_changes() {
		if ($_POST) {
			$tbl_school_type_id        = $_POST['school_type_id_enc'];	
			$school_type               = $_POST['school_type'];
			$school_type_ar            = $_POST['school_type_ar'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");			
		$this->model_classes->save_type_changes($tbl_school_type_id, $school_type, $school_type_ar, $tbl_school_id);
	}


	/**
	* @desc    Edit school type
	* @param   none
	* @access  default
	*/
    function edit_school_type() {
		$data['page'] = "view_school_type";
		$data['menu'] = "classes";
		$data['sub_menu'] = "class_types";
		$data['mid'] = "3";
		$this->load->model("model_classes");
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_school_type_id = 0;
		if (array_key_exists('school_type_id_enc',$param_array)) {
			$tbl_school_type_id = $param_array['school_type_id_enc'];
		}	 
		
		//School Type details
		$school_type_obj = $this->model_classes->get_school_type_obj($tbl_school_type_id);
		$data['school_type_obj'] = $school_type_obj;
		$this->load->view('admin/view_template', $data);
	}

	
	
	//START CLASS SECTION MANAGEMENT
	
	/**
	* @desc    Show all CLASS SECTIONS
	*
	* @param   none
	* @access  default
	*/
    function class_sections() {
		$data['page'] = "view_class_section";
		$data['menu'] = "classes";
        $data['sub_menu'] = "class_sections";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "section_name";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "section_name";
					 break;
				}
				default: {
					$sort_name = "section_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_category_id = "";
		if (array_key_exists('tbl_section_id',$param_array)) {
			$tbl_section_id = $param_array['tbl_section_id'];
			$data['tbl_sel_section_id'] = $tbl_section_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_classes");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			$rs_all_class_sections      = $this->model_classes->get_all_class_sections($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id);
			$total_class_sections       = $this->model_classes->get_total_class_sections($q, $is_active, $tbl_school_id);
		}else{
			$rs_all_class_sections     = array();
			$total_class_sections      = array();
		}
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/classes/class_sections";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_section_id) && trim($tbl_section_id)!="") {
			$page_url .= "/tbl_section_id/".rawurlencode($tbl_section_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_class_sections;
		$config['per_page'] = TBL_SECTION_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_SECTION_PAGING >= $total_class_sections) {
			$range = $total_class_sections;
		} else {
			$range = $offset+TBL_SECTION_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_class_sections class divisions</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_class_sections']	     = $rs_all_class_sections;
		$data['total_class_sections'] 	      = $total_class_sections;

		$this->load->view('admin/view_template',$data);
	}
	
	function is_exist_section() {
		if ($_POST) {
			$tbl_section_id     = $_POST['section_id_enc'];		
			$section_name       = $_POST['section_name'];
			$section_name_ar     = $_POST['section_name_ar'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");		
		$results = $this->model_classes->is_exist_section($tbl_section_id, $section_name, $section_name_ar, $tbl_school_id);
		if (count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
    /**
	* @desc    Create class section
	* @param   POST array
	* @access  default
	*/
    function create_class_section() {
		if ($_POST) {
			$tbl_section_id      = $_POST['section_id_enc'];	
			$section_name        = $_POST['section_name'];
			$section_name_ar     = $_POST['section_name_ar'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");	
		$resData = $this->model_classes->create_class_section($tbl_section_id, $section_name, $section_name_ar, $tbl_school_id);
		if ($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}

    /**
	* @desc    Activate class section
	* @param   String section_id_enc
	* @access  default
	*/
    function activateSection() {
		if(isset($_POST) && count($_POST) != 0) {
			$section_id_enc = trim($_POST["section_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");		
		$this->model_classes->activate_section($section_id_enc,$tbl_school_id);
	}


	/**
	* @desc    Deactivate class section
	* @param   String section_id_enc
	* @access  default
	*/
    function deactivateSection() {
		if(isset($_POST) && count($_POST) != 0) {
			$section_id_enc = trim($_POST["section_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");		
		$this->model_classes->deactivate_section($section_id_enc,$tbl_school_id);
	}

     /**
	* @desc    Delete school type
	* @param   POST array
	* @access  default
	*/
     
	function deleteSection() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['section_id_enc']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('section_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_classes->delete_section($str[$i],$tbl_school_id);
		}
	}
	
	/**
	* @desc    Save changes
	* @param   POST array
	* @access  default
	*/
    function save_section_changes() {
		if ($_POST) {
			$tbl_section_id            = $_POST['section_id_enc'];	
			$section_name              = $_POST['section_name'];
			$section_name_ar           = $_POST['section_name_ar'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");			
		$this->model_classes->save_section_changes($tbl_section_id, $section_name, $section_name_ar, $tbl_school_id);
	}

	/**
	* @desc    Edit section
	* @param   none
	* @access  default
	*/
    function edit_class_section() {
		$data['page'] = "view_class_section";
		$data['menu'] = "classes";
        $data['sub_menu'] = "class_sections";
		$data['mid'] = "3";
		$this->load->model("model_classes");
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_school_type_id = 0;
		if (array_key_exists('section_id_enc',$param_array)) {
			$tbl_section_id = $param_array['section_id_enc'];
		}	 
		
		//Class details
		$class_section_obj = $this->model_classes->get_class_section_obj($tbl_section_id);
		$data['class_section_obj'] = $class_section_obj;
		$this->load->view('admin/view_template', $data);
	}

	
	
	
	
	//START CLASS GRADES MANAGEMENT / CLASS MANAGEMENT
	
	/**
	* @desc    Show all CLASSES
	*
	* @param   none
	* @access  default
	*/
    function class_grades() {
		$data['page'] = "view_class_grade";
		$data['menu'] = "classes";
        $data['sub_menu'] = "class_grades";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "P";
		
		$sort_name = "priority";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "class_name";
					 break;
				}
				case("P"): {
					$sort_name = "priority";
					 break;
				}
				default: {
					$sort_name = "priority";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_category_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_classes");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			
			$schoolTypesObj = $this->model_classes->get_school_types($tbl_school_id, 'Y');
			$data['school_types'] = $schoolTypesObj;
			$classSectionsObj = $this->model_classes->get_class_sections($tbl_school_id, 'Y');
			$data['class_sections'] = $classSectionsObj;
			
			$rs_all_class_grades      = $this->model_classes->get_all_class_grades($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id);
			$total_class_grades       = $this->model_classes->get_total_class_grades($q, $is_active, $tbl_school_id);
		}else{
			$rs_all_class_grades     = array();
			$total_class_grades      = array();
		}
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/classes/class_grades";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_class_id) && trim($tbl_class_id)!="") {
			$page_url .= "/tbl_class_id/".rawurlencode($tbl_class_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] 	= $page_url;
		$config['total_rows'] 	= $total_class_grades;
		$config['per_page'] 	= TBL_CLASS_PAGING;//constant
		$config['uri_segment'] 	= $this->uri->total_segments();
		$config['num_links'] 	= 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_CLASS_PAGING >= $total_class_grades) {
			$range = $total_class_grades;
		} else {
			$range = $offset+TBL_CLASS_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_class_grades classes</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_class_grades']	     = $rs_all_class_grades;
		$data['total_class_grades'] 	      = $total_class_grades;

		$this->load->view('admin/view_template',$data);
	}
	
	function is_exist_grade() {
		if ($_POST) {
			$tbl_class_id       = $_POST['class_id_enc'];
			$tbl_school_type_id = $_POST['tbl_school_type_id'];		
			$class_name         = $_POST['class_name'];
			$class_name_ar      = $_POST['class_name_ar'];
			$tbl_section_id     = $_POST['tbl_section_id'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");		
		$results = $this->model_classes->is_exist_grade($tbl_class_id, $tbl_school_type_id, $class_name, $class_name_ar, $tbl_section_id, $tbl_school_id);
		if (count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
    /**
	* @desc    Create class
	* @param   POST array
	* @access  default
	*/
    function create_class_grade() {
		if ($_POST) {
			$tbl_class_id       = $_POST['class_id_enc'];
			$tbl_school_type_id = $_POST['tbl_school_type_id'];	
			$school_type        = $_POST['school_type'];
			$school_type_ar     = $_POST['school_type_ar'];
			$class_name         = $_POST['class_name'];
			$class_name_ar      = $_POST['class_name_ar'];
			$tbl_section_id     = $_POST['tbl_section_id'];
			$section_name       = $_POST['section_name'];
			$section_name_ar    = $_POST['section_name_ar'];
		}
		
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");	
	 	$resData = $this->model_classes->create_class_grade($tbl_class_id, $tbl_school_type_id, $school_type, $school_type_ar, $class_name, $class_name_ar, $tbl_section_id, $section_name, $section_name_ar,  $tbl_school_id);
		if ($resData=="Y") {
			echo "Y";
		}else if ($resData=="X") {
			echo "X";
		} else {
			echo "N";
		}
	}

    /**
	* @desc    Activate class 
	* @param   String section_id_enc
	* @access  default
	*/
    function activateGrade() {
		if(isset($_POST) && count($_POST) != 0) {
			$class_id_enc = trim($_POST["class_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");		
		$this->model_classes->activate_grade($class_id_enc,$tbl_school_id);
	}


	/**
	* @desc    Deactivate class
	* @param   String section_id_enc
	* @access  default
	*/
    function deactivateGrade() {
		if(isset($_POST) && count($_POST) != 0) {
			$class_id_enc = trim($_POST["class_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");		
		$this->model_classes->deactivate_grade($class_id_enc,$tbl_school_id);
	}

     /**
	* @desc    Delete class
	* @param   POST array
	* @access  default
	*/
     
	function deleteGrade() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['class_id_enc']; //e.g." school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('class_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_classes->delete_grade($str[$i],$tbl_school_id);
		}
	}

	
	
	/**
	* @desc    Save changes
	* @param   POST array
	* @access  default
	*/
    function save_grade_changes() {
		if ($_POST) {
			$tbl_class_id       = $_POST['class_id_enc'];
			$tbl_school_type_id = $_POST['tbl_school_type_id'];		
			$class_name         = $_POST['class_name'];
			$class_name_ar      = $_POST['class_name_ar'];
			$tbl_section_id     = $_POST['tbl_section_id'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");			
		$this->model_classes->save_grade_changes($tbl_class_id, $tbl_school_type_id, $class_name, $class_name_ar, $tbl_section_id, $tbl_school_id);
	}


	/**
	* @desc    Edit class 
	* @param   none
	* @access  default
	*/
    function edit_class_grade() {
		$data['page'] = "view_class_grade";
		$data['menu'] = "classes";
		$data['sub_menu'] = "class_grades";
		$data['mid'] = "3";
		$this->load->model("model_classes");
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_class_id = 0;
		if (array_key_exists('class_id_enc',$param_array)) {
			$tbl_class_id = $param_array['class_id_enc'];
		}
		
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$schoolTypesObj = $this->model_classes->get_school_types($tbl_school_id, 'Y');
	    $data['school_types'] = $schoolTypesObj;
	    $classSectionsObj = $this->model_classes->get_class_sections($tbl_school_id, 'Y');
		$data['class_sections'] = $classSectionsObj;	 
		
		//Class grade details
		$class_grade_obj = $this->model_classes->get_class_grade_obj($tbl_class_id);
		$data['class_grade_obj'] = $class_grade_obj;
		$this->load->view('admin/view_template', $data);
	}

	
	
	
	
	//START CLASS SESSIONS MANAGEMENT / CLASS PERIOD MANAGEMENT
	
	/**
	* @desc    Show all CLASSES SESSIONS
	* @param   none
	* @access  default
	*/
    function class_sessions() {
		$data['page'] = "view_class_session";
		$data['menu'] = "classes";
        $data['sub_menu'] = "class_sessions";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "title";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "title";
					 break;
				}
				default: {
					$sort_name = "title";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_class_sessions_id = "";
		if (array_key_exists('tbl_class_sessions_id',$param_array)) {
			$tbl_class_sessions_id = $param_array['tbl_class_sessions_id'];
			$data['tbl_sel_class_sessions_id'] = $tbl_class_sessions_id;
		}
		
		$tbl_class_id  = "";
		if (array_key_exists('class_id_enc',$param_array)) {
			$tbl_class_id = $param_array['class_id_enc'];
			$data['tbl_class_search_id'] = $tbl_class_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_class_sessions");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
			$classesObj = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
			$data['classes_list'] = $classesObj;
			
			$rs_all_class_sessions    = $this->model_class_sessions->get_all_class_sessions($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id, $tbl_class_id);
			$total_class_sessions     = $this->model_class_sessions->get_total_class_sessions($q, $is_active, $tbl_school_id, $tbl_class_id);
		}else{
			$rs_all_class_sessions     = array();
			$total_class_sessions      = array();
		}
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/classes/class_sessions";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_class_session_id) && trim($tbl_class_session_id)!="") {
			$page_url .= "/tbl_class_session_id/".rawurlencode($tbl_class_session_id);
		}
		
		if (isset($tbl_class_id) && trim($tbl_class_id)!="") {
			$page_url .= "/class_id_enc/".rawurlencode($tbl_class_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_class_sessions;
		$config['per_page'] = TBL_CLASS_SESSIONS_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_CLASS_SESSIONS_PAGING >= $total_class_sessions) {
			$range = $total_class_sessions;
		} else {
			$range = $offset+TBL_CLASS_SESSIONS_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_class_sessions class periods</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_class_sessions']	     = $rs_all_class_sessions;
		$data['total_class_sessions'] 	      = $total_class_sessions;

		$this->load->view('admin/view_template',$data);
	}
	
	function is_exist_session() {
		if ($_POST) {
			$tbl_class_session_id = $_POST['class_sessions_id_enc'];
			$tbl_class_id 	      = $_POST['tbl_class_id'];		
			$title         	      = $_POST['title'];
			$title_ar      	      = $_POST['title_ar'];
			$start_time     	  = date("H:i:s", strtotime($_POST['start_time']));
			$end_time     	      = date("H:i:s", strtotime($_POST['end_time']));
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_class_sessions");		
		$results = $this->model_class_sessions->is_exist_session($tbl_class_session_id, $tbl_class_id, $title, $title_ar, $start_time, $end_time, $tbl_school_id);
		echo $results;
	
	}
	
    /**
	* @desc    Create class session
	* @param   POST array
	* @access  default
	*/
    function create_class_session() {
		if ($_POST) {
			$tbl_class_session_id  = $_POST['class_sessions_id_enc'];
			$str = $_POST['tbl_class_id']; //e.g." tbl_class_id=f31981d6285a6a958f754&tbl_class_id=d99f9fee0&tbl_class_id=a343d70666c3
			$str = explode("&", $str);
			$title         	        = $_POST['title'];
			$title_ar      	        = $_POST['title_ar'];
			$start_time     	    = date("H:i:s", strtotime($_POST['start_time']));
			$end_time     	        = date("H:i:s", strtotime($_POST['end_time']));
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_class_sessions");
		for($m=0; $m<count($str); $m++) {
			if($str[$m]<>""){
				$resData = $this->model_class_sessions->create_class_session($tbl_class_session_id, $str[$m], $title, $title_ar, $start_time, $end_time, $tbl_school_id);
			}
		}
		echo "Y";
	}

    /**
	* @desc    Activate class session
	* @param   String session_id_enc
	* @access  default
	*/
    function activateSession() {
		if(isset($_POST) && count($_POST) != 0) {
			$class_sessions_id_enc = trim($_POST["class_sessions_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_class_sessions");		
		$this->model_class_sessions->activate_session($class_sessions_id_enc,$tbl_school_id);
	}


	/**
	* @desc    Deactivate class session
	* @param   String session_id_enc
	* @access  default
	*/
    function deactivateSession() {
		if(isset($_POST) && count($_POST) != 0) {
			$class_sessions_id_enc = trim($_POST["class_sessions_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_class_sessions");		
		$data = $this->model_class_sessions->deactivate_session($class_sessions_id_enc,$tbl_school_id);
		
	}

     /**
	* @desc    Delete class session
	* @param   POST array
	* @access  default
	*/
     
	function deleteSession() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['class_sessions_id_enc']; //e.g."             school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('class_sessions_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_class_sessions");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_class_sessions->delete_session($str[$i],$tbl_school_id);
		}
	}
	
	/**
	* @desc    Save changes
	* @param   POST array
	* @access  default
	*/
    function save_session_changes() {
		if ($_POST) {
			$tbl_class_session_id  = $_POST['class_sessions_id_enc'];
			$tbl_class_id 	      = $_POST['tbl_class_id'];		
			$title         	     = $_POST['title'];
			$title_ar      	      = $_POST['title_ar'];
			$start_time     	    = date("H:i:s", strtotime($_POST['start_time']));
			$end_time     	      =   date("H:i:s", strtotime($_POST['end_time']));
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_class_sessions");			
		$this->model_class_sessions->save_session_changes($tbl_class_session_id, $tbl_class_id, $title, $title_ar, $start_time, $end_time, $tbl_school_id);
	}


	/**
	* @desc    Edit class session
	* @param   none
	* @access  default
	*/
    function edit_class_session() {
		$data['page'] = "view_class_session";
		$data['menu'] = "classes";
		$data['sub_menu'] = "class_sessions";
		$data['mid'] = "3";
		$this->load->model("model_class_sessions");
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_class_id = 0;
		if (array_key_exists('class_sessions_id_enc',$param_array)) {
			$tbl_class_session_id = $param_array['class_sessions_id_enc'];
		}
		
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$classesObj = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
		$data['classes_list'] = $classesObj;

		//Class grade details
		$class_sessions_obj = $this->model_class_sessions->get_class_session_obj($tbl_class_session_id);
		$data['class_sessions_obj'] = $class_sessions_obj;
		$this->load->view('admin/view_template', $data);
	}

  // END CLASS SESSIONS
  
  //START SEMESTERS
	/**
	* @desc    Show all SEMESTERS
	*
	* @param   none
	* @access  default
	*/
    function semester_conf() {
		$data['page'] = "view_semester";
		$data['menu'] = "configuration";
        $data['sub_menu'] = "semester_conf";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "added_date";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "title";
					 break;
				}
				default: {
					$sort_name = "title";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_category_id = "";
		if (array_key_exists('tbl_class_sessions_id',$param_array)) {
			$tbl_class_sessions_id = $param_array['tbl_class_sessions_id'];
			$data['tbl_sel_class_sessions_id'] = $tbl_class_sessions_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_classes");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  
		    $rs_academic_year    = $this->model_classes->get_academic_year($tbl_school_id);
			$data['rs_academic_year'] = $rs_academic_year;
			$rs_all_semesters    = $this->model_classes->get_all_semesters($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id);
			$total_semesters     = $this->model_classes->get_total_semesters($q, $is_active, $tbl_school_id);
		}else{
			$rs_all_semesters     = array();
			$total_semesters      = array();
		}
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/classes/semester_conf";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_class_id) && trim($tbl_class_id)!="") {
			$page_url .= "/tbl_class_session_id/".rawurlencode($tbl_class_session_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_semesters;
		$config['per_page'] = TBL_SEMESTER_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_SEMESTER_PAGING >= $total_semesters) {
			$range = $total_semesters;
		} else {
			$range = $offset+TBL_SEMESTER_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_semesters semesters</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_semesters']	     = $rs_all_semesters;
		$data['total_semesters'] 	      = $total_semesters;

		$this->load->view('admin/view_template',$data);
	}
	
	function is_exist_semester() {
		if ($_POST) {
			$tbl_semester_id = $_POST['tbl_semester_id'];
			$title = $_POST['title'];
			$start_date = date("Y-m-d", strtotime($_POST['start_date']));
			$end_date = date("Y-m-d", strtotime($_POST['end_date']));
		}
		
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		
		$this->load->model("model_classes");		
		$results = $this->model_classes->is_exist_semester($tbl_semester_id, $title, $start_date, $end_date, $tbl_school_id);
		echo $results;
	}
	
    /**
	* @desc    Create Semester
	* @param   POST array
	* @access  default
	*/
    function create_semester() {
		if ($_POST) {
			$tbl_semester_id = $_POST['tbl_semester_id'];
			$title = $_POST['title'];
			$title_ar = $_POST['title_ar'];
			$tbl_academic_year_id = $_POST['tbl_academic_year_id'];
			$start_date = date("Y-m-d", strtotime($_POST['start_date']));
			$end_date = date("Y-m-d", strtotime($_POST['end_date']));
		}
		
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");
		$resData = $this->model_classes->create_semester($tbl_semester_id, $title, $title_ar, $tbl_academic_year_id, $start_date, $end_date, $tbl_school_id);
		echo "Y";
	}

    /**
	* @desc    Activate Semester
	* @param   String session_id_enc
	* @access  default
	*/
    function activateSemester() {
		if(isset($_POST) && count($_POST) != 0) {
			$semester_id_enc = trim($_POST["semester_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");		
		$this->model_classes->activate_semester($semester_id_enc,$tbl_school_id);
	}


	/**
	* @desc    Deactivate Semester
	* @param   String session_id_enc
	* @access  default
	*/
    function deactivateSemester() {
		if(isset($_POST) && count($_POST) != 0) {
			$semester_id_enc = trim($_POST["semester_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");		
		$data = $this->model_classes->deactivate_semester($semester_id_enc,$tbl_school_id);
		
	}

     /**
	* @desc    Delete Semester
	* @param   POST array
	* @access  default
	*/
     
	function deleteSemester() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['semester_id_enc']; //e.g."             school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('semester_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_classes->delete_semester($str[$i],$tbl_school_id);
		}
	}
	
	/**
	* @desc    Save changes
	* @param   POST array
	* @access  default
	*/
    function save_semester_changes() {
		if ($_POST) {
			$tbl_semester_id       = $_POST['tbl_semester_id'];
			$title         	     = $_POST['title'];
			$title_ar         	  = $_POST['title_ar'];
			$tbl_academic_year_id  = $_POST['tbl_academic_year_id'];
			$start_date     	    = date("Y-m-d", strtotime($_POST['start_date']));
			$end_date     	      = date("Y-m-d", strtotime($_POST['end_date']));
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");			
		$this->model_classes->save_semester_changes($tbl_semester_id, $title, $title_ar, $tbl_academic_year_id, $start_date, $end_date, $tbl_school_id);
	}


	/**
	* @desc    Edit Semester
	* @param   none
	* @access  default
	*/
    function edit_semester() {
		$data['page'] = "view_semester";
		$data['menu'] = "configuration";
        $data['sub_menu'] = "semester_conf";
		$data['mid'] = "3";
		$this->load->model("model_classes");
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_class_id = 0;
		if (array_key_exists('semester_id_enc',$param_array)) {
			$tbl_semester_id = $param_array['semester_id_enc'];
		}
		
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		
		$rs_academic_year = $this->model_classes->get_academic_year($tbl_school_id);
		$data['rs_academic_year'] = $rs_academic_year;

		$class_semester_obj = $this->model_classes->get_semester_obj($tbl_semester_id);
		$data['class_semester_obj'] = $class_semester_obj;
		$this->load->view('admin/view_template', $data);
	}
    //END SEMESTERS
  
    //START ACADEMIC YEAR
	/**
	* @desc    Show all academic years
	*
	* @param   none
	* @access  default
	*/
    function academic_year_conf() {
		$data['page'] = "view_academic_year";
		$data['menu'] = "configuration";
        $data['sub_menu'] = "semester_conf";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "added_date";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "title";
					 break;
				}
				default: {
					$sort_name = "title";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_category_id = "";
		if (array_key_exists('tbl_class_sessions_id',$param_array)) {
			$tbl_class_sessions_id = $param_array['tbl_class_sessions_id'];
			$data['tbl_sel_class_sessions_id'] = $tbl_class_sessions_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_classes");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  

			$rs_all_academic_years    = $this->model_classes->get_all_academic_year($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id);
			$total_academic_years     = $this->model_classes->get_total_academic_year($q, $is_active, $tbl_school_id);
		}else{
			$rs_all_academic_years     = array();
			$total_academic_years      = array();
		}
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/classes/academic_year_conf";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_class_id) && trim($tbl_class_id)!="") {
			$page_url .= "/tbl_class_session_id/".rawurlencode($tbl_class_session_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url']    = $page_url;
		$config['total_rows']  = $total_academic_years;
		$config['per_page']    = TBL_SEMESTER_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_SEMESTER_PAGING >= $total_academic_years) {
			$range = $total_academic_years;
		} else {
			$range = $offset+TBL_SEMESTER_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_academic_years academic years</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_academic_years']	     = $rs_all_academic_years;
		$data['total_academic_years'] 	      = $total_academic_years;

		$this->load->view('admin/view_template',$data);
	}
	
	function is_exist_academic_year() {
		if ($_POST) {
			$tbl_academic_year_id  = $_POST['tbl_academic_year_id'];
			$academic_start        = $_POST['academic_start'];
			$academic_end     	  = $_POST['academic_end'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");		
		$results = $this->model_classes->is_exist_academic_year($tbl_academic_year_id, $academic_start, $academic_end, $tbl_school_id);
		echo $results;
	
	}
	
    /**
	* @desc    Create ACADEMIC YEAR
	* @param   POST array
	* @access  default
	*/
    function create_academic_year() {
		if ($_POST) {
		    $tbl_academic_year_id  = $_POST['tbl_academic_year_id'];
			$academic_start        = $_POST['academic_start'];
			$academic_end     	  = $_POST['academic_end'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");
		$resData = $this->model_classes->create_academic_year($tbl_academic_year_id, $academic_start, $academic_end, $tbl_school_id);
		echo "Y";
	}

    /**
	* @desc    Activate ACADEMIC YEAR
	* @param   String session_id_enc
	* @access  default
	*/
    function activateAcademicYear() {
		if(isset($_POST) && count($_POST) != 0) {
			$academic_year_id_enc = trim($_POST["academic_year_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");		
		$this->model_classes->activate_academic_year($academic_year_id_enc,$tbl_school_id);
	}


	/**
	* @desc    Deactivate ACADEMIC YEAR
	* @param   String session_id_enc
	* @access  default
	*/
    function deactivateAcademicYear() {
		if(isset($_POST) && count($_POST) != 0) {
			$academic_year_id_enc = trim($_POST["academic_year_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");		
		$data = $this->model_classes->deactivate_academic_year($academic_year_id_enc,$tbl_school_id);
		
	}

     /**
	* @desc    Delete ACADEMIC YEAR
	* @param   POST array
	* @access  default
	*/
     
	function deleteAcademicYear() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['academic_year_id_enc']; //e.g."             school_type_id_enc=f31981d6285a6a958f754774013472f7&school_type_id_enc=d99f9fee0ed779292475c&school_type_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('academic_year_id_enc=', '', $str);
			$str = explode("&", $str);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");	
		for ($i=0; $i<count($str); $i++) {
			$this->model_classes->delete_academic_year($str[$i],$tbl_school_id);
		}
	}
	
	/**
	* @desc    Save changes
	* @param   POST array
	* @access  default
	*/
    function save_academic_year_changes() {
		if ($_POST) {
			$tbl_academic_year_id  = $_POST['tbl_academic_year_id'];
			$academic_start        = $_POST['academic_start'];
			$academic_end     	  = $_POST['academic_end'];
		}
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");			
		$this->model_classes->save_academic_year_changes($tbl_academic_year_id, $academic_start, $academic_end, $tbl_school_id);
	}


	/**
	* @desc    Edit ACADEMIC YEAR
	* @param   none
	* @access  default
	*/
    function edit_academic_year() {
		$data['page'] = "view_academic_year";
		$data['menu'] = "configuration";
        $data['sub_menu'] = "semester_conf";
		
		$data['mid'] = "3";
		$this->load->model("model_classes");
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_class_id = 0;
		if (array_key_exists('academic_year_id_enc',$param_array)) {
			$tbl_academic_year_id = $param_array['academic_year_id_enc'];
		}
		
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		
		$rs_academic_year    = $this->model_classes->get_academic_year_obj($tbl_academic_year_id);
		$data['academic_year_obj'] = $rs_academic_year;

		$this->load->view('admin/view_template', $data);
	}
  //END ACADEMIC YEAR
  
  // UPDATE CLASS SORT ORDER
  
    function update_sort_order() {
		if(isset($_POST) && count($_POST) != 0) {
			$class_id_enc = trim($_POST["class_id_enc"]);
			$sortVal      = trim($_POST["sortVal"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
		$this->load->model("model_classes");		
		$this->model_classes->update_sort_order($sortVal, $class_id_enc,$tbl_school_id);
	}

  
  //END UPDATE CLASS SORT ORDER
  
	
}
?>