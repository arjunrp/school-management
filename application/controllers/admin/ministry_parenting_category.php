<?php

/**
 * @desc   	  	Ministry Parenting Category Controller
 *
 * @category   	Controller
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Ministry_parenting_category extends CI_Controller {


	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
    function index() {
		$this->all_ministry_parenting_categorys();
	}


	/**
	* @desc    Activate job category
	*
	* @param   String tbl_parenting_cat_id
	* @access  default
	*/
    function activate() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_parenting_cat_id = trim($_POST["parenting_cat_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		
		$this->load->model("model_ministry_parenting_category");		
		$this->model_ministry_parenting_category->activate($tbl_parenting_cat_id);
	}


	/**
	* @desc    Deactivate job category
	*
	* @param   String tbl_parenting_cat_id
	* @access  default
	*/
    function deactivate() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_parenting_cat_id = trim($_POST["parenting_cat_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		
		$this->load->model("model_ministry_parenting_category");		
		$this->model_ministry_parenting_category->deactivate($tbl_parenting_cat_id);
	}


	/**
	* @desc    Delete job category
	*
	* @param   CSV tbl_parenting_cat_id
	* @access  default
	*/
    function delete() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['parenting_cat_id_enc']; //e.g." parenting_cat_id_enc=f31981d6285a6a958f754774013472f7&parenting_cat_id_enc=d99f9fee0ed779292475c&parenting_cat_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('parenting_cat_id_enc=', '', $str);
			$str = explode("&", $str);

			$is_ajax = trim($_POST["is_ajax"]);
		} 
		
		$this->load->model("model_ministry_parenting_category");		
		
		for ($i=0; $i<count($str); $i++) {
			$this->model_ministry_parenting_category->delete($str[$i]);
		}
	}


	/**
	* @desc    Show all ministry parenting categorys
	*
	* @param   none
	* @access  default
	*/
    function all_ministry_parenting_categorys() {
		$data['page'] = "view_ministry_parenting_category";
		$data['menu'] = "parenting";

		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		$sort_name = "title_en";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_ministry_parenting_categorys = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "title_en";
					 break;
				}
				default: {
					$sort_name = "title_en";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_ministry_parenting_category");
		
		$is_active = "";
		$rs_all_ministry_parenting_categorys = $this->model_ministry_parenting_category->get_all_ministry_parenting_categorys($sort_name, $sort_by, $offset, $q, $is_active);
		$total_ministry_parenting_categorys = $this->model_ministry_parenting_category->get_total_ministry_parenting_categorys($q, $is_active);

		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/ministry_parenting_category/all_ministry_parenting_categorys";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_ministry_parenting_categorys;
		$config['per_page'] = TBL_PARENTING_CAT_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_PARENTING_CAT_PAGING >= $total_ministry_parenting_categorys) {
			$range = $total_ministry_parenting_categorys;
		} else {
			$range = $offset+TBL_PARENTING_CAT_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_ministry_parenting_categorys category(s)</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_ministry_parenting_categorys'] = $rs_all_ministry_parenting_categorys;
		$data['total_ministry_parenting_categorys'] = $total_ministry_parenting_categorys;

		$this->load->view('admin/view_ministry_template',$data);
	}


	/**
	* @desc    Check if job category already exists
	*
	* @param   POST array
	* @access  default
	*/
    function is_exist() {
		if ($_POST) {
			$tbl_parenting_cat_id = $_POST['parenting_cat_id_enc'];		
			$title_en = $_POST['title_en'];
			$title_ar = $_POST['title_ar'];
		}
		$this->load->model("model_ministry_parenting_category");		
		$results = $this->model_ministry_parenting_category->is_exist($tbl_parenting_cat_id, $title_en, $title_ar);

		if (count($results)>0) {
			echo "*Y*";
		} else {
			echo "*N*";
		}
	}


	/**
	* @desc    Create job category
	*
	* @param   POST array
	* @access  default
	*/
    function create_ministry_parenting_category() {
		if ($_POST) {
			$tbl_parenting_cat_id = $_POST['parenting_cat_id_enc'];		
			$title_en = $_POST['title_en'];
			$title_ar = $_POST['title_ar'];
		}
		$this->load->model("model_ministry_parenting_category");		
		$this->model_ministry_parenting_category->create_ministry_parenting_category($tbl_parenting_cat_id, $title_en, $title_ar);
	}


	/**
	* @desc    Save changes
	*
	* @param   POST array
	* @access  default
	*/
    function save_changes() {
		if ($_POST) {
			$tbl_parenting_cat_id = $_POST['parenting_cat_id_enc'];		
			$title_en = $_POST['title_en'];
			$title_ar = $_POST['title_ar'];
		}
		$this->load->model("model_ministry_parenting_category");		
		$this->model_ministry_parenting_category->save_changes($tbl_parenting_cat_id, $title_en, $title_ar);
	}


	/**
	* @desc    Edit job ministry parenting categorys
	*
	* @param   none
	* @access  default
	*/
    function edit_ministry_parenting_category() {
		$data['page'] = "view_ministry_parenting_category";
		$data['menu'] = "parenting";
		$data['mid'] = "3";

		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_parenting_cat_id = 0;
					
		if (array_key_exists('parenting_cat_id_enc',$param_array)) {
			$tbl_parenting_cat_id = $param_array['parenting_cat_id_enc'];
		}	 
		
		//Ministry_parenting_category details
		$this->load->model("model_ministry_parenting_category");		
		$ministry_parenting_category_obj = $this->model_ministry_parenting_category->get_ministry_parenting_category_obj($tbl_parenting_cat_id);
		$data['ministry_parenting_category_obj'] = $ministry_parenting_category_obj;
		
		$this->load->view('admin/view_ministry_template', $data);
	}


	/**
	* @desc    Get all ministry parenting categorys
	*
	* @param   none
	* @access  default
	*/
    function ajax_all_ministry_parenting_categorys() {
		$data['page'] = "ajax_all_ministry_parenting_categorys";

		$lan = $_POST['lan'];
		$data['lan'] = $lan;
		
		$this->load->model("model_ministry_parenting_category");		
		$rs_ministry_parenting_category = $this->model_ministry_parenting_category->get_all_ministry_parenting_categorys('title_en', 'ASC', '', '', 'Y');
		
		$data['rs_ministry_parenting_category'] = $rs_ministry_parenting_category;
		$this->load->view('admin/ajax_all_ministry_parenting_categorys', $data);
	}
	
}
?>