<?php

/**
 * @desc   	  	Admin User Controller
 * @category   	Controller
 * @author     	Shanavas.PK
 * @version    	0.1
 */
class Admin_user extends CI_Controller {


	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
    function index() {
		$this->login_form();
	}


	/**
	* @desc    Login page
	*
	* @param   none
	* @access  default
	*/
    function login_form() {
		$data['page'] = "login_form";
		$data['user_type'] = "admin";
		$this->load->view("admin/login_form", $data);
	}


	/**
	* @desc    Validate user credentials and generate session
	*
	* @param   none
	* @access  default
	*/
    function ajax_validate_and_login_user() {
		if(isset($_POST) && count($_POST) != 0) {
			$username = trim($_POST["username"]);
			$password = trim($_POST["password"]);
			$remember_me = trim($_POST["remember_me"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} else {
			echo "*N*";	
		   exit;
		}

		if (trim($remember_me) == "Y") {
			$year = time() + 31536000;
			setcookie('remember_me', $username, $year);	
		} else {
			if(isset($_COOKIE['remember_me'])) {
				$past = time() - 100;
				setcookie('remember_me', '', $past);
			}
		}
		
		$module_access = "S";
        $this->load->model('model_admin');
        $is_user_status = $this->model_admin->authenticate($username, $password, $module_access);
		if($is_user_status=="D")
		{
			 $module_access  = "G";
			 $is_user_status = $this->model_admin->authenticate($username, $password, $module_access);
		}
		if($is_user_status=="D")
		{
			 $module_access  = "T";
			 $is_user_status = $this->model_admin->authenticate($username, $password, $module_access);
		}
		
		
		switch($is_user_status) {
			case("N"): {//User does not exist
				echo "*N*";
				exit;
				}
			case("Y"): {
					$admin_user_obj = $this->model_admin->get_admin_info($username);
					
					//Create user sessions
					$_SESSION['aqdar_smartcare']['tbl_admin_id_sess']      = $admin_user_obj[0]['tbl_admin_id'];
					$_SESSION['aqdar_smartcare']['tbl_admin_user_id_sess'] = $admin_user_obj[0]['user_id'];
					$_SESSION['aqdar_smartcare']['admin_email_sess']       = $admin_user_obj[0]['email'];
					$_SESSION['aqdar_smartcare']['admin_first_name_sess']  = $admin_user_obj[0]['first_name'];
					$_SESSION['aqdar_smartcare']['admin_last_name_sess']   = $admin_user_obj[0]['last_name'];
					$_SESSION['aqdar_smartcare']['tbl_school_id_sess']     = $admin_user_obj[0]['tbl_school_id'];
					$_SESSION['aqdar_smartcare']['added_date_sess']        = date('M d, Y', strtotime($admin_user_obj[0]['reg_date']));
					
					$_SESSION['tbl_admin_id_sess']      = $admin_user_obj[0]['tbl_admin_id'];
					$_SESSION['tbl_admin_user_id_sess'] = $admin_user_obj[0]['user_id'];
					$_SESSION['admin_email_sess']       = $admin_user_obj[0]['email'];
					$_SESSION['admin_first_name_sess']  = $admin_user_obj[0]['first_name'];
					$_SESSION['admin_last_name_sess']   = $admin_user_obj[0]['last_name'];
					$_SESSION['tbl_school_id_sess']     = $admin_user_obj[0]['tbl_school_id'];
					$_SESSION['added_date_sess']        = date('M d, Y', strtotime($admin_user_obj[0]['reg_date']));
					
					if($module_access == "G")
					{ 
						$_SESSION['aqdar_smartcare']['user_type_sess']         = "ministry";
					}else if( $module_access == "T" ) {
						$_SESSION['aqdar_smartcare']['user_type_sess']         = "timeline";
					}else{
						$_SESSION['aqdar_smartcare']['user_type_sess']         = "admin";
					}
					$_SESSION['aqdar_smartcare']['admin_auth_sess']        =  $admin_user_obj[0]['user_type'];
					$_SESSION['aqdar_smartcare']['module_access']          =  $admin_user_obj[0]['module_access'];
					
					
					
					if($admin_user_obj[0]['module_access']=="S" || $admin_user_obj[0]['module_access']=="T" )
					{
						$admin_access_obj = $this->model_admin->getSchoolModuleAccess($admin_user_obj[0]['tbl_admin_id']);
						
						for($k=0;$k<count($admin_access_obj);$k++)
						{
							$label = $admin_access_obj[$k]["module_key"];
							$_SESSION[$admin_access_obj[$k]["module_key"]] = "Y";
							
						}
						
					}
					
					$this->load->model("model_file_mgmt");		
		            $file_mgmt_obj = $this->model_file_mgmt->get_file_mgmt_obj($admin_user_obj[0]['tbl_admin_id']);
					if (count($file_mgmt_obj)>0) {
						$tbl_uploads_id = $file_mgmt_obj['tbl_uploads_id'];	
						$img_url = HOST_URL."/admin/uploads/".$file_mgmt_obj['file_name_updated'];	
						$_SESSION['aqdar_smartcare']['admin_pic']          = $img_url;
					}
				
					
					//Update last login time
					$this->model_admin->update_last_login($admin_user_obj[0]['tbl_admin_id']);
					
					if($admin_user_obj[0]['module_access']=="S")
					{
						$this->load->model('model_school');
						$school_information = $this->model_school->get_school_details($admin_user_obj[0]['tbl_school_id'],LAN_SEL);
						$_SESSION['aqdar_smartcare']['school_name']    = $school_information[0]['school_name'];
						$_SESSION['aqdar_smartcare']['school_name_ar'] = $school_information[0]['school_name_ar'];
						$_SESSION['aqdar_smartcare']['school_logo']    = $school_information[0]['logo'];
						$_SESSION['aqdar_smartcare']['school_type']    = $school_information[0]['school_type'];
					}else if($admin_user_obj[0]['module_access']=="T")
					{
						$_SESSION['aqdar_smartcare']['school_name']    = $admin_user_obj[0]['first_name']." ".$admin_user_obj[0]['last_name'];
						$_SESSION['aqdar_smartcare']['school_logo']    = $img_url;
						$_SESSION['aqdar_smartcare']['admin_pic']      = $img_url;
					}else{
						
						$_SESSION['aqdar_smartcare']['school_name']    = '';
						$_SESSION['aqdar_smartcare']['school_name_ar'] = '';
						$_SESSION['aqdar_smartcare']['school_logo']    = '';
						$_SESSION['aqdar_smartcare']['school_type']    = '';
					}
					
				echo "*Y*";
				exit;
				}
		}
	echo "*N*";
	exit;
	}

   // ADMIN USERS LIST START

   /**
	* @desc    Show all users
	* @param   none
	* @access  default
	*/
    function all_users() {
	
		
		$data['page'] = "view_admin_user";
		$data['menu'] = "admin";
        $data['sub_menu'] = "admin_users";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "D";
		$sort_name = "reg_date";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_users = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name";
					 break;
				}
				case("D"): {
					$sort_name = "reg_date";
					 break;
				}
				default: {
					$sort_name = "first_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
		$this->load->model("model_admin");
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];      
		$is_active = "";
		$rs_all_users = $this->model_admin->get_all_users($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id);
		$total_users = $this->model_admin->get_total_users($q, $is_active, $tbl_school_id);

		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/admin_user/all_users";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_users;
		$config['per_page'] = TBL_ADMIN_USERS_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_ADMIN_USERS_PAGING >= $total_users) {
			$range = $total_users;
		} else {
			$range = $offset+TBL_ADMIN_USERS_PAGING;
		}
        if(LAN_SEL=="ar")
		    $paging_string = "$range - $start <font color='#333'>من $total_users المستخدمين </font>&nbsp;";
		else
			$paging_string = "$start - $range <font color='#333'>of $total_users users</font>&nbsp;";
			
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_users']	= $rs_all_users;
		$data['total_users'] 	= $total_users;
		$this->load->view('admin/view_template',$data);
	}



/**
	* @desc    Activate user
	*
	* @param   String tbl_admin_user_id
	* @access  default
	*/
    function activate() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_admin_user_id = trim($_POST["admin_user_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		
		$this->load->model("model_admin");		
		$this->model_admin->activate($tbl_admin_user_id);
	}



/**
	* @desc    Deactivate user
	*
	* @param   String tbl_admin_user_id
	* @access  default
	*/
    function deactivate() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_admin_user_id = trim($_POST["admin_user_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		
		$this->load->model("model_admin");		
		$this->model_admin->deactivate($tbl_admin_user_id);
	}


	/**
	* @desc    Delete user
	*
	* @param   CSV tbl_admin_user_id
	* @access  default
	*/
    function delete() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['admin_user_id_enc']; //e.g." admin_user_id_enc=f31981d6285a6a958f754774013472f7&admin_user_id_enc=d99f9fee0ed779292475c&admin_user_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('admin_user_id_enc=', '', $str);
			$str = explode("&", $str);

			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$this->load->model("model_admin");	
	
		for ($i=0; $i<count($str); $i++) {
			$this->model_admin->delete($str[$i]);
		}
	}


  	/**
	* @desc    Create user
	*
	* @param   POST array
	* @access  default
	*/
    function create_user() {
		if ($_POST) {
			$tbl_admin_user_id = $_POST['admin_user_id_enc'];		
			$first_name 		= $_POST['first_name'];			
			$last_name 		 = $_POST['last_name'];			
			$dob 			   = $_POST['dob'];			
			$gender            = $_POST['gender'];			
			$mobile            = $_POST['mobile'];			
			$email             = $_POST['email'];
			$username          = $_POST['username'];				
			$password          = $_POST['password'];
		}
		
		$this->load->model("model_admin");		
		$data = $this->model_admin->create_user($tbl_admin_user_id, $first_name, $last_name, $dob, $gender, $mobile, $email, $username, $password);
		echo $data;
		exit;
		
	}

	/**
	* @desc    Save changes
	*
	* @param   POST array
	* @access  default
	*/
    function save_changes() {
		if ($_POST) {
			$tbl_admin_user_id = $_POST['admin_user_id_enc'];		
			$first_name = $_POST['first_name'];			
			$last_name = $_POST['last_name'];			
			$dob = $_POST['dob'];			
			$gender = $_POST['gender'];			
			$mobile = $_POST['mobile'];			
			$email= $_POST['email'];			
			$password = $_POST['password'];			
		}
		$this->load->model("model_admin");		
		$this->model_admin->save_changes($tbl_admin_user_id, $first_name, $last_name, $dob, $gender, $mobile, $email, $password);
	}

	/**
	* @desc    Edit user
	*
	* @param   none
	* @access  default
	*/
    function edit_user() {
		$data['page'] = "view_admin_user";
		$data['menu'] = "admin";
		$data['sub_menu'] = "admin_users";
		$data['mid'] = "3";

		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_admin_user_id = 0;
					
		if (array_key_exists('admin_user_id_enc',$param_array)) {
			$tbl_admin_user_id = $param_array['admin_user_id_enc'];
		}	 
		
		//User details
		$this->load->model("model_admin");		
		$admin_user_obj = $this->model_admin->get_admin_user_obj($tbl_admin_user_id);
		$data['admin_user_obj'] = $admin_user_obj;
		
		//User picture
		$tbl_uploads_id = "";
		$img_url = "";
		$this->load->model("model_file_mgmt");		
		$file_mgmt_obj = $this->model_file_mgmt->get_file_mgmt_obj($tbl_admin_user_id);

		if (count($file_mgmt_obj)>0) {
			$tbl_uploads_id = $file_mgmt_obj['tbl_uploads_id'];	
			$img_url = HOST_URL."/admin/uploads/".$file_mgmt_obj['file_name_updated'];	

			$data['tbl_uploads_id'] = $tbl_uploads_id;
			$data['img_url'] = $img_url;
		}
		$this->load->view('admin/view_template', $data);
	}
	
	/**
	* @desc    Student details
	* @param   none
	* @access  default
	*/
    function show_details() {
		$data['page'] = "view_admin_user";
		$data['menu'] = "admin";
		$data['sub_menu'] = "admin_users";
		$data['mid'] = "4";//Details

		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_user_id = 0;
					
		if (array_key_exists('admin_user_id_enc',$param_array)) {
			$tbl_admin_user_id = $param_array['admin_user_id_enc'];
			$data['tbl_admin_user_id'] = $tbl_admin_user_id;
		}	 
		
		//User details
		$this->load->model("model_admin");		
		$admin_user_obj = $this->model_admin->get_admin_user_obj($tbl_admin_user_id);
		$data['admin_user_obj'] = $admin_user_obj;
		
		$this->load->view('admin/view_template', $data);
	}


 // END ADMIN USER SECTION

 // START ADMIN USER RIGHTS
 
 	/**
	* @desc    Save changes
	* @param   POST array
	* @access  default
	*/
    function save_admin_rights() {
		$this->load->model("model_admin");

		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['admin_users_allowed']; //e.g." admin_user_id_enc=f31981d6285a6a958f754774013472f7&admin_user_id_enc=d99f9fee0ed779292475c&admin_user_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('admin_users_allowed=', '', $str);
			$is_ajax = trim($_POST["is_ajax"]);
			$tbl_admin_user_id = $_POST['tbl_admin_user_id'];	
			$str = explode("&", $str);
		} 
		
	    $this->model_admin->del_admin_rights($tbl_admin_user_id);
		for ($m=0; $m<count($str); $m++) {
		  $this->model_admin->save_admin_rights($str[$m],$tbl_admin_user_id);	
		}
		echo "Y";
		exit;
	}


	/**
	* @desc    Edit user
	* @param   none
	* @access  default
	*/
    function user_rights() {
		$data['page'] = "view_user_rights";
		$data['menu'] = "admin";
		$data['sub_menu'] = "admin_users";
		$data['mid'] = "3";

		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_admin_user_id = 0;
					
		if (array_key_exists('admin_user_id_enc',$param_array)) {
			$tbl_admin_user_id = $param_array['admin_user_id_enc'];
			
		}
		$data['admin_user_id_enc'] = $tbl_admin_user_id;
		// 'S' for school
		//User details
		$this->load->model("model_admin");	
		$backend_modules_rights  = $this->model_admin->get_backend_modules_rights($tbl_admin_user_id,'S');
		$data['backend_modules_rights'] = $backend_modules_rights;
		$this->load->view('admin/view_template', $data);
	}
	

 
 //END ADMIN USER RIGHTS
 
 
//MINISTRY ADMIN USERS

 // ADMIN USERS LIST START

   /**
	* @desc    Show all ministry users
	* @param   none
	* @access  default
	*/
    function all_ministry_users() {
		
		$data['page'] = "view_ministry_admin_user";
		$data['menu'] = "admin";
        $data['sub_menu'] = "admin_users";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "D";
		$sort_name = "reg_date";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_users = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name";
					 break;
				}
				case("D"): {
					$sort_name = "reg_date";
					 break;
				}
				default: {
					$sort_name = "first_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
		$this->load->model("model_admin");
		$is_active = "";
        $rs_all_users = $this->model_admin->get_all_ministry_users($sort_name, $sort_by, $offset, $q, $is_active);
	    $total_users = $this->model_admin->get_total_ministry_users($q, $is_active);

		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/admin_user/all_ministry_users";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_users;
		$config['per_page'] = TBL_ADMIN_USERS_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_ADMIN_USERS_PAGING >= $total_users) {
			$range = $total_users;
		} else {
			$range = $offset+TBL_ADMIN_USERS_PAGING;
		}
        if(LAN_SEL=="ar")
		    $paging_string = "$range - $start <font color='#333'>من $total_users المستخدمين </font>&nbsp;";
		else
			$paging_string = "$start - $range <font color='#333'>of $total_users users</font>&nbsp;";
			
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_users']	= $rs_all_users;
		$data['total_users'] 	= $total_users;
		$this->load->view('admin/view_ministry_template',$data);
	}



/**
	* @desc    Activate user
	*
	* @param   String tbl_admin_user_id
	* @access  default
	*/
    function activate_ministry() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_admin_user_id = trim($_POST["admin_user_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		
		$this->load->model("model_admin");		
		$this->model_admin->activate_ministry($tbl_admin_user_id);
	}



/**
	* @desc    Deactivate user
	*
	* @param   String tbl_admin_user_id
	* @access  default
	*/
    function deactivate_ministry() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_admin_user_id = trim($_POST["admin_user_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		
		$this->load->model("model_admin");		
		$this->model_admin->deactivate_ministry($tbl_admin_user_id);
	}


	/**
	* @desc    Delete user
	*
	* @param   CSV tbl_admin_user_id
	* @access  default
	*/
    function delete_ministry() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['admin_user_id_enc']; //e.g." admin_user_id_enc=f31981d6285a6a958f754774013472f7&admin_user_id_enc=d99f9fee0ed779292475c&admin_user_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('admin_user_id_enc=', '', $str);
			$str = explode("&", $str);

			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$this->load->model("model_admin");		
		for ($i=0; $i<count($str); $i++) {
			$this->model_admin->delete_ministry($str[$i]);
		}
	}


  	/**
	* @desc    Create user
	*
	* @param   POST array
	* @access  default
	*/
    function create_ministry_user() {
		if ($_POST) {
			$tbl_admin_user_id = $_POST['admin_user_id_enc'];		
			$first_name 		= $_POST['first_name'];			
			$last_name 		 = $_POST['last_name'];			
			$dob 			   = $_POST['dob'];			
			$gender            = $_POST['gender'];			
			$mobile            = $_POST['mobile'];			
			$email             = $_POST['email'];
			$username          = $_POST['username'];				
			$password          = $_POST['password'];
		}
		
		$this->load->model("model_admin");		
		$data = $this->model_admin->create_ministry_user($tbl_admin_user_id, $first_name, $last_name, $dob, $gender, $mobile, $email, $username, $password);
		echo $data;
		exit;
		
	}

	/**
	* @desc    Save changes
	*
	* @param   POST array
	* @access  default
	*/
    function save_ministry_changes() {
		if ($_POST) {
			$tbl_admin_user_id = $_POST['admin_user_id_enc'];		
			$first_name = $_POST['first_name'];			
			$last_name = $_POST['last_name'];			
			$dob = $_POST['dob'];			
			$gender = $_POST['gender'];			
			$mobile = $_POST['mobile'];			
			$email= $_POST['email'];			
			$password = $_POST['password'];			
		}
		$this->load->model("model_admin");		
		$this->model_admin->save_ministry_changes($tbl_admin_user_id, $first_name, $last_name, $dob, $gender, $mobile, $email, $password);
	}

	/**
	* @desc    Edit user
	*
	* @param   none
	* @access  default
	*/
    function edit_ministry_user() {
		$data['page'] = "view_ministry_admin_user";
		$data['menu'] = "admin";
		$data['sub_menu'] = "admin_users";
		$data['mid'] = "3";

		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_admin_user_id = 0;
					
		if (array_key_exists('admin_user_id_enc',$param_array)) {
			$tbl_admin_user_id = $param_array['admin_user_id_enc'];
		}	 
		
		//User details
		$this->load->model("model_admin");		
		$admin_user_obj = $this->model_admin->get_admin_user_obj($tbl_admin_user_id);
		$data['admin_user_obj'] = $admin_user_obj;
		
		//User picture
		$tbl_uploads_id = "";
		$img_url = "";
		$this->load->model("model_file_mgmt");		
		$file_mgmt_obj = $this->model_file_mgmt->get_file_mgmt_obj($tbl_admin_user_id);

		if (count($file_mgmt_obj)>0) {
			$tbl_uploads_id = $file_mgmt_obj['tbl_uploads_id'];	
			$img_url = HOST_URL."/admin/uploads/".$file_mgmt_obj['file_name_updated'];	

			$data['tbl_uploads_id'] = $tbl_uploads_id;
			$data['img_url'] = $img_url;
		}
		$this->load->view('admin/view_ministry_template', $data);
	}
	
	/**
	* @desc    Student details
	* @param   none
	* @access  default
	*/
    function show_ministry_details() {
		$data['page'] = "view_ministry_admin_user";
		$data['menu'] = "admin";
		$data['sub_menu'] = "admin_users";
		$data['mid'] = "4";//Details

		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_user_id = 0;
					
		if (array_key_exists('admin_user_id_enc',$param_array)) {
			$tbl_admin_user_id = $param_array['admin_user_id_enc'];
			$data['tbl_admin_user_id'] = $tbl_admin_user_id;
		}	 
		
		//User details
		$this->load->model("model_admin");		
		$admin_user_obj = $this->model_admin->get_admin_user_obj($tbl_admin_user_id);
		$data['admin_user_obj'] = $admin_user_obj;
		
		$this->load->view('admin/view_ministry_template', $data);
	}


 // END ADMIN USER SECTION

 // START ADMIN USER RIGHTS
 
 	/**
	* @desc    Save changes
	* @param   POST array
	* @access  default
	*/
    function save_ministry_admin_rights() {
		$this->load->model("model_admin");

		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['admin_users_allowed']; //e.g." admin_user_id_enc=f31981d6285a6a958f754774013472f7&admin_user_id_enc=d99f9fee0ed779292475c&admin_user_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('admin_users_allowed=', '', $str);
			$is_ajax = trim($_POST["is_ajax"]);
			$tbl_admin_user_id = $_POST['tbl_admin_user_id'];	
			$str = explode("&", $str);
		} 
		
	    $this->model_admin->del_ministry_admin_rights($tbl_admin_user_id);
		for ($m=0; $m<count($str); $m++) {
		  $this->model_admin->save_ministry_admin_rights($str[$m],$tbl_admin_user_id);	
		}
		echo "Y";
		exit;
	}


	/**
	* @desc    Edit ministry user rights
	* @param   none
	* @access  default
	*/
    function ministry_user_rights() {
		$data['page'] = "view_ministry_user_rights";
		$data['menu'] = "admin";
		$data['sub_menu'] = "admin_users";
		$data['mid'] = "3";

		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_admin_user_id = 0;
					
		if (array_key_exists('admin_user_id_enc',$param_array)) {
			$tbl_admin_user_id = $param_array['admin_user_id_enc'];
			
		}
		$data['admin_user_id_enc'] = $tbl_admin_user_id;
		// 'S' for school
		//User details
		$this->load->model("model_admin");	
		$backend_modules_rights  = $this->model_admin->get_ministry_backend_modules_rights($tbl_admin_user_id,'G');
		$data['backend_modules_rights'] = $backend_modules_rights;
		$this->load->view('admin/view_ministry_template', $data);
	}
	

 
 //END ADMIN USER RIGHTS

//END MINISTRY ADMIN USERS 
 
 


//TIMELINE ADMIN USERS

 // ADMIN USERS LIST START

   /**
	* @desc    Show all ministry users
	* @param   none
	* @access  default
	*/
    function all_timeline_users() {
		
		$data['page'] = "view_timeline_admin_user";
		$data['menu'] = "admin";
        $data['sub_menu'] = "admin_users";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "D";
		$sort_name = "reg_date";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_users = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name";
					 break;
				}
				case("D"): {
					$sort_name = "reg_date";
					 break;
				}
				default: {
					$sort_name = "first_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
		$this->load->model("model_admin");
		$is_active = "";
        $rs_all_users = $this->model_admin->get_all_timeline_users($sort_name, $sort_by, $offset, $q, $is_active);
	    $total_users = $this->model_admin->get_total_timeline_users($q, $is_active);

		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/admin_user/all_timeline_users";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_users;
		$config['per_page'] = TBL_ADMIN_USERS_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_ADMIN_USERS_PAGING >= $total_users) {
			$range = $total_users;
		} else {
			$range = $offset+TBL_ADMIN_USERS_PAGING;
		}
        if(LAN_SEL=="ar")
		    $paging_string = "$range - $start <font color='#333'>من $total_users المستخدمين </font>&nbsp;";
		else
			$paging_string = "$start - $range <font color='#333'>of $total_users users</font>&nbsp;";
			
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_users']	= $rs_all_users;
		$data['total_users'] 	= $total_users;
		$this->load->view('admin/view_timeline_template',$data);
	}



/**
	* @desc    Activate user
	*
	* @param   String tbl_admin_user_id
	* @access  default
	*/
    function activate_timeline() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_admin_user_id = trim($_POST["admin_user_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		
		$this->load->model("model_admin");		
		$this->model_admin->activate_timeline($tbl_admin_user_id);
	}



/**
	* @desc    Deactivate user
	*
	* @param   String tbl_admin_user_id
	* @access  default
	*/
    function deactivate_timeline() {
		if(isset($_POST) && count($_POST) != 0) {
			$tbl_admin_user_id = trim($_POST["admin_user_id_enc"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} 
		
		$this->load->model("model_admin");		
		$this->model_admin->deactivate_timeline($tbl_admin_user_id);
	}


	/**
	* @desc    Delete user
	*
	* @param   CSV tbl_admin_user_id
	* @access  default
	*/
    function delete_timeline() {
		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['admin_user_id_enc']; //e.g." admin_user_id_enc=f31981d6285a6a958f754774013472f7&admin_user_id_enc=d99f9fee0ed779292475c&admin_user_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('admin_user_id_enc=', '', $str);
			$str = explode("&", $str);

			$is_ajax = trim($_POST["is_ajax"]);
		} 
		$this->load->model("model_admin");		
		for ($i=0; $i<count($str); $i++) {
			$this->model_admin->delete_timeline($str[$i]);
		}
	}


  	/**
	* @desc    Create user
	*
	* @param   POST array
	* @access  default
	*/
    function create_timeline_user() {
		if ($_POST) {
			$tbl_admin_user_id = $_POST['admin_user_id_enc'];		
			$first_name 		= $_POST['first_name'];			
			$last_name 		 = $_POST['last_name'];			
			$dob 			   = $_POST['dob'];			
			$gender            = $_POST['gender'];			
			$mobile            = $_POST['mobile'];			
			$email             = $_POST['email'];
			$username          = $_POST['username'];				
			$password          = $_POST['password'];
		}
		
		$this->load->model("model_admin");		
		$data = $this->model_admin->create_timeline_user($tbl_admin_user_id, $first_name, $last_name, $dob, $gender, $mobile, $email, $username, $password);
		echo $data;
		exit;
		
	}

	/**
	* @desc    Save changes
	*
	* @param   POST array
	* @access  default
	*/
    function save_timeline_changes() {
		if ($_POST) {
			$tbl_admin_user_id = $_POST['admin_user_id_enc'];		
			$first_name = $_POST['first_name'];			
			$last_name = $_POST['last_name'];			
			$dob = $_POST['dob'];			
			$gender = $_POST['gender'];			
			$mobile = $_POST['mobile'];			
			$email= $_POST['email'];			
			$password = $_POST['password'];			
		}
		$this->load->model("model_admin");		
		$this->model_admin->save_timeline_changes($tbl_admin_user_id, $first_name, $last_name, $dob, $gender, $mobile, $email, $password);
	}

	/**
	* @desc    Edit user
	*
	* @param   none
	* @access  default
	*/
    function edit_timeline_user() {
		$data['page'] = "view_timeline_admin_user";
		$data['menu'] = "admin";
		$data['sub_menu'] = "admin_users";
		$data['mid'] = "3";

		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_admin_user_id = 0;
					
		if (array_key_exists('admin_user_id_enc',$param_array)) {
			$tbl_admin_user_id = $param_array['admin_user_id_enc'];
		}	 
		
		//User details
		$this->load->model("model_admin");		
		$admin_user_obj = $this->model_admin->get_admin_user_obj($tbl_admin_user_id);
		$data['admin_user_obj'] = $admin_user_obj;
		
		//User picture
		$tbl_uploads_id = "";
		$img_url = "";
		$this->load->model("model_file_mgmt");		
		$file_mgmt_obj = $this->model_file_mgmt->get_file_mgmt_obj($tbl_admin_user_id);

		if (count($file_mgmt_obj)>0) {
			$tbl_uploads_id = $file_mgmt_obj['tbl_uploads_id'];	
			$img_url = HOST_URL."/admin/uploads/".$file_mgmt_obj['file_name_updated'];	

			$data['tbl_uploads_id'] = $tbl_uploads_id;
			$data['img_url'] = $img_url;
		}
		$this->load->view('admin/view_timeline_template', $data);
	}
	
	/**
	* @desc    Student details
	* @param   none
	* @access  default
	*/
    function show_timeline_details() {
		$data['page'] = "view_timeline_admin_user";
		$data['menu'] = "admin";
		$data['sub_menu'] = "admin_users";
		$data['mid'] = "4";//Details

		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_user_id = 0;
					
		if (array_key_exists('admin_user_id_enc',$param_array)) {
			$tbl_admin_user_id = $param_array['admin_user_id_enc'];
			$data['tbl_admin_user_id'] = $tbl_admin_user_id;
		}	 
		
		//User details
		$this->load->model("model_admin");		
		$admin_user_obj = $this->model_admin->get_admin_user_obj($tbl_admin_user_id);
		$data['admin_user_obj'] = $admin_user_obj;
		
		$this->load->view('admin/view_timeline_template', $data);
	}


 // END ADMIN USER SECTION

 // START ADMIN USER RIGHTS
 
 	/**
	* @desc    Save changes
	* @param   POST array
	* @access  default
	*/
    function save_timeline_admin_rights() {
		$this->load->model("model_admin");

		if(isset($_POST) && count($_POST) != 0) {
			$str = $_POST['admin_users_allowed']; //e.g." admin_user_id_enc=f31981d6285a6a958f754774013472f7&admin_user_id_enc=d99f9fee0ed779292475c&admin_user_id_enc=a343d70666c3fb6c9677b2a6f3e8309d
			$str = str_replace('admin_users_allowed=', '', $str);
			$is_ajax = trim($_POST["is_ajax"]);
			$tbl_admin_user_id = $_POST['tbl_admin_user_id'];	
			$str = explode("&", $str);
		} 
		
	    $this->model_admin->del_timeline_admin_rights($tbl_admin_user_id);
		for ($m=0; $m<count($str); $m++) {
		  $this->model_admin->save_timeline_admin_rights($str[$m],$tbl_admin_user_id);	
		}
		echo "Y";
		exit;
	}


	/**
	* @desc    Edit timeline user rights
	* @param   none
	* @access  default
	*/
    function timeline_user_rights() {
		$data['page'] = "view_timeline_user_rights";
		$data['menu'] = "admin";
		$data['sub_menu'] = "admin_users";
		$data['mid'] = "3";

		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$tbl_admin_user_id = 0;
					
		if (array_key_exists('admin_user_id_enc',$param_array)) {
			$tbl_admin_user_id = $param_array['admin_user_id_enc'];
			
		}
		$data['admin_user_id_enc'] = $tbl_admin_user_id;
		// 'S' for school
		//User details
		$this->load->model("model_admin");	
		$backend_modules_rights  = $this->model_admin->get_timeline_backend_modules_rights($tbl_admin_user_id,'T');
		$data['backend_modules_rights'] = $backend_modules_rights;
		$this->load->view('admin/view_timeline_template', $data);
	}
 
    //END ADMIN USER RIGHTS
 
 
     function ajax_validate_and_login_admin() {
		if(isset($_POST) && count($_POST) != 0) {
			$username 		= trim($_POST["username"]);
			$password 		= trim($_POST["password"]);
			$module_access 	= trim($_POST["module_access"]);  // S - School Login, G - Government Login, T - Teacher Login
			$remember_me 	= trim($_POST["remember_me"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} else {
			echo "*N*";	
		   exit;
		}
		
		
		
        $this->load->model('model_admin');
       	$is_user_status = $this->model_admin->authenticate($username, $password, $module_access);
		
		switch($is_user_status) {
			case("N"): {//User does not exist
				echo "*N*";
				exit;
				}
			case("Y"): {
					$admin_user_obj = $this->model_admin->get_admin_info($username);
					
					//Create user sessions
					$_SESSION['aqdar_smartcare']['tbl_admin_id_sess']      = $admin_user_obj[0]['tbl_admin_id'];
					$_SESSION['aqdar_smartcare']['tbl_admin_user_id_sess'] = $admin_user_obj[0]['user_id'];
					$_SESSION['aqdar_smartcare']['admin_email_sess']       = $admin_user_obj[0]['email'];
					$_SESSION['aqdar_smartcare']['admin_first_name_sess']  = $admin_user_obj[0]['first_name'];
					$_SESSION['aqdar_smartcare']['admin_last_name_sess']   = $admin_user_obj[0]['last_name'];
					$_SESSION['aqdar_smartcare']['tbl_school_id_sess']     = $admin_user_obj[0]['tbl_school_id'];
					$_SESSION['aqdar_smartcare']['added_date_sess']        = date('M d, Y', strtotime($admin_user_obj[0]['reg_date']));
					
					$_SESSION['tbl_admin_id_sess']      = $admin_user_obj[0]['tbl_admin_id'];
					$_SESSION['tbl_admin_user_id_sess'] = $admin_user_obj[0]['user_id'];
					$_SESSION['admin_email_sess']       = $admin_user_obj[0]['email'];
					$_SESSION['admin_first_name_sess']  = $admin_user_obj[0]['first_name'];
					$_SESSION['admin_last_name_sess']   = $admin_user_obj[0]['last_name'];
					$_SESSION['tbl_school_id_sess']     = $admin_user_obj[0]['tbl_school_id'];
					$_SESSION['added_date_sess']        = date('M d, Y', strtotime($admin_user_obj[0]['reg_date']));
					
					if($module_access == "G")
					{ 
						$_SESSION['aqdar_smartcare']['user_type_sess']         = "ministry";
					}else if( $module_access == "T" ) {
						$_SESSION['aqdar_smartcare']['user_type_sess']         = "timeline";
					}else{
						$_SESSION['aqdar_smartcare']['user_type_sess']         = "admin";
					}
					$_SESSION['aqdar_smartcare']['admin_auth_sess']        =  $admin_user_obj[0]['user_type'];
					$_SESSION['aqdar_smartcare']['module_access']          =  $admin_user_obj[0]['module_access'];
					
					
					
					if($admin_user_obj[0]['module_access']=="S" || $admin_user_obj[0]['module_access']=="T" )
					{
						$admin_access_obj = $this->model_admin->getSchoolModuleAccess($admin_user_obj[0]['tbl_admin_id']);
						
						for($k=0;$k<count($admin_access_obj);$k++)
						{
							$label = $admin_access_obj[$k]["module_key"];
							$_SESSION[$admin_access_obj[$k]["module_key"]] = "Y";
							
						}
						
					}
					
					$this->load->model("model_file_mgmt");		
		            $file_mgmt_obj = $this->model_file_mgmt->get_file_mgmt_obj($admin_user_obj[0]['tbl_admin_id']);
					if (count($file_mgmt_obj)>0) {
						$tbl_uploads_id = $file_mgmt_obj['tbl_uploads_id'];	
						$img_url = HOST_URL."/admin/uploads/".$file_mgmt_obj['file_name_updated'];	
						$_SESSION['aqdar_smartcare']['admin_pic']          = $img_url;
					}
				
					
					//Update last login time
					$this->model_admin->update_last_login($admin_user_obj[0]['tbl_admin_id']);
					
					if($admin_user_obj[0]['module_access']=="S")
					{
						$this->load->model('model_school');
						$school_information = $this->model_school->get_school_details($admin_user_obj[0]['tbl_school_id'],LAN_SEL);
						$_SESSION['aqdar_smartcare']['school_name']    = $school_information[0]['school_name'];
						$_SESSION['aqdar_smartcare']['school_name_ar'] = $school_information[0]['school_name_ar'];
						$_SESSION['aqdar_smartcare']['school_logo']    = $school_information[0]['logo'];
						$_SESSION['aqdar_smartcare']['school_type']    = $school_information[0]['school_type'];
					}else if($admin_user_obj[0]['module_access']=="T")
					{
						$_SESSION['aqdar_smartcare']['school_name']    = $admin_user_obj[0]['first_name']." ".$admin_user_obj[0]['last_name'];
						$_SESSION['aqdar_smartcare']['school_logo']    = $img_url;
						$_SESSION['aqdar_smartcare']['admin_pic']      = $img_url;
					}else{
						
						$_SESSION['aqdar_smartcare']['school_name']    = '';
						$_SESSION['aqdar_smartcare']['school_name_ar'] = '';
						$_SESSION['aqdar_smartcare']['school_logo']    = '';
						$_SESSION['aqdar_smartcare']['school_type']    = '';
					}
					
				echo "*Y*";
				exit;
				}
		}
	echo "*N*";
	exit;
	}

//END MINISTRY ADMIN USERS 





/******************************************************************/





	

	

	


}
?>