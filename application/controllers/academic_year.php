<?php
/**
* @desc   	  	Academic Year Controller
*
* @category   	Controller
* @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
* @version    	0.1
*/

class Academic_year extends CI_Controller {
	
	
	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
    function index() {
		//Do nothing
	}
	
	
	/**
	* @desc    Image Gallery
	*
	* @param   none
	* @access  default
	*/
	function all_academic_year() {
		
		$data['page'] = "view_academic_year_ajax";

		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["school_id"];
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "added_date";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		//$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "title";
					 break;
				}
				default: {
					$sort_name = "title";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}

		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_classes");
		
		$is_active = "";
		
		//$tbl_school_id = $_SESSION['tbl_school_id_sess'];  
		if($tbl_school_id<>"")
		{  

			$rs_all_academic_years    = $this->model_classes->get_all_academic_year($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id);
			$total_academic_years     = $this->model_classes->get_total_academic_year($q, $is_active, $tbl_school_id);
		}else{
			$rs_all_academic_years     = array();
			$total_academic_years      = array();
		}
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/admin/classes/academic_year_conf";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_class_id) && trim($tbl_class_id)!="") {
			$page_url .= "/tbl_class_session_id/".rawurlencode($tbl_class_session_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url']    = $page_url;
		$config['total_rows']  = $total_academic_years;
		$config['per_page']    = TBL_SEMESTER_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_SEMESTER_PAGING >= $total_academic_years) {
			$range = $total_academic_years;
		} else {
			$range = $offset+TBL_SEMESTER_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_academic_years academic years</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_academic_years'] = $rs_all_academic_years;
		$data['total_academic_years'] = $total_academic_years;

		$this->load->view('include/'.LAN_SEL."/view_academic_year_ajax.php",$data);
	
	}
}
?>
