<?php

/**
 * @desc   	  	Message Controller
 * @category   	Controller
 * @version    	0.1
 */
class Message extends CI_Controller {

	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
    function index() {
		//Do nothing
	}

	/**
	* @desc    Save message
	*
	* @param   none
	* @access  default
	*/
    function get_message_list() {

		$user_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		$tbl_class_id = $_REQUEST["tbl_class_id"];
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		$module_id = $_REQUEST["module_id"];
		
		$data["user_id"] = $user_id;
		$data["role"] = $role;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] = $school_id;
		$data["tbl_class_id"] = $tbl_class_id;
		$data["tbl_student_id"] = $tbl_student_id;
		$data["module_id"] = $module_id;

		$tbl_teacher_id = $user_id;
		
		// Get Student Picture
		$this->load->model('model_student');
		$file_name_updated = $this->model_student->get_student_picture($tbl_student_id);
		
		$this->load->model('model_parents');
		$tbl_parent_id = $this->model_parents->get_parent_of_student($tbl_student_id);	
		$data_parent = $this->model_parents->get_parent_obj($tbl_parent_id);
		
		//print_r($data_parent);
		$mobile = $data_parent[0]["mobile"];
		$this->load->model('model_message');
		//$data_rs = $this->model_message->get_message_list($tbl_teacher_id, $tbl_student_id, $tbl_parent_id, $school_id);
		$data_rs = $this->model_message->get_message_list("", $tbl_student_id, $tbl_parent_id, $school_id);
		
		if (count($data_rs) <=0) {
			$arr["code"] = "200";
			echo json_encode($arr);
			return;
		}

		if ($tbl_student_id == "all_students") {
			$name = "All Students";	
		} else {
			$this->load->model('model_student');
			$data_student = $this->model_student->get_student_obj($tbl_student_id);
			$data["data_student"] = $data_student;
			if ($lan == "ar") {
				$name = $data_student[0]["first_name_ar"]." ".$data_student[0]["last_name_ar"];
			} else {
				$name = $data_student[0]["first_name"]." ".$data_student[0]["last_name"];
			}
			$data["name"] = $name;
		}
		
		$data["file_name_updated"] = $file_name_updated;
		$data["data_parent"] = $data_parent;
		$data["data_rs"] = $data_rs;
		$data["page"] = "view_message_list_page";
		
		//Teacher admin
		//echo "---->".$_POST["is_admin"] == "Y";
		if (trim($_POST["is_admin"]) == "Y") {
			$this->load->view('admin/view_message_list_page',$data);
			return;
		}
		$this->load->view('view_template',$data);
	}

	/**
	* @desc    Get all message
	* @param   none
	* @access  default
	*/
    function save_message() {
		$user_id 			= $_REQUEST["user_id"];
		$role 				= $_REQUEST["role"];
		$lan 				= $_REQUEST["lan"];
		$device 			= $_REQUEST["device"];
		$device_uid 		= $_REQUEST["device_uid"];
		$school_id 			= $_REQUEST["school_id"];
		$tbl_class_id 		= $_REQUEST["tbl_class_id"];
		$tbl_student_id 	= $_REQUEST["tbl_student_id"];
		$message_type 		= $_REQUEST["message_type"];
		$message 			= $_REQUEST["message"];
		$tbl_item_id 		= urldecode($_REQUEST["tbl_item_id"]);		// This will be tbl_message_id if sent by user
		$message_mode 		= $_REQUEST["message_mode"];
		
		
		$data["user_id"] 		= $user_id;
		$data["role"] 			= $role;
		$data["lan"] 			= $lan;
		$data["device"] 		= $device;
		$data["device_uid"] 	= $device_uid;
		$data["school_id"] 		= $school_id;
		$data["tbl_class_id"] 	= $tbl_class_id;
		$data["tbl_student_id"] = $tbl_student_id;
		$data["message_type"] 	= $message_type;
		//$data["message"] = urldecode($message);
		$data["tbl_message_id"] = urldecode($tbl_item_id);
		$tbl_student_id_arr 	= explode(":", $tbl_student_id);
		$tbl_teacher_id 		= $user_id;
		$tbl_school_id 			= $school_id;
		$tbl_message_group_id   = substr(md5(uniqid(rand())),0,10);
		
		if ($tbl_student_id == "all_students") {
				// Get all students in given class room
				$this->load->model('model_student');
				$data_students = $this->model_student->get_all_students_against_class($tbl_class_id);
				for($i=0; $i<count($data_students); $i++) {
					$tbl_student_idd = $data_students[$i]["tbl_student_id"];
					$this->load->model('model_parents');
					$tbl_parent_id = $this->model_parents->get_parent_of_student($tbl_student_idd);
					//echo "---->".$tbl_school_id;
					$this->load->model('model_message');
					$this->model_message->save_message($tbl_teacher_id, $tbl_parent_id, $tbl_student_idd, $message_type, $message, $tbl_school_id, '', $tbl_class_id, $message_mode, $tbl_message_group_id);
					$this->load->model('model_student');
					$stu_obj = $this->model_student->get_student_obj($tbl_student_idd);
					$first_name_stu = $stu_obj[0]['first_name'];
					$last_name_stu = $stu_obj[0]['last_name'];
					$this->load->model('model_user_notify_token');
					$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_parent_id);
					for ($b=0; $b<count($data_tkns); $b++) {										
						//Send push message
						if (trim(ENABLE_PUSH) == "Y") {
							/*PUSH START*/
							if (trim($message_type) == "email") {
								$message = 'Dear Parent, please check your email regarding your child '.$first_name_stu.' '.$last_name_stu;
							} else {
								$message = 'Dear Parent, please check message in your application inbox regarding your child '.$first_name_stu.' '.$last_name_stu;
							}
							$message = urlencode($message);
							//echo $message."<br>";
							//$token = $tbl_parent_id."-".$data_tkns[$b]["token"];
							$token = $data_tkns[$b]["token"];
                            $device = $data_tkns[$b]["device"];
							//$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$tbl_parent_id."&msg=".$message."&data=";
							$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$token."&msg=".$message."&data=";
							$this->load->model('model_user_notify_token');
							$this->model_user_notify_token->send_notification($token , $message, $device);//echo $url;
							//echo $url;
							//$ch = curl_init();
							//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
							//curl_setopt($ch, CURLOPT_URL, $url);
							//curl_setopt($ch, CURLOPT_HEADER, 0);
							//curl_exec($ch);
							//curl_close($ch);
							/*PUSH END*/				
						}//if (trim(ENABLE_PUSH) == "Y")
					}
				}//for($i=0; $i<count($data_students); $i++) {

				$name = "All Students";	
		} else {
			for($j=0; $j<count($tbl_student_id_arr); $j++) {
				
					$tbl_student_id = $tbl_student_id_arr[$j];
					if ($tbl_student_id == "") {continue;}
					$this->load->model('model_parents');
					$tbl_parent_id = $this->model_parents->get_parent_of_student($tbl_student_id);
					//echo "-------->".$tbl_parent_id ;
					$message = $_REQUEST["message"];
					$this->load->model('model_message');
					$this->model_message->save_message($tbl_teacher_id, $tbl_parent_id, $tbl_student_id, $message_type, urldecode($message), $tbl_school_id, $j, $tbl_class_id, $message_mode, $tbl_message_group_id);
					$this->load->model('model_student');
					$stu_obj = $this->model_student->get_student_obj($tbl_student_id);
					$first_name_stu = $stu_obj[0]['first_name'];
					$last_name_stu = $stu_obj[0]['last_name'];				
					$name = $first_name_stu." ".$last_name_stu;
					//print_r($stu_obj);
					//Send push message

					$this->load->model('model_user_notify_token');
					$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_parent_id);
					for ($b=0; $b<count($data_tkns); $b++) {
						if (trim(ENABLE_PUSH) == "Y") {
							/*PUSH START*/
							if (trim($message_type) == "email") {
								$message = 'Dear Parent, please check your email regarding your child '.$first_name_stu.' '.$last_name_stu;
							} else {
								$message = 'Dear Parent, please check message in your application inbox regarding your child '.$first_name_stu.' '.$last_name_stu;
							}
						//$token = $tbl_parent_id."-".$data_tkns[$b]["token"];

							$token = $data_tkns[$b]["token"];
                            $device = $data_tkns[$b]["device"];
							//$message = urlencode($message);
							//$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$tbl_parent_id."&msg=".$message."&data=";
							$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$token."&msg=".$message."&data=";
							$this->load->model('model_user_notify_token');
							$this->model_user_notify_token->send_notification($token , $message, $device);//echo $url;
							//echo $url;
							//$ch = curl_init();
							//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
							//curl_setopt($ch, CURLOPT_URL, $url);
							//curl_setopt($ch, CURLOPT_HEADER, 0);
							//curl_exec($ch);
							//curl_close($ch);
							/*PUSH END*/				
						}//if (trim(ENABLE_PUSH) == "Y")
					}
				} //for($j=0; $j<count($tbl_student_id_arr); $j++) {
			}
			//$this->load->model('model_message');
			//$data_rs = $this->model_message->get_message_list($tbl_teacher_id, $tbl_student_id, $tbl_parent_id, $tbl_school_id);
			$arr["code"]="200";
			echo json_encode($arr);
			//$data["page"] = "view_message_list";
			//include("views/include/".$lan."/view_message_list.php");
	}	/**
	* @desc    Function to save SMS message
	*
	* @param   none
	* @access  default
	*/
    function get_all_messages() {

		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		$tbl_parent_or_teacher_id = $_REQUEST["tbl_parent_or_teacher_id"]; 
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		$this->load->model('model_message');
		$all_messages_rs = $this->model_message->get_all_messages($tbl_parent_or_teacher_id, $tbl_student_id);
        //print_r($all_messages_rs); exit;
		$data['all_messages_rs'] = $all_messages_rs;
		$currentDate = date("Y-m-d");
		$prevDate    = date("Y-m-d", strtotime('-14 days', strtotime($currentDate)));
		$parent_school_messages =  $this->model_message->get_school_parent_messages($tbl_parent_or_teacher_id,$currentDate,$prevDate);
		//print_r($parent_school_messages); exit;
        $data['parent_school_messages'] = $parent_school_messages;
		$data["page"] = "view_all_messages";
		$this->load->view('view_template',$data);
	}	/**
	* @desc    Get points detail
	*
	* @param   none
	* @access  default
	*/
    function save_leave_application() {
		
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		$application_message = $_REQUEST["application_message"];
		
		$this->load->model('model_parents');
		$tbl_parent_id = $this->model_parents->get_parent_of_student($tbl_student_id);		
		
		$this->load->model('model_student');
		$stu_obj = $this->model_student->get_student_obj($tbl_student_id);
		$first_name_stu = $stu_obj[0]['first_name'];
		$last_name_stu = $stu_obj[0]['last_name'];
		$tbl_class_id  = $stu_obj[0]['tbl_class_id'];
		
		$this->load->model('model_classes');
		$class_name = $this->model_classes->get_class_name($tbl_class_id);		
		$rs_teachers = $this->model_classes->get_all_teachers_in_class($tbl_class_id);	

		for ($i=0; $i<count($rs_teachers); $i++) {
			$tbl_teacher_id = $rs_teachers[$i]['tbl_teacher_id'];

			$this->load->model('model_teachers');
			$teacher_obj = $this->model_teachers->get_teachers_obj($tbl_teacher_id);
			$first_name_teacher = $teacher_obj[0]['first_name'];
			$last_name_teacher = $teacher_obj[0]['last_name'];
			
			$this->load->model('model_message');
			$this->model_message->save_leave_application($tbl_teacher_id, $tbl_student_id, $tbl_parent_id, $application_message, $tbl_school_id);
			
			$this->load->model('model_user_notify_token');
			$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_teacher_id);
			for ($b=0; $b<count($data_tkns); $b++) {
				if (trim(ENABLE_PUSH) == "Y") {
						$message = 'Dear '.$first_name_teacher.' '.$last_name_teacher.', Student '.$first_name_stu.' '.$last_name_stu.' from '.$class_name.' is on leave today.' ;
						$message = urlencode($message);
						//$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$tbl_teacher_id."&msg=".$message."&data=";
						//$token = $tbl_teacher_id."-".$data_tkns[$b]["token"];
						//$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$tbl_teacher_id."&msg=".$message."&data=";
						$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$token."&msg=".$message."&data=";
						$token = $data_tkns[$b]["token"];
                        $device = $data_tkns[$b]["device"];						
						$this->load->model('model_user_notify_token');
						$this->model_user_notify_token->send_notification($token , $message, $device);
						//$ch = curl_init();
						//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
						//curl_setopt($ch, CURLOPT_URL, $url);
						//curl_setopt($ch, CURLOPT_HEADER, 0);
						//curl_exec($ch);
						//curl_close($ch);								//$post_data = array();
				}//if (trim(ENABLE_PUSH) == "Y")		
			}
		}//for ($i=0; $i<count($rs_teachers); $i++)
		echo "--yes--";
	}

	/**
	* @desc    Get cards detail
	*
	* @param   none
	* @access  default
	*/
    function get_points_detail() {
		$lan            = $_REQUEST["lan"];
		$device         = $_REQUEST["device"];
		$device_uid     = $_REQUEST["device_uid"];
		$login_id       = $_REQUEST["user_id"];
		$role           = $_REQUEST["role"];
		$school_id      = $_REQUEST["school_id"];
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		$tbl_school_id  = $school_id;		$data["lan"]    = $lan;
		$data["device"]   = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		$data["login_id"] = $login_id;
		$data["role"] = $role;
		$data["school_id"] = $school_id;
		$data["tbl_student_id"] = $tbl_student_id;
		
		
		$this->load->model('model_message');
		$all_messages_rs = $this->model_message->get_points_detail($tbl_student_id);
		$data["all_messages_rs"] = $all_messages_rs;
		
		$data["page"] = "view_points_detail";
		
		$this->load->view('view_template',$data);
	}
	
	function getTeacherMessages(){
		$lan             = $_REQUEST["lan"];
		$device          = $_REQUEST["device"];
		$device_uid      = $_REQUEST["device_uid"];
		$login_id        = $_REQUEST["user_id"];
		$school_id       = $_REQUEST["school_id"];
		$tbl_teacher_id_to  = $_REQUEST["tbl_teacher_id_to"];
		$page = 50;
		$tbl_school_id      = $school_id;

		$data["lan"]           = $lan;
		$data["device"]        = $device;
		$data["device_uid"]    = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		$data["login_id"]      = $login_id;
		$data["role"]          = $role;
		$data["school_id"]     = $school_id;

		$data["tbl_teacher_id_to"] = $tbl_teacher_id_to;
		$this->load->model('model_message');
		$all_messages_rs = $this->model_message->getTeacherMessagesFromSchool($tbl_teacher_id_to,$tbl_school_id,$page);
		$data["all_messages_rs"] = $all_messages_rs;
		$data["page"] = "view_all_messages";
		$this->load->view('view_template',$data);
	}

	function saveGovtFeedback()
	{

		$lan 		= $_REQUEST["lan"];
		$device 	= $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];

		$email_id 	= $_REQUEST["email_id"];
		$user_type 	= $_REQUEST["user_type"];
		$subject    = $_REQUEST["subject"];
		$feedback   = $_REQUEST["feedback"];

		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$this->load->model('model_message');
		$saveMessage = $this->model_message->saveMessageToGovt($email_id,$user_type,$subject,$feedback,$tbl_school_id);

		$response = "Success";
		$errorMsg = "Queries sent successfully";
		$response_array = array('response' => $response, 'result' => '', 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
	}
		
	function saveTimelineFeedback()
	   {

		$lan 		   = $_REQUEST["lan"];
		$device 	   = $_REQUEST["device"];
		$device_uid    = $_REQUEST["device_uid"];
		$email_id 	   = $_REQUEST["email_id"];
		$user_type 	   = $_REQUEST["user_type"];
		$subject       = $_REQUEST["subject"];
		$feedback      = $_REQUEST["feedback"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];

		$this->load->model('model_message');
		$saveMessage = $this->model_message->saveMessageToTimeline($email_id,$user_type,$subject,$feedback,$tbl_school_id);
		$response = "Success";
		$errorMsg = "Queries sent successfully";
		$response_array = array('response' => $response, 'result' => '', 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;

	}	
	
	function savePrivateMessage()
	 {
		$lan 				= $_REQUEST["lan"];
		$device 			= $_REQUEST["device"];
		$device_uid 		= $_REQUEST["device_uid"];
		$message_from 	    = $_REQUEST["message_from"];
		$message_to 		= $_REQUEST["message_to"];
		$tbl_student_id     = $_REQUEST["tbl_student_id"];
		$message_type  	    = $_REQUEST["message_type"];
		$tbl_message 		= $_REQUEST["tbl_message"];
		$tbl_item_id    	= $_REQUEST["tbl_item_id"];
		$message_dir 		= $_REQUEST["message_dir"];
		$tbl_school_id 		= $_REQUEST["tbl_school_id"];
		$this->load->model('model_message');
		$this->load->model('model_teachers');
		$this->load->model('model_parents');
		$saveMessage = $this->model_message->save_private_message($message_from, $message_to, $tbl_student_id, $message_type, $tbl_message, $tbl_school_id, $tbl_item_id, $message_dir);
		if($message_dir=="TP"){
			$teacher_obj = $this->model_teachers->get_teacher_details($message_from);
			if($lan=="ar"){
				$first_name_teacher = $teacher_obj[0]['first_name_ar'];
				$last_name_teacher  = $teacher_obj[0]['last_name_ar'];
			}else{
				$first_name_teacher = $teacher_obj[0]['first_name'];
				$last_name_teacher  = $teacher_obj[0]['last_name'];
			}
			$tacher_name = $first_name_teacher." ".$last_name_teacher;  //from
			$parent_obj = $this->model_parents->get_parentInfo_of_student($message_to); //from
			if($lan=="ar"){
					$first_name_parent = $parent_obj[0]['first_name_ar'];
					$last_name_parent  = $parent_obj[0]['last_name_ar'];
			}else{
					$first_name_parent = $parent_obj[0]['first_name'];
					$last_name_parent  = $parent_obj[0]['last_name'];
			}
			$parent_name = $first_name_parent." ".$last_name_parent;
			$tbl_userId	=	$parent_obj[0]['tbl_parent_id'];
			//$tbl_userId     =   $tbl_parent_id;
		}else{
			$parent_obj = $this->model_parents->get_parentInfo_of_student($message_from); //from
			if($lan=="ar"){
					$first_name_parent = $parent_obj[0]['first_name_ar'];
					$last_name_parent  = $parent_obj[0]['last_name_ar'];
			}else{
					$first_name_parent = $parent_obj[0]['first_name'];
					$last_name_parent  = $parent_obj[0]['last_name'];
			}
			$parent_name = $first_name_parent." ".$last_name_parent;
			$teacher_obj = $this->model_teachers->get_teacher_details($message_to); //To
			if($lan=="ar"){
				$first_name_teacher = $teacher_obj[0]['first_name_ar'];
				$last_name_teacher  = $teacher_obj[0]['last_name_ar'];
			}else{
				$first_name_teacher = $teacher_obj[0]['first_name'];
				$last_name_teacher  = $teacher_obj[0]['last_name'];
			}
			$tbl_userId  = $teacher_obj[0]['tbl_teacher_id'];
			$tacher_name = $first_name_teacher." ".$last_name_teacher;
			//$tbl_userId     =   $tbl_teacher_id;
		}
		$this->load->model('model_student');
		$student_obj = $this->model_student->get_student_name($tbl_student_id); 
		if($lan=="ar"){
				$first_name_stu = $student_obj[0]['first_name_ar'];
				$last_name_stu  = $student_obj[0]['last_name_ar'];
			}else{
				$first_name_stu = $student_obj[0]['first_name'];
				$last_name_stu  = $student_obj[0]['last_name'];
			}
			//echo $tbl_userId; exit;
		$this->load->model('model_user_notify_token');
		$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_userId);
		for ($b=0; $b<count($data_tkns); $b++) {										
			//Send push message
			if (trim(ENABLE_PUSH) == "Y") {
				
				/*PUSH START*/
				if ($message_dir=="TP") {
					$message = 'Dear Parent, please check your message regarding your child '.$first_name_stu.' '.$last_name_stu;
				} else {
					$message = 'Dear Teacher, please check message in your application inbox regarding my child '.$first_name_stu.' '.$last_name_stu;
				}
				
				//print_r($data_tkns);
				//$message = urlencode($message);
				//echo $message."<br>"; 
				//$token = $tbl_parent_id."-".$data_tkns[$b]["token"];
				$token = $data_tkns[$b]["token"];
				$device = $data_tkns[$b]["device"];
				//$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$tbl_parent_id."&msg=".$message."&data=";
				$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$token."&msg=".$message."&data=";
				$this->model_user_notify_token->send_notification($token , $message, $device);//echo $url;
				//echo $url;
				//$ch = curl_init();
				//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				//curl_setopt($ch, CURLOPT_URL, $url);
				//curl_setopt($ch, CURLOPT_HEADER, 0);
				//curl_exec($ch);
				//curl_close($ch);
				/*PUSH END*/				
			}//if (trim(ENABLE_PUSH) == "Y")
		}
		$response = "Success";
		$errorMsg = "Private message sent successfully";
		$response_array = array('response' => $response, 'result' => '', 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
	}

	   
  
  function get_private_message_list()
  {

		$lan 				= $_REQUEST["lan"];
		$device 			= $_REQUEST["device"];
		$device_uid 		= $_REQUEST["device_uid"];
		$tbl_teacher_id    	= $_REQUEST["teacher_id"];
		$tbl_student_id  	= $_REQUEST["tbl_student_id"];
		$tbl_school_id 		= $_REQUEST["tbl_school_id"];

		$this->load->model('model_message');
		$this->load->model('model_teachers');
		$this->load->model('model_parents');

		$listMessage = $this->model_message->get_private_message_list($tbl_teacher_id, $tbl_student_id, $tbl_school_id);
		$data = array();
		for($i=0;$i<count($listMessage);$i++)
		{
			if($listMessage[$i]['message_dir'] == "TP")
			{
					$teacher_obj = $this->model_teachers->get_teacher_details($listMessage[$i]['message_from']);
					if($lan=="ar"){
						$first_name_teacher = $teacher_obj[0]['first_name_ar'];
						$last_name_teacher  = $teacher_obj[0]['last_name_ar'];
					}else{
						$first_name_teacher = $teacher_obj[0]['first_name'];
						$last_name_teacher  = $teacher_obj[0]['last_name'];
					}

					$listMessage[$i]['name'] = $first_name_teacher." ".$last_name_teacher;  //from
					$listMessage[$i]['tbl_teacher_id'] = $teacher_obj[0]['tbl_teacher_id'];
					/*$parent_obj = $this->model_parents->get_parentInfo_of_student($listMessage[$i]['message_to']); //to
					if($lan=="ar"){
						$first_name_parent = $parent_obj[0]['first_name_ar'];
						$last_name_parent  = $parent_obj[0]['last_name_ar'];
					}else{

						$first_name_parent = $parent_obj[0]['first_name'];
						$last_name_parent  = $parent_obj[0]['last_name'];
					}
					$listMessage[$i]['to'] = $first_name_parent." ".$last_name_parent;*/
			}else{
					$parent_obj = $this->model_parents->get_parentInfo_of_student($listMessage[$i]['message_from']); //from
					if($lan=="ar"){
						$first_name_parent = $parent_obj[0]['first_name_ar'];
						$last_name_parent  = $parent_obj[0]['last_name_ar'];
					}else{
						$first_name_parent = $parent_obj[0]['first_name'];
						$last_name_parent  = $parent_obj[0]['last_name'];
					}
					$listMessage[$i]['name'] = $first_name_parent." ".$last_name_parent;
					$listMessage[$i]['tbl_teacher_id'] = $parent_obj[0]['tbl_parent_id'];
					/*$teacher_obj = $this->model_teachers->get_teachers_obj($listMessage[$i]['message_to']);
					if($lan=="ar"){
						$first_name_teacher = $teacher_obj[0]['first_name_ar'];
						$last_name_teacher  = $teacher_obj[0]['last_name_ar'];
					}else{
						$first_name_teacher = $teacher_obj[0]['first_name'];
						$last_name_teacher  = $teacher_obj[0]['last_name'];
					}
					$listMessage[$i]['to'] = $first_name_teacher." ".$last_name_teacher;*/
			}			$file_name_updated = "";
			$file_name_updated_audio ="";
			$data_image_rs = "";
			$data_audio_rs = "";

			if($listMessage[$i]['tbl_item_id']<>""){
				$data_image_rs =  $this->model_message->get_message_image($listMessage[$i]['tbl_item_id']);	
				//print_r($data_image_rs);
				$file_name_updated = $data_image_rs[0]['file_name_updated'];
				$file_name_updated_thumb = $data_image_rs[0]['file_name_updated_thumb'];
				$data_audio_rs =  $this->model_message->get_message_audio($listMessage[$i]['tbl_item_id']);	
				$file_name_updated_audio = $data_audio_rs[0]['file_name_updated'];
			}

			if (trim($file_name_updated) != "") {
			$data[$i]["message_img"] = HOST_URL."/admin/uploads/".$file_name_updated;
			$data[$i]["message_img_thumb"] = HOST_URL."/admin/uploads/".$file_name_updated_thumb;
			} else {
				$data[$i]["message_img"] = "";
				$data[$i]["message_img_thumb"] = "";
			}

			if (trim($file_name_updated_audio) != "") {
				$data[$i]["message_audio"] = HOST_URL."/admin/uploads/".$file_name_updated_audio;
			} else {
				$data[$i]["message_audio"] = "";
			}

			$data[$i]['name'] 			=  	$listMessage[$i]['name'];
			$data[$i]['message'] 		=  	isset($listMessage[$i]['message'])? $listMessage[$i]['message']:'' ;
			$data[$i]['teacher_id']     =   $listMessage[$i]['tbl_teacher_id'];
			$data[$i]['date'] 			= 	date("H:i - d/m/Y",strtotime($listMessage[$i]["added_date"]));
		}

		$response = "Success";
		$errorMsg = "Private message sent successfully";
		$response_array = array('response' => $response, 'messages' => $data, 'errorMsg' => $errorMsg);
		//Teacher admin
		//echo "---->".$_POST["is_admin"] == "Y";
		if (trim($_POST["is_admin"]) == "Y") {
			$data["response_array"] = $response_array;
			$this->load->view('admin/view_private_message_list_page',$data);
			return;
		}
		echo json_encode($response_array);
		exit;
	}
	function getParentMessages(){
		$lan           = $_REQUEST["lan"];
		$device        = $_REQUEST["device"];
		$device_uid    = $_REQUEST["device_uid"];
		$login_id      = $_REQUEST["user_id"];
		$school_id     = $_REQUEST["school_id"];

		$tbl_parent_id_to = $_REQUEST["tbl_parent_id_to"];
		$page = 50;
		$tbl_school_id = $school_id;
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		$this->load->model('model_student');

		$stu_obj = $this->model_student->get_student_obj($tbl_student_id);
		$first_name_stu = $stu_obj[0]['first_name'];
		$last_name_stu = $stu_obj[0]['last_name'];
		$tbl_class_id  = $stu_obj[0]['tbl_class_id'];

		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		$data["login_id"] = $login_id;
		$data["role"] = $role;
		$data["school_id"] = $school_id;
		$data["tbl_parent_id_to"] = $tbl_parent_id_to;

		$this->load->model('model_message');
		$all_messages_rs = $this->model_message->getParentMessagesFromSchool($tbl_parent_id_to,$tbl_school_id,$page,$tbl_class_id);
		$data["all_messages_rs"] = $all_messages_rs;
		$data["page"] = "view_all_parent_messages";
		$this->load->view('view_template',$data);
	}
	
	
	//Mobileapp List Teacher Message to a group of students - One way - Message History
	
	 function get_teacher_message_list() {

		$tbl_teacher_id = $_REQUEST["tbl_teacher_id"];
		$role 			= $_REQUEST["role"];
		$lan 			= $_REQUEST["lan"];
		$device 		= $_REQUEST["device"];
		$device_uid 	= $_REQUEST["device_uid"];
		$tbl_school_id 	= $_REQUEST["tbl_school_id"];
		$tbl_class_id   = $_REQUEST["tbl_class_id"];
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		$module_id      = $_REQUEST["module_id"];
		$message_mode 	= $_REQUEST["message_mode"];
		
		$data["tbl_teacher_id"] 		= $tbl_teacher_id;
	
		$this->load->model('model_message');
		$this->load->model('model_teachers');
		$data_rs = $this->model_message->get_teacher_message_list($tbl_teacher_id, $tbl_student_id, $tbl_parent_id, $tbl_school_id, $message_mode, $tbl_class_id);
		$index   = 0;
		for ($i=0; $i<count($data_rs); $i++) {
			$id 			= $data_rs[$i]['id']; 	
			$tbl_message_id = $data_rs[$i]['tbl_message_id'];
			$message_from 	= $data_rs[$i]['message_from'];
			$message_to 	= $data_rs[$i]['message_to'];
			$message_type 	= $data_rs[$i]['message_type'];
			$message 		= $data_rs[$i]['message'];
			$added_date 	= $data_rs[$i]['added_date'];
			$added_date 	= date("H:i - d/m/Y",strtotime($data_rs[$i]["added_date"]));
			
			$data_image_rs =  $this->model_message->get_message_image($tbl_message_id);	
			$file_name_updated = $data_image_rs[0]['file_name_updated'];
			$file_name_updated_thumb = $data_image_rs[0]['file_name_updated_thumb'];
			
			$data_audio_rs =  $this->model_message->get_message_audio($tbl_message_id);	
			$file_name_updated_audio = $data_audio_rs[0]['file_name_updated'];
			
			$teacher_obj    =  $this->model_teachers->get_teacher_details($message_from);	
			$first_name 	= $teacher_obj[0]['first_name'];
			$last_name 		= $teacher_obj[0]['last_name'];
			$file_name_updated_teacher = $teacher_obj[0]['file_name_updated'];
			
			$tbl_teacher_id = $teacher_obj[0]['tbl_teacher_id'];

			if (trim($lan) == "ar") {
				$first_name = $teacher_obj[0]['first_name_ar'];	
				$last_name = $teacher_obj[0]['last_name_ar'];
			}
   
            $arr_msg[$index]["tbl_message_id"] 	= $tbl_message_id;
			$arr_msg[$index]["name"] 			= $first_name." ".$last_name;
			$arr_msg[$index]["teacher_id"] 		= $tbl_teacher_id;
			$arr_msg[$index]["date"] 			= $added_date;
			$arr_msg[$index]["message"] 		= $message;
			$arr_msg[$index]["url"] 			= IMG_PATH."/teacher/".$file_name_updated_teacher;
		
			if (trim($file_name_updated) != "") {
				$arr_msg[$index]["message_img"] = HOST_URL."/admin/uploads/".$file_name_updated;
				$arr_msg[$index]["message_img_thumb"] = HOST_URL."/admin/uploads/".$file_name_updated_thumb;
			} else {
				$arr_msg[$index]["message_img"] = "";
				$arr_msg[$index]["message_img_thumb"] = "";
			}
		
			if (trim($file_name_updated_audio) != "") {
				$arr_msg[$index]["message_audio"] = HOST_URL."/admin/uploads/".$file_name_updated_audio;
			} else {
				$arr_msg[$index]["message_audio"] = "";
			}
		
			$index = $index + 1;
		}
		
		$data['messages'] = $arr_msg;
		echo json_encode($data);
		return;
	}

	
	
    function get_total_unread_message_teacher() {

		$tbl_teacher_id =  isset($_REQUEST["tbl_teacher_id"])? $_REQUEST["tbl_teacher_id"] : '' ;
		$tbl_school_id 	=  isset($_REQUEST["tbl_school_id"])? $_REQUEST["tbl_school_id"]: '';
		$message_mode 	=  isset($_REQUEST["message_mode"])? $_REQUEST["message_mode"]: '' ;
		$this->load->model('model_message');
		
		$tbl_community_id="";
		$community_unread_message = $this->model_message->get_total_unread_message_teacher_community($tbl_teacher_id, $tbl_school_id, 'G', $tbl_community_id);
		
		$message_from = "";
		$private_unread_message = $this->model_message->get_total_unread_message_teacher_private($tbl_teacher_id, $tbl_school_id, 'P', $message_from);
		
	    $total_unread_message = 0;
		
	    for($c=0;$c<count($community_unread_message);$c++)
		{
			$total_unread_message = $total_unread_message + $community_unread_message[$c]['cntMessage'];
			$community_unread_message[$c]['msg_mode'] = "G";
		}
		
		 for($p=0;$p<count($private_unread_message);$p++)
		{
			$total_unread_message = $total_unread_message + $private_unread_message[$p]['cntMessage'];
			$private_unread_message[$p]['msg_mode'] = "P";
		}
		
	
		$data['community_unread_message'] = $community_unread_message;
		$data['private_unread_message']   = $private_unread_message;
		$data['total_unread_message']     = $total_unread_message;
		echo json_encode($data);
		return;
	}
	
	
	
	

}
?>

