<?php

/**
 * @desc   	  	Parents Controller
 *
 * @category   	Controller
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Parents extends CI_Controller {


	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
    function index() {
		//Do nothing
	}


	/**
	* @desc    Show parents login page
	*
	* @param   none
	* @access  default
	*/
    function parents_login_page() {
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		
		$tbl_parent_id = $_REQUEST["tbl_parent_id"];
		$data["page"] = "view_parents_login_page";
		$this->load->view('view_template',$data);

	}


	/**
	* @desc    Get children landing page
	*
	* @param   none
	* @access  default
	*/
	function parents_modules_page() {
		$user_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		
		$data["user_id"] = $user_id;
		$data["role"] = $role;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] = $school_id;
		$data["tbl_student_id"] = $tbl_student_id;
		
		$tbl_parent_or_teacher_id = $user_id;
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		
		
		$this->load->model('model_message');
		$all_messages_rs = $this->model_message->get_all_messages($tbl_parent_or_teacher_id, $tbl_student_id);
		$data['all_messages_rs'] = $all_messages_rs;
		//print_r($all_messages_rs);
		
		$this->load->model('model_student');
		$stu_obj = $this->model_student->get_student_obj($tbl_student_id);
		
		$data["stu_obj"] = $stu_obj;
		$data["page"] = "view_parents_modules_page";
		$this->load->view('view_template',$data);
		
	}


	/**
	* @desc    Login Parents
	*
	* @param   none
	* @access  default
	*/
	function child_data_text() {
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		
		$tbl_parent_id = $_REQUEST["tbl_parent_id"];
		$tbl_child_data_id = $_REQUEST["tbl_child_data_id"];
		
		$this->load->model('model_parents');
		$data_prnt_text = $this->model_parents->get_child_data_text($tbl_child_data_id);
		$data["page"] = "view_child_data_text";
		$this->load->view('view_template',$data);
		
		}


	/**
	* @desc    Fetch parent info
	*
	* @param   none
	* @access  default
	*/
	function children_data() {
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		
		$tbl_parent_id = $_REQUEST["tbl_parent_id"];
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		
		$this->load->model('model_parents');
		$data_prnt = $this->model_parents->get_children_data($tbl_parent_id, $tbl_student_id);
		if (count($data_prnt)<=0) {
			echo "--no--";
			return;
		}
		$data["page"] = "view_child_data";
		$this->load->view('view_template',$data);
		
	}


	/**
	* @desc    Fetch all children
	*
	* @param   none
	* @access  default
	*/
	function login_parents() {
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		
		$email = $_REQUEST["email"];
		$password = $_REQUEST["password"];
		
		$tbl_parent_id = $_REQUEST["tbl_parent_id"];
		
		$this->load->model('model_parents');
		$is_exist = $this->model_parents->authenticate($email, $password);
		
		if ($is_exist == "D") {
			echo "--D--";	// User could not be authenticated as user Does Not Exist
			return;
		} else if ($is_exist == "N") {
			echo "--N--";
			return;
		} else if ($is_exist == "Y") {
			$tbl_parent_id = $this->model_parents->get_parent_id($email);
		} else {
			echo "--no--";
			return;
		}
		
		$data["page"] = "view_parents_landing_page";
		$this->load->view('view_template',$data);
		
	}
	

	/**
	* @desc    Show parenting page
	*
	* @param   none
	* @access  default
	*/
	function mychild() {
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		
		$tbl_parent_id = $_REQUEST["tbl_parent_id"];
		
		$this->load->model('model_parents');
		$rs_children = $this->model_parents->fetch_all_children($tbl_parent_id);
		
		$data["page"] = "view_children_landing_page";
		$this->load->view('view_template',$data);
	}


	 function parent_msg_list_page() {
		 
		$user_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		$tbl_parent_id_from = $_REQUEST["tbl_parent_id_from"];
		$tbl_parent_id_to = $_REQUEST["tbl_parent_id_to"];
		$src_id = $_REQUEST["src_id"];					// 
		
		$data["user_id"] = $user_id;
		$data["role"] = $role;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] = $school_id;
		$data["tbl_parent_id_from"] = $tbl_parent_id_from;
		$data["tbl_parent_id_to"] = $tbl_parent_id_to;
		
		$tbl_school_id = $school_id;
		$tbl_parent_id = $user_id;
		
		$this->load->model('model_message');
		$data_rs = $this->model_message->get_parent_messages($tbl_parent_id_from, $tbl_parent_id_to,$tbl_school_id);
		if (count($data_rs) <=0) {
			$arr["code"] = "N";
			echo json_encode($arr);
			return;
		}
		// Change all is_read of messages from from N to Y
		//$this->model_message->update_unread_msg_count($tbl_parent_id_from, $tbl_parent_id_to, $tbl_school_id);
		$data["data_rs"] = $data_rs;
		$data["page"] = "view_teacher_message_list_page";
		$this->load->view('view_template',$data);
		
	}


	 function parent_group_list() {
		$user_id 	= $_REQUEST["user_id"];
		$role    	= $_REQUEST["role"];
		$lan     	= $_REQUEST["lan"];
		$device  	= $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		
		$data["user_id"]    = $user_id;
		$data["role"]       = $role;
		$data["lan"]        = $lan;
		$data["device"]     = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"]  = $school_id;
		$tbl_school_id      = $school_id;
		$tbl_parent_id      = $user_id;
		//$tbl_parent_id    = "5a10b34248df24d";
		//$tbl_school_id    = "9697bfd53bcaf6d";
		//$lan              = "ar";
		$this->load->model('model_parents');
		$data = $this->model_parents->get_list_parent_groups($tbl_school_id,$tbl_parent_id);
        $result = array();
		if($lan=="en")
		{
 			 if(count($data)>0)
			 {
				for($m=0;$m<count($data);$m++){
					$result[$m]['group_name']		    = $data[$m]['group_name_en'];
					$result[$m]['tbl_parent_group_id']	= $data[$m]['tbl_parent_group_id'];
					$parentGroupForums = $this->model_parents->get_list_parent_topics($tbl_school_id,$tbl_parent_id,$data[$m]['tbl_parent_group_id']);
					$result[$m]['cntForum']             = count($parentGroupForums);
				}
			 }
		}else{
			 if(count($data)>0)
			 {
				 for($m=0;$m<count($data);$m++){
					$result[$m]['group_name']		= $data[$m]['group_name_ar'];
					$result[$m]['tbl_parent_group_id']	= $data[$m]['tbl_parent_group_id'];
					$parentGroupForums = $this->model_parents->get_list_parent_topics($tbl_school_id,$tbl_parent_id,$data[$m]['tbl_parent_group_id']);
					$result[$m]['cntForum'] = count($parentGroupForums);
				}
			 }
		}
		$response = "Success";
		$errorMsg = "Parents Group Information";
		$response_array = array('response' => $response, 'result' => $result, 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
	}


	 function get_list_parent_topics() {
		$user_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		$tbl_parent_group_id 	= $_REQUEST["tbl_parent_group_id"];
		$student_id 	= $_REQUEST["student_id"];
		
		$data["user_id"] = $user_id;
		$data["role"] = $role;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] = $school_id;
		$data["tbl_parent_group_id"] = $tbl_parent_group_id;
		$data["student_id"] = $student_id;
		
		
		$tbl_school_id = $school_id;
		$tbl_parent_id = $user_id;
		//$tbl_parent_id = "5a10b34248df24d";
		//$tbl_school_id = "9697bfd53bcaf6d";
		//$tbl_parent_group_id = "c9e2fb449318c74e398df4de54a782c3";
		
		$this->load->model('model_student');
		$student_data = $this->model_student->get_student_obj($student_id);
		$tbl_class_id = $student_data[0]['tbl_class_id'];
		$this->load->model('model_parents');
		$data = $this->model_parents->get_list_parent_topics($tbl_school_id,$tbl_parent_id,$tbl_parent_group_id,$tbl_class_id);
		for($y=0;$y<count($data);$y++)
		{
			$dataParentInfo = $this->model_parents->get_parent_obj($data[$y]['posted_by']);
			if($lan=="ar")
			{
				$first_name = $dataParentInfo[0]["first_name_ar"];
				$last_name = $dataParentInfo[0]["last_name_ar"];
			}else{
				$first_name = $dataParentInfo[0]["first_name"];
				$last_name = $dataParentInfo[0]["last_name"];
			}
			$data[$y]['posted_by'] = $first_name." ".$last_name;
			$item_id = $data[$y]["item_id"];
			if($item_id<>"")
					{
						$this->load->model('model_message');
						$dataRecords = $this->model_message->get_upload_files($item_id);
						$image 				= $dataRecords[0]['file_name_updated'];
						$image_thumb 		= $dataRecords[0]['file_name_updated_thumb'];
						$data[$y]['message_img']		= HOST_URL."/admin/uploads/".$image; 
						$data[$y]['message_img_thumb']	= HOST_URL."/admin/uploads/".$image_thumb;
					}else{
						
						$data[$y]['message_img']		  = ""; 
						$data[$y]['message_img_thumb']	= "";
					}
			$posted_datetime 	= $data[$y]['added_date'];
			$current_datetime 	= date("Y-m-d h:i:s");
			$postedTime = $this->model_parents->time_difference($current_datetime, $posted_datetime);
			$data[$y]['posted_expiry'] = $postedTime;
			
			$forumComments = $this->model_parents->get_list_forum_comments($tbl_school_id,$tbl_parent_id,$data[$y]['tbl_parent_forum_id']);
			$data[$y]['cntComments'] = count($forumComments);
		}
		
		$response = "Success";
		$errorMsg = "Parents forum topics listed successfully";
		$response_array = array('response' => $response, 'result' => $data, 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
			}


	 function get_list_forum_comments() {
		$user_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		$tbl_parent_forum_id 	= $_REQUEST["tbl_parent_forum_id"];
		
		$data["user_id"] = $user_id;
		$data["role"] = $role;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] = $school_id;
		$data["tbl_parent_forum_id"] = $tbl_parent_forum_id;
		
		
		$tbl_school_id = $school_id;
		$tbl_parent_id = $user_id;
		//$tbl_parent_id = "5a10b34248df24d";
		//$tbl_school_id = "9697bfd53bcaf6d";
		//$tbl_parent_group_id = "c9e2fb449318c74e398df4de54a782c3";
		$this->load->model('model_parents');
		$data = $this->model_parents->get_list_forum_comments($tbl_school_id,$tbl_parent_id,$tbl_parent_forum_id);
		for($y=0;$y<count($data);$y++)
		{
			$dataParentInfo = $this->model_parents->get_parent_obj($data[$y]['commented_by']);
			if($lan=="ar")
			{
				$first_name = $dataParentInfo[0]["first_name_ar"];
				$last_name = $dataParentInfo[0]["last_name_ar"];
			}else{
				$first_name = $dataParentInfo[0]["first_name"];
				$last_name = $dataParentInfo[0]["last_name"];
			}
			$data[$y]['commented_by'] = $first_name." ".$last_name;
			$item_id = $data[$y]["item_id"];
			if($item_id<>"")
			{
				$this->load->model('model_message');
				$dataRecords = $this->model_message->get_upload_files($item_id);
		
				$image 				= $dataRecords[0]['file_name_updated'];
				$image_thumb 		= $dataRecords[0]['file_name_updated_thumb'];
				$data[$y]['message_img']		= HOST_URL."/admin/uploads/".$image; 
				$data[$y]['message_img_thumb']	= HOST_URL."/admin/uploads/".$image_thumb;
			}
			$posted_datetime 	= $data[$y]['added_date'];
			$current_datetime 	= date("Y-m-d h:i:s");
			$postedTime = $this->model_parents->time_difference($current_datetime, $posted_datetime);
			$data[$y]['commented_expiry'] = $postedTime;
			
		}
		$response = "Success";
		$errorMsg = "Parents forum comments listed successfully";
		$response_array = array('response' => $response, 'result' => $data, 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
			}


	function add_parent_forum()
    {
		$user_id 		= $_REQUEST["user_id"];
		$role 		   = $_REQUEST["role"];
		$lan 		    = $_REQUEST["lan"];
		$device 	     = $_REQUEST["device"];
		$device_uid     = $_REQUEST["device_uid"];
		$school_id      = $_REQUEST["school_id"];
		$message 	    = $_REQUEST["message"];
		$item_id 	    = $_REQUEST["tbl_item_id"];
		$token 		  = $_REQUEST["token"];
		$tbl_parent_group_id 	= $_REQUEST["tbl_parent_group_id"];
		$tbl_parent_id          = $user_id;
		$this->load->model("model_parents");
		$data = $this->model_parents->saveParentGroupForum($school_id,$tbl_parent_group_id,$message,$item_id,$tbl_parent_id);
		$dataParents = $this->model_parents->get_list_parent_group_members($school_id,$tbl_parent_id,$tbl_parent_group_id);
		for($m=0;$m<count($dataParents); $m++){
			   $tbl_userId = "";
			   $tbl_userId = $dataParents[$m]['tbl_parent_id'];
			   if($tbl_parent_id<>$tbl_userId)
		       {
					$first_name = $dataParents[$m]['first_name'];
					$last_name  = $dataParents[$m]['last_name'];
					$first_name_ar = $dataParents[$m]['first_name_ar'];
					$first_name_ar  = $dataParents[$m]['last_name_ar'];
					$this->load->model('model_user_notify_token');
					$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_userId);
				
					for ($b=0; $b<count($data_tkns); $b++) {										
						
						if (trim(ENABLE_PUSH) == "Y") {
							/*PUSH START*/
							$message = 'Dear '.$first_name.' '.$last_name.' , Added a new topic in your discussion forum';
							$token = $data_tkns[$b]["token"];
							$device = $data_tkns[$b]["device"];
							$this->model_user_notify_token->send_notification($token , $message, $device);//echo $url;
									
						}
					}
			   }
		}
		/**************** end forum post notification ******************/
		
		
		echo json_encode($data);
	}

	function add_parent_forum_comments()
	{
		$user_id 		= $_REQUEST["user_id"];
		$role 		   = $_REQUEST["role"];
		$lan 		    = $_REQUEST["lan"];
		$device 	     = $_REQUEST["device"];
		$device_uid     = $_REQUEST["device_uid"];
		$school_id      = $_REQUEST["school_id"];
		$comments 	   = $_REQUEST["comments"];
		$item_id 	    = $_REQUEST["tbl_item_id"];
		$token 		  = $_REQUEST["token"];
		$tbl_parent_forum_id 	= $_REQUEST["tbl_parent_forum_id"];
		$tbl_parent_id = $user_id;
		$this->load->model("model_parents");
		$data = $this->model_parents->saveParentForumComments($school_id,$tbl_parent_forum_id,$comments,$item_id,$tbl_parent_id);
		
		$dataParentGroup = $this->model_parents->get_parentgroup_byforum($school_id,$tbl_parent_forum_id);
		$tbl_parent_group_id = $dataParentGroup[0]['tbl_parent_group_id'];
        /********************* forum comments notification **************************/
		$this->load->model('model_user_notify_token');
		$dataParents = $this->model_parents->get_list_parent_group_members($school_id,$tbl_parent_id,$tbl_parent_group_id);
		for($m=0;$m<count($dataParents); $m++){
			   $tbl_userId = "";
			   $tbl_userId = $dataParents[$m]['tbl_parent_id'];
			   if($tbl_parent_id<>$tbl_userId)
		       {
				$first_name = $dataParents[$m]['first_name'];
				$last_name  = $dataParents[$m]['last_name'];
				$first_name_ar = $dataParents[$m]['first_name_ar'];
				$first_name_ar  = $dataParents[$m]['last_name_ar'];
				$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_userId);
				//print_r($data_tkns);
				for ($b=0; $b<count($data_tkns); $b++) {										
					//Send push message
					if (trim(ENABLE_PUSH) == "Y") {
						/*PUSH START*/
					
						$message = 'Dear '.$first_name.' '.$last_name.' , Added a new comments in your discussion forum';
						$token = $data_tkns[$b]["token"];
						$device = $data_tkns[$b]["device"];
						
					   $this->model_user_notify_token->send_notification($token , $message, $device);//echo $url;
						/*PUSH END*/				
					}//if (trim(ENABLE_PUSH) == "Y")
				}
			}
		}
		/************************ end forum comments notification ************************/
		echo json_encode($data);
	}
	

	 function parent_group_members() {
		$user_id 			 = $_REQUEST["user_id"];
		$role 				= $_REQUEST["role"];
		$lan 				 = $_REQUEST["lan"];
		$device 			  = $_REQUEST["device"];
		$device_uid 		  = $_REQUEST["device_uid"];
		$school_id 		   = $_REQUEST["school_id"];
		$tbl_parent_group_id = $_REQUEST["tbl_parent_group_id"];
		$data["user_id"]     = $user_id;
		$data["role"] = $role;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] = $school_id;
		$data["tbl_parent_group_id"] = $tbl_parent_group_id;
		$tbl_school_id = $school_id;
		$tbl_parent_id = $user_id;
		//$tbl_parent_id = "5a10b34248df24d";
		//$tbl_school_id = "9697bfd53bcaf6d";
		//$tbl_parent_group_id = "c9e2fb449318c74e398df4de54a782c3";
		$this->load->model('model_parents');
		$data = $this->model_parents->get_list_parent_group_members($tbl_school_id,$tbl_parent_id,$tbl_parent_group_id);
		$response = "Success";
		$errorMsg = "Parents Group Information";
		$response_array = array('response' => $response, 'result' => $data, 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
	}
	

	//get all parents against student against class

	function get_all_parents_students_against_class() {
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$tbl_class_id = $_REQUEST["tbl_class_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		$data["tbl_class_id"] = $tbl_class_id;
		$classes = explode(",",$tbl_class_id);
		$this->load->model('model_student');
		$this->load->model('model_parents');
		$this->load->model('model_classes');
		if(count($classes)>0)
		{  $m=0;
		   for($x=0;$x<count($classes);$x++)
		   {
		
						$tbl_class_id = $classes[$x];
				$all_students_against_class_rs = $this->model_student->list_all_students_against_class($tbl_class_id);
				
				for($a=0;$a<count($all_students_against_class_rs);$a++)
				{
					$parentList = array();
					$tbl_student_id = $all_students_against_class_rs[$a]['tbl_student_id'];
					$tbl_parent_id = $this->model_parents->get_parent_of_student($tbl_student_id);
					$parentList = $this->model_parents->get_parent_obj($tbl_parent_id);
					if(!empty($parentList)){
						$all_parents_students_against_class_rs[$m]['tbl_parent_id'] 	= $parentList[0]['tbl_parent_id'];
						$all_parents_students_against_class_rs[$m]['first_name']   		= $parentList[0]['first_name'];
						$all_parents_students_against_class_rs[$m]['last_name']   		= $parentList[0]['last_name'];
						$all_parents_students_against_class_rs[$m]['first_name_ar']     = $parentList[0]['first_name_ar'];
						$all_parents_students_against_class_rs[$m]['last_name_ar']      = $parentList[0]['last_name_ar'];
						$all_parents_students_against_class_rs[$m]['tbl_class_id']      = $tbl_class_id;
						$class_details = $this->model_classes->getClassInfo($tbl_class_id);
						$tbl_section_id 	= $class_details[0]['tbl_section_id'];
						$data_se = $this->model_classes->getClassSectionInfo($tbl_section_id);		
						$section_name 		=  $data_se[0]["section_name"];
						$section_name_ar 	=  $data_se[0]["section_name_ar"];
						$all_parents_students_against_class_rs[$m]['class_name']      	= $class_details[0]['class_name']." ".$section_name;
						$all_parents_students_against_class_rs[$m]['class_name_ar']     = $class_details[0]['class_name_ar']." ".$section_name_ar;
						$m=$m+1;
					}
				}
		   }
		
		}else{
				$all_students_against_class_rs = $this->model_student->list_all_students_against_class($tbl_class_id);
				$m=0;
				for($a=0;$a<count($all_students_against_class_rs);$a++)
				{
					$parentList = array();
					$tbl_student_id = $all_students_against_class_rs[$a]['tbl_student_id'];
					$tbl_parent_id = $this->model_parents->get_parent_of_student($tbl_student_id);
					$parentList = $this->model_parents->get_parent_obj($tbl_parent_id);
					if(!empty($parentList)){
						$all_parents_students_against_class_rs[$m]['tbl_parent_id'] 	= $parentList[0]['tbl_parent_id']."*".$tbl_class_id;
						$all_parents_students_against_class_rs[$m]['first_name']   		= $parentList[0]['first_name'];
						$all_parents_students_against_class_rs[$m]['last_name']   		= $parentList[0]['last_name'];
						$all_parents_students_against_class_rs[$m]['first_name_ar']     = $parentList[0]['first_name_ar'];
						$all_parents_students_against_class_rs[$m]['last_name_ar']      = $parentList[0]['last_name_ar'];
						$all_parents_students_against_class_rs[$m]['tbl_class_id']      = $tbl_class_id;
						$class_details 		= $this->model_classes->getClassInfo($tbl_class_id);
						$tbl_section_id 	= $class_details[0]['tbl_section_id'];
						$data_se = $this->model_classes->getClassSectionInfo($tbl_section_id);		
						$section_name 		=  $data_se[0]["section_name"];
						$section_name_ar 	=  $data_se[0]["section_name_ar"];
						$all_parents_students_against_class_rs[$m]['class_name']      	= $class_details[0]['class_name']." ".$section_name;
						$all_parents_students_against_class_rs[$m]['class_name_ar']     = $class_details[0]['class_name_ar']." ".$section_name_ar;
						
						
						$m=$m+1;
					}
				}
		}
		$data['all_parents_students_against_class_rs'] = $all_parents_students_against_class_rs;
		$data['page'] = "view_parents_students_against_class_ddm";
		$this->load->view('view_template',$data);
		
	}

	/**
	* @desc		Save parent location
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/

	function save_location() {
		
		$user_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["school_id"];
		
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		
		$tbl_parent_id = $user_id;
		
		$latitude = $_REQUEST["latitude"];
		$longitude = $_REQUEST["longitude"];
		$this->load->model('model_parents');
		$this->model_parents->save_location($latitude, $longitude, $tbl_parent_id, $tbl_school_id);
		echo "--yes--";
	}
	
	
	
	/**
	* @desc		Save parent location
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/

	function get_parent_obj_ajax() {
		
		$emirates_id = $_REQUEST["emirates_id"];
		$tbl_school_id = $_REQUEST["school_id"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		
		if ($tbl_school_id == "") {
			echo '{"code":"200"}';
			exit();
		}
		
		if ($emirates_id == "") {
			echo '{"code":"200"}';
			exit();
		}
		//echo "Here";
		//exit();
		$this->load->model('model_parents');
		$data_rs = $this->model_parents->get_parent_obj("", $emirates_id, $tbl_school_id);
		if ($data_rs[0]["id"] == "") {
			echo '{"code":"200"}';
			exit();
		}
		$rs["first_name"] = $data_rs[0]["first_name"];
		$rs["first_name_ar"] = $data_rs[0]["first_name_ar"];
		$rs["last_name"] = $data_rs[0]["last_name"];
		$rs["last_name_ar"] = $data_rs[0]["last_name_ar"];
		$rs["email"] = $data_rs[0]["email"];
		$rs["user_id"] = $data_rs[0]["user_id"];
		$rs["gender"] = $data_rs[0]["gender"];
		
		$arr["parent_obj"] = $rs;
		echo json_encode($arr);
	}
	
	
	
}
?>

