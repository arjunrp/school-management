<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Books extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	  public function __construct() {
			parent::__construct();
		}

	
	public function my_books()
	{
		$lan 					= $_REQUEST["lan"];
		$tbl_school_id 			= $_REQUEST["tbl_school_id"];
		$tbl_class_id 			= $_REQUEST["tbl_class_id"];
		$tbl_category_id 		= $_REQUEST["tbl_category_id"];
		$offset 			    = $_REQUEST["offset"];
		$q 			            = $_REQUEST["q"];
		
		$this->load->model('model_book');
		$this->load->model('model_class_sessions');
		$this->load->model('model_category');
		
		if($tbl_school_id<>"")
		{  
			
			$sort_by    = "ASC";
		    $sort_name  = "title_en";
			$offset     = 0;
			$total_books= 0;
			$q          = "";
		    $tbl_category_id = "";
			$page = "";
			$list      = "Y";
		
			$list_category = $this->model_category->get_all_book_categories($sort_name, $sort_by, $offset, $q, $list, 'Y');
			
            $list_books  = $this->model_book->get_all_my_books($sort_name, $sort_by, $offset, $q,$tbl_category_id,'',$tbl_class_id,$tbl_school_id);
			$total_books = $this->model_book->get_all_my_total_books($q,$tbl_category_id,'',$tbl_class_id,$tbl_school_id);
				
			$data['list_books']        = $list_books;
			$data['total_books']       = $total_books;
			
		}else{
			$data['list_books']        = array();;
			$data['total_books']       = 0;
		}

		$response = "Success";
		$errorMsg = "Books List";
		$response_array = array('response' => $response, 'result' => $data, 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
	}
	
	
	public function book_details()
	{
		$data['page']     = "view_books_list";
		$data['menu']     = "school_library";
        $data['sub_menu'] = "library_books";
		$data['mid'] = "4";
		
	   
	    $param_array = $this->uri->uri_to_assoc(3);
	    if (array_key_exists('bid',$param_array)) {
			$bid = $param_array['bid'];
		}
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
		}
		
		$this->load->model('model_book');	
		$this->load->model('model_category');
			
		$tbl_book_id = $bid;
		$getBookInfo = $this->model_book->getBookInfo($tbl_book_id);
		
		
		$tbl_book_category_id   = $getBookInfo[0]['tbl_book_category_id'];
		$category_info = $this->model_category->getCategoryDetails($tbl_book_category_id);
		$data['category_name_en'] = $category_info[0]['category_name_en'];
		$data['category_name_ar'] = $category_info[0]['category_name_ar'];
		
		$data['getBookInfo']       = $getBookInfo;
		$data['offset']            = $offset;
		$this->load->view('students/view_student_template', $data);
	}
	
	
	
	
	public function list_books()
	{
		
		$this->load->model('model_book');
		$sort_name = "";
		$sort_by   = "";
		$offset    = "0";  
		$q		 = "";
		
	
		$tbl_school_id 			= isset($_REQUEST["tbl_school_id"])? $_REQUEST["tbl_school_id"]:'' ;
		$tbl_class_id 			= isset($_REQUEST["tbl_class_id"])? $_REQUEST["tbl_class_id"]:'';
		$tbl_user_id     		= isset($_REQUEST['tbl_user_id'])? $_REQUEST['tbl_user_id']:'';
		$lan             		= isset($_REQUEST['lan'])? $_REQUEST['lan']:'ar';
		$recent_books = $this->model_book->get_recent_books($sort_name, $sort_by, $offset, $q, '',$tbl_class_id,$lan);
		$data['recent_books'] = $recent_books;
		
		$categorized_books = $this->model_book->get_categorized_books($sort_name, $sort_by, $offset, $q, $tbl_class_id,$lan);
		$data['categorized_books'] = $categorized_books;
		
	
	    //echo "<pre>";
		//print_r($data);
		$response = "Success";
		$errorMsg = "Books listed successfully";
		$response_array = array('response' => $response, 'result' => $data, 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
	}
	
	
	
	
	
	

	
	
}
