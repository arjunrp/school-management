<?php

/**
 * @desc   	  	Bus Controller
 *
 * @category   	Controller
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Bus extends CI_Controller {


	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
    function index() {
		//Do nothing
	}


	/**
	* @desc    Image Gallery
	*
	* @param   none
	* @access  default
	*/
    function get_student_bus_tracking() {
		$user_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		
		$data["user_id"] = $user_id;
		$data["role"] = $role;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] = $school_id;
		$data["tbl_student_id"] = $tbl_student_id;
		
		$this->load->model('model_student');
		$tbl_bus_id = $this->model_student->get_student_bus($tbl_student_id);
		
		$this->load->model('model_bus');
		$data_rs = $this->model_bus->get_student_bus_tracking_live($tbl_bus_id);
		
		if (count($data_rs)<= 0) {
			$arr["code"] = "200";
		} else {
			$arr["latitude"] = $data_rs[0]["latitude"];
			$arr["longitude"] = $data_rs[0]["longitude"];
		}
		echo json_encode($arr);
		return;
	}
	
	
	/**
	* @desc    Image Gallery
	*
	* @param   none
	* @access  default
	*/
    function load_attendance_options_page_teacher() {
		/*$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		
		$data["page"] = "view_attendance_options_page_teacher";
		$this->load->view('view_template',$data);*/
	}
	
	
	/**
	* @desc    Image Gallery
	*
	* @param   none
	* @access  default
	*/
    function get_student_bus_tracking_page() {
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		
		$this->load->model('model_student');
		$tbl_bus_id = $this->model_student->get_student_bus($tbl_student_id);
		
		$this->load->model('model_bus');
		$lat_long =$this->model_bus->get_student_bus_tracking_live($tbl_bus_id);
		//echo "Here 222";
		//exit();
		$lat_long_arr = explode(",",$lat_long);
		$latitude = $lat_long_arr[0];
		$longitude = $lat_long_arr[1];
		
		$data["page"] = "view_student_bus_tracking_page";
		$this->load->view('view_template',$data);
	}
	
	
	/**
	* @desc    Image Gallery
	*
	* @param   none
	* @access  default
	*/
    function bus_tracking_login_page() {
		/*$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		
		$data["page"] = "view_bus_tracking_login_page";
		$this->load->view('view_template',$data);*/
	}
	
	
	/**
	* @desc    Image Gallery
	*
	* @param   none
	* @access  default
	*/
    function start_bus_tracking_session() {
		/*$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		
		$device_uid = $_REQUEST["device_uid"];
		$tbl_bus_id = $_REQUEST["tbl_bus_id"];
		
		$is_exist = authenticate_bus($bus_code, $bus_password);
		if ($is_exist == "D") {
			echo "--D--";	// User could not be authenticated as user Does Not Exist
			exit();
		} else if ($is_exist == "N") {
			echo "--N--";
			exit();
		} else if ($is_exist == "Y") {
			// Get bus id
			$tbl_bus_id = get_bus_id($bus_code);
			start_bus_tracking_session($tbl_bus_id, $device_uid, $device);
		} else {
			echo "--no--";
			exit();
		}
		$this->load->model('model_bus');
		$this->model_bus->start_bus_tracking_session($tbl_bus_id, $device_uid, $device, $tbl_school_id);
		$data["page"] = "view_start_bus_tracking_page";
		$this->load->view('view_template',$data); */
	}
	
	
	/**
	* @desc    Image Gallery
	*
	* @param   none
	* @access  default
	*/
    function stop_bus_tracking() {
		/*$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		
		$tbl_bus_id = $_REQUEST["tbl_bus_id"];
		$device_uid = $_REQUEST["device_uid"];
		
		$this->load->model('model_bus');
		$this->model_bus->stop_bus_tracking($tbl_bus_id, $device_uid);
		echo "--Y--";	*/
	}
	
	
	/**
	* @desc    Save bus lat and log
	*
	* @param   none
	* @access  default
	*/
    function save_bus_lat_long() {
		$lan = $_REQUEST["lan"];
		$role = $_REQUEST["role"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["school_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		
		$tbl_bus_id = $_REQUEST["user_id"];
		$device_uid = $_REQUEST["device_uid"];
		$latitude = $_REQUEST["latitude"];
		$longitude = $_REQUEST["longitude"];
		
		$this->load->model('model_bus');
		$is_exist = $this->model_bus->check_bus_device_session($tbl_bus_id, $device_uid);
		
		// Bus tracking session is not there. This happens when we login from another device which voids/logs out old session or bus user voluntarilty logs out application
		if ($is_exist == "N") {
			echo "--no--";			// Please login again
			return;
		}
		
		$this->load->model('model_bus');
		$this->model_bus->save_bus_lat_long($tbl_bus_id, $latitude, $longitude, $device_uid, $tbl_school_id);
		echo "--yes--";
	}
	
	
	/**
	* @desc    Bus user logs out
	*
	* @param   none
	* @access  default
	*/
    function bus_log_out() {
		$lan = $_REQUEST["lan"];
		$role = $_REQUEST["role"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["school_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		
		$tbl_bus_id = $_REQUEST["user_id"];
		$device_uid = $_REQUEST["device_uid"];
		
		$this->load->model('model_bus');
		$this->model_bus->bus_log_out($tbl_bus_id);
		
		echo "--yes--";
	}
	
	
	
	/**
	* @desc    Login bus module
	*
	* @param   none
	* @access  default
	*/
    function load_attendance_options_page() {
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		
		$bus_code = $_REQUEST["bus_code"];
		$bus_password = $_REQUEST["bus_password"];
		
		$device_uid = $_REQUEST["device_uid"];
		
		$attendance_date = date("Y-m-d");
		$this->load->model('model_bus');
		$is_exist = $this->model_bus->authenticate_bus($bus_code, $bus_password);
		
		if ($is_exist == "D") {
			echo "--D--";	// User could not be authenticated as user Does Not Exist
			exit();
		} else if ($is_exist == "N") {
			echo "--N--";
			exit();
		} else if ($is_exist == "Y") {
			$tbl_bus_id = $this->model_bus->get_bus_id($bus_code);
			$tbl_school_id = $this->model_bus->get_tbl_school_id($bus_code);
		} else {
			echo "--no--";
			exit();
		}
		
		// Once successfully logged in we need to delete old tracking session to avoid bus tracking from different devices.
		$this->model_bus->set_bus_tracking_session($tbl_bus_id, $device_uid, $device, $tbl_school_id);
		
		$this->load->model('model_bus_sessions');
		$data_rs = $this->model_bus_sessions->get_bus_sessions($tbl_school_id);
		$arr_data["tbl_bus_id"] = $tbl_bus_id;
		$arr_data["tbl_school_id"] = $tbl_school_id;
		$arr_data["bus_sessions"] = $data_rs;
		
		echo json_encode($arr_data);
	}
}
?>

