<?php



/**

 * @desc   	  	Bus Absent Details Controller

 *

 * @category   	Controller

 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>

 * @version    	0.1

 */

class Bus_absent_details extends CI_Controller {





	/**

	* @desc    Default function for the Controller

	*

	* @param   none

	* @access  default

	*/

    function index() {

		//Do nothing

	}





	/**

	* @desc    Get details of student who is absent in bus and for which absent details were already submitted

	*

	* @param   none

	* @access  default

	*/

    function get_bus_absent_details() {

		$tbl_bus_id = $_REQUEST["user_id"];

		$role = $_REQUEST["role"];

		$lan = $_REQUEST["lan"];

		$device = $_REQUEST["device"];

		$device_uid = $_REQUEST["device_uid"];

		$tbl_school_id = $_REQUEST["school_id"];

		$tbl_bus_absent_details_id = $_REQUEST["tbl_bus_absent_details_id"];

		

		$this->load->model('model_teachers');

		$this->load->model('model_attendance');

		$this->load->model('model_admin');

		$this->load->model('model_parents');

		$this->load->model('model_fixed_message');

		$this->load->model('model_bus_absent_details');

		$this->load->model('model_student');

		$this->load->model('model_classes');

	

		$data_rs = $this->model_bus_absent_details->get_bus_absent_details($tbl_bus_absent_details_id);

		//print_r($data_rs);

		if ($data_rs[0]["message_type"] != "") {

			$message_type = $data_rs[0]["message_type"];

			$message_from = $data_rs[0]["message_from"];

			$tbl_student_id = $data_rs[0]["tbl_student_id"];

			

			$data_student = $this->model_student->get_student_obj($tbl_student_id);

			$tbl_class_id = $data_student[0]['tbl_class_id'];

			

			$data_class = $this->model_classes->getClassInfo($tbl_class_id);

			if ($lan == "ar") {

				$arr_data["class_name"] = $data_class[0]["class_name_ar"];

			} else {

				$arr_data["class_name"] = $data_class[0]["class_name"];

			}

			

			$tbl_section_id = $data_class[0]['tbl_section_id'];

			$data_section = $this->model_classes->getClassSectionInfo($tbl_section_id);

			if ($lan == "ar") {

				$arr_data["section_name"] = $data_section[0]["section_name"];

			} else {

				$arr_data["section_name"] = $data_section[0]["section_name_ar"];

			}

			

			$data_teacher = $this->model_teachers->get_class_teacher_details($tbl_student_id);

			$arr_data["mobile_teacher"] = $data_teacher[0]["mobile"];

			

			if ($lan == "ar") {

				$arr_data["first_name_teacher"] = $data_teacher[0]["first_name_ar"];

				$arr_data["last_name_teacher"] = $data_teacher[0]["last_name_ar"];

			} else {

				$arr_data["first_name_teacher"] = $data_teacher[0]["first_name"];

				$arr_data["last_name_teacher"] = $data_teacher[0]["last_name"];

			}

			

			$tbl_fixed_message_id = $data_rs[0]["tbl_fixed_message_id"];

			$data_fm = $this->model_fixed_message->get_fixed_message($tbl_fixed_message_id);

			if ($lan == "ar") {

				$message = $data_fm[0]["message_ar"];

			}else{

				$message = $data_fm[0]["message_en"];

			}

			

			if ($message_type == "A") {	

				

				// School Admin

				$data_admin = $this->model_admin->get_admin_user_obj($message_from);

				$first_name = $data_admin[0]["first_name"];

				$last_name = $data_admin[0]["last_name"];

				$phone = $data_admin[0]["phone"];

				

				$arr_data["first_name_admin"] = $first_name;

				$arr_data["last_name_admin"] = $last_name;

				$arr_data["phone_admin"] = $phone;

				

				$tbl_parent_id = $this->model_parents->get_parent_of_student($tbl_student_id);

				$data_p = $this->model_parents->get_parent_obj($tbl_parent_id);

				$phone = $data_p[0]["mobile"];

				$latitude = $data_p[0]["latitude"];

				$longitude = $data_p[0]["longitude"];

				

				if ($lan == "ar") {

					$first_name = $data_p[0]["first_name_ar"];

					$last_name = $data_p[0]["last_name_ar"];

				} else {

					$first_name = $data_p[0]["first_name"];

					$last_name = $data_p[0]["last_name"];

				}

					

			} else if ($message_type == "P") {			// Parent

				$arr_data["first_name_admin"] = "";

				$arr_data["last_name_admin"] = "";

				$arr_data["phone_admin"] = "";

				

				$data_p = $this->model_parents->get_parent_obj($message_from);

				$phone = $data_p[0]["mobile"];

				$latitude = $data_p[0]["latitude"];

				$longitude = $data_p[0]["longitude"];

				if ($lan == "ar") {

					$first_name = $data_p[0]["first_name_ar"];

					$last_name = $data_p[0]["last_name_ar"];

				} else {

					$first_name = $data_p[0]["first_name"];

					$last_name = $data_p[0]["last_name"];

				}

			}

			

			if ($latitude == "") {

				$latitude = "0";

			}

			if ($longitude == "") {

				$longitude = "0";

			}

			

			$arr_data["message_type"] = $message_type;			// A = Admin User, P = Parent

			$arr_data["first_name"] = $first_name;

			$arr_data["last_name"] = $last_name;

			$arr_data["phone"] = $phone;

			$arr_data["message"] = $message;					

			$arr_data["latitude"] = $latitude;

			$arr_data["longitude"] = $longitude;

			

			$arr["absent_message"] = $arr_data;

			echo json_encode($arr);

		} else {

			echo "--no--";

		}

		return;

	}

	

	

	/**

	* @desc    Image Gallery

	*

	* @param   none

	* @access  default

	*/

    function child_bus_attendance() {

		$user_id = $_REQUEST["user_id"];

		$role = $_REQUEST["role"];

		$lan = $_REQUEST["lan"];

		$device = $_REQUEST["device"];

		$device_uid = $_REQUEST["device_uid"];

		$school_id = $_REQUEST["school_id"];

		$tbl_student_id = $_REQUEST["tbl_student_id"];

		

		$data["user_id"] = $user_id;

		$data["role"] = $role;

		$data["lan"] = $lan;

		$data["device"] = $device;

		$data["device_uid"] = $device_uid;

		$data["school_id"] = $school_id;

		$data["tbl_student_id"] = $tbl_student_id;

		

		$this->load->model('model_attendance');

		$data_rs = $this->model_attendance->get_child_bus_attendance($tbl_student_id);



		if (count($data_rs) <=0) {

			$arr["code"] = "200";

			echo json_encode($arr);

			return;

		}

		

		//print_r($data_rs );

		$data["data_rs"] = $data_rs;

		$data["page"] = "view_attendance_page_bus";

		$this->load->view('view_template',$data);

	}

	

	

	/**

	* @desc    Image Gallery

	*

	* @param   none

	* @access  default

	*/

    function attendance_page() {

		$lan = $_REQUEST["lan"];

		$device = $_REQUEST["device"];

		$device_uid = $_REQUEST["device_uid"];

		$tbl_school_id = $_REQUEST["tbl_school_id"];

		$data["lan"] = $lan;

		$data["device"] = $device;

		$data["device_uid"] = $device_uid;

		$data["tbl_school_id"] = $tbl_school_id;	

		

		$tbl_bus_id = $_REQUEST["tbl_bus_id"];

		$att_option = $_REQUEST["att_option"];

		date_default_timezone_set('Asia/Dubai');



		$attendance_date = date("Y-m-d");

		$data["attendance_date"] = $attendance_date;	

		

		//print_r(date("d/m/Y"));

		$this->load->model('model_student');

		$data_rs = $this->model_student->get_all_bus_students($tbl_bus_id);

		if (count($data_rs)<=0) {

			echo "--no--";

			return;

		}

		$data["page"] = "view_attendance_page";

		$this->load->view('view_template',$data);

	}

	

	

	/**

	* @desc    Image Gallery

	*

	* @param   none

	* @access  default

	*/

    function submit_attendance() {

		$lan = $_REQUEST["lan"];

		$device = $_REQUEST["device"];

		$device_uid = $_REQUEST["device_uid"];

		$tbl_school_id = $_REQUEST["tbl_school_id"];

		$data["lan"] = $lan;

		$data["device"] = $device;

		$data["device_uid"] = $device_uid;

		$data["tbl_school_id"] = $tbl_school_id;

		

		date_default_timezone_set('Asia/Dubai');

		

		$att_str = $_REQUEST["att_str"];

		$tbl_bus_id = $_REQUEST["tbl_bus_id"];

		$attendance_date = date("Y-m-d");

		$att_option = $_REQUEST["att_option"];

		

		$this->load->model('model_attendance');

		$this->model_attendance->delete_attendance($tbl_bus_id, $attendance_date, $att_option);

		 

		if ($att_option == "M") {	// Morning Attendance

			$message_ = " has successfully reached school.";	// Afternoon Attendance

		} else {

			$message_ = " has left for home on school transport (Bus).";	// Afternoon Attendance					

		}

		$tbl_teacher_id_str = "";

		$tbl_class_id_str = "";

		$att_str_arr = explode(",",$att_str);

		

		for ($i=0; $i<(count($att_str_arr)-1); $i++) {

			$tbl_student_id_arr = explode(":",$att_str_arr[$i]);

			$tbl_student_id = $tbl_student_id_arr[0];

			$is_present = $tbl_student_id_arr[1];

			if (trim($tbl_student_id) == "") {continue;}

			

			$this->load->model('model_attendance');

			$this->model_attendance->save_attendance($tbl_student_id, $tbl_bus_id, $attendance_date, $is_present, $att_option, $tbl_school_id);

			

			if ($att_option == "M" && $is_present == "Y") {

				$message_ = " has entered the school bus.";

			} else if ($att_option == "M" && $is_present == "N") {

				$message_ = " did not reach for school bus.";

			}

			

			$this->load->model('model_parents');

			$tbl_parent_id = $this->model_parents->get_parent_of_student($tbl_student_id);

			

			$this->load->model('model_student');

			$stu_obj = $this->model_student->get_student_obj($tbl_student_id);

			$first_name_stu = $stu_obj[0]['first_name'];

			$last_name_stu = $stu_obj[0]['last_name'];

			$tbl_class_id = $stu_obj[0]['tbl_class_id'];

			$tbl_class_id_str = $tbl_class_id_str."'".$tbl_class_id."',";

			$message = 'Dear Parent, your Child '.$first_name_stu.' '.$last_name_stu.' '.$message_;

			//save_attendance($tbl_teacher_id, $tbl_student_id, $tbl_parent_id, $option1, $option2, $option3, $option4, $message);

			

			$this->load->model('model_user_notify_token');

			$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_parent_id);

			

			for ($b=0; $b<count($data_tkns); $b++) {

				//$token = $tbl_parent_id."-".$data_tkns[$b]["token"];

				$token = $data_tkns[$b]["token"];
				$device = $data_tkns[$b]["device"];

				if (trim(ENABLE_PUSH) == "Y") {	

					$message = urlencode($message);

					//$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$tbl_parent_id."&msg=".$message."&data=";

					$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$token."&msg=".$message."&data=";

					

					$this->load->model('model_user_notify_token');

					$this->model_user_notify_token->send_notification($token , $message, $device);

					//echo "<br>".$url;

					//$ch = curl_init();

					//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

					//curl_setopt($ch, CURLOPT_URL, $url);

					//curl_setopt($ch, CURLOPT_HEADER, 0);

					//curl_exec($ch);

					//curl_close($ch);

				}//if (trim(ENABLE_PUSH) == "Y")

			}

		}

		echo "--yes--";

	}

	

	

	/**

	* @desc    Image Gallery

	*

	* @param   none

	* @access  default

	*/

    function send_absent_notification() {

		$lan = $_REQUEST["lan"];

		$device = $_REQUEST["device"];

		$device_uid = $_REQUEST["device_uid"];

		$tbl_school_id = $_REQUEST["tbl_school_id"];

		$data["lan"] = $lan;

		$data["device"] = $device;

		$data["device_uid"] = $device_uid;

		$data["tbl_school_id"] = $tbl_school_id;

		

		$tbl_student_id = $_REQUEST["tbl_student_id"];

		$module_name = $_REQUEST["module_name"];

		

		$this->load->model('model_parents');

		$tbl_parent_id = $this->model_parents->get_parent_of_student($tbl_student_id);

		

		$this->load->model('model_student');

		$stu_obj = $this->model_student->get_student_obj($tbl_student_id);

		$first_name_stu = $stu_obj[0]['first_name'];

		$last_name_stu = $stu_obj[0]['last_name'];

		$tbl_class_id = $stu_obj[0]['tbl_class_id'];

		

		$message_ = " has not arrived for the school bus at subscribed location.";	// Afternoon Attendance					

		$message = 'Dear Parent, your Child '.$first_name_stu.' '.$last_name_stu.' '.$message_;

		

		$this->load->model('model_user_notify_token');

		$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_parent_id);

		

		for ($b=0; $b<count($data_tkns); $b++) {

			if (trim(ENABLE_PUSH) == "Y") {	

				$message = urlencode($message);

				//$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$tbl_parent_id."&msg=".$message."&data=";

				//$token = $tbl_parent_id."-".$data_tkns[$b]["token"];

				$token = $data_tkns[$b]["token"];
				$device = $data_tkns[$b]["device"];

				$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$token."&msg=".$message."&data=";

				

				$this->load->model('model_user_notify_token');

				$this->model_user_notify_token->send_notification($token , $message, $device);//$ch = curl_init();

				

				//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

				//curl_setopt($ch, CURLOPT_URL, $url);

				//curl_setopt($ch, CURLOPT_HEADER, 0);

				//curl_exec($ch);

				//curl_close($ch);

			}

		}

		echo "--yes--";

	}

	

	

	

	 function get_bus_absentees_message() {

		$tbl_bus_id = $_REQUEST["user_id"];

		$role = $_REQUEST["role"];

		$lan = $_REQUEST["lan"];

		$device = $_REQUEST["device"];

		$device_uid = $_REQUEST["device_uid"];

		$tbl_school_id = $_REQUEST["school_id"];

		$tbl_bus_sessions_id 	= $_REQUEST["tbl_bus_sessions_id"];

		$attendance_date 		= $_REQUEST["attendance_date"];

		if (trim($attendance_date) == "") {

			date_default_timezone_set('Asia/Dubai');

			$attendance_date = date("Y-m-d");

		}

		

		$this->load->model('model_teachers');

		$this->load->model('model_attendance');

		$this->load->model('model_admin');

		$this->load->model('model_parents');

		$this->load->model('model_fixed_message');

		$this->load->model('model_bus_absent_details');

		$this->load->model('model_bus_sessions');

		

		$data_rs = $this->model_bus_absent_details->get_bus_leave_messages($tbl_bus_id,$attendance_date, $tbl_school_id);

		$arr_data = array();

		for($k=0;$k<count($data_rs);$k++)

		{

			//print_r($data_rs);

			if ($data_rs[$k]["message_type"] != "") {

				$message_type = $data_rs[$k]["message_type"];

				$message_from = $data_rs[$k]["message_from"];

				$tbl_student_id = $data_rs[$k]["tbl_student_id"];

				$tbl_bus_sessions_id = $data_rs[$k]["tbl_bus_sessions_id"];

				

				$arr_data[$k]["tbl_bus_sessions_id"] = $tbl_bus_sessions_id;

				

				$data_session = $this->model_bus_sessions->get_bus_session_details($tbl_bus_sessions_id);

				if($lan == "ar") {

					$bus_session_title 	= $data_session[0]["title_ar"];

				}else{

					$bus_session_title 	= $data_session[0]["title"];

				}

				$arr_data[$k]["bus_session_title"] = $bus_session_title;

				$data_teacher = $this->model_teachers->get_class_teacher_details($tbl_student_id);

				$arr_data[$k]["mobile_teacher"] = $data_teacher[0]["mobile"];

				

				if ($lan == "ar") {

					$arr_data[$k]["first_name_teacher"] = $data_teacher[0]["first_name_ar"];

					$arr_data[$k]["last_name_teacher"] = $data_teacher[0]["last_name_ar"];

				} else {

					$arr_data[$k]["first_name_teacher"] = $data_teacher[0]["first_name"];

					$arr_data[$k]["last_name_teacher"] = $data_teacher[0]["last_name"];

				}

				

				$tbl_fixed_message_id = $data_rs[$k]["tbl_fixed_message_id"];

				$data_fm = $this->model_fixed_message->get_fixed_message($tbl_fixed_message_id);

				if ($lan == "ar") {

					$message = $data_fm[0]["message_ar"];

				}else{

					$message = $data_fm[0]["message_en"];

				}

				

				if ($message_type == "A") {	

					

					// School Admin

					$data_admin = $this->model_admin->get_admin_user_obj($message_from);

					$first_name = $data_admin[0]["first_name"];

					$last_name = $data_admin[0]["last_name"];

					$phone = $data_admin[0]["phone"];

					

					$arr_data[$k]["first_name_admin"] = $first_name;

					$arr_data[$k]["last_name_admin"] = $last_name;

					$arr_data[$k]["phone_admin"] = $phone;

					

					$tbl_parent_id = $this->model_parents->get_parent_of_student($tbl_student_id);

					$data_p = $this->model_parents->get_parent_obj($tbl_parent_id);

					$phone = $data_p[0]["mobile"];

					$latitude = $data_p[0]["latitude"];

					$longitude = $data_p[0]["longitude"];

					

					if ($lan == "ar") {

						$first_name = $data_p[0]["first_name_ar"];

						$last_name = $data_p[0]["last_name_ar"];

					} else {

						$first_name = $data_p[0]["first_name"];

						$last_name = $data_p[0]["last_name"];

					}

						

				} else if ($message_type == "P") {			// Parent

					$arr_data[$k]["first_name_admin"] = "";

					$arr_data[$k]["last_name_admin"] = "";

					$arr_data[$k]["phone_admin"] = "";

					

					$data_p = $this->model_parents->get_parent_obj($message_from);

					$phone = $data_p[0]["mobile"];

					$latitude = $data_p[0]["latitude"];

					$longitude = $data_p[0]["longitude"];

					if ($lan == "ar") {

						$first_name = $data_p[0]["first_name_ar"];

						$last_name = $data_p[0]["last_name_ar"];

					} else {

						$first_name = $data_p[0]["first_name"];

						$last_name = $data_p[0]["last_name"];

					}

				}

				

				if ($latitude == "") {

					$latitude = "0";

				}

				if ($longitude == "") {

					$longitude = "0";

				}

				

				$arr_data[$k]["message_type"] = $message_type;			// A = Admin User, P = Parent

				$arr_data[$k]["first_name"] = $first_name;

				$arr_data[$k]["last_name"] = $last_name;

				$arr_data[$k]["phone"] = $phone;

				$arr_data[$k]["message"] = $message;					

				$arr_data[$k]["latitude"] = $latitude;

				$arr_data[$k]["longitude"] = $longitude;

				

				

			} else {

				$arr_data = "";

			}

		        

		}

		$arr["absent_message"] = $arr_data;

	    echo json_encode($arr);

		return;

	}

	

	

	

}

?>



