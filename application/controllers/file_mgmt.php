<?php

/**
 * @desc   	  	File mgmt Controller
 *
 * @category   	Controller
 * @author     	Shanavas.PK
 * @version    	0.1
 */
class File_mgmt extends CI_Controller {


	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
    function index() {
	}


	/**
	* @desc    Upload file
	*
	* @param   none
	* @access  default
	*/
    function upload_the_file() {
		$module_name 	= $_POST["module_name"];
		$item_id 		= $_POST["item_id"];	
		$is_ajax 		= $_POST["is_ajax"];
		$upload_from 	= $_POST["upload_from"];

	 	$file_name_original = $_FILES["myfile"]["name"];
		$Ext = strchr($file_name_original,".");
		$Ext = strtolower($Ext);
		$file_name_updated_ = md5(uniqid(rand()));
		$file_name_updated = $file_name_updated_.$Ext;
		$file_name_original_temp = $_FILES["myfile"]["tmp_name"];
		$file_type = $_FILES["myfile"]["type"];
		$file_size = $_FILES["myfile"]["size"];
		
		//$copy_file = @copy($file_name_original_temp, ); 
		if($module_name=="admin_user")
		{
			move_uploaded_file($file_name_original_temp, UPLOADS_PATH."/".$file_name_updated);
		}else if($module_name=="parenting_logo" || $module_name=="parenting_file")
		{
			move_uploaded_file($file_name_original_temp, UPLOADS_PATH."/".$file_name_updated);
		}else if($module_name=="student"){
			move_uploaded_file($file_name_original_temp, UPLOAD_PATH_STUDENT."/".$file_name_updated);
		}else if($module_name=="teacher"){
			move_uploaded_file($file_name_original_temp, UPLOAD_PATH_TEACHER."/".$file_name_updated);
		}else if($module_name=="book_category"){
			move_uploaded_file($file_name_original_temp, UPLOAD_PATH_BOOK."/".$file_name_updated);
		}		
		else if($module_name=="school_gallery_large" || $module_name=="school_gallery_category"){
			move_uploaded_file($file_name_original_temp, UPLOAD_IMG_GALLERY_PATH."/".$file_name_updated);
		}
		else if($module_name=="school"){
			move_uploaded_file($file_name_original_temp, IMG_UPLOAD_PATH_LOGO."/".$file_name_updated);
		}
		else{
			move_uploaded_file($file_name_original_temp, UPLOADS_PATH."/".$file_name_updated);
		}
		
		$tbl_uploads_id = md5(uniqid(rand()));

        $this->load->model('model_file_mgmt');
		
        $this->model_file_mgmt->save_file($tbl_uploads_id, $module_name, $item_id, $file_name_original, $file_name_updated, $file_type, $file_size);
		
		if (($is_ajax == true || $is_ajax == "true") && $module_name=="student") {
			$arr = array('url'=>IMG_PATH_STUDENT."/".$file_name_updated, 'tbl_item_id'=>$item_id);
			echo json_encode($arr);	
			exit();
		}else if (($is_ajax == true || $is_ajax == "true") && $module_name=="teacher") {
			$arr = array('url'=>IMG_PATH_TEACHER."/".$file_name_updated, 'tbl_item_id'=>$item_id);
			echo json_encode($arr);	
			exit();
		}
		
		$arr = array('tbl_uploads_id'=>$tbl_uploads_id, 'file_name_updated'=>$file_name_updated);
		
	echo json_encode($arr);	
	}

	/**
	* @desc    Delete file
	*
	* @param   none
	* @access  default
	*/
    function delete_file() {
		$tbl_uploads_id = $_POST["tbl_uploads_id"];

        $this->load->model('model_file_mgmt');
        $this->model_file_mgmt->delete_file($tbl_uploads_id);
	}
	
	
	/****************************************************************************/
	/****************************************************************************/
	 function cover_photo_upload() {
		
		$item_id 	 		= $_POST["item_id"];	
	 	$file_name_original = $_FILES["myfile"]["name"];
		$Ext 				= strchr($file_name_original,".");
		$Ext 				= strtolower($Ext);
		$file_name_updated_ = md5(uniqid(rand()));
		$file_name_updated 	= $file_name_updated_.$Ext;
		$file_name_original_temp = $_FILES["myfile"]["tmp_name"];
		$file_type 				 = $_FILES["myfile"]["type"];
		$file_size 				 = $_FILES["myfile"]["size"];
		
		move_uploaded_file($file_name_original_temp, UPLOAD_PATH_BOOK."/".$file_name_updated);

        $this->load->model('model_book');
		$tbl_book_id = $item_id;
        $this->model_book->upload_book_cover($file_name_updated,$tbl_book_id);
		$arr = array('tbl_uploads_id'=>$tbl_book_id, 'file_name_updated'=>$file_name_updated);
		
	  echo json_encode($arr);	
	}
	
	 function book_file_upload() {
		$item_id 			= $_POST["item_id"];	
	 	$file_name_original = $_FILES["myfile"]["name"];
		$Ext 				= strchr($file_name_original,".");
		$Ext 				= strtolower($Ext);
		$file_name_updated_ = md5(uniqid(rand()));
		$file_name_updated 	= $file_name_updated_.$Ext;
		$file_name_original_temp = $_FILES["myfile"]["tmp_name"];
		$file_type 				 = $_FILES["myfile"]["type"];
		$file_size 				 = $_FILES["myfile"]["size"];
        $tbl_book_id             = $item_id;
		if ($Ext==".pdf" || $Ext==".epub" ) {
			move_uploaded_file($file_name_original_temp, UPLOAD_PATH_BOOK."/".$file_name_updated);
        	$this->load->model('model_book');
        	$this->model_book->upload_book_file($file_name_updated,$tbl_book_id);
			$arr = array('tbl_uploads_id'=>$tbl_uploads_id, 'file_name_updated'=>$file_name_updated);
		}
		
	  echo json_encode($arr);	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
   
}
?>