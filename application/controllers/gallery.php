<?php
/**
 * @desc   	  	Gallery Controller
 * @category   	Controller
 */

class Gallery extends CI_Controller {

	/**
	* @desc    Default function for the Controller
	* @param   none
	* @access  default
	*/
    function index() {
		//Do nothing
	}
	/**
	* @desc    Image Gallery
	*/
    function image_gallery_categories() {
		$user_id 	= $_REQUEST["user_id"];
		$role 		= $_REQUEST["role"];
		$lan 		= $_REQUEST["lan"];
		$device 	= $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id 	= $_REQUEST["school_id"];

		$data["user_id"] 	= $user_id;
		$data["role"] 		= $role;
		$data["lan"] 		= $lan;
		$data["device"] 	= $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"]  = $school_id;

		$this->load->model('model_gallery');
		$data_gallery_categories = $this->model_gallery->get_gallery_categories($school_id);

		if (count($data_gallery_categories) <=0) {
			$arr["code"] = "200";
			echo json_encode($arr);
			return;
		}

		$data["data_gallery_categories"] = $data_gallery_categories;
		$data["page"] = "view_image_gallery_categories";
		$this->load->view('view_template',$data);
	}

	/**
	* @desc    Image Gallery
	* @param   none
	* @access  default
	*/

    function gallery_images() {
		$user_id 	= $_REQUEST["user_id"];
		$role 		= $_REQUEST["role"];
		$lan        = $_REQUEST["lan"];
		$device     = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id  = $_REQUEST["school_id"];
		$tbl_gallery_category_id = $_REQUEST["tbl_gallery_category_id"];

		$data["user_id"] 	= $user_id;
		$data["role"]    	= $role;
		$data["lan"]     	= $lan;
		$data["device"]  	= $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] = $school_id;

		$data["tbl_gallery_category_id"] = $tbl_gallery_category_id;

		$this->load->model('model_gallery');
		$data_category = $this->model_gallery->get_gallery_category_name($tbl_gallery_category_id);
		$data_gallery_images = $this->model_gallery->get_gallery_category_images($tbl_gallery_category_id);

		if (count($data_gallery_images) <=0) {
			$arr["code"] = "200";
			echo json_encode($arr);
			return;
		}

		if ($lan == "en") {
			$category_name = $data_category[0]["category_name_en"];
		} else {
			$category_name = $data_category[0]["category_name_ar"];
		}

		$data["category_name"] = $category_name;
		$data["data_gallery_images"] = $data_gallery_images;
		$data["page"] = "view_image_gallery_images";
		$this->load->view('view_template',$data);
	}

	/**
	* @desc    Image Gallery
	* @param   none
	* @access  default
	*/

    function gal_img() {
		$lan 			= $_REQUEST["lan"];
		$device 		= $_REQUEST["device"];
		$device_uid 	= $_REQUEST["device_uid"];
		$tbl_school_id 	= $_REQUEST["tbl_school_id"];

		$data["lan"] 			= $lan;
		$data["device"] 		= $device;
		$data["device_uid"] 	= $device_uid;
		$data["tbl_school_id"] 	= $tbl_school_id;

		$tbl_image_gallery_id 	= $_REQUEST["tbl_image_gallery_id"];
		$this->load->model('model_gallery');

		$data_gallery_image = $this->model_gallery->get_gallery_image($tbl_image_gallery_id);
		if (count($data_gallery_image)<=0) {
			echo "--no--";
			return;
		}
		$data["page"] = "view_image_gallery_image";
		$this->load->view('view_template',$data);
	}

}

?>



