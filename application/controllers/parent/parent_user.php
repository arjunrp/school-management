<?php

/**
 * @desc   	  	Parent User Controller
 * @category   	Controller
 * @author     	Shanavas.PK
 * @version    	0.1
 */
class Parent_user extends CI_Controller {


	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
    function index() {
		
		$this->login_form();
	}


	/**
	* @desc    Login page
	*
	* @param   none
	* @access  default
	*/
    function login_form() {
		$data['page'] = "login_form";
		$data['user_type'] = "parent";
		$this->load->view("parent/login_form", $data);
	}


	/**
	* @desc    Validate user credentials and generate session
	* @param   none
	* @access  default
	*/
    function ajax_validate_and_login_user() {
		if(isset($_POST) && count($_POST) != 0) {
			$username = trim($_POST["username"]);
			$password = trim($_POST["password"]);
			$remember_me = trim($_POST["remember_me"]);
			$is_ajax = trim($_POST["is_ajax"]);
		} else {
			echo "*N*";	
		   exit;
		}

		if (trim($remember_me) == "Y") {
			$year = time() + 31536000;
			setcookie('remember_me', $username, $year);	
		} else {
			if(isset($_COOKIE['remember_me'])) {
				$past = time() - 100;
				setcookie('remember_me', '', $past);
			}
		}
		
		$module_access  = "P";
		$this->load->model("model_parents");
		$is_exist = $this->model_parents->authenticate($username, $password);
			switch($is_exist) {
				case("N"): {//User does not exist
					echo "*N*";
					exit;
					}
				case("Y"): {
	
				$this->load->model("model_parents");
				$tbl_parent_id 	= $this->model_parents->get_parent_id($username);
				$parent_obj 	= $this->model_parents->get_parent_obj($tbl_parent_id);
				$data["first_name"] 		= $parent_obj[0]["first_name"];
				$data["first_name_ar"] 		= $parent_obj[0]["first_name_ar"];
				$data["last_name"] 			= $parent_obj[0]["last_name"];
				$data["last_name_ar"] 		= $parent_obj[0]["last_name_ar"];
				$data["data_stu"]           =  $this->model_parents->get_students_of_parent($tbl_parent_id);
				$data["tbl_parent_id"]      = $tbl_parent_id;
	
				$user_type = "P";
				$user_id   = $tbl_parent_id;
				$token     = $device_uid;
				
				$_SESSION['aqdar_smartcare']['tbl_admin_id_sess']         = $tbl_parent_id;
				$_SESSION['aqdar_smartcare']['tbl_admin_user_id_sess']    = $parent_obj[0]['user_id'];
				$_SESSION['aqdar_smartcare']['admin_email_sess']          = $parent_obj[0]['email'];
				$_SESSION['aqdar_smartcare']['admin_first_name_sess']     = $parent_obj[0]['first_name'];
				$_SESSION['aqdar_smartcare']['admin_last_name_sess']      = $parent_obj[0]['last_name'];
				$_SESSION['aqdar_smartcare']['admin_first_name_sess_ar']  = $parent_obj[0]['first_name_ar'];
				$_SESSION['aqdar_smartcare']['admin_last_name_sess_ar']   = $parent_obj[0]['last_name_ar'];
				$_SESSION['aqdar_smartcare']['added_date_sess']           = date('M d, Y', strtotime($parent_obj[0]['added_date']));
				$_SESSION['aqdar_smartcare']['user_type_sess']            = "parent";
				$_SESSION['aqdar_smartcare']['admin_auth_sess']           = "SA";
				$_SESSION['aqdar_smartcare']['module_access']             = "P";
			
				echo "*Y*";
				exit;
				}
		}
	echo "*N*";
	exit;
	}
   
	
	/**
	* @desc    Show all my children
	* @param   none
	* @access  default
	*/
    function mychild() {
		$data['page'] = "view_my_children";
		$data['menu'] = "children";
        $data['sub_menu'] = "children";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "first_name, last_name";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name, last_name";
					 break;
				}
				default: {
					$sort_name = "first_name, last_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_parent_id = "";
		if (array_key_exists('tbl_parent_id',$param_array)) {
			$tbl_parent_id = $param_array['tbl_parent_id'];
			$data['tbl_sel_parent_id'] = $tbl_parent_id;
		}
		
		$tbl_class_search_id = "";
		if (array_key_exists('tbl_class_search_id',$param_array)) {
			$tbl_class_search_id = $param_array['tbl_class_search_id'];
			$data['tbl_class_search_id'] = $tbl_class_search_id;
		}
		$q = "";
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
			$data['q'] = $q;
		}
		
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		
		$is_active = "";
		if($tbl_parent_id<>"")
		{				
			$rs_all_students      = $this->model_student->get_all_students($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_class_search_id,$tbl_parent_id);
			$total_students       = $this->model_student->get_total_students($q, $is_active, $tbl_school_id,$tbl_class_search_id,$tbl_parent_id);
			
			//PAGINATION CLASS
			$page_url = HOST_URL."/".LAN_SEL."/parent/parent_user/mychild";
			if (isset($q) && trim($q)!="") {
				$page_url .= "/q/".rawurlencode($q);
			}
			if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
				$page_url .= "/tbl_student_id/".rawurlencode($tbl_student_id);
			}
			if (isset($tbl_class_search_id) && trim($tbl_class_search_id)!="") {
				$page_url .= "/tbl_class_search_id/".rawurlencode($tbl_class_search_id);
			}
			if (isset($sort_name) && trim($sort_name)!="") {
				$page_url .= "/sort_name/".rawurlencode($sort_name_param);
			}
			if (isset($sort_by) && trim($sort_by)!="") {
				$page_url .= "/sort_by/".rawurlencode($sort_by);
			}
			$page_url .= "/offset";
			
			$this->load->library('pagination');
			$config['base_url'] = $page_url;
			$config['total_rows'] = $total_students;
			$config['per_page'] = TBL_STUDENT_PAGING;//constant
			$config['uri_segment'] = $this->uri->total_segments();
			$config['num_links'] = 5;
	
			$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
			$config['next_link_disable'] = '';
	
			$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
			$config['prev_link_disable'] = "";		
				
			$config['first_link'] = "";
			$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
			$config['first_tag_close'] = '</span>';
			
			$config['last_link'] = "";
			$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
			$config['last_tag_close'] = '</span>';
	
			$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
			$config['cur_tag_close'] = "&nbsp;</span>";
			
			$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
			$config['num_tag_close'] = "&nbsp;</span>";
	
			$this->pagination->initialize($config);
			$start = $offset + 1;
			$range = "";
			if ($offset+TBL_STUDENT_PAGING >= $total_students) {
				$range = $total_students;
			} else {
				$range = $offset+TBL_STUDENT_PAGING;
			}
	
			$paging_string = "$start - $range <font color='#333'>of $total_students children</font>&nbsp;";
			$data['paging_string'] = $paging_string;
			$data['start'] = $start;
			$data['rs_all_students']	     = $rs_all_students;
			$data['total_students'] 	      = $total_students;
		}else{
			header("location:".HOST_URL."/".LAN_SEL."/parent/home");
			exit;
		}

		$this->load->view('parent/view_parent_template',$data);
	}
	
	// Child Details
	
	/**
	* @desc    Student details
	* @param   none
	* @access  default
	*/
    function child_details() {
		$data['page']     = "view_child_details";
		$data['menu']     = "children";
        $data['sub_menu'] = "children";
		$data['mid'] = "4";//Details
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			$data['offset'] = $offset;
		}	 			
		if (array_key_exists('child_id_enc',$param_array)) {
			$tbl_student_id = $param_array['child_id_enc'];
			$data['tbl_student_id'] = $tbl_student_id;
		}	 
		
		if (array_key_exists('tab',$param_array)) {
			$tab = $param_array['tab'];
			$data['tab'] = $tab;
		}	 
		
		$tbl_school_id = "";
		//User details
		$this->load->model("model_student");	
		$student_obj = $this->model_student->student_info($tbl_student_id,$tbl_school_id);
		$data['student_obj'] = $student_obj;
		
		$tbl_class_id  = $student_obj[0]['tbl_class_id'];
		$tbl_parent_id = $student_obj[0]['tbl_parent_id'];
		$tbl_school_id  = $student_obj[0]['tbl_school_id'];
	    /********* Points History ************************/
		if($tab=="points")
		{
	    	$points_obj 			= $this->model_student->student_points_details($q, $tbl_student_id,$tbl_class_id,$tbl_school_id,$offset);
			$data['points_obj'] 	= $points_obj;
		
			$totPoints 			    = $this->model_student->total_points_details($q, $tbl_student_id,$tbl_class_id,$tbl_school_id);
			$data['totPoints'] 	    = $totPoints;
			
			//PAGINATION CLASS
			$page_url = HOST_URL."/".LAN_SEL."/parent/parent_user/child_details/";
			if (isset($q) && trim($q)!="") {
				$page_url .= "/q/".rawurlencode($q);
			}
			if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
				$page_url .= "/child_id_enc/".rawurlencode($tbl_student_id);
			}
			if (isset($tab) && trim($tab)!="") {
				$page_url .= "/tab/".rawurlencode($tab);
			}
			/*if (isset($tbl_class_search_id) && trim($tbl_class_search_id)!="") {
				$page_url .= "/tbl_class_search_id/".rawurlencode($tbl_class_search_id);
			}
			if (isset($sort_name) && trim($sort_name)!="") {
				$page_url .= "/sort_name/".rawurlencode($sort_name_param);
			}
			if (isset($sort_by) && trim($sort_by)!="") {
				$page_url .= "/sort_by/".rawurlencode($sort_by);
			}*/
			$page_url .= "/offset";
			
			$this->load->library('pagination');
			$config['base_url'] = $page_url;
			$config['total_rows'] = $totPoints;
			$config['per_page'] = TBL_CARD_CATEGORY_PAGING;//constant
			$config['uri_segment'] = $this->uri->total_segments();
			$config['num_links'] = 5;
	
			$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
			$config['next_link_disable'] = '';
	
			$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
			$config['prev_link_disable'] = "";		
				
			$config['first_link'] = "";
			$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
			$config['first_tag_close'] = '</span>';
			
			$config['last_link'] = "";
			$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
			$config['last_tag_close'] = '</span>';
	
			$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
			$config['cur_tag_close'] = "&nbsp;</span>";
			
			$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
			$config['num_tag_close'] = "&nbsp;</span>";
	
			$this->pagination->initialize($config);
			$start = $offset + 1;
			$range = "";
			if ($offset+TBL_CARD_CATEGORY_PAGING >= $totPoints) {
				$range = $totPoints;
			} else {
				$range = $offset+TBL_CARD_CATEGORY_PAGING;
			}
	
			$paging_string = "$start - $range <font color='#333'>of $totPoints points</font>&nbsp;";
			$data['paging_string'] = $paging_string;
			
		/******** End Points History *********************/
		/******** Cards History **************************/
		} else if($tab=="cards"){
			$cards_obj 				= $this->model_student->student_cards_details($q, $tbl_student_id,$tbl_class_id,$tbl_school_id,$offset);
			$data['cards_obj'] 		= $cards_obj;
		
			$totCards 				= $this->model_student->total_student_cards($q, $tbl_student_id,$tbl_class_id,$tbl_school_id);
			$data['total_cards'] 	= $totCards;	
		
			//PAGINATION CLASS
			$page_url = HOST_URL."/".LAN_SEL."/parent/parent_user/child_details/";
			if (isset($q) && trim($q)!="") {
				$page_url .= "/q/".rawurlencode($q);
			}
			if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
				$page_url .= "/child_id_enc/".rawurlencode($tbl_student_id);
			}
			if (isset($tab) && trim($tab)!="") {
				$page_url .= "/tab/".rawurlencode($tab);
			}
			/*if (isset($tbl_class_search_id) && trim($tbl_class_search_id)!="") {
				$page_url .= "/tbl_class_search_id/".rawurlencode($tbl_class_search_id);
			}
			if (isset($sort_name) && trim($sort_name)!="") {
				$page_url .= "/sort_name/".rawurlencode($sort_name_param);
			}
			if (isset($sort_by) && trim($sort_by)!="") {
				$page_url .= "/sort_by/".rawurlencode($sort_by);
			}*/
			$page_url .= "/offset";
			
			$this->load->library('pagination');
			$config['base_url'] = $page_url;
			$config['total_rows'] = $totCards;
			$config['per_page'] = TBL_CARD_CATEGORY_PAGING;//constant
			$config['uri_segment'] = $this->uri->total_segments();
			$config['num_links'] = 5;
	
			$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
			$config['next_link_disable'] = '';
	
			$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
			$config['prev_link_disable'] = "";		
				
			$config['first_link'] = "";
			$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
			$config['first_tag_close'] = '</span>';
			
			$config['last_link'] = "";
			$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
			$config['last_tag_close'] = '</span>';
	
			$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
			$config['cur_tag_close'] = "&nbsp;</span>";
			
			$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
			$config['num_tag_close'] = "&nbsp;</span>";
	
			$this->pagination->initialize($config);
			$start = $offset + 1;
			$range = "";
			if ($offset+TBL_CARD_CATEGORY_PAGING >= $totCards) {
				$range = $totCards;
			} else {
				$range = $offset+TBL_CARD_CATEGORY_PAGING;
			}
	
			$paging_string = "$start - $range <font color='#333'>of $totCards cards</font>&nbsp;";
			$data['paging_string'] = $paging_string;
			
		/******** End Cards History **********************/
		/******** Records History ************************/
		} else if($tab=="records"){
			
			$rs_all_records     	= $this->model_student->get_student_records($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parenting_school_cat_id,$tbl_student_id,$tbl_class_id);
			$data['records_obj'] 	= $rs_all_records;
			
			$total_records     	    = $this->model_student->get_total_student_records($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parenting_school_cat_id,$tbl_student_id,$tbl_class_id);
			$data['total_records'] 	= $total_records;
			
			//PAGINATION CLASS
			$page_url = HOST_URL."/".LAN_SEL."/parent/parent_user/child_details/";
			if (isset($q) && trim($q)!="") {
				$page_url .= "/q/".rawurlencode($q);
			}
			if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
				$page_url .= "/child_id_enc/".rawurlencode($tbl_student_id);
			}
			if (isset($tab) && trim($tab)!="") {
				$page_url .= "/tab/".rawurlencode($tab);
			}
		
			$page_url .= "/offset";
			
			$this->load->library('pagination');
			$config['base_url'] = $page_url;
			$config['total_rows'] = $total_records;
			$config['per_page'] = TBL_CARD_CATEGORY_PAGING;//constant
			$config['uri_segment'] = $this->uri->total_segments();
			$config['num_links'] = 5;
	
			$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
			$config['next_link_disable'] = '';
	
			$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
			$config['prev_link_disable'] = "";		
				
			$config['first_link'] = "";
			$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
			$config['first_tag_close'] = '</span>';
			
			$config['last_link'] = "";
			$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
			$config['last_tag_close'] = '</span>';
	
			$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
			$config['cur_tag_close'] = "&nbsp;</span>";
			
			$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
			$config['num_tag_close'] = "&nbsp;</span>";
	
			$this->pagination->initialize($config);
			$start = $offset + 1;
			$range = "";
			if ($offset+TBL_CARD_CATEGORY_PAGING >= $total_records) {
				$range = $total_records;
			} else {
				$range = $offset+TBL_CARD_CATEGORY_PAGING;
			}
	
			$paging_string = "$start - $range <font color='#333'>of $total_records records</font>&nbsp;";
			$data['paging_string'] = $paging_string;
		
		/******** End Records History *******************/
		/******** Message History ***********************/
		} else if($tab=="message"){
			
			$rs_all_messages        = $this->model_student->list_parent_messages_of_student($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parent_id);
			$data['messages_obj']   = $rs_all_messages;
			
			$total_messages         = $this->model_student->get_total_parent_messages_of_student($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id, $tbl_parent_id);
			$data['total_messages'] = $total_messages;
			
					//PAGINATION CLASS
			$page_url = HOST_URL."/".LAN_SEL."/parent/parent_user/child_details/";
			if (isset($q) && trim($q)!="") {
				$page_url .= "/q/".rawurlencode($q);
			}
			if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
				$page_url .= "/child_id_enc/".rawurlencode($tbl_student_id);
			}
			if (isset($tab) && trim($tab)!="") {
				$page_url .= "/tab/".rawurlencode($tab);
			}
		
			$page_url .= "/offset";
			
			$this->load->library('pagination');
			$config['base_url'] = $page_url;
			$config['total_rows'] = $total_messages;
			$config['per_page'] = TBL_CARD_CATEGORY_PAGING;//constant
			$config['uri_segment'] = $this->uri->total_segments();
			$config['num_links'] = 5;
	
			$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
			$config['next_link_disable'] = '';
	
			$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
			$config['prev_link_disable'] = "";		
				
			$config['first_link'] = "";
			$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
			$config['first_tag_close'] = '</span>';
			
			$config['last_link'] = "";
			$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
			$config['last_tag_close'] = '</span>';
	
			$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
			$config['cur_tag_close'] = "&nbsp;</span>";
			
			$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
			$config['num_tag_close'] = "&nbsp;</span>";
	
			$this->pagination->initialize($config);
			$start = $offset + 1;
			$range = "";
			if ($offset+TBL_CARD_CATEGORY_PAGING >= $total_messages) {
				$range = $total_messages;
			} else {
				$range = $offset+TBL_CARD_CATEGORY_PAGING;
			}
	
			$paging_string = "$start - $range <font color='#333'>of $total_records records</font>&nbsp;";
			$data['paging_string'] = $paging_string;
			
		} else if($tab=="events")
		{
			 
			$this->load->model("model_events");
			$total_events      = 0;
			$rs_all_events     = array();
			$rs_all_events      = $this->model_events->get_all_events($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id);
			$total_events       = $this->model_events->get_total_events($q, $is_active, $tbl_school_id);
			
			$data['rs_all_events']   = $rs_all_events;
			$data['total_events']    = $total_events;
			
			//PAGINATION CLASS
			$page_url = HOST_URL."/".LAN_SEL."/parent/parent_user/child_details/";
			if (isset($q) && trim($q)!="") {
				$page_url .= "/q/".rawurlencode($q);
			}
			if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
				$page_url .= "/child_id_enc/".rawurlencode($tbl_student_id);
			}
			if (isset($tab) && trim($tab)!="") {
				$page_url .= "/tab/".rawurlencode($tab);
			}
		
			$page_url .= "/offset";
			
			$this->load->library('pagination');
			$config['base_url'] = $page_url;
			$config['total_rows'] = $total_messages;
			$config['per_page'] = TBL_CARD_CATEGORY_PAGING;//constant
			$config['uri_segment'] = $this->uri->total_segments();
			$config['num_links'] = 5;
	
			$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
			$config['next_link_disable'] = '';
	
			$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
			$config['prev_link_disable'] = "";		
				
			$config['first_link'] = "";
			$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
			$config['first_tag_close'] = '</span>';
			
			$config['last_link'] = "";
			$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
			$config['last_tag_close'] = '</span>';
	
			$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
			$config['cur_tag_close'] = "&nbsp;</span>";
			
			$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
			$config['num_tag_close'] = "&nbsp;</span>";
	
			$this->pagination->initialize($config);
			$start = $offset + 1;
			$range = "";
			if ($offset+TBL_CARD_CATEGORY_PAGING >= $total_messages) {
				$range = $total_messages;
			} else {
				$range = $offset+TBL_CARD_CATEGORY_PAGING;
			}
	
			$paging_string = "$start - $range <font color='#333'>of $total_records records</font>&nbsp;";
			$data['paging_string'] = $paging_string;
			
		}
		
		
		/******** End Message History ******************/
		
		$data['pagination_link'] = $this->pagination->create_links();
		$this->load->view('parent/view_parent_template', $data);
	}
	
	// Parenting Categories
	 function parenting_category() {
		$data['page'] = "view_ministry_parenting_category";
		$data['menu'] = "parenting";
		$data['sub_menu'] = "parenting";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		$sort_name = "title_en";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_ministry_parenting_categorys = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "title_en";
					 break;
				}
				default: {
					$sort_name = "title_en";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_ministry_parenting_category");
		
		$is_active = "";
		$rs_all_ministry_parenting_categorys = $this->model_ministry_parenting_category->get_all_ministry_parenting_categorys($sort_name, $sort_by, $offset, $q, $is_active);
		$total_ministry_parenting_categorys = $this->model_ministry_parenting_category->get_total_ministry_parenting_categorys($q, $is_active);

		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/parent/parent_user/parenting_category";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_ministry_parenting_categorys;
		$config['per_page'] = TBL_PARENTING_CAT_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_PARENTING_CAT_PAGING >= $total_ministry_parenting_categorys) {
			$range = $total_ministry_parenting_categorys;
		} else {
			$range = $offset+TBL_PARENTING_CAT_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_ministry_parenting_categorys category(s)</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_ministry_parenting_categorys'] = $rs_all_ministry_parenting_categorys;
		$data['total_ministry_parenting_categorys'] = $total_ministry_parenting_categorys;

		$this->load->view('parent/view_parent_template',$data);
	}
	
	//ministry_parenting
	/**
	* @desc    Show all ministry parenting
	* @param   none
	* @access  default
	*/
    function ministry_parenting() {
		$data['page']     = "view_ministry_parenting";
		$data['menu']     = "parenting";
		$data['sub_menu'] = "parenting";

		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		$sort_name = "parenting_title_en";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_ministry_parentings = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	
		$parenting_cat_id_enc = "";
		if (array_key_exists('parenting_cat_id_enc',$param_array)) {
			$parenting_cat_id_enc = $param_array['parenting_cat_id_enc'];
		}	
		$data['parenting_cat_id_enc'] = $parenting_cat_id_enc;
		 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "parenting_title_en";
					 break;
				}
				default: {
					$sort_name = "parenting_title_en";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_ministry_parenting");
		
		$is_active = "";
		$rs_all_ministry_parentings = $this->model_ministry_parenting->get_all_ministry_parentings($sort_name, $sort_by, $offset, $q, $is_active, $parenting_cat_id_enc);
		$total_ministry_parentings = $this->model_ministry_parenting->get_total_ministry_parentings($q, $is_active, $parenting_cat_id_enc);

		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/parent/parent_user/ministry_parenting";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($parenting_cat_id_enc) && trim($parenting_cat_id_enc)!="") {
			$page_url .= "/parenting_cat_id_enc/".rawurlencode($parenting_cat_id_enc);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_ministry_parentings;
		$config['per_page'] = TBL_PARENTING_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_PARENTING_PAGING >= $total_ministry_parentings) {
			$range = $total_ministry_parentings;
		} else {
			$range = $offset+TBL_PARENTING_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_ministry_parentings record(s)</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_ministry_parentings'] = $rs_all_ministry_parentings;
		$data['total_ministry_parentings'] = $total_ministry_parentings;

		//Load categories
		$this->load->model("model_ministry_parenting_category");
		$rs_parenting_categorys = $this->model_ministry_parenting_category->get_all_ministry_parenting_categorys("title_en", "ASC", "", "", "Y");
		$data['rs_parenting_categorys'] = $rs_parenting_categorys;

		$this->load->view('parent/view_parent_template',$data);
	}


	
    function view_ministry_parenting() {
		$data['page']     = "view_ministry_parenting";
		$data['menu']     = "parenting";
		$data['sub_menu'] = "parenting";
		$data['mid']      = "3";

		//GET Params
	   
		$tbl_parenting_id = $_POST['parenting_id_enc'];		
		//Ministry_parenting details
		$this->load->model("model_ministry_parenting");		
		$ministry_parenting_obj = $this->model_ministry_parenting->get_ministry_parenting_obj($tbl_parenting_id);
		$data['ministry_parenting_obj'] = $ministry_parenting_obj;
       
		//Load categories
		$this->load->model("model_ministry_parenting_category");
		$rs_parenting_categorys = $this->model_ministry_parenting_category->get_all_ministry_parenting_categorys("title_en", "ASC", "", "", "Y");
		$data['rs_parenting_categorys'] = $rs_parenting_categorys;
		
		//Video
		$tbl_uploads_id = $ministry_parenting_obj['tbl_parenting_id'];
		$video_url = "";
		$this->load->model("model_file_mgmt");		
		$file_mgmt_obj = $this->model_file_mgmt->get_file_mgmt_obj($tbl_uploads_id);
		
		if (count($file_mgmt_obj)>0) {
			$tbl_uploads_id = $file_mgmt_obj['tbl_uploads_id'];	
			$video_url = HOST_URL."/admin/uploads/".$file_mgmt_obj['file_name_updated'];	

			$data['tbl_uploads_id'] = $tbl_uploads_id;
			$data['video_url'] = $video_url;
		}
		
		$this->load->view('parent/view_ministry_parenting_details.php', $data);
	}
	
	
	
	// Children Record
	
	/**
	* @desc    Show all my children
	* @param   none
	* @access  default
	*/
    function mychild_record() {
		$data['page'] = "view_my_children_record";
		$data['menu'] = "records";
        $data['sub_menu'] = "records";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "first_name, last_name";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name, last_name";
					 break;
				}
				default: {
					$sort_name = "first_name, last_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_parent_id = "";
		if (array_key_exists('tbl_parent_id',$param_array)) {
			$tbl_parent_id = $param_array['tbl_parent_id'];
			$data['tbl_sel_parent_id'] = $tbl_parent_id;
		}
		
		$tbl_class_search_id = "";
		if (array_key_exists('tbl_class_search_id',$param_array)) {
			$tbl_class_search_id = $param_array['tbl_class_search_id'];
			$data['tbl_class_search_id'] = $tbl_class_search_id;
		}
		$q = "";
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
			$data['q'] = $q;
		}
		
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		
		$is_active = "";
						
		$rs_all_students      = $this->model_student->get_all_students($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_class_search_id,$tbl_parent_id);
		$total_students       = $this->model_student->get_total_students($q, $is_active, $tbl_school_id,$tbl_class_search_id,$tbl_parent_id);
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/parent/parent_user/mychild";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
			$page_url .= "/tbl_student_id/".rawurlencode($tbl_student_id);
		}
		if (isset($tbl_class_search_id) && trim($tbl_class_search_id)!="") {
			$page_url .= "/tbl_class_search_id/".rawurlencode($tbl_class_search_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_students;
		$config['per_page'] = TBL_STUDENT_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_STUDENT_PAGING >= $total_students) {
			$range = $total_students;
		} else {
			$range = $offset+TBL_STUDENT_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_students children</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		$data['rs_all_students']	     = $rs_all_students;
		$data['total_students'] 	      = $total_students;

		$this->load->view('parent/view_parent_template',$data);
	}
	
	
	 // my child records
	 function school_records() {
		$data['page'] = "view_school_records";
		$data['menu'] = "records";
        $data['sub_menu'] = "records";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "id";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "id";
					 break;
				}
				default: {
					$sort_name = "id";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_parenting_school_cat_id',$param_array)) {
			$tbl_parenting_school_cat_id = $param_array['tbl_parenting_school_cat_id'];
			$data['tbl_sel_parenting_school_cat_id'] = $tbl_parenting_school_cat_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$tbl_school_id = "";
		if (array_key_exists('tbl_school_id',$param_array)) {
			$tbl_school_id = $param_array['tbl_school_id'];
			$data['tbl_sel_school_id'] = $tbl_school_id;
		}
		
		$tbl_student_id = "";
		if (array_key_exists('child_id_enc',$param_array)) {
			$tbl_student_id = $param_array['child_id_enc'];
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_parenting_school");
		$this->load->model("model_class_sessions");
		
		
		$is_active = "";
		$categoryObj = $this->model_parenting_school->get_parenting_school_categories($tbl_school_id);
	    $data['category_list'] = $categoryObj;	
		$total_records      = 0; 
		
		if($tbl_school_id<>"")
		{  
			$rs_all_records     = $this->model_parenting_school->get_child_parenting_datas($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parenting_school_cat_id, $tbl_student_id );
			$total_records      = $this->model_parenting_school->get_child_total_parenting_datas($q, $is_active, $tbl_school_id, $tbl_parenting_school_cat_id, $tbl_student_id);
		}else{
			$rs_all_records     = array();
		}
		
		$this->load->model("model_student");
		$child_info = $this->model_student->get_student_name($tbl_student_id);
		$data['child_info'] = $child_info;	
		
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/parent/parent_user/school_records";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_teacher_id) && trim($tbl_teacher_id)!="") {
			$page_url .= "/tbl_teacher_id/".rawurlencode($tbl_teacher_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_records;
		$config['per_page'] = TBL_PARENTING_SCHOOL_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_PARENTING_SCHOOL_PAGING >= $total_records) {
			$range = $total_records;
		} else {
			$range = $offset+TBL_PARENTING_SCHOOL_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_records records</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_records']	     = $rs_all_records;
		$data['total_records'] 	      = $total_records;

		$this->load->view('parent/view_parent_template',$data);
	}
	
	
	 //View School Records
	/**
	* @param   none
	* @access  default
	*/
    function view_school_record() {
		$data['page'] = "view_school_records";
		$data['menu'] = "records";
        $data['sub_menu'] = "records";
		
		$this->load->model("model_parenting_school");

		$tbl_parenting_school_id = $_POST['tbl_parenting_school_id'];	
		$tbl_school_id           = $_POST['tbl_school_id'];	
		
		$is_active = "";
		if($tbl_school_id<>"")
		{  
		   $categoryObj = $this->model_parenting_school->get_parenting_school_categories($tbl_school_id);
	       $data['category_list'] = $categoryObj;	
		  
		   $results = $this->model_parenting_school->get_school_record($tbl_parenting_school_id,$tbl_school_id);
	       $data['school_record'] = $results;  
		}
		$this->load->view('parent/view_school_record_details.php', $data);
	}
	
	
	// Leave Request For My child
	
	// Children Record
	
	/**
	* @desc    Show all my children
	* @param   none
	* @access  default
	*/
    function mychild_leave() {
		$data['page'] = "view_my_children_leave";
		$data['menu'] = "leaves";
        $data['sub_menu'] = "leaves";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "first_name, last_name";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name, last_name";
					 break;
				}
				default: {
					$sort_name = "first_name, last_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_parent_id = "";
		if (array_key_exists('tbl_parent_id',$param_array)) {
			$tbl_parent_id = $param_array['tbl_parent_id'];
			$data['tbl_sel_parent_id'] = $tbl_parent_id;
		}
		
		$tbl_class_search_id = "";
		if (array_key_exists('tbl_class_search_id',$param_array)) {
			$tbl_class_search_id = $param_array['tbl_class_search_id'];
			$data['tbl_class_search_id'] = $tbl_class_search_id;
		}
		$q = "";
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
			$data['q'] = $q;
		}
		
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		
		$is_active = "";
						
		$rs_all_students      = $this->model_student->get_all_students($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_class_search_id,$tbl_parent_id);
		$total_students       = $this->model_student->get_total_students($q, $is_active, $tbl_school_id,$tbl_class_search_id,$tbl_parent_id);
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/parent/parent_user/mychild_leaves";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
			$page_url .= "/tbl_student_id/".rawurlencode($tbl_student_id);
		}
		if (isset($tbl_class_search_id) && trim($tbl_class_search_id)!="") {
			$page_url .= "/tbl_class_search_id/".rawurlencode($tbl_class_search_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_students;
		$config['per_page'] = TBL_STUDENT_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_STUDENT_PAGING >= $total_students) {
			$range = $total_students;
		} else {
			$range = $offset+TBL_STUDENT_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_students children</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		$data['rs_all_students']	     = $rs_all_students;
		$data['total_students'] 	      = $total_students;

		$this->load->view('parent/view_parent_template',$data);
	}
	
	
	//child_leaves
	 function child_leaves() {
		$data['page'] = "view_child_leaves";
		$data['menu'] = "leaves";
        $data['sub_menu'] = "leaves";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "first_name, last_name";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
		
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name, last_name";
					 break;
				}
				default: {
					$sort_name = "first_name, last_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('child_id_enc',$param_array)) {
			$tbl_student_id = $param_array['child_id_enc'];
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_parent_id = "";
		if (array_key_exists('tbl_parent_id',$param_array)) {
			$tbl_parent_id = $param_array['tbl_parent_id'];
			$data['tbl_sel_parent_id'] = $tbl_parent_id;
		}
		
		$tbl_class_search_id = "";
		if (array_key_exists('tbl_class_search_id',$param_array)) {
			$tbl_class_search_id = $param_array['tbl_class_search_id'];
			$data['tbl_class_search_id'] = $tbl_class_search_id;
		}
		
		$tbl_school_id = "";
		if (array_key_exists('tbl_school_id',$param_array)) {
			$tbl_school_id = $param_array['tbl_school_id'];
			$data['tbl_sel_school_id'] = $tbl_school_id;
		}
		
		$q = "";
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
			$data['q'] = $q;
		}
		
		$tbl_teacher_id = "";
		if (array_key_exists('tbl_teacher_id',$param_array)) {
			$tbl_teacher_id = $param_array['tbl_teacher_id'];
			$data['tbl_sel_teacher_id'] = $tbl_teacher_id;
		}
	
		$data['offset'] = $offset;
	   
		$this->load->model("model_message");
		
		$is_active = "";
						
		$rs_all_leaves_application      = $this->model_message->get_my_leave_applications($tbl_teacher_id, $today_date, $tbl_student_id, $tbl_school_id, $offset, $is_active);
		$total_leaves_application       = $this->model_message->get_total_my_leave_applications($tbl_teacher_id, $today_date, $tbl_student_id, $tbl_school_id, $offset, $is_active);
		
		$this->load->model("model_student");
		$child_info = $this->model_student->get_student_name($tbl_student_id);
		$data['child_info'] = $child_info;	
		
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/parent/parent_user/child_leaves";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
			$page_url .= "/child_id_enc/".rawurlencode($tbl_student_id);
		}
		if (isset($tbl_school_id) && trim($tbl_school_id)!="") {
			$page_url .= "/tbl_school_id/".rawurlencode($tbl_school_id);
		}
		if (isset($tbl_teacher_id) && trim($tbl_teacher_id)!="") {
			$page_url .= "/tbl_teacher_id/".rawurlencode($tbl_teacher_id);
		}
		if (isset($tbl_class_search_id) && trim($tbl_class_search_id)!="") {
			$page_url .= "/tbl_class_search_id/".rawurlencode($tbl_class_search_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_leaves_application;
		$config['per_page'] = TBL_SCHOOL_TYPE_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_SCHOOL_TYPE_PAGING >= $total_leaves_application) {
			$range = $total_leaves_application;
		} else {
			$range = $offset+TBL_SCHOOL_TYPE_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_leaves_application leaves application</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		$data['rs_all_leaves_application']	      = $rs_all_leaves_application;
		$data['total_leaves_application'] 	      = $total_leaves_application;

		$this->load->view('parent/view_parent_template',$data);
	}
	
	
	function is_exist_leave() {
		if ($_POST) {
			$tbl_leave_application_id           = $_POST['tbl_leave_application_id'];		
			$start_date                         = $_POST['start_date'];
			$tbl_student_id                     = $_POST['tbl_student_id'];
			$tbl_school_id                      = $_POST['tbl_school_id'];
		}
		$this->load->model("model_message");
		$results = $this->model_message->is_exist_leave_aplication($tbl_leave_application_id, $start_date, $tbl_student_id, $tbl_school_id);
		if(count($results)>0) {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
	 /**
	* @desc    Add Leave Application
	* @param   POST array
	* @access  default
	*/
    function add_leave_application() {
		if ($_POST) {
			$tbl_leave_application_id        = $_POST['tbl_leave_application_id'];		
			$comments_parent                 = $_POST['comments_parent'];
			$tbl_student_id                  = $_POST['tbl_student_id'];
			$tbl_school_id                   = $_POST['tbl_school_id'];
			$start_date     	  			 = date("Y-m-d", strtotime($_POST['start_date']));
			$end_date     	  			     = $_POST['end_date'];
			if($end_date<>"")
			{
				$end_date     	    			 = date("Y-m-d", strtotime($_POST['end_date']));
			}else{
				$end_date     	    			 = $start_date;
			}
		}
		$this->load->model("model_message");
		$tbl_parent_id  = $_SESSION['aqdar_smartcare']['tbl_admin_id_sess'];	
		$resData = $this->model_message->save_leave_application($tbl_teacher_id, $tbl_student_id, $tbl_parent_id, $comments_parent, $tbl_school_id, $start_date, $end_date);
	    echo "Y";
	}
	
	
	// attendance report detaild
	
	
	// Daily Attendance Detailed -  Daily Report
	 function attendance_reports_list()
	{
		$data['page'] = "view_attendance_report_detailed";
		$data['menu'] = "attendance";
        $data['sub_menu'] = "attendance";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "first_name";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name";
					 break;
				}
				default: {
					$sort_name = "first_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];                               
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_parent_id = "";
		if (array_key_exists('tbl_parent_id',$param_array)) {
			$tbl_parent_id = $param_array['tbl_parent_id'];                               
			$data['tbl_sel_parent_id'] = $tbl_parent_id;
		}
		
		if (array_key_exists('attendance_date',$param_array)) {
			$attendance_date = $param_array['attendance_date'];
			$dateArray       = explode("-",$attendance_date);   
			$data['attendance_date'] = $dateArray[1]."/".$dateArray[2]."/".$dateArray[0];
		}
		if($attendance_date=="")
		{
			$attendance_date =  date("Y-m-d");
			$dateArray       = explode("-",$attendance_date);   
			$data['attendance_date'] = $dateArray[1]."/".$dateArray[2]."/".$dateArray[0];
		}
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_report");
		
		$is_active = "";
		/*if($tbl_student_id<>"")
		{
			$rs_all_students      		= $this->model_student->get_student_obj($tbl_student_id);
		}else{*/
			$rs_all_students      		= $this->model_student->get_all_students($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_class_search_id,$tbl_parent_id);
		//}
		$data['rs_all_students'] 	= $rs_all_students;
		
		if(count($rs_all_students)>0)
		{
			$tbl_student_id             = $rs_all_students[0]['tbl_student_id'];
			$tbl_school_id              = $rs_all_students[0]['tbl_school_id'];
			$tbl_class_id               = $rs_all_students[0]['tbl_class_id'];
			
			$this->load->model("model_student");
			$child_info = $this->model_student->get_student_name($tbl_student_id);
			$data['child_info'] = $child_info;	
			
			$attendanceDetailedObj 		= $this->model_report->mychild_attendance_reports_detailed($sort_name, $sort_by, $offset, $q="", $tbl_point_category_id, $is_active='', $tbl_school_id, $tbl_gender, $tbl_country_id, $tbl_class_id, $tbl_student_id, $tbl_semester_id, $attendance_date, $tbl_teacher_id);
			$attendanceDetailedTotalObj = $this->model_report->mychild_total_attendance_reports_detailed($q="", $tbl_point_category_id, $is_active='', $tbl_school_id, $tbl_gender, $tbl_country_id, $tbl_class_id, $tbl_student_id, $tbl_semester_id, $attendance_date, $tbl_teacher_id);
		}else{
			$attendanceDetailedObj = array();
			$attendanceDetailedTotalObj = 0;
		}
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/parent/parent_user/attendance_reports_list";
		
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
			$page_url .= "/tbl_student_id/".rawurlencode($tbl_student_id);
		}
		if (isset($tbl_parent_id) && trim($tbl_parent_id)!="") {
			$page_url .= "/tbl_parent_id/".rawurlencode($tbl_parent_id);
		}
		if (isset($tbl_class_id) && trim($tbl_class_id)!="") {
			$page_url .= "/tbl_class_id/".rawurlencode($tbl_class_id);
		}
		if (isset($tbl_semester_id) && trim($tbl_semester_id)!="") {
			$page_url .= "/tbl_semester_id/".rawurlencode($tbl_semester_id);
		}
		if (isset($tbl_teacher_id) && trim($tbl_teacher_id)!="") {
			$page_url .= "/tbl_teacher_id/".rawurlencode($tbl_teacher_id);
		}
		
		if (isset($tbl_gender) && trim($tbl_gender)!="") {
			$page_url .= "/gender/".rawurlencode($tbl_gender);
		}
		
		if ($attendance_date!="") {
			$page_url .= "/attendance_date/".$attendance_date;
		}
	
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		$this->load->library('pagination');
		$config['base_url']   = $page_url;
		$config['total_rows'] = $attendanceDetailedTotalObj;
		$config['per_page'] = TBL_TEACHER_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_TEACHER_PAGING >= $attendanceDetailedTotalObj) {
			$range = $attendanceDetailedTotalObj;
		} else {
			$range = $offset+TBL_TEACHER_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $attendanceDetailedTotalObj Records</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		$data['attendanceDetailedTotalObj'] =  $attendanceDetailedTotalObj;
		$data['rs_all_attendance_details']   =  $attendanceDetailedObj;
		//$this->load->view('admin/view_cards_report_list',$data);
		$this->load->view('parent/view_parent_template',$data);
		
	}
	// End Daily Attendance Detailed 

	// PRIVATE MESSAGE
	
	// Leave Request For My child
	
	// Children Record
	
	/**
	* @desc    Show all my children
	* @param   none
	* @access  default
	*/
    function mychild_private_message() {
		$data['page'] = "view_my_children_pvtmessage";
		$data['menu'] = "private_message";
        $data['sub_menu'] = "private_message";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "first_name, last_name";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name, last_name";
					 break;
				}
				default: {
					$sort_name = "first_name, last_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_parent_id = "";
		if (array_key_exists('tbl_parent_id',$param_array)) {
			$tbl_parent_id = $param_array['tbl_parent_id'];
			$data['tbl_sel_parent_id'] = $tbl_parent_id;
		}
		
		$tbl_class_search_id = "";
		if (array_key_exists('tbl_class_search_id',$param_array)) {
			$tbl_class_search_id = $param_array['tbl_class_search_id'];
			$data['tbl_class_search_id'] = $tbl_class_search_id;
		}
		$q = "";
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
			$data['q'] = $q;
		}
		
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		
		$is_active = "";
						
		$rs_all_students      = $this->model_student->get_all_students($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_class_search_id,$tbl_parent_id);
		$total_students       = $this->model_student->get_total_students($q, $is_active, $tbl_school_id,$tbl_class_search_id,$tbl_parent_id);
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/parent/parent_user/mychild_leaves";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
			$page_url .= "/tbl_student_id/".rawurlencode($tbl_student_id);
		}
		if (isset($tbl_class_search_id) && trim($tbl_class_search_id)!="") {
			$page_url .= "/tbl_class_search_id/".rawurlencode($tbl_class_search_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_students;
		$config['per_page'] = TBL_STUDENT_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_STUDENT_PAGING >= $total_students) {
			$range = $total_students;
		} else {
			$range = $offset+TBL_STUDENT_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_students children</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		$data['rs_all_students']	     = $rs_all_students;
		$data['total_students'] 	      = $total_students;

		$this->load->view('parent/view_parent_template',$data);
	}
	
	 // private message - teachers list
	 function mychild_teachers() {
		$data['page'] = "view_mychild_teachers";
		$data['menu'] = "private_message";
        $data['sub_menu'] = "private_message";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "id";
		$sort_by = "DESC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "id";
					 break;
				}
				default: {
					$sort_name = "id";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_parenting_school_cat_id',$param_array)) {
			$tbl_parenting_school_cat_id = $param_array['tbl_parenting_school_cat_id'];
			$data['tbl_sel_parenting_school_cat_id'] = $tbl_parenting_school_cat_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$tbl_school_id = "";
		if (array_key_exists('tbl_school_id',$param_array)) {
			$tbl_school_id = $param_array['tbl_school_id'];
			$data['tbl_sel_school_id'] = $tbl_school_id;
		}
		
		$tbl_student_id = "";
		if (array_key_exists('child_id_enc',$param_array)) {
			$tbl_student_id = $param_array['child_id_enc'];
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_parent_id = "";
		if (array_key_exists('tbl_parent_id',$param_array)) {
			$tbl_parent_id = $param_array['tbl_parent_id'];
			$data['tbl_sel_parent_id'] = $tbl_parent_id;
		}
		
		$this->load->model("model_parenting_school");
		$this->load->model("model_class_sessions");
		$this->load->model("model_student");
		$this->load->model("model_teachers");
		
		if($tbl_parent_id==""){
			$rs_parent_info = $this->model_student->get_assigned_parent($tbl_student_id);
			$tbl_parent_id  = $rs_parent_info[0]['tbl_parent_id'];
			$data['tbl_sel_parent_id'] = $tbl_parent_id;
		}
		
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		
		
		$is_active = "";
		$total_teachers      = 0; 
		
		$rs_all_students = $this->model_student->get_student_obj($tbl_student_id);
		$tbl_class_id     =  $rs_all_students[0]['tbl_class_id'];
		$tbl_school_id    =  $rs_all_students[0]['tbl_school_id'];
		
		if($tbl_school_id<>"")
		{  
			$rs_all_teachers      = $this->model_teachers->list_all_teachers($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_class_search_id);
			$total_teachers       = $this->model_teachers->get_total_teachers($q, $is_active, $tbl_school_id,$tbl_class_search_id);
		}else{
			$rs_all_teachers     = array();
		}
		
		$this->load->model("model_student");
		$child_info = $this->model_student->get_student_name($tbl_student_id);
		$data['child_info'] = $child_info;	
		
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/parent/parent_user/mychild_teachers";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
			$page_url .= "/child_id_enc/".rawurlencode($tbl_student_id);
		}
		if (isset($tbl_teacher_id) && trim($tbl_teacher_id)!="") {
			$page_url .= "/tbl_teacher_id/".rawurlencode($tbl_teacher_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_teachers;
		$config['per_page'] = TBL_PARENTING_SCHOOL_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_PARENTING_SCHOOL_PAGING >= $total_teachers) {
			$range = $total_teachers;
		} else {
			$range = $offset+TBL_PARENTING_SCHOOL_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_records teachers</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_teachers']	     = $rs_all_teachers;
		$data['total_teachers'] 	     = $total_teachers;

		$this->load->view('parent/view_parent_template',$data);
	}
	
	
     // private message - teachers list
	 function mychild_teacher_messages() {
		$data['page'] 		= "view_mychild_teacher_pvtmessage";
		$data['menu'] 		= "private_message";
        $data['sub_menu'] 	= "private_message";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		
		$offset = 0;
	
		$this->load->model("model_student");
		$this->load->model("model_teachers");
	    /*********my section ***************/
		$tbl_teacher_id = "";
		if (array_key_exists('tbl_teacher_id',$param_array)) {
			$tbl_teacher_id = $param_array['tbl_teacher_id'];
			$data['tbl_sel_teacher_id'] = $tbl_teacher_id;
		}
		
		$tbl_student_id = "";
		if (array_key_exists('child_id_enc',$param_array)) {
			$tbl_student_id = $param_array['child_id_enc'];
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_school_id = "";
		if (array_key_exists('tbl_school_id',$param_array)) {
			$tbl_school_id = $param_array['tbl_school_id'];
			$data['tbl_sel_school_id'] = $tbl_school_id;
		}

		$this->load->model('model_message');
		$this->load->model('model_parents');
        $totalMessage = 0;
		$msgdata = array(); 
        if($tbl_school_id<>"")
		{ 
			$listMessage  = $this->model_message->get_private_message_to_teacher($tbl_teacher_id, $tbl_student_id, $tbl_school_id, $offset);
			$totalMessage = $this->model_message->get_private_total_message_to_teacher($tbl_teacher_id, $tbl_student_id, $tbl_school_id);
		}else{
			$listMessage  = array();
		}
		
		for($i=0;$i<count($listMessage);$i++)
		{
			if($listMessage[$i]['message_dir'] == "TP")
			{
					$teacher_obj = $this->model_teachers->get_teacher_details($listMessage[$i]['message_from']);
					if($lan=="ar"){
						$first_name_teacher = $teacher_obj[0]['first_name_ar'];
						$last_name_teacher  = $teacher_obj[0]['last_name_ar'];
					}else{
						$first_name_teacher = $teacher_obj[0]['first_name'];
						$last_name_teacher  = $teacher_obj[0]['last_name'];
					}

					$listMessage[$i]['name'] = $first_name_teacher." ".$last_name_teacher;  //from
					$listMessage[$i]['tbl_teacher_id'] = $teacher_obj[0]['tbl_teacher_id'];
					$parent_obj = $this->model_parents->get_parentInfo_of_student($listMessage[$i]['message_to']); //to
					if($lan=="ar"){
						$first_name_parent = $parent_obj[0]['first_name_ar'];
						$last_name_parent  = $parent_obj[0]['last_name_ar'];
					}else{

						$first_name_parent = $parent_obj[0]['first_name'];
						$last_name_parent  = $parent_obj[0]['last_name'];
					}
					$listMessage[$i]['to'] = $first_name_parent." ".$last_name_parent;
			}else{
					$parent_obj = $this->model_parents->get_parentInfo_of_student($listMessage[$i]['message_from']); //from
					if($lan=="ar"){
						$first_name_parent = $parent_obj[0]['first_name_ar'];
						$last_name_parent  = $parent_obj[0]['last_name_ar'];
					}else{
						$first_name_parent = $parent_obj[0]['first_name'];
						$last_name_parent  = $parent_obj[0]['last_name'];
					}
					$listMessage[$i]['name'] = $first_name_parent." ".$last_name_parent;
					$listMessage[$i]['tbl_teacher_id'] = $parent_obj[0]['tbl_parent_id'];
					$teacher_obj = $this->model_teachers->get_teachers_obj($listMessage[$i]['message_to']);
					if($lan=="ar"){
						$first_name_teacher = $teacher_obj[0]['first_name_ar'];
						$last_name_teacher  = $teacher_obj[0]['last_name_ar'];
					}else{
						$first_name_teacher = $teacher_obj[0]['first_name'];
						$last_name_teacher  = $teacher_obj[0]['last_name'];
					}
					$listMessage[$i]['to'] = $first_name_teacher." ".$last_name_teacher;
			}			$file_name_updated = "";
			$file_name_updated_audio ="";
			$data_image_rs = "";
			$data_audio_rs = "";

			if($listMessage[$i]['tbl_item_id']<>""){
				$data_image_rs =  $this->model_message->get_message_image($listMessage[$i]['tbl_item_id']);	
				//print_r($data_image_rs);
				$file_name_updated = $data_image_rs[0]['file_name_updated'];
				$file_name_updated_thumb = $data_image_rs[0]['file_name_updated_thumb'];
				$data_audio_rs =  $this->model_message->get_message_audio($listMessage[$i]['tbl_item_id']);	
				$file_name_updated_audio = $data_audio_rs[0]['file_name_updated'];
			}

			if (trim($file_name_updated) != "") {
			$msgdata[$i]["message_img"] = HOST_URL."/admin/uploads/".$file_name_updated;
			$msgdata[$i]["message_img_thumb"] = HOST_URL."/admin/uploads/".$file_name_updated_thumb;
			} else {
				$data[$i]["message_img"] = "";
				$data[$i]["message_img_thumb"] = "";
			}

			if (trim($file_name_updated_audio) != "") {
				$msgdata[$i]["message_audio"] = HOST_URL."/admin/uploads/".$file_name_updated_audio;
			} else {
				$msgdata[$i]["message_audio"] = "";
			}

            $msgdata[$i]['message_dir'] 	=  	$listMessage[$i]['message_dir'];
			$msgdata[$i]['name'] 			=  	$listMessage[$i]['name'];
			$msgdata[$i]['message'] 		=  	isset($listMessage[$i]['message'])? $listMessage[$i]['message']:'' ;
			$msgdata[$i]['teacher_id']     =   $listMessage[$i]['tbl_teacher_id'];
			$msgdata[$i]['date'] 			= 	date("H:i - d/m/Y",strtotime($listMessage[$i]["added_date"]));
		}
		
		$child_info = $this->model_student->get_student_name($tbl_student_id);
		$data['child_info'] = $child_info;	
		
		$teacher_info = $this->model_teachers->get_teacher_details($tbl_teacher_id);
		$data['teacher_info'] = $teacher_info;
		/*********end my section ***********/
	
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/parent/parent_user/mychild_teacher_messages";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
			$page_url .= "/child_id_enc/".rawurlencode($tbl_student_id);
		}
		if (isset($tbl_teacher_id) && trim($tbl_teacher_id)!="") {
			$page_url .= "/tbl_teacher_id/".rawurlencode($tbl_teacher_id);
		}
		if (isset($tbl_school_id) && trim($tbl_school_id)!="") {
			$page_url .= "/tbl_school_id/".rawurlencode($tbl_school_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $totalMessage;
		$config['per_page'] = TBL_PARENTING_SCHOOL_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_PARENTING_SCHOOL_PAGING >= $totalMessage) {
			$range = $totalMessage;
		} else {
			$range = $offset+TBL_PARENTING_SCHOOL_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $totalMessage records</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['rs_all_messages']	 = $msgdata;
		$data['totalMessage'] 	     = $totalMessage;
        
		$this->load->view('parent/view_parent_template',$data);
	}
	
	
	function sendPrivateMessageToTeacher()
	 {
		if ($_POST) {
			$message_from        = $_POST['message_from'];		
			$message_to          = $_POST['message_to'];
			$tbl_student_id      = $_POST['tbl_student_id'];
			$message_type        = $_POST['message_type'];
			$tbl_message         = $_POST['tbl_message'];
			$tbl_school_id       = $_POST['tbl_school_id'];
			$tbl_item_id         = $_POST['tbl_item_id'];
			$message_dir         = $_POST['message_dir'];
		    $this->load->model("model_message");
			$saveMessage = $this->model_message->save_private_message($message_from, $message_to, $tbl_student_id, $message_type, $tbl_message, $tbl_school_id, $tbl_item_id, $message_dir);
	        echo "Y";
		}
	
	}
	
	function sendParentMessageToSchool()
	 {
		$this->load->model('model_parents');
		if ($_POST) {
			$tbl_parent_id       = $_POST['tbl_parent_id'];	
			$parent_info         = $this->model_parents->get_parent_obj($tbl_parent_id);
			$contact_us_name     = $parent_info[0]['first_name_en']." ".$parent_info[0]['last_name_en'];
			$contact_us_email    = $parent_info[0]['email'];
			$contact_us_comments = $_POST['message'];	
			$tbl_school_id       = $_POST['tbl_school_id'];
			
		    $this->load->model("model_message");
			$saveMessage = $this->model_message->sendParentMessageToSchool($tbl_parent_id, $contact_us_name, $contact_us_email, $contact_us_comments, $tbl_school_id);
	        echo "Y";
		}
	
	}
	
	function sendParentMessageToGovt()
	 {
		$this->load->model('model_parents');
		if ($_POST) {
			$tbl_parent_id       = $_POST['tbl_parent_id'];	
			$parent_info         = $this->model_parents->get_parent_obj($tbl_parent_id);
			$contact_us_name     = $parent_info[0]['first_name_en']." ".$parent_info[0]['last_name_en'];
			$email_id            = $parent_info[0]['email'];
			$feedback            = $_POST['message'];
			$subject             = $_POST['subject'];		
			$tbl_school_id       = $_POST['tbl_school_id'];
		    $user_type           = "P";
		    $this->load->model("model_message");
			$saveMessage = $this->model_message->saveMessageToGovt($email_id,$user_type,$subject,$feedback,$tbl_school_id);
	        echo "Y";
		}
	
	}
	
	function sendParentMessageToTimeline()
	 {
		$this->load->model('model_parents');
		if ($_POST) {
			$tbl_parent_id       = $_POST['tbl_parent_id'];	
			$parent_info         = $this->model_parents->get_parent_obj($tbl_parent_id);
			$contact_us_name     = $parent_info[0]['first_name_en']." ".$parent_info[0]['last_name_en'];
			$email_id            = $parent_info[0]['email'];
			$feedback            = $_POST['message'];
			$subject             = $_POST['subject'];			
			$tbl_school_id       = $_POST['tbl_school_id'];
			$user_type           = "P";
			
		    $this->load->model("model_message");
			$saveMessage = $this->model_message->saveMessageToTimeline($email_id,$user_type,$subject,$feedback,$tbl_school_id);
	        echo "Y";
		}
	
	}
	
	
	// My Child School Galleries
	/*** @desc    Show all my children*/
    function mychild_school_gallery() {
		$data['page']     = "view_my_children_school_gallery";
		$data['menu']     = "gallery";
        $data['sub_menu'] = "gallery";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "first_name, last_name";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name, last_name";
					 break;
				}
				default: {
					$sort_name = "first_name, last_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_parent_id = "";
		if (array_key_exists('tbl_parent_id',$param_array)) {
			$tbl_parent_id = $param_array['tbl_parent_id'];
			$data['tbl_sel_parent_id'] = $tbl_parent_id;
		}
		
		$tbl_class_search_id = "";
		if (array_key_exists('tbl_class_search_id',$param_array)) {
			$tbl_class_search_id = $param_array['tbl_class_search_id'];
			$data['tbl_class_search_id'] = $tbl_class_search_id;
		}
		$q = "";
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
			$data['q'] = $q;
		}
		
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		
		$is_active = "";
						
		$rs_all_students      = $this->model_student->get_all_students($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_class_search_id,$tbl_parent_id);
		$total_students       = $this->model_student->get_total_students($q, $is_active, $tbl_school_id,$tbl_class_search_id,$tbl_parent_id);
		
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/parent/parent_user/mychild";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
			$page_url .= "/tbl_student_id/".rawurlencode($tbl_student_id);
		}
		if (isset($tbl_class_search_id) && trim($tbl_class_search_id)!="") {
			$page_url .= "/tbl_class_search_id/".rawurlencode($tbl_class_search_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_students;
		$config['per_page'] = TBL_STUDENT_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_STUDENT_PAGING >= $total_students) {
			$range = $total_students;
		} else {
			$range = $offset+TBL_STUDENT_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_students children</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		$data['rs_all_students']	     = $rs_all_students;
		$data['total_students'] 	      = $total_students;

		$this->load->view('parent/view_parent_template',$data);
	}
	
	
	// Gallery Categories
	 function my_school_gallery_category() {
		$data['page'] 		= "view_child_school_gallery_category";
		$data['menu'] 		= "gallery";
		$data['sub_menu'] 	= "gallery";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		
		$offset = 0;
	
		$this->load->model("model_student");
		$this->load->model("model_teachers");
	    /*********my section ***************/
		
		$tbl_student_id = "";
		if (array_key_exists('child_id_enc',$param_array)) {
			$tbl_student_id = $param_array['child_id_enc'];
			$data['tbl_student_id'] = $tbl_student_id;
		}
		
		$tbl_school_id = "";
		if (array_key_exists('tbl_school_id',$param_array)) {
			$tbl_school_id = $param_array['tbl_school_id'];
			$data['tbl_school_id'] = $tbl_school_id;
		}
		
		$offset = 0;
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			$data['offset'] = $offset;
		}
		
		$rs_all_students = $this->model_student->get_student_obj($tbl_student_id);
		$tbl_class_id     =  $rs_all_students[0]['tbl_class_id'];
		$tbl_school_id    =  $rs_all_students[0]['tbl_school_id'];
		$is_active        = "Y";
		
		
		if($tbl_parent_id==""){
			$rs_parent_info = $this->model_student->get_assigned_parent($tbl_student_id);
			$tbl_parent_id  = $rs_parent_info[0]['tbl_parent_id'];
			$data['tbl_sel_parent_id'] = $tbl_parent_id;
		}
		
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
	    $this->load->model('model_gallery');
		$data_gallery_categories  = $this->model_gallery->get_gallery_categories_list($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_gallery_category_id);
	    $total_gallery_categories = $this->model_gallery->get_total_gallery_categories_list($q, $is_active, $tbl_school_id, $tbl_gallery_category_id);
		//PAGINATION CLASS
		$page_url = HOST_URL."/".LAN_SEL."/parent/parent_user/my_school_gallery_category";
		if (isset($q) && trim($q)!="") {
			$page_url .= "/q/".rawurlencode($q);
		}
		if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
			$page_url .= "/child_id_enc/".rawurlencode($tbl_student_id);
		}
		if (isset($sort_name) && trim($sort_name)!="") {
			$page_url .= "/sort_name/".rawurlencode($sort_name_param);
		}
		if (isset($sort_by) && trim($sort_by)!="") {
			$page_url .= "/sort_by/".rawurlencode($sort_by);
		}
		$page_url .= "/offset";
		
		$this->load->library('pagination');
		$config['base_url'] = $page_url;
		$config['total_rows'] = $total_gallery_categories;
		$config['per_page'] = TBL_GALLERY_CATEGORY_PAGING;//constant
		$config['uri_segment'] = $this->uri->total_segments();
		$config['num_links'] = 5;

		$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
		$config['next_link_disable'] = '';

		$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
		$config['prev_link_disable'] = "";		
			
		$config['first_link'] = "";
		$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
		$config['first_tag_close'] = '</span>';
		
		$config['last_link'] = "";
		$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
		$config['last_tag_close'] = '</span>';

		$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
		$config['cur_tag_close'] = "&nbsp;</span>";
		
		$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
		$config['num_tag_close'] = "&nbsp;</span>";

		$this->pagination->initialize($config);
		$start = $offset + 1;
		$range = "";
		if ($offset+TBL_GALLERY_CATEGORY_PAGING >= $total_gallery_categories) {
			$range = $total_gallery_categories;
		} else {
			$range = $offset+TBL_GALLERY_CATEGORY_PAGING;
		}

		$paging_string = "$start - $range <font color='#333'>of $total_gallery_categories category(s)</font>&nbsp;";
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		
		$data['data_gallery_categories']  = $data_gallery_categories;
		$data['total_gallery_categories'] = $total_gallery_categories;

		$this->load->view('parent/view_parent_template',$data);
	}
	
	
	//View Gallery Pictures
	/**
	* @param   none
	* @access  default
	*/
    function view_school_gallery_details() {
		$data['page'] = "view_school_gallery_details";
		$data['menu'] = "gallery";
        $data['sub_menu'] = "gallery";
		
		$this->load->model("model_parenting_school");

		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		
		
		$tbl_gallery_category_id = "";
		if (array_key_exists('tbl_gallery_category_id',$param_array)) {
			$tbl_gallery_category_id = $param_array['tbl_gallery_category_id'];
			$data['tbl_gallery_category_id'] = $tbl_gallery_category_id;
		}
		
		$tbl_school_id = "";
		if (array_key_exists('tbl_school_id',$param_array)) {
			$tbl_school_id = $param_array['tbl_school_id'];
			$data['tbl_school_id'] = $tbl_school_id;
		}
		
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];
			$data['tbl_student_id'] = $tbl_student_id;
		}
		
	
		$this->load->model('model_gallery');
		$data_category = $this->model_gallery->get_gallery_category_name($tbl_gallery_category_id);
		$data_gallery_images = $this->model_gallery->get_gallery_category_images($tbl_gallery_category_id);
		
		$data['data_category'] = $data_category;
		$data['data_gallery_images'] = $data_gallery_images;
		
		$this->load->view('parent/view_parent_template', $data);
	}
	
    
	// Start Child Progress Report
	 function child_progress_report()
	{
		$data['page'] = "view_progress_report";
		$data['menu'] = "reports";
        $data['sub_menu'] = "student_progress_report";
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		
		$sort_name = "first_name";
		$sort_by = "ASC";
		
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			if (trim($offset) == "") {
				$offset = 0;	
			}
		}	 
		
		if (array_key_exists('q',$param_array)) {
			$q = $param_array['q'];
		}
		
		if (array_key_exists('sort_by_click',$param_array)) {
			$sort_by_click = $param_array['sort_by_click'];
		}	 
		if (array_key_exists('sort_name',$param_array)) {
			$sort_name_param = $param_array['sort_name'];
			
			switch($sort_name_param) {
				case("A"): {
					$sort_name = "first_name";
					 break;
				}
				default: {
					$sort_name = "first_name";
				}					
			}
		}	 
		
		if (array_key_exists('sort_by',$param_array)) {
			$sort_by = $param_array['sort_by'];
		}	 
		
		if (trim($sort_by_click) == "Y") { 
			if (trim($sort_by) == "ASC") {
				$sort_by = "DESC";
			} else if (trim($sort_by) == "DESC") {
				$sort_by = "ASC";
			}
		}
		
		$tbl_student_id = "";
		if (array_key_exists('tbl_student_id',$param_array)) {
			$tbl_student_id = $param_array['tbl_student_id'];                               
			$data['tbl_sel_student_id'] = $tbl_student_id;
		}
		
		$tbl_class_id = "";
		if (array_key_exists('tbl_class_id',$param_array)) {
			$tbl_class_id = $param_array['tbl_class_id'];
			$data['tbl_sel_class_id'] = $tbl_class_id;
		}
		
		$data['sort_by_click'] = $sort_by_click;			
		$data['sort_name'] = $sort_name;			
		$data['sort_name_param'] = $sort_name_param;			
		$data['sort_by'] = $sort_by;			
		$data['icon_sort'] = $icon_sort;			
		
		$data['q'] = $q;
		$data['offset'] = $offset;
	   
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		$this->load->model("model_teachers");
		$this->load->model("model_classes");
		$this->load->model("model_report");
		
		$is_active = "";
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  
	
		$countriesObj          = $this->model_student->get_country_list('Y');
		$data['countries_list']= $countriesObj;
			
		$academicObj          = $this->model_student->get_academic_year('Y',$tbl_school_id);
		$data['academic_list']= $academicObj;
			
			
		$classesObj 		   = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
		$data['classes_list'] = $classesObj;
			
		$rs_all_semesters         = $this->model_classes->get_academic_semesters($tbl_school_id,$tbl_sel_academic_year);
		$data['rs_all_semesters'] = $rs_all_semesters;	
		
		if($tbl_class_id<>"")
		{
			$students_list = $this->model_student->get_all_students_against_class($tbl_class_id);
			$data['rs_all_students'] = $students_list;
		}else{
			$data['rs_all_students'] =  array();
		}
		
		
			$this->load->model('model_student');
			$current_date           = date("Y-m-d");
				
			$data["semester_title"] = "";
			if($tbl_semester_id=="")
			{
				$getSemesterId   = $this->model_student->get_semester_id($school_id,$current_date);
				$tbl_semester_id = $getSemesterId[0]['tbl_semester_id'];
				if($lan=="en")
					$semester_title = $getSemesterId[0]['title'];
				else
					$semester_title = $getSemesterId[0]['title_ar'];
			
			 $data["semester_title"] = $semester_title;
			}
			
			$tbl_teacher_id = "";
			$this->load->model('model_config');
			$this->load->model('model_classes');
			$this->load->model('model_student');
			$this->load->model('model_school_subject');
			
			$rs_subject 		= $this->model_school_subject->get_all_school_subjects('', '', '', $q, 'Y', $tbl_school_id);
			
			$rs_progress_report = $this->model_student->get_progress_report_list($tbl_school_id, $tbl_teacher_id);
			// Progress Student Report List
		
			$param_array = $this->uri->uri_to_assoc(3);
			$q = "";
			$sort_by_click = "N";
			$sort_name_param = "A";
			
			$sort_name = "id";
			$sort_by = "DESC";
			
			$icon_sort = "icon_bottom.jpg";
			$offset = 0;
			$total_categories = 0;
						
			if (array_key_exists('offset',$param_array)) {
				$offset = $param_array['offset'];
				if (trim($offset) == "") {
					$offset = 0;	
				}
			}	 
			
			if (array_key_exists('tbl_student_id',$param_array)) {
				$tbl_student_id = $param_array['tbl_student_id'];
			}
		
			
			if (array_key_exists('sort_by_click',$param_array)) {
				$sort_by_click = $param_array['sort_by_click'];
			}	 
			if (array_key_exists('sort_name',$param_array)) {
				$sort_name_param = $param_array['sort_name'];
				
				switch($sort_name_param) {
					case("A"): {
						$sort_name = "id";
						 break;
					}
					default: {
						$sort_name = "id";
					}					
				}
			}	 
			
			if (array_key_exists('sort_by',$param_array)) {
				$sort_by = $param_array['sort_by'];
			}	 
			
			if (trim($sort_by_click) == "Y") { 
				if (trim($sort_by) == "ASC") {
					$sort_by = "DESC";
				} else if (trim($sort_by) == "DESC") {
					$sort_by = "ASC";
				}
			}
			
			$data['tbl_sel_student_id']  = $tbl_student_id; 
			$data['sort_by_click']   = $sort_by_click;			
			$data['sort_name']       = $sort_name;			
			$data['sort_name_param'] = $sort_name_param;			
			$data['sort_by']         = $sort_by;			
			$data['icon_sort']       = $icon_sort;			
			
			$data['q']      = $q;
			$data['offset'] = $offset;
		   
			$this->load->model("model_school_roles");
			
			$is_active = "";
			
			$tbl_parent_id = "";
			if (array_key_exists('tbl_parent_id',$param_array)) {
				$tbl_parent_id = $param_array['tbl_parent_id'];                               
				$data['tbl_sel_parent_id'] = $tbl_parent_id;
			}
			
			$rs_student_info  = $this->model_student->get_student_obj($tbl_student_id);
			$tbl_class_id     =  $rs_student_info[0]['tbl_class_id'];
			$tbl_school_id    =  $rs_student_info[0]['tbl_school_id'];
			$is_active        = "Y";
			
			$rs_all_students      		= $this->model_student->get_all_students($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_class_search_id,$tbl_parent_id);
			$data['rs_all_students'] 	= $rs_all_students;
			
			if($tbl_school_id<>"" && $tbl_student_id <>"")
			{  
				$rs_all_progress_reports_student      = $this->model_student->get_all_progress_reports_student($sort_name, $sort_by, $offset, $q, $is_active, $tbl_class_id, $tbl_student_id, $tbl_school_id);
				$total_progress_reports_student       = $this->model_student->get_total_progress_reports_student($q, $is_active, $tbl_class_id, $tbl_student_id, $tbl_school_id);
			}else{
				$rs_all_progress_reports_student     = array();
				$total_progress_reports_student      = 0;
			}
		
			//PAGINATION CLASS
			$page_url = HOST_URL."/".LAN_SEL."/parent/parent_user/child_progress_report";
			if (isset($q) && trim($q)!="") {
				$page_url .= "/q/".rawurlencode($q);
			}
			if (isset($tbl_school_type_id) && trim($tbl_school_type_id)!="") {
				$page_url .= "/tbl_school_type_id/".rawurlencode($tbl_school_type_id);
			}
			if (isset($sort_name) && trim($sort_name)!="") {
				$page_url .= "/sort_name/".rawurlencode($sort_name_param);
			}
			if (isset($sort_by) && trim($sort_by)!="") {
				$page_url .= "/sort_by/".rawurlencode($sort_by);
			}
			$page_url .= "/offset";
			
			$this->load->library('pagination');
			$config['base_url'] = $page_url;
			$config['total_rows'] = $total_progress_reports_student;
			$config['per_page'] = TBL_SCHOOL_ROLES_PAGING;//constant
			$config['uri_segment'] = $this->uri->total_segments();
			$config['num_links'] = 5;
	
			$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
			$config['next_link_disable'] = '';
	
			$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
			$config['prev_link_disable'] = "";		
				
			$config['first_link'] = "";
			$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
			$config['first_tag_close'] = '</span>';
			
			$config['last_link'] = "";
			$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
			$config['last_tag_close'] = '</span>';
	
			$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
			$config['cur_tag_close'] = "&nbsp;</span>";
			
			$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
			$config['num_tag_close'] = "&nbsp;</span>";
	
			$this->pagination->initialize($config);
			$start = $offset + 1;
			$range = "";
			if ($offset+TBL_SCHOOL_ROLES_PAGING >= $total_progress_reports_student) {
				$range = $total_progress_reports_student;
			} else {
				$range = $offset+TBL_SCHOOL_ROLES_PAGING;
			}
	
			$paging_string = "$start - $range <font color='#333'>of $total_progress_reports_student reports</font>&nbsp;";
			$data['paging_string'] = $paging_string;
			$data['start']         = $start;
			
			// End Progress Student Report List
		
			//new change
			//$data_config = $this->model_config->get_config($school_id);
			
			if($tbl_class_id<>""){
				$class_info = $this->model_classes->getClassInfo($tbl_class_id);
				$student_info = $this->model_student->get_student_obj($tbl_student_id);
			}
		
		
			// Get Student Picture
			$file_name_updated = $this->model_student->get_student_picture($tbl_student_id);
		
			$this->load->model('model_teachers');
			$all_classes_against_teacher_rs 		= $this->model_teachers->get_all_classes_against_teacher_t($tbl_teacher_id);
			$data["file_name_updated"] 				= $file_name_updated;
			$data["all_classes_against_teacher_rs"] = $all_classes_against_teacher_rs;
			$data["rs_subject"] 					= $rs_subject;
			$data["rs_progress_report"] 			= $rs_progress_report;  // category
	
			$data["progress_reports_student"] 		= $rs_all_progress_reports_student;   
			$data["total_progress_reports_student"] = $total_progress_reports_student;  
			
			$data["student_info"] 					= $student_info;
			$data["class_info"] 					= $class_info; 
			
		$user_panel   = $_SESSION['aqdar_smartcare']['user_type_sess'];  
		$this->load->view('parent/view_parent_template',$data);
		
	}
	
	
	function view_child_progress_card() {
		$data['page']     = "view_progress_report";
		$data['menu']     = "progress_report";
        $data['sub_menu'] = "progress_report";
		
		$this->load->model("model_student");
		$tbl_progress_report_id  = $_POST['tbl_progress_report_id'];	
		$tbl_progress_id         = $_POST['tbl_progress_id'];	
		$tbl_school_id           = $_POST['tbl_school_id'];	
		$tbl_student_id          = $_POST['tbl_student_id'];	
		$is_active = "";
		if($tbl_school_id<>"")
		{  
		   $progress_report_details          = $this->model_student->get_progress_report_details($tbl_progress_report_id, $tbl_progress_id, $tbl_class_id, $tbl_student_id, $tbl_school_id);
	       $data['progress_report_details'] = $progress_report_details;  
		}
		
		$this->load->view('parent/view_child_progress_card.php', $data);
	}
		
}
?>