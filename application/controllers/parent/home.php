<?php

/**
 * @desc   	  	Home Controller
 * @category   	Controller
 * @author     	Shanavas PK
 * @version    	0.1
 */
class Home extends CI_Controller {


	/**
	* @desc    Default function for the Controller
	* @param   none
	* @access  default
	*/
    function index() {
		
		$this->welcome();
	}


	/**
	* @desc    Welcome page
	* @param   none
	* @access  default
	*/
    function welcome() {
		 $data = array();
		 $data['page'] = "welcome";
		 $month = array(); 
		 $this->load->model('model_student');
		 $this->load->model('model_teachers');
		 $tbl_school_id = isset($_SESSION['aqdar_smartcare']['tbl_school_id_sess'])? $_SESSION['aqdar_smartcare']['tbl_school_id_sess']:'';
		 $total_student_school = $this->model_student->get_total_students_against_school($tbl_school_id);
		 $total_teacher_school = $this->model_teachers->get_total_teachers_against_school($tbl_school_id);
		 $data['total_teacher_school']        = $total_teacher_school;
		 $data['total_student_school']        = $total_student_school;
		 $this->load->view("parent/view_parent_template", $data);
		 
	}

}
?>

