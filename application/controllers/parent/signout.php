<?php

/**
 * @desc   	  	Signout Controller
 * @category   	Controller
 * @author     	Shanavas.PK
 * @version    	0.1
 */
class Signout extends CI_Controller {

	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
    function index() {

			unset($_SESSION['aqdar_smartcare']['tbl_admin_user_id_sess']);
			unset($_SESSION['aqdar_smartcare']['tbl_admin_id_sess']);
			unset($_SESSION['aqdar_smartcare']['module_access']);
			unset($_SESSION['aqdar_smartcare']['tbl_school_id_sess']);
			unset($_SESSION['aqdar_smartcare']['admin_first_name_sess']);
			unset($_SESSION['aqdar_smartcare']['admin_last_name_sess']);
			
	        unset($_SESSION['aqdar_smartcare']['tbl_admin_id_sess']);
			unset($_SESSION['aqdar_smartcare']['tbl_admin_user_id_sess']);
			unset($_SESSION['aqdar_smartcare']['admin_email_sess']);
			unset($_SESSION['aqdar_smartcare']['admin_first_name_sess']);
			unset($_SESSION['aqdar_smartcare']['admin_last_name_sess']);
			unset($_SESSION['aqdar_smartcare']['admin_first_name_sess_ar']);
			unset($_SESSION['aqdar_smartcare']['admin_last_name_sess_ar']);
			unset($_SESSION['aqdar_smartcare']['added_date_sess']);
			unset($_SESSION['aqdar_smartcare']['user_type_sess']);
			unset($_SESSION['aqdar_smartcare']['admin_auth_sess']);
			unset($_SESSION['aqdar_smartcare']['module_access']);
			
			unset($_SESSION['aqdar_smartcare']['tbl_teacher_id_sess']);
			unset($_SESSION['aqdar_smartcare']['teacher_email_sess']);
			unset($_SESSION['aqdar_smartcare']['teacher_first_name_sess']);
			unset($_SESSION['aqdar_smartcare']['teacher_last_name_sess']);
			unset($_SESSION['aqdar_smartcare']['teacher_picture_sess']);
			unset($_SESSION['aqdar_smartcare']['added_date_sess']);
		
			//session_destroy();
			/*header("Location: ".HOST_URL."/".LAN_SEL."/parent/parent_user/login_form");*/
			header("Location: ".HOST_URL."/".LAN_SEL."/school_web");
	}
	
}
?>