<?php



/**

 * @desc   	  	Attendance Controller

 *

 * @category   	Controller

 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>

 * @version    	0.1

 */

class Attendance extends CI_Controller {





	/**

	* @desc    Default function for the Controller

	*

	* @param   none

	* @access  default

	*/

    function index() {

		//Do nothing

	}





	/**

	* @desc    Image Gallery

	*

	* @param   none

	* @access  default

	*/

    function attendance_page_teacher() {

		$lan = $_REQUEST["lan"];

		$device = $_REQUEST["device"];

		$device_uid = $_REQUEST["device_uid"];

		$tbl_school_id = $_REQUEST["tbl_school_id"];

		$data["lan"] = $lan;

		$data["device"] = $device;

		$data["device_uid"] = $device_uid;

		$data["tbl_school_id"] = $tbl_school_id;	

		

		$att_option = $_REQUEST["att_option"];

		

		date_default_timezone_set('Asia/Dubai');

		$attendance_date = date("Y-m-d");

		$data["attendance_date"] = $attendance_date;	



		$tbl_teacher_id_sel = $_REQUEST["user_id"];

		

		$this->load->model('model_attendance');

		$data_rs = $this->model_attendance->get_attendance_details($attendance_date, $att_option, $tbl_teacher_id_sel);



		//print_r($data_rs);

		

		if (count($data_rs)<=0) {

			echo "{\"code\":\"N\"}";

			return;

		}

		

		$data["data_rs"] = $data_rs;

		

		$data["page"] = "view_attendance_page_teacher";

		$this->load->view('view_template',$data);

	}

	

	

	/**

	* @desc    Image Gallery

	*

	* @param   none

	* @access  default

	*/

    function child_bus_attendance() {

		$user_id = $_REQUEST["user_id"];

		$role = $_REQUEST["role"];

		$lan = $_REQUEST["lan"];

		$device = $_REQUEST["device"];

		$device_uid = $_REQUEST["device_uid"];

		$school_id = $_REQUEST["school_id"];

		$tbl_student_id = $_REQUEST["tbl_student_id"];

		

		$data["user_id"] = $user_id;

		$data["role"] = $role;

		$data["lan"] = $lan;

		$data["device"] = $device;

		$data["device_uid"] = $device_uid;

		$data["school_id"] = $school_id;

		$data["tbl_student_id"] = $tbl_student_id;

		

		$this->load->model('model_attendance');

		$data_rs = $this->model_attendance->get_child_bus_attendance($tbl_student_id);



		if (count($data_rs) <=0) {

			$arr["code"] = "200";

			echo json_encode($arr);

			return;

		}

		

		//print_r($data_rs );

		$data["data_rs"] = $data_rs;

		$data["page"] = "view_attendance_page_bus";

		$this->load->view('view_template',$data);

	}

	

	

	/**

	* @desc    Image Gallery

	*

	* @param   none

	* @access  default

	*/

    function attendance_page() {

		$lan = $_REQUEST["lan"];

		$device = $_REQUEST["device"];

		$device_uid = $_REQUEST["device_uid"];

		$tbl_school_id = $_REQUEST["tbl_school_id"];

		$data["lan"] = $lan;

		$data["device"] = $device;

		$data["device_uid"] = $device_uid;

		$data["tbl_school_id"] = $tbl_school_id;	

		

		$tbl_bus_id = $_REQUEST["tbl_bus_id"];

		$att_option = $_REQUEST["att_option"];

		date_default_timezone_set('Asia/Dubai');



		$attendance_date = date("Y-m-d");

		$data["attendance_date"] = $attendance_date;	

		

		//print_r(date("d/m/Y"));

		$this->load->model('model_student');

		$data_rs = $this->model_student->get_all_bus_students($tbl_bus_id);

		if (count($data_rs)<=0) {

			echo "--no--";

			return;

		}

		$data["page"] = "view_attendance_page";

		$this->load->view('view_template',$data);

	}

	

	

	/**

	* @desc    Image Gallery

	*

	* @param   none

	* @access  default

	*/

    function submit_attendance() {

		$lan = $_REQUEST["lan"];

		$device = $_REQUEST["device"];

		$device_uid = $_REQUEST["device_uid"];

		$tbl_school_id = $_REQUEST["tbl_school_id"];

		$data["lan"] = $lan;

		$data["device"] = $device;

		$data["device_uid"] = $device_uid;

		$data["tbl_school_id"] = $tbl_school_id;

		

		date_default_timezone_set('Asia/Dubai');

		

		$att_str = $_REQUEST["att_str"];

		$tbl_bus_id = $_REQUEST["tbl_bus_id"];

		$attendance_date = date("Y-m-d");

		$att_option = $_REQUEST["att_option"];

		

		$this->load->model('model_attendance');

		$this->model_attendance->delete_attendance($tbl_bus_id, $attendance_date, $att_option);

		 

		if ($att_option == "M") {	// Morning Attendance

			$message_ = " has successfully reached school.";	// Afternoon Attendance

		} else {

			$message_ = " has left for home on school transport (Bus).";	// Afternoon Attendance					

		}

		$tbl_teacher_id_str = "";

		$tbl_class_id_str = "";

		$att_str_arr = explode(",",$att_str);

		

		for ($i=0; $i<(count($att_str_arr)-1); $i++) {

			$tbl_student_id_arr = explode(":",$att_str_arr[$i]);

			$tbl_student_id = $tbl_student_id_arr[0];

			$is_present = $tbl_student_id_arr[1];

			if (trim($tbl_student_id) == "") {continue;}

			

			$this->load->model('model_attendance');

			$this->model_attendance->save_attendance($tbl_student_id, $tbl_bus_id, $attendance_date, $is_present, $att_option, $tbl_school_id);

			

			if ($att_option == "M" && $is_present == "Y") {

				$message_ = " has entered the school bus.";

			} else if ($att_option == "M" && $is_present == "N") {

				$message_ = " did not reach for school bus.";

			}

			

			$this->load->model('model_parents');

			$tbl_parent_id = $this->model_parents->get_parent_of_student($tbl_student_id);

			

			$this->load->model('model_student');

			$stu_obj = $this->model_student->get_student_obj($tbl_student_id);

			$first_name_stu = $stu_obj[0]['first_name'];

			$last_name_stu = $stu_obj[0]['last_name'];

			$tbl_class_id = $stu_obj[0]['tbl_class_id'];

			$tbl_class_id_str = $tbl_class_id_str."'".$tbl_class_id."',";

			$message = 'Dear Parent, your Child '.$first_name_stu.' '.$last_name_stu.' '.$message_;

			//save_attendance($tbl_teacher_id, $tbl_student_id, $tbl_parent_id, $option1, $option2, $option3, $option4, $message);

			

			$this->load->model('model_user_notify_token');

			$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_parent_id);

			

			for ($b=0; $b<count($data_tkns); $b++) {

				//$token = $tbl_parent_id."-".$data_tkns[$b]["token"];

				$token = $data_tkns[$b]["token"];
				$device = $data_tkns[$b]["device"];

				if (trim(ENABLE_PUSH) == "Y") {	

					$message = urlencode($message);

					//$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$tbl_parent_id."&msg=".$message."&data=";

					$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$token."&msg=".$message."&data=";

					

					$this->load->model('model_user_notify_token');

					$this->model_user_notify_token->send_notification($token , $message, $device);

					//echo "<br>".$url;

					//$ch = curl_init();

					//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

					//curl_setopt($ch, CURLOPT_URL, $url);

					//curl_setopt($ch, CURLOPT_HEADER, 0);

					//curl_exec($ch);

					//curl_close($ch);

				}//if (trim(ENABLE_PUSH) == "Y")

			}

		}

		echo "--yes--";

	}

	

	

	/**

	* @desc    Image Gallery

	*

	* @param   none

	* @access  default

	*/

    function send_absent_notification() {

		$lan = $_REQUEST["lan"];

		$device = $_REQUEST["device"];

		$device_uid = $_REQUEST["device_uid"];

		$tbl_school_id = $_REQUEST["tbl_school_id"];

		$data["lan"] = $lan;

		$data["device"] = $device;

		$data["device_uid"] = $device_uid;

		$data["tbl_school_id"] = $tbl_school_id;

		

		$tbl_student_id = $_REQUEST["tbl_student_id"];

		$module_name = $_REQUEST["module_name"];

		

		$this->load->model('model_parents');

		$tbl_parent_id = $this->model_parents->get_parent_of_student($tbl_student_id);

		

		$this->load->model('model_student');

		$stu_obj = $this->model_student->get_student_obj($tbl_student_id);

		$first_name_stu = $stu_obj[0]['first_name'];

		$last_name_stu = $stu_obj[0]['last_name'];

		$tbl_class_id = $stu_obj[0]['tbl_class_id'];

		

		$message_ = " has not arrived for the school bus at subscribed location.";	// Afternoon Attendance					

		$message = 'Dear Parent, your Child '.$first_name_stu.' '.$last_name_stu.' '.$message_;

		

		$this->load->model('model_user_notify_token');

		$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_parent_id);

		

		for ($b=0; $b<count($data_tkns); $b++) {

			if (trim(ENABLE_PUSH) == "Y") {	

				$message = urlencode($message);

				//$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$tbl_parent_id."&msg=".$message."&data=";

				//$token = $tbl_parent_id."-".$data_tkns[$b]["token"];

				$token = $data_tkns[$b]["token"];

				$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$token."&msg=".$message."&data=";

				

				$this->load->model('model_user_notify_token');

				$this->model_user_notify_token->send_notification($token , $message, $device);//$ch = curl_init();

				

				//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

				//curl_setopt($ch, CURLOPT_URL, $url);

				//curl_setopt($ch, CURLOPT_HEADER, 0);

				//curl_exec($ch);

				//curl_close($ch);

			}

		}

		echo "--yes--";

	}

}

?>



