<?php

/**
 * @desc   	  	Home Controller
 * @category   	Controller
 * @author     	Shanavas PK
 * @version    	0.1
 */
class Home extends CI_Controller {


	/**
	* @desc    Default function for the Controller
	* @param   none
	* @access  default
	*/
    function index() {
		
		$this->welcome();
	}


	/**
	* @desc    Welcome page
	* @param   none
	* @access  default
	*/
    /*function welcome() {
		 $data = array();
		 $data['page'] = "welcome";
		 $month = array(); 
		 $this->load->model('model_student');
		 $this->load->model('model_teachers');
		 $tbl_school_id = isset($_SESSION['aqdar_smartcare']['tbl_school_id_sess'])? $_SESSION['aqdar_smartcare']['tbl_school_id_sess']:'';
		 $total_student_school = $this->model_student->get_total_students_against_school($tbl_school_id);
		 $total_teacher_school = $this->model_teachers->get_total_teachers_against_school($tbl_school_id);
		 $data['total_teacher_school']        = $total_teacher_school;
		 $data['total_student_school']        = $total_student_school;
		 $this->load->view("students/view_student_template", $data);
	}*/
	
	
	  function welcome() {
		$data['page']     = "view_child_details";
		$data['menu']     = "my_profile";
        $data['sub_menu'] = "my_profile";
		$data['mid'] = "4";//Details
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(3);
		
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
			$data['offset'] = $offset;
		}	 			
		$tbl_student_id = $_SESSION['aqdar_smartcare']['tbl_admin_id_sess'];
		$tab            = "";
		
		if (array_key_exists('tab',$param_array)) {
			$tab = $param_array['tab'];
			$data['tab'] = $tab;
		}	
		
		if($tab==""){
			$tab = "points";
			$data['tab'] = $tab;
		}
		
		$tbl_school_id = "";
		//User details
		$this->load->model("model_student");	
		$student_obj = $this->model_student->student_info($tbl_student_id,$tbl_school_id);
		$data['student_obj'] = $student_obj;
		
		$tbl_class_id  = $student_obj[0]['tbl_class_id'];
		$tbl_parent_id = $student_obj[0]['tbl_parent_id'];
		$tbl_school_id  = $student_obj[0]['tbl_school_id'];
	    /********* Points History ************************/
		if($tab=="points")
		{
	    	$points_obj 			= $this->model_student->student_points_details($q, $tbl_student_id,$tbl_class_id,$tbl_school_id,$offset);
			$data['points_obj'] 	= $points_obj;
		
			$totPoints 			    = $this->model_student->total_points_details($q, $tbl_student_id,$tbl_class_id,$tbl_school_id);
			$data['totPoints'] 	    = $totPoints;
			
			//PAGINATION CLASS
			$page_url = HOST_URL."/".LAN_SEL."/students/home/welcome/";
			if (isset($q) && trim($q)!="") {
				$page_url .= "/q/".rawurlencode($q);
			}
			if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
				$page_url .= "/child_id_enc/".rawurlencode($tbl_student_id);
			}
			if (isset($tab) && trim($tab)!="") {
				$page_url .= "/tab/".rawurlencode($tab);
			}
			/*if (isset($tbl_class_search_id) && trim($tbl_class_search_id)!="") {
				$page_url .= "/tbl_class_search_id/".rawurlencode($tbl_class_search_id);
			}
			if (isset($sort_name) && trim($sort_name)!="") {
				$page_url .= "/sort_name/".rawurlencode($sort_name_param);
			}
			if (isset($sort_by) && trim($sort_by)!="") {
				$page_url .= "/sort_by/".rawurlencode($sort_by);
			}*/
			$page_url .= "/offset";
			
			$this->load->library('pagination');
			$config['base_url'] = $page_url;
			$config['total_rows'] = $totPoints;
			$config['per_page'] = TBL_CARD_CATEGORY_PAGING;//constant
			$config['uri_segment'] = $this->uri->total_segments();
			$config['num_links'] = 5;
	
			$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
			$config['next_link_disable'] = '';
	
			$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
			$config['prev_link_disable'] = "";		
				
			$config['first_link'] = "";
			$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
			$config['first_tag_close'] = '</span>';
			
			$config['last_link'] = "";
			$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
			$config['last_tag_close'] = '</span>';
	
			$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
			$config['cur_tag_close'] = "&nbsp;</span>";
			
			$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
			$config['num_tag_close'] = "&nbsp;</span>";
	
			$this->pagination->initialize($config);
			$start = $offset + 1;
			$range = "";
			if ($offset+TBL_CARD_CATEGORY_PAGING >= $totPoints) {
				$range = $totPoints;
			} else {
				$range = $offset+TBL_CARD_CATEGORY_PAGING;
			}
	
			$paging_string = "$start - $range <font color='#333'>of $totPoints points</font>&nbsp;";
			$data['paging_string'] = $paging_string;
			
		/******** End Points History *********************/
		/******** Cards History **************************/
		} else if($tab=="cards"){
			$cards_obj 				= $this->model_student->student_cards_details($q, $tbl_student_id,$tbl_class_id,$tbl_school_id,$offset);
			$data['cards_obj'] 		= $cards_obj;
		
			$totCards 				= $this->model_student->total_student_cards($q, $tbl_student_id,$tbl_class_id,$tbl_school_id);
			$data['total_cards'] 	= $totCards;	
		
			//PAGINATION CLASS
			$page_url = HOST_URL."/".LAN_SEL."/students/home/welcome/";
			if (isset($q) && trim($q)!="") {
				$page_url .= "/q/".rawurlencode($q);
			}
			if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
				$page_url .= "/child_id_enc/".rawurlencode($tbl_student_id);
			}
			if (isset($tab) && trim($tab)!="") {
				$page_url .= "/tab/".rawurlencode($tab);
			}
			/*if (isset($tbl_class_search_id) && trim($tbl_class_search_id)!="") {
				$page_url .= "/tbl_class_search_id/".rawurlencode($tbl_class_search_id);
			}
			if (isset($sort_name) && trim($sort_name)!="") {
				$page_url .= "/sort_name/".rawurlencode($sort_name_param);
			}
			if (isset($sort_by) && trim($sort_by)!="") {
				$page_url .= "/sort_by/".rawurlencode($sort_by);
			}*/
			$page_url .= "/offset";
			
			$this->load->library('pagination');
			$config['base_url'] = $page_url;
			$config['total_rows'] = $totCards;
			$config['per_page'] = TBL_CARD_CATEGORY_PAGING;//constant
			$config['uri_segment'] = $this->uri->total_segments();
			$config['num_links'] = 5;
	
			$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
			$config['next_link_disable'] = '';
	
			$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
			$config['prev_link_disable'] = "";		
				
			$config['first_link'] = "";
			$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
			$config['first_tag_close'] = '</span>';
			
			$config['last_link'] = "";
			$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
			$config['last_tag_close'] = '</span>';
	
			$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
			$config['cur_tag_close'] = "&nbsp;</span>";
			
			$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
			$config['num_tag_close'] = "&nbsp;</span>";
	
			$this->pagination->initialize($config);
			$start = $offset + 1;
			$range = "";
			if ($offset+TBL_CARD_CATEGORY_PAGING >= $totCards) {
				$range = $totCards;
			} else {
				$range = $offset+TBL_CARD_CATEGORY_PAGING;
			}
	
			$paging_string = "$start - $range <font color='#333'>of $totCards cards</font>&nbsp;";
			$data['paging_string'] = $paging_string;
			
		/******** End Cards History **********************/
		/******** Records History ************************/
		} else if($tab=="records"){
			
			$rs_all_records     	= $this->model_student->get_student_records($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parenting_school_cat_id,$tbl_student_id,$tbl_class_id);
			$data['records_obj'] 	= $rs_all_records;
			
			$total_records     	    = $this->model_student->get_total_student_records($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parenting_school_cat_id,$tbl_student_id,$tbl_class_id);
			$data['total_records'] 	= $total_records;
			
			//PAGINATION CLASS
			$page_url = HOST_URL."/".LAN_SEL."/students/home/welcome/";
			if (isset($q) && trim($q)!="") {
				$page_url .= "/q/".rawurlencode($q);
			}
			if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
				$page_url .= "/child_id_enc/".rawurlencode($tbl_student_id);

			}
			if (isset($tab) && trim($tab)!="") {
				$page_url .= "/tab/".rawurlencode($tab);
			}
		
			$page_url .= "/offset";
			
			$this->load->library('pagination');
			$config['base_url'] = $page_url;
			$config['total_rows'] = $total_records;
			$config['per_page'] = TBL_CARD_CATEGORY_PAGING;//constant
			$config['uri_segment'] = $this->uri->total_segments();
			$config['num_links'] = 5;
	
			$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
			$config['next_link_disable'] = '';
	
			$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
			$config['prev_link_disable'] = "";		
				
			$config['first_link'] = "";
			$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
			$config['first_tag_close'] = '</span>';
			
			$config['last_link'] = "";
			$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
			$config['last_tag_close'] = '</span>';
	
			$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
			$config['cur_tag_close'] = "&nbsp;</span>";
			
			$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
			$config['num_tag_close'] = "&nbsp;</span>";
	
			$this->pagination->initialize($config);
			$start = $offset + 1;
			$range = "";
			if ($offset+TBL_CARD_CATEGORY_PAGING >= $total_records) {
				$range = $total_records;
			} else {
				$range = $offset+TBL_CARD_CATEGORY_PAGING;
			}
	
			$paging_string = "$start - $range <font color='#333'>of $total_records records</font>&nbsp;";
			$data['paging_string'] = $paging_string;
		
		/******** End Records History *******************/
		/******** Message History ***********************/
		} else if($tab=="message"){
			
			$rs_all_messages        = $this->model_student->list_parent_messages_of_student($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parent_id);
			$data['messages_obj']   = $rs_all_messages;
			
			$total_messages         = $this->model_student->get_total_parent_messages_of_student($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id, $tbl_parent_id);
			$data['total_messages'] = $total_messages;
			
					//PAGINATION CLASS
			$page_url = HOST_URL."/".LAN_SEL."/students/home/welcome/";
			if (isset($q) && trim($q)!="") {
				$page_url .= "/q/".rawurlencode($q);
			}
			if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
				$page_url .= "/child_id_enc/".rawurlencode($tbl_student_id);
			}
			if (isset($tab) && trim($tab)!="") {
				$page_url .= "/tab/".rawurlencode($tab);
			}
		
			$page_url .= "/offset";
			
			$this->load->library('pagination');
			$config['base_url'] = $page_url;
			$config['total_rows'] = $total_messages;
			$config['per_page'] = TBL_CARD_CATEGORY_PAGING;//constant
			$config['uri_segment'] = $this->uri->total_segments();
			$config['num_links'] = 5;
	
			$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
			$config['next_link_disable'] = '';
	
			$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
			$config['prev_link_disable'] = "";		
				
			$config['first_link'] = "";
			$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
			$config['first_tag_close'] = '</span>';
			
			$config['last_link'] = "";
			$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
			$config['last_tag_close'] = '</span>';
	
			$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
			$config['cur_tag_close'] = "&nbsp;</span>";
			
			$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
			$config['num_tag_close'] = "&nbsp;</span>";
	
			$this->pagination->initialize($config);
			$start = $offset + 1;
			$range = "";
			if ($offset+TBL_CARD_CATEGORY_PAGING >= $total_messages) {
				$range = $total_messages;
			} else {
				$range = $offset+TBL_CARD_CATEGORY_PAGING;
			}
	
			$paging_string = "$start - $range <font color='#333'>of $total_records records</font>&nbsp;";
			$data['paging_string'] = $paging_string;
			
		} else if($tab=="events")
		{
			 
			$this->load->model("model_events");
			$total_events      = 0;
			$rs_all_events     = array();
			$rs_all_events      = $this->model_events->get_all_events($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id);
			$total_events       = $this->model_events->get_total_events($q, $is_active, $tbl_school_id);
			
			$data['rs_all_events']   = $rs_all_events;
			$data['total_events']    = $total_events;
			
			//PAGINATION CLASS
			$page_url = HOST_URL."/".LAN_SEL."/students/home/welcome/";
			if (isset($q) && trim($q)!="") {
				$page_url .= "/q/".rawurlencode($q);
			}
			if (isset($tbl_student_id) && trim($tbl_student_id)!="") {
				$page_url .= "/child_id_enc/".rawurlencode($tbl_student_id);
			}
			if (isset($tab) && trim($tab)!="") {
				$page_url .= "/tab/".rawurlencode($tab);
			}
		
			$page_url .= "/offset";
			
			$this->load->library('pagination');
			$config['base_url'] = $page_url;
			$config['total_rows'] = $total_messages;
			$config['per_page'] = TBL_CARD_CATEGORY_PAGING;//constant
			$config['uri_segment'] = $this->uri->total_segments();
			$config['num_links'] = 5;
	
			$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
			$config['next_link_disable'] = '';
	
			$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px '><< Prev</span>&nbsp;&nbsp;";
			$config['prev_link_disable'] = "";		
				
			$config['first_link'] = "";
			$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
			$config['first_tag_close'] = '</span>';
			
			$config['last_link'] = "";
			$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
			$config['last_tag_close'] = '</span>';
	
			$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #CCC; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
			$config['cur_tag_close'] = "&nbsp;</span>";
			
			$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #CCC; padding:5px; background-color:#eeeeee; margin-right:3px'>";
			$config['num_tag_close'] = "&nbsp;</span>";
	
			$this->pagination->initialize($config);
			$start = $offset + 1;
			$range = "";
			if ($offset+TBL_CARD_CATEGORY_PAGING >= $total_messages) {
				$range = $total_messages;
			} else {
				$range = $offset+TBL_CARD_CATEGORY_PAGING;
			}
	
			$paging_string = "$start - $range <font color='#333'>of $total_records records</font>&nbsp;";
			$data['paging_string'] = $paging_string;
			$data['pagination_link'] = $this->pagination->create_links();
		}
		
		
		/******** End Message History ******************/
		
		
		$this->load->view('students/view_student_template', $data);
	}
	
	
	

}
?>

