<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Books extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() // to remember use public
    {
        parent::__construct(); 
        $this->load->helper('url');
    }
	
	public function index()
	{
		$data['page']     = "view_books_list";
		$data['menu']     = "school_library";
        $data['sub_menu'] = "library_books";
		
		 if (isset($_SESSION) && trim($_SESSION['aqdar_smartcare']['tbl_admin_id_sess']) == "") {
			header("Location: ".HOST_URL."/index/login/");
			exit;
		}else{
			
			$this->load->model('model_book');
			$this->load->model('model_class_sessions');
			$this->load->model('model_category');
				  
			$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];  	
		    $classesObj = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
				  
		    $sort_by    = "ASC";
		    $sort_name  = "title_en";
			$offset     = 0;
			$total_books= 0;
			$q          = "";
		    $tbl_category_id = "";
			$page = "";
			$list      = "Y";
		
			$list_category = $this->model_category->get_all_book_categories($sort_name, $sort_by, $offset, $q, $list, 'Y');

				   
		   $param_array = $this->uri->uri_to_assoc(3);
			if (array_key_exists('page',$param_array)) {
				$page = $param_array['page'];
			}	
		   if (array_key_exists('offset',$param_array)) {
				$offset = $param_array['offset'];
				if (trim($offset) == "") {
					$offset = 0;	
				}
			}	
			if (array_key_exists('sort_name',$param_array)) {
				$sort_name = $param_array['sort_name'];
				
				switch($sort_name) {
					case("R"): {
						 $sort_name = "id";
						 break;
					}
					case("TR"): {
						 $sort_name = "id";
						 break;
					}
					default: {
						$sort_name = "title_en";
					}					
				}
			}	 
			if (array_key_exists('sort_by',$param_array)) {
				$sort_by = $param_array['sort_by'];
			}else{
				$sort_by = "ASC";
			}
		
			if (array_key_exists('q',$param_array)) {
				$q = trim(urldecode($param_array['q']));
			}
			
			 if (array_key_exists('tbl_category_id',$param_array)) {
				$tbl_category_id = $param_array['tbl_category_id'];
			}
			
			$tbl_class_id 	= $_SESSION['aqdar_smartcare']['tbl_class_id_sess'];
		    $tbl_school_id 	= $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
			
			$list_books  = $this->model_book->get_all_my_books($sort_name, $sort_by, $offset, $q,$tbl_category_id,'',$tbl_class_id,$tbl_school_id);
			$total_books = $this->model_book->get_all_my_total_books($q,$tbl_category_id,'',$tbl_class_id,$tbl_school_id);
	
		 //PAGINATION CLASS
			$page_url = HOST_URL."/".LAN_SEL."/students/books/index/";
			if ($page<>"") {
				$page_url .= "/page/".$page;
			}
			if ($q<>"") {
				$page_url .= "/q/".$q;
			}
			if ($sort_name<>"") {
				$page_url .= "/sort_name/".$sort_name;
			}
			if ($sort_by<>"") {
				$page_url .= "/sort_by/".$sort_by;
			}
			$page_url .= "/offset";
		
		
			$this->load->library('pagination');
			$config['base_url'] = $page_url;
			$config['total_rows'] = $total_books;
			$config['per_page'] = PAGING_TBL_BOOKS;//constant
			$config['uri_segment'] = $this->uri->total_segments();
			$config['num_links'] = 5;
	
			$config['next_link'] = "<span style='color:#3F3F3F; border:solid 1px #ccc; padding:5px; background-color:#eeeeee; margin-right:3px '>Next >></span>&nbsp;&nbsp;";
			$config['next_link_disable'] = '';
	
			$config['prev_link'] ="<span style='color:#3F3F3F; border:solid 1px #ccc; padding:5px; background-color:#eeeeee; margin-right:3px '><<Prev</span>&nbsp;&nbsp;";
			$config['prev_link_disable'] = "";		
				
			$config['first_link'] = "";
			$config['first_tag_open'] = "<span style='background-color:#eeeeee;color:red;'>";
			$config['first_tag_close'] = '</span>';
			
			$config['last_link'] = "";
			$config['last_tag_open'] = "<span style='background-color:#eeeeee;'>";
			$config['last_tag_close'] = '</span>';
	
			$config['cur_tag_open'] = "&nbsp;<span  style='border:solid 1px #ccc; padding:5px; line-height:35px; margin-bottom:10px; margin-top:10px text-decoration:none; margin-right:3px; '>";
			$config['cur_tag_close'] = "&nbsp;</span>";
			
			$config['num_tag_open']="<span style='color:#3F3F3F; border:solid 1px #ccc; padding:5px; background-color:#eeeeee; margin-right:3px'>";
			$config['num_tag_close'] = "&nbsp;</span>";
	
			$this->pagination->initialize($config);
			$start = $offset + 1;
			$range = "";
			if ($offset+PAGING_TBL_BOOKS >= $total_books) {
				$range = $total_books;
			} else {
				$range = $offset+PAGING_TBL_BOOKS;
			}
	
			$paging_string = "$start - $range <font color='#000'>of $total_books books</font>";
		
		$data['offset'] = $offset;
		$data['q'] = $q;
		$data['paging_string'] = $paging_string;
		$data['start'] = $start;
		$data['total_books'] = $total_books;
		$data['list_books'] = $list_books;
		$data['search_data'] = $q;
		$data['classes_list'] = $classesObj;
		$data['list_category'] = $list_category;
	
		$this->load->view('students/view_student_template', $data);
		}
	}

	
	public function book_details()
	{
		$data['page']     = "view_books_list";
		$data['menu']     = "school_library";
        $data['sub_menu'] = "library_books";
		$data['mid'] = "4";
		
	   
	    $param_array = $this->uri->uri_to_assoc(3);
	    if (array_key_exists('bid',$param_array)) {
			$bid = $param_array['bid'];
		}
		if (array_key_exists('offset',$param_array)) {
			$offset = $param_array['offset'];
		}
		
		$this->load->model('model_book');	
		$this->load->model('model_category');
			
		$tbl_book_id = $bid;
		$getBookInfo = $this->model_book->getBookInfo($tbl_book_id);
		
		
		$tbl_book_category_id   = $getBookInfo[0]['tbl_book_category_id'];
		$category_info = $this->model_category->getCategoryDetails($tbl_book_category_id);
		$data['category_name_en'] = $category_info[0]['category_name_en'];
		$data['category_name_ar'] = $category_info[0]['category_name_ar'];
		
		$data['getBookInfo']       = $getBookInfo;
		$data['offset']            = $offset;
		$this->load->view('students/view_student_template', $data);
	}
	
	

	
	
}
