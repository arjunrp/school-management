<?php

/**
 * @desc   	  	Contacts Controller
 *
 * @category   	Controller
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Contacts extends CI_Controller {


	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
    function index() {
		//Do nothing
	}
	
	
	/**
	* @desc    Function to get school contacts page
	*
	* @param   none
	* @access  default
	*/
    function school_contacts() {

		$user_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		
		$data["user_id"] = $user_id;
		$data["role"] = $role;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] = $school_id;
		$data["tbl_student_id"] = $tbl_student_id;
		
		$this->load->model('model_contacts');
		$data_rs = $this->model_contacts->get_school_contacts($lan, $school_id);
       // print_r($data_rs); exit;
		$data["data_rs"] = $data_rs;
		$data["page"] = "view_school_contacts";
		$this->load->view('view_template',$data);
	}
	
	
	/**
	* @desc    Function to save contact details
	*
	* @param   none
	* @access  default
	*/
    function save_contact_details() {

		$user_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		
		$data["user_id"] = $user_id;
		$data["role"] = $role;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] = $school_id;
		$data["tbl_student_id"] = $tbl_student_id;
		
		$tbl_parent_id = $user_id;
		$contact_us_name = $_REQUEST["contact_us_name"];
		$contact_us_email = $_REQUEST["contact_us_email"];
		$contact_us_comments = $_REQUEST["contact_us_comments"];
		
		$this->load->model('model_contacts');
		$this->model_contacts->save_contact_details($lan, $device, $tbl_parent_id, $contact_us_name, $contact_us_email, $contact_us_comments, $school_id);
		$arr["code"] = "200";
		echo json_encode($arr);
	}
}
?>

