<?php



/**

 * @desc   	  	Leave Controller

 *

 * @category   	Controller

 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>

 * @version    	0.1

 */

class Leave extends CI_Controller {





	/**

	* @desc Default constructor for the Controller

	*

	* @access default

	*/

    public function __construct() {

		parent::__construct();

    }





	/**

	* @desc    Save Leave Application

	*

	* @param   none

	* @access  default

	*/

    function save_leave_application() {

		$tbl_student_id = $_REQUEST["tbl_student_id"];

		$application_message = $_REQUEST["application_message"];

		$tbl_school_id = $_REQUEST["school_id"];

		$start_date = $_REQUEST["start_date"];

		$end_date = $_REQUEST["end_date"];

		

		$application_message = str_replace("%20"," ",$application_message);

		

		$this->load->model('model_parents');		

		$tbl_parent_id = $this->model_parents->get_parent_of_student($tbl_student_id);//model_parents		



		$this->load->model('model_student');		

		$stu_obj = $this->model_student->get_student_obj($tbl_student_id);//model_student

		

		$first_name_stu = $stu_obj[0]['first_name'];

		$last_name_stu = $stu_obj[0]['last_name'];

		$tbl_class_id  = $stu_obj[0]['tbl_class_id'];



		$this->load->model('model_classes');

		$class_name = $this->model_classes->get_class_name($tbl_class_id);//model_classes		

		$rs_teachers = $this->model_classes->get_all_teachers_in_class($tbl_class_id);//model_classes	



		$this->load->model('model_teachers');

		$this->load->model('model_message');		

		

		for ($i=0; $i<count($rs_teachers); $i++) {

			$tbl_teacher_id = $rs_teachers[$i]['tbl_teacher_id'];

			$teacher_obj = $this->model_teachers->get_teachers_obj($tbl_teacher_id);//model_teachers

			$first_name_teacher = $teacher_obj[0]['first_name'];

			$last_name_teacher = $teacher_obj[0]['last_name'];

			

			$this->model_message->save_leave_application($tbl_teacher_id, $tbl_student_id, $tbl_parent_id, $application_message, $tbl_school_id, $start_date, $end_date);

			

			$this->load->model('model_user_notify_token');

			$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_teacher_id);

			for ($b=0; $b<count($data_tkns); $b++) {

				if (trim(ENABLE_PUSH) == "Y") {

						$message = 'Dear '.$first_name_teacher.' '.$last_name_teacher.', Student '.$first_name_stu.' '.$last_name_stu.' from '.$class_name.' is on leave today.' ;

						$message = urlencode($message);

						//$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$tbl_teacher_id."&msg=".$message."&data=";

						//$token = $tbl_teacher_id."-".$data_tkns[$b]["token"];

						$token = $data_tkns[$b]["token"];
						$device = $data_tkns[$b]["device"];

						//$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$tbl_teacher_id."&msg=".$message."&data=";

						$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$token."&msg=".$message."&data=";

						//$ch = curl_init();

						//echo "Sending";

						//exit();

						$this->load->model('model_user_notify_token');

						$this->model_user_notify_token->send_notification($token , $message, $device);

						

						//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

						//curl_setopt($ch, CURLOPT_URL, $url);

						//curl_setopt($ch, CURLOPT_HEADER, 0);

						//curl_exec($ch);

						//curl_close($ch);								//$post_data = array();

				}//if (trim(ENABLE_PUSH) == "Y")		

			}

		}//for ($i=0; $i<count($rs_teachers); $i++)

	echo "{\"code\":\"200\"}";

	}

	

	

	/**

	* @desc    Get Leave Applications

	*

	* @param   none

	* @access  default

	*/

    function get_leaves() {

		

		$lan = $_REQUEST["lan"];

		$device = $_REQUEST["device"];

		$device_uid = $_REQUEST["device_uid"];

		$tbl_student_id = $_REQUEST["tbl_student_id"];

		$tbl_teacher_id = $_REQUEST["user_id"];

		$role = $_REQUEST["role"];

		

		$data["lan"] = $lan;

		$data["device"] = $device;

		$data["device_uid"] = $device_uid;

		$data["tbl_student_id"] = $tbl_student_id;

		$data["tbl_teacher_id"] = $tbl_teacher_id;

		$data["role"] = $role;

		

		date_default_timezone_set('Asia/Dubai');

		$today_date = date("Y-m-d");

		

		$this->load->model('model_message');

		$data_rs = $this->model_message->get_leave_applications($tbl_teacher_id, $today_date);

		//print_r($data_rs);

		//echo "Here";

		if (count($data_rs)<=0) {echo "{\"code\":\"200\"}"; return;}

		

		$data['data_rs'] = $data_rs;

		$data['page'] = "view_leave_applications";

		$this->load->view('view_template',$data);

	}

	

	

	/**

	* @desc    Approve or disapprove a leave

	*

	* @param   none

	* @access  default

	*/

    function update_leave() {

		

		$lan = $_REQUEST["lan"];

		$device = $_REQUEST["device"];

		$device_uid = $_REQUEST["device_uid"];

		$tbl_student_id = $_REQUEST["tbl_student_id"];

		$tbl_teacher_id = $_REQUEST["user_id"];

		$role = $_REQUEST["role"];

		$tbl_leave_application_id = $_REQUEST["tbl_leave_application_id"];

		$is_approved = $_REQUEST["is_approved"];					// Y=Approved,R=Rejected,N=None

		$approved_by = $_REQUEST["user_id"];						// tbl_teacher_id

		$authority_comments = $_REQUEST["authority_comments"];		// Comments at the time of approval or rejection

		

		$data["lan"] = $lan;

		$data["device"] = $device;

		$data["device_uid"] = $device_uid;

		$data["tbl_student_id"] = $tbl_student_id;

		$data["tbl_teacher_id"] = $tbl_teacher_id;

		$data["role"] = $role;

		

		$this->load->model('model_student');

		$stu_obj = $this->model_student->get_student_obj($tbl_student_id);

		$first_name_stu = $stu_obj[0]['first_name'];

		$last_name_stu = $stu_obj[0]['last_name'];



		if ($is_approved == "N") {									//  Y=Approved,R=Rejected,N=None

			echo "{\"code\":\"200\"}"; return;

		}

		

		if ($is_approved == "R") {

			$leave_status = "rejected.";

		}

		

		if ($is_approved == "Y") {

			$leave_status = "approved.";

		}

		

		$today_date = date("Y-m-d");

		

		$this->load->model('model_message');

		$this->model_message->update_leave_applications($tbl_leave_application_id, $is_approved, $approved_by, $authority_comments);

		

		$this->load->model('model_parents');		

		$tbl_parent_id = $this->model_parents->get_parent_of_student($tbl_student_id);//model_parents

		

		$this->load->model('model_user_notify_token');

		$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_parent_id);



		$message = 'Dear Parent, leave applicatoin for '.$first_name_stu.' '.$last_name_stu.' has been '.$leave_status ;

		if (trim($authority_comments) != "") {$message .= " Comments: ".$authority_comments; }

		

		for ($b=0; $b<count($data_tkns); $b++) {

			if (trim(ENABLE_PUSH) == "Y") {

				$message = urlencode($message);

				$token = $data_tkns[$b]["token"];

								

				$this->load->model('model_user_notify_token');

				$this->model_user_notify_token->send_notification($token , $message, $device);

			}

		}

		

		echo "{\"code\":\"200\"}"; return;

		exit();

	}

}

?>



