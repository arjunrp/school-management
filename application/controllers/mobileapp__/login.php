<?php
/**
 * @desc   	  	Login Controller
 * @category   	Controller
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Login extends CI_Controller {
	/**
	* @desc    Default function for the Controller
	* @param   none
	* @access  default
	*/

    function index() {
		return "";
	}

	/**
	* @desc    Function to get general login page
	* @param   none
	* @access  default
	*/

    function general_login_page() {
		$lan        = $_REQUEST["lan"];
		$device     = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];

		$tbl_school_id = $_REQUEST["tbl_school_id"];

		$data["lan"] = $lan;

		$data["device"] = $device;

		$data["device_uid"] = $device_uid;

		$data["tbl_school_id"] = $tbl_school_id;

		$data['page'] = "view_general_login_page";

		$this->load->view('view_template',$data);

	}





	/**

	* @desc    Function to get general login

	*

	* @param   none

	* @access  default

	*/

	function general_login() {

		$lan 		= $_REQUEST["lan"];
		$device 	= $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$truncate 	= $_REQUEST["truncate"];
		$data["lan"] 	    = $lan;
		$data["device"]     = $device;
		$data["device_uid"] = $device_uid;
		$data["truncate"]   = $_REQUEST["truncate"];
		$email              = $_REQUEST["email"];
		$password           = $_REQUEST["password"];
		//check for govt login
		$this->load->model("model_admin");
		$result = $this->model_admin->authenticate_g($email,$password);
		$arr = "";

		if(!empty($result)){
			$arr["login_id"] = $result[0]['tbl_admin_id'];
			$arr["role"] = "G";
			echo json_encode($arr);
			exit;
		}
		// Check for teacher login
		$this->load->model("model_teachers");
		$is_exist = $this->model_teachers->authenticate_t($email, $password);
		if ($is_exist == "Y") {
			$tbl_teacher_id = $this->model_teachers->get_teachers_id($email);
			
			$teacher_obj = $this->model_teachers->get_teachers_obj($tbl_teacher_id);
			$data["tbl_school_id"] 		= $teacher_obj[0]["tbl_school_id"];

			$data["first_name"] 		= $teacher_obj[0]["first_name"];

			$data["first_name_ar"] 		= $teacher_obj[0]["first_name_ar"];

			$data["last_name"] 			= $teacher_obj[0]["last_name"];

			$data["last_name_ar"] 		= $teacher_obj[0]["last_name_ar"];

			

			$this->load->model("model_message");

			$data["data_messages"] = $this->model_message->get_all_teacher_messages($tbl_teacher_id);

			$user_type = "T";

			$user_id = $tbl_teacher_id;

			$token = $device_uid;

			

			//$this->load->model("model_user_notify_token");

			//$this->model_user_notify_token->save_user_notify_token($user_type, $user_id, $token, $device);

			$data["tbl_teacher_id"] = $tbl_teacher_id;

			$data["page"] = "view_teachers_landing_page";

			$this->load->view('view_template',$data);

			return;

		}

		

		// Check for parent login

		$this->load->model("model_parents");

		$is_exist = $this->model_parents->authenticate($email, $password);

		if ($is_exist == "Y") {

			

			$this->load->model("model_parents");

			

			$tbl_parent_id 	= $this->model_parents->get_parent_id($email);

			$parent_obj 	= $this->model_parents->get_parent_obj($tbl_parent_id);

			$data["first_name"] 		= $parent_obj[0]["first_name"];

			$data["first_name_ar"] 		= $parent_obj[0]["first_name_ar"];

			$data["last_name"] 			= $parent_obj[0]["last_name"];

			$data["last_name_ar"] 		= $parent_obj[0]["last_name_ar"];

			

			$data["data_stu"] =  $this->model_parents->get_students_of_parent($tbl_parent_id);

			$data["tbl_parent_id"] = $tbl_parent_id;

			

			$user_type = "P";

			$user_id = $tbl_parent_id;

			$token = $device_uid;

			//$this->load->model("model_user_notify_token");

			//$this->model_user_notify_token->save_user_notify_token($user_type, $user_id, $token, $device);

			//print_r($data); exit;

			$data["page"] = "view_parents_landing_page";

			$this->load->view('view_template',$data);

			return;

		}

		

		// Check for bus login

		$bus_code = $email;

		$bus_password = $password;

		$this->load->model("model_bus");

		$is_exist = $this->model_bus->authenticate_bus($bus_code, $bus_password);

		

		if ($is_exist == "Y") {

			$tbl_bus_id = $this->model_bus->get_bus_id($bus_code);

			$tbl_school_id = $this->model_bus->get_tbl_school_id($bus_code);

			// Once successfully logged in we need to delete old tracking session to avoid bus tracking from different devices.

			$this->model_bus->set_bus_tracking_session($tbl_bus_id, $device_uid, $device, $tbl_school_id);

			

			$this->load->model('model_bus_sessions');

			$data_rs = $this->model_bus_sessions->get_bus_sessions($tbl_school_id);

			$arr_data["tbl_bus_id"] = $tbl_bus_id;

			$arr_data["tbl_school_id"] = $tbl_school_id;

			$arr_data["role"] = "B";

			$arr_data["bus_sessions"] = $data_rs;

			

			echo json_encode($arr_data);

			return;

		}

		

		if (trim($tbl_bus_id) == "") {$arr["login_id"] = "N";$arr["role"] = "N";}

		echo json_encode($arr);

		return;

		

	}



	function logout() {

		$user_id = $_REQUEST["user_id"];

		$role = $_REQUEST["role"];

		$lan = $_REQUEST["lan"];

		$device = $_REQUEST["device"];

		$device_uid = $_REQUEST["device_uid"];

		$school_id = $_REQUEST["school_id"];

		$token = $_REQUEST["token"];

		

		$data["user_id"] = $user_id;

		$data["role"] = $role;

		$data["lan"] = $lan;

		$data["device"] = $device;

		$data["device_uid"] = $device_uid;

		$data["school_id"] = $school_id;

		$data["token"] = $token;

		

		$this->load->model("model_user_notify_token");

		$this->model_user_notify_token->delete_user_notify_token($user_id);

		

		$arr["code"] = "200";



		echo json_encode($arr);

	}

}



?>



