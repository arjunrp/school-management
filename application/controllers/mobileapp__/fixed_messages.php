<?php

/**
 * @desc   	  	Fixed Messages Controller
 *
 * @category   	Controller
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Fixed_messages extends CI_Controller {


	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
    function index() {
		//Do nothing
	}


	/**
	* @desc    Get fixed messages that parents/admin can send to bus people
	*
	* @param   none
	* @access  default
	*/
    function get_fixed_messages() {
		$user_id = $_REQUEST["user_id"];				// tbl_parent_id or tbl_admin_id
		$role = $_REQUEST["role"];						// P=Parent Message, A=School Admin messages
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["school_id"];
		$message_type = $_REQUEST["message_type"];
		
		$this->load->model('model_fixed_message');
		$data_rs = $this->model_fixed_message-> get_all_fixed_messages($message_type);
		
		$arr["fixed_messages"] = $data_rs;
		echo json_encode($arr);
	}
	
	
	
	/**
	* @desc    Get fixed messages that parents/admin can send to bus people and also the bus session deails.
	*
	* @param   none
	* @access  default
	*/
    function get_fixed_messages_with_sessions() {
		$user_id = $_REQUEST["user_id"];				// tbl_parent_id or tbl_admin_id
		$role = $_REQUEST["role"];						// P=Parent Message, A=School Admin messages
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["school_id"];
		$message_type = $_REQUEST["message_type"];
		
		$this->load->model('model_fixed_message');
		$data_rs = $this->model_fixed_message-> get_all_fixed_messages($message_type);
		
		$arr["fixed_messages"] = $data_rs;
		
		$this->load->model('model_bus_sessions');
		$data_rs = $this->model_bus_sessions->get_bus_sessions($tbl_school_id);
		
		$arr["bus_sessions"] = $data_rs;
		echo json_encode($arr);
	}
	
	
}
?>

