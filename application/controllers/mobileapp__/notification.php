<?php

/**
 * @desc   	  	Notification Controller
 *
 * @category   	Controller
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Notification extends CI_Controller {


	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
    function index() {
		//Do nothing
	}


	/**
	* @desc    Save Notification Token
	*
	* @param   none
	* @access  default
	*/
    function save() {
		$user_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		$token = $_REQUEST["token"];
		
		//print_r($_REQUEST);
		//exit();
		
		$data["user_id"] = $user_id;
		$data["role"] = $role;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] = $school_id;
		$data["token"] = $token;
		
		//$token_type = $device;
		//$this->load->model('model_notification');
		
		$this->load->model("model_user_notify_token");
		$this->model_user_notify_token->save_user_notify_token($role, $user_id, $token, $device);
		
		//$data_rs = $this->model_notification->save($token, $device_uid, $token_type, $school_id, $token_type);
		
		$arr["code"] = "200";
    	echo json_encode($arr);
	}
	
	function notificationPage()
	{
		$this->load->model("model_school");
		$arr['school'] = $this->model_school->get_all_schools();
		$this->load->model("model_config");
		$arr['country'] = $this->model_config->get_all_country();
		$arr['image_path']  = IMG_SHOW_PATH_LOGO."/";
		echo json_encode($arr);
	}
	function sentMessageToParents()
	{
		$user_id 	= $_REQUEST["user_id"];
		$role 		= $_REQUEST["role"];
		$lan 		= $_REQUEST["lan"];
		$device 	= $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_ids = $_REQUEST["school_ids"];
		$school_type= $_REQUEST["school_type"];
		$country 	= $_REQUEST["country"];
		$message 	= $_REQUEST["message"];
		$item_id 	= $_REQUEST["tbl_item_id"];
		$token 		= $_REQUEST["token"];
		$this->load->model("model_notification");
		$data = $this->model_notification->sentMessageToParents($school_type,$school_ids,$country,$message,$item_id);
		echo json_encode($data);
	}
	
	function sentMessageToTeachers()
	{
		$user_id 	= $_REQUEST["user_id"];
		$role 		= $_REQUEST["role"];
		$lan 		= $_REQUEST["lan"];
		$device 	= $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_ids = $_REQUEST["school_ids"];
		$school_type= $_REQUEST["school_type"];
		$country 	= $_REQUEST["country"];
		$message 	= $_REQUEST["message"];
		$token 		= $_REQUEST["token"];
		$item_id 	= $_REQUEST["tbl_item_id"];
		$this->load->model("model_notification");
		$data = $this->model_notification->sentMessageToTeachers($school_type,$school_ids,$country,$message,$item_id);
		echo json_encode($data);
	}
	function listSentParentNotifications() //from govt.
	{
		$school_id = $_REQUEST["school_id"];
		//echo $school_ids;
		$this->load->model("model_notification");
		$data['messages'] = $this->model_notification->listSentParentNotifications($school_id);
		echo json_encode($data);
	}
	function listSentTeacherNotifications() //from govt.
	{
		$school_id = $_REQUEST["school_id"];
		$this->load->model("model_notification");
		$data['messages'] = $this->model_notification->listSentTeacherNotifications($school_id);
		echo json_encode($data);
	}
	function listParentQueries(){
		$this->load->model("model_notification");
		$data['messages'] = $this->model_notification->getParentQueries($tbl_school_id);
		echo json_encode($data);
	}
	function listBehaviourReport(){
		$this->load->model("model_notification");
		$tbl_category_id = $_REQUEST["tbl_category_id"];
		$data= $this->model_notification->listBehaviourReportForSchools($tbl_category_id);
		echo json_encode($data);
	}
}
?>

