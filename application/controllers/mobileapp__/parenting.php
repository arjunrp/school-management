<?php

/**
 * @desc   	  	Parenting Controller
 *
 * @category   	Controller
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Parenting extends CI_Controller {


	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
    function index() {
		//Do nothing
	}


	/**
	* @desc    Image Gallery
	*
	* @param   none
	* @access  default
	*/
    function parenting_categories() {
		$user_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		
		$data["user_id"] = $user_id;
		$data["role"] = $role;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] = $school_id;
		
		$tbl_parent_id = $user_id;
		
		$this->load->model('model_parenting');
		$data_prnt = $this->model_parenting->get_parenting_categories($school_id);
		$data["data_prnt"] = $data_prnt;
		$data["page"] = "view_parenting_categories_page";
		$this->load->view('view_template',$data); 
	}
	
	
	/**
	* @desc    Image Gallery
	*
	* @param   none
	* @access  default
	*/
    function parenting_page() {
		$user_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		$tbl_parenting_cat_id = $_REQUEST["tbl_parenting_cat_id"];
		
		$data["user_id"] = $user_id;
		$data["role"] = $role;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] = $school_id;
		$data["tbl_parenting_cat_id"] = $tbl_parenting_cat_id;
		
		$this->load->model('model_parenting');
		$data_prnt = $this->model_parenting->get_parenting_data($tbl_parenting_cat_id, $school_id);
		$data["data_prnt"] = $data_prnt;
		$data["page"] = "view_parenting_page";
		$this->load->view('view_template',$data);
	}
	
	
	/**
	* @desc    Image Gallery
	*
	* @param   none
	* @access  default
	*/
    function parenting_text() {
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		
		$tbl_parenting_id = $_REQUEST["tbl_parenting_id"];
		
		$this->load->model('model_parenting');
		$data_prnt = $this->model_parenting->get_parenting_text($tbl_parenting_id);
		$data["page"] = "view_parenting_text_page";
		$this->load->view('view_template',$data);
	}
}
?>

