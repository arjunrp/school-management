<?php







/**



 * @desc   	  	Teachers Controller



 *



 * @category   	Controller



 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>



 * @version    	0.1



 */



class Teachers extends CI_Controller {











	/**



	* @desc    Default function for the Controller



	*



	* @param   none



	* @access  default



	*/



    function index() {



		//Do nothing



	}











	/**



	* @desc    Show teachers login page



	*



	* @param   none



	* @access  default



	*/



    function teachers_login_page() {



		$lan = $_REQUEST["lan"];



		$device = $_REQUEST["device"];



		$device_uid = $_REQUEST["device_uid"];



		$tbl_school_id = $_REQUEST["tbl_school_id"];



		$data["lan"] = $lan;



		$data["device"] = $device;



		$data["device_uid"] = $device_uid;



		$data["tbl_school_id"] = $tbl_school_id;



		



		$tbl_teacher_id = $_REQUEST["tbl_teacher_id"];



    	$data["page"] = "view_teachers_login_page";



		$this->load->view('view_template',$data);



	}



	



	



	/**



	* @desc    Login Parents



	*



	* @param   none



	* @access  default



	*/



    function login_teachers() {



		$lan = $_REQUEST["lan"];



		$device = $_REQUEST["device"];



		$device_uid = $_REQUEST["device_uid"];



		$tbl_school_id = $_REQUEST["tbl_school_id"];



		$data["lan"] = $lan;



		$data["device"] = $device;



		$data["device_uid"] = $device_uid;



		$data["tbl_school_id"] = $tbl_school_id;



		



		$teacher_email_en = $_REQUEST["teacher_email_en"];



		$teacher_password_en =  $_REQUEST["teacher_password_en"];



		



		$tbl_teacher_id = $_REQUEST["tbl_teacher_id"];



		



		$this->load->model('model_teachers');



		$is_exist = $this->model_teachers->authenticate_t($teacher_email_en, $teacher_password_en);



		if ($is_exist == "D") {



			echo "--D--";	// User could not be authenticated as user Does Not Exist



			exit();



		} else if ($is_exist == "N") {



			echo "--N--";



			exit();



		} else if ($is_exist == "Y") {



			$tbl_teacher_id = $this->model_teachers->get_teachers_id($teacher_email_en);



		} else {



			echo "--no--";



			exit();



		}



		



		$data["page"] = "view_teachers_landing_page";



		$this->load->view('view_template',$data);



	}



	



	



	/**



	* @desc    Show teachers login page



	*



	* @param   none



	* @access  default



	*/



    function register_absent_form() {



		



		$lan = $_REQUEST["lan"];



		$device = $_REQUEST["device"];



		$device_uid = $_REQUEST["device_uid"];



		$tbl_school_id = $_REQUEST["tbl_school_id"];



		$data["lan"] = $lan;



		$data["device"] = $device;



		$data["device_uid"] = $device_uid;



		$data["tbl_school_id"] = $tbl_school_id;



		



		$tbl_teacher_id = $_REQUEST["tbl_teacher_id"];



		$tbl_class_id = $_REQUEST["tbl_class_id"];



		$tbl_student_id = $_REQUEST["tbl_student_id"];



		



		// Get Student Picture



		$this->load->model('model_student');



		$file_name_updated = $this->model_student->get_student_picture($tbl_student_id);



		



		$this->load->model('model_teachers');



		$all_classes_against_teacher_rs = $this->model_teachers->get_all_classes_against_teacher_t($tbl_teacher_id);



		



    	$data["page"] = "view_register_absent_form";



		$this->load->view('view_template',$data);



	}



	



	



	/**



	* @desc    Function to load issue cards fpr,



	*



	* @param   none



	* @access  default



	*/



   /* function issue_cards_form() {



		



		$user_id = $_REQUEST["user_id"];



		$role = $_REQUEST["role"];



		$lan = $_REQUEST["lan"];



		$device = $_REQUEST["device"];



		$device_uid = $_REQUEST["device_uid"];



		$school_id = $_REQUEST["school_id"];



		$tbl_class_id = $_REQUEST["tbl_class_id"];



		$tbl_student_id = $_REQUEST["tbl_student_id"];



		



		$data["user_id"] = $user_id;



		$data["role"] = $role;



		$data["lan"] = $lan;



		$data["device"] = $device;



		$data["device_uid"] = $device_uid;



		$data["school_id"] = $school_id;



		$data["tbl_class_id"] = $tbl_class_id;



		$data["tbl_student_id"] = $tbl_student_id;



		



		$this->load->model('model_config');



		$data_config = $this->model_config->get_config($school_id);



		



//		print_r($data_config);



		// Get Student Picture



		$this->load->model('model_student');



		$file_name_updated = $this->model_student->get_student_picture($tbl_student_id);



		



		$this->load->model('model_teachers');



		$all_classes_against_teacher_rs = $this->model_teachers->get_all_classes_against_teacher_t($tbl_teacher_id);



		



		$data["file_name_updated"] = $file_name_updated;



		$data["all_classes_against_teacher_rs"] = $all_classes_against_teacher_rs;



		$data["data_config"] = $data_config;



    	$data["page"] = "view_issue_cards_form";



		$this->load->view('view_template',$data);



	}*/



	



	/**



	* @desc    Function to load issue cards fpr,



	*



	* @param   none



	* @access  default



	*/



    function issue_cards_form() {

		

		$user_id = $_REQUEST["user_id"];

		$role = $_REQUEST["role"];

		$lan = $_REQUEST["lan"];

		$device = $_REQUEST["device"];

		$device_uid = $_REQUEST["device_uid"];

		$school_id = $_REQUEST["school_id"];

		$tbl_class_id = $_REQUEST["tbl_class_id"];

		$tbl_student_id = $_REQUEST["tbl_student_id"];
		
		$tbl_semester_id = $_REQUEST["tbl_semester_id"];

		$data["user_id"] = $user_id;

		$data["role"] = $role;

		$data["lan"] = $lan;

		$data["device"] = $device;

		$data["device_uid"] = $device_uid;

		$data["school_id"] = $school_id;

		$data["tbl_class_id"] = $tbl_class_id;

		$data["tbl_student_id"] = $tbl_student_id;

		$this->load->model('model_student');

		$current_date = date("Y-m-d");
		
		$data["semester_title"] = "";

        if($tbl_semester_id=="")
		{
			$getSemesterId   = $this->model_student->get_semester_id($school_id,$current_date);
			$tbl_semester_id = $getSemesterId[0]['tbl_semester_id'];
			if($lan=="en")
				$semester_title = $getSemesterId[0]['title'];
			else
				$semester_title = $getSemesterId[0]['title_ar'];
		
		 $data["semester_title"] = $semester_title;
		}
      
	    
		$rs_cards = $this->model_student->get_all_cards_student($tbl_student_id,$tbl_semester_id);

		$cardTypeArray = array();

		for($p=0;$p<count($rs_cards);$p++)

		{

			$cardTypeArray[$p] = 	$rs_cards[$p]['card_type'];

		}

		$this->load->model('model_config');

		$this->load->model('model_classes');

		$this->load->model('model_student');

		//new change

		//$data_config = $this->model_config->get_config($school_id);

		$arr_cards = array();

		

		if($tbl_class_id==""){

			$student_info = $this->model_student->get_student_obj($tbl_student_id);

			$tbl_class_id = $student_info[0]['tbl_class_id'];

		}

	

		if($tbl_class_id<>""){

			$class_info = $this->model_classes->getClassInfo($tbl_class_id);

			$tbl_school_type_id = $class_info[0]['tbl_school_type_id'];

		}



		$card_categories = $this->model_config->get_cards_categories_new($school_id,$tbl_school_type_id,$lan);

		

		for($m=0;$m<count($card_categories);$m++)

		{

			$arr_cards[$m]['id'] 		= $card_categories[$m]['tbl_card_category_id'];

			$arr_cards[$m]['title'] 	= $card_categories[$m]['category_name_en'];

			$arr_cards[$m]['title_ar']  = $card_categories[$m]['category_name_ar'];

			/*$arr_cards[$m]['score'] 	= $card_categories[$m]['card_point'];*/

			$issuedPoint = 0;

			if(in_array($arr_cards[$m]['id'], $cardTypeArray))

			{

				 $rs_cards_semester = $this->model_student->get_cards_semester($tbl_student_id, $tbl_semester_id, $school_id, $arr_cards[$m]['id']);

					for ($i=0; $i<count($rs_cards_semester); $i++) {

						$cnt 				= $rs_cards_semester[$i]['cnt'];

						$card_type 			= $rs_cards_semester[$i]['card_type'];

						$card_issue_type    = $rs_cards_semester[$i]['card_issue_type'];

						if($card_issue_type=="issue"){

							$cardData[$card_type]['issue'] = $cnt;

							$cardData[$card_type]['card_type'] = $card_type;

						}else{

							$cardData[$card_type]['cancel'] = $cnt;

							$cardData[$card_type]['card_type'] = $card_type;

						}

						

					}

					$cardCntIssued  = 0;

					foreach($cardData as $key=>$value)

					{ 

						$cardPoint	    = 0;

						$issueCntCard 	= isset($value['issue'])? $value['issue'] : '0';

						$cancelCntCard 	= isset($value['cancel'])? $value['cancel'] : '0';

						$cardCntIssued  = $issueCntCard - $cancelCntCard;

					}

					//echo $cardCntIssued;

					

					$cardCntIssued = abs($cardCntIssued); 

					if(abs($cardCntIssued)<=0){

						$arr_cards[$m]['image'] = "N";

						$arr_cards[$m]['color'] =  "#0a7429";

						$arr_cards[$m]['score']  = "".abs($cardCntIssued);

					}else if($cardCntIssued==1){

						$arr_cards[$m]['image'] = "N";

						$arr_cards[$m]['color'] 	=  "#faf707";

						$arr_cards[$m]['score']  = "-".abs($cardCntIssued);

					}else if($cardCntIssued >=2){

						$arr_cards[$m]['image'] = "N";

						$arr_cards[$m]['color'] 	=  "#fa2307";

						$cardCntIssued = $cardCntIssued - 1;

						$arr_cards[$m]['score']  = "".abs($cardCntIssued);

					}

			}else{

				

				$arr_cards[$m]['score'] 	= "0";

				$arr_cards[$m]['image'] = "N";

			    $arr_cards[$m]['color'] 	=  "#0a7429";

			}

			/*if($card_categories[$m]['card_logo'] <>""){



				$arr_cards[$m]['image'] 	=  IMG_GALLERY_PATH."/".$card_categories[$m]['card_logo'];



				$arr_cards[$m]['color'] 	=  "N";



			}else{



				$arr_cards[$m]['image'] 	=  "N";



				$arr_cards[$m]['color'] 	=  "#".$card_categories[$m]['color'];



			}*/

		}

		

		$rs_student_point 	= $this->model_student->get_all_cards_student($tbl_student_id, $tbl_semester_id);

		$total = 0;

		if(!empty($rs_student_point))

		{

			for($m=0;$m<count($rs_student_point);$m++)

			{

				$total =	$total + $rs_student_point[$m]['card_point'];

			}

		}

		// Get Student Picture

		$this->load->model('model_student');

		$file_name_updated = $this->model_student->get_student_picture($tbl_student_id);

		$this->load->model('model_teachers');

		$all_classes_against_teacher_rs = $this->model_teachers->get_all_classes_against_teacher_t($tbl_teacher_id);

		$data["file_name_updated"] = $file_name_updated;

		$data["all_classes_against_teacher_rs"] = $all_classes_against_teacher_rs;

		//$data["data_config"] = $data_config;

		$data["cards"] = $arr_cards;

		$rs_total_mark_card = $this->model_student->get_total_mark_cards($school_id);

		$total_card_mark = isset($rs_total_mark_card[0]['total_points'])? $rs_total_mark_card[0]['total_points']:'0' ;

		$data["total_mark"] 	= $total_card_mark;

		//$data["total_points"] 	= $total_card_mark + $total_points ; 

		$total_points  = $total_card_mark - abs($total);

		$data["total_points"] 	= $total_card_mark." - ".abs($total)." = ".$total_points ; 

		echo json_encode($data);

		//$data["page"] = "view_issue_cards_form";

		//$this->load->view('view_template',$data);

		//end new change

	}

	/**

	* @desc    Function to load give points form



	*



	* @param   none



	* @access  default



	*/

    function give_points_form() {

		$lan = $_REQUEST["lan"];

		$device = $_REQUEST["device"];

		$device_uid = $_REQUEST["device_uid"];

		$tbl_school_id = $_REQUEST["tbl_school_id"];

		$data["lan"] = $lan;

		$data["device"] = $device;

		$data["device_uid"] = $device_uid;

		$data["tbl_school_id"] = $tbl_school_id;

		$tbl_teacher_id = $_REQUEST["tbl_teacher_id"];

		$tbl_class_id = $_REQUEST["tbl_class_id"];

		$tbl_student_id = $_REQUEST["tbl_student_id"];

		// Get Student Picture

		$this->load->model('model_student');

		$file_name_updated = $this->model_student->get_student_picture($tbl_student_id);

		$this->load->model('model_teachers');

		$all_classes_against_teacher_rs = $this->model_teachers->get_all_classes_against_teacher_t($tbl_teacher_id);

    	$data["page"] = "view_give_points_form";

		$this->load->view('view_template',$data);

	}

	/**

	* @desc    Function to laod contact parents form

	*

	* @param   none

	* @access  default

	*/



    function contact_teacher_page() {



		$user_id = $_REQUEST["user_id"];



		$role = $_REQUEST["role"];



		$lan = $_REQUEST["lan"];



		$device = $_REQUEST["device"];

		$device_uid = $_REQUEST["device_uid"];

		$school_id = $_REQUEST["school_id"];



		



		$data["user_id"] = $user_id;



		$data["role"] = $role;



		$data["lan"] = $lan;



		$data["device"] = $device;



		$data["device_uid"] = $device_uid;



		$data["school_id"] = $school_id;



		



		$tbl_school_id = $school_id;



		$tbl_teacher_id = $user_id;



		



		$this->load->model('model_teachers');



		$data_teachers = $this->model_teachers->get_all_teachers($tbl_teacher_id, $tbl_school_id);



		if (count($data_teachers) <=0) {



			$arr["code"] = "N";



			echo json_encode($arr);



			return;



		}



		



		$data["data_teachers"] = $data_teachers;



    	$data["page"] = "view_contact_teacher_page";



		$this->load->view('view_template',$data);



	}











	/**



	* @desc    Show contact parents sub form



	*



	* @param   none



	* @access  default



	*/



    function teacher_msg_list_page() {



		$user_id = $_REQUEST["user_id"];



		$role = $_REQUEST["role"];



		$lan = $_REQUEST["lan"];



		$device = $_REQUEST["device"];



		$device_uid = $_REQUEST["device_uid"];



		$school_id = $_REQUEST["school_id"];



		$tbl_teacher_id_from = $_REQUEST["tbl_teacher_id_from"];



		$tbl_teacher_id_to = $_REQUEST["tbl_teacher_id_to"];



		$src_id = $_REQUEST["src_id"];					// 



		$tbl_teacher_group_id = $_REQUEST["tbl_teacher_group_id"]; 



			



		$data["user_id"] = $user_id;



		$data["role"] = $role;



		$data["lan"] = $lan;



		$data["device"] = $device;



		$data["device_uid"] = $device_uid;



		$data["school_id"] = $school_id;



		$data["tbl_teacher_id_from"] = $tbl_teacher_id_from;



		$data["tbl_teacher_id_to"] = $tbl_teacher_id_to;



		$data["tbl_teacher_group_id"] = $tbl_teacher_group_id;



		



		$tbl_school_id = $school_id;



		$tbl_teacher_id = $user_id;



		



		$this->load->model('model_message');



		$data_rs = $this->model_message->get_teacher_messages($tbl_teacher_id_from, $tbl_teacher_id_to, $tbl_teacher_group_id,$tbl_school_id);



		//print_r($data_rs); exit;



    	if (count($data_rs) <=0) {



			$arr["code"] = "N";



			echo json_encode($arr);



			return;



		}



		



		// Change all is_read of messages from from N to Y



		$this->model_message->update_unread_msg_count($tbl_teacher_id_from, $tbl_teacher_id_to, $tbl_school_id, $tbl_teacher_group_id);



		$data["data_rs"] = $data_rs;



		$data["page"] = "view_teacher_message_list_page";



		$this->load->view('view_template',$data);



	}











	/**



	* @desc    Function to send message to other teacher



	*



	* @param   none



	* @access  default



	*/



    function send_message_teacher() {



		$user_id = $_REQUEST["user_id"];



		$role = $_REQUEST["role"];



		$lan = $_REQUEST["lan"];



		$device = $_REQUEST["device"];



		$device_uid = $_REQUEST["device_uid"];



		$school_id = $_REQUEST["school_id"];



		$tbl_teacher_id_from = $_REQUEST["tbl_teacher_id_from"];



		$tbl_teacher_id_to = $_REQUEST["tbl_teacher_id_to"];



		$message = urldecode($_REQUEST["message"]);



		



		$data["user_id"] = $user_id;



		$data["role"] = $role;



		$data["lan"] = $lan;



		$data["device"] = $device;



		$data["device_uid"] = $device_uid;



		$data["school_id"] = $school_id;



		$data["tbl_teacher_id_from"] = $tbl_teacher_id_from;



		$data["tbl_teacher_id_to"] = $tbl_teacher_id_to;



		$data["message"] = urldecode($message);



		



		$tbl_school_id = $school_id;



		$tbl_teacher_id = $user_id;



		



		$tbl_teacher_id_from = $_REQUEST["tbl_teacher_id_from"];



		$tbl_teacher_id_to = $_REQUEST["tbl_teacher_id_to"];



		$message = urldecode($_REQUEST["message"]);



		



		$this->load->model('model_message');



		$this->model_message->save_message_teacher($tbl_teacher_id_from, $tbl_teacher_id_to, $message, $tbl_school_id);



		



		$this->load->model('model_teachers');



		$teacher_from_obj = $this->model_teachers->get_teachers_obj($tbl_teacher_id_from);



		$name_from = $teacher_from_ob[0]["first_name"]." ".$teacher_from_ob[0]["last_name"];



		



		$teacher_to_obj = $this->model_teachers->get_teachers_obj($tbl_teacher_id_to);



		$name_to = $teacher_to_ob[0]["first_name"]." ".$teacher_to_ob[0]["last_name"];



	 	



		$this->load->model('model_user_notify_token');



		$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_teacher_id_to);



		



		for ($b=0; $b<count($data_tkns); $b++) {



			if (trim(ENABLE_PUSH) == "Y") {



				$message = 'Dear '.$name_from.', Teacher '.$name_to.' has sent you a message. '.$message ;



				$message = urlencode($message);



				//$token = $tbl_teacher_id_to."-".$data_tkns[$b]["token"];



				$token  = $data_tkns[$b]["token"];
				$device = $data_tkns[$b]["device"];



				//$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$tbl_teacher_id_to."&msg=".$message."&data=";



				$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$token."&msg=".$message."&data=";







				$this->load->model('model_user_notify_token');



				$this->model_user_notify_token->send_notification($token , $message, $device);



				//$ch = curl_init();



				//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);



				//curl_setopt($ch, CURLOPT_URL, $url);



				//curl_setopt($ch, CURLOPT_HEADER, 0);



				//curl_exec($ch);



				//curl_close($ch);



			}//if (trim(ENABLE_PUSH) == "Y")		



		}



		$arr["code"] = "200";



    	echo json_encode($arr);



	}







	



	/**



	* @desc    Get classes against teacher ddm



	*



	* @param   none



	* @access  default



	*/



    function get_all_classes_against_teacher_ddm() {



		$lan = $_REQUEST["lan"];



		$device = $_REQUEST["device"];



		$device_uid = $_REQUEST["device_uid"];



		$tbl_school_id = $_REQUEST["tbl_school_id"];



		$data["lan"] = $lan;



		$data["device"] = $device;



		$data["device_uid"] = $device_uid;



		$data["tbl_school_id"] = $tbl_school_id;



		



		$tbl_teacher_id = $_REQUEST["tbl_teacher_id"];



		



		$this->load->model('model_classes');



		$all_classes_against_teacher_rs = $this->model_classes->get_all_classes_against_teacher($tbl_teacher_id);







		$data["page"] = "view_all_classes_against_teacher_ddm";



		$this->load->view('view_template',$data);



	}



	



	



	/**



	* @desc    Show teachers login page



	*



	* @param   none



	* @access  default



	*/



    function teachers_roles() {



		$lan = $_REQUEST["lan"];



		$device = $_REQUEST["device"];



		$device_uid = $_REQUEST["device_uid"];



		$tbl_school_id = $_REQUEST["school_id"];



		$tbl_teacher_id = $_REQUEST["user_id"];



		



		$data["lan"] = $lan;



		$data["device"] = $device;



		$data["device_uid"] = $device_uid;



		$data["tbl_school_id"] = $tbl_school_id;



		$data["tbl_teacher_id"] = $tbl_teacher_id;



		//echo "--->".$data["tbl_school_id"] ;



		////$this->load->model('model_admin');



		////$data_rs = $this->model_admin->get_admin_users_with_roles($tbl_school_id);



		//print_r($data_rs);



		



		



		



		$this->load->model('model_message');



		$rs_groups = $this->model_message->get_message_groups_against_teacher($tbl_teacher_id, $tbl_school_id);



		//print_r($rs_groups);



		



		$data["rs_groups"] = $rs_groups;



		/*Note: $rs_groups will contain all messages against group. Sometimes tbl_teacher_group_id is blank [message directly send to teacher and not group] so we should alos process those records and assign them to default group of teachers */



		



		



		$data["page"] = "view_teacher_messages";



		$this->load->view('view_template',$data);



	}



}



?>



























