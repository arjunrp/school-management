<?php



/**

 * @desc   	  	Attendance Bus Controller

 *

 * @category   	Controller

 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>

 * @version    	0.1

 */

class Attendance_bus extends CI_Controller {





	/**

	* @desc    Default function for the Controller

	*

	* @param   none

	* @access  default

	*/

    function index() {

		//Do nothing

	}

	

	

	/**

	* @desc    Get list of all bus students along with their attendance and attendance session details

	*

	* @param   none

	* @access  default

	*/

    function get_attendance_bus() {

		$tbl_bus_id = $_REQUEST["user_id"];

		$role = $_REQUEST["role"];

		$lan = $_REQUEST["lan"];

		$device = $_REQUEST["device"];

		$device_uid = $_REQUEST["device_uid"];

		$tbl_school_id = $_REQUEST["school_id"];

		$tbl_bus_sessions_id = $_REQUEST["tbl_bus_sessions_id"];

		$attendance_date = $_REQUEST["attendance_date"];

		

		if (trim($attendance_date) == "") {

			date_default_timezone_set('Asia/Dubai');

			$attendance_date = date("Y-m-d");

		}

		

		$data["tbl_bus_id"] = $tbl_bus_id;

		$data["role"] = $role;

		$data["lan"] = $lan;

		$data["device"] = $device;

		$data["device_uid"] = $device_uid;

		$data["tbl_school_id"] = $tbl_school_id;

		$data["attendance_date"] = $attendance_date;

		$arr_data  = array();

		// Get all the bus students

		$this->load->model('model_bus_student');

		$data_rs = $this->model_bus_student->get_bus_students($tbl_bus_id, $tbl_school_id);

		

		$this->load->model('model_attendance_bus');

		$this->load->model('model_bus_absent_details');

		$this->load->model('model_student');

		if(!empty($data_rs)){

			for($i=0; $i<count($data_rs); $i++) {
	
				$tbl_bus_absent_details_id = "";
	
				$tbl_student_id = $data_rs[$i]["tbl_student_id"];
	
				
	
				$arr_data[$i]["tbl_student_id"] = $tbl_student_id;
	
				$data_stu = $this->model_student->get_student_name($tbl_student_id);
	
				if ($lan == "ar") {
	
					$name = $data_stu[0]["first_name_ar"]." ".$data_stu[0]["last_name_ar"];
	
				} else {
	
					$name = $data_stu[0]["first_name"]." ".$data_stu[0]["last_name"];
	
				}
	
				
	
				$arr_data[$i]["name"] = $name;
	
				
	
				$file_name_updated = $this->model_student->get_student_picture($tbl_student_id);
	
	
	
				if ($file_name_updated != "") {
	
					$arr_data[$i]["picture"] = STUDENT_IMG_SHOW_PATH."/".$file_name_updated;	
	
				} else {
	
					$arr_data[$i]["picture"] = "";	
	
				}
	
				
	
				// Get student's today's attendance for given session
	
				$status = $this->model_attendance_bus->get_attendance_bus($tbl_bus_id, $tbl_student_id, $tbl_bus_sessions_id, $tbl_school_id, $attendance_date);
	
				if ($status == "NA" ) {	// Absent / Attendance Not Taken
	
					$arr_data[$i]["status"] = "";
	
					$arr_data[$i]["tbl_bus_absent_details_id"] = "";
	
				}else
	
				{
	
					if($status=="N")
	
						$status = "L";
	
					// Check if absent details are there in bus absent table
	
					$tbl_bus_absent_details_id = "";
	
					$data_bt = $this->model_bus_absent_details->get_bus_absent_details_student($tbl_bus_id, $tbl_student_id, $tbl_bus_sessions_id, $tbl_school_id);
	
					if ($data_bt[0]["id"] != "") {
	
						
	
						$tbl_bus_absent_details_id = $data_bt[0]["tbl_bus_absent_details_id"];
	
					}
	
					$arr_data[$i]["status"] = $status;
	
					$arr_data[$i]["tbl_bus_absent_details_id"] = $tbl_bus_absent_details_id;
	
				}
	
	
	
				
	
			}
		}
		

		$arr["bus_attendance"] = $arr_data;

		//echo "<br><br><br>";

		echo json_encode($arr);

	}

	

	

	

	/**

	* @desc    Submit Bus Attendance Leave

	*

	* @param   none

	* @access  default

	*/

    function submit_bus_leave() {

		$lan = $_REQUEST["lan"];

		$device = $_REQUEST["device"];

		$device_uid = $_REQUEST["device_uid"];

		$tbl_school_id = $_REQUEST["school_id"];

		$message_type = $_REQUEST["message_type"];							//Message is from P=Parent, A=School Admin

		$message_from = $_REQUEST["user_id"];

		$tbl_fixed_message_id = $_REQUEST["tbl_fixed_message_id"];

		$attendance_date = $_REQUEST["attendance_date"];

		$tbl_student_id = $_REQUEST["tbl_student_id"];

		$tbl_bus_sessions_id = $_REQUEST["tbl_bus_sessions_id"];			

		$tbl_bus_sessions_id_arr = explode(":"	, $tbl_bus_sessions_id);	// User may be submitting leave for more than one session

		

		$this->load->model('model_student');

		$tbl_bus_id = $this->model_student->get_student_bus($tbl_student_id);

			

		$this->load->model('model_bus_absent_details');

		$this->load->model('model_attendance_bus');

		//print_r($tbl_bus_sessions_id_arr);

		for($i=0; $i<count($tbl_bus_sessions_id_arr); $i++) {

			$tbl_bus_sessions_id_ = $tbl_bus_sessions_id_arr[$i];

			if($tbl_bus_sessions_id_<>"")

			{

				$this->model_bus_absent_details->save_bus_absent_details($tbl_bus_id, $tbl_bus_sessions_id_, $tbl_student_id, $message_type, $message_from, $tbl_fixed_message_id, $attendance_date, $tbl_school_id);

		    	$this->model_attendance_bus->save_attendance_b($tbl_bus_id, $tbl_bus_sessions_id_ ,$tbl_student_id, 'L', $attendance_date,  $tbl_school_id);

			}

		}

		echo "{\"code\":\"200\"}";

	}

	

	/**

	* @desc    Get Bus Leave Details

	*

	* @param   none

	* @access  default

	*/

	function get_bus_leave_details() {

		$lan = $_REQUEST["lan"];

		$device = $_REQUEST["device"];

		$device_uid = $_REQUEST["device_uid"];

		$tbl_school_id = $_REQUEST["school_id"];

		$message_type = $_REQUEST["message_type"];							//Message is from P=Parent, A=School Admin

		$message_from = $_REQUEST["user_id"];

		

		$tbl_student_id = $_REQUEST["tbl_student_id"];

		$attendance_date = $_REQUEST["attendance_date"];

		

		$this->load->model('model_bus_absent_details');

		$data_rs = $this->model_bus_absent_details->get_bus_leave_details($tbl_student_id, $attendance_date);

		

		$this->load->model('model_teachers');

		$this->load->model('model_attendance');

		$this->load->model('model_admin');

		$this->load->model('model_parents');

		$this->load->model('model_parents');

		$this->load->model('model_fixed_message');

		$this->load->model('model_bus_absent_details');

		$this->load->model('model_bus_sessions');

		

		if (count($data_rs)<=0) {

			echo "--no--";

			return;

		}

		

		for($i=0; $i<count($data_rs); $i++) {

			

			//print_r($data_rs);

			if ($data_rs[$i]["id"] != "") {

				$message_type = $data_rs[$i]["message_type"];

				$message_from = $data_rs[$i]["message_from"];

				$tbl_student_id = $data_rs[$i]["tbl_student_id"];

				$tbl_fixed_message_id = $data_rs[$i]["tbl_fixed_message_id"];

				$tbl_bus_sessions_id = $data_rs[$i]["tbl_bus_sessions_id"];

				

				$data_bs = $this->model_bus_sessions->get_bus_session_details($tbl_bus_sessions_id);

				

				if ($lan == "ar") {

					$bus_session_title = $data_bs[0]["title_ar"];

				}else{

					$bus_session_title = $data_bs[0]["title"];

				}

				

				$arr_data[$i]["bus_session_title"] = $bus_session_title;

				

				$tbl_fixed_message_id = $data_rs[0]["tbl_fixed_message_id"];

				$data_fm = $this->model_fixed_message->get_fixed_message($tbl_fixed_message_id);

				if ($lan == "ar") {

					$message = $data_fm[0]["message_ar"];

				}else{

					$message = $data_fm[0]["message_en"];

				}

				

				if ($message_type == "A") {	

					

					// School Admin

					$data_admin = $this->model_admin->get_admin_user_obj($message_from);

					$first_name = $data_admin[0]["first_name"];

					$last_name = $data_admin[0]["last_name"];

					$phone = $data_admin[0]["phone"];

					

				} else if ($message_type == "P") {			// Parent

					$data_p = $this->model_parents->get_parent_obj($message_from);

					$phone = $data_p[0]["mobile"];



					if ($lan == "ar") {

						$first_name = $data_p[0]["first_name_ar"];

						$last_name = $data_p[0]["last_name_ar"];

					} else {

						$first_name = $data_p[0]["first_name"];

						$last_name = $data_p[0]["last_name"];

					}

				}

				

				$arr_data[$i]["message_type"] = $message_type;			// A = Admin User, P = Parent

				$arr_data[$i]["first_name"] = $first_name;

				$arr_data[$i]["last_name"] = $last_name;

				$arr_data[$i]["phone"] = $phone;

				$arr_data[$i]["message"] = $message;					

			}

		}

		$arr["absent_message"] = $arr_data;

		echo json_encode($arr);	

		return;	

	}

	

	

	

	/**

	* @desc    Submit Bus Attendance

	*

	* @param   none

	* @access  default

	*/

    function submit_attendance() {

		$lan = $_REQUEST["lan"];

		$device = $_REQUEST["device"];

		$device_uid = $_REQUEST["device_uid"];

		$tbl_school_id = $_REQUEST["school_id"];

		$tbl_bus_id = $_REQUEST["user_id"];

		$tbl_bus_sessions_id = $_REQUEST["tbl_bus_sessions_id"];

			

		//$send_notification = $_REQUEST["send_notification"];				//Y or N



	 	$att_str = $_REQUEST["att_str"];

		$attendance_date = $_REQUEST["attendance_date"];



		// Y : Means tick

		// N : Means cross

		date_default_timezone_set('Asia/Dubai');

		$attendance_date = date("Y-m-d");

		

		$this->load->model('model_bus_sessions');

		$data_bs = $this->model_bus_sessions->get_bus_session_details($tbl_bus_sessions_id);

		

		if ($lan == "ar") {

			$session_title = $data_bs[0]["title_ar"];

		} else {

			$session_title = $data_bs[0]["title"];

		}

		

		$this->load->model('model_attendance_bus');

		$this->model_attendance_bus->delete_attendance_b($attendance_date, $tbl_bus_id, $tbl_bus_sessions_id,  $tbl_school_id);

		

		$tbl_teacher_id_str = "";

		 

		//att_str = att_str + tbl_student_id+":"+option1+","+option2+","+option3+","+option4+";";

		$att_str_arr = explode(",",$att_str);

		for ($i=0; $i<(count($att_str_arr)-1); $i++) {

				

			$tbl_student_id_arr = explode(":",$att_str_arr[$i]);

			$tbl_student_id = $tbl_student_id_arr[0];

			$is_present = $tbl_student_id_arr[1];

			if ($is_present == "Y") {

				$message_ = "is present inside school bus. Attendance Session [".$session_titles."]";

			} else {

				$message_ = "is not present inside school bus. Attendance Session [".$session_title."]";

			}

			

			$this->load->model('model_attendance_bus');

			$this->model_attendance_bus->save_attendance_b($tbl_bus_id, $tbl_bus_sessions_id ,$tbl_student_id, $is_present, $attendance_date,  $tbl_school_id);

			

			$this->load->model('model_parents');

			$tbl_parent_id = $this->model_parents->get_parent_of_student($tbl_student_id);

			

			$this->load->model('model_student');

			$stu_obj = $this->model_student->get_student_obj($tbl_student_id);

			$first_name_stu = $stu_obj[0]['first_name'];

			$last_name_stu = $stu_obj[0]['last_name'];

			$message = 'Dear Parent, your Child '.$first_name_stu.' '.$last_name_stu.' '.$message_;

			//echo $message."<br>";

			$this->load->model('model_user_notify_token');

			$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_parent_id);

			

			for ($b=0; $b<count($data_tkns); $b++) {			



				if (trim(ENABLE_PUSH) == "Y") {	

					$message = urlencode($message);

					//$token = $tbl_parent_id."-".$data_tkns[$b]["token"];

					$token = $data_tkns[$b]["token"];
                    $device = $data_tkns[$b]["device"];
					//$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$tbl_parent_id."&msg=".$message."&data=";

					$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$token."&msg=".$message."&data=";



					$this->load->model('model_user_notify_token');

					$this->model_user_notify_token->send_notification($token , $message, $device);

					//$ch = curl_init();

					//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

					//curl_setopt($ch, CURLOPT_URL, $url);

					//curl_setopt($ch, CURLOPT_HEADER, 0);

					//curl_exec($ch);

					//curl_close($ch);

				}//if (trim(ENABLE_PUSH) == "Y")

			}

		}

		echo "{\"code\":\"200\"}";

	}	

	

	//get absent students once we get message

	 function get_student_leave_bus() { 

		$tbl_bus_id = $_REQUEST["user_id"];

		$role = $_REQUEST["role"];

		$lan = $_REQUEST["lan"];

		$device = $_REQUEST["device"];

		$device_uid = $_REQUEST["device_uid"];

		$tbl_school_id = $_REQUEST["school_id"];

		$tbl_bus_sessions_id = $_REQUEST["tbl_bus_sessions_id"];

		$attendance_date = $_REQUEST["attendance_date"];

		if (trim($attendance_date) == "") {

			date_default_timezone_set('Asia/Dubai');

			$attendance_date = date("Y-m-d");

		}

		$data["tbl_bus_id"] = $tbl_bus_id;

		$data["role"] = $role;

		$data["lan"] = $lan;

		$data["device"] = $device;

		$data["device_uid"] = $device_uid;

		$data["tbl_school_id"] = $tbl_school_id;

		$data["attendance_date"] = $attendance_date;

		

		// Get all the bus students

		$this->load->model('model_bus_student');

		$data_rs = $this->model_bus_student->get_bus_students($tbl_bus_id, $tbl_school_id);

		$this->load->model('model_attendance_bus');

		$this->load->model('model_bus_absent_details');

		$this->load->model('model_student');

		$arr_data = array();

		$j=0;

		for($i=0; $i<count($data_rs); $i++) {

			$tbl_bus_absent_details_id = "";

			$tbl_student_id = $data_rs[$i]["tbl_student_id"];

			// Get student's today's attendance for given session

			$status = $this->model_attendance_bus->get_attendance_bus($tbl_bus_id, $tbl_student_id, $tbl_bus_sessions_id, $tbl_school_id, $attendance_date);

			if ($status <> "NA") {	// Absent / Attendance Not Taken

				// Check if absent details are there in bus absent table

				if($status=="L" || $status=="N" ){

					$data_bt = $this->model_bus_absent_details->get_bus_absent_details_student($tbl_bus_id, $tbl_student_id, $tbl_bus_sessions_id, $tbl_school_id);

					if ($data_bt[0]["id"] != "") {

						$tbl_bus_absent_details_id = $data_bt[0]["tbl_bus_absent_details_id"];

						$arr_data[$j]["tbl_bus_absent_details_id"] = $tbl_bus_absent_details_id;

					}else

					{

						$arr_data[$j]["tbl_bus_absent_details_id"] = "";

					}

						$arr_data[$j]["status"] = "L";

						

						$arr_data[$j]["tbl_student_id"] = $tbl_student_id;

						$data_stu = $this->model_student->get_student_name($tbl_student_id);

						if ($lan == "ar") {

							$name = $data_stu[0]["first_name_ar"]." ".$data_stu[0]["last_name_ar"];

						} else {

							$name = $data_stu[0]["first_name"]." ".$data_stu[0]["last_name"];

						}

						$arr_data[$j]["name"] = $name;

						$file_name_updated = $this->model_student->get_student_picture($tbl_student_id);

						if ($file_name_updated != "") {

							$arr_data[$j]["picture"] = STUDENT_IMG_SHOW_PATH."/".$file_name_updated;	

						} else {

							$arr_data[$j]["picture"] = "";	

						}

					$j= $j+1;

				}

			  

			}

		}

		

		$arr["bus_attendance"] = $arr_data;

		echo json_encode($arr);

	}

	

}

?>



