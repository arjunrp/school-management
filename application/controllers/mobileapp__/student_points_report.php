<?php
/** @desc  Student attendance report Controller */

class Student_points_report extends CI_Controller {

    function index() {
		//Do nothing
	}


    function points_report() {
		$lan 						   = $_REQUEST["lan"];
		$device 	     				= $_REQUEST["device"];
		$device_uid 	                = $_REQUEST["device_uid"];
		$tbl_school_id                 = $_REQUEST["tbl_school_id"];
		$tbl_class_id                  = $_REQUEST["tbl_class_id"];
		$tbl_teacher_id                = $_REQUEST["tbl_teacher_id"];
		$date_from                     = isset($_REQUEST["date_from"])?$_REQUEST["date_from"]: date("Y-m-d");
		$date_to                       = isset($_REQUEST["date_to"])? $_REQUEST["date_to"]:'';
		$tbl_school_id                 = "9162b77c3c344ea";
		$tbl_class_id 				  = "b56cfe987a872b2";
		$date_from                     = "2016-05-01";
		$date_to                       = "2016-05-31";
		date_default_timezone_set('Asia/Dubai');
		
		$this->load->model('model_student');
		$this->load->model('model_student_points_report');
		$data_rs = $this->model_student->get_student_list($tbl_class_id, $tbl_school_id);
       // print_r($data_rs); exit;
		for($a=0;$a<count($data_rs);$a++)
		{
			$points_report[$a]['student_name']   = $data_rs[$a]['first_name']." ".$data_rs[$a]['last_name'];
			$tbl_student_id						  = $data_rs[$a]['tbl_student_id'];
			$points_report[$a]['tbl_student_id'] = $data_rs[$a]['tbl_student_id'];
			$points_info =  $this->model_student_points_report->getPointsInformation($tbl_student_id, $date_from, $date_to, $tbl_school_id, $tbl_teacher_id, $tbl_class_id);
			$points_report[$a]['points_info']    = $points_info;
		}
		echo json_encode($points_report);
	}

}

?>



