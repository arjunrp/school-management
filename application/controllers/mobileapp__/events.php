<?php

/**
 * @desc   	  	Events Controller
 *
 * @category   	Controller
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Events extends CI_Controller {


	/**
	* @desc    Events detail
	*
	* @param   string tbl_events_id
	* @access  default
	*/
    function event_details() {
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		
		$event_id = $_REQUEST["event_id"];
		
		$this->load->model('model_events');
		$data_rs =  $this->model_events->get_event_details($event_id);
		$data["data_rs"] = $data_rs;
		
		$page = "event_details";
		$data['page'] = $page;
		
		$this->load->view('view_template',$data);
	}
	
	
	 function event_list() {
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["school_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		$this->load->model('model_events');
		$data_rs =  $this->model_events->get_event_list($tbl_school_id);
		//print_r($data_rs);
		$data["data_rs"] = $data_rs;
		$page = "event_list";
		$data['page'] = $page;
		$this->load->view('view_template',$data);
	}
	
	
	
	
	
	
}
?>