<?php

/**
 * @desc   	  	Home Controller
 *
 * @category   	Controller
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Home extends CI_Controller {

	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
    function index() {
		return "";
	}

	/**
	* @desc    Function to get home page images
	*
	* @param   none
	* @access  default
	*/
    function get_home_page() {

		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;

		$this->load->model('model_home');
		$home_page_images_rs = $this->model_home->home_page_images();
		$data['home_page_images_rs'] = $home_page_images_rs;

		$home_page_message_rs = $this->model_home->home_page_message();
		$data['home_page_message_rs'] = $home_page_message_rs;
		
		$data['page'] = "view_home_page";
		$this->load->view('view_template',$data);
	}

	
	/**
	* @desc    Get application information
	*
	* @param   none
	* @access  default
	*/
    function app_info() {
		
		$user_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		
		$data["user_id"] = $user_id;
		$data["role"] = $role;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] = $school_id;
		
		$this->load->model('model_about_us');
		$data_rs = $this->model_about_us->get_about_us_page($school_id);
		
		if (count($data_rs) <=0) {
			$arr["code"] = "200";
			echo json_encode($arr);
			return;
		}
		
		$data["data_rs"] = $data_rs;
    	$data["page"] = "view_app_info";
		
		$this->load->view('view_template',$data);
	}
}
?>
