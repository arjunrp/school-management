<?php

/**
 * @desc   	  	Daily Report Controller
 *
 * @category   	Controller
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Daily_report extends CI_Controller {


	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
    function index() {
		//Do nothing
	}


	/**
	* @desc    Image Gallery
	*
	* @param   none
	* @access  default
	*/
    function get_daily_report() {
		
		$user_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		$tbl_class_id = $_REQUEST["tbl_class_id"];
		$start_date = $_REQUEST["start_date"];
		$end_date = $_REQUEST["end_date"];
		
		$data["user_id"] = $user_id;
		$data["role"] = $role;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] = $school_id;
		$data["tbl_class_id"] = $tbl_class_id;

		$output_format = "Y-m-d";
		$step = '+1 day';
		
		$first = $start_date;			//"2015-05-01";
		$last = $end_date;				//"2015-05-31";

		$current = strtotime($first);
		$last = strtotime($last);
	 	
		$this->load->model("model_daily_report");
		$index = 0;
		
		while( $current <= $last ) {
			$report_date =  date($output_format, $current);
			$is_daily_report_exists = $this->model_daily_report->is_daily_report_exists($report_date, $tbl_class_id);
			
			$current = strtotime($step, $current);
			$arr[$index]["report_date"] = $report_date;
			$arr[$index]["is_report_exists"] = $is_daily_report_exists;
			$index = $index + 1;
			//echo "<br>".$report_date." ".$is_daily_report_exists."<br>";
		}
		
		$arr_dr["daily_report"] = $arr;
		echo json_encode($arr_dr);
	}
}
?>

