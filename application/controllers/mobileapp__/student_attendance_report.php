<?php
/** @desc  Student attendance report Controller */

class Student_attendance_report extends CI_Controller {

    function index() {
		//Do nothing
	}


    function attendance_report() {
		$lan 						   = $_REQUEST["lan"];
		$device 	     				= $_REQUEST["device"];
		$device_uid 	                = $_REQUEST["device_uid"];
		$tbl_school_id                 = $_REQUEST["tbl_school_id"];
		$tbl_class_id                  = $_REQUEST["tbl_class_id"];
		$tbl_teacher_id                = $_REQUEST["tbl_teacher_id"];
		$tbl_class_sessions_id         = $_REQUEST["tbl_class_sessions_id"];
		$attendance_from               = isset($_REQUEST["date_from"])?$_REQUEST["date_from"]: date("Y-m-d");
		$attendance_to                 = isset($_REQUEST["date_to"])? $_REQUEST["date_to"]:'';
		/*$tbl_school_id                 = "3fcc0e4fa4d7314";
		$tbl_class_id 				  = "19b98b0cf3022cb";
		$attendance_from               = "2016-05-01";
		$attendance_to                 = "2016-05-31";*/
		date_default_timezone_set('Asia/Dubai');
		
		$this->load->model('model_student');
		$this->load->model('model_student_attendance_report');
		$data_rs = $this->model_student->get_student_list($tbl_class_id, $tbl_school_id);
       // print_r($data_rs); exit;
		for($a=0;$a<count($data_rs);$a++)
		{
			$attendance_report[$a]['student_name']   = $data_rs[$a]['first_name']." ".$data_rs[$a]['last_name'];
			$tbl_student_id						  = $data_rs[$a]['tbl_student_id'];
			$attendance_report[$a]['tbl_student_id'] = $data_rs[$a]['tbl_student_id'];
			$attendance_info =  $this->model_student_attendance_report->getAttendanceInformation($tbl_student_id,$attendance_from,$attendance_to,$tbl_school_id,$tbl_teacher_id,$tbl_class_id);
			$attendance_report[$a]['attendance_info'] = $attendance_info;
		}
		echo json_encode($attendance_report);
	}

}

?>



