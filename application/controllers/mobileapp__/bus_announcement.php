<?php

/**
 * @desc   	  	Bus Announcement Controller
 *
 * @category   	Controller
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Bus_announcement extends CI_Controller {


	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
    function index() {
		//Do nothing
	}


	/**
	* @desc    Get details of student who is absent in bus and for which absent details were already submitted
	*
	* @param   none
	* @access  default
	*/
    function save_bus_announcement() {
		$tbl_bus_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["school_id"];
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		
		$this->load->model('model_bus_announcement');
		
		$tbl_student_id_arr = explode(":",$tbl_student_id);
		for($i=0; $i<count($tbl_student_id_arr); $i++) {
			$tbl_student_id_ = $tbl_student_id_arr[$i];
			if (trim($tbl_student_id_) == "") {continue;}
			$this->model_bus_announcement->save_bus_announcement($tbl_bus_id, $tbl_student_id_, $tbl_school_id);
		}
		
		echo "{\"code\":\"200\"}";
	}
	
	
	/**
	* @desc    Function to load main container inside which information will be updated periodically
	*
	* @param   none
	* @access  default
	*/
    function bus_announcement_page() {
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$lan = $_REQUEST["lan"];
		$data["tbl_school_id"] = $tbl_school_id;
		$data["lan"] = $lan;
		$data["page"] = "view_bus_announcement_page";
		$this->load->view('view_template',$data);	
	}
	
	
	/**
	* @desc    Function to get anncounements information.
	*
	* @param   none
	* @access  default
	*/
    function get_bus_announcement_ajax() {

		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$lan = $_REQUEST["lan"];
		
		$data["tbl_school_id"] = $tbl_school_id;
		$data["lan"] = $lan;
		
		$this->load->model('model_bus_announcement');
		$data_rs = $this->model_bus_announcement->get_bus_announcement($tbl_school_id);
		
		if ($data_rs[0]["id"] == "") {
			echo "--no--";
			return;
		}
		
		$this->load->model('model_student');
		$this->load->model('model_classes');
		$this->load->model('model_section');
		$this->load->model('model_bus');
		
		$tbl_bus_id = $data_rs[0]["tbl_bus_id"];
		$tbl_student_id = $data_rs[0]["tbl_student_id"];
		
		$stu_obj = $this->model_student->get_student_obj($tbl_student_id);
		$first_name_stu = $stu_obj[0]['first_name'];
		$last_name_stu = $stu_obj[0]['last_name'];
		$tbl_class_id = $stu_obj[0]['tbl_class_id'];
		$file_name_updated = $stu_obj[0]['file_name_updated'];
		
		$class_name = $this->model_classes->get_class_name($tbl_class_id);
		$tbl_section_id = $this->model_classes->get_class_section($tbl_class_id);
		
		$data_se = $this->model_section->get_section_obj($tbl_section_id);
		$section_name = $data_se[0]["section_name"];
		
		$data_bd = $this->model_bus->get_bus_details($tbl_bus_id);
		$bus_number = $data_bd[0]["bus_number"];
		$bus_code = $data_bd[0]["bus_code"];
		
		$arr_data["first_name_stu"] = $first_name_stu;
		$arr_data["last_name_stu"] = $last_name_stu;
		$arr_data["class_name"] = $class_name;
		$arr_data["section_name"] = $section_name;
		$arr_data["picture"] = "";
		if ($file_name_updated != "") {
			$arr_data["picture"] = STUDENT_IMG_SHOW_PATH."/".$file_name_updated;
		}
		$arr_data["bus_number"] = $bus_number;
		$arr_data["bus_code"] = $bus_code;
		$arr_data["bus_message_display"] = "Please go to your bus ".$bus_number." [".$bus_code."]";
		$arr_data["bus_message_speak"] = $first_name_stu." ".$last_name_stu." of ".$class_name." ".$section_name." Please go to your bus having number ".$bus_number;
		
		echo json_encode($arr_data);
	}
}
?>

