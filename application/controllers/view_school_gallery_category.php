<?php
//Init Parameters
$gallery_category_id_enc = md5(uniqid(rand()));

if (trim($mid) == "") {
	$mid = "1";	
}
?>
 

<script language="javascript">
    var item_id = '<?=$gallery_category_id_enc?>';

    function show_gallery()
	{
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/all_galleries/";
		window.location.href = url;
	}
	
	$(document).ready(function(){
		$('#select_all').on('click',function(){
			if(this.checked){
				$('.checkbox').each(function(){
					this.checked = true;
				});
			}else{
				 $('.checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		
		$('.checkbox').on('click',function(){
			if($('.checkbox:checked').length == $('.checkbox').length){
				$('#select_all').prop('checked',true);
			}else{
				$('#select_all').prop('checked',false);
			}
		});
	});
	
	function show_create_form() {
		$('#mid1').hide(function(){
			$('#mid1_list').hide(500);
			$('#mid2').show(500);
		});
	}
	
	function show_listing() {
		$('#mid2').hide(function(){
			$('#mid1').show(500);
		    $('#mid1_list').show(500);
		});
	}

		
	var refresh_page = "N";
	var confirm_delete = "Y";
	$(document).ready(function(e) {
		$('#alert_box').on('hidden.bs.modal', function () {
			if (refresh_page == "Y") {
				//window.location.reload();
				window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/gallery_categories_list";
			}
		})
	});
	
function confirm_delete_popup() {
		var len = $("input[id='gallery_category_id_enc']:checked").length;
		
		if (len <= 0) {
			refresh_page = "N";
			my_alert("Please select one or more gallery category(s)", 'green');
		return;	
		}
		
		$('#button_confirm').show();	

		refresh_page = "N";
		my_alert("Are you sure you want to delete? This operation cannot be undone.", 'red');
	}
	
	function ajax_delete() {
		$("#pre-loader").show();
		$('#button_confirm').hide();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/deleteGalleryCategory",
			data: {
				gallery_category_id_enc: $("input[id='gallery_category_id_enc']:checked").serialize(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "Y";
				my_alert("Gallery category(s) deleted successfully.", 'green')

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}	
	function ajax_activate(gallery_category_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/activateGalleryCategory",
			data: {
				gallery_category_id_enc: gallery_category_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Gallery category activated successfully.", 'green')

				$('#act_deact_'+gallery_category_id_enc).html('<span style="cursor:pointer" onClick="ajax_deactivate(\''+gallery_category_id_enc+'\')" onMouseOver="deactivate_me(this)" onMouseOut="reset_activate(this)" class="label label-success">Active</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_deactivate(gallery_category_id_enc) {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/deactivateGalleryCategory",
			data: {
				gallery_category_id_enc: gallery_category_id_enc,
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Gallery category de-activated successfully.", 'green')
				
				$('#act_deact_'+gallery_category_id_enc).html('<span style="cursor:pointer" onClick="ajax_activate(\''+gallery_category_id_enc+'\')" onMouseOver="activate_me(this)" onMouseOut="reset_deactivate(this)" class="label label-danger">Inactive</span>');

				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist() {
		$("#pre-loader").show();
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/is_exist_gallery_category",
			data: {
				gallery_category_id_enc: "<?=$gallery_category_id_enc?>",
				category_name_en: $('#category_name_en').val(),
				category_name_ar: "",
				is_ajax: true
			},
			success: function(data) {
				
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
					refresh_page = "N";
					my_alert("Gallery category already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_create();
				}
			
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function is_exist_edit() {
		$("#pre-loader").show();

		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/is_exist_gallery_category",
			data: {
				gallery_category_id_enc:$('#gallery_category_id_enc').val(),
				category_name_en: $('#category_name_en').val(),
				category_name_ar: "",
				is_ajax: true
			},
			success: function(data) {
				var temp = new String(data);
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
					refresh_page = "N";
					my_alert("Gallery category already exists.", 'red');
					$("#pre-loader").hide();
				}else{
					ajax_save_changes();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
	
	function ajax_create() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/add_gallery_category",
			data: {
				gallery_category_id_enc: "<?=$gallery_category_id_enc?>",
				category_name_en: $('#category_name_en').val(),
				category_name_ar: $('#category_name_ar').val(),
				is_ajax: true
			},
			success: function(data) {
				var temp = new String();
				temp = data;
				temp = temp.trim();
				if (temp=='Y') {
				refresh_page = "Y";
				    my_alert("Gallery category is created successfully.", 'green');
				    $("#pre-loader").hide();
				}else{
					refresh_page = "N";
					my_alert("Gallery category added failed, Please try again.", 'red');
					$("#pre-loader").hide();
				}
				
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}

	function ajax_save_changes() {
		$.ajax({
			type: "POST",
			url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/update_school_gallery_category",
			data: {
				gallery_category_id_enc:$('#gallery_category_id_enc').val(),
				category_name_en: $('#category_name_en').val(),
				category_name_ar: $('#category_name_ar').val(),
				is_ajax: true
			},
			success: function(data) {
				refresh_page = "N";
				my_alert("Changes saved successfully.", 'green');
				
				$("#pre-loader").hide();
			},
			error: function() {
				$("#pre-loader").hide();
			}, 
			complete: function() {
				$("#pre-loader").hide();
			}
		});
	}
</script>
<script language="javascript">
	function ajax_validate() {
		if (validate_category() == false || validate_category_ar() == false ) {
			return false;
		} else {
			is_exist();
		}
	} 

	function ajax_validate_edit() {
		if ( validate_category() == false || validate_category_ar() == false ) {
			return false;
		} else {
			is_exist_edit();
		}
	} 


	function validate_category() {
		var regExp = / /g;
		var str = $('#category_name_en').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Category Name [En] is blank. Please enter category name [En].", 'red');
		return false;
		}
	}

	function validate_category_ar() {
		var regExp = / /g;
		var str = $('#category_name_ar').val();
		str = str.replace(regExp,'');
		if (str.length <= 0) {
			my_alert("Category Name [Ar] is blank. Please enter category name [Ar].", 'red');
		return false;
		}
	}

</script>
<?php if(LAN_SEL=="ar"){ 
      $positionBreadCrumb = 'float:right;';
}else{
	$positionBreadCrumb = 'float:left;';
	
}?>

<div class="content-wrapper">
  <section class="content-header"> 
    <!--HEADING-->
    <h1> Gallery Category <small> Management</small> </h1>
    <!--/HEADING--> 

    <!--BREADCRUMB-->
    <ol class="breadcrumb" style=" <?=$positionBreadCrumb?> position:relative; top:0px">
      <li><a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/home" target="_parent"><i class="fa fa-home"></i>Home</a></li>
      <li>Gallery Category</li>
    </ol>
    <!--/BREADCRUMB--> 
 
     <div style=" float:right; padding-right:10px;"> <button onclick="show_gallery()" title="Records" type="button" class="btn btn-primary">Gallery</button></div>
    <div style="clear:both"></div>
  </section>
  
  <section class="content"> 
    <!--WORKING AREA-->	
    <?php
    	if (trim($mid) == "3" || trim($mid) == 3) {

			$tbl_gallery_category_id    = $gallery_categories[0]['tbl_gallery_category_id'];
			$category_name_en           = $gallery_categories[0]['category_name_en'];
			$category_name_ar           = $gallery_categories[0]['category_name_ar'];
			$added_date                 = $gallery_categories[0]['added_date'];
			$is_active                  = $gallery_categories[0]['is_active'];
			$file_name_updated          = $gallery_categories[0]['file_name_updated'];
			$tbl_uploads_id             = $gallery_categories[0]['tbl_uploads_id'];
			
			if($file_name_updated<>"")
			{
				$img_url = IMG_GALLERY_PATH."/".$file_name_updated;
				
			}
	?>
        <!--Edit-->
        
              <div id="mid2" class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Records Category</h3>
                  <div class="box-tools">
                    <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/gallery_categories_list"><button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back"></button></a>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_edit" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                  
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="category_name_en">Category Name [En]</label>
        
                          <div class="col-sm-10">
                            <input type="text" placeholder="Category Name [En]" id="category_name_en" name="category_name_en" class="form-control" value="<?=$category_name_en?>">
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-2 control-label" for="category_name_ar">Category Name [Ar]</label>
        
                          <div class="col-sm-10">
                            <input type="text" placeholder="Category Name [Ar]" id="category_name_ar" name="category_name_ar" class="form-control" value="<?=$category_name_ar?>" dir="rtl">
                          </div>
                        </div>
                        
                      </div>
                    
                        <div class="form-group" >
                      <label class="col-sm-2 control-label" for="image">Image</label>
                      
                      <div class="col-sm-10" >
                      <div id="uploaded_items_file" >
                                <div id="div_listing_container" class="listing_container" style="display:block">	            
										<?php
                                            if (trim($img_url) != "") {
                                        ?>
                                                    <div id='<?=$tbl_uploads_id?>' class='box-header with-border'>
                                                      <div class='box-title'><img src='<?=$img_url?>' style='width:100%; max-width:600px' /></div>
                                                      <div class='box-tools'> <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick="confirm_delete_file_popup('<?=$tbl_gallery_category_id?>')">
                                                        </button>
                                                      </div>
                                                    </div>
											<style>
												.ajax-upload-dragdrop {
													display:none;	
												}
                                            </style>        
                                        <?php		
                                            }
                                        ?>
                                </div>        
                          </div>
                         <div id="divFile">
                        <!--File Upload START-->
                            <style>
                            #advancedFileUpload {
                                padding-bottom:0px;
                            }
							
                            </style>
                                 
                            <div id="advancedFileUpload">Upload File</div>
                        </div>
                            
                            
                        <!--File Upload END-->
                      </div>
                    </div>
                  
                  
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate_edit()">Save Changes</button>
                    <input type="hidden" name="gallery_category_id_enc" id="gallery_category_id_enc" value="<?=$tbl_gallery_category_id?>" />
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
              <script>
              var item_id = $("#gallery_category_id_enc").val();
			  </script>
		        
        <!--/Edit-->
	<?php							
		} else {
			
		$sort_url = HOST_URL."/".LAN_SEL."/admin/gallery/gallery_categories_list";
		if (trim($q) != "") {
			$sort_url .= "/q/".rawurlencode($q);
		}
	?>  
    
  
 <link href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" rel="stylesheet">
 <script src="http://code.jquery.com/jquery-1.11.1.js"></script>
 <script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
  <script>
  $( function() {
		    $( "tbody1" ).sortable({
			axis: 'y',
			update: function (event, tr) {
				
				/* var order = $("#tabledivbody").sortable("serialize");
				
				alert(order);
				
				var data = $(this).sortable('serialize');
				// POST to server using $.post or $.ajax
				$.ajax({
					data: data,
					type: 'POST',
					url: '/your/url/here'
				});*/
				
				
				
			 var order = $("#tabledivbody").sortable("serialize");
   
			$.ajax({
			type: "POST", dataType: "json", url: "<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/updateSortOrder/",
			data: order,
			success: function(response) {
				if (response == "success") {
					window.location.href = window.location.href;
				} else {
					alert('Some error occurred');
				}
			}
			});	
				
				
				
				
				
			}
	  } );
  
  } );
  
  
	
  </script> 
 
    
                    <div id="mid1">
                    
                                   <!-- <div class="box-header">
                                      <div style="width:20%; float:left;">
                                      <h3 class="box-title">SEARCH</h3>
                                      </div>
                                      <div style="width:60%; float:left;"> 
                                         <div class="col-sm-5">
                                         <?php //echo $category_parent_list ?>
                                          </div>
                                         
                                      </div>
                                    </div>  -->
                    </div>    
                      
    
            <!--Listing-->
                    <div id="mid1_list" class="box">
                        <div class="box-header">
                          <h3 class="box-title">Gallery Categories</h3>
                          <div class="box-tools">
                            <?php if (count($rs_all_categories)>0) { echo $paging_string;}?>	
                            <button class="btn bg-orange fa fa-plus" type="button" title="Add" onclick="show_create_form()"></button>
                            <button class="btn bg-maroon fa fa-trash-o" type="button" title="Delete" onclick="confirm_delete_popup()"></button>
                          </div>
                        </div>
                        
                        <div class="box-body">
                     <!--   <div style="color:#030; font-weight:bold;">You can sort categories by using drag and drop of rows </div>-->
                          <table width="100%" class="table table-bordered table-striped" id="example1 sort-table">
                            <thead>
                            <tr>
                              <th width="5%" align="center" valign="middle"><input id="select_all" type="checkbox" value="" /></th>
                              <!--<th width="10%" align="center" valign="middle">Sl No.</th>-->
                              <th width="20%" align="center" valign="middle">
	                              <a href="<?=$sort_url?>/sort_name/A/sort_by/<?=$sort_by?>/sort_by_click/Y">Category Name [En] <?php if (trim($sort_name_param) != "" && trim($sort_name_param) == "A" && $sort_by == "ASC") { ?><div class="fa fa-sort-up"></div><?php } else {?><div class="fa fa-sort-desc"></div><?php } ?></a>
                              </th>
                              <th width="20%" align="center" valign="middle">Category Name [Ar]</th>
                              <th width="15%" align="center" valign="middle">Image</th>
                              <th width="10%" align="center" valign="middle">Date</th>
                              <th width="10%" align="center" valign="middle">Status</th>
                              <th width="10%" align="center" valign="middle">Action</th>
                            </tr>
                            </thead>
                            <tbody id="tabledivbody" >
                            <?php
                                for ($i=0; $i<count($rs_all_categories); $i++) { 
                                    $id                      = $rs_all_categories[$i]['id'];
                                    $tbl_gallery_category_id = $rs_all_categories[$i]['tbl_gallery_category_id'];
                                    $category_name_en        = $rs_all_categories[$i]['category_name_en'];
                                    $category_name_ar        = $rs_all_categories[$i]['category_name_ar'];
									$file_name_updated       = $rs_all_categories[$i]['file_name_updated'];
                                    $added_date              = $rs_all_categories[$i]['added_date'];
                                    $is_active               = $rs_all_categories[$i]['is_active'];
									
									$img_url = "";
									if(trim($file_name_updated)<>"") {
										$img_url = "<img src='".IMG_GALLERY_PATH."/".$file_name_updated."' style='width:100%; max-width:200px' />";
									}
                                    
                                    $school_type = ucfirst($school_type);
                                    $added_date = date('m-d-Y',strtotime($added_date));
                            ?>
                            <tr  class="sectionsid" id="sectionsid_<?=$tbl_gallery_category_id?>" >
                              <td align="left" valign="middle">
                              <span style="float:left;">
                              <input id="gallery_category_id_enc" name="gallery_category_id_enc" class="checkbox" type="checkbox" value="<?=$tbl_gallery_category_id?>" />
                              </span>
                              
                             <?php /*?> <span style="float:left;">&nbsp;
                              <?php if($i<>0){ ?> <i class="fa fa-arrow-up"  style="color:#3c8dbc; cursor:pointer;"  aria-hidden="true" title="Sorting - Drag & Drop To Up"></i> &nbsp; <?php } ?>
                               <?php if($i<> count($rs_all_categories)-1){ ?> <i class="fa fa-arrow-down" style="color:#3c8dbc;cursor:pointer;" aria-hidden="true" title="Sorting - Drag & Drop To Down"></i> <?php } ?>
                              </span><?php */?>
                              </td>
                             <!-- <td align="left" valign="middle"><?=$offset+$i+1?></td>-->
                              <td align="left" valign="middle"><?=$category_name_en?></td>
                              <td align="left" valign="middle"><?=$category_name_ar?></td>
                              <td align="left" valign="middle"><?=$img_url?></td>
                              <td align="left" valign="middle"><?=$added_date?></td>
                              <td align="left" valign="middle">
                                <div id="act_deact_<?=$tbl_gallery_category_id?>">
                                <?php if (trim($is_active) == "Y") { ?>
                                    <span style="cursor:pointer" onclick="ajax_deactivate('<?=$tbl_gallery_category_id?>')" onmouseover="deactivate_me(this)" onmouseout="reset_activate(this)" class="label label-success">Active</span>
                                <?php } else { ?>
                                    <span style="cursor:pointer" onclick="ajax_activate('<?=$tbl_gallery_category_id?>')" onmouseover="activate_me(this)" onmouseout="reset_deactivate(this)" class="label label-danger">Inactive</span>
                                <?php } ?>
                                </div>
                              </td>
                              <td align="left" valign="middle">
                                <a href="<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/edit_gallery_category/tbl_gallery_category_id/<?=$tbl_gallery_category_id?>"><button class="btn bg-purple fa fa-pencil" type="button" title="Edit"></button></a>
                              </td>
                            </tr>
                            <?php } ?>
                            <tr>
                              <td colspan="8" align="right" valign="middle">
                              <?php echo $this->pagination->create_links(); ?>
                              </td>
                            </tr>
							<?php 
                                if (count($rs_all_categories)<=0) {
                            ?>
                            <tr>
                              <td colspan="8" align="center" valign="middle">
                              <div class="alert alert-warning alert-dismissible" style="width:50%">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-info"></i> Information!</h4>
                                There are no categories available. Click on the + button to create one.
                              </div>                                
                              </td>
                            </tr>
							<?php   
                                }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                          </table>
                        </div>
                    </div>        
            <!--/Listing-->
    
            <!--Add or Create-->
              <div id="mid2" class="box box-primary" style="display:none">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Gallery Category</h3>
                  <div class="box-tools">
                    <button class="btn bg-purple fa fa-arrow-circle-o-left" type="button" title="Back" onclick="show_listing()"></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form name="frm_listing" id="frm_listing" class="form-horizontal" method="post">
                  <div class="box-body">
                  
                   
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="category_name_en">Category Name [En]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Category Name [En]" id="category_name_en" name="category_name_en" class="form-control">
                      </div>
                    </div>
                    
                    <div class="form-group">
                      <label class="col-sm-2 control-label" for="category_name_ar">Category Name [Ar]</label>
    
                      <div class="col-sm-10">
                        <input type="text" placeholder="Category Name [Ar]" id="category_name_ar" name="category_name_ar" class="form-control" dir="rtl">
                      </div>
                    </div>
                    
                    
                        <div class="form-group" >
                      <label class="col-sm-2 control-label" for="image">Image</label>
                      
                      <div class="col-sm-10" >
                      <div id="uploaded_items_file" >
                                <div id="div_listing_container" class="listing_container" style="display:block">	            
										<?php
                                            if (trim($file_url) != "") {
                                        ?>
                                                    <div id='<?=$tbl_uploads_id?>' class='box-header with-border'>
                                                      <div class='box-title'><img src='<?=$file_url?>' style='width:100%; max-width:600px' /></div>
                                                      <div class='box-tools'> <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick="confirm_delete_file_popup('<?=$tbl_uploads_id?>')">
                                                        </button>
                                                      </div>
                                                    </div>
											<style>
												.ajax-upload-dragdrop {
													display:none;	
												}
                                            </style>        
                                        <?php		
                                            }
                                        ?>
                                </div>        
                          </div>
                         <div id="divFile">
                        <!--File Upload START-->
                            <style>
                            #advancedFileUpload {
                                padding-bottom:0px;
                            }
							
                            </style>
                                 
                            <div id="advancedFileUpload">Upload File</div>
                        </div>
                            
                            
                        <!--File Upload END-->
                      </div>
                    </div>
                    
                    
                    
                    
                  </div>
                  <!-- /.box-body -->
                  <div class="box-footer">
                    <button class="btn btn-primary" type="button" onclick="ajax_validate()">Submit</button>
                    <!--<button class="btn btn-info pull-right" type="submit">Sign in</button>-->
                  </div>
                  <!-- /.box-footer -->
                </form>
              </div>
            <!--/Add or Create-->
                
        <!--/Admin Category Management-->

	<?php			
		}//if (trim($mid) == "3" || trim($mid) == 3)	
	?>

        
    <!--/WORKING AREA--> 
  </section>
</div>
<link href="http://hayageek.github.io/jQuery-Upload-File/uploadfile.min.css" rel="stylesheet">

<style type="text/css">
	.btncls {
		background-color:red;
		color:red;
		clear:both;
		float:left;
	}
	.upload_del {
		width:15px;
		height:15px;
		background-image:url(<?=IMG_PATH?>/delete.jpg);
		background-repeat:no-repeat;
		background-position:center;
		padding:8px 2px 2px 4px;
		float:left;
		cursor:pointer;
	}
	.upload_content {
		float:left;
		padding-top:2px;
	}
	.row_item {
		float:left;
		padding:4px 0px 0px 2px;
		width:100%;
	}
</style>
<script src="<?=HOST_URL?>/assets/admin/dist/js/jquery.uploadfile.min.js"></script>
<script language="javascript">
//Primary Key for a Form. 
function set_item_id(obj) {
	item_id = obj.value;
	get_files();	
}
$(document).ready(function() {
	/*
	var uploadObj = $("#advancedNewUpload").uploadFile({
		url:"<?=HOST_URL?>/file_mgmt/upload_the_file",
		multiple:true,
		autoSubmit:true,
		maxFileSize:130000,
		allowedTypes: "jpg,jpeg,png,gif",
		fileName:"myfile",
		formData: {"module_name":"school_gallery_thumb"},
		dynamicFormData: function() {
			var data = { item_id:item_id}
			return data;
		},
		showStatusAfterSuccess:false,
		dragDropStr: "<span class='d_d_text'>Drag and Drop the File to Upload.</span>",
		abortStr:"Abourt",
		cancelStr:"Cancel",
		doneStr:"Done",
		multiDragErrorStr: "Multi Drag Error.",
		extErrorStr:"Extention Error:",
		sizeErrorStr:"Max Size Error:",
		uploadErrorStr:"Upload Error",
		onSelect:function(files) {
 		},
		onSubmit:function(files) {
 		},
		onSuccess:function(files, data, xhr) {
			if (data == "error") {
				alert("Error uploading file. Please try again.");
				return;
			}
			var obj = JSON.parse(data);
			var tbl_uploads_id = obj.tbl_uploads_id;
			var file_name_updated = obj.file_name_updated;
			$("#divThumb").hide();
			//alert("tbl_uploads_id: "+tbl_uploads_id)
			//alert("file_name_updated: "+file_name_updated)
			add_new_uploaded_item(tbl_uploads_id, file_name_updated);
		},
		afterUploadAll:function() {
 		},
		onError: function(files, status, errMsg) {
 		}
	});
	*/
	
	var uploadFileObj = $("#advancedFileUpload").uploadFile({
		url:"<?=HOST_URL?>/file_mgmt/upload_the_file",
		multiple:true,
		autoSubmit:true,
		maxFileSize:130000,
		allowedTypes: "jpg,jpeg,png,gif",
		fileName:"myfile",
		formData: {"module_name":"school_gallery_category"},
		dynamicFormData: function() {
			var data = { item_id:item_id}
			return data;
		},
		showStatusAfterSuccess:false,
		dragDropStr: "<span class='d_d_text'>Drag and Drop the File to Upload.</span>",
		abortStr:"Abourt",
		cancelStr:"Cancel",
		doneStr:"Done",
		multiDragErrorStr: "Multi Drag Error.",
		extErrorStr:"Extention Error:",
		sizeErrorStr:"Max Size Error:",
		uploadErrorStr:"Upload Error",
		onSelect:function(files) {
 		},
		onSubmit:function(files) {
 		},
		onSuccess:function(files, data, xhr) {
			if (data == "error") {
				alert("Error uploading file. Please try again.");
				return;
			}
			var obj = JSON.parse(data);
			var tbl_uploads_id = obj.tbl_uploads_id;
			var file_name_updated = obj.file_name_updated;
			$("#divFile").hide();
			
			//alert("tbl_uploads_id: "+tbl_uploads_id)
			//alert("file_name_updated: "+file_name_updated)
			add_new_uploaded_file(tbl_uploads_id, file_name_updated);
		},
		afterUploadAll:function() {
 		},
		onError: function(files, status, errMsg) {
 		}
	});
	
	

	$("#startUpload").click(function() {
		uploadObj.startUpload();
	});
	
	$("#startUpload").click(function() {
		uploadFileObj.startUpload();
	});
	
	try { 
		$('input[type=file]').click();
	} catch(e) {
		alert(e)
	}
});

//Function called when file is uploaded
function add_new_uploaded_item(tbl_uploads_id, file_name_updated) {
	var str = "<div id='"+tbl_uploads_id+"' class='box-header with-border'> <div class='box-title'><img src='<?=IMG_GALLERY_PATH?>/"+file_name_updated+"' style='width:100%; max-width:600px' /></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_img_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
		
	$("#div_listing_container_new").show();
	$("#div_listing_container_new").append(str);
	
	//$(".ajax-upload-dragdrop-new").hide();//Hide the upload button
return;
}

function add_new_uploaded_file(tbl_uploads_id, file_name_updated) {
	var result      = $(file_name_updated).text().split('.');
    var type_file   =  result[1];
	var str = "<div id='"+tbl_uploads_id+"' class='box-header with-border'> <div class='box-title'><img src='<?=IMG_GALLERY_PATH?>/"+file_name_updated+"' style='width:100%; max-width:600px' /></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_file_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
	/*if(type_file=='png' || type_file=='jpg')
	{
		var str = "<div id='"+tbl_uploads_id+"' class='box-header with-border'> <div class='box-title'><img src='<?=IMG_GALLERY_PATH?>/"+file_name_updated+"' /></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_file_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
	}else{
		var str = "<div id='"+tbl_uploads_id+"' class='box-header with-border'> <div class='box-title'><a target='_blank' href='<?=IMG_GALLERY_PATH?>/"+file_name_updated+"' >"+file_name_updated+"</a></div> <div class='box-tools'>   <button class='btn bg-maroon fa fa-trash-o' type='button' title='Delete' onclick=\"confirm_delete_file_popup('"+tbl_uploads_id+"')\" ></button> </div></div>";
	}*/
		
	$("#div_listing_container").show();
	$("#div_listing_container").append(str);
	//$(".ajax-upload-dragdrop").hide();//Hide the upload button
return;
}


function confirm_delete_img_popup(tbl_uploads_id) {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		$('#'+tbl_uploads_id).hide();	
		//$(".ajax-upload-dragdrop-new").show();
		$("#divThumb").show();
		var url_str = "<?=HOST_URL?>/file_mgmt/delete_file";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					tbl_uploads_id: tbl_uploads_id
				},
			success: function(data) {
				$("#pre-loader").hide();
				
			}
		});
	} else {
		$("#pre-loader").hide();		
	}
}


function confirm_delete_img() {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		$("#divThumb .ajax-upload-dragdrop").css({"display":"block"});
		$("#divThumb").show();
		$("#div_listing_container_new").html('');
		$("#pre-loader").hide();	
	} 
}

function confirm_delete_file(tbl_uploads_id) {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		$('#'+tbl_uploads_id).hide();	
		//$(".ajax-upload-dragdrop").show();
		$("#divFile .ajax-upload-dragdrop").css({"display":"block"});
		$("#divFile").show();
		
		var url_str = "<?=HOST_URL?>/file_mgmt/delete_file";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					tbl_uploads_id: tbl_uploads_id
				},
			success: function(data) {
				$("#pre-loader").hide();
			}
		});	
	} else {
		$("#pre-loader").hide();		
	}
}






function confirm_delete_file_popup(gallery_category_id_enc) {
	$("#pre-loader").show();
	var a = confirm("Are you sure you want to delete?")
	if (a) {
		/*
		$('#'+tbl_uploads_id).hide();	
		//$(".ajax-upload-dragdrop").show();
		$("#divFile").show();
		*/
		var url_str = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/gallery/delete_gallery_category_image";

		$.ajax({
			type: "POST",
			url: url_str,
			data: {
					gallery_category_id_enc: gallery_category_id_enc
				},
			success: function(data) {
				$("#pre-loader").hide();
			}
		});
		
		try { 
		$("#divFile .ajax-upload-dragdrop").css({"display":"block"});
		$("#divFile").show();
		$("#div_listing_container").html('');
		$("#pre-loader").hide();		
		} catch(e) {alert("e: "+e)}
		
	} else {
		$("#pre-loader").hide();		
	}
}


function get_files() {
	var url_str = "<?=HOST_URL?>/misc/get_files.php";
	
	$.ajax({
		type: "POST",
		url: url_str,
		data: {
				module_name: "student",
				show_del: "Y",
				item_id: item_id//global variable
			},
		success: function(data) {
			$('#div_listing_container_new').show();
			$('#div_listing_container_new').html(data)
			
		}
	});	
}


</script>

<!--File Upload END-->

<script language="javascript" >
function search_data() {
		var tbl_category_id = $("#tbl_category_id").val();
		var url = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/category/all_categories/";
		if(tbl_category_id !='')
			url += "tbl_category_id/"+tbl_category_id+"/";
		
			url += "offset/0/";
		window.location.href = url;
		<?php /*?>window.location.href = "<?=HOST_URL?>/<?=LAN_SEL?>/admin/enquiry/all_enquiries/is_not_replied/"+is_not_replied+"/tbl_court_id/"+tbl_court_id+"/tbl_category_id/"+tbl_category_id;<?php */?>
	}
</script>