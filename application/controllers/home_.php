<?php

/**
 * @desc   	  	Home Controller
 *
 * @category   	Controller
 * @author     	Shanavas
 * @version    	0.1
 */
class Home extends CI_Controller {


	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
    function index() {
		$this->load->view("login_form", $data);
		
	}


	/**
	* @desc    Function test
	*
	* @param   none
	* @access  default
	*/
    function test() {
		echo "here";
	}
	

	
}
?>