<?php
/**
 * @desc   	  	Login Controller
 * @category   	Controller
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */

class Login extends CI_Controller {
	/**
	* @desc    Default function for the Controller
	* @param   none
	* @access  default
	*/

    function index() {
		return "";
	}

	/**
	* @desc    Function to get general login page
	* @param   none
	* @access  default
	*/

    function general_login_page() {
		$lan            = $_REQUEST["lan"];
		$device         = $_REQUEST["device"];
		$device_uid     = $_REQUEST["device_uid"];
		$tbl_school_id  = $_REQUEST["tbl_school_id"];

		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		$data['page'] = "view_general_login_page";
		$this->load->view('view_template',$data);

	}
	/**
	* @desc    Function to get general login
	* @param   none
	* @access  default
	*/

function general_login() {
        $device_uid  = "";
		$lan 		 = $_REQUEST["lan"];
		$device 	 = $_REQUEST["device"];
		$device_uid  = $_REQUEST["device_uid"];
		$truncate    = $_REQUEST["truncate"];
		
		$data["lan"]        = $lan;
		$data["device"]     = $device;
		$data["device_uid"] = $device_uid;
		$data["truncate"]   = $_REQUEST["truncate"];
		
		$email              = $_REQUEST["email"];
		$password           = $_REQUEST["password"];

		//check for govt login
		$this->load->model("model_admin");
		$is_exist = $this->model_admin->authenticate_g($email,$password);
		if ($is_exist == "Y") {
		    $admin_user_obj = $this->model_admin->get_admin_info($email);
			$arr["login_id"] = $admin_user_obj[0]['tbl_admin_id'];
			$arr["role"] = "G";
			echo json_encode($arr);
			exit;
		}
	
	    //students login
		$this->load->model("model_student");
		$this->load->model("model_school");
		$this->load->model("model_classes");
		$is_exist = $this->model_student->authenticate_student($email, $password);	
		
		if ($is_exist == "Y") {
				
			$tbl_student_id 			= $this->model_student->get_student_id($email);
			$student_obj 	    		= $this->model_student->get_student_obj($tbl_student_id);
			$data["login_id"]           = $tbl_student_id;
			
			if($lan=="en")
			{
				$data["first_name"]    	=  $student_obj[0]["first_name"];
				$data["last_name"] 		=  $student_obj[0]["last_name"];
				
			}else{
				$data["first_name"]     =  $student_obj[0]["first_name_ar"];
				$data["last_name"] 	    = $student_obj[0]["last_name_ar"];
			}
			
			$file_name_updated          = $student_obj[0]["file_name_updated"];
			if($file_name_updated<>""){
				$data["url"] = IMG_PATH."/student/".$file_name_updated;
			}else{
				$data["url"] = "";
			}
			
			$tbl_class_id				=	$student_obj[0]["tbl_class_id"];
			$tbl_school_id              =   $student_obj[0]["tbl_school_id"];
			$active                     =   "Y";
			$data["tbl_class_id"] 		=   $student_obj[0]["tbl_class_id"];
			$data["school_id"] 		=   $student_obj[0]["tbl_school_id"];
			
			$class_obj                  = $this->model_classes->get_all_classes($tbl_school_id, $active, $tbl_class_id);
			
			if($lan=="en")
			{
				$class_name = $class_obj[0]['class_name']." ".$class_obj[0]['section_name'];
			}else{
				$class_name = $class_obj[0]['class_name_ar']." ".$class_obj[0]['section_name_ar'];
			}
			
			
			$school_name  = $this->model_school->get_school_name($tbl_school_id,$lan);
			
			$data["class_name"] 		= $class_name;
			$data["school_name"]        = $school_name;
				
			$user_type = "ST";
			$user_id   = $tbl_student_id;
			$token     = $device_uid;
			$data["role"]     = $user_type;
			
			
			$this->load->model("model_user_notify_token");
			if($device<>"iPhone")
			{
				$this->model_user_notify_token->save_user_notify_token($user_type, $user_id, $token, $device);
			}
			echo json_encode($data);
			return;
		}
		
			// Check for teacher login
		$this->load->model("model_teachers");
		$is_exist = $this->model_teachers->authenticate_t($email, $password);
		if ($is_exist == "Y") {
			
			$tbl_teacher_id     = $this->model_teachers->get_teachers_id($email);
			$teacher_obj        = $this->model_teachers->get_teacher_details($tbl_teacher_id);

			$tbl_school_id              = $teacher_obj[0]["tbl_school_id"];
			$data["tbl_school_id"] 		= $teacher_obj[0]["tbl_school_id"];
			$data["first_name"] 		= $teacher_obj[0]["first_name"];
			$data["first_name_ar"] 		= $teacher_obj[0]["first_name_ar"];
			$data["last_name"] 			= $teacher_obj[0]["last_name"];
			$data["last_name_ar"] 		= $teacher_obj[0]["last_name_ar"];
			
			 if($teacher_obj[0]['file_name_updated']<>""){
				$teacher_obj[0]['img_url']	    =  IMG_PATH_TEACHER."/".$teacher_obj[0]['file_name_updated'];
			 }else
			 {
				$teacher_obj[0]['img_url']	    =  '';
			}
			
			$data["img_url"]        = $teacher_obj[0]['img_url'];
			
			$this->load->model("model_school");
			$school_name  = $this->model_school->get_school_name($tbl_school_id,$lan);
			$data["school_name"]        = $school_name;

			$this->load->model("model_message");
			$data["data_messages"] = $this->model_message->get_all_teacher_messages($tbl_teacher_id);
			$user_type = "T";
			$user_id = $tbl_teacher_id;
			$token = $device_uid;
			$this->load->model("model_user_notify_token");
			if($device<>"iPhone")
			{
				$this->model_user_notify_token->save_user_notify_token($user_type, $user_id, $token, $device);
			}
			$data["tbl_teacher_id"] = $tbl_teacher_id;
			$data["page"] = "view_teachers_landing_page";
			$this->load->view('view_template',$data);
			return;
		}

		// Check for parent login
		$this->load->model("model_parents");
		$is_exist = $this->model_parents->authenticate($email, $password, "Y");
		if ($is_exist == "Y") {
			$tbl_parent_id 	= $this->model_parents->get_parent_id($email);
			$parent_obj 	= $this->model_parents->get_parent_obj($tbl_parent_id);
			$data["first_name"] 		= $parent_obj[0]["first_name"];
			$data["first_name_ar"] 		= $parent_obj[0]["first_name_ar"];
			$data["last_name"] 			= $parent_obj[0]["last_name"];
			$data["last_name_ar"] 		= $parent_obj[0]["last_name_ar"];
			$data["data_stu"]           =  $this->model_parents->get_students_of_parent($tbl_parent_id);
			$data["tbl_parent_id"]      = $tbl_parent_id;

			$user_type = "P";
			$user_id   = $tbl_parent_id;
			$token     = $device_uid;

			$this->load->model("model_user_notify_token");
			if($device<>"iPhone")
			{
				$this->model_user_notify_token->save_user_notify_token($user_type, $user_id, $token, $device);
			}
			$data["page"] = "view_parents_landing_page";
			$this->load->view('view_template',$data);
			return;
		}
		

		// Check for bus login
		$bus_code     = $email;
		$bus_password = $password;
		$this->load->model("model_bus");
		$is_exist = $this->model_bus->authenticate_bus($bus_code, $bus_password);

		if ($is_exist == "Y") {
			$tbl_bus_id    = $this->model_bus->get_bus_id($bus_code);
			$tbl_school_id = $this->model_bus->get_tbl_school_id($bus_code);
	// Once successfully logged in we need to delete old tracking session to avoid bus tracking from different devices.
			$this->model_bus->set_bus_tracking_session($tbl_bus_id, $device_uid, $device, $tbl_school_id);
			$this->load->model('model_bus_sessions');
			$data_rs                   = $this->model_bus_sessions->get_bus_sessions($tbl_school_id);
			$arr_data["tbl_bus_id"]    = $tbl_bus_id;
			$arr_data["tbl_school_id"] = $tbl_school_id;
			$arr_data["role"]          = "B";
			$arr_data["bus_sessions"]  = $data_rs;

			echo json_encode($arr_data);
			//$temp = json_encode($arr_data);
			//$temp = str_replace("null", '{}', $temp);
			//echo $temp;
			
			
			return;
		}

		if (trim($tbl_bus_id) == "") {$arr["login_id"] = "N";$arr["role"] = "N";}
		echo json_encode($arr);
		//$temp = json_encode($arr);
		//$temp = str_replace("null", '{}', $temp);
		//echo $temp;
		return;
	}


	function logout() {

		$user_id    = $_REQUEST["user_id"];
		$role       = $_REQUEST["role"];
		$lan        = $_REQUEST["lan"];
		$device     = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id  = $_REQUEST["school_id"];
		$token      = $_REQUEST["token"];

		$data["user_id"]    = $user_id;
		$data["role"]       = $role;
		$data["lan"]        = $lan;
		$data["device"]     = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"]  = $school_id;
		$data["token"]      = $token;

		$this->load->model("model_user_notify_token");
		$this->model_user_notify_token->delete_user_notify_token($user_id, $role);
		
		$arr["code"] = "200";
		echo json_encode($arr);

	}
	
	// students / children list  
	function students_list() {
		$lan 		 		= $_REQUEST["lan"];
		$device 	 		= $_REQUEST["device"];
		$device_uid  		= $_REQUEST["device_uid"];
		$truncate    		= $_REQUEST["truncate"];
		$tbl_parent_id    	= $_REQUEST["tbl_parent_id"];
		$data["lan"]        = $lan;
	
		$this->load->model("model_parents");
		$parent_obj 				= 	$this->model_parents->get_parent_obj($tbl_parent_id);
		$data["first_name"] 		= 	$parent_obj[0]["first_name"];
		$data["first_name_ar"] 		= 	$parent_obj[0]["first_name_ar"];
		$data["last_name"] 			= 	$parent_obj[0]["last_name"];
		$data["last_name_ar"] 		= 	$parent_obj[0]["last_name_ar"];
		$data["data_stu"]           =   $this->model_parents->get_students_of_parent($tbl_parent_id);
		$data["tbl_parent_id"]      =  $tbl_parent_id;

		$user_type = "P";
		$user_id   = $tbl_parent_id;
		$token     = $device_uid;
		$data["page"] = "view_parents_landing_page";
		$this->load->view('view_template',$data);
		return;
	}
	
	
	//Forgot Password Student / Teacher / Parent / Bus Supervisor / Ministry 
	
	public function forgot_password()
		{       
				$data = array();
				$email                = isset($_REQUEST['email'])? $_REQUEST['email']:'';
				$this->load->model('model_student');
				$this->load->model('model_parents');
				$this->load->model('model_teachers');
				$this->load->model('model_admin');
				$this->load->model('model_email');
				$password = "";
				
				$student_details = $this->model_student->get_student_info_by_email($email);
				if(!empty($student_details))
				{
					$user_obj = array();
					$name        = $student_details[0]['first_name']." ".$student_details[0]['last_name'];
					$user_id     = $student_details[0]['student_user_id'];
					$password    = base64_decode($student_details[0]['student_pass_code']);
					$email       = $student_details[0]['email'];
				} else {
				
					$parent_details = $this->model_parents->get_parent_info_by_email($email);
					if(!empty($parent_details))
					{
						$user_obj = array();
						$name        = $parent_details[0]['first_name']." ".$parent_details[0]['last_name'];
						$user_id     = $parent_details[0]['user_id'];
						$password    = base64_decode($parent_details[0]['pass_code']);
						$email       = $parent_details[0]['email'];
					} else {
						
							$teacher_details = $this->model_teachers->get_teacher_info_by_email($email);
							if(!empty($teacher_details))
							{
								$user_obj = array();
								$name        = $teacher_details[0]['first_name']." ".$teacher_details[0]['last_name'];
								$user_id     = $teacher_details[0]['user_id'];
								$password    = base64_decode($teacher_details[0]['pass_code']);
								$email       = $teacher_details[0]['email'];
							} else {
							
								// School Admin / Minstry / School / Timeline
								$admin_details = $this->model_admin->get_admin_info_by_email($email); 
								if(!empty($admin_details))
								{
									$user_obj = array();
									$name        = $admin_details[0]['first_name']." ".$admin_details[0]['last_name'];
									$user_id     = $admin_details[0]['user_id'];
									$password    = base64_decode($admin_details[0]['password_code']);
									$email       = $admin_details[0]['email'];
								}
							}
					}
				}
				
				if($email<>"" && $password <>"")
				{
					
					$response = "Success";
					$errorMsg = "User account information";
					$content ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
								<html xmlns="http://www.w3.org/1999/xhtml"><head>
								<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
								<title>AQDAR SCHOOL APPLICATION ACCOUNT INFORMATION</title>
								</head>
								<body bgcolor="">
								<table bgcolor="" cellpadding="0" cellspacing="0" width="100%">
								  <tbody><tr>
									<td><table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border:1px solid #c3a336;">
										<tbody>
									
										<tr style="background-color:#ba3a39; color:#fff; height:50px;"  >
										  <td align="center"> <strong>AQDAR SCHOOL APPLICATION</strong> </td>
										</tr>
										<tr>
										  <td><table border="0" cellpadding="0" cellspacing="0" width="100%">
											  <tbody><tr>
												<td width="10%">&nbsp;</td>
												<td align="left" width="80%"  style="padding-top:20px;"> <font style="font-family: Verdana, Geneva, sans-serif; color:#000; font-size:12px; line-height:21px">
												<strong><em>Dear '.$name.',</em></strong></font><br><br>
												  <font style="font-family: Verdana, Geneva, sans-serif; color:#000; font-size:12px; line-height:21px">
												 Your Aqdar School Application account information.
												 <br>
												 <br>
												 
												 Username:&nbsp;'.$user_id.'<br>
												 Password:&nbsp;'.$password.'<br>';
										
							$content .='	<br><br>
								On behalf of the AQDAR SCHOOL APPLICATION</font></td>
												<td width="10%">&nbsp;</td>
											  </tr>
											</tbody></table></td>
										</tr>
										<tr>
										  <td>&nbsp;</td>
										</tr>
										<tr>
										  <td>&nbsp;</td>
										</tr>
										
										<tr style="background-color:#c3a336; color:#fff; height:50px;" >
										  <td>&nbsp;</td>
										</tr>
									  </tbody></table></td>
								  </tr>
								</tbody></table>
								</body></html>'; 
					$subject   = "Aqdar School Application Account Information";
					$send_mail = $this->model_email->sendMail($email,$subject,$content);
				}else{
					$response = "Failure";
					$errorMsg = "Invalid User";
				}
				
				$data['email'] = $email;
				$response_array = array('response' => $response, 'result' => $data, 'errorMsg' => $errorMsg);
				echo json_encode($response_array);
				exit;
		}
	
	
		// Change Password
		 function change_password() {
			$password 		    = isset($_REQUEST['password'])? $_REQUEST['password']:'' ;
			$tbl_user_id 		= isset($_REQUEST['tbl_user_id'])? $_REQUEST['tbl_user_id']:'' ;
			$user_type 		    = isset($_REQUEST['user_type'])? $_REQUEST['user_type']:'' ;
			$data =  array();
			if (trim($password) == "") {
				$response = "Failure";
				$errorMsg = "Password couldn't change successfully.";
			} else {
				$this->load->model('model_student');
				$this->model_student->change_password(trim($password),$tbl_user_id, $user_type);
				$response = "Success";
				$errorMsg = "Password changed successfully.";
			}
				$response_array = array('response' => $response, 'result' => $data, 'errorMsg' => $errorMsg);
				echo json_encode($response_array);
				return;
		}	
		
	
	
	
	
	  // Web Forgot Password
	  
	public function web_forgot_password()
		{       
				$data = array();
				$email                    = isset($_POST['email'])? $_POST['email']:'';
				$user_type                = isset($_POST['user_type'])? $_POST['user_type']:'';
				$this->load->model('model_student');
				$this->load->model('model_parents');
				$this->load->model('model_teachers');
				$this->load->model('model_admin');
				$this->load->model('model_email');
				$password = "";
				
				if($user_type=="STUDENT")
				{
					$student_details = $this->model_student->get_student_info_by_email($email);
					if(!empty($student_details))
					{
						$user_obj = array();
						$name        = $student_details[0]['first_name']." ".$student_details[0]['last_name'];
						$user_id     = $student_details[0]['student_user_id'];
						$password    = base64_decode($student_details[0]['student_pass_code']);
						$email       = $student_details[0]['email'];
					} 
				} else if($user_type=="PARENT") {
				
					$parent_details = $this->model_parents->get_parent_info_by_email($email);
					if(!empty($parent_details))
					{
						$user_obj = array();
						$name        = $parent_details[0]['first_name']." ".$parent_details[0]['last_name'];
						$user_id     = $parent_details[0]['user_id'];
						$password    = base64_decode($parent_details[0]['pass_code']);
						$email       = $parent_details[0]['email'];
					} 
				} else if($user_type=="TEACHER") {	
					
					$teacher_details = $this->model_teachers->get_teacher_info_by_email($email);
					if(!empty($teacher_details))
					{
						$user_obj = array();
						$name        = $teacher_details[0]['first_name']." ".$teacher_details[0]['last_name'];
						$user_id     = $teacher_details[0]['user_id'];
						$password    = base64_decode($teacher_details[0]['pass_code']);
						$email       = $teacher_details[0]['email'];
					} 
					
				} else if($user_type=="SCHOOL" ||  $user_type=="MINISTRY"  || $user_type=="TIMELINE" ) {		
				
					// School Admin / Minstry / Timeline
					$admin_details = $this->model_admin->get_admin_info_by_email($email); 
					if(!empty($admin_details))
					{
						$user_obj = array();
						$name        = $admin_details[0]['first_name']." ".$admin_details[0]['last_name'];
						$user_id     = $admin_details[0]['user_id'];
						$password    = base64_decode($admin_details[0]['password_code']);
						$email       = $admin_details[0]['email'];
					}
							
				}
				
				if($email<>"" && $password <>"")
				{
					
					$response = "Success";
					$errorMsg = "User account information";
					$content ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
								<html xmlns="http://www.w3.org/1999/xhtml"><head>
								<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
								<title>AQDAR SCHOOL APPLICATION ACCOUNT INFORMATION</title>
								</head>
								<body bgcolor="">
								<table bgcolor="" cellpadding="0" cellspacing="0" width="100%">
								  <tbody><tr>
									<td><table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border:1px solid #c3a336;">
										<tbody>
									
										<tr style="background-color:#ba3a39; color:#fff; height:50px;"  >
										  <td align="center"> <strong>AQDAR SCHOOL APPLICATION</strong> </td>
										</tr>
										<tr>
										  <td><table border="0" cellpadding="0" cellspacing="0" width="100%">
											  <tbody><tr>
												<td width="10%">&nbsp;</td>
												<td align="left" width="80%"  style="padding-top:20px;"> <font style="font-family: Verdana, Geneva, sans-serif; color:#000; font-size:12px; line-height:21px">
												<strong><em>Dear '.$name.',</em></strong></font><br><br>
												  <font style="font-family: Verdana, Geneva, sans-serif; color:#000; font-size:12px; line-height:21px">
												 Your Aqdar School Application account information.
												 <br>
												 <br>
												 
												 Username:&nbsp;'.$user_id.'<br>
												 Password:&nbsp;'.$password.'<br>';
										
							$content .='	<br><br>
								On behalf of the AQDAR SCHOOL APPLICATION</font></td>
												<td width="10%">&nbsp;</td>
											  </tr>
											</tbody></table></td>
										</tr>
										<tr>
										  <td>&nbsp;</td>
										</tr>
										<tr>
										  <td>&nbsp;</td>
										</tr>
										
										<tr style="background-color:#c3a336; color:#fff; height:50px;" >
										  <td>&nbsp;</td>
										</tr>
									  </tbody></table></td>
								  </tr>
								</tbody></table>
								</body></html>'; 
					$subject   = "Aqdar School Application Account Information";
					$send_mail = $this->model_email->sendMail($email,$subject,$content);
					echo "Y";
					exit;
				}else{
					echo "N";
					exit;
				}
				echo "N";
				exit;
		}
	  // End Web Forgot Password
	

}



?>



