
<?php

/**
 * @desc   	  	Teacher attendance Controller
 *
 * @category   	Controller
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Teacher_attendance extends CI_Controller {


	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
    function index() {
		//Do nothing
	}


	/**
	* @desc    Image Gallery
	*
	* @param   none
	* @access  default
	*/
    function attendance_page() {
		
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["school_id"];
		$tbl_class_sessions_id = $_REQUEST["tbl_class_sessions_id"];
		
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		$data["tbl_class_id"] =  $_REQUEST["tbl_class_id"];
		$data["tbl_teacher_id"] =  $_REQUEST["user_id"];
		$data["tbl_class_sessions_id"] =  $_REQUEST["tbl_class_sessions_id"];
		
		$tbl_class_id = $_REQUEST["tbl_class_id"];
		$tbl_teacher_id = $_REQUEST["tbl_teacher_id"];
		$tbl_class_id = $tbl_class_id;
		$tbl_teacher_id = $tbl_teacher_id;
		date_default_timezone_set('Asia/Dubai');
		
		$attendance_date = date("Y-m-d");
		$data["attendance_date"] = $attendance_date;	

		$this->load->model('model_student');
		$data_rs = $this->model_student->get_student_list($tbl_class_id, $tbl_school_id);
		
		$this->load->model('model_teacher_attendance');
		$is_attendance_exists = $this->model_teacher_attendance->is_attendance_exists($tbl_class_id, $tbl_class_sessions_id, $attendance_date, $tbl_school_id);
		
		//print_r($data_rs);
		//exit();
		$data['data_rs'] = $data_rs;
		if (count($data_rs)<=0) {
			echo "{\"code\":\"N\"}";
			exit();
		}
		
		$data["is_attendance_exists"] = $is_attendance_exists;
		//echo "---->".trim($_POST["is_admin"]);
		//return;
		//Teacher admin
		if (trim($_POST["is_admin"]) == "Y") {
			$this->load->view('admin/view_teacher_teacher_attendance_page',$data);
			return;
		}			
		
		$data["page"] = "view_teacher_attendance_page";
		$this->load->view('view_template',$data);	 
	}
	
	
	/**
	* @desc    submit attendance
	* @param   none
	* @access  default
	*/
    function submit_attendance() {
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["school_id"];
		$user_id = $_REQUEST["user_id"];
		$tbl_class_sessions_id = $_REQUEST["tbl_class_sessions_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		$data["tbl_class_sessions_id"] = $tbl_class_sessions_id;
	 	$att_str = $_REQUEST["att_str"];
		$tbl_teacher_id = $user_id;
		$attendance_date = $_REQUEST["attendance_date"];
		$tbl_class_id = $_REQUEST["tbl_class_id"];
		// Y : Means tick
		// N : Means cross
		date_default_timezone_set('Asia/Dubai');
		$attendance_date = date("Y-m-d");
		$this->load->model('model_teacher_attendance');
		$this->model_teacher_attendance->delete_attendance_t($tbl_teacher_id, $attendance_date, $tbl_class_id, $tbl_class_sessions_id);
		$tbl_teacher_id_str = "";
		$tbl_class_id_str = "";
		//att_str = att_str + tbl_student_id+":"+option1+","+option2+","+option3+","+option4+";";
		$att_str_arr = explode(",",$att_str);
		for ($i=0; $i<(count($att_str_arr)); $i++) {
			$tbl_student_id_arr = explode(":",$att_str_arr[$i]);
			$tbl_student_id = $tbl_student_id_arr[0];
			if($tbl_student_id<>"")
			{
				$is_present = $tbl_student_id_arr[1];
				if ($is_present == "Y") {
					$message_ = "is present inside classroom.";
				} else if ($is_present == "LC") {
					$message_ = "is late coming inside classroom.";
				} if ($is_present == "EG") {
					$message_ = "is early going inside classroom.";
				} else {
					$message_ = "is not present inside classroom.";
				}
				$this->load->model('model_teacher_attendance');
				$this->load->model('model_parents');
				$this->load->model('model_student');
				
				$current_date = date("Y-m-d");
				$getSemesterId = $this->model_student->get_semester_id($tbl_school_id,$current_date);
				$tbl_semester_id = $getSemesterId[0]['tbl_semester_id'];
				
				$this->model_teacher_attendance->save_attendance_t($tbl_student_id, $tbl_teacher_id, $attendance_date, $is_present, $tbl_class_id, $tbl_school_id, $tbl_class_sessions_id, $tbl_semester_id);
				
				$tbl_parent_id = $this->model_parents->get_parent_of_student($tbl_student_id);
				
				$stu_obj = $this->model_student->get_student_obj($tbl_student_id);
				$first_name_stu = $stu_obj[0]['first_name'];
				$last_name_stu = $stu_obj[0]['last_name'];
				$tbl_class_id = $stu_obj[0]['tbl_class_id'];
				$tbl_class_id_str = $tbl_class_id_str."'".$tbl_class_id."',";
				$message = 'Dear Parent, your Child '.$first_name_stu.' '.$last_name_stu.' '.$message_;
				//echo $message."<br>";
				$this->load->model('model_user_notify_token');
				$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_parent_id);
				for ($b=0; $b<count($data_tkns); $b++) {
					
					if($data_tkns[$b]["token"]<>""){
								
						if (trim(ENABLE_PUSH) == "Y") {	
							//$message = urlencode($message);
							//$token = $tbl_parent_id."-".$data_tkns[$b]["token"];
							$token = $data_tkns[$b]["token"];
							$device = $data_tkns[$b]["device"];
		
							//$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$tbl_parent_id."&msg=".$message."&data=";
		
							$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$token."&msg=".$message."&data=";
		
							$this->load->model('model_user_notify_token');
							//$this->model_user_notify_token->send_notification($token , $message, $device);
							//$ch = curl_init();
							//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
							//curl_setopt($ch, CURLOPT_URL, $url);
							//curl_setopt($ch, CURLOPT_HEADER, 0);
							//curl_exec($ch);
							//curl_close($ch);
						}//if (trim(ENABLE_PUSH) == "Y")
					}
				}
			}
		}
		echo "{\"code\":\"200\"}";
	}

	/**
	* @desc    get attendance details between dates
	*
	* @param   none
	* @access  default
	*/
    function get_student_attendance() {
		
		/*$arr_att[0]["date"] = "2015-05-01";
		$arr_sess["9:00-9:30"] = "A";
		$arr_sess["9:30-10:00"] = "P";
		$arr_att[0]["sessions"] = $arr_sess;
		
		$arr_att[1]["date"] = "2015-05-02";
		$arr_sess["9:00-9:30"] = "P";
		$arr_sess["9:30-10:00"] = "P";
		$arr_att[1]["sessions"] = $arr_sess;
		
		$arr["attendance"] = $arr_att;
		echo json_encode($arr);
		exit();*/
		
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["school_id"];
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		$start_date = $_REQUEST["start_date"];
		$end_date = $_REQUEST["end_date"];
		
		//$start_date = strtotime('-1 day', strtotime($start_date));
		$start_date = date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $start_date) ) ));
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		$data["tbl_class_id"] =  $_REQUEST["tbl_class_id"];
		$data["tbl_teacher_id"] =  $_REQUEST["user_id"];
		$data["tbl_student_id"] =  $_REQUEST["tbl_student_id"];
		$data["start_date"] =  $_REQUEST["start_date"];
		$data["end_date"] =  $_REQUEST["end_date"];
		
	
		$tbl_teacher_id = $_REQUEST["tbl_teacher_id"];
		$tbl_class_id = $tbl_class_id;
		$tbl_teacher_id = $tbl_teacher_id;
		date_default_timezone_set('Asia/Dubai');
		
		$this->load->model('model_student');
		$student_data = $this->model_student->get_student_obj($tbl_student_id);
		$tbl_class_id = $student_data[0]['tbl_class_id'];
		
		$this->load->model('model_class_sessions');
		$data_ss = $this->model_class_sessions->get_class_sessions_all($tbl_school_id,$tbl_class_id);
		//print_r($data_ss);
		//exit();
		
		$this->load->model('model_teacher_attendance');
		$data_rs = $this->model_teacher_attendance->get_attendance_between_dates($tbl_student_id, $start_date, $end_date);
		
		/*print_r($data_rs);
		
		echo "<br>";
		echo "<br>";
		echo "<br>";*/
		//exit();
		$data['data_rs'] = $data_rs;
		if (count($data_rs)<=0) {
			echo "{\"code\":\"N\"}";
			exit();
		}
		
		$output_format = "Y-m-d";
		$step = '-1 day';
		$first = $start_date;			//"2015-05-01";
		$last = $end_date;				//"2015-05-31";
		$current = strtotime($last);
		$last = strtotime($first);
		$index = 0;
		//print_r($data_ss);
		//exit();
		while( $current >= $last ) {
			$computed_date = date($output_format, $current);
			$current = strtotime($step, $current);
			$index = $index + 1;
			
			$arr_dates[$index] = $computed_date;
			$arr_att[$index]["date"] = $computed_date;
			unset($arr_sess);
			
			for($i=0; $i<count($data_ss); $i++) {
				$tbl_class_sessions_id = $data_ss[$i]["tbl_class_sessions_id"];
				$start_time = $data_ss[$i]["start_time"];
				$end_time = $data_ss[$i]["end_time"];
				$arr_sess[$i]["tbl_class_sessions_id"] = $tbl_class_sessions_id;
				$arr_sess[$i]["start_time"] = $start_time;
				$arr_sess[$i]["end_time"] = $end_time;
				$arr_sess[$i]["status"] = "Z";
			}
			$arr_att[$index]["sessions"] = $arr_sess;
			//echo "<br>".$computed_date;
		}
		
		//$arr_att[$index]["sessions"] = $index;
		 
		/*
		//exit();
		$arr["attendance"] = $arr_att;
		echo json_encode($arr);
		echo "<br>";
		echo "<br>";
		echo "<br>";
		$key = array_search('2015-10-26', $arr_dates); 
		echo "--->".$key;
		echo "<br>";
		echo "<br>";
		echo "<br>";
		
		print_r($arr_att[$key]["sessions"][0]["start_time"]);
		exit();
		//$arr_att[$key] = "";
		$arr_temp = $arr_att[$key];
		$date_ = $arr_temp["date"];
		$arr_sess_ = $arr_temp["sessions"];
		
		for($i=0; $i<count($arr_sess_); $i++) {
			echo $arr_sess_[$i]["start_time"]."<br>";
		}
		exit();*/
		
		/*$arr_att[0]["date"] = "2015-05-01";
		$arr_sess["9:00-9:30"] = "A";
		$arr_sess["9:30-10:00"] = "P";
		$arr_att[0]["sessions"] = $arr_sess;
		
		$arr_att[1]["date"] = "2015-05-02";
		$arr_sess["9:00-9:30"] = "P";
		$arr_sess["9:30-10:00"] = "P";
		$arr_att[1]["sessions"] = $arr_sess;
		
		$arr["attendance"] = $arr_att;
		echo json_encode($arr);
		exit();*/

		//print_r($data_rs);
		for($i=0; $i<count($data_rs); $i++) {
			$attendance_date = $data_rs[$i]["attendance_date"];
			$tbl_class_sessions_id = $data_rs[$i]["tbl_class_sessions_id"];
			$is_present = $data_rs[$i]["is_present"];
		
			$key = array_search($attendance_date, $arr_dates);
			
			for($j=0; $j<count($data_ss); $j++) {
				$tbl_class_sessions_id_ = $arr_att[$key]["sessions"][$j]["tbl_class_sessions_id"];
				if ($tbl_class_sessions_id_ == $tbl_class_sessions_id) {
					$arr_att[$key]["sessions"][$j]["status"] = $is_present;
				}
			}
		}
		
		$arr["cnt"] = $index;
		$arr["attendance"] = $arr_att;
		
		echo json_encode($arr);
	}
	
	
	function in_array_r($needle, $haystack) {
		$found = false;
		foreach ($haystack as $item) {
		if ($item === $needle) { 
				$found = true; 
				break; 
			} elseif (is_array($item)) {
				$found = in_array_r($needle, $item); 
				if($found) { 
					break; 
				} 
			}    
		}
		return $found;
	}
	
}
?>

