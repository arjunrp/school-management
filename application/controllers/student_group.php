<?php

/**
 * @desc   	  	Student Group Controller
 * @category   	Controller
 * @version    	0.1
 */
class Student_group extends CI_Controller {

	/**
	* @desc    Default function for the Controller
	* @param   none
	* @access  default
	*/
    function index() {
		//Do nothing
	}

    /******************************************* STUDENT GROUP LIST ****************/
	 function student_group_list() {
		$user_id 	= $_REQUEST["user_id"];
		$role    	= $_REQUEST["role"];
		$lan     	= $_REQUEST["lan"];
		$device  	= $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		
		$data["user_id"]    = $user_id;
		$data["role"]       = $role;
		$data["lan"]        = $lan;
		$data["device"]     = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"]  = $school_id;
		$tbl_school_id      = $school_id;
		$tbl_student_id     = $user_id;
		//$tbl_parent_id    = "5a10b34248df24d";
		//$tbl_school_id    = "9697bfd53bcaf6d";
		//$lan              = "ar";
		$this->load->model('model_student_group');
		$data = $this->model_student_group->get_list_students_of_group($tbl_school_id,$tbl_student_id,$role);
        $result = array();
		if($lan=="en")
		{
 			 if(count($data)>0)
			 {
				for($m=0;$m<count($data);$m++){
					$result[$m]['group_name']		    = $data[$m]['group_name_en'];
					$result[$m]['tbl_student_group_id']	= $data[$m]['tbl_student_group_id'];
					$studentGroupForums 				= $this->model_student_group->get_list_student_topics($tbl_school_id,$tbl_student_id,$data[$m]['tbl_student_group_id']);
					$result[$m]['cntForum']             = count($studentGroupForums);
				}
			 }
		}else{
			 if(count($data)>0)
			 {
				 for($m=0;$m<count($data);$m++){
					$result[$m]['group_name']			= $data[$m]['group_name_ar'];
					$result[$m]['tbl_student_group_id']	= $data[$m]['tbl_student_group_id'];
					$studentGroupForums                 = $this->model_student_group->get_list_student_topics($tbl_school_id,$tbl_student_id,$data[$m]['tbl_student_group_id']);
					$result[$m]['cntForum']             = count($studentGroupForums);
				}
			 }
		}
		$response = "Success";
		$errorMsg = "Students Group Information";
		$response_array = array('response' => $response, 'result' => $result, 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
	}

    /******************************* END STUDENT GROUP LIST ****************************************/
	
	/******************************* STUDENT GROUP MESSAGE *****************************************/
	
	/************************* Student Community Group ****************************/
     function student_message_community() {
		 
		$lan 				= $_REQUEST["lan"];
		$device 			= $_REQUEST["device"];
		$device_uid 		= $_REQUEST["device_uid"];
		$tbl_school_id  	= $_REQUEST["tbl_school_id"]; 
		$tbl_teacher_id  	= $_REQUEST["tbl_teacher_id"];
		$tbl_student_id  	= $_REQUEST["tbl_student_id"];
				
		$this->load->model("model_teachers");
		$this->load->model("model_student_group");
		$this->load->model("model_message");
		
	/*	$rs_all_teachers     = $this->model_teachers->get_list_teachers($tbl_school_id,'','Y');	
		for($d=0;$d<count($rs_all_teachers);$d++)
		{
			if($rs_all_teachers[$d]['file_name_updated']<>"")
				$rs_all_teachers[$d]['file_name_updated'] = IMG_PATH_TEACHER."/".$rs_all_teachers[$d]['file_name_updated'];
			else
				$rs_all_teachers[$d]['file_name_updated'] = "";
		}*/
		
		$rs_all_groups       = $this->model_student_group->list_my_students_group($tbl_school_id, $tbl_student_id, $tbl_teacher_id );
			
		for($m=0;$m<count($rs_all_groups); $m++)
		{
			$students_list  = $this->model_message->get_students_list_in_group($rs_all_groups[$m]['tbl_student_group_id'],$tbl_school_id);
			$rs_all_groups[$m]['cntStudent']      = count($students_list);
			
			for($d=0;$d<count($students_list);$d++)
			{
				if($students_list[$d]['file_name_updated']<>"")
					$students_list[$d]['file_name_updated'] = IMG_PATH_STUDENT."/".$students_list[$d]['file_name_updated'];
				else
					$students_list[$d]['file_name_updated'] = "";
			}
			$rs_all_groups[$m]['students_list']   = $students_list;
		}
		//$data['students_list']     =  $rs_all_teachers;
		$data['student_community'] =  $rs_all_groups;
		
		$response = "Success";
		$errorMsg = "Students Community Listed  successfully";
		$response_array = array('response' => $response, 'result' => $data, 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
	 }
	 
	 // Show Messages - Students Community / Teacher 
  	function get_student_community_message()
  	{
		$lan 					= $_REQUEST["lan"];
		$device 				= $_REQUEST["device"];
		$device_uid 			= $_REQUEST["device_uid"];
		$logged_user_id    	    = $_REQUEST["logged_user_id"];
		$message_to    	        = $_REQUEST["message_to"];
		$role    	            = $_REQUEST["role"];   // Student / Group Admin - Teacher
		$tbl_school_id 			= $_REQUEST["tbl_school_id"];
		$message_mode 			= $_REQUEST["message_mode"];  // G - Group ,  P - Private
		$tbl_student_group_id 	= $_REQUEST["tbl_student_group_id"];

		$this->load->model('model_student_group');
		$this->load->model('model_teachers');
		$this->load->model('model_message');
		$this->load->model('model_student');

		$listMessage = $this->model_student_group->get_student_community_messages($logged_user_id, $role, $tbl_school_id, $message_mode, $tbl_student_group_id, $message_to);
		$data = array();
		for($i=0;$i<count($listMessage);$i++)
		{
					if($listMessage[$i]['message_dir'] == "T-ST")
					{
						$teacher_obj = $this->model_teachers->get_teacher_details($listMessage[$i]['message_from']);
						if($lan=="ar"){
							$first_name_user = $teacher_obj[0]['first_name_ar'];
							$last_name_user  = $teacher_obj[0]['last_name_ar'];
						}else{
							$first_name_user = $teacher_obj[0]['first_name'];
							$last_name_user  = $teacher_obj[0]['last_name'];
						}
	
						$listMessage[$i]['name']        = $first_name_user." ".$last_name_user;  //from
						$listMessage[$i]['tbl_user_id'] = $teacher_obj[0]['tbl_teacher_id'];
					}else{
						$this->load->model('model_student');
						$student_obj = $this->model_student->get_student_obj($listMessage[$i]['message_from']);
						if($lan=="ar"){
							$first_name_user = $student_obj[0]['first_name_ar'];
							$last_name_user  = $student_obj[0]['last_name_ar'];
						}else{
							$first_name_user = $student_obj[0]['first_name'];
							$last_name_user  = $student_obj[0]['last_name'];
						}
	
						$listMessage[$i]['name']        = $first_name_user." ".$last_name_user;  //from
						$listMessage[$i]['tbl_user_id'] = $student_obj[0]['tbl_student_id'];
					}
					
			$file_name_updated = "";
			$file_name_updated_audio ="";
			$data_image_rs = "";
			$data_audio_rs = "";

			if($listMessage[$i]['tbl_item_id']<>""){
				$data_image_rs =  $this->model_message->get_message_image($listMessage[$i]['tbl_item_id']);	
				//print_r($data_image_rs);
				$file_name_updated = $data_image_rs[0]['file_name_updated'];
				$file_name_updated_thumb = $data_image_rs[0]['file_name_updated_thumb'];
				$data_audio_rs =  $this->model_message->get_message_audio($listMessage[$i]['tbl_item_id']);	
				$file_name_updated_audio = $data_audio_rs[0]['file_name_updated'];
			}

			if (trim($file_name_updated) != "") {
			$data[$i]["message_img"] = HOST_URL."/admin/uploads/".$file_name_updated;
			$data[$i]["message_img_thumb"] = HOST_URL."/admin/uploads/".$file_name_updated_thumb;
			} else {
				$data[$i]["message_img"] = "";
				$data[$i]["message_img_thumb"] = "";
			}

			if (trim($file_name_updated_audio) != "") {
				$data[$i]["message_audio"] = HOST_URL."/admin/uploads/".$file_name_updated_audio;
			} else {
				$data[$i]["message_audio"] = "";
			}

			$data[$i]['name'] 			=  	$listMessage[$i]['name'];
			$data[$i]['message'] 		=  	isset($listMessage[$i]['message'])? $listMessage[$i]['message']:'' ;
			$data[$i]['teacher_id']     =   $listMessage[$i]['message_from'];  // for easiness student id or teacher id is passing to same variable 
			$data[$i]['date'] 			= 	date("H:i - d/m/Y",strtotime($listMessage[$i]["added_date"]));
		}

		$response = "Success";
		$errorMsg = "Student community message listed successfully";
		$response_array = array('response' => $response, 'messages' => $data, 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
	}
	  
	 
	//Save Student Community Message
	function save_student_community_message()
	 {
		$lan 				= $_REQUEST["lan"];
		$device 			= $_REQUEST["device"];
		$device_uid 		= $_REQUEST["device_uid"];
		$message_from 	    = $_REQUEST["message_from"];
		$message_to 		= $_REQUEST["message_to"];
		$message_dir 	    = $_REQUEST["message_dir"];
		$message_type  	    = $_REQUEST["message_type"];
		$tbl_message 		= $_REQUEST["tbl_message"];
		$tbl_item_id    	= $_REQUEST["tbl_item_id"];
		$message_mode 		= $_REQUEST["message_mode"];  // G - Group , P - Private
		$tbl_student_group_id 	= $_REQUEST["tbl_student_group_id"];
		$tbl_school_id 			= $_REQUEST["tbl_school_id"];
		$this->load->model('model_student_group');
		$this->load->model('model_teachers');
		$this->load->model('model_parents');
		$this->load->model('model_student');
		$this->load->model('model_message');
		$saveMessage = $this->model_student_group->save_student_community_message($message_from, $message_to, $message_type, $tbl_message, $tbl_school_id, $tbl_item_id, $message_mode,
		                                                                              $tbl_student_group_id, $message_dir);
			
	    if($message_dir=="T-ST")
		{ 		
			$teacher_obj = $this->model_teachers->get_teacher_details($message_from);
			if($lan=="ar"){
				$first_name_teacher = $teacher_obj[0]['first_name_ar'];
				$last_name_teacher  = $teacher_obj[0]['last_name_ar'];
			}else{
				$first_name_teacher = $teacher_obj[0]['first_name'];
				$last_name_teacher  = $teacher_obj[0]['last_name'];
			}
			$user_name = $first_name_teacher." ".$last_name_teacher;  //from teacher
			$messageTo = "Student";
		
		}else{
			
			$student_obj = $this->model_student->get_student_obj($message_from);
			if($lan=="ar"){
				$first_name_student = $student_obj[0]['first_name_ar'];
				$last_name_student  = $student_obj[0]['last_name_ar'];
			}else{
				$first_name_student = $student_obj[0]['first_name'];
				$last_name_student  = $student_obj[0]['last_name'];
			}
			$user_name = $first_name_student." ".$last_name_student;  //from teacher
			
			$messageTo = "";
		}
		
	    
		 if($message_dir=="T-ST")
		{ 
			$students_list  = $this->model_message->get_students_list_in_group($rs_all_groups[$m]['tbl_student_group_id'],$tbl_school_id);
			for($m=0;$m<count($students_list);$m++)
			{
				if($lan=="ar"){
					    $first_name_to_student 	= $students_list[$m]['first_name_ar'];
						$last_name_to_student  	= $students_list[$m]['last_name_ar'];
				}else{
						$first_name_to_student 	= $students_list[$m]['first_name'];
						$last_name_to_student  	= $students_list[$m]['last_name'];
				}
				
				$student_to_name = $first_name_to_student." ".$last_name_to_student;
				$tbl_userId	     =	$students_list[$m]['tbl_student_id'];
		
		
				$this->load->model('model_user_notify_token');
				$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_userId);
				for ($b=0; $b<count($data_tkns); $b++) {										
					//Send push message
					if (trim(ENABLE_PUSH) == "Y") {
							
						$message = 'Dear '.$messageTo.', please check message in your application inbox from '.$user_name;
						$token   = $data_tkns[$b]["token"];
						$device  = $data_tkns[$b]["device"];
						$this->model_user_notify_token->send_notification($token , $message, $device);//echo $url;
					}//if (trim(ENABLE_PUSH) == "Y")
				}
					
				
				
			}
			
		}else{
			$teacher_to_obj = $this->model_teachers->get_teacher_details($message_to); //to teacher
			if($lan=="ar"){
					$first_name_to_teacher = $teacher_to_obj[0]['first_name_ar'];
					$last_name_to_teacher  = $teacher_to_obj[0]['last_name_ar'];
			}else{
					$first_name_to_teacher = $teacher_to_obj[0]['first_name'];
					$last_name_to_teacher  = $teacher_to_obj[0]['last_name'];
			}
				
			$teacher_to_name = $first_name_to_teacher." ".$last_name_to_teacher;
			$tbl_userId	=	$teacher_to_obj[0]['tbl_teacher_id'];
	
	
			$this->load->model('model_user_notify_token');
			$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_userId);
			for ($b=0; $b<count($data_tkns); $b++) {										
				//Send push message
				if (trim(ENABLE_PUSH) == "Y") {
						
					$message = 'Dear '.$messageTo.', please check message in your application inbox from '.$user_name;
					$token   = $data_tkns[$b]["token"];
					$device  = $data_tkns[$b]["device"];
					$this->model_user_notify_token->send_notification($token , $message, $device);//echo $url;
				}//if (trim(ENABLE_PUSH) == "Y")
			}
		}
		$response = "Success";
		$errorMsg = "Student community message sent successfully";
		$response_array = array('response' => $response, 'result' => '', 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
	}
	/************************* End Student Community Group ************************/
	
	/******************************* END STUDENT GROUP MESSAGE *************************************/ 
	
	
	
	

     // STUDENTS GROUP FORUM.................................

	 function get_list_student_topics() {
		$user_id 		= $_REQUEST["user_id"];
		$role 			= $_REQUEST["role"];
		$lan 			= $_REQUEST["lan"];
		$device 		= $_REQUEST["device"];
		$device_uid 	= $_REQUEST["device_uid"];
		$school_id 		= $_REQUEST["school_id"];
		$tbl_student_group_id 	= $_REQUEST["tbl_student_group_id"];
		$student_id 			= $_REQUEST["student_id"];
		
		$data["user_id"] 		= $user_id;
		$data["role"] 			= $role;
		$data["lan"] 			= $lan;
		$data["device"] 		= $device;
		$data["device_uid"] 	= $device_uid;
		$data["school_id"] 		= $school_id;
		$data["tbl_student_group_id"] 	= $tbl_student_group_id;
		$data["student_id"] 			= $student_id;
		
		
		$tbl_school_id = $school_id;
		$tbl_student_id = $user_id;
		//$tbl_parent_id = "5a10b34248df24d";
		//$tbl_school_id = "9697bfd53bcaf6d";
		//$tbl_parent_group_id = "c9e2fb449318c74e398df4de54a782c3";
		
		$this->load->model('model_student');
		$this->load->model('model_student_group');
		$student_data = $this->model_student->get_student_obj($student_id);
		$tbl_class_id = $student_data[0]['tbl_class_id'];
		$data = $this->model_student_group->get_list_student_topics($tbl_school_id,$tbl_student_id,$tbl_student_group_id,$tbl_class_id);
		for($y=0;$y<count($data);$y++)
		{
			$dataStudentInfo = $this->model_student->get_student_obj($data[$y]['posted_by']);
			if($lan=="ar")
			{
				$first_name = $dataStudentInfo[0]["first_name_ar"];
				$last_name  = $dataStudentInfo[0]["last_name_ar"];
			}else{
				$first_name = $dataStudentInfo[0]["first_name"];
				$last_name  = $dataStudentInfo[0]["last_name"];
			}
			$data[$y]['posted_by'] = $first_name." ".$last_name;
			$item_id = $data[$y]["item_id"];
			if($item_id<>"")
					{
						$this->load->model('model_message');
						$dataRecords = $this->model_message->get_upload_files($item_id);
						$image 				= $dataRecords[0]['file_name_updated'];
						$image_thumb 		= $dataRecords[0]['file_name_updated_thumb'];
						$data[$y]['message_img']		= HOST_URL."/admin/uploads/".$image; 
						$data[$y]['message_img_thumb']	= HOST_URL."/admin/uploads/".$image_thumb;
					}else{
						
						$data[$y]['message_img']		  = ""; 
						$data[$y]['message_img_thumb']	= "";
					}
			$posted_datetime 	= $data[$y]['added_date'];
			$current_datetime 	= date("Y-m-d h:i:s");
			$this->load->model('model_parents');
			$postedTime = $this->model_parents->time_difference($current_datetime, $posted_datetime);
			$data[$y]['posted_expiry'] = $postedTime;
			
			$forumComments = $this->model_student_group->get_list_forum_comments($tbl_school_id,$tbl_student_id,$data[$y]['tbl_student_forum_id']);
			$data[$y]['cntComments'] = count($forumComments);
		}
		
		$response = "Success";
		$errorMsg = "Students forum topics listed successfully";
		$response_array = array('response' => $response, 'result' => $data, 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
	}
	
	
	 function get_list_forum_comments() {
		$user_id 				= $_REQUEST["user_id"];
		$role 					= $_REQUEST["role"];
		$lan 					= $_REQUEST["lan"];
		$device 				= $_REQUEST["device"];
		$device_uid 			= $_REQUEST["device_uid"];
		$school_id 				= $_REQUEST["school_id"];
		$tbl_student_forum_id 	= $_REQUEST["tbl_student_forum_id"];
		
		$data["user_id"] 	= $user_id;
		$data["role"] 		= $role;
		$data["lan"] 		= $lan;
		$data["device"] 	= $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"]  = $school_id;
		$data["tbl_student_forum_id"] = $tbl_student_forum_id;
		
		$tbl_school_id = $school_id;
		$tbl_student_id = $user_id;
		//$tbl_parent_id = "5a10b34248df24d";
		//$tbl_school_id = "9697bfd53bcaf6d";
		//$tbl_parent_group_id = "c9e2fb449318c74e398df4de54a782c3";
		$this->load->model('model_student_group');
		$this->load->model('model_student');
		$data = $this->model_student_group->get_list_forum_comments($tbl_school_id,$tbl_student_id,$tbl_student_forum_id);
		for($y=0;$y<count($data);$y++)
		{
			$dataStudentInfo = $this->model_student->get_student_obj($data[$y]['commented_by']);
			if($lan=="ar")
			{
				$first_name 	= $dataStudentInfo[0]["first_name_ar"];
				$last_name 		= $dataStudentInfo[0]["last_name_ar"];
			}else{
				$first_name 	= $dataStudentInfo[0]["first_name"];
				$last_name 		= $dataStudentInfo[0]["last_name"];
			}
			$data[$y]['commented_by'] = $first_name." ".$last_name;
			$item_id = $data[$y]["item_id"];
			if($item_id<>"")
			{
				$this->load->model('model_message');
				$dataRecords = $this->model_message->get_upload_files($item_id);
		
				$image 				= $dataRecords[0]['file_name_updated'];
				$image_thumb 		= $dataRecords[0]['file_name_updated_thumb'];
				$data[$y]['message_img']		= HOST_URL."/admin/uploads/".$image; 
				$data[$y]['message_img_thumb']	= HOST_URL."/admin/uploads/".$image_thumb;
			}
			$posted_datetime 	= $data[$y]['added_date'];
			$current_datetime 	= date("Y-m-d h:i:s");
			$this->load->model('model_parents');
			$postedTime = $this->model_parents->time_difference($current_datetime, $posted_datetime);
			$data[$y]['commented_expiry'] = $postedTime;
		}
		$response = "Success";
		$errorMsg = "Parents forum comments listed successfully";
		$response_array = array('response' => $response, 'result' => $data, 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
	}
	
	function add_student_forum_comments()
	{
		$user_id 				= $_REQUEST["user_id"];
		$role 		   			= $_REQUEST["role"];
		$lan 		    		= $_REQUEST["lan"];
		$device 	     		= $_REQUEST["device"];
		$device_uid    	 		= $_REQUEST["device_uid"];
		$school_id      		= $_REQUEST["school_id"];
		$comments 	  			= $_REQUEST["comments"];
		$item_id 	    		= $_REQUEST["tbl_item_id"];
		$token 		  			= $_REQUEST["token"];
		$tbl_student_forum_id 	= $_REQUEST["tbl_student_forum_id"];
		$tbl_student_id 		= $user_id;
		$this->load->model("model_student_group");
		$res = $this->model_student_group->saveStudentForumComments($school_id,$tbl_student_forum_id,$comments,$item_id,$tbl_student_id);
		
		$dataStudentGroup = $this->model_student_group->get_studentgroup_byforum($school_id,$tbl_student_forum_id);
		$tbl_student_group_id = $dataStudentGroup[0]['tbl_student_group_id'];
        /********************* forum comments notification **************************/
		$this->load->model('model_user_notify_token');
		$dataStudents = $this->model_student_group->get_list_student_group_members($school_id,$tbl_student_id,$tbl_student_group_id);
		for($m=0;$m<count($dataStudents); $m++){
			   $tbl_userId = "";
			   $tbl_userId = $dataStudents[$m]['tbl_student_id'];
			   if($tbl_student_id<>$tbl_userId)
		       {
				$first_name 	= $dataStudents[$m]['first_name'];
				$last_name  	= $dataStudents[$m]['last_name'];
				$first_name_ar  = $dataStudents[$m]['first_name_ar'];
				$first_name_ar  = $dataStudents[$m]['last_name_ar'];
				$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_userId);
				//print_r($data_tkns);
				for ($b=0; $b<count($data_tkns); $b++) {										
					//Send push message
					if (trim(ENABLE_PUSH) == "Y") {
						/*PUSH START*/
					
						$message = 'Dear '.$first_name.' '.$last_name.' , Added a new comments in your discussion forum';
						$token = $data_tkns[$b]["token"];
						$device = $data_tkns[$b]["device"];
						
					   $this->model_user_notify_token->send_notification($token , $message, $device);//echo $url;
						/*PUSH END*/				
					}//if (trim(ENABLE_PUSH) == "Y")
				}
			}
		}
		/************************ end forum comments notification ************************/
		
		$data['msg'] = $res['msg'];
		$response = "Success";
		$errorMsg = "Students - forum comment saved successfully";
		$response_array = array('response' => $response, 'result' => $data, 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
	}


	function add_student_forum()
    {
		$user_id 		= $_REQUEST["user_id"];
		$role 		   	= $_REQUEST["role"];
		$lan 		    = $_REQUEST["lan"];
		$device 	    = $_REQUEST["device"];
		$device_uid     = $_REQUEST["device_uid"];
		$school_id      = $_REQUEST["school_id"];
		$message 	    = $_REQUEST["message"];
		$item_id 	    = $_REQUEST["tbl_item_id"];
		$token 		  	= $_REQUEST["token"];
		$tbl_student_group_id 		= $_REQUEST["tbl_student_group_id"];
		$tbl_student_id          	= $user_id;
		$this->load->model("model_student_group");
		$res 			= $this->model_student_group->saveStudentGroupForum($school_id,$tbl_student_group_id,$message,$item_id,$tbl_student_id);
		$dataStudents 	= $this->model_student_group->get_list_student_group_members($school_id,$tbl_student_id,$tbl_student_group_id);
		for($m=0;$m<count($dataStudents); $m++){
			   $tbl_userId = "";
			   $tbl_userId = $dataStudents[$m]['tbl_student_id'];
			   if($tbl_student_id<>$tbl_userId)
		       {
					$first_name 	= $dataStudents[$m]['first_name'];
					$last_name  	= $dataStudents[$m]['last_name'];
					$first_name_ar  = $dataStudents[$m]['first_name_ar'];
					$first_name_ar  = $dataStudents[$m]['last_name_ar'];
					$this->load->model('model_user_notify_token');
					$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_userId);
				
					for ($b=0; $b<count($data_tkns); $b++) {										
						
						if (trim(ENABLE_PUSH) == "Y") {
							/*PUSH START*/
							$message 	= 'Dear '.$first_name.' '.$last_name.' , Added a new topic in your discussion forum';
							$token 		= $data_tkns[$b]["token"];
							$device 	= $data_tkns[$b]["device"];
							$this->model_user_notify_token->send_notification($token , $message, $device);//echo $url;
						}
					}
			   }
		}
		/**************** end forum post notification ******************/
		$data['msg'] = $res['msg'];
		$response = "Success";
		$errorMsg = "Students - forum comment saved successfully";
		$response_array = array('response' => $response, 'result' => $data, 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
	}


	 function student_group_members() {
		$user_id 			 	= $_REQUEST["user_id"];
		$role 					= $_REQUEST["role"];
		$lan 				 	= $_REQUEST["lan"];
		$device 			  	= $_REQUEST["device"];
		$device_uid 		  	= $_REQUEST["device_uid"];
		$school_id 		   		= $_REQUEST["school_id"];
		$tbl_student_group_id 	= $_REQUEST["tbl_student_group_id"];
		$data["user_id"]     	= $user_id;
		$data["role"] 			= $role;
		$data["lan"] 			= $lan;
		$data["device"] 		= $device;
		$data["device_uid"] 	= $device_uid;
		$data["school_id"] 		= $school_id;
		$data["tbl_student_group_id"] 	= $tbl_student_group_id;
		$tbl_school_id 					= $school_id;
		$tbl_student_id					= $user_id;
		//$tbl_parent_id = "5a10b34248df24d";
		//$tbl_school_id = "9697bfd53bcaf6d";
		//$tbl_parent_group_id = "c9e2fb449318c74e398df4de54a782c3";
		$this->load->model('model_student_group');
		$data = $this->model_student_group->get_list_student_group_members($tbl_school_id,$tbl_student_id,$tbl_student_group_id);
		$response = "Success";
		$errorMsg = "Students Group Information";
		$response_array = array('response' => $response, 'result' => $data, 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
	}	
	
	/******************************** END STUDENT GROUP FORUM ****************************/
	
	
}
?>

