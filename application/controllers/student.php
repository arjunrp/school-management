<?php
/**
 * @desc   	  	Student Controller
 * @category   	Controller
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */

class Student extends CI_Controller {
	/**
	* @desc Default constructor for the Controller
	* @access default
	*/


    public function __construct() {
		parent::__construct();
    }


	/**
	* @desc    Get students against class
	* @param   none
	* @access  default
	*/
    function get_all_students_against_class() {
		$lan     	 	= $_REQUEST["lan"];
		$device     	 = $_REQUEST["device"];
		$device_uid 	 = $_REQUEST["device_uid"];
		$tbl_school_id  = $_REQUEST["tbl_school_id"];
		$tbl_class_id   = $_REQUEST["tbl_class_id"];
		$data["lan"]    = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		$data["tbl_class_id"] = $tbl_class_id;
		
		$this->load->model('model_student');
		$all_students_against_class_rs = $this->model_student->get_all_students_against_class($tbl_class_id);
		//print_r($all_students_against_class_rs);
		//if (count($all_students_against_class_rs)<=0) {echo "--no--"; exit();}
		$data['all_students_against_class_rs'] = $all_students_against_class_rs;
		$data['page'] = "view_all_students_against_class_ddm";
		$this->load->view('view_template',$data);
	}

   function get_all_students_against_selected_class() {
		$lan 					= $_REQUEST["lan"];
		$device 		 		 = $_REQUEST["device"];
		$device_uid             = $_REQUEST["device_uid"];
		$tbl_school_id          = $_REQUEST["tbl_school_id"];
		$tbl_class_id           = $_REQUEST["tbl_class_id"];
		$data["lan"]            = $lan;
		$data["device"]         = $device;
		$data["device_uid"]     = $device_uid;
		$data["tbl_school_id"]  = $tbl_school_id;
		$data["tbl_class_id"]   = $tbl_class_id;
		$classes = explode(",",$tbl_class_id);
		$this->load->model('model_student');
		$this->load->model('model_classes');
		$student_record = array();
		if(count($classes)>0)
		{  $m=0;
		   for($x=0;$x<count($classes);$x++)
		   {
			   $tbl_class_id = $classes[$x];
			   if($tbl_class_id<>""){
				   $student_record= $this->model_student->get_all_students_against_class($tbl_class_id);
				   for($a=0;$a<count($student_record);$a++)
				   {
							$tbl_student_id                                             = $student_record[$a]['tbl_student_id'];
							$all_students_against_class_rs[$m]['tbl_class_id']  = $tbl_class_id;
							$all_students_against_class_rs[$m]['first_name']    = $student_record[$a]['first_name'];
							$all_students_against_class_rs[$m]['last_name']     = $student_record[$a]['last_name'];
							$all_students_against_class_rs[$m]['first_name_ar'] = $student_record[$a]['first_name_ar'];
							$all_students_against_class_rs[$m]['last_name_ar']  = $student_record[$a]['last_name_ar'];
							
							$all_students_against_class_rs[$m]['tbl_student_id']= $tbl_student_id;
							$class_details       = $this->model_classes->getClassInfo($tbl_class_id);
							$tbl_section_id 	  = $class_details[0]['tbl_section_id'];
							$data_se             = $this->model_classes->getClassSectionInfo($tbl_section_id);		
							$section_name 		=  $data_se[0]["section_name"];
							$section_name_ar 	 =  $data_se[0]["section_name_ar"];
							$all_students_against_class_rs[$m]['class_name']    = $class_details[0]['class_name']." ".$section_name;
							$all_students_against_class_rs[$m]['class_name_ar'] = $class_details[0]['class_name_ar']." ".$section_name_ar;
							
							$m=$m+1;
					}
			   }
			}
		}
		
		//print_r($all_students_against_class_rs);
		//if (count($all_students_against_class_rs)<=0) {echo "--no--"; exit();}
		$data['all_students_against_class_rs'] = $all_students_against_class_rs;
		$data['page'] = "view_all_students_against_selected_class";
		$this->load->view('view_template',$data);
	}


	/**
	* @desc    Get students images
	* @param   none
	* @access  default
	*/
    function gallery_images() {
		$user_id 		 = $_REQUEST["user_id"];
		$role 			= $_REQUEST["role"];
		$lan             = $_REQUEST["lan"];
		$device          = $_REQUEST["device"];
		$device_uid      = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		$data["user_id"] 	 = $user_id;
		$data["role"]    	= $role;
		$data["lan"]         = $lan;
		$data["device"]      = $device;
		$data["device_uid"]  = $device_uid;
		$data["school_id"] = $school_id;
		$data["tbl_student_id"] = $tbl_student_id;
		$this->load->model('model_student');
		$data_gallery_images = $this->model_student->get_gallery_student_images($tbl_student_id);
		if (count($data_gallery_images) <=0) {
			$arr["code"] = "200";
			echo json_encode($arr);
			return;
		}
		$data["data_gallery_images"] = $data_gallery_images;
		$data["page"] = "view_image_gallery_images_student";
		$this->load->view('view_template',$data);
	}


	/**
	* @desc    Get students against class
	* @param   none
	* @access  default
	*/
	function gal_img() {
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		
		$this->load->model('model_student');
		$tbl_image_gallerystu_id = $_REQUEST["tbl_image_gallerystu_id"];
		$data_gallery_image = $this->model_student->get_gallerystu_image($tbl_image_gallerystu_id);
		if (count($data_gallery_image)<=0) {
			echo "--no--";
			exit();
		}
		
		$data["page"] = "view_image_gallery_image";
		$this->load->view('view_template',$data);
	
	}


	/**
	* @desc    Save points
	*
	* @param   none
	* @access  default
	*/
	function children_instant_snaps() {
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		$tbl_parent_id = $_REQUEST["tbl_parent_id"];
		if ($module_id == "cdrpt") {
			$data["page"] = "view_student_list_cdrpt";
		} else {
			$data["page"] = "view_student_list";
		}			
		$this->load->view('view_template',$data);
	}


	/**
	* @desc    Issue card
	* @param   none
	* @access  default
	*/
	function student_list() {
		
		$user_id 			= $_REQUEST["user_id"];
		$role 				= $_REQUEST["role"];
		$lan 				= $_REQUEST["lan"];
		$device 			= $_REQUEST["device"];
		$device_uid 		= $_REQUEST["device_uid"];
		$school_id 			= $_REQUEST["school_id"];
		$module_id 			= $_REQUEST["module_id"];
		$tbl_class_id 		= $_REQUEST["tbl_class_id"];
		$issue_cards 		= $_REQUEST["issue_cards"];
		$performance_calc 	= $_REQUEST["performance_calc"];
		$progress_report 	= $_REQUEST["progress_report"];
		
		$data["user_id"] 			= $user_id;
		$data["role"] 				= $role;
		$data["lan"] 				= $lan;
		$data["device"] 			= $device;
		$data["device_uid"] 		= $device_uid;
		$data["school_id"] 			= $school_id;
		$data["module_id"] 			= $module_id;
		$data["tbl_class_id"] 		= $tbl_class_id;
		$data["issue_cards"] 		= $issue_cards;
		$data["performance_calc"] 	= $performance_calc;
		$data["progress_report"] 	= $progress_report;
		
		$this->load->model('model_student');
		// When role as student
		$data_rs = $this->model_student->get_student_list($tbl_class_id, $school_id, $user_id);
		
		
		for($j=0;$j<count($data_rs);$j++)
		{
			$tbl_student_id  = $data_rs[$j]['tbl_student_id'];
			$leave_today = $this->model_student->student_leave_today($tbl_student_id,$tbl_school_id);
			if(!empty($leave_today))
			{
				$data_rs[$j]['leave_request'] = 'Y';
				$data_rs[$j]['leave_reason']  = $leave_today[0]['comments_parent'];
			}else{
				$data_rs[$j]['leave_request'] = 'N';
				$data_rs[$j]['leave_reason']  = '';
			}
			
			$getCards  = $this->model_student->get_total_issued_cards_student($tbl_class_id, $tbl_student_id, $school_id);
			
			$data_rs[$j]['cntCards'] = isset($getCards[0]['cntCards'])? $getCards[0]['cntCards']:'0' ;
			
			$getPoints = $this->model_student->get_total_issued_points_student($tbl_class_id, $tbl_student_id, $school_id);
			$data_rs[$j]['cntPoints'] = isset($getPoints[0]['totPoints'])? $getPoints[0]['totPoints']:'0' ;
			
			$getAbsent = $this->model_student->get_total_student_absent($tbl_class_id, $tbl_student_id, $school_id);
			$data_rs[$j]['cntAbsent'] = isset($getAbsent[0]['totAbsent'])? $getAbsent[0]['totAbsent']:'0' ;
			
		}
		
		//print_r($data_rs); exit;
		if (count($data_rs) <=0) {
			$arr["code"] = "200";
			echo json_encode($arr);
			return;
		}
		
		if ($module_id == "cdrpt") {
			$data["page"] = "view_student_list_cdrpt";
		} else {
			$data["page"] = "view_student_list";
		}
		
		//$data['data_rs'] = $data_rs;
		//$this->load->view('admin/view_teacher_student_list_cdrpt',$data);
		//return;
		
		//Teacher admin
		$data["contact_parents"] = $_GET["contact_parents"];			// Y or empty
		if (trim($_POST["is_admin"]) == "Y") {	
		
			$data['data_rs'] = $data_rs;			
			if ($module_id == "cdrpt") {
				$this->load->view('admin/view_teacher_student_list_cdrpt',$data);
				return;
			}
			$this->load->view('admin/view_teacher_student_list',$data);
			return;
		}			
		
		$data["data_rs"] = $data_rs;
		$this->load->view('view_template',$data);	
	}


	/**
	* @desc    Register absent
	*
	* @param   none
	* @access  default
	*/
	function register_absent() {
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		
		$tbl_teacher_id = $_REQUEST["tbl_teacher_id"];
		$tbl_class_id = $_REQUEST["tbl_class_id"];
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		
		$this->load->model('model_student');
		$this->model_student->register_absent($tbl_teacher_id, $tbl_class_id, $tbl_student_id);
		
		$stu_obj = $this->model_student->get_student_obj($tbl_student_id);
		$first_name_stu = $stu_obj[0]['first_name'];
		$last_name_stu = $stu_obj[0]['last_name'];
		
		$this->load->model('model_user_notify_token');
		$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_parent_id);
		
		for ($b=0; $b<count($data_tkns); $b++) {
			//Send push message
			if (trim(ENABLE_PUSH) == "Y") {
				$tbl_parent_id = get_parent_of_student($tbl_student_id);
				$message = 'Dear Parent, your Child '.$first_name_stu.' '.$last_name_stu.' is absent from school today.';
				$message = urlencode($message);
				
				//$token = $tbl_parent_id."-".$data_tkns[$b]["token"];
				$token = $data_tkns[$b]["token"];
				$device = $data_tkns[$b]["device"];
						//$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$tbl_parent_id."&msg=".$message."&data=";
				$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$token."&msg=".$message."&data=";
				
				$this->load->model('model_user_notify_token');
				$this->model_user_notify_token->send_notification($token , $message, $device);
				//echo $url;
				//$ch = curl_init();
				//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				//curl_setopt($ch, CURLOPT_URL, $url);
				//curl_setopt($ch, CURLOPT_HEADER, 0);
				//curl_exec($ch);
				//curl_close($ch);
			}//if (trim(ENABLE_PUSH) == "Y")
		}
		echo "--ok--";
	}


	/**
	* @desc    Total points
	*
	* @param   none
	* @access  default
	*/ 
	function cancel_card() {
		$user_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		$tbl_class_id = $_REQUEST["tbl_class_id"];
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		$card_type = $_REQUEST["card_type"];
		$tbl_card_id = $_REQUEST["tbl_card_id"];
		$message = $_REQUEST["message"];
		$this->load->model('model_student');
		$cardPointInfo = $this->model_student->card_added_point($tbl_card_id);
		$card_point = abs($cardPointInfo[0]['card_point']);
		$tbl_teacher_id = $user_id ;
		$current_date = date("Y-m-d");
		$getSemesterId = $this->model_student->get_semester_id($school_id,$current_date);
		$tbl_semester_id = $getSemesterId[0]['tbl_semester_id'];
		// cancel card
		$this->model_student->issue_cancel_card($tbl_card_id, $tbl_teacher_id, $tbl_class_id, $tbl_student_id, $card_type, $message, 'cancel', $card_point,  $school_id, $tbl_semester_id);
        //$this->model_student->cancel_card($tbl_card_id); //delete
		$stu_obj = $this->model_student->get_student_obj($tbl_student_id);
		$first_name_stu = $stu_obj[0]['first_name'];
		$last_name_stu = $stu_obj[0]['last_name'];
		$this->load->model('model_parents');
		$tbl_parent_id = $this->model_parents->get_parent_of_student($tbl_student_id);		
		$this->load->model('model_user_notify_token');
		$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_parent_id);
		for ($b=0; $b<count($data_tkns); $b++) {
			//Send push message
			if (trim(ENABLE_PUSH) == "Y") {
				//$message = 'Dear Parent, teacher has issued '.ucfirst($card_type).' card to your child '.$first_name_stu.' '.$last_name_stu;
				$message = 'Dear Parent, teacher has cancelled card from your child '.$first_name_stu.' '.$last_name_stu;
				//$message = urlencode($message);
				//$token = $tbl_parent_id."-".$data_tkns[$b]["token"];
				$token = $data_tkns[$b]["token"];
                $device = $data_tkns[$b]["device"];
				//$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$tbl_parent_id."&msg=".$message."&data=";
				$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$token."&msg=".$message."&data=";
				$this->load->model('model_user_notify_token');
				$this->model_user_notify_token->send_notification($token , $message, $device);//echo $url;
				//$ch = curl_init();
				//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				//curl_setopt($ch, CURLOPT_URL, $url);
				//curl_setopt($ch, CURLOPT_HEADER, 0);
				//curl_exec($ch);
				//curl_close($ch);
			}//if (trim(ENABLE_PUSH) == "Y")
		}
		$arr["code"] = "200";
		echo json_encode($arr);
	}


	function issue_card_old() {
		$user_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		$tbl_class_id = $_REQUEST["tbl_class_id"];
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		$card_type = $_REQUEST["card_type"];
		$message = $_REQUEST["message"];
		$comment = $_REQUEST["comment"];
		$card_issue_type = $_REQUEST["card_issue_type"];
		$data["user_id"] = $user_id;
		$data["role"] = $role;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] = $school_id;
		$data["tbl_class_id"] = $tbl_class_id;
		$data["tbl_student_id"] = $tbl_student_id;
		$data["card_type"] = $card_type;
		$data["message"] = $message;
		$data["comment"] = $comment;
		$data["card_issue_type"] = $card_issue_type;
		
		$tbl_teacher_id = $user_id;
		$tbl_school_id = $school_id;
		$current_date = date("Y-m-d");
		$this->load->model('model_student');
		$getSemesterId = $this->model_student->get_semester_id($school_id,$current_date);
		//print_r($getSemesterId); exit;
		$tbl_semester_id = $getSemesterId[0]['tbl_semester_id'];
		$this->model_student->issue_card($tbl_teacher_id, $tbl_class_id, $tbl_student_id, $card_type, $message, $card_issue_type, $school_id, $tbl_semester_id);
		
		$stu_obj = $this->model_student->get_student_obj($tbl_student_id);
		$first_name_stu = $stu_obj[0]['first_name'];
		$last_name_stu = $stu_obj[0]['last_name'];
		
		$this->load->model('model_parents');
		$tbl_parent_id = $this->model_parents->get_parent_of_student($tbl_student_id);		
		
		$this->load->model('model_user_notify_token');
		$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_parent_id);
		
		for ($b=0; $b<count($data_tkns); $b++) {
			//Send push message
			if (trim(ENABLE_PUSH) == "Y") {
				//$message = 'Dear Parent, teacher has issued '.ucfirst($card_type).' card to your child '.$first_name_stu.' '.$last_name_stu;
				$message = 'Dear Parent, teacher has issued card to your child '.$first_name_stu.' '.$last_name_stu;
				$message = urlencode($message);
				//$token = $tbl_parent_id."-".$data_tkns[$b]["token"];
				$token = $data_tkns[$b]["token"];
						$device = $data_tkns[$b]["device"];
		
		
						//$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$tbl_parent_id."&msg=".$message."&data=";
				$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$token."&msg=".$message."&data=";
		
			$this->load->model('model_user_notify_token');
				$this->model_user_notify_token->send_notification($token , $message, $device);//echo $url;
				//$ch = curl_init();
				//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				//curl_setopt($ch, CURLOPT_URL, $url);
				//curl_setopt($ch, CURLOPT_HEADER, 0);
				//curl_exec($ch);
				//curl_close($ch);
			}//if (trim(ENABLE_PUSH) == "Y")
		}
		$arr["code"] = "200";
		echo json_encode($arr);
	}


   function issue_card() {
		$user_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		$tbl_class_id = $_REQUEST["tbl_class_id"];
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		$card_type = $_REQUEST["card_type"];
		$message = $_REQUEST["message"];
		$comment = $_REQUEST["comment"];
		$card_issue_type = $_REQUEST["card_issue_type"];
		$data["user_id"] = $user_id;
		$data["role"] = $role;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] = $school_id;
		$data["tbl_class_id"] = $tbl_class_id;
		$data["tbl_student_id"] = $tbl_student_id;
		$data["card_type"] = $card_type;
		$data["message"] = $message;
		$data["comment"] = $comment;
		$data["card_issue_type"] = $card_issue_type;
		$tbl_teacher_id = $user_id;
		$current_date = date("Y-m-d");
		$this->load->model('model_student');
		$getSemesterId = $this->model_student->get_semester_id($school_id,$current_date);
        //print_r($getSemesterId); exit;
		$tbl_semester_id = $getSemesterId[0]['tbl_semester_id'];
		/************** new codes for point ***************/
		 $rs_cards_semester = $this->model_student->get_cards_semester($tbl_student_id, $tbl_semester_id, $school_id, $card_type);
		 for ($i=0; $i<count($rs_cards_semester); $i++) {
			$cnt 				= $rs_cards_semester[$i]['cnt'];
			$card_type 			= $rs_cards_semester[$i]['card_type'];
			$card_issue_type    = $rs_cards_semester[$i]['card_issue_type'];
			if($card_issue_type=="issue"){
				$cardData[$card_type]['issue'] = $cnt;
				$cardData[$card_type]['card_type'] = $card_type;
			}else{
				$cardData[$card_type]['cancel'] = $cnt;
				$cardData[$card_type]['card_type'] = $card_type;
			}
		}
		$total_points = 0;
		$k=0;
		$cardCntIssued  = 0;
		$nextCard = 0;
		foreach($cardData as $key=>$value)
		{ 
			$cardPoint	    = 0;
			$issueCntCard 	= isset($value['issue'])? $value['issue'] : '0';
			$cancelCntCard 	= isset($value['cancel'])? $value['cancel'] : '0';
			$cardCntIssued  = abs($issueCntCard) - abs($cancelCntCard);
			$cardDet[$k]['id'] 			= $value['card_type'];
			$cardDet[$k]['card_type'] 	= $value['card_type'];
			$cardDet[$k]['issue'] 		= $issueCntCard;
		    $cardDet[$k]['cancel'] 		= $cancelCntCard;
			$cardDet[$k]['issued'] 		= $cardCntIssued;
		}
		$nextCard = $cardCntIssued + 1 ; 
		$rs_cards_points = $this->model_student->get_cards_points($school_id, $nextCard);	
		if($rs_cards_points[0]['card_point']<>""){	
			$card_point	=	$rs_cards_points[0]['card_point'];
		}else{
			$card_point	=	'-1';
		}
		/*************** end card point *****************************/
        $issued_type = "issue"; 
		$this->model_student->issue_card($tbl_teacher_id, $tbl_class_id, $tbl_student_id, $card_type, $message, $issued_type, $card_point, $school_id, $tbl_semester_id);
		$stu_obj = $this->model_student->get_student_obj($tbl_student_id);
		$first_name_stu = $stu_obj[0]['first_name'];
		$last_name_stu = $stu_obj[0]['last_name'];
		$this->load->model('model_parents');
		$tbl_parent_id = $this->model_parents->get_parent_of_student($tbl_student_id);		
		$this->load->model('model_user_notify_token');
	    $data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_parent_id);
		
		for ($b=0; $b<count($data_tkns); $b++) {
			//Send push message
			if (trim(ENABLE_PUSH) == "Y") {
				//$message = 'Dear Parent, teacher has issued '.ucfirst($card_type).' card to your child '.$first_name_stu.' '.$last_name_stu;
				$message = 'Dear Parent, teacher has issued card to your child '.$first_name_stu.' '.$last_name_stu;
				//$message = urlencode($message);
				//$token = $tbl_parent_id."-".$data_tkns[$b]["token"];
				$token = $data_tkns[$b]["token"]; 
				$device = $data_tkns[$b]["device"];
				
				//$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$tbl_parent_id."&msg=".$message."&data=";
				$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$token."&msg=".$message."&data=";
				$this->load->model('model_user_notify_token');
	            $this->model_user_notify_token->send_notification($token , $message, $device);//echo $url;
				//$ch = curl_init();
				//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				//curl_setopt($ch, CURLOPT_URL, $url);
				//curl_setopt($ch, CURLOPT_HEADER, 0);
				//curl_exec($ch);
				//curl_close($ch);
			}//if (trim(ENABLE_PUSH) == "Y")
		}
		$arr["code"] = "200";
		echo json_encode($arr);
	}

	/**
	* @desc    Total cards
	*
	* @param   none
	* @access  default
	*/ 
    // save points to single student
    function save_points() {
		$user_id 				= $_REQUEST["user_id"];
		$role 					= $_REQUEST["role"];
		$lan 					= $_REQUEST["lan"];
		$device 				= $_REQUEST["device"];
		$device_uid 			= $_REQUEST["device_uid"];
		$school_id 				= $_REQUEST["school_id"];
		$tbl_class_id 			= $_REQUEST["tbl_class_id"];
		$tbl_student_id 		= $_REQUEST["tbl_student_id"];
		$tbl_point_category_id 	= $_REQUEST["points_student"];
		$message 				= urldecode($_REQUEST["message"]);
		$data["user_id"] 		= $user_id;
		$data["role"] 			= $role;
		$data["lan"] 			= $lan;
		$data["device"] 		= $device;
		$data["device_uid"] 	= $device_uid;
		$data["school_id"] 		= $school_id;
		$data["tbl_class_id"] 	= $tbl_class_id;
		$tbl_teacher_id 		= $user_id;
		$tbl_school_id 			= $school_id;
		$this->load->model('model_school');
		$dataPoint 				= $this->model_school->getPointById($tbl_point_category_id);
		
		if($lan=="en")
			$message = $dataPoint[0]['point_name_en'];
		else
			$message = $dataPoint[0]['point_name_ar'];
		$points_student			= $dataPoint[0]['behaviour_point'];
		$tbl_student_id_arr 	= explode(":", $tbl_student_id);
		$data["points_student"] = $points_student;
	    $data["message"]        = urldecode($message);
		//$data["tbl_student_id"] = $tbl_student_id;
		$this->load->model('model_student');
		$current_date = date("Y-m-d");
		$getSemesterId = $this->model_student->get_semester_id($school_id,$current_date);
		$tbl_semester_id = $getSemesterId[0]['tbl_semester_id'];
		
		for($k=0;$k<count($tbl_student_id_arr);$k++)
		{
		    $tbl_student_id = $tbl_student_id_arr[$k];
			if($tbl_student_id<>"")
			{
				$this->model_student->save_points($tbl_teacher_id, $tbl_class_id, $tbl_student_id, $points_student, $message, $tbl_school_id, $tbl_point_category_id, $tbl_semester_id);
				$stu_obj = $this->model_student->get_student_obj($tbl_student_id);
				$first_name_stu = $stu_obj[0]['first_name'];
				$last_name_stu = $stu_obj[0]['last_name'];
				$this->load->model('model_parents');
				$tbl_parent_id = $this->model_parents->get_parent_of_student($tbl_student_id);		
				$this->load->model('model_user_notify_token');
				$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_parent_id);
				for ($b=0; $b<count($data_tkns); $b++) {
					//Send push message
					if (trim(ENABLE_PUSH) == "Y") {
						$message_notification = 'Dear Parent, teacher has issued '.$points_student.' point(s) to your child '.$first_name_stu.' '.$last_name_stu;
						//$message = urlencode($message);
						//$token = $tbl_parent_id."-".$data_tkns[$b]["token"];
						$token  = $data_tkns[$b]["token"];
						$device = $data_tkns[$b]["device"];
						//$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$tbl_parent_id."&msg=".$message."&data=";
						$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$token."&msg=".$message_notification."&data=";
						// You can POST a file by prefixing with an @ (for <input type="file"> fields)
						$this->load->model('model_user_notify_token');
						$this->model_user_notify_token->send_notification($token , $message_notification, $device);
						//$ch = curl_init();
						//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
						//curl_setopt($ch, CURLOPT_URL, $url);
						//curl_setopt($ch, CURLOPT_HEADER, 0);
						//curl_exec($ch);
						//curl_close($ch);
					}//if (trim(ENABLE_PUSH) == "Y")				
				}
			}
		}
		$arr["code"] = "200";
		echo json_encode($arr);
	}



	/**
	* @desc    Get student object
	*
	* @param   none
	* @access  default
	*/ 

    function submit_daily_report() {
		$user_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		$report_str = $_REQUEST["report_str"];
		$data["user_id"] = $user_id;
		$data["role"] = $role;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] = $school_id;
		$data["report_str"] = $report_str;
		$tbl_teacher_id = $user_id;
		$tbl_school_id = $school_id;
		// Y : Means tick
		// N : Means cross
		$this->load->model('model_config');
		$data_config = $this->model_config->get_config($tbl_school_id);
		//report_str = report_str + tbl_student_id+":"+option1+","+option2+","+option3+","+option4+";";
		$report_str_arr = explode(";",$report_str);
		for ($i=0; $i<(count($report_str_arr)-1); $i++) {
			$option1_txt = $data_config[0]["option1_txt"];
			$option2_txt = $data_config[0]["option2_txt"];
			$option3_txt = $data_config[0]["option3_txt"];
			$option4_txt = $data_config[0]["option4_txt"];
			$option1_txt_tick = $data_config[0]["option1_txt_tick"];
			$option2_txt_tick = $data_config[0]["option2_txt_tick"];
			$option3_txt_tick = $data_config[0]["option3_txt_tick"];
			$option4_txt_tick = $data_config[0]["option4_txt_tick"];
			$option1_txt = " ".trim($option1_txt);
			$option2_txt = " ".trim($option2_txt);
			$option3_txt = " ".trim($option3_txt);
			$option4_txt = " ".trim($option4_txt);
		$option1_txt_tick = " ".trim($option1_txt_tick);
			$option2_txt_tick = " ".trim($option2_txt_tick);
			$option3_txt_tick = " ".trim($option3_txt_tick);
			$option4_txt_tick = " ".trim($option4_txt_tick);
			$tbl_student_id_arr = explode(":",$report_str_arr[$i]);
			$tbl_student_id = $tbl_student_id_arr[0];
			$tbl_option_id_temp = $tbl_student_id_arr[1];
			$tbl_option_id_str = explode(",",$tbl_option_id_temp);
			$option1 = $tbl_option_id_str[0];
			$option2 = $tbl_option_id_str[1];
			$option3 = $tbl_option_id_str[2];
			$option4 = $tbl_option_id_str[3];
			if ($option1 == "Y") {
				$option1_txt = $option1_txt_tick;
			}
			
			if ($option2 == "Y") {
				$option2_txt = $option2_txt_tick;
			}
			
			if ($option3 == "Y") {
				$option3_txt = $option3_txt_tick;
			}
			
			if ($option4 == "Y") {
				$option4_txt = $option4_txt_tick;
			}
			$option1_txt .= " ,";
			$option2_txt .= " ,";
			$option3_txt .= " ,";
			//$message = $option1_txt.$option2_txt.$option3_txt.$option4_txt;
			$message = $option1_txt.$option2_txt.$option4_txt;
			$message = trim($message).".";
			$this->load->model('model_parents');
			$tbl_parent_id = $this->model_parents->get_parent_of_student($tbl_student_id);
			$this->load->model('model_student');
			$stu_obj = $this->model_student->get_student_obj($tbl_student_id);
			$first_name_stu = $stu_obj[0]['first_name'];
			$last_name_stu = $stu_obj[0]['last_name'];
			$message = 'Daily Report Message: Dear Parent, your Child '.$first_name_stu.' '.$last_name_stu.' '.$message;
			$this->load->model('model_daily_report');
			$this->model_daily_report->save_daily_report($tbl_teacher_id, $tbl_student_id, $tbl_parent_id, $option1, $option2, $option3, $option4, $message, $tbl_school_id);
			$this->load->model('model_user_notify_token');
			$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_parent_id);
			for ($b=0; $b<count($data_tkns); $b++) {			
				if (trim(ENABLE_PUSH) == "Y") {	
					$message = urlencode($message);
					//$token = $tbl_parent_id."-".$data_tkns[$b]["token"];
					$token 	 = $data_tkns[$b]["token"];
					$device 	= $data_tkns[$b]["device"];
					//$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$tbl_parent_id."&msg=".$message."&data=";
					$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$token."&msg=".$message."&data=";
					$this->load->model('model_user_notify_token');
					$message = urldecode($message);
					$this->model_user_notify_token->send_notification($token , $message, $device);
					//$ch = curl_init();
					//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					//curl_setopt($ch, CURLOPT_URL, $url);
					//curl_setopt($ch, CURLOPT_HEADER, 0);
					//curl_exec($ch);
					//curl_close($ch);
				}//if (trim(ENABLE_PUSH) == "Y")
			}
		}
		//echo "--yes--";
		$arr["code"] = "200";
	    echo json_encode($arr);
	}
	
	
	/**
	* @desc    Get children instant snaps as taken by teacher
	*
	* @param   none
	* @access  default
	*/ 
    function get_leave_app_form() {
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		
		$tbl_parent_id = $_REQUEST["tbl_parent_id"];
		$this->load->model('model_parents');
		$rs_children = $this->model_parents->fetch_all_children($tbl_parent_id);
		$data["page"] = "view_leave_app_form";
		$this->load->view('view_template',$data);
	}
	
	
	function get_cards() {
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$login_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$school_id = $_REQUEST["school_id"];
		$tbl_student_id  = $_REQUEST["tbl_student_id"];
		$tbl_semester_id = $_REQUEST["tbl_semester_id"];
		$tbl_school_id = $school_id;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		$data["login_id"] = $login_id;
		$data["role"] = $role;
		$data["school_id"] = $school_id;
		$data["tbl_student_id"] = $tbl_student_id;
		$data["semester_title"] = "";
		$this->load->model('model_student');
		$current_date = date("Y-m-d");
		if($tbl_semester_id=="")
		{
			$getSemesterId = $this->model_student->get_semester_id($school_id,$current_date);
			//print_r($getSemesterId); exit;
			$tbl_semester_id = $getSemesterId[0]['tbl_semester_id'];
			if($lan=="en")
				$semester_title = $getSemesterId[0]['title'];
			else
				$semester_title = $getSemesterId[0]['title_ar'];
		
		   $data["semester_title"] = $semester_title;
		}
  	    $rs_cards = $this->model_student->get_cards($tbl_student_id,$tbl_semester_id);
        //print_r($rs_cards); exit;
		// Cards and their counts	
		$cntCards = count($rs_cards);
		$cntCards = 0;	
		for ($i=0; $i<$cntCards; $i++) {
			$cnt 				= 	$rs_cards[$i]['cnt'];
			$card_type 			= 	$rs_cards[$i]['card_type'];
			$card_issue_type 	= 	$rs_cards[$i]['card_issue_type'];
			$card_point			=	$rs_cards[$i]['card_point'];
			if($card_issue_type=="issue"){
				$cardData[$card_type]['issue'] = $cnt;
				$cardData[$card_type]['card_type'] = $card_type;
				$cardData[$card_type]['card_point'] = $card_point;
			}else{
				$cardData[$card_type]['cancel'] = $cnt;
				$cardData[$card_type]['card_type'] = $card_type;
				$cardData[$card_type]['card_point'] = $card_point;
			}
			$cardDet[$i]['id'] 			= $card_type;
			$cardDet[$i]['card_type'] 	= $card_type;
			$cardDet[$i]['issued'] 		= $cnt;
			$this->load->model('model_config');
			$cardCategoryPoints = $this->model_config->get_cards_categories($tbl_school_id,$card_type);
			$cardDet[$i]['title'] 		= $cardCategoryPoints[0]['category_name_en'];
			$cardDet[$i]['title_ar'] 	= $cardCategoryPoints[0]['category_name_ar'];
			$cardDet[$i]['score']		= $card_point;
			if($cardCategoryPoints[0]['card_logo']<>""){ $cardDet[$i]['image'] = IMG_GALLERY_PATH."/".$cardCategoryPoints[0]['card_logo']; } else{ $cardDet[$i]['image'] = "N"; }
			if($cardCategoryPoints[0]['card_logo']=="")
				$cardDet[$i]['color'] 		= "#".$cardCategoryPoints[0]['color'];
			else
				$cardDet[$i]['color'] 		= "N";
			
			$cardDet[$i]['catTotPoint'] 		= $card_point;
			$total_points += $card_point;
			$k = $k+1;
		}

        /*********** school total mark for card - settings ****************/
		$rs_total_mark_card = $this->model_student->get_total_mark_cards($tbl_school_id);
		$total_card_mark = $rs_total_mark_card[0]['total_points'];
		$rs_student_point 	= $this->model_student->get_all_cards_student($tbl_student_id, $tbl_semester_id);
		//print_r($rs_student_point); exit;
		$total = 0;
		if(!empty($rs_student_point))
		{
		for($m=0;$m<count($rs_student_point);$m++)
		{
			$total =	$total + $rs_student_point[$m]['card_point'];
		}
		}
		$data["total_mark"] 	= $total_card_mark;
		
			//	$data["total_points"] 	= $total_card_mark + $total_points ; 
		
			//	$data["total_points"] 	= $total_card_mark."".$total_points."= ".$data["total_points"] ;
		$total_points  			= $total_card_mark - abs($total);
		$data["total_points"] 	= $total_card_mark." - ".abs($total)." = ".$total_points ;
		
		$data["all_cards_arr"]  = $cardDet;
		$this->load->model('model_message');
		$all_messages_rs = $this->model_message->get_cards_detail_semester($tbl_student_id, $tbl_semester_id);
		$data['all_messages_rs'] = $all_messages_rs;
		/*echo "<pre>";
		print_r($all_messages_rs);
		exit;*/
		//print_r($data); exit;
		$data["page"] = "view_my_cards";
		$this->load->view('view_template',$data);

	}
	
	
	/**
	* @desc    Submit Daily Report
	*
	* @param   none
	* @access  default
	*/
	//url =  host + "/student/submit_daily_report/"+lan+"/"+device+"/"+tbl_teacher_id_sel+"/"+report_str+"/"+rnd;
    function get_card_messages() {
		
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$login_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$school_id = $_REQUEST["school_id"];
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		$card_type = $_REQUEST["card_type"];//e.g. Blue
		$card_type_req = $_REQUEST["card_type"];//e.g. Blue
		$tbl_school_id = $school_id;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["login_id"] = $login_id;
		$data["role"] = $role;
		$data["school_id"] = $school_id;
		$data["tbl_student_id"] = $tbl_student_id;
		$data["card_type"] = $card_type;
		$this->load->model('model_student');

  	    $rs_cards = $this->model_student->get_cards($tbl_student_id);
		// Cards and their counts		
		for ($i=0; $i<count($rs_cards); $i++) {
			$cnt = $rs_cards[$i]['cnt'];
			$card_type = $rs_cards[$i]['card_type'];
			$card_issue_type = $rs_cards[$i]['card_issue_type'];
			
			if($card_issue_type=="issue"){
				$cardData[$card_type]['issue'] = $cnt;
				$cardData[$card_type]['card_type'] = $card_type;
			}else{
				$cardData[$card_type]['cancel'] = $cnt;
				$cardData[$card_type]['card_type'] = $card_type;
			}
		}
	
		$total_points = 0;
		$k=0;
		foreach($cardData as $key=>$value)
		{ 
			$cardPoint	    = 0;
			$cardCntIssued  = 0;
			$issueCntCard 	= isset($value['issue'])? $value['issue'] : '0';
			$cancelCntCard 	= isset($value['cancel'])? $value['cancel'] : '0';
			$cardCntIssued  = $issueCntCard - $cancelCntCard;
			$cardDet[$k]['id'] 			= $value['card_type'];
			$cardDet[$k]['card_type'] 	= $value['card_type'];
			$cardDet[$k]['issue'] 		= $issueCntCard;
			$cardDet[$k]['cancel'] 		= $cancelCntCard;
			$cardDet[$k]['issued'] 		= $cardCntIssued;
				  
			$this->load->model('model_config');
			$cardCategoryPoints = $this->model_config->get_cards_categories($tbl_school_id,$value['card_type']);
			$cardDet[$k]['title'] 		= $cardCategoryPoints[0]['category_name_en'];
			$cardDet[$k]['title_ar'] 	= $cardCategoryPoints[0]['category_name_ar'];
			/*$cardDet[$k]['score']		= $cardCategoryPoints[0]['card_point'];*/
		
			$cardDet[$k]['score']		= $value['card_point'];
			if($cardCategoryPoints[0]['card_logo']<>""){ $cardDet[$k]['image'] = IMG_GALLERY_PATH."/".$cardCategoryPoints[0]['card_logo']; } else{ $cardDet[$k]['image'] = "N"; }
			if($cardCategoryPoints[0]['card_logo']=="")
				$cardDet[$k]['color'] 		= "#".$cardCategoryPoints[0]['color'];
			else
				$cardDet[$k]['color'] 		= "N";
				$cardDet[$k]['card_type'] 	= $value['card_type'];
				$catTotPoint = $cardDet[$k]['score'];
				$cardDet[$k]['catTotPoint'] 		= $catTotPoint;
			$total_points += $catTotPoint;
			$k = $k+1;
			
		}
		$data["total_points"] 	= $total_points; 
		$data["all_cards_arr"]  = $cardDet;
		//print_r($data);
		// echo $tbl_student_id."   ".$card_type_req; exit;
		$this->load->model('model_message');
		$all_messages_rs = $this->model_message->get_card_messages($tbl_student_id, $card_type_req);
		$data['all_messages_rs'] = $all_messages_rs;
		// print_r($all_messages_rs);
		// exit;
		$data["page"] = "view_card_messages";
		$this->load->view('view_template',$data);
	}
	
	
	/**
	* @desc    Get student object
	*
	* @param   none
	* @access  default
	*/ 
	function daily_report() {
		$user_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		
		$data["user_id"] = $user_id;
		$data["role"] = $role;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] = $school_id;
		$data["tbl_student_id"] = $tbl_student_id;
		
		$this->load->model('model_daily_report');
		$data_rs = $this->model_daily_report->get_daily_report($tbl_student_id);
		
		$this->load->model('model_config');
		$config_rs = $this->model_config->get_config($school_id);
		
		if ($lan == "en") {
			$option1_caption = $config_rs[0]["option1_caption"];
			$option2_caption = $config_rs[0]["option2_caption"];
			//$option3_caption = $config_rs[0]["option3_caption"];
			$option4_caption = $config_rs[0]["option4_caption"];
		} else {
			$option1_caption = $config_rs[0]["option1_caption_ar"];
			$option2_caption = $config_rs[0]["option2_caption_ar"];
			//$option3_caption = $config_rs[0]["option3_caption_ar"];
			$option4_caption = $config_rs[0]["option4_caption_ar"];
		}
		
		$data["option1_caption"] = $option1_caption;
		$data["option2_caption"] = $option2_caption;
		//$data["option3_caption"] = $option3_caption;
		$data["option4_caption"] = $option4_caption;
		
		$data["data_rs"] = $data_rs;
		$data["config_rs"] = $config_rs;
		$data["page"] = "view_daily_report";			
		$this->load->view('view_template',$data);
	}


	/**
	* @desc    Daily Report
	*
	* @param   none
	* @access  default
	*/ 

	function get_total_points() {
		
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		$total_points = 0;
		
		$this->load->model('model_student');
		$total_points = $this->model_student->get_total_points($tbl_student_id);
		echo $total_points;	
	}


	function get_negative_issue_cards() {
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$login_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$school_id = $_REQUEST["school_id"];
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		$tbl_class_id = $_REQUEST["tbl_class_id"];
		$tbl_teacher_id = $_REQUEST["tbl_teacher_id"];
		$tbl_school_id = $school_id;
		
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;
		$data["login_id"] = $login_id;
		$data["role"] = $role;
		$data["school_id"] = $school_id;
		$data["tbl_student_id"] = $tbl_student_id;
		$data["tbl_class_id"] = $tbl_class_id;
		$data["tbl_teacher_id"] = $tbl_teacher_id;
		
		$this->load->model('model_student');
		$rs_cards = $this->model_student->get_negative_cards($tbl_student_id,$tbl_class_id,$tbl_teacher_id);
		
		if(count($rs_cards)>0){
			// Cards and their counts		
			for ($i=0; $i<count($rs_cards); $i++) {
				$cnt = $rs_cards[$i]['cnt'];
				$card_type = $rs_cards[$i]['card_type'];
				$card_issue_type = $rs_cards[$i]['card_issue_type'];
				if($card_issue_type=="issue"){
					$cardData[$card_type]['issue'] = $cnt;
					$cardData[$card_type]['card_type'] = $card_type;
				}else{
					$cardData[$card_type]['cancel'] = $cnt;
					$cardData[$card_type]['card_type'] = $card_type;
				}
				$cardData[$card_type]['tbl_card_id'] = $rs_cards[$i]['tbl_card_id'];
			}
			
			$total_points = 0;
			$k=0;
			foreach($cardData as $key=>$value)
			{ 
				
				$cardPoint	    = 0;
				$cardCntIssued  = 0;
				$issueCntCard 	= isset($value['issue'])? $value['issue'] : '0';
				$cancelCntCard 	= isset($value['cancel'])? $value['cancel'] : '0';
				$cardCntIssued  = $issueCntCard - $cancelCntCard;
				$cardDet[$k]['id'] 			= $value['tbl_card_id'];
				$cardDet[$k]['card_type'] 	= $value['card_type'];
				$cardDet[$k]['issue'] 		= $issueCntCard;
				$cardDet[$k]['cancel'] 		= $cancelCntCard;
				$cardDet[$k]['issued'] 		= $cardCntIssued;
			  
				$this->load->model('model_config');
				$cardCategoryPoints = $this->model_config->get_cards_categories($tbl_school_id,$value['card_type']);
				$cardDet[$k]['title'] 		= $cardCategoryPoints[0]['category_name_en'];
				$cardDet[$k]['title_ar'] 	= $cardCategoryPoints[0]['category_name_ar'];
				$cardDet[$k]['score']		= $cardCategoryPoints[0]['card_point'];
				if($cardCategoryPoints[0]['card_logo']<>""){ $cardDet[$k]['image'] = IMG_GALLERY_PATH."/".$cardCategoryPoints[0]['card_logo']; } else{ $cardDet[$k]['image'] = "N"; }
				if($cardCategoryPoints[0]['card_logo']=="")
					$cardDet[$k]['color'] 		= "#".$cardCategoryPoints[0]['color'];
				else
					$cardDet[$k]['color'] 		= "N";
				//$data['config_rs'] = $config_rs;
				$catTotPoint = $cardCntIssued * $cardCategoryPoints[0]['card_point'];
				$cardDet[$k]['catTotPoint'] 		= $catTotPoint;
				$total_points += $catTotPoint;
				$k = $k+1;
				
			}
			$data["total_points"] 	= $total_points; 
			$data["all_cards_arr"]  = $cardDet;
			$this->load->model('model_message');
			$all_messages_rs = $this->model_message->get_card_messages($tbl_student_id, $card_type);
			$data['all_messages_rs'] = $all_messages_rs;
		}else{
			$data["total_points"] 	= '';
			$data["all_cards_arr"]  = array();
			$data['all_messages_rs']= array();
			
		}
		/*echo "<pre>";
		print_r($all_messages_rs);
		exit;*/
		//print_r($data); exit;
		$data["page"] = "view_my_cards";
		$this->load->view('view_template',$data);
	}


	function get_behaviour_points() {
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["tbl_school_id"];
		$login_id = $_REQUEST["user_id"];
		
		$this->load->model('model_school');
		$rs_points = $this->model_school->get_behaviour_points($tbl_school_id);
		
		$ministry_points = array();
		$positive_points = array();
		$negative_points = array();
		$m =0;
		$p =0;
		$n =0;
		for($s=0;$s<count($rs_points); $s++)
		{
			$tbl_school_id = $rs_points[$s]['tbl_school_id'];
			$behaviour_point = $rs_points[$s]['behaviour_point'];
				if($tbl_school_id=="")
				{
					$ministry_points[$m]['id']	 					= $rs_points[$s]['id'];
					$ministry_points[$m]['tbl_point_category_id'] 	= $rs_points[$s]['tbl_point_category_id'];
					$ministry_points[$m]['point_name_en'] 			= $rs_points[$s]['point_name_en'];
					$ministry_points[$m]['point_name_ar'] 			= $rs_points[$s]['point_name_ar'];
					$ministry_points[$m]['behaviour_point'] 		= $rs_points[$s]['behaviour_point'];
					$ministry_points[$m]['point_category_order'] 	= $rs_points[$s]['point_category_order'];
					$ministry_points[$m]['is_active'] 				= $rs_points[$s]['is_active'];
					$ministry_points[$m]['added_date'] 				= $rs_points[$s]['added_date'];
					$ministry_points[$m]['tbl_school_id'] 			= $rs_points[$s]['tbl_school_id'];
					$m = $m+1;
				}else{
					
						if($behaviour_point<0)
						{
							$negative_points[$n]['id']	 					= $rs_points[$s]['id'];
							$negative_points[$n]['tbl_point_category_id'] 	= $rs_points[$s]['tbl_point_category_id'];
							$negative_points[$n]['point_name_en'] 			= $rs_points[$s]['point_name_en'];
							$negative_points[$n]['point_name_ar'] 			= $rs_points[$s]['point_name_ar'];
							$negative_points[$n]['behaviour_point'] 		= $rs_points[$s]['behaviour_point'];
							$negative_points[$n]['point_category_order'] 	= $rs_points[$s]['point_category_order'];
							$negative_points[$n]['is_active'] 				= $rs_points[$s]['is_active'];
							$negative_points[$n]['added_date'] 				= $rs_points[$s]['added_date'];
							$negative_points[$n]['tbl_school_id'] 			= $rs_points[$s]['tbl_school_id'];
							$n = $n+1;
							
						}else{
							$positive_points[$p]['id']	 					= $rs_points[$s]['id'];
							$positive_points[$p]['tbl_point_category_id'] 	= $rs_points[$s]['tbl_point_category_id'];
							$positive_points[$p]['point_name_en'] 			= $rs_points[$s]['point_name_en'];
							$positive_points[$p]['point_name_ar'] 			= $rs_points[$s]['point_name_ar'];
							$positive_points[$p]['behaviour_point'] 		= $rs_points[$s]['behaviour_point'];
							$positive_points[$p]['point_category_order'] 	= $rs_points[$s]['point_category_order'];
							$positive_points[$p]['is_active'] 				= $rs_points[$s]['is_active'];
							$positive_points[$p]['added_date'] 				= $rs_points[$s]['added_date'];
							$positive_points[$p]['tbl_school_id'] 			= $rs_points[$s]['tbl_school_id'];
							$p = $p+1;
						}
				}
		}
		
		
		$data["points_arr"]['ministry_points']  = $ministry_points;
		$data["points_arr"]['negative_points']  = $negative_points;
		$data["points_arr"]['positive_points']  = $positive_points;
		
		$response = "Success";
		$errorMsg = "Points listed successfully";
		$response_array = array('response' => $response, 'result' => $data, 'errorMsg' => $errorMsg);
		
		//Teacher admin
		if (trim($_POST["is_admin"]) == "Y") {
			$data['data_rs'] = $data_rs;
			$data['student_name'] = $_POST["student_name"];
			$this->load->view('admin/view_teacher_behaviour_points',$data);
			return;
		}	
		
		echo json_encode($response_array);
		exit;
	}
	
	
	function get_student_info() {
		$lan 			= $_REQUEST["lan"];
		$device 		= $_REQUEST["device"];
		$device_uid 	= $_REQUEST["device_uid"];
		$tbl_school_id 	= $_REQUEST["tbl_school_id"];
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		$this->load->model('model_student');
		
		$data = $this->model_student->get_student_obj($tbl_student_id); 
		$id = $data[0]["id"];
		$tbl_teacher_id = $data[0]["tbl_teacher_id"];
		$first_name = $data[0]["first_name"];
		$first_name_ar = $data[0]["first_name_ar"];
		$last_name = $data[0]["last_name"];
		$last_name_ar = $data[0]["last_name_ar"];
		$mobile = $data[0]["mobile"];
		$dob_month = $data[0]["dob_month"];
		$dob_day = $data[0]["dob_day"];
		$dob_year = $data[0]["dob_year"];
		$gender = $data[0]["gender"];
		$email = $data[0]["email"];
		$tbl_class_id = $data[0]["tbl_class_id"];
		$file_name_updated = $data[0]["file_name_updated"];
		$added_date = $data[0]["added_date"];
		$is_active = $data[0]["is_active"];
		$emirates_id_father = $data[0]["emirates_id_father"];
		$emirates_id_mother = $data[0]["emirates_id_mother"];
		$this->load->model('model_classes');
		$rs_class = $this->model_classes->getClassInfo($tbl_class_id);
		$class_info = array();
		$tbl_section_id = $rs_class[0]["tbl_section_id"];
		$class_name 	= isset($rs_class[0]["class_name"])? $rs_class[0]["class_name"] : '' ;
		$class_name_ar 	= isset($rs_class[0]["class_name_ar"])? $rs_class[0]["class_name_ar"] : '' ;
		$data[0]['class_name'] = $class_name;
		$data[0]['class_name_ar'] = $class_name_ar;
		$data_se = $this->model_classes->getClassSectionInfo($tbl_section_id);
		$section_info = array();
		$section_name =  isset($data_se[0]["section_name"])? $data_se[0]["section_name"]:'';
		$section_name_ar =  isset($data_se[0]["section_name_ar"])? $data_se[0]["section_name_ar"]:'';
		$data[0]['section_name']    = $section_name;
		$data[0]['section_name_ar'] = $section_name;
		$this->load->model('model_parents');
		$tbl_parent_id = $this->model_parents->get_parent_of_student($tbl_student_id);	
		$parentDetails = $this->model_parents->get_parent_obj($tbl_parent_id);
		$data[0]['parent_first_name'] 			= isset($parentDetails[0]['first_name'])? $parentDetails[0]['first_name'] : '' ;
		$data[0]['parent_first_name_ar']     	= isset($parentDetails[0]['first_name_ar'])? $parentDetails[0]['first_name_ar'] : '' ;
		$data[0]['parent_last_name'] 			= isset($parentDetails[0]['last_name'])? $parentDetails[0]['last_name'] :'' ;
		$data[0]['parent_last_name_ar'] 		= isset($parentDetails[0]['last_name_ar'])? $parentDetails[0]['last_name_ar'] :'' ;
		$data[0]['parent_mobile'] 			    = isset($parentDetails[0]['mobile'])? $parentDetails[0]['mobile'] : '' ;
		$data[0]['parent_email'] 			    = isset($parentDetails[0]['email'])? $parentDetails[0]['email'] : '' ;
		$data[0]['parent_gender'] 			    = isset($parentDetails[0]['gender'])? $parentDetails[0]['gender'] : '' ; 
		
		$getCards  = $this->model_student->get_total_issued_cards_student($tbl_class_id, $tbl_student_id, $tbl_school_id);
		$data[0]['cntCards'] = isset($getCards[0]['cntCards'])? $getCards[0]['cntCards']:'0' ;
			
		$getPoints = $this->model_student->get_total_issued_points_student($tbl_class_id, $tbl_student_id, $tbl_school_id);
		$data[0]['cntPoints'] = isset($getPoints[0]['totPoints'])? $getPoints[0]['totPoints']:'0' ;
			
		$getAbsent = $this->model_student->get_total_student_absent($tbl_class_id, $tbl_student_id, $tbl_school_id);
		$data[0]['cntAbsent'] = isset($getAbsent[0]['totAbsent'])? $getAbsent[0]['totAbsent']:'0' ;
		
		$student_data['personal_info'] = $data;
		$response = "Success";
		$errorMsg = "Student Information";
		$response_array = array('response' => $response, 'result' => $student_data, 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
	}


	function get_card_list_by_type() {
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$login_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$school_id = $_REQUEST["school_id"];
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		$card_type = $_REQUEST["card_type"];//e.g. Blue
		$card_type_req = $_REQUEST["card_type"];//e.g. Blue
		$tbl_school_id = $school_id;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["login_id"] = $login_id;
		$data["role"] = $role;
		$data["school_id"] = $school_id;
		$data["tbl_student_id"] = $tbl_student_id;
		$data["card_type"] = $card_type;
		$data["role"] = $role;
		$this->load->model('model_student');
		$current_date = date("Y-m-d");
		$getSemesterId = $this->model_student->get_semester_id($school_id,$current_date);
		$tbl_semester_id = $getSemesterId[0]['tbl_semester_id'];

		$rs_cards = $this->model_student->get_all_cards_student($tbl_student_id, $tbl_semester_id, $card_type);
		// Cards and their counts		
			for ($i=0; $i<count($rs_cards); $i++) {
			$cnt = $rs_cards[$i]['cnt'];
			$card_type = $rs_cards[$i]['card_type'];
			$card_issue_type = $rs_cards[$i]['card_issue_type'];
			if($card_issue_type=="issue"){
				$cardData[$card_type]['issue'] = $cnt;
				$cardData[$card_type]['card_type'] = $card_type;
			}else{
				$cardData[$card_type]['cancel'] = $cnt;
				$cardData[$card_type]['card_type'] = $card_type;
			}
		}
		$total_points = 0;
		$k=0;
		foreach($cardData as $key=>$value)
		{
		$cardPoint	    = 0;
		$cardCntIssued  = 0;
		$issueCntCard 	= isset($value['issue'])? $value['issue'] : '0';
		$cancelCntCard 	= isset($value['cancel'])? $value['cancel'] : '0';
		$cardCntIssued  = $issueCntCard - $cancelCntCard;
		$cardDet[$k]['id'] 			= $value['card_type'];
		$cardDet[$k]['card_type'] 	= $value['card_type'];
		$cardDet[$k]['issue'] 		= $issueCntCard;
		$cardDet[$k]['cancel'] 		= $cancelCntCard;
		$cardDet[$k]['issued'] 		= $cardCntIssued;
		$this->load->model('model_config');
		$cardCategoryPoints = $this->model_config->get_cards_categories($tbl_school_id,$value['card_type']);
		$cardDet[$k]['title'] 		= $cardCategoryPoints[0]['category_name_en'];
		$cardDet[$k]['title_ar'] 	= $cardCategoryPoints[0]['category_name_ar'];
		/*$cardDet[$k]['score']		= $cardCategoryPoints[0]['card_point'];*/
		$cardDet[$k]['score']		= $value['card_point'];
		if($cardCategoryPoints[0]['card_logo']<>""){ $cardDet[$k]['image'] = IMG_GALLERY_PATH."/".$cardCategoryPoints[0]['card_logo']; } else{ $cardDet[$k]['image'] = "N"; }
		if($cardCategoryPoints[0]['card_logo']=="")
			$cardDet[$k]['color'] 		= "#".$cardCategoryPoints[0]['color'];
		else
			$cardDet[$k]['color'] 		= "N";
			
		$cardDet[$k]['card_type'] 	= $value['card_type'];
		//$data['config_rs'] = $config_rs;
			//$catTotPoint = $cardCntIssued * $cardCategoryPoints[0]['card_point'];
		
		$catTotPoint = $cardDet[$k]['score'];
		$cardDet[$k]['catTotPoint'] 		= $catTotPoint;
		$total_points += $catTotPoint;
		$k = $k+1;
		}
		$data["total_points"] 	= $total_points;
		$data["all_cards_arr"]  = $cardDet;
		$this->load->model('model_message');
		$this->load->model('model_teachers');
		$all_messages_rs = $this->model_message->get_non_cancel_cards($tbl_student_id, $tbl_semester_id, $card_type_req);
		for($n=0;$n<count($all_messages_rs);$n++)
		{
		$negative_card_point = "".abs($all_messages_rs[$n]['card_point']);
		if(abs($negative_card_point)<=0){
					$all_messages_rs[$n]['image'] = "N";
					$all_messages_rs[$n]['color'] =  "0a7429";
				}else if(abs($negative_card_point)==1){
					$all_messages_rs[$n]['image'] = "N";
					$all_messages_rs[$n]['color'] 	=  "faf707";
				}else if(abs($negative_card_point) >=2){
					$all_messages_rs[$n]['image'] = "N";
					$all_messages_rs[$n]['color'] 	=  "fa2307";
				}
		$teacher_obj = "";		
		$tbl_teacher_id = $all_messages_rs[$n]['tbl_teacher_id'];
		$teacher_obj = $this->model_teachers->get_teachers_obj($tbl_teacher_id);
		$first_name = $teacher_obj['first_name'];	
		$last_name = $teacher_obj['last_name'];
			
		if (trim($lan) == "ar") {
			$first_name = $teacher_obj['first_name_ar'];	
			$last_name = $teacher_obj['last_name_ar'];
		}
		$all_messages_rs[$n]['name'] 	=	$first_name." ".$last_name;	
		
		/*$added_date = $all_messages_rs[$n]['added_date'];
		$added_date = date("H:i - d/m/Y",strtotime($all_messages_rs[$n]["added_date"]));
		$all_messages_rs[$n]['added_date'] 	= $added_date;*/
		}
		$data['all_messages_rs'] = $all_messages_rs;
		
		//Teacher admin
		if (trim($_POST["is_admin"]) == "Y") {
			$this->load->view('admin/view_card_list_by_type',$data);
			return;
		}
				// echo "<pre>";
		echo json_encode($data);
		/*$data["page"] = "view_card_messages";
		$this->load->view('view_template',$data);*/
	}

	function get_cancelled_cards_student() {
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$login_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$school_id = $_REQUEST["school_id"];
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		$card_type = $_REQUEST["card_type"];//e.g. Blue
		$card_type_req = $_REQUEST["card_type"];//e.g. Blue
		$tbl_school_id = $school_id;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["login_id"] = $login_id;
		$data["role"] = $role;
		$data["school_id"] = $school_id;
		$data["tbl_student_id"] = $tbl_student_id;
		$data["card_type"] = $card_type;
		$data["role"] = $role;
		$this->load->model('model_student');
		$current_date = date("Y-m-d");
		$getSemesterId = $this->model_student->get_semester_id($school_id,$current_date);
		$tbl_semester_id = $getSemesterId[0]['tbl_semester_id'];
		
		$rs_cards = $this->model_student->get_all_cancel_cards_student($tbl_student_id, $tbl_semester_id, $card_type);
		// Cards and their counts		
		for ($i=0; $i<count($rs_cards); $i++) {
			$cnt = $rs_cards[$i]['cnt'];
			$card_type = $rs_cards[$i]['card_type'];
			$card_issue_type = $rs_cards[$i]['card_issue_type'];
			if($card_issue_type=="issue"){
				$cardData[$card_type]['issue'] = $cnt;
				$cardData[$card_type]['card_type'] = $card_type;
			}else{
				$cardData[$card_type]['cancel'] = $cnt;
				$cardData[$card_type]['card_type'] = $card_type;
			}
		}
		$total_points = 0;
		$k=0;
		foreach($cardData as $key=>$value) { 
			$cardPoint	    = 0;
			$cardCntIssued  = 0;
			$issueCntCard 	= isset($value['issue'])? $value['issue'] : '0';
			$cancelCntCard 	= isset($value['cancel'])? $value['cancel'] : '0';
			$cardCntIssued  = $issueCntCard - $cancelCntCard;
			$cardDet[$k]['id'] 			= $value['card_type'];
			$cardDet[$k]['card_type'] 	= $value['card_type'];
			$cardDet[$k]['issue'] 		= $issueCntCard;
			$cardDet[$k]['cancel'] 		= $cancelCntCard;
			$cardDet[$k]['issued'] 		= $cardCntIssued;
		
			$this->load->model('model_config');
			$cardCategoryPoints = $this->model_config->get_cards_categories($tbl_school_id,$value['card_type']);
			$cardDet[$k]['title'] 		= $cardCategoryPoints[0]['category_name_en'];
			$cardDet[$k]['title_ar'] 	= $cardCategoryPoints[0]['category_name_ar'];
			/*$cardDet[$k]['score']		= $cardCategoryPoints[0]['card_point'];*/
		
			$cardDet[$k]['score']		= $value['card_point'];
			if($cardCategoryPoints[0]['card_logo']<>""){ $cardDet[$k]['image'] = IMG_GALLERY_PATH."/".$cardCategoryPoints[0]['card_logo']; } else{ $cardDet[$k]['image'] = "N"; }
			if($cardCategoryPoints[0]['card_logo']=="")
				$cardDet[$k]['color'] 		= "#".$cardCategoryPoints[0]['color'];
			else
				$cardDet[$k]['color'] 		= "N";
			
			$cardDet[$k]['card_type'] 	= $value['card_type'];
			//$data['config_rs'] = $config_rs;
			//$catTotPoint = $cardCntIssued * $cardCategoryPoints[0]['card_point'];
			
						$catTotPoint = $cardDet[$k]['score'];
			$cardDet[$k]['catTotPoint'] 		= $catTotPoint;
			$total_points += $catTotPoint;
			$k = $k+1;
		}
		$data["total_points"] 	= $total_points;
		$data["all_cards_arr"]  = $cardDet;
		$this->load->model('model_message');
		$this->load->model('model_teachers');
		$all_messages_rs = $this->model_message->get_cancelled_cards($tbl_student_id, $tbl_semester_id, $card_type_req);
		for($n=0;$n<count($all_messages_rs);$n++) {
			$negative_card_point = "".abs($all_messages_rs[$n]['card_point']);
			if(abs($negative_card_point)<=0){
			$all_messages_rs[$n]['image'] = "N";
			$all_messages_rs[$n]['color'] =  "0a7429";
			}else if(abs($negative_card_point)==1){
			$all_messages_rs[$n]['image'] = "N";
			$all_messages_rs[$n]['color'] 	=  "faf707";
			}else if(abs($negative_card_point) >=2){
			$all_messages_rs[$n]['image'] = "N";
			$all_messages_rs[$n]['color'] 	=  "fa2307";
			}
			$teacher_obj = "";		
			$tbl_teacher_id = $all_messages_rs[$n]['tbl_teacher_id'];
			$teacher_obj = $this->model_teachers->get_teachers_obj($tbl_teacher_id);
			$first_name = $teacher_obj[0]['first_name'];	
			$last_name = $teacher_obj[0]['last_name'];
			if (trim($lan) == "ar") {
			$first_name = $teacher_obj[0]['first_name_ar'];	
			$last_name = $teacher_obj[0]['last_name_ar'];
			}
			$all_messages_rs[$n]['name'] 	=	$first_name." ".$last_name;	
			}
			$data['all_messages_rs'] = $all_messages_rs;
			echo json_encode($data);
		}
		
		
		// Student Performance Indicator
		function list_performance_indicator()
		{
			$lan 					= $_REQUEST["lan"];
			$tbl_school_id 			= $_REQUEST["tbl_school_id"];
			$tbl_student_id 		= $_REQUEST["tbl_student_id"];
			$tbl_class_id 		    = $_REQUEST["tbl_class_id"];
			$tbl_teacher_id 		= $_REQUEST["tbl_teacher_id"];
			
			$data["lan"] 			= $lan;
			$data["tbl_school_id"] 	= $tbl_school_id;
			$data["tbl_student_id"] = $tbl_student_id;
			$data["tbl_class_id"]   = $tbl_class_id;
			$data["tbl_teacher_id"] = $tbl_teacher_id;


			$this->load->model('model_student');
			$this->load->model('model_config');
			$current_date = date("Y-m-d");
			$getSemesterId = $this->model_student->get_semester_id($tbl_school_id,$current_date);
			$tbl_semester_id = $getSemesterId[0]['tbl_semester_id'];
			
			$topic_categories = $this->model_config->get_performance_topics_new($tbl_student_id,$tbl_class_id,$tbl_school_id, 'Y');
		
		    $arr_cards = array();
			for($m=0;$m<count($topic_categories);$m++)
			{
				$arr_cards[$m]['id'] 					= $topic_categories[$m]['tbl_performance_id'];
				if($lan=="en")
					$arr_cards[$m]['title'] 			= $topic_categories[$m]['topic_en'];
				else
					$arr_cards[$m]['title']  			= $topic_categories[$m]['topic_ar'];
					
					
				$arr_cards[$m]['performance_value']  	= isset($topic_categories[$m]['performance_value'])? $topic_categories[$m]['performance_value']: '0' ;
				$arr_cards[$m]['reported_date']  	    = isset($topic_categories[$m]['reported_date'])? $topic_categories[$m]['reported_date']: '' ;
			}
			$data['arr_cards'] 		= $arr_cards;
			echo json_encode($data);
		}
		
       // Update Student Performance
	
	   function update_student_performance() {
		   
			$tbl_performance_student_id      	= md5(uniqid(rand()));
			$topic_val          				= $_POST['topic_val'];		
			$topic_id           				= $_POST['topic_id'];
			$tbl_student_id     				= $_POST['tbl_student_id'];
			$tbl_class_id       				= $_POST['tbl_class_id'];
			$tbl_teacher_id     				= $_POST['tbl_teacher_id'];
			$tbl_school_id      				= $_POST['tbl_school_id'];
			$lan          				        = $_POST['lan'];	
				
			$this->load->model("model_student");	
			$resData = $this->model_student->save_student_performance_rate($tbl_performance_student_id,$topic_id,$topic_val,$tbl_student_id,$tbl_class_id,$tbl_teacher_id,$tbl_school_id);
			$arr['lan'] 				= $lan;
			$arr['topic_id'] 			= $topic_id;
			$arr['tbl_student_id'] 		= $tbl_student_id;
			$arr['tbl_teacher_id'] 		= $tbl_teacher_id;
			$arr['tbl_class_id'] 		= $tbl_class_id;
			
			if ($resData=="Y") {
				$arr["code"] = "200";
				$response = "Success";
				$errorMsg = "Student Performance Updated Successfully";
				
			} else {
				$arr["code"] = "400";
				$response = "Failure";
				$errorMsg = "Student Performance Updation failed";
			}
			
		$student_perfo['student_perfo'] = $arr;
		$response_array = array('response' => $response, 'result' => $student_perfo, 'errorMsg' => $errorMsg);

		$this->load->model('model_student');
		$current_date = date("Y-m-d");
		
		$topic_obj = $this->model_student->get_student_topic_info('', $topic_id, $tbl_school_id);
		$topic     = $topic_obj[0]['topic_en'];

		$stu_obj = $this->model_student->get_student_obj($tbl_student_id);
		$first_name_stu = $stu_obj[0]['first_name'];
		$last_name_stu = $stu_obj[0]['last_name'];
		$this->load->model('model_parents');
		$tbl_parent_id = $this->model_parents->get_parent_of_student($tbl_student_id);		
		$this->load->model('model_user_notify_token');
		$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_parent_id);

		if (trim(ENABLE_PUSH) == "Y") {
			$message = 'Dear Parent, teacher has issued '.$topic_val.'% in '.$topic.' to your child '.$first_name_stu.' '.$last_name_stu;
			$token  = $data_tkns[0]["token"];
			$device = $data_tkns[0]["device"];
			$this->load->model('model_user_notify_token');
			$this->model_user_notify_token->send_notification($token , $message, $device);
		}
		
		echo json_encode($response_array);
		exit;
	}	
	
	
		// Student Performance Indicator
		function performance_indicator_graph()
		{
			$lan 					= $_REQUEST["lan"];
			$tbl_school_id 			= $_REQUEST["tbl_school_id"];
			$tbl_student_id 		= $_REQUEST["tbl_student_id"];
			$tbl_class_id 		    = $_REQUEST["tbl_class_id"];
			$tbl_teacher_id 		= $_REQUEST["tbl_teacher_id"];
			$tbl_performance_id 	= $_REQUEST["tbl_performance_id"];
			$from_date 	            = $_REQUEST["from_date"];
			$to_date 	            = $_REQUEST["to_date"];
			$range                  = $_REQUEST["range"];
			
			$curr_date              =  date("Y-m-d");
			$curr_month             =  date("m");
			$curr_year              =  date("Y");
			if($range<>"")
			{
				 if($range == "week")
				 {
					 $to_date      =  $curr_date;
					 $startdateStr = strtotime("-1 week", strtotime($curr_date));
					 $from_date = date("Y-m-d",$startdateStr);
				 }
				 else if($range == "month")
				 {
					 $from_date      =  $curr_year."-".$curr_month."-01";
					 $to_date        =  $curr_year."-".$curr_month."-31";
					
				 }
				 else if($range == "year")
				 {
					  $to_date_range      =  $curr_date;
					  $startdateStr = strtotime("-11 months", strtotime($curr_date));
					  $from_date_range    = date("Y-m-d",$startdateStr);
					  $from_m		      = date("m",  strtotime($from_date_range));
					  $from_y		      = date("Y",  strtotime($from_date_range));
					  $to_m		          = date("m",  strtotime($to_date_range));
					  $to_y		          = date("Y",  strtotime($to_date_range));
					  
					  $from_date          = $from_y."-".$from_m."-01";
					  $to_date            = $to_y."-".$to_m."-31";
				 }
			}
			
			$this->load->model('model_student');
			$this->load->model('model_config');
			
			if($tbl_class_id=="")
			{
				$studentObj 	= $this->model_student->get_student_obj($tbl_student_id);
				$tbl_class_id 	= $studentObj[0]['tbl_class_id'];
			}
			
			$data["lan"] 			= $lan;
			$data["tbl_school_id"] 	= $tbl_school_id;
			$data["tbl_student_id"] = $tbl_student_id;
			$data["tbl_class_id"]   = $tbl_class_id;
			$data["tbl_teacher_id"] = $tbl_teacher_id;
			
			$current_date = date("Y-m-d");
			$getSemesterId = $this->model_student->get_semester_id($tbl_school_id,$current_date);
			$tbl_semester_id = $getSemesterId[0]['tbl_semester_id'];
			$topic_categories = array();
			
			$topic_categories = $this->model_config->get_performance_graphs($tbl_student_id,$tbl_class_id,$tbl_school_id, 'Y',$tbl_performance_id,$from_date,$to_date,$range);
			
			$data['arr_cards'] 		= $topic_categories;
			echo json_encode($data);
		}
	
	
    /********************** STUDENTS GROUP *************************/
	// Start Manage Students Group	
		
    function students_group_by_teacher() {
		
		$lan 					= $_REQUEST["lan"];
		$tbl_school_id 			= $_REQUEST["tbl_school_id"];
		$tbl_teacher_id 		= $_REQUEST["tbl_teacher_id"];
		
		$this->load->model("model_student");
		$this->load->model("model_class_sessions");
		$this->load->model("model_message");
		
		if($tbl_school_id<>"")
		{  
			// add purpose
			$classesObj = $this->model_class_sessions->get_all_classes($tbl_school_id, 'Y');
			$data['classes_list'] = $classesObj;
			
			//list purpose
			$rs_all_groups     = $this->model_student->get_student_group($tbl_school_id, $tbl_teacher_id);
			for($m=0;$m<count($rs_all_groups); $m++)
			{
				$students_list  = $this->model_message->get_students_list_in_group($rs_all_groups[$m]['tbl_student_group_id'],$tbl_school_id);
				$rs_all_groups[$m]['cntStudents']       = count($students_list);
				$rs_all_groups[$m]['students_list']     = $students_list;
			}
			
		}else{
			$data['classes_list'] = array();
			$rs_all_groups     = array();
		}
		$data['rs_all_groups']	       = $rs_all_groups;
		$response = "Success";
		$errorMsg = "Student Information";
		$response_array = array('response' => $response, 'result' => $rs_all_groups, 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
	}
	
	
	function save_student_group()
	{
		$this->load->model("model_message");
		$group_name_en        = $_POST['group_name_en'];
		$group_name_ar        = $_POST['group_name_ar'];
		$group_for            = $_POST['group_for'];
		$tbl_student_id       = $_POST['tbl_student_id'];        /**** Separated by : ****/
		$tbl_student_group_id = substr(md5(uniqid(rand())),0,10);  
		$tbl_school_id 		  = $_POST["tbl_school_id"];
		$added_by 		      = $_POST["added_by"];
		$added_type 		  = "T";
		
		$is_exist = $this->model_message->is_exist_student_group($tbl_student_group_id, $group_name_en, $tbl_school_id);
		if(count($is_exist)>0)
		{
		   $result = "X";
		}else{
			if($tbl_school_id<>"")
			{  
				if($tbl_student_group_id<>"")
				{
					$this->model_message->save_student_group($tbl_student_group_id, $group_name_en, $group_name_ar, $tbl_student_id, $tbl_school_id, $group_for, $added_by, $added_type);
				}
			   $result = "Y"; 
			}else{
			   $result = "N";	
			}
		}
		
		$data['result'] =  $result;
		$response = "Success";
		$errorMsg = "Student Group Added  Successfully";
		$response_array = array('response' => $response, 'result' => $data, 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
		
	}
	

	function delete_student_group() {
		
		$this->load->model("model_message");
		$tbl_school_id 				= $_REQUEST["tbl_school_id"];
		$tbl_student_group_id 		= $_REQUEST["tbl_student_group_id"];
		
		$this->model_message->delete_student_group($tbl_student_group_id,$tbl_school_id);
		
		$data = array();
		$response = "Success";
		$errorMsg = "Student Group Deleted Successfully";
		$response_array = array('response' => $response, 'result' => $data, 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
	}

	// Update Student Group
	function update_student_group()
	{
		$this->load->model("model_message");
		$group_name_en        	= $_POST['group_name_en'];
		$group_name_ar        	= $_POST['group_name_ar'];
		$group_for              = $_POST['group_for'];
		$tbl_student_id        	= $_POST['tbl_student_id'];  /**** Separated by : ****/
		$tbl_student_group_id  	= $_POST['tbl_student_group_id'];
		$tbl_school_id 			= $_POST["tbl_school_id"];
		$added_type    			= "T"; //teacher
		$is_exist = $this->model_message->is_exist_student_group($tbl_student_group_id,$group_name_en,$tbl_school_id);
		if(count($is_exist)>0)
		{
		  $result = "X";
		}else{
			if($tbl_school_id<>"")
			{  
			  $this->model_message->update_student_group($tbl_student_group_id, $group_name_en, $group_name_ar, $tbl_student_id, $tbl_school_id, $group_for, $added_type );
			   $result = "Y"; 
			}else{
			  $result = "N";	
			}
		}
		
		
		$data = array();
		$data['result'] = $result;
		$response = "Success";
		$errorMsg = "Student Group Updated Successfully";
		$response_array = array('response' => $response, 'result' => $data, 'errorMsg' => $errorMsg);
		echo json_encode($response_array);
		exit;
		
		
	}
	
	  /********************** END STUDENTS GROUP *********************/
	

		
	}
?>







