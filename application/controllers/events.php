<?php



/**

 * @desc   	  	Events Controller

 *

 * @category   	Controller

 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>

 * @version    	0.1

 */

class Events extends CI_Controller {





	/**

	* @desc    Events detail

	*

	* @param   string tbl_events_id

	* @access  default

	*/

    function event_details() {

		$lan = $_REQUEST["lan"];

		$device = $_REQUEST["device"];

		$device_uid = $_REQUEST["device_uid"];

		$tbl_school_id = $_REQUEST["tbl_school_id"];

		$data["lan"] = $lan;

		$data["device"] = $device;

		$data["device_uid"] = $device_uid;

		$data["tbl_school_id"] = $tbl_school_id;

		

		$event_id = $_REQUEST["event_id"];

		

		$this->load->model('model_events');

		$data_rs =  $this->model_events->get_event_details($event_id);

		$data["data_rs"] = $data_rs;

		

		$page = "event_details";

		$data['page'] = $page;

		

		$this->load->view('view_template',$data);

	}


	 function event_list() {
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["school_id"];

		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;

		$this->load->model('model_events');

		$data_rs =  $this->model_events->get_event_list($tbl_school_id);

		//print_r($data_rs);
		$data["data_rs"] = $data_rs;
		$page = "event_list";
		$data['page'] = $page;

		$this->load->view('view_template',$data);
	}

	 function event_list_web() {
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$tbl_school_id = $_REQUEST["school_id"];

		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["tbl_school_id"] = $tbl_school_id;

		$this->load->model('model_events');

		$data =  $this->model_events->get_event_list($tbl_school_id);

		$temp = array();
		for ($i=0; $i<count($data); $i++) {
			$id = $data[$i]["id"];
			$title_en = $data[$i]["title"];
			$title_ar = $data[$i]["title_ar"];
			
			$description_en = stripslashes($data[$i]["description"]);
			$description_ar = stripslashes($data[$i]["description_ar"]);

			$start_time = $data[$i]["start_time"];
			$start_date = $data[$i]["start_date"];
			$end_date = $data[$i]["end_date"];
			$color = $data[$i]["color"];
			
			$added_date_i = $data[$i]["added_date"];
			$is_active = $data[$i]["is_active"];
			
			if (trim($lan) == "ar") {
				$title = $title_ar;
				$description = $description_ar;
			} else {
				$title = $title_en;
				$description = $description_en;
			} 
			
			$temp[$i] = array('id' => $id, 'title' => $title, 'start' => $start_date, 'end' => $end_date, 'color' => "#".$color );
		}
		
		echo json_encode($temp);
	}

}

?>