<?php
/**
 * @desc   	  	Registration Controller
 * @category   	Controller
 * @author     	Shanavas.PK
 * @version    	0.1
 */

class Registration extends CI_Controller {
	/**
	* @desc Default constructor for the Controller
	* @access default
	*/

    public function __construct() {
		parent::__construct();
    }


    // Registration
	function student_registration() {
		
	
		
		if ($_POST) {
			$tbl_student_id            = isset($_POST['student_id_enc'])? $_POST['student_id_enc']:'';		
			$first_name                = isset($_POST['first_name'])? $_POST['first_name']:'';
			$last_name                 = isset($_POST['last_name'])? $_POST['last_name']:'';
			$first_name_ar             = isset($_POST['first_name_ar'])? $_POST['first_name_ar']:'';
			$last_name_ar         	   = isset($_POST['last_name_ar'])? $_POST['last_name_ar']:'';		
			$dob_month                 = isset($_POST['dob_month'])? $_POST['dob_month']:'';
			$dob_day                   = isset($_POST['dob_day'])? $_POST['dob_day']:'';
			$dob_year                  = isset($_POST['dob_year'])? $_POST['dob_year']:'';
			$gender                    = isset($_POST['gender'])? $_POST['gender']:'';		
			$mobile                    = isset($_POST['mobile'])? $_POST['mobile']:'';
			$email                     = isset($_POST['email'])? $_POST['email']:'';
			$tbl_emirates_id           = isset($_POST['tbl_emirates_id'])? $_POST['tbl_emirates_id']:'';	
			$country                   = isset($_POST['country'])? $_POST['country']:'';
			$emirates_id_father        = isset($_POST['emirates_id_father'])? $_POST['emirates_id_father']:'';		
			$emirates_id_mother        = isset($_POST['emirates_id_mother'])? $_POST['emirates_id_mother']:'';
			$tbl_academic_year_id      = isset($_POST['tbl_academic_year_id'])? $_POST['tbl_academic_year_id']:'';
			$tbl_class_id              = isset($_POST['tbl_class_id'])? $_POST['tbl_class_id']:'';
			$tbl_school_id             = isset($_POST['tbl_school_id'])? $_POST['tbl_school_id']:'';
			$lan                       = isset($_POST['lan'])? $_POST['lan']:'en';
			$device                    = isset($_POST['device'])? $_POST['device']:'';
			$device_uid                = isset($_POST['device_uid'])? $_POST['device_uid']:'';
			$is_active                 = isset($_POST['is_active'])? $_POST['is_active']:'N';
			
			$this->load->model("model_student");	
			$resData = $this->model_student->student_registration($tbl_student_id, $first_name, $last_name, $first_name_ar, $last_name_ar, $dob_month, $dob_day, $dob_year, 
		                                             $gender, $mobile, $email, $country, $emirates_id_father, $emirates_id_mother, $tbl_academic_year_id, $tbl_class_id, $tbl_school_id,$tbl_emirates_id,$is_active);
		
		
			if ($resData=="Y") {
				
				$tbl_parent_id                    = md5(uniqid(rand()));
				$first_name_parent                = isset($_POST['first_name_parent'])? $_POST['first_name_parent']: '' ;
				$last_name_parent              	  = isset($_POST['last_name_parent'])? $_POST['last_name_parent']: '';
				$first_name_parent_ar             = isset($_POST['first_name_parent_ar'])? $_POST['first_name_parent_ar']: '';
				$last_name_parent_ar         	  = isset($_POST['last_name_parent_ar'])? $_POST['last_name_parent_ar']: '';		
				$dob_month_parent                 = isset($_POST['dob_month_parent'])? $_POST['dob_month_parent']: '';
				$dob_day_parent                   = isset($_POST['dob_day_parent'])? $_POST['dob_day_parent']: '';
				$dob_year_parent                  = isset($_POST['dob_year_parent'])? $_POST['dob_year_parent']: '';
				$gender_parent                    = isset($_POST['gender_parent'])? $_POST['gender_parent']: '';		
				$mobile_parent                    = isset($_POST['mobile_parent'])? $_POST['mobile_parent']: '' ;
				$email_parent                     = isset($_POST['email_parent'])? $_POST['email_parent']:'';
				$emirates_id_parent               = isset($_POST['emirates_id_parent'])? $_POST['emirates_id_parent']:'';
				$parent_user_id                   = isset($_POST['parent_user_id'])? $_POST['parent_user_id']:'';
				$password                         = isset($_POST['password'])? $_POST['password']:'';
			
				$this->load->model("model_parents");
				$existParent = $this->model_parents->is_exist_parent($emirates_id_parent);  //$first_name_parent,$last_name_parent,
				if(count($existParent)>0)
				{
					$tbl_parent_id           = $existParent[0]['tbl_parent_id'];
					
					$updateParent = $this->model_parents->update_parent($tbl_parent_id, $first_name_parent, $last_name_parent, $first_name_parent_ar, $last_name_parent_ar, $dob_month_parent, $dob_day_parent, $dob_year_parent, $gender_parent, $mobile_parent, $email_parent, $emirates_id_parent, $parent_user_id, $password, $tbl_school_id);
					
					$resParent = $this->model_parents->assign_parents($tbl_parent_id, $tbl_student_id, $tbl_school_id);
					
				}else{
					$resData = $this->model_parents->add_parent($tbl_parent_id, $first_name_parent, $last_name_parent, $first_name_parent_ar, $last_name_parent_ar, $dob_month_parent, $dob_day_parent, $dob_year_parent, $gender_parent, $mobile_parent, $email_parent, $emirates_id_parent, $parent_user_id, $password, $tbl_school_id);
					
					$resParent = $this->model_parents->assign_parents($tbl_parent_id, $tbl_student_id, $tbl_school_id);
				}
				
			}
		
		}
		
		
		if ($resData=="Y") {
			echo "Y";
		} else {
			echo "N";
		}
	}
	
	
	
	/**
	* @desc    Total cards
	*
	* @param   none
	* @access  default
	*/ 
    function save_points() {
		$user_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		$tbl_class_id = $_REQUEST["tbl_class_id"];
		$tbl_student_id = $_REQUEST["tbl_student_id"];
		$tbl_point_category_id = $_REQUEST["points_student"];
		$message = urldecode($_REQUEST["message"]);
		$data["user_id"] = $user_id;
		$data["role"] = $role;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] = $school_id;
		$data["tbl_class_id"] = $tbl_class_id;
		$tbl_teacher_id = $user_id;
		$tbl_school_id = $school_id;
		$this->load->model('model_school');
		$dataPoint = $this->model_school->getPointById($tbl_point_category_id);
		
		if($lan=="en")
			$message = $dataPoint[0]['point_name_en'];
		else
			$message = $dataPoint[0]['point_name_ar'];
		$points_student = $dataPoint[0]['behaviour_point'];
		$data["points_student"] = $points_student;
	    $data["message"]        = urldecode($message);
		//$data["tbl_student_id"] = $tbl_student_id;
		$this->load->model('model_student');
		$current_date = date("Y-m-d");
		$getSemesterId = $this->model_student->get_semester_id($school_id,$current_date);
		$tbl_semester_id = $getSemesterId[0]['tbl_semester_id'];
		$this->model_student->save_points($tbl_teacher_id, $tbl_class_id, $tbl_student_id, $points_student, $message, $tbl_school_id, $tbl_point_category_id, $tbl_semester_id);
		$stu_obj = $this->model_student->get_student_obj($tbl_student_id);
		$first_name_stu = $stu_obj[0]['first_name'];
		$last_name_stu = $stu_obj[0]['last_name'];
		$this->load->model('model_parents');
		$tbl_parent_id = $this->model_parents->get_parent_of_student($tbl_student_id);		
		$this->load->model('model_user_notify_token');
		$data_tkns = $this->model_user_notify_token->get_user_tokens($tbl_parent_id);
		for ($b=0; $b<count($data_tkns); $b++) {
			//Send push message
			if (trim(ENABLE_PUSH) == "Y") {
				$message = 'Dear Parent, teacher has issued '.$points_student.' point(s) to your child '.$first_name_stu.' '.$last_name_stu;
				//$message = urlencode($message);
				//$token = $tbl_parent_id."-".$data_tkns[$b]["token"];
				$token  = $data_tkns[$b]["token"];
				$device = $data_tkns[$b]["device"];
				//$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$tbl_parent_id."&msg=".$message."&data=";
				$url = "https://webservices.appmobi.com/pushmobi.aspx?CMD=SendMessage&authuser=developer@timeline.ae&authpw=Developer007!&appname=HKkfv45oHFRTyWq7Lekg6n1PDfXfAv_AVeVX73fe2P4&user=".$token."&msg=".$message."&data=";
				// You can POST a file by prefixing with an @ (for <input type="file"> fields)
				$this->load->model('model_user_notify_token');
				$this->model_user_notify_token->send_notification($token , $message, $device);
				//$ch = curl_init();
				//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				//curl_setopt($ch, CURLOPT_URL, $url);
				//curl_setopt($ch, CURLOPT_HEADER, 0);
				//curl_exec($ch);
				//curl_close($ch);
			}//if (trim(ENABLE_PUSH) == "Y")				
		}
		$arr["code"] = "200";
		echo json_encode($arr);
	}

}
?>







