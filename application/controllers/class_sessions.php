<?php
/**
 * @desc   	  	Sessions Controller
 *
 * @category   	Controller
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */

class Class_sessions extends CI_Controller {
	
	
	/**
	* @desc    Default function for the Controller
	*
	* @param   none
	* @access  default
	*/
    function index() {
		//Do nothing
	}
	

	/**
	* @desc    Image Gallery
	*
	* @param   none
	* @access  default
	*/
	function get_class_sessions() {
		
		$user_id = $_REQUEST["user_id"];
		$role = $_REQUEST["role"];
		$lan = $_REQUEST["lan"];
		$device = $_REQUEST["device"];
		$device_uid = $_REQUEST["device_uid"];
		$school_id = $_REQUEST["school_id"];
		$tbl_class_id = $_REQUEST["tbl_class_id"];
		
		$data["user_id"] = $user_id;
		$data["role"] = $role;
		$data["lan"] = $lan;
		$data["device"] = $device;
		$data["device_uid"] = $device_uid;
		$data["school_id"] = $school_id;
		
		$this->load->model('model_class_sessions');
		$data_rs = $this->model_class_sessions->get_class_sessions($school_id,$tbl_class_id);
		
		//print_r($data_rs);
		//exit();
		
		date_default_timezone_set('Asia/Dubai');
		
		$attendance_date = date("Y-m-d");
		$data["attendance_date"] = $attendance_date;	
		
		$this->load->model('model_teacher_attendance');
		$index = 0 ;
		
		for ($i=0; $i<count($data_rs); $i++) {
			$res_arr[$index]["tbl_class_sessions_id"] = $data_rs[$i]["tbl_class_sessions_id"];
			if($lan=="ar"){
				$res_arr[$index]["title"] = $data_rs[$i]["title_ar"];
			}else{
				$res_arr[$index]["title"] = $data_rs[$i]["title"];
			}
			$res_arr[$index]["start_time"] = $data_rs[$i]["start_time"];
			$res_arr[$index]["end_time"] = $data_rs[$i]["end_time"];
			
			$is_attendance_exists = $this->model_teacher_attendance->is_attendance_exists($tbl_class_id, $data_rs[$i]["tbl_class_sessions_id"], $attendance_date, $school_id);
			
			$res_arr[$index]["is_attendance_exists"] = $is_attendance_exists;
			$index = $index+1;
		}
		
		//print_r($arr);
		//exit();
		
		if (count($data_rs) <=0) {
			$arr["code"] = "200";
			echo json_encode($arr);
			return;
		}
		
		//Teacher admin
		if (trim($_POST["is_admin"]) == "Y") {
			$data["res_arr"] = $res_arr;
			$this->load->view('admin/view_teacher_class_sessions',$data);
			return;
		}	
	
		$arr["class_sessions"] = $res_arr;
		echo json_encode($arr);
	}
}
?>







