<?php
/**
 * @desc   	  	School_web Controller
 * @category   	Controller
 * @version    	0.1
 */

class School_web extends CI_Controller {

	/**
	* @desc    Default function for the Controller
	* @param   none
	* @access  default
	*/
    function index() {
		$data['lan']  = LAN_SEL;
		$data['page'] = "view_school_web";
		$this->load->view('view_template',$data);
	}
	
	function student_details() {
      	$data['lan']  = LAN_SEL;
		$this->load->model("model_student");
		$this->load->model('model_school');
		
		$countriesObj          = $this->model_student->get_country_list('Y');
		$data['countries_list']= $countriesObj;
			
		$academicObj          = $this->model_student->get_academic_year('Y',$tbl_school_id);
		$data['academic_list']= $academicObj;
		
		$rs_schools = $this->model_school->get_all_schools();
		$data['list_schools'] = $rs_schools;
		
		$data['page'] = "view_student_web";
		$this->load->view('view_template',$data);
	}
	
	function parents_details() {
      	$data['lan']  = LAN_SEL;
		$this->load->model("model_student");
		$this->load->model('model_school');
		
		$countriesObj          = $this->model_student->get_country_list('Y');
		$data['countries_list']= $countriesObj;
			
		$academicObj          = $this->model_student->get_academic_year('Y',$tbl_school_id);
		$data['academic_list']= $academicObj;
		
		$rs_schools = $this->model_school->get_all_schools();
		$data['list_schools'] = $rs_schools;
		
		$data['page'] = "view_parent_web";
		$this->load->view('view_template',$data);
	}
	
	function teacher_details() {
      	$data['lan']  = LAN_SEL;
		$this->load->model("model_student");
		$this->load->model('model_school');
		
		$countriesObj          = $this->model_student->get_country_list('Y');
		$data['countries_list']= $countriesObj;
			
		$academicObj          = $this->model_student->get_academic_year('Y',$tbl_school_id);
		$data['academic_list']= $academicObj;
		
		$rs_schools = $this->model_school->get_all_schools();
		$data['list_schools'] = $rs_schools;
		
		$data['page'] = "view_teacher_web";
		$this->load->view('view_template',$data);
	}
	
	function feedback() {
      	$data['lan']  = LAN_SEL;
		$data['page'] = "view_feedback_web";
		$this->load->view('view_template',$data);
	}
	
	function school_details() {
      	$data['lan']  = LAN_SEL;
		$data['page'] = "view_school_details";
		$this->load->view('view_template',$data);
	}
	
		
	function contact_us() {
      	$data['lan']  = LAN_SEL;
		
		if(isset($_POST["name"]))
		{
			$name 	       = $_POST["name"];
			$phone 	       = $_POST["phone"];
			$email_id 	   = $_POST["email_id"];		
			$company       = $_POST["company"];
			$subject       = $_POST["subject"];
			$enquiry       = $_POST["enquiry"];
	
			$this->load->model('model_message');
			$saveMessage = $this->model_message->saveCustomerEnquiry($name, $phone, $email_id, $company, $subject, $enquiry);
			echo 'Y';
			exit;
		}
		$data['page'] = "view_contact_web";
		$this->load->view('view_template',$data);
	}
	
	
	function student_progress_report() {
      	$data['lan']  = LAN_SEL;
		$data['lan']  = $lan;
		
		$this->load->model("model_student");
		$this->load->model('model_school');
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(2);
					
		if (array_key_exists('student_id_enc',$param_array)) {
			$tbl_student_id = $param_array['student_id_enc'];
			$data['tbl_student_id'] = $tbl_student_id;
		}	 
		
		//User details
		$this->load->model("model_student");
		$this->load->model("model_config");	
		$this->load->model("model_classes");	
			
		$tbl_school_id = ""; 	
		$student_obj = $this->model_student->student_info($tbl_student_id,$tbl_school_id);
		$data['student_obj'] = $student_obj;
		
		$tbl_class_id  = $student_obj[0]['tbl_class_id'];
		$tbl_parent_id = $student_obj[0]['tbl_parent_id'];
		$tbl_school_id = $student_obj[0]['tbl_school_id'];
		
		
		
		//PERFORMANCE GRAPH PRESENTATION
		$range                  = "year";
		$curr_date              =  date("Y-m-d");
		$curr_month             =  date("m");
		$curr_year              =  date("Y");
		if($range<>"")
		{
				 if($range == "week")
				 {
					 $to_date      =  $curr_date;
					 $startdateStr = strtotime("-1 week", strtotime($curr_date));
					 $from_date = date("Y-m-d",$startdateStr);
				 }
				 else if($range == "month")
				 {
					 $from_date      =  $curr_year."-".$curr_month."-01";
					 $to_date        =  $curr_year."-".$curr_month."-31";
					
				 }
				 else if($range == "year")
				 {
					  $to_date_range      =  $curr_date;
					  $startdateStr = strtotime("-11 months", strtotime($curr_date));
					  $from_date_range    = date("Y-m-d",$startdateStr);
					  $from_m		      = date("m",  strtotime($from_date_range));
					  $from_y		      = date("Y",  strtotime($from_date_range));
					  $to_m		          = date("m",  strtotime($to_date_range));
					  $to_y		          = date("Y",  strtotime($to_date_range));
					  
					  $from_date          = $from_y."-".$from_m."-01";
					  $to_date            = $to_y."-".$to_m."-31";
				 }

			$getSemesterId = $this->model_student->get_semester_id($tbl_school_id,$current_date);
			$tbl_semester_id = $getSemesterId[0]['tbl_semester_id'];
			$months = array();
		
		    $sel_month =	 date("F");
			$months[0]	   =	 $sel_month;
		   
		   for ($i = 1; $i < 12; $i++) {
				$months[$i] = date("F", strtotime( date( 'Y-m-01' )." -$i months"));
			}
			$data['months'] 		        = $months;
		
			$performance_graph 				= $this->model_config->get_performance_graphs($tbl_student_id,$tbl_class_id,$tbl_school_id, 'Y',$tbl_performance_id,$from_date,$to_date,$range);
			$data['performance_graph'] 		= $performance_graph;
	   }
		
		
		//PERFORMANCE ACTIVITIES
		$topic_categories = $this->model_config->get_performance_topics_new($tbl_student_id,$tbl_class_id,$tbl_school_id);
		$performance_activities = array();
		
		for($m=0;$m<count($topic_categories);$m++)
		{
			$performance_activities[$m]['id'] 					= $topic_categories[$m]['tbl_performance_id'];
			$performance_activities[$m]['title'] 				= $topic_categories[$m]['topic_en'];
			$performance_activities[$m]['title_ar']  			= $topic_categories[$m]['topic_ar'];
			$performance_activities[$m]['performance_value']  	= isset($topic_categories[$m]['performance_value'])? $topic_categories[$m]['performance_value']: '0' ;
			
		}
		$data['performance_activities'] = $performance_activities;
		
		
		//CARDS
		$current_date = date("Y-m-d");
			
		$data["semester_title"] = "";
	    if($tbl_semester_id=="")
		{
			$getSemesterId   = $this->model_student->get_semester_id($tbl_school_id,$current_date);
			$tbl_semester_id = $getSemesterId[0]['tbl_semester_id'];
			if($lan=="en")
				$semester_title = $getSemesterId[0]['title'];
			else
				$semester_title = $getSemesterId[0]['title_ar'];
		
		 $data["semester_title"] = $semester_title;
		}
		$tbl_semester_id = "";
		
		
		
		$rs_cards = $this->model_student->get_all_cards_student($tbl_student_id,$tbl_semester_id);
		$cardTypeArray = array();
		for($p=0;$p<count($rs_cards);$p++)
		{
			$cardTypeArray[$p] = 	$rs_cards[$p]['card_type'];
		}
		
		
		$arr_cards = array();
		if($tbl_class_id<>""){
			$class_info = $this->model_classes->getClassInfo($tbl_class_id);
			$tbl_school_type_id = $class_info[0]['tbl_school_type_id'];
		}
		$card_categories = $this->model_config->get_cards_categories_new($tbl_school_id,$tbl_school_type_id,$lan);
		
		for($m=0;$m<count($card_categories);$m++)
		{
			$arr_cards[$m]['id'] 		= $card_categories[$m]['tbl_card_category_id'];
			$arr_cards[$m]['title'] 	= $card_categories[$m]['category_name_en'];
			$arr_cards[$m]['title_ar']  = $card_categories[$m]['category_name_ar'];
			/*$arr_cards[$m]['score'] 	= $card_categories[$m]['card_point'];*/
			$issuedPoint = 0;
			if(in_array($arr_cards[$m]['id'], $cardTypeArray))
			{
				 $rs_cards_semester = $this->model_student->get_cards_semester($tbl_student_id, $tbl_semester_id, $tbl_school_id, $arr_cards[$m]['id']);
					for ($i=0; $i<count($rs_cards_semester); $i++) {
						$cnt 				= $rs_cards_semester[$i]['cnt'];
						$card_type 			= $rs_cards_semester[$i]['card_type'];
						$card_issue_type    = $rs_cards_semester[$i]['card_issue_type'];
						if($card_issue_type=="issue"){
							$cardData[$card_type]['issue'] = $cnt;
							$cardData[$card_type]['card_type'] = $card_type;
						}else{
							$cardData[$card_type]['cancel'] = $cnt;
							$cardData[$card_type]['card_type'] = $card_type;
						}
						
					}
					$cardCntIssued  = 0;
					foreach($cardData as $key=>$value)
					{ 
						$cardPoint	    = 0;
						$issueCntCard 	= isset($value['issue'])? $value['issue'] : '0';
						$cancelCntCard 	= isset($value['cancel'])? $value['cancel'] : '0';
						$cardCntIssued  = $issueCntCard - $cancelCntCard;
					}
					//echo $cardCntIssued;
					
					$cardCntIssued = abs($cardCntIssued); 
					if(abs($cardCntIssued)<=0){
						$arr_cards[$m]['image'] = "N";
						$arr_cards[$m]['color'] =  "#0a7429";
						$arr_cards[$m]['score']  = "".abs($cardCntIssued);
					}else if($cardCntIssued==1){
						$arr_cards[$m]['image'] = "N";
						$arr_cards[$m]['color'] 	=  "#faf707";
						$arr_cards[$m]['score']  = "-".abs($cardCntIssued);
					}else if($cardCntIssued >=2){
						$arr_cards[$m]['image'] = "N";
						$arr_cards[$m]['color'] 	=  "#fa2307";
						$cardCntIssued = $cardCntIssued - 1;
						$arr_cards[$m]['score']  = "".abs($cardCntIssued);
					}
			}else{
				
				$arr_cards[$m]['score'] 	= "0";
				$arr_cards[$m]['image'] = "N";
				$arr_cards[$m]['color'] 	=  "#0a7429";
			}
		}
		$data["cards"] 				= $arr_cards;
		
		//Points
		$rs_student_point 	= $this->model_student->get_overview_student_points_detail($tbl_student_id,$tbl_class_id,$tbl_semester_id,$tbl_school_id,'Y',$from_date,$to_date,$range);
		$data["rs_student_point"] 	= $rs_student_point;
		$totalPoint = 0;
		for($v=0;$v<count($rs_student_point);$v++)
		{
			$totalPoint = $totalPoint + $rs_student_point[$v]['student_point'];
		}
		$data["rs_student_total_point"] 	=   $totalPoint;
		
		$rs_student_point_graph 			= $this->model_student->get_student_points_graph($tbl_student_id,$tbl_class_id,$tbl_semester_id,$tbl_school_id, 'Y',$from_date,$to_date,$range);
		$data["rs_student_point_graph"] 	= $rs_student_point_graph;
		
		// Attendance 
		$rs_student_attendance 	    	= $this->model_student->get_total_student_attendance($tbl_student_id,$tbl_class_id,$tbl_school_id, 'Y',$from_date,$to_date,$range);
		$data["rs_student_attendance"] 	= $rs_student_attendance;
		
		// Attendance Graph
		$rs_student_attendance_graph 	    	= $this->model_student->get_student_attendance_graph($tbl_student_id,$tbl_class_id,$tbl_school_id, 'Y',$from_date,$to_date,$range);
		$data["rs_student_attendance_graph"] 	= $rs_student_attendance_graph;
		
		
		
	  
	   /* $points_obj = $this->model_student->student_points_details($q, $tbl_student_id,$tbl_class_id,$tbl_school_id);
		$data['points_obj'] = $points_obj;
		
		$cards_obj = $this->model_student->student_cards_details($q, $tbl_student_id,$tbl_class_id,$tbl_school_id);
		$data['cards_obj'] = $cards_obj;
		
		$rs_all_records     = $this->model_student->get_student_records($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parenting_school_cat_id,$tbl_student_id,$tbl_class_id);
        $data['records_obj'] = $rs_all_records;
		
		$rs_all_messages     = $this->model_student->list_parent_messages_of_student($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parent_id);
		$data['messages_obj'] = $rs_all_messages;*/
		$data['page'] = "view_student_progress_report";
		$this->load->view('view_template',$data);
	}
	
	
	
	function student_progress_report_mobile() {
      	$data['lan']  = LAN_SEL;
		
		$this->load->model("model_student");
		$this->load->model('model_school');
		
		//GET Params
		$param_array = $this->uri->uri_to_assoc(2);
					
		if (array_key_exists('student_id_enc',$param_array)) {
			$tbl_student_id = $param_array['student_id_enc'];
			$data['tbl_student_id'] = $tbl_student_id;
		}	 
		
		//User details
		$this->load->model("model_student");
		$this->load->model("model_config");	
		$this->load->model("model_classes");	
			
		$tbl_school_id = ""; 	
		$student_obj = $this->model_student->student_info($tbl_student_id,$tbl_school_id);
		$data['student_obj'] = $student_obj;
		
		$tbl_class_id  = $student_obj[0]['tbl_class_id'];
		$tbl_parent_id = $student_obj[0]['tbl_parent_id'];
		$tbl_school_id = $student_obj[0]['tbl_school_id'];
		
		
		
		//PERFORMANCE GRAPH PRESENTATION
		$range                  = "year";
		$curr_date              =  date("Y-m-d");
		$curr_month             =  date("m");
		$curr_year              =  date("Y");
		if($range<>"")
		{
			 
			
				 if($range == "week")
				 {
					 $to_date      =  $curr_date;
					 $startdateStr = strtotime("-1 week", strtotime($curr_date));
					 $from_date = date("Y-m-d",$startdateStr);
				 }
				 else if($range == "month")
				 {
					 $from_date      =  $curr_year."-".$curr_month."-01";
					 $to_date        =  $curr_year."-".$curr_month."-31";
					
				 }
				 else if($range == "year")
				 {
					  $to_date_range      =  $curr_date;
					  $startdateStr = strtotime("-11 months", strtotime($curr_date));
					  $from_date_range    = date("Y-m-d",$startdateStr);
					  $from_m		      = date("m",  strtotime($from_date_range));
					  $from_y		      = date("Y",  strtotime($from_date_range));
					  $to_m		          = date("m",  strtotime($to_date_range));
					  $to_y		          = date("Y",  strtotime($to_date_range));
					  
					  $from_date          = $from_y."-".$from_m."-01";
					  $to_date            = $to_y."-".$to_m."-31";
				 }

			$getSemesterId = $this->model_student->get_semester_id($tbl_school_id,$current_date);
			$tbl_semester_id = $getSemesterId[0]['tbl_semester_id'];
			$months = array();
		
		    $sel_month =	 date("F");
			
		    $months[0]	   =	 $sel_month;
		   
		   for ($i = 1; $i < 12; $i++) {
				$months[$i] = date("F", strtotime( date( 'Y-m-01' )." -$i months"));
			}
		  
		
		    $data['months'] 		        = $months;
			$performance_graph 				= $this->model_config->get_performance_graphs($tbl_student_id,$tbl_class_id,$tbl_school_id, 'Y',$tbl_performance_id,$from_date,$to_date,$range);
			$data['performance_graph'] 		= $performance_graph;
	   }
	
		
		//PERFORMANCE ACTIVITIES
		$topic_categories = $this->model_config->get_performance_topics_new($tbl_student_id,$tbl_class_id,$tbl_school_id);
		$performance_activities = array();
		
		for($m=0;$m<count($topic_categories);$m++)
		{
			$performance_activities[$m]['id'] 					= $topic_categories[$m]['tbl_performance_id'];
			$performance_activities[$m]['title'] 				= $topic_categories[$m]['topic_en'];
			$performance_activities[$m]['title_ar']  			= $topic_categories[$m]['topic_ar'];
			$performance_activities[$m]['performance_value']  	= isset($topic_categories[$m]['performance_value'])? $topic_categories[$m]['performance_value']: '0' ;
			
		}
		$data['performance_activities'] = $performance_activities;
		
		
		//CARDS
		$current_date = date("Y-m-d");
			
		$data["semester_title"] = "";
	    if($tbl_semester_id=="")
		{
			$getSemesterId   = $this->model_student->get_semester_id($tbl_school_id,$current_date);
			$tbl_semester_id = $getSemesterId[0]['tbl_semester_id'];
			if($lan=="en")
				$semester_title = $getSemesterId[0]['title'];
			else
				$semester_title = $getSemesterId[0]['title_ar'];
		
		 $data["semester_title"] = $semester_title;
		}
		$tbl_semester_id = "";
		
		
		
		$rs_cards = $this->model_student->get_all_cards_student($tbl_student_id,$tbl_semester_id);
		$cardTypeArray = array();
		for($p=0;$p<count($rs_cards);$p++)
		{
			$cardTypeArray[$p] = 	$rs_cards[$p]['card_type'];
		}
		
		
		$arr_cards = array();
		if($tbl_class_id<>""){
			$class_info = $this->model_classes->getClassInfo($tbl_class_id);
			$tbl_school_type_id = $class_info[0]['tbl_school_type_id'];
		}
		$card_categories = $this->model_config->get_cards_categories_new($tbl_school_id,$tbl_school_type_id,$lan);
		
		for($m=0;$m<count($card_categories);$m++)
		{
			$arr_cards[$m]['id'] 		= $card_categories[$m]['tbl_card_category_id'];
			$arr_cards[$m]['title'] 	= $card_categories[$m]['category_name_en'];
			$arr_cards[$m]['title_ar']  = $card_categories[$m]['category_name_ar'];
			/*$arr_cards[$m]['score'] 	= $card_categories[$m]['card_point'];*/
			$issuedPoint = 0;
			if(in_array($arr_cards[$m]['id'], $cardTypeArray))
			{
				 $rs_cards_semester = $this->model_student->get_cards_semester($tbl_student_id, $tbl_semester_id, $tbl_school_id, $arr_cards[$m]['id']);
					for ($i=0; $i<count($rs_cards_semester); $i++) {
						$cnt 				= $rs_cards_semester[$i]['cnt'];
						$card_type 			= $rs_cards_semester[$i]['card_type'];
						$card_issue_type    = $rs_cards_semester[$i]['card_issue_type'];
						if($card_issue_type=="issue"){
							$cardData[$card_type]['issue'] = $cnt;
							$cardData[$card_type]['card_type'] = $card_type;
						}else{
							$cardData[$card_type]['cancel'] = $cnt;
							$cardData[$card_type]['card_type'] = $card_type;
						}
						
					}
					$cardCntIssued  = 0;
					foreach($cardData as $key=>$value)
					{ 
						$cardPoint	    = 0;
						$issueCntCard 	= isset($value['issue'])? $value['issue'] : '0';
						$cancelCntCard 	= isset($value['cancel'])? $value['cancel'] : '0';
						$cardCntIssued  = $issueCntCard - $cancelCntCard;
					}
					//echo $cardCntIssued;
					
					$cardCntIssued = abs($cardCntIssued); 
					if(abs($cardCntIssued)<=0){
						$arr_cards[$m]['image'] = "N";
						$arr_cards[$m]['color'] =  "#0a7429";
						$arr_cards[$m]['score']  = "".abs($cardCntIssued);
					}else if($cardCntIssued==1){
						$arr_cards[$m]['image'] = "N";
						$arr_cards[$m]['color'] 	=  "#faf707";
						$arr_cards[$m]['score']  = "-".abs($cardCntIssued);
					}else if($cardCntIssued >=2){
						$arr_cards[$m]['image'] = "N";
						$arr_cards[$m]['color'] 	=  "#fa2307";
						$cardCntIssued = $cardCntIssued - 1;
						$arr_cards[$m]['score']  = "".abs($cardCntIssued);
					}
			}else{
				
				$arr_cards[$m]['score'] 	= "0";
				$arr_cards[$m]['image'] = "N";
				$arr_cards[$m]['color'] 	=  "#0a7429";
			}
		}
		$data["cards"] 				= $arr_cards;
		
		//Points
		$rs_student_point 	= $this->model_student->get_overview_student_points_detail($tbl_student_id,$tbl_class_id,$tbl_semester_id,$tbl_school_id);
		$data["rs_student_point"] 	= $rs_student_point;
		
		$totalPoint = 0;
		for($v=0;$v<count($rs_student_point);$v++)
		{
			$totalPoint = $totalPoint + $rs_student_point[$v]['student_point'];
		}
		$data["rs_student_total_point"] 	=   $totalPoint;
		
		$rs_student_point_graph 			= $this->model_student->get_student_points_graph($tbl_student_id,$tbl_class_id,$tbl_semester_id,$tbl_school_id, 'Y',$from_date,$to_date,$range);
		$data["rs_student_point_graph"] 	= $rs_student_point_graph;
		
		// Attendance 
		$rs_student_attendance 	    	= $this->model_student->get_total_student_attendance($tbl_student_id,$tbl_class_id,$tbl_school_id, 'Y',$from_date,$to_date,$range);
		$data["rs_student_attendance"] 	= $rs_student_attendance;
		
		// Attendance Graph
		$rs_student_attendance_graph 	    	= $this->model_student->get_student_attendance_graph($tbl_student_id,$tbl_class_id,$tbl_school_id, 'Y',$from_date,$to_date,$range);
		$data["rs_student_attendance_graph"] 	= $rs_student_attendance_graph;
		
	  
	   /* $points_obj = $this->model_student->student_points_details($q, $tbl_student_id,$tbl_class_id,$tbl_school_id);
		$data['points_obj'] = $points_obj;
		
		$cards_obj = $this->model_student->student_cards_details($q, $tbl_student_id,$tbl_class_id,$tbl_school_id);
		$data['cards_obj'] = $cards_obj;
		
		$rs_all_records     = $this->model_student->get_student_records($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parenting_school_cat_id,$tbl_student_id,$tbl_class_id);
        $data['records_obj'] = $rs_all_records;
		
		$rs_all_messages     = $this->model_student->list_parent_messages_of_student($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parent_id);
		$data['messages_obj'] = $rs_all_messages;*/
		$data['page'] = "view_student_progress_report_mobile";
		$this->load->view('view_template',$data);
	}
	
	
	
	
	
    function classes_against_school() {
	  $param_array = $this->uri->uri_to_assoc(2);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		//$sort_name = "point_name_en";
		$sort_by = "ASC";
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('tbl_school_id',$param_array)) {
			$tbl_school_id = $param_array['tbl_school_id'];
			if (trim($tbl_school_id) == "") {
				$tbl_school_id = '';	
			}
		}	
		$data["tbl_school_id"]  = $tbl_school_id;
		$this->load->model('model_classes');
		$classes_list = array();
		$is_active    = "Y";
		
	     if($tbl_school_id<>""){
			$classes_list= $this->model_classes->get_all_class_grades('', '', '', '', $is_active, $tbl_school_id);
			
			$option_str .= '<select name="tbl_class_id" id="tbl_class_id" class="form-control">
                            <option value="">--Select Class--</option>';

                           for ($u=0; $u<count($classes_list); $u++) { 
                                  $tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
                                  $class_name             = $classes_list[$u]['class_name'];
                                  $class_name_ar          = $classes_list[$u]['class_name_ar'];
								  $section_name           = $classes_list[$u]['section_name'];
                                  $section_name_ar        = $classes_list[$u]['section_name_ar'];
                                
                                 $option_str .= '<option value="'.$tbl_class_id_u.'"  >
                                      '.$class_name.'&nbsp;'.$section_name.'</option>';
                                    }
                                
                         $option_str .= '</select>';
						
			   }
	   echo $option_str;
		
	}
	
	
	function classes_subject_against_school() {
	  $param_array = $this->uri->uri_to_assoc(2);
		$q = "";
		$sort_by_click = "N";
		$sort_name_param = "A";
		//$sort_name = "point_name_en";
		$sort_by = "ASC";
		$icon_sort = "icon_bottom.jpg";
		$offset = 0;
		$total_categories = 0;
					
		if (array_key_exists('tbl_school_id',$param_array)) {
			$tbl_school_id = $param_array['tbl_school_id'];
			if (trim($tbl_school_id) == "") {
				$tbl_school_id = '';	
			}
		}	
		$data["tbl_school_id"]  = $tbl_school_id;
		$this->load->model('model_classes');
		$this->load->model('model_school_subject');
		$this->load->model('model_teachers');
		$classes_list = array();
		$is_active    = "Y";
		$option_str = "";
	     if($tbl_school_id<>"")
		 {
				
			$classes_list			  = $this->model_classes->get_all_class_grades('', '', '', '', $is_active, $tbl_school_id);
			$subject_list 	          = $this->model_school_subject->get_all_school_subjects('', '', '', '', 'Y', $tbl_school_id);
			$teacher_roles_list       = $this->model_teachers->get_teacher_roles('Y',$tbl_school_id);
			
			 if(count($teacher_roles_list)>0){ 	
			
			 $option_str .=  '<li><label>Role</label><br>
                     <select name="tbl_school_roles_id" id="tbl_school_roles_id" class="form-control">
                                  <option value="">--Select Role --</option>';
                                  
                                        for ($u=0; $u<count($teacher_roles_list); $u++) { 
                                            $tbl_school_roles_id_u  = $teacher_roles_list[$u]['tbl_school_roles_id'];
                                            $role                   = $teacher_roles_list[$u]['role'];
                                            $role_ar                = $teacher_roles_list[$u]['role_ar'];
                                            $section_name           = $classes_list[$u]['section_name'];
                                            $section_name_ar        = $classes_list[$u]['section_name_ar'];
											   
               $option_str .=  '<option value="'.$tbl_school_roles_id_u.'" >
                                         '.$role.'&nbsp;[::]&nbsp;'.$role_ar.'&nbsp;
                                          </option>';
                                        }
               $option_str .=  '</select>
                     <span class="errorRole"  id="role_error" style="display:none;">Please select Role.</span>
                   <li>';
				
			 }
			
				//classes list
			 if(count($classes_list)>0){ 	
			
				$option_str .=  '<li>
						<label>Class Teacher</label><br>';
				
				$option_str .= '<select name="tbl_class_id" id="tbl_class_id" class="form-control">
								<option value="">--Select Class--</option>';
	
							   for ($u=0; $u<count($classes_list); $u++) { 
									  $tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
									  $class_name             = $classes_list[$u]['class_name'];
									  $class_name_ar          = $classes_list[$u]['class_name_ar'];
									  $section_name           = $classes_list[$u]['section_name'];
									  $section_name_ar        = $classes_list[$u]['section_name_ar'];
									
									 $option_str .= '<option value="'.$tbl_class_id_u.'"  >
										  '.$class_name.'&nbsp;'.$section_name.'</option>';
										}
									
							 $option_str .= '</select>';
				
				   
			   $option_str .= '<span class="errorClass"  id="class_error" style="display:none;">Please select Class.</span>
					  </li>';
					  
			 }
			 
			 
			 if(count($classes_list)>0){ 		
				  $option_str .=  " <li>
						<label>Class(s)</label><br>";
				
                       
                  $option_str .= ' <div class="form-group" id="all_student">
        
                          <div id="divClass" style="max-height:200px; overflow-y:scroll;">
                          <div>';
	 
                     
                 /* $option_str .= '<div style="padding-bottom:10px;background-color:#e8eaeb;" class="col-sm-12"> 
                           <input type="checkbox" value="" id="select_all_class" class="checkboxClass" >&nbsp;Select All</div>';*/
                      
                            for ($u=0; $u<count($classes_list); $u++) { 
								$tbl_class_id_u         = $classes_list[$u]['tbl_class_id'];
								$class_name             = $classes_list[$u]['class_name'];
								$class_name_ar          = $classes_list[$u]['class_name_ar'];
								$section_name           = $classes_list[$u]['section_name'];
								$section_name_ar        = $classes_list[$u]['section_name_ar'];
							
                           
				$option_str .= '<div class="col-sm-4" style="padding-top:10px; padding-bottom:10px; border:1px solid #CCC;"> 
								  <input id="tbl_class_id_'.$u.'" name="tbl_class_id[]" class="checkboxClass" type="checkbox" value="'.$tbl_class_id_u.'" />&nbsp;
								   '.$class_name.'&nbsp;'.$section_name.'&nbsp;[::]&nbsp;'.$class_name_ar.'&nbsp;'.$section_name_ar.'
                             </div>';
	                         }
	            $option_str .= '</div></div></div></li>'; 
				  }    
				      
                   if(count($subject_list)>0){ 
		   
					$option_str .= '<li><label>Subject(s)</label><br>
							  <div style="max-height:200px; overflow-y:scroll;">
							  <div>';
                       
                /* $option_str .= '<div style="padding-bottom:10px;background-color:#e8eaeb;" class="col-sm-12"> 
                           <input type="checkbox" value="" id="select_all_subject" class="checkboxSubject" >&nbsp;Select All</div>';*/
                      
                       
					for ($s=0; $s<count($subject_list); $s++) { 
						$tbl_subject_id_s       = $subject_list[$s]['tbl_subject_id'];
						$subject                = $subject_list[$s]['subject'];
						$subject_ar             = $subject_list[$s]['subject_ar'];
	
						$option_str .= ' <div class="col-sm-6" style="padding-top:10px; padding-bottom:10px; border:1px solid #CCC;"> 
									  <input id="tbl_subject_id_'.$u.'" name="tbl_subject_id[]" class="checkboxSubject" type="checkbox" value="'.$tbl_subject_id_s.'" />&nbsp;
									   '.$subject.'&nbsp;[::]&nbsp;'.$subject_ar.'
								 </div>';
								 }
						$option_str .= '</div></div></div></li>'; 
				   }	
				 
			    }    
	   echo $option_str;
		
	}
	
	
	

}

?>

