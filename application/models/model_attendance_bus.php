<?php
include_once('include/common_functions.php');

/**
 * @desc   	  	Attendance Bus Model
 *
 * @category   	Model
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Model_attendance_bus extends CI_Model {
	var $cf;


	/**
	* @desc Default constructor for the Controller
	*
	* @access default
	*/
    function model_attendance_bus() {
		$this->cf = new Common_functions();
    }


	/**
	* @desc		Get student attendance for particular bus and for particular bus sessions (morning, evening etc)
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_attendance_bus($tbl_bus_id, $tbl_student_id, $tbl_bus_sessions_id, $tbl_school_id, $attendance_date) { 
		$qry = "SELECT is_present FROM ".TBL_ATTENDANCE_BUS." WHERE tbl_bus_id='".$tbl_bus_id."' AND tbl_bus_sessions_id='".$tbl_bus_sessions_id."' AND tbl_student_id='".$tbl_student_id."' AND attendance_date='".$attendance_date."' AND tbl_school_id='".$tbl_school_id."' AND is_active='Y'";
		$data_rs = $this->cf->selectMultiRecords($qry);
		if ($data_rs[0]["is_present"] == "") {
			return "NA";
		} else {
			return $data_rs[0]['is_present'];
		}
	}
	
	
	/**
	* @desc		Save attendance
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function save_attendance_b($tbl_bus_id, $tbl_bus_sessions_id ,$tbl_student_id, $is_present, $attendance_date,  $tbl_school_id) {
		$tbl_attendance_bus_id = substr(md5(uniqid(rand())),0,10);
		
		$qry = "INSERT INTO ".TBL_ATTENDANCE_BUS." (
			`tbl_attendance_bus_id` ,
			`tbl_bus_id` ,
			`tbl_bus_sessions_id` ,
			`tbl_student_id` ,
			`is_present` ,
			`attendance_date` ,
			`is_active` ,
			`added_date` ,
			`tbl_school_id`
			)
			VALUES (
			'$tbl_attendance_bus_id', '$tbl_bus_id', '$tbl_bus_sessions_id', '$tbl_student_id', '$is_present', '$attendance_date', 'Y', NOW(), '$tbl_school_id'
			)";
		
		//echo $qry."<br />";
		$this->cf->insertInto($qry);
	}
	
	
	/**
	* @desc		Delate old attendance data if any, for a given bus on particular date for either morning or afternoon session.
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function delete_attendance_b($attendance_date, $tbl_bus_id, $tbl_bus_sessions_id, $tbl_school_id) {
		$qry = "DELETE FROM ".TBL_ATTENDANCE_BUS." WHERE tbl_bus_id='".$tbl_bus_id."' AND tbl_bus_sessions_id='".$tbl_bus_sessions_id."' AND attendance_date='".$attendance_date."' AND tbl_school_id='".$tbl_school_id."' ";
		if($tbl_student_id<>""){
			$qry .= " AND tbl_student_id='".$tbl_student_id."' ";  
		}
		//echo $qry."<br />";
		$this->cf->deleteFrom($qry);
	}
	
}
?>

