<?php
include_once('include/common_functions.php');
/**
 * @desc   	  	User Notify Token Model
 * @category   	Model
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */

class Model_user_notify_token extends CI_Model {
	var $cf;
	/**
	* @desc Default constructor for the Controller
	* @access default
	*/

    function model_user_notify_token() {
		$this->cf = new Common_functions();
    }

	/**
	* @desc		Function to save device token and user information
	* @param	
	* @access	default
	* @return	array $rs
	*/

	function save_user_notify_token($user_type, $user_id, $token, $device) {
		$qry = "DELETE FROM ".TBL_USER_NOTIFY_TOKEN." WHERE  user_id='$user_id' AND device='$device' AND is_active='Y' ";
		$this->cf->deleteFrom($qry);
		$tbl_user_notify_token_id = substr(md5(uniqid(rand())),0,10);
		$qry = "INSERT INTO ".TBL_USER_NOTIFY_TOKEN." (
			`tbl_user_notify_token_id` ,
			`user_type` ,
			`user_id` ,
			`device` ,
			`token` ,
			`is_active` ,
			`added_date`
			)
			VALUES (
			'$tbl_user_notify_token_id', '$user_type', '$user_id', '$device', '$token', 'Y', NOW()
		)";
		$this->cf->insertInto($qry);
		return "";	
	}

	/**
	* @desc		Function to get all user tokens
	* @param	
	* @access	default
	* @return	array $rs
	*/

	function get_user_tokens($user_id) {
		$qry = "SELECT token,device FROM ".TBL_USER_NOTIFY_TOKEN." WHERE user_id='$user_id' AND is_active='Y'  order by id desc";
		$data_rs = $this->cf->SelectMultiRecords($qry);
		return $data_rs;	
	}

	function send_notification($deviceToken, $message, $device) {
		//echo "M";
		$this->cf->send_notification($deviceToken, $message, $device);
		if ($deviceToken == "null" || $deviceToken == "(null)") {return;}
		$message = urldecode($message);
		$tbl_notification_log_id = substr(md5(uniqid(rand())),0,10);
		$qry = "INSERT INTO ".TBL_NOTIFICATION_LOG." (`tbl_notification_log_id`, `token`, `message`, `is_sent`) VALUES ('$tbl_notification_log_id', '$deviceToken', '$message', 'N')";
		//echo $qry;
		$this->cf->insertInto($qry);
		//echo "P";
	}

	function delete_user_notify_token($user_id, $role)
	{
		$qry = "DELETE FROM ".TBL_USER_NOTIFY_TOKEN." WHERE user_id='$user_id' ";
		$this->cf->deleteFrom($qry);
		if($role=="P")
		{
			$qry_update = "UPDATE ".TBL_PARENT." SET is_logged='N' WHERE tbl_parent_id='$user_id' ";
			$this->cf->update($qry_update);
		}else if($role=="T")
		{
		    $qry_update = "UPDATE ".TBL_TEACHER." SET is_logged='N' WHERE tbl_teacher_id='$user_id' ";
			$this->cf->update($qry_update);
		}
	}
	
	
	function send_test_notification($deviceToken, $message, $device) {
        if($deviceToken<>"")
		{
			$this->cf->send_notification($deviceToken, $message, $device);
		}
	}
	
	

}


?>





