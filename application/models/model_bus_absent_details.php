<?php
include_once('include/common_functions.php');

/**
 * @desc   	  	Bus Absent Details Model
 *
 * @category   	Model
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Model_bus_absent_details extends CI_Model {
	var $cf;


	/**
	* @desc Default constructor for the Controller
	*
	* @access default
	*/
    function model_bus_absent_details() {
		$this->cf = new Common_functions();
    }


	/**
	* @desc		Get bus absent details
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_bus_absent_details_student($tbl_bus_id, $tbl_student_id, $tbl_bus_sessions_id, $tbl_school_id) { 
	    $currDate = date("Y-m-d");
		$qry = "SELECT * FROM ".TBL_BUS_ABSENT_DETAILS." WHERE tbl_bus_id='".$tbl_bus_id."' AND  tbl_student_id='".$tbl_student_id."' AND  tbl_bus_sessions_id='".$tbl_bus_sessions_id."' ";
		$qry .= " and  attendance_date= '".$currDate."'";
		//echo $qry;
		$data_rs = $this->cf->selectMultiRecords($qry);
		return $data_rs;
	}
	
	
	/**
	* @desc		Get bus absent details
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_bus_absent_details($tbl_bus_absent_details_id) { 
		$qry = "SELECT * FROM ".TBL_BUS_ABSENT_DETAILS." WHERE tbl_bus_absent_details_id='".$tbl_bus_absent_details_id."'";
		//echo $qry;
		$data_rs = $this->cf->selectMultiRecords($qry);
		return $data_rs;
	}
	
	
	/**
	* @desc		Get all bus absent details 
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_bus_leave_details($tbl_student_id, $attendance_date) { 
		$qry = "SELECT * FROM ".TBL_BUS_ABSENT_DETAILS." WHERE tbl_student_id='".$tbl_student_id."' AND attendance_date='".$attendance_date."'";
		//echo $qry;
		$data_rs = $this->cf->selectMultiRecords($qry);
		return $data_rs;
	}
	
	
	
	/**
	* @desc		Save bus absent details
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function save_bus_absent_details($tbl_bus_id, $tbl_bus_sessions_id, $tbl_student_id, $message_type, $message_from, $tbl_fixed_message_id, $attendance_date, $tbl_school_id) {
		
		$qry = "DELETE FROM ".TBL_BUS_ABSENT_DETAILS." WHERE tbl_student_id='".$tbl_student_id."' AND tbl_bus_sessions_id='".$tbl_bus_sessions_id."' AND attendance_date='".$attendance_date."'";
		$this->cf->deleteFrom($qry);
		
		$tbl_bus_absent_details_id = substr(md5(uniqid(rand())),0,10); 
		$qry = "INSERT INTO ".TBL_BUS_ABSENT_DETAILS." (
			`tbl_bus_absent_details_id` ,
			`tbl_bus_id` ,
			`tbl_bus_sessions_id` ,
			`tbl_student_id` ,
			`message_type` ,
			`message_from` ,
			`tbl_fixed_message_id` ,
			`attendance_date` ,
			`is_active` ,
			`added_date` ,
			`tbl_school_id`
			)
			VALUES (
			'$tbl_bus_absent_details_id', '$tbl_bus_id', '$tbl_bus_sessions_id', '$tbl_student_id', '$message_type' , '$message_from', '$tbl_fixed_message_id', '$attendance_date', 'Y',NOW(), '$tbl_school_id'
			)";
		//echo $qry;
		$this->cf->insertInto($qry);
		return;
	}
	
	function get_bus_leave_messages($tbl_bus_id,$attendance_date, $tbl_school_id)
	{
		$qry = "SELECT * FROM ".TBL_BUS_ABSENT_DETAILS." WHERE tbl_bus_id='".$tbl_bus_id."' AND attendance_date='".$attendance_date."' and tbl_school_id='".$tbl_school_id."' ";
		//echo $qry;
		$data_rs = $this->cf->selectMultiRecords($qry);
		return $data_rs;
	}
	
		
}
?>

