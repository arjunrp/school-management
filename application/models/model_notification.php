<?php
include_once('include/common_functions.php');
/**
 * @desc   	  	Notification Model
 * @category   	Model
 * @author     	Shanavas.PK
 * @version    	0.1
 */


class Model_notification extends CI_Model {
	var $cf;
	/**
	* @desc Default constructor for the Controller
	* @access default
	*/

    function model_notification() {
		$this->cf = new Common_functions();
    }

	/**
	* @desc		Save user token
	* @param	
	* @access	default
	* @return	array $rs
	*/

	function save($token, $device_uid, $token_type, $tbl_school_id, $device) {
		$tbl_token_id = substr(md5(uniqid(rand())),0,10);
		$tbl_class_id = $this->cf->get_data(trim($tbl_class_id));
		$qry_sel = "INSERT INTO ".TBL_TOKEN." (
			`tbl_token_id` ,
			`token` ,
			`device_uid` ,
			`token_type` ,
			`added_date` ,
			`is_active` ,
			`tbl_school_id`
			)
			VALUES (
				'$tbl_token_id', '$token', '$device_uid', '$token_type', NOW(), 'Y', '$tbl_school_id'
			)";
		//echo $qry_sel."<br />";
		$rs = $this->cf->insertInto($qry_sel);
	    return $rs;	
	}

	function listSentParentNotifications($school_id='')
	{
		$offset = 0;
		$CountRec .= "SELECT * FROM ".TBL_NOTIFICATION_RPT." WHERE 1 ";	
		$Query .= "SELECT * FROM ".TBL_NOTIFICATION_RPT." WHERE 1 ";	
		if($school_id<>""){
  			$Query    .= " AND find_in_set('$school_id', schools) <> 0 ";
		  	$CountRec .= " AND find_in_set('$school_id', schools) <> 0"; 
				}
		if(!$by){$by="DESC";}
		if($field && !empty($q)){	    	

			if($LIKE=="LIKE"){
					$CountRec .= " AND `$field` $LIKE '%$q%' ";
					$Query .= " AND `$field` $LIKE '%$q%' ";
			}else{
					$CountRec .= " AND `$field` $LIKE '$q' ";	
					$Query .= " AND `$field` $LIKE '$q' ";
			}
		}	

		 $Query .= " ORDER BY added_date $by ";
		// $Query .=" LIMIT $offset, ".TBL_NOTIFICATION_RPT_PAGING;
		 $Query .=" LIMIT 0, 50 ";
   	     $q = stripslashes($q);
		//echo $Query; exit;
		//$total_record = CountRecords($CountRec);
		 $content = array();
		 $data = $this->cf->selectMultiRecords($Query);
		// print_r($data); exit;
		 for($k=0;$k<count($data);$k++)
		 {
			 $content[$k]['name'] 					= "";
			 $content[$k]['teacher_id'] 			= $data[$k]['tbl_notification_rpt_id'];
			 $content[$k]['date'] 					= date("H:i - d/m/Y",strtotime($data[$k]['added_date'])); 
			 $content[$k]['message'] 				= $data[$k]['message'];
			 $content[$k]['url'] 					= "";
			 $content[$k]['item_id'] 				= $data[$k]['item_id'];
			if($content[$k]['item_id']<>""){
				 $dataQuery = "SELECT * FROM ".TBL_UPLOADS." WHERE tbl_item_id='".$content[$k]['item_id']."'";
				 $dataRecords = $this->cf->selectMultiRecords($dataQuery);
				 if(empty($dataRecords))
				 {
					  $itemArrays = explode(":", $content[$k]['item_id']);
					  $dataQuery = "SELECT * FROM ".TBL_UPLOADS." WHERE tbl_item_id='".$itemArrays[0]."'";
				 	  $dataRecords = $this->cf->selectMultiRecords($dataQuery);
				 }

				 if(trim($dataRecords[0]['file_name_original']) == "audio.mp4")
				 {
					 $audio 		= $dataRecords[0]['file_name_updated'];
					 $content[$k]['message_audio'] = HOST_URL."/admin/uploads/".$audio; 
					 $content[$k]['message_img'] = "";
					 $content[$k]['message_img_thumb'] 	= "";
				 }else{
					 $image 		= $dataRecords[0]['file_name_updated'];
					 $image_thumb 	= $dataRecords[0]['file_name_updated_thumb'];
					 $content[$k]['message_img'] = HOST_URL."/admin/uploads/".$image; 
					 $content[$k]['message_img_thumb'] 	= HOST_URL."/admin/uploads/".$image_thumb;
					 $content[$k]['message_audio'] 		= "";
				 }
			 }else{
				 $content[$k]['message_img'] 		= ""; 
				 $content[$k]['message_img_thumb'] 	= "";
				 $content[$k]['message_audio'] 		= "";
			 }
		 }
		 return $content;
	}

	function listSentTeacherNotifications($school_id=''){
		$q = addslashes($q);
		$offset = 0;
		$CountRec .= "SELECT * FROM ".TBL_NOTIFICATION_RPT_TEACHERS." WHERE 1 ";	
		$Query .= "SELECT * FROM ".TBL_NOTIFICATION_RPT_TEACHERS." WHERE 1 ";	
		if($school_id<>""){
		  $Query    .= " AND find_in_set('$school_id', schools) <> 0 ";
		  $CountRec .= " AND find_in_set('$school_id', schools) <> 0"; 
		}
		if(!$by){$by="DESC";}
		if($field && !empty($q)){	    	

			if($LIKE=="LIKE"){
					$CountRec .= " AND `$field` $LIKE '%$q%' ";
					$Query .= " AND `$field` $LIKE '%$q%' ";
			}else{
					$CountRec .= " AND `$field` $LIKE '$q' ";	
					$Query .= " AND `$field` $LIKE '$q' ";
			}

		}	

		 $Query .= " ORDER BY added_date $by ";
		 /*$Query .=" LIMIT $offset, ".TBL_NOTIFICATION_RPT_TEACHERS_PAGING;*/
		 $Query .=" LIMIT 0, 50 ";
   	     $q = stripslashes($q);
		 $content = array();
		 $data = $this->cf->selectMultiRecords($Query);
		 for($k=0;$k<count($data);$k++)
		 {

			 $content[$k]['name'] 					= "";
			 $content[$k]['teacher_id'] 			= $data[$k]['tbl_notification_rpt_id'];
			 $content[$k]['date'] 					= date("H:i - d/m/Y",strtotime($data[$k]['added_date'])); 
			 $content[$k]['message'] 				= $data[$k]['message'];
			 $content[$k]['url'] 					= "";
			 $content[$k]['item_id'] 				= $data[$k]['item_id'];
			 if($content[$k]['item_id']<>""){
				 $dataQuery = "SELECT * FROM ".TBL_UPLOADS." WHERE tbl_item_id='".$content[$k]['item_id']."'";
				 $dataRecords = $this->cf->selectMultiRecords($dataQuery);
				 if(empty($dataRecords))
				 {
					  $itemArrays = explode(":", $content[$k]['item_id']);
					  $dataQuery = "SELECT * FROM ".TBL_UPLOADS." WHERE tbl_item_id='".$itemArrays[0]."'";
				 	  $dataRecords = $this->cf->selectMultiRecords($dataQuery);
				 }

				 if(trim($dataRecords[0]['file_name_original']) == "audio.mp4")
				 {
					 $audio 		= $dataRecords[0]['file_name_updated'];
					 $content[$k]['message_audio'] = HOST_URL."/admin/uploads/".$audio; 
					 $content[$k]['message_img'] = "";
					 $content[$k]['message_img_thumb'] 	= "";
				 }else{

					 $image 		  = $dataRecords[0]['file_name_updated'];
					 $image_thumb 	= $dataRecords[0]['file_name_updated_thumb'];
					 $content[$k]['message_img'] = HOST_URL."/admin/uploads/".$image; 
					 $content[$k]['message_img_thumb'] 	= HOST_URL."/admin/uploads/".$image_thumb;
					 $content[$k]['message_audio'] 		= "";
				 }
			 }else{

				 $content[$k]['message_img'] 		= ""; 
				 $content[$k]['message_img_thumb'] 	= "";
				 $content[$k]['message_audio'] 		= "";
			 }
		 }
		 return $content;
	}

	function sentMessageToParents($school_type,$school_ids,$country,$message,$item_id){
		$gender = "N";
		$tbl_school_id_arr = explode(":", $school_ids);
		for($i=0; $i<count($tbl_school_id_arr); $i++) {
			$schools_str     = $tbl_school_id_arr[$i].",".$schools_str;
			$schools_qry_str = $schools_qry_str.$tbl_school_id_arr[$i].'","';
		}

		if (strlen($schools_str)>0) {
			$schools_str      = substr($schools_str,0,-1); 
			$schools_qry_str  = substr($schools_qry_str,0,-3);
			$schools_qry_str  = '"'.$schools_qry_str.'"';
		}

		$tbl_notification_rpt_id = substr(md5(uniqid(rand())),0,10);
		$qry = "INSERT INTO ".TBL_NOTIFICATION_RPT." (
			`tbl_notification_rpt_id` ,
			`country` ,
			`gender` ,
			`school_type` ,
			`schools` ,
			`message` ,
			`is_active` ,
			`added_date`,
			`item_id`
			)
			VALUES (
				'$tbl_notification_rpt_id', '$country', '$gender', '$school_type', '$schools_str', '$message', 'Y', NOW(),'$item_id'
			)";
		$rs = $this->cf->insertInto($qry);
		/*if ($male != "" && $female != "") {
			$gender_qry = "";
		} else if ($male != "") {
			$gender_qry = "AND gender='male'";
		} else if ($female != "") {
			$gender_qry = "AND gender='female'";
		}*/
		if (trim($country) !="" && $country != "All") {
			$country_qry = "AND country='".$country."'";
		}

		$schools_qry_str = $schools_qry_str.'"0000000000"';
		$qry1 = "SELECT tbl_student_id FROM ".TBL_STUDENT." WHERE is_active='Y' ".$gender_qry." ".$country_qry." AND tbl_school_id IN (".$schools_qry_str.")";
		$qry2 = "SELECT tbl_parent_id AS user_id FROM ".TBL_PARENT_STUDENT." WHERE is_active='Y' AND tbl_student_id IN (".$qry1.")";
		$qry3 = "SELECT * FROM ".TBL_USER_NOTIFY_TOKEN." WHERE user_type='P' AND is_active='Y' AND user_id IN (".$qry2.")";
		$data_rs = $this->cf->selectMultiRecords($qry3);
		for($i=0;$i<count($data_rs);$i++) {
			$user_type = $data_rs[$i]["user_type"];
			$user_id   = $data_rs[$i]["user_id"];
			$device    = $data_rs[$i]["device"];
			$token     = $data_rs[$i]["token"];

			$qry = "INSERT INTO ".TBL_NOTIFICATION_DATA_RPT." (
				`tbl_notification_rpt_id` ,
				`user_type` ,
				`user_id` ,
				`device` ,
				`token` ,
				`is_sent` ,
				`sent_date`
				)
				VALUES (
					'$tbl_notification_rpt_id', '$user_type', '$user_id', '$device', '$token', 'Y', NOW()
				)";
		 $rs = $this->cf->insertInto($qry);
			//send_notification_log($token, $message,  "");
			/*USE CRONE TO SEND NOTIFICATION*/
			//send_notification($token, $message, $device);
		}
		//echo $qry3;
		$MSG = "Success";	
		$arr['msg'] = $MSG;
		return $arr;
	}

	function sentMessageToTeachers($school_type,$school_ids,$country,$message,$item_id){
	    $gender = "N";
		$tbl_school_id_arr = explode(":", $school_ids);
		for($i=0; $i<count($tbl_school_id_arr); $i++) {
			$schools_str = $tbl_school_id_arr[$i].",".$schools_str;
			$schools_qry_str = $schools_qry_str.$tbl_school_id_arr[$i].'","';
		}

		if (strlen($schools_str)>0) {
			$schools_str = substr($schools_str,0,-1); 
			$schools_qry_str = substr($schools_qry_str,0,-3);
			$schools_qry_str = '"'.$schools_qry_str.'"';
		}

		$tbl_notification_rpt_id = substr(md5(uniqid(rand())),0,10);
		$qry = "INSERT INTO ".TBL_NOTIFICATION_RPT_TEACHERS." (
			`tbl_notification_rpt_id` ,
			`country` ,
			`gender` ,
			`school_type` ,
			`schools` ,
			`message` ,
			`is_active` ,
			`added_date`,
			`item_id`
			)
			VALUES (
				'$tbl_notification_rpt_id', '$country', '$gender', '$school_type', '$schools_str', '$message', 'Y', NOW(),'$item_id'
			)";
		//echo $qry;
		$rs = $this->cf->insertInto($qry);
		/*if ($male != "" && $female != "") {
			$gender_qry = "";
		} else if ($male != "") {
			$gender_qry = "AND gender='male'";
		} else if ($female != "") {
			$gender_qry = "AND gender='female'";
		}*/

		if (trim($country) !="" && $country != "All") {
			$country_qry = "AND country='".$country."'";
		}

		$schools_qry_str = $schools_qry_str.'"0000000000"';
		//$qry1 = "SELECT tbl_student_id FROM ".TBL_STUDENT." WHERE is_active='Y' ".$gender_qry." ".$country_qry." AND tbl_school_id IN (".$schools_qry_str.")";
		$qry2 = "SELECT DISTINCT(tbl_teacher_id) AS user_id FROM ".TBL_TEACHER." WHERE is_active='Y' ".$gender_qry." ".$country_qry." AND is_active='Y'";
		$qry3 = "SELECT * FROM ".TBL_USER_NOTIFY_TOKEN." WHERE user_type='T' AND is_active='Y' AND user_id IN (".$qry2.")";
		//echo $qry3."<br>";
		$data_rs = $this->cf->selectMultiRecords($qry3);

		for($i=0;$i<count($data_rs);$i++) {
			$user_type = $data_rs[$i]["user_type"];
			$user_id = $data_rs[$i]["user_id"];
			$device = $data_rs[$i]["device"];
			$token = $data_rs[$i]["token"];
			$qry = "INSERT INTO ".TBL_NOTIFICATION_DATA_RPT_TEACHERS." (
				`tbl_notification_rpt_id` ,
				`user_type` ,
				`user_id` ,
				`device` ,
				`token` ,
				`is_sent` ,
				`sent_date`
				)
				VALUES (
					'$tbl_notification_rpt_id', '$user_type', '$user_id', '$device', '$token', 'N', NOW()
				)";
			 $rs = $this->cf->insertInto($qry);
			//send_notification_log($token, $message,  "");
			/*USE CRONE TO SEND NOTIFICATION*/
			//send_notification($token, $message, $device);
		}
		$MSG = "Success";	
		$arr['msg'] = $MSG;
		return $arr;
	}

	function get_School_name($tbl_school_id) {
		$qry_school = "SELECT school_name FROM ".TBL_SCHOOL." WHERE tbl_school_id='$tbl_school_id' ";
		$rs_school  = $this->cf->selectMultiRecords($qry_school);
		return $rs_school[0]['school_name'];	
	}

	function getParentQueries(){
	    $CountRec .= "SELECT * FROM ".TBL_GOVT_FEEDBACK." WHERE 1 ";	
		$Query .= "SELECT * FROM ".TBL_GOVT_FEEDBACK." WHERE 1 ";	
		if(!$by){$by="DESC";}
		if($field && !empty($q)){	    	
			if($LIKE=="LIKE"){
					$CountRec .= " AND `$field` $LIKE '%$q%' ";
					$Query .= " AND `$field` $LIKE '%$q%' ";
			}else{
					$CountRec .= " AND `$field` $LIKE '$q' ";	
					$Query .= " AND `$field` $LIKE '$q' ";
			}
		}	
		 $Query .= " ORDER BY added_date $by ";
		// $Query .=" LIMIT $offset, ".TBL_NOTIFICATION_RPT_PAGING;
		 $Query .=" LIMIT 0, 50 ";
		 //echo $Query;
		 //$total_record = CountRecords($CountRec);
	     $content = array();
		 $data = $this->cf->selectMultiRecords($Query);
		 for($k=0;$k<count($data);$k++)
		 {
			 $content[$k]['name'] 					= "";
			 $content[$k]['teacher_id'] 			= $data[$k]['feedback_id'];
			 $content[$k]['date'] 					= date("H:i - d/m/Y",strtotime($data[$k]['added_date']));
			 $content[$k]['message'] 				= $data[$k]['feedback'];
			 $content[$k]['subject'] 				= $data[$k]['subject'];
			 $content[$k]['email_id'] 				= $data[$k]['email_id'];
			 $content[$k]['url'] 					= "";
			 $content[$k]['message_img'] 			= "";
			 $content[$k]['message_img_thumb'] 	= "";
			 $content[$k]['message_audio'] 		= "";
		 }
		 return $content;
	}

	function listBehaviourReportForSchools($tbl_category_id="")
	{
		 $CountRec = "SELECT * FROM ".TBL_CARD_CATEGORY." WHERE is_active='Y' ";
		 if($tbl_category_id<>"")
		 	$CountRec .= " AND tbl_card_category_id	= '".$tbl_category_id."'";
		//WHERE tbl_school_id='$tbl_school_id_sess'  now add crad to move govt section. cards common for all schools
		$Query = "SELECT * FROM ".TBL_CARD_CATEGORY." WHERE is_active='Y' ";
		 if($tbl_category_id<>"")
		 	$Query .= " AND tbl_card_category_id	= '".$tbl_category_id."'";

		$Query .= " ORDER BY card_category_order $by ";
		$Query .=" LIMIT 0, 15 ";
		$q = stripslashes($q);
		$data = $this->cf->selectMultiRecords($Query);
		//print_r($data); exit;
		$arrayCards = array();
		$dataArray  = array();

		for($m=0;$m<count($data); $m++){
			$card_logo = "";
			$color= "";
			$card_logo = $data[$m]['card_logo'];
			$category_name_en = $data[$m]['category_name_en'];
			$category_name_ar = $data[$m]['category_name_ar'];
			$color = $data[$m]['color'];
			$cardCategoryId = $data[$m]['tbl_card_category_id'];
			$card_point =	$data[$m]["card_point"];
			if($card_logo<>"")
			{
				$card_logo = IMG_GALLERY_PATH."/".$card_logo;
			}
			$arrayCards[$m]['card_logo'] = $card_logo;
			$arrayCards[$m]['tbl_card_category_id'] = $cardCategoryId;
			$arrayCards[$m]['category_name_en'] = $category_name_en;
			$arrayCards[$m]['category_name_ar'] = $category_name_ar;
			$arrayCards[$m]['color'] = $color;
			$arrayCards[$m]['card_point'] = $card_point;
		}


		if($tbl_category_id<>""){
					$qry_schools = "SELECT * FROM ".TBL_SCHOOL." WHERE is_active='Y' ";
					$rs_schools  = $this->cf->selectMultiRecords($qry_schools);	
					$tbl_school_id_param = array();

					for ($s=0; $s<count($rs_schools); $s++) { 
						$tbl_school_id_param[$s] = $rs_schools[$s]['tbl_school_id'];
						$school_name_s[$s]       = $rs_schools[$s]['school_name'];
					}
					$start_date = "";
					$is_male ="";
					$country = "";

					$final_arr = array();			  
					for ($i=0; $i<count($tbl_school_id_param); $i++) {
							$qry_issued    = "SELECT COUNT(*) as total_cards, card_type FROM ".VIEW_TBL_TEACHER_CARDS;
							$qry_cancelled = "SELECT COUNT(*) as total_cards, card_type FROM ".VIEW_TBL_TEACHER_CARDS;

 							$qry_issued .= " WHERE tbl_school_id='".$tbl_school_id_param[$i]."' AND card_issue_type='issue' "; 
 							$qry_cancelled .= " WHERE tbl_school_id='".$tbl_school_id_param[$i]."' AND card_issue_type='cancel' ";
 						
							$qry_issued .= " AND card_type='".$tbl_category_id."' "; 
							$qry_cancelled .= " AND card_type='".$tbl_category_id."'  ";

							if($start_date<>""){
								$qry_issued .= " AND (added_date>='$start_date' AND added_date<='$end_date') ";
								$qry_cancelled .= " AND (added_date>='$start_date' AND added_date<='$end_date') ";
							}

							if ($is_male == "Y" && $is_female == "N") {
								$qry_issued .= " AND gender='male'";
								$qry_cancelled .= " AND gender='male'";
							}

							if ($is_male == "N" && $is_female == "Y") {
								$qry_issued .= " AND gender='female'";
								$qry_cancelled .= " AND gender='female'";
							}

							if (trim($country) != "") {
								$qry_issued .= " AND country='$country' ";
								$qry_cancelled .= " AND country='$country' ";
							}

								$qry_issued .= " GROUP BY card_type";
								$qry_cancelled .= " GROUP BY card_type";

								$rs_issued = $this->cf->selectMultiRecords($qry_issued);	
								$rs_cancelled = $this->cf->selectMultiRecords($qry_cancelled);		

								$total = 0;

								for ($k=0; $k<count($rs_issued); $k++) {
									$card_type_k   = $rs_issued[$k]['card_type'];
									$total_cards_k = $rs_issued[$k]['total_cards'];
									$totalCards[$tbl_school_id_param[$i]][$card_type_k]= $total_cards_k;
									$total = $total + $totalCards[$tbl_school_id_param[$i]][$card_type_k];
								}//for ($k=0; $k<count($rs_issued); $k++)

								for ($m=0; $m<count($rs_cancelled); $m++) {
									$card_type_m = $rs_cancelled[$m]['card_type'];
									$total_cards_m = $rs_cancelled[$m]['total_cards'];
									$totalCards[$tbl_school_id_param[$i]][$card_type_m] = $totalCards[$tbl_school_id_param[$i]][$card_type_m] - $total_cards_m;
									$total =  $total - $totalCards[$tbl_school_id_param[$i]][$card_type_m];
								}
								$totalCards[$tbl_school_id_param[$i]][$total_cards] = $total;	

								//for ($m=0; $m<count($rs_cancelled); $m++)
								$final_arr[$i]['school_name'] =  $this->get_School_name($tbl_school_id_param[$i]);
								$final_arr[$i]['tbl_school_id']= $tbl_school_id_param[$i];
								//print_r($final_arr);
							 $cntCard = 0;
						  // print_r($tbl_school_id_param);
						   for($m=0;$m<count($data); $m++){ 
							  if(isset($totalCards[$tbl_school_id_param[$i]][$tbl_category_id]))
							  {
								$cntCard = $totalCards[$tbl_school_id_param[$i]][$tbl_category_id];
							   }else{
								$cntCard = 0;
							   }
							  // $final_arr[$i]['tbl_card_category_id']	= $tbl_category_id;
							   $final_arr[$i]['cntCard']			    = $cntCard;
						   } 
						   $totcntCard = $totcntCard + $cntCard;
						  // $final_arr[$i]['total_cards']= $totalCards[$tbl_school_id_param[$i]][$total_cards];
					 }
					 $dataArray['cardsCnt']  =  $final_arr;
					 $dataArray['total']     = $totcntCard;
		}else
		{
			$dataArray['cardsList'] =  $arrayCards;
		}
		return $dataArray;
	}
	
	
	//START BACKEND MINISTRY NOTIFICATION 
	
	function list_ministry_message_to_teachers($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_teacher_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
		if (trim($offset) == "") {$offset = 0;}
		
		$Query .= "SELECT * FROM ".TBL_NOTIFICATION_RPT_TEACHERS."  AS MT WHERE 1 ";	
		if($school_id<>""){
		  $Query    .= " AND find_in_set('$tbl_school_id', schools) <> 0 ";
		}
		if(!$by){$by="DESC";}
		
		//Search
		if (trim($q) != "") {
			$Query    .= " AND  MT.message LIKE '%$q%' ";
		}
			
		 $Query .= " ORDER BY added_date $by ";
		 $Query .=" LIMIT $offset, ".TBL_NOTIFICATION_RPT_TEACHERS_PAGING;
		 $content = array();
		 $data = $this->cf->selectMultiRecords($Query);
		 for($k=0;$k<count($data);$k++)
		 {

			 $content[$k]['name'] 					= "";
			 $content[$k]['tbl_notification_rpt_id'] = $data[$k]['tbl_notification_rpt_id'];
			 $content[$k]['added_date'] 			  = $data[$k]['added_date']; 
			 $content[$k]['message'] 				 = $data[$k]['message'];
			 $content[$k]['url'] 					 = "";
			 $content[$k]['item_id'] 				 = $data[$k]['item_id'];
			 $tbl_notification_rpt_id                = $data[$k]['tbl_notification_rpt_id'];
			 
			 if($content[$k]['item_id']<>""){
				 $dataQuery = "SELECT * FROM ".TBL_UPLOADS." WHERE tbl_item_id='".$content[$k]['item_id']."'";
				 $dataRecords = $this->cf->selectMultiRecords($dataQuery);
				 if(empty($dataRecords))
				 {
					  $itemArrays = explode(":", $content[$k]['item_id']);
					  $dataQuery = "SELECT * FROM ".TBL_UPLOADS." WHERE tbl_item_id='".$itemArrays[0]."'";
				 	  $dataRecords = $this->cf->selectMultiRecords($dataQuery);
				 }

				 if(trim($dataRecords[0]['file_name_original']) == "audio.mp4")
				 {
					 $audio 		= $dataRecords[0]['file_name_updated'];
					 $content[$k]['message_audio'] = HOST_URL."/admin/uploads/".$audio; 
					 $content[$k]['message_img'] = "";
					 $content[$k]['message_img_thumb'] 	= "";
				 }else{

					 $image 		  = $dataRecords[0]['file_name_updated'];
					 $image_thumb 	= $dataRecords[0]['file_name_updated_thumb'];
					 $content[$k]['message_img'] = HOST_URL."/admin/uploads/".$image; 
					 $content[$k]['message_img_thumb'] 	= HOST_URL."/admin/uploads/".$image_thumb;
					 $content[$k]['message_audio'] 		= "";
				 }
			 }else{

				 $content[$k]['message_img'] 		= ""; 
				 $content[$k]['message_img_thumb'] 	= "";
				 $content[$k]['message_audio'] 		= "";
			 }
			 
			     //schools list
			     $schools         = $data[$k]['schools'];
				 $school_name_str = "";
				 $schools         = str_replace(",",'","',$schools);
				

				 $tbl_school_id_arr = '"'.$schools.'"';
				 $qry = "SELECT school_name FROM ".TBL_SCHOOL." WHERE tbl_school_id IN (".$tbl_school_id_arr.")";
				 $data_rs = $this->cf->selectMultiRecords($qry);

				 
				 $school_name_str = "";
				 for($j=0;$j<count($data_rs);$j++){
					if($j<>0)
						$school_name_str .= ", ";
					
					$school_name_str .= $data_rs[$j]["school_name"];
				 }

				 $school_name_str = "<strong>Schools:</strong> ".$school_name_str;

                 $content[$k]['school_name'] = $school_name_str;
                 //end schools list
				 
				 $gender      = "--".$data[$k]["gender"];
				 $school_type = "--".$data[$k]["school_type"];
				 
				 if (strpos($gender,"M")>0) {$gender_male = "Male";}
				 if (strpos($gender,"F")>0) {$gender_female = "Female";}
				 
				 if($gender_male<>"" && $gender_female<>""){
						$gender_str = "<strong>Gender</strong>: ".trim($gender_male.", ". $gender_female);
	 			 }else{
						$gender_str = "<strong>Gender</strong>: ".trim($gender_male."". $gender_female);
				}
				
				$content[$k]['gender'] = $gender_str;

				if (strpos($school_type,"PU")>0) {$school_type_public = "Public School";}
				if (strpos($school_type,"PR")>0) {$school_type_private = "Private School";}

				if($school_type_public<>"" && $school_type_private<>""){
					$school_type_str = "<strong>School Type</strong>: ".trim($school_type_public.", ". $school_type_private);
				}else{
		            $school_type_str = "<strong>School Type</strong>: ".trim($school_type_public."". $school_type_private);
				}					

				$content[$k]['school_type'] =  $school_type_str;


				$qryNtCnt = "SELECT COUNT(*) AS cnt FROM ".TBL_NOTIFICATION_DATA_RPT_TEACHERS." WHERE tbl_notification_rpt_id='".$tbl_notification_rpt_id."'";
				$data_rs_cnt = $this->cf->selectMultiRecords($qryNtCnt);
				$cnt_recepients = $data_rs_cnt[0]["cnt"];

				$qry = "SELECT COUNT(*) AS cnt FROM ".TBL_NOTIFICATION_DATA_RPT_TEACHERS." WHERE tbl_notification_rpt_id='".$tbl_notification_rpt_id."' AND is_sent='Y'";
				$data_rs = $this->cf->selectMultiRecords($qry);
				$cnt = $data_rs[0]["cnt"];
			 
			    $content[$k]['cnt_recepients'] =  $cnt_recepients;
			    $content[$k]['cnt_sent']       =  $cnt_sent;
			 
		 }
		 return $content;
	}
	
	
	
	function get_total_ministry_message_to_teachers($q, $is_active, $tbl_school_id,$tbl_teacher_id) {
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
		
		$Query .= "SELECT COUNT(tbl_notification_rpt_id) as cntReport FROM ".TBL_NOTIFICATION_RPT_TEACHERS."  AS MT WHERE 1 ";	
		if($school_id<>""){
		  $Query    .= " AND find_in_set('$tbl_school_id', schools) <> 0 ";
		}
		
		//Search
		if (trim($q) != "") {
			$Query    .= " AND  MT.message LIKE '%$q%' ";
		}
		 $data = $this->cf->selectMultiRecords($Query);
		 return $data[0]['cntReport'];
	}
	
	
	//send ministry message
	function send_ministry_message_to_teacher($tbl_notification_rpt_id,$message,$tbl_school_id,$country,$gender_male,$gender_female,$public_school,$private_school) {
	
	     $gender = "N";
		 $school_type = "N";

		if ($gender_male == "m") {
			$gender = "M";
		}

		if ($gender_female == "f") {
			$gender = $gender.":F";
		}

		if ($pubic_school == "pu") {
			$school_type = "PU";
		}

		if ($private_school == "pr") {
			$school_type = $school_type.":PR";
		}

		
		    $tbl_school_id               = explode("&", $tbl_school_id);
			for($m=0; $m<count($tbl_school_id); $m++) {
			  if($m<>0)
			  {
				$tbl_school_id_t .= ",";
			  }
			  if($tbl_school_id[$m]<>""){
				$tbl_school_id_t .= $tbl_school_id[$m];
			  }
			}
	
		$qry  = "INSERT INTO ".TBL_NOTIFICATION_RPT_TEACHERS." (`tbl_notification_rpt_id` ,`country` ,`gender` ,`school_type` ,`schools` ,`message` ,`is_active` ,`added_date`)
			    VALUES ('$tbl_notification_rpt_id', '$country', '$gender', '$school_type', '$tbl_school_id_t', '$message', 'Y', NOW())";
		echo $qry; exit;
		$this->cf->insertInto($qry);

		if ($male != "" && $female != "") {
			$gender_qry = "";
		} else if ($male != "") {
			$gender_qry = "AND gender='male'";
		} else if ($female != "") {
			$gender_qry = "AND gender='female'";
		}

		if (trim($country) !="" && $country != "All") {
			$country_qry = "AND country='".$country."'";
		}
		$schools_qry_str = $schools_qry_str.',"0000000000"';
		

	    $qryTeachers = "SELECT DISTINCT(T.tbl_teacher_id) AS user_id FROM ".TBL_TEACHER." AS T WHERE is_active='Y' ".$gender_qry." ".$country_qry." AND tbl_school_id IN (".$schools_qry_str.") ";
		$data_rs = SelectMultiRecords($qryTeachers);

		for($i=0;$i<count($data_rs);$i++) {
			$user_type = 'T';
			$user_id = $data_rs[$i]["user_id"];

			$qryNotification = "SELECT NT.* FROM ".TBL_USER_NOTIFY_TOKEN." AS NT WHERE NT.user_type='T' AND NT.is_active='Y' AND NT.user_id ='$user_id'";
			$data_rs_notify = SelectMultiRecords($qryNotification);

			if(count($data_rs_notify)>0)
			{
				$device 	= 	$data_rs_notify[0]['device'];
				$token 		=	$data_rs_notify[0]['token'];
			}else{
				$device 	= 	 "";
				$token 		=    "";
			}

			$qry = "INSERT INTO ".TBL_NOTIFICATION_DATA_RPT_TEACHERS." (`tbl_notification_rpt_id` ,`user_type` ,`user_id` ,`device` ,`token` ,`is_sent` ,`sent_date`)
				VALUES ('$tbl_notification_rpt_id', '$user_type', '$user_id', '$device', '$token', 'N', NOW())";
			$this->cf->insertInto($qry);

			/*
			if(count($data_rs_notify)>0)
			{
				$device 	= 	$data_rs_notify[0]['device'];
				$token 		=	$data_rs_notify[0]['token'];
				$qry = "Update ".TBL_NOTIFICATION_DATA_RPT_TEACHERS." SET device= '$device', token='$token', is_sent='N', sent_date=NOW() where tbl_notification_rpt_id = '$tbl_notification_rpt_id' ";
				update($qry);
			}
			*/
			//send_notification_log($token, $message,  "");
			/*USE CRONE TO SEND NOTIFICATION*/
			//send_notification($token, $message, $device);

		}

		/*$qry = "SELECT id, token, device FROM ".TBL_NOTIFICATION_DATA_RPT_TEACHERS." WHERE tbl_notification_rpt_id='".$tbl_notification_rpt_id."' AND is_sent='N'";
		$data_rs = SelectMultiRecords($qry);
		for($i=0;$i<count($data_rs);$i++) {
			$id     = $data_rs[$i]["id"];
			$device = $data_rs[$i]["device"];
			$token  = $data_rs[$i]["token"];
			$qry = "UPDATE ".TBL_NOTIFICATION_DATA_RPT_TEACHERS." SET is_sent='Y', sent_date=NOW() WHERE id='".$id."'";
			update($qry);
			//send_notification2($token, $message, $device);
		}*/
		//echo $qry3;
	  echo "Y";	

	}//if 


	
	
	
	//END BACKEND MINISTRY NOTIFICATION 
	
	



	/*function listBehaviourReportForSchools()


	{


		 $CountRec = "SELECT * FROM ".TBL_CARD_CATEGORY." WHERE is_active='Y' ";		


		//WHERE tbl_school_id='$tbl_school_id_sess'  now add crad to move govt section. cards common for all schools


		$Query = "SELECT * FROM ".TBL_CARD_CATEGORY." WHERE is_active='Y' ";


		$Query .= " ORDER BY card_category_order $by ";


		$Query .=" LIMIT 0, 15 ";


		$q = stripslashes($q);


		$data = $this->cf->selectMultiRecords($Query);


		//print_r($data); exit;


		$arrayCards = array();


		$dataArray  = array();


		for($m=0;$m<count($data); $m++){


			$card_logo = "";


			$color= "";


			$card_logo = $data[$m]['card_logo'];


			$category_name_en = $data[$m]['category_name_en'];


			$category_name_ar = $data[$m]['category_name_ar'];


			$color = $data[$m]['color'];


			$cardCategoryId = $data[$m]['tbl_card_category_id'];


			$card_point =	$data[$m]["card_point"];


			if($card_logo<>"")


			{


				$card_logo = IMG_GALLERY_PATH."/".$card_logo;


			}


			$arrayCards[$m]['card_logo'] = $card_logo;


			$arrayCards[$m]['tbl_card_category_id'] = $cardCategoryId;


			$arrayCards[$m]['category_name_en'] = $category_name_en;


			$arrayCards[$m]['category_name_ar'] = $category_name_ar;


			$arrayCards[$m]['color'] = $color;


			$arrayCards[$m]['card_point'] = $card_point;


		}


		


		$qry_schools = "SELECT * FROM ".TBL_SCHOOL." WHERE is_active='Y' ";


		$rs_schools = $this->cf->selectMultiRecords($qry_schools);	


		$tbl_school_id_param = array();


		for ($s=0; $s<count($rs_schools); $s++) { 


			$tbl_school_id_param[$s] = $rs_schools[$s]['tbl_school_id'];


			$school_name_s[$s] = $rs_schools[$s]['school_name'];


		}


		$start_date = "";


		$is_male ="";


		$country = "";


		


		$final_arr = array();			  


		for ($i=0; $i<count($tbl_school_id_param); $i++) {


							


				$qry_issued = "SELECT COUNT(*) as total_cards, card_type FROM ".VIEW_TBL_TEACHER_CARDS;


				$qry_cancelled = "SELECT COUNT(*) as total_cards, card_type FROM ".VIEW_TBL_TEACHER_CARDS;


							


				$qry_issued .= " WHERE tbl_school_id='".$tbl_school_id_param[$i]."' AND card_issue_type='issue' "; 


				$qry_cancelled .= " WHERE tbl_school_id='".$tbl_school_id_param[$i]."' AND card_issue_type='cancel' ";


			


				


				if($start_date<>""){


					$qry_issued .= " AND (added_date>='$start_date' AND added_date<='$end_date') ";


					$qry_cancelled .= " AND (added_date>='$start_date' AND added_date<='$end_date') ";


				}


				if ($is_male == "Y" && $is_female == "N") {


					$qry_issued .= " AND gender='male'";


					$qry_cancelled .= " AND gender='male'";


				}


				if ($is_male == "N" && $is_female == "Y") {


					$qry_issued .= " AND gender='female'";


					$qry_cancelled .= " AND gender='female'";


				}


				if (trim($country) != "") {


					$qry_issued .= " AND country='$country' ";


					$qry_cancelled .= " AND country='$country' ";


				}


						


					$qry_issued .= " GROUP BY card_type";


					$qry_cancelled .= " GROUP BY card_type";


							


					//echo $qry_issued."<br />"; exit;


					//echo $qry_cancelled."<br />";


					$rs_issued = $this->cf->selectMultiRecords($qry_issued);	


					//print_r($rs_issued); 


					$rs_cancelled = $this->cf->selectMultiRecords($qry_cancelled);		


					$total = 0;


					for ($k=0; $k<count($rs_issued); $k++) {


					$card_type_k = $rs_issued[$k]['card_type'];


					$total_cards_k = $rs_issued[$k]['total_cards'];


								


								


					$totalCards[$tbl_school_id_param[$i]][$card_type_k]= $total_cards_k;


								$total = $total + $totalCards[$tbl_school_id_param[$i]][$card_type_k];


									


							}//for ($k=0; $k<count($rs_issued); $k++)


							


							


							for ($m=0; $m<count($rs_cancelled); $m++) {


								$card_type_m = $rs_cancelled[$m]['card_type'];


								$total_cards_m = $rs_cancelled[$m]['total_cards'];


								


								


								


								$totalCards[$tbl_school_id_param[$i]][$card_type_m] = $totalCards[$tbl_school_id_param[$i]][$card_type_m] - $total_cards_m;


								$total =  $total - $totalCards[$tbl_school_id_param[$i]][$card_type_m];


					       }


					$totalCards[$tbl_school_id_param[$i]][$total_cards] = $total;	


					


					//for ($m=0; $m<count($rs_cancelled); $m++)


				    $final_arr[$i]['school_name'] =  $this->get_School_name($tbl_school_id_param[$i]);


					$final_arr[$i]['tbl_school_id']= $tbl_school_id_param[$i];


					//print_r($final_arr);


                 $cntCard = 0;


			  // print_r($tbl_school_id_param);


			   for($m=0;$m<count($data); $m++){ 


                  if(isset($totalCards[$tbl_school_id_param[$i]][$data[$m]['tbl_card_category_id']]))


                  {


                  	$cntCard = $totalCards[$tbl_school_id_param[$i]][$data[$m]['tbl_card_category_id']];


                   }else{


                    $cntCard = 0;


                   }


				   $final_arr[$i][$m]['tbl_card_category_id']	= $data[$m]['tbl_card_category_id'];


				   $final_arr[$i][$m]['cntCard']			    = $cntCard;


               } 


			   $final_arr[$i]['total_cards']= $totalCards[$tbl_school_id_param[$i]][$total_cards];


               


         }


		$dataArray['cardsList'] =  $arrayCards;


		$dataArray['cardsCnt']  =  $final_arr;


		return $dataArray;


	}*/


	


	


	


	


}


?>





