<?php
include_once('include/common_functions.php');
/**
 * @desc   	  	Forum Model
 * @category   	Model
 * @author     	Shanavas PK
 * @version    	0.1
 */

class Model_forum extends CI_Model {

	var $cf;
	/**
	* @desc Default constructor for the Controller
	* @access default
	*/

    function model_forum() {
		$this->cf = new Common_functions();
    }


	
	// BACKEND FUNCTIONALITY
		/**
	* @desc		Get all topics
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function get_all_topics($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry = "SELECT * FROM ".TBL_PARENT_FORUM." AS PF WHERE 1 ";
		$qry .= " AND PF.tbl_school_id= '".$tbl_school_id."' ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PF.is_status='$is_active' ";
		}
		$qry .= " AND PF.is_status<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( PF.forum_topic LIKE '%$q%' ) ";
		}
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY PF.id DESC ";
		}
		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_FORUM_TOPICS_PAGING;
		}
		$results = $this->cf->selectMultiRecords($qry);
		
		for($c=0;$c<count($results);$c++)
		{
			$results[$c]['cntComments']  = $this->cntCommnets($results[$c]['tbl_parent_forum_id'],$tbl_school_id);
		}
	return $results;
	}
	
	
	function cntCommnets($tbl_parent_forum_id,$tbl_school_id)
	{
		$qry = "SELECT tbl_parent_comment_id FROM ".TBL_PARENT_FORUM_COMMENTS." WHERE tbl_parent_forum_id ='".$tbl_parent_forum_id."'  AND tbl_school_id='".$tbl_school_id."' 
		        AND is_status<>'D'  ";
        $results = $this->cf->selectMultiRecords($qry);
		return count($results);
	}
	
	
	/**
	* @desc		Get total topics
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_topics($q, $is_active, $tbl_school_id) {
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry =  "SELECT COUNT(distinct(PF.tbl_parent_forum_id)) as total_topics FROM ".TBL_PARENT_FORUM." AS PF WHERE 1 ";
		$qry .= " AND PF.tbl_school_id= '".$tbl_school_id."' ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PF.is_status='$is_active' ";
		}
		$qry .= " AND PF.is_status<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( PF.forum_topic LIKE '%$q%' ) ";
		}
		$results = $this->cf->selectMultiRecords($qry);
	    return $results[0]['total_topics'];
	}
	

	function is_exist_topic($tbl_parent_forum_id, $forum_topic, $tbl_school_id) {
		$tbl_parent_forum_id  	   = $this->cf->get_data($tbl_parent_forum_id);
		$forum_topic 		       = $this->cf->get_data($forum_topic);

		$qry = "SELECT * FROM ".TBL_PARENT_FORUM." WHERE 1 ";
		if (trim($forum_topic) != "") {
			 $qry .= " AND forum_topic='$forum_topic' ";
		}
		if (trim($tbl_parent_forum_id) != "") {
			 $qry .= " AND tbl_parent_forum_id<>'$tbl_parent_forum_id' ";
		}
	    $qry .= " AND is_status<>'D' "; 
		$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}
	
	
	/**
	* @desc		Add forum topic
	* @param	String params
	* @access	default
	*/
	function add_forum_topic($tbl_parent_forum_id, $forum_topic, $tbl_parent_group_id, $tbl_school_id)
    {
		$tbl_parent_forum_id     = $this->cf->get_data($tbl_parent_forum_id);
		$forum_topic        	 = $this->cf->get_data($forum_topic);
		$tbl_parent_group_id     = $this->cf->get_data($tbl_parent_group_id);
		$tbl_school_id           = $this->cf->get_data($tbl_school_id);
		$posted_by               = $_SESSION['aqdar_smartcare']['tbl_admin_user_id_sess'];
		if($tbl_school_id<>"")
		{
			$qry_ins = "INSERT INTO ".TBL_PARENT_FORUM." (`tbl_parent_forum_id`, `forum_topic`, `tbl_parent_group_id`, `posted_by`, `added_date`, `is_status`, `tbl_school_id`)
						VALUES ('$tbl_parent_forum_id', '$forum_topic', '$tbl_parent_group_id', '$posted_by', NOW(), 'Y', '$tbl_school_id')";
			$this->cf->insertInto($qry_ins);
			return "Y";
		}else{
		   return "N";	
			
		}
	}
	
	
	function get_parent_group_list_of_topic($tbl_parent_group_id,$tbl_school_id) {
		$tbl_parent_group_id = str_replace("||","','",$tbl_parent_group_id);
		$qry = " SELECT group_name_en,group_name_ar FROM ".TBL_PARENT_GROUP." WHERE tbl_parent_group_id IN ('".$tbl_parent_group_id."') ORDER BY group_name_en ASC ";
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
	}
	
	
	
	/**
	* @desc		Activate topic
	* @param	string $tbl_event_id
	* @access	default
	*/
	function activate_topic($tbl_parent_forum_id,$tbl_school_id) {
		$tbl_parent_forum_id = $this->cf->get_data(trim($tbl_parent_forum_id));
		$qry = "UPDATE ".TBL_PARENT_FORUM." SET is_status='Y' WHERE tbl_parent_forum_id='$tbl_parent_forum_id' AND  tbl_school_id='$tbl_school_id' ";
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate topic
	* @param	string $tbl_student_id
	* @access	default
	*/
	function deactivate_topic($tbl_parent_forum_id,$tbl_school_id) {
		$tbl_parent_forum_id = $this->cf->get_data(trim($tbl_parent_forum_id));
		
		$qry = "UPDATE ".TBL_PARENT_FORUM." SET is_status='N' WHERE tbl_parent_forum_id='$tbl_parent_forum_id' AND  tbl_school_id='$tbl_school_id' ";
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete topic
	* @param	string $tbl_student_id
	* @access	default
	*/
	function delete_topic($tbl_parent_forum_id,$tbl_school_id) {
		$tbl_parent_forum_id = $this->cf->get_data(trim($tbl_parent_forum_id));
        $qry = "UPDATE ".TBL_PARENT_FORUM." SET is_status='D' WHERE tbl_parent_forum_id='$tbl_parent_forum_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_CATEGORY." WHERE tbl_category_id='$tbl_category_id' ";
		//$this->cf->deleteFrom($qry);
	}
	
	
	
	function get_topic_obj($tbl_parent_forum_id,$tbl_school_id) {
		$qry = "SELECT * FROM ".TBL_PARENT_FORUM." WHERE 1  AND tbl_parent_forum_id='$tbl_parent_forum_id' AND  tbl_school_id='$tbl_school_id'";
		$results = $this->cf->selectMultiRecords($qry);
		$topic_obj = array();
		$topic_obj['tbl_parent_forum_id']= $results[0]['tbl_parent_forum_id'];
		$topic_obj['forum_topic']        = $results[0]['forum_topic'];
		$topic_obj['tbl_parent_group_id']= $results[0]['tbl_parent_group_id'];
		
	   return $topic_obj;
	}

	function save_topic_changes($tbl_parent_forum_id, $forum_topic, $str, $tbl_school_id)
	{
	    $tbl_parent_forum_id      = $this->cf->get_data($tbl_parent_forum_id);
		$forum_topic        	  = $this->cf->get_data($forum_topic);
		$tbl_parent_group_id      = $this->cf->get_data($str);
		$tbl_school_id            = $this->cf->get_data($tbl_school_id);
		
		if($tbl_school_id<>"")
		{
			$qry  = "UPDATE ".TBL_PARENT_FORUM." SET forum_topic='$forum_topic', tbl_parent_group_id='$tbl_parent_group_id' 
			         WHERE tbl_parent_forum_id = '$tbl_parent_forum_id' and tbl_school_id = '$tbl_school_id' ";
			$this->cf->update($qry);
		    return "Y";
		}else{
		   return "N";	
			
		}
	}
	
	// FORUM TOPICS END
	
	//FORUM COMMENTS START
	
    /**
	* @desc		Get all comments
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function get_all_comments($sort_name, $sort_by, $offset, $q, $is_active, $tbl_parent_forum_id, $tbl_school_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
		$tbl_parent_forum_id  = $this->cf->get_data($tbl_parent_forum_id);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry = "SELECT PFC.*, P.first_name,P.last_name,P.first_name_ar,P.last_name_ar FROM ".TBL_PARENT_FORUM_COMMENTS." AS PFC 
		        LEFT JOIN ".TBL_PARENT." AS P ON P.tbl_parent_id = PFC.commented_by WHERE 1 ";
		$qry .= " AND PFC.tbl_school_id= '".$tbl_school_id."' ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PFC.is_status='$is_active' ";
		}
		$qry .= " AND PFC.is_status<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( PFC.forum_comment LIKE '%$q%'  
			               OR CONCAT(P.first_name,' ',P.last_name) LIKE '%$q%' 
					   	   OR CONCAT(TRIM(P.first_name_ar),' ',TRIM(P.last_name_ar)) LIKE '%$q%' ) ";
		}
		
		//Search
		if (trim($tbl_parent_forum_id) != "") {
			$qry    .= " 	AND PFC.tbl_parent_forum_id = '$tbl_parent_forum_id'  ";
		}
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY PFC.$sort_name $sort_by";
		} else {
			$qry .= " ORDER BY PFC.id DESC ";
		}
		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_FORUM_COMMENTS_PAGING;
		}
		$results = $this->cf->selectMultiRecords($qry);
	
	return $results;
	}
	

	
	/**
	* @desc		Get total comments
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_comments($q, $is_active, $tbl_parent_forum_id, $tbl_school_id){
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$tbl_parent_forum_id  = $this->cf->get_data($tbl_parent_forum_id);
		$qry =  "SELECT COUNT(distinct(PFC.tbl_parent_comment_id)) as total_comments FROM ".TBL_PARENT_FORUM_COMMENTS." AS PFC 
		        LEFT JOIN ".TBL_PARENT." AS P ON P.tbl_parent_id = PFC.commented_by WHERE 1 ";
		$qry .= " AND PFC.tbl_school_id= '".$tbl_school_id."' ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PFC.is_status='$is_active' ";
		}
		$qry .= " AND PFC.is_status<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( PFC.forum_comment LIKE '%$q%'  
			               OR CONCAT(P.first_name,' ',P.last_name) LIKE '%$q%' 
					   	   OR CONCAT(TRIM(P.first_name_ar),' ',TRIM(P.last_name_ar)) LIKE '%$q%' ) ";
		}
		
		//Search
		if (trim($tbl_parent_forum_id) != "") {
			$qry    .= " 	AND PFC.tbl_parent_forum_id ='$tbl_parent_forum_id'  ";
		}
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( PFC.forum_comment LIKE '%$q%' ) ";
		}
		$results = $this->cf->selectMultiRecords($qry);
	    return $results[0]['total_comments'];
	}
	
	/**
	* @desc		Activate comment
	* @param	string $tbl_event_id
	* @access	default
	*/
	function activate_comment($tbl_parent_comment_id,$tbl_school_id) {
		$tbl_parent_comment_id = $this->cf->get_data(trim($tbl_parent_comment_id));
		$qry = "UPDATE ".TBL_PARENT_FORUM_COMMENTS." SET is_status='Y' WHERE tbl_parent_comment_id='$tbl_parent_comment_id' AND  tbl_school_id='$tbl_school_id' ";
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate comment
	* @param	string $tbl_student_id
	* @access	default
	*/
	function deactivate_comment($tbl_parent_comment_id,$tbl_school_id) {
		$tbl_parent_comment_id = $this->cf->get_data(trim($tbl_parent_comment_id));
		
		$qry = "UPDATE ".TBL_PARENT_FORUM_COMMENTS." SET is_status='N' WHERE tbl_parent_comment_id='$tbl_parent_comment_id' AND  tbl_school_id='$tbl_school_id' ";
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete comment
	* @param	string $tbl_student_id
	* @access	default
	*/
	function delete_comment($tbl_parent_comment_id,$tbl_school_id) {
		$tbl_parent_comment_id = $this->cf->get_data(trim($tbl_parent_comment_id));
        $qry = "UPDATE ".TBL_PARENT_FORUM_COMMENTS." SET is_status='D' WHERE 	tbl_parent_comment_id='$tbl_parent_comment_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_CATEGORY." WHERE tbl_category_id='$tbl_category_id' ";
		//$this->cf->deleteFrom($qry);
	}
	//FORUM COMMENTS END
	
	
	// END BACKEND FUNCTIONALITY
	
	// STUDENT FORUM TOPICS & COMMENTS START
	function get_all_student_topics($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry = "SELECT * FROM ".TBL_STUDENT_FORUM." AS PF WHERE 1 ";
		$qry .= " AND PF.tbl_school_id= '".$tbl_school_id."' ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PF.is_status='$is_active' ";
		}
		$qry .= " AND PF.is_status<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( PF.forum_topic LIKE '%$q%' ) ";
		}
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY PF.id DESC ";
		}
		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_FORUM_TOPICS_PAGING;
		}
		$results = $this->cf->selectMultiRecords($qry);
		
		for($c=0;$c<count($results);$c++)
		{
			$results[$c]['cntComments']  = $this->cntStudentCommnets($results[$c]['tbl_student_forum_id'],$tbl_school_id);
		}
	return $results;
	}
	
	
	function cntStudentCommnets($tbl_student_forum_id,$tbl_school_id)
	{
		$qry = "SELECT tbl_student_comment_id FROM ".TBL_STUDENT_FORUM_COMMENTS." WHERE tbl_student_forum_id ='".$tbl_student_forum_id."'  AND tbl_school_id='".$tbl_school_id."' 
		        AND is_status<>'D'  ";
        $results = $this->cf->selectMultiRecords($qry);
		return count($results);
	}
	
	
	/**
	* @desc		Get total student topics
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_student_topics($q, $is_active, $tbl_school_id) {
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry =  "SELECT COUNT(distinct(PF.tbl_student_forum_id)) as total_topics FROM ".TBL_STUDENT_FORUM." AS PF WHERE 1 ";
		$qry .= " AND PF.tbl_school_id= '".$tbl_school_id."' ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PF.is_status='$is_active' ";
		}
		$qry .= " AND PF.is_status<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( PF.forum_topic LIKE '%$q%' ) ";
		}
		$results = $this->cf->selectMultiRecords($qry);
	    return $results[0]['total_topics'];
	}
	

	function is_exist_student_topic($tbl_student_forum_id, $forum_topic, $tbl_school_id) {
		$tbl_student_forum_id  	   = $this->cf->get_data($tbl_student_forum_id);
		$forum_topic 		       = $this->cf->get_data($forum_topic);

		$qry = "SELECT * FROM ".TBL_STUDENT_FORUM." WHERE 1 ";
		if (trim($forum_topic) != "") {
			 $qry .= " AND forum_topic='$forum_topic' ";
		}
		if (trim($tbl_student_forum_id) != "") {
			 $qry .= " AND tbl_student_forum_id<>'$tbl_student_forum_id' ";
		}
	    $qry .= " AND is_status<>'D' "; 
		$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}
	
	
	/**
	* @desc		Add forum topic
	* @param	String params
	* @access	default
	*/
	function add_forum_student_topic($tbl_student_forum_id, $forum_topic, $tbl_student_group_id, $tbl_school_id)
    {
		$tbl_student_forum_id     = $this->cf->get_data($tbl_student_forum_id);
		$forum_topic        	  = $this->cf->get_data($forum_topic);
		$tbl_student_group_id     = $this->cf->get_data($tbl_student_group_id);
		$tbl_school_id            = $this->cf->get_data($tbl_school_id);
		$posted_by                = $_SESSION['aqdar_smartcare']['tbl_admin_user_id_sess'];
		if($tbl_school_id<>"")
		{
			$qry_ins = "INSERT INTO ".TBL_STUDENT_FORUM." (`tbl_student_forum_id`, `forum_topic`, `tbl_student_group_id`, `posted_by`, `added_date`, `is_status`, `tbl_school_id`)
						VALUES ('$tbl_student_forum_id', '$forum_topic', '$tbl_student_group_id', '$posted_by', NOW(), 'Y', '$tbl_school_id')";
			$this->cf->insertInto($qry_ins);
			return "Y";
		}else{
		   return "N";	
			
		}
	}
	
	
	function get_student_group_list_of_topic($tbl_student_group_id,$tbl_school_id) {
		$tbl_student_group_id = str_replace("||","','",$tbl_student_group_id);
		$qry = " SELECT group_name_en,group_name_ar FROM ".TBL_STUDENT_GROUP." WHERE tbl_student_group_id IN ('".$tbl_student_group_id."') ORDER BY group_name_en ASC ";
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
	}
	
	
	
	/**
	* @desc		Activate topic
	* @param	string $tbl_event_id
	* @access	default
	*/
	function activate_student_topic($tbl_student_forum_id,$tbl_school_id) {
		$tbl_student_forum_id = $this->cf->get_data(trim($tbl_student_forum_id));
		$qry = "UPDATE ".TBL_STUDENT_FORUM." SET is_status='Y' WHERE tbl_student_forum_id='$tbl_student_forum_id' AND  tbl_school_id='$tbl_school_id' ";
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate topic
	* @param	string $tbl_student_id
	* @access	default
	*/
	function deactivate_student_topic($tbl_student_forum_id,$tbl_school_id) {
		$tbl_student_forum_id = $this->cf->get_data(trim($tbl_student_forum_id));
		
		$qry = "UPDATE ".TBL_STUDENT_FORUM." SET is_status='N' WHERE tbl_student_forum_id='$tbl_student_forum_id' AND  tbl_school_id='$tbl_school_id' ";
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete topic
	* @param	string $tbl_student_id
	* @access	default
	*/
	function delete_student_topic($tbl_student_forum_id,$tbl_school_id) {
		$tbl_student_forum_id = $this->cf->get_data(trim($tbl_student_forum_id));
        $qry = "UPDATE ".TBL_STUDENT_FORUM." SET is_status='D' WHERE tbl_student_forum_id='$tbl_student_forum_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_CATEGORY." WHERE tbl_category_id='$tbl_category_id' ";
		//$this->cf->deleteFrom($qry);
	}
	
	
	
	function get_student_topic_obj($tbl_student_forum_id,$tbl_school_id) {
		$qry = "SELECT * FROM ".TBL_STUDENT_FORUM." WHERE 1  AND tbl_student_forum_id='$tbl_student_forum_id' AND  tbl_school_id='$tbl_school_id'";
		$results = $this->cf->selectMultiRecords($qry);
		$topic_obj = array();
		$topic_obj['tbl_student_forum_id']= $results[0]['tbl_student_forum_id'];
		$topic_obj['forum_topic']         = $results[0]['forum_topic'];
		$topic_obj['tbl_student_group_id']= $results[0]['tbl_student_group_id'];
		
	   return $topic_obj;
	}

	function save_student_topic_changes($tbl_student_forum_id, $forum_topic, $str, $tbl_school_id)
	{
	    $tbl_student_forum_id      = $this->cf->get_data($tbl_student_forum_id);
		$forum_topic        	   = $this->cf->get_data($forum_topic);
		$tbl_student_group_id      = $this->cf->get_data($str);
		$tbl_school_id             = $this->cf->get_data($tbl_school_id);
		
		if($tbl_school_id<>"")
		{
			$qry  = "UPDATE ".TBL_STUDENT_FORUM." SET forum_topic='$forum_topic', tbl_student_group_id='$tbl_student_group_id' 
			         WHERE tbl_student_forum_id = '$tbl_student_forum_id' and tbl_school_id = '$tbl_school_id' ";
			$this->cf->update($qry);
		    return "Y";
		}else{
		   return "N";	
			
		}
	}
	
	// FORUM TOPICS END
	
	//FORUM COMMENTS START
	
    /**
	* @desc		Get all comments
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function get_all_student_comments($sort_name, $sort_by, $offset, $q, $is_active, $tbl_student_forum_id, $tbl_school_id) {
		$sort_name  			= $this->cf->get_data($sort_name);
		$sort_by 				= $this->cf->get_data($sort_by);
		$offset     			= $this->cf->get_data($offset);
		$q          			= urldecode($this->cf->get_data($q));
		$is_active  			= $this->cf->get_data($is_active);
		$tbl_student_forum_id   = $this->cf->get_data($tbl_student_forum_id);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry = "SELECT PFC.*, P.first_name,P.last_name,P.first_name_ar,P.last_name_ar FROM ".TBL_STUDENT_FORUM_COMMENTS." AS PFC 
		        LEFT JOIN ".TBL_STUDENT." AS P ON P.tbl_student_id = PFC.commented_by WHERE 1 ";
		$qry .= " AND PFC.tbl_school_id= '".$tbl_school_id."' ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PFC.is_status='$is_active' ";
		}
		$qry .= " AND PFC.is_status<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( PFC.forum_comment LIKE '%$q%'  
			               OR CONCAT(P.first_name,' ',P.last_name) LIKE '%$q%' 
					   	   OR CONCAT(TRIM(P.first_name_ar),' ',TRIM(P.last_name_ar)) LIKE '%$q%' ) ";
		}
		
		//Search
		if (trim($tbl_student_forum_id) != "") {
			$qry    .= " 	AND PFC.tbl_student_forum_id = '$tbl_student_forum_id'  ";
		}
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY PFC.$sort_name $sort_by";
		} else {
			$qry .= " ORDER BY PFC.id DESC ";
		}
		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_FORUM_COMMENTS_PAGING;
		}
		$results = $this->cf->selectMultiRecords($qry);
	
	return $results;
	}
	

	
	/**
	* @desc		Get total comments
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_student_comments($q, $is_active, $tbl_student_forum_id, $tbl_school_id){
		$q         				= urldecode($this->cf->get_data($q));
		$is_active 				= $this->cf->get_data($is_active);
		$tbl_student_forum_id  	= $this->cf->get_data($tbl_student_forum_id);
		$qry =  "SELECT COUNT(distinct(PFC.tbl_student_comment_id)) as total_comments FROM ".TBL_STUDENT_FORUM_COMMENTS." AS PFC 
		        LEFT JOIN ".TBL_STUDENT." AS S ON S.tbl_student_id = PFC.commented_by WHERE 1 ";
		$qry .= " AND PFC.tbl_school_id= '".$tbl_school_id."' ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PFC.is_status='$is_active' ";
		}
		$qry .= " AND PFC.is_status<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( PFC.forum_comment LIKE '%$q%'  
			               OR CONCAT(S.first_name,' ',S.last_name) LIKE '%$q%' 
					   	   OR CONCAT(TRIM(S.first_name_ar),' ',TRIM(S.last_name_ar)) LIKE '%$q%' ) ";
		}
		
		//Search
		if (trim($tbl_student_forum_id) != "") {
			$qry    .= " 	AND PFC.tbl_student_forum_id ='$tbl_student_forum_id'  ";
		}
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( PFC.forum_comment LIKE '%$q%' ) ";
		}
		$results = $this->cf->selectMultiRecords($qry);
	    return $results[0]['total_comments'];
	}
	
	/**
	* @desc		Activate comment
	* @param	string $tbl_student_comment_id
	* @access	default
	*/
	function activate_student_comment($tbl_student_comment_id,$tbl_school_id) {
		$tbl_student_comment_id = $this->cf->get_data(trim($tbl_student_comment_id));
		$qry = "UPDATE ".TBL_STUDENT_FORUM_COMMENTS." SET is_status='Y' WHERE tbl_student_comment_id='$tbl_student_comment_id' AND  tbl_school_id='$tbl_school_id' ";
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate comment
	* @param	string $tbl_student_id
	* @access	default
	*/
	function deactivate_student_comment($tbl_student_comment_id,$tbl_school_id) {
		$tbl_student_comment_id = $this->cf->get_data(trim($tbl_student_comment_id));
		$qry = "UPDATE ".TBL_STUDENT_FORUM_COMMENTS." SET is_status='N' WHERE tbl_student_comment_id='$tbl_student_comment_id' AND  tbl_school_id='$tbl_school_id' ";
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete comment
	* @param	string $tbl_student_id
	* @access	default
	*/
	function delete_student_comment($tbl_student_comment_id,$tbl_school_id) {
		$tbl_student_comment_id = $this->cf->get_data(trim($tbl_student_comment_id));
        $qry = "UPDATE ".TBL_STUDENT_FORUM_COMMENTS." SET is_status='D' WHERE 	tbl_student_comment_id='$tbl_student_comment_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_CATEGORY." WHERE tbl_category_id='$tbl_category_id' ";
		//$this->cf->deleteFrom($qry);
	}
	//FORUM COMMENTS END
	
	
    // END BACKEND FUNCTIONALITY
	

	
	// END STUDENT FORUM TOPICS & COMMENTS END
	
	
	
	
	
	
	
	

    }

?>



