<?php
include_once('include/common_functions.php');

/**
 * @desc   	  	Attendance Model
 *
 * @category   	Model
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Model_attendance extends CI_Model {
	var $cf;


	/**
	* @desc Default constructor for the Controller
	*
	* @access default
	*/
    function model_attendance() {
		$this->cf = new Common_functions();
    }
	

	/**
	* @desc		Get attendances against class
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_all_attendances_against_class($tbl_class_id) {
		$tbl_class_id = $this->cf->get_data(trim($tbl_class_id));
		$qry = "SELECT * FROM ".TBL_STUDENT." WHERE tbl_class_id='$tbl_class_id' AND is_active='Y' ORDER BY first_name ASC ";
		//echo $qry."<br />";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;	
	}
	
	
	/**
	* @desc		Get attendances against class
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_attendance($tbl_student_id, $tbl_bus_id, $attendance_date,  $att_option){
		$qry = "SELECT is_present FROM ".TBL_ATTENDANCE." WHERE tbl_student_id='$tbl_student_id' AND tbl_bus_id='$tbl_bus_id' AND attendance_date='$attendance_date' AND att_option='$att_option' AND is_active='Y'";
		//echo $qry."<br />";
		$rs = $this->cf->selectMultiRecords($qry);
		if (trim($rs[0]["is_present"]) == "") {return "N";}
		return $rs[0]["is_present"];	
	}
	
	
	/**
	* @desc		Save attendance
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function save_attendance($tbl_student_id, $tbl_bus_id, $attendance_date, $is_present, $att_option, $tbl_school_id) {
		$tbl_attendance_id = substr(md5(uniqid(rand())),0,10);
		$qry = "INSERT INTO ".TBL_ATTENDANCE." (
			`tbl_attendance_id` ,
			`att_option` ,
			`tbl_student_id` ,
			`tbl_bus_id` ,
			`attendance_date` ,
			`is_present` ,
			`is_active` ,
			`added_date` ,
			`tbl_school_id`
			)
			VALUES (
			'$tbl_attendance_id', '$att_option', '$tbl_student_id', '$tbl_bus_id', '$attendance_date', '$is_present', 'Y', NOW(), '$tbl_school_id'
			)";
		
		//echo $qry."<br />";
		$this->cf->insertInto($qry);
	}
	
	
	/**
	* @desc		Delate old attendance data if any, for a given bus on particular date for either morning or afternoon session.
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function delete_attendance($tbl_bus_id, $attendance_date, $att_option) {
		$qry = "DELETE FROM ".TBL_ATTENDANCE." WHERE tbl_bus_id='".$tbl_bus_id."' AND attendance_date='".$attendance_date."' AND att_option='".$att_option."'";
		//echo $qry."<br />";
		$this->cf->deleteFrom($qry);
	}
	
	
	/**
	* @desc		Get attendances details 
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_attendance_details($attendance_date, $att_option, $tbl_teacher_id_sel) {
		$qry = "SELECT tbl_student_id FROM ".TBL_STUDENT." WHERE is_active='Y' AND tbl_class_id IN (SELECT tbl_class_id FROM ".TBL_TEACHER_CLASS." WHERE tbl_teacher_id='".$tbl_teacher_id_sel."')";
		//echo $qry;
		
		$rs = $this->cf->selectMultiRecords($qry);
		for ($i=0; $i<count($rs); $i++) {
			$str = $str."'".$rs[$i]["tbl_student_id"]."',";
		}
		if ($str != "") {
			$str = substr($str, 0, -1);
		} else {
			return $rs;
		}
		
		$qry = "SELECT * FROM ".TBL_ATTENDANCE." WHERE attendance_date='".$attendance_date."' AND att_option='".$att_option."' AND is_active='Y' AND tbl_student_id IN (".$str.")";
		//echo $qry."<br />";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
	
	
	/**
	* @desc		Get child attendance listing
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_child_bus_attendance($tbl_student_id_sel) {
		$qry = "SELECT * FROM ".TBL_ATTENDANCE." WHERE tbl_student_id='".$tbl_student_id_sel."' AND is_active='Y' ORDER BY attendance_date DESC LIMIT 0,60";
		//echo $qry."<br />";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
    }
?>

