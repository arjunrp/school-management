<?php
include_once('include/common_functions.php');

/**
 * @desc   	  	Teacher Group Model
 *
 * @category   	Model
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Model_teacher_group extends CI_Model {
	var $cf;


	/**
	* @desc Default constructor for the Controller
	*
	* @access default
	*/
    function model_teacher_group() {
		$this->cf = new Common_functions();
    }


	/**
	* @desc		Get group deails
	* 
	* @param	string $tbl_teacher_group_id  
	* @access	default
	* @return	$rs
	*/
	function get_group_details($tbl_teacher_group_id) {
		
		$qry = "SELECT * FROM ".TBL_TEACHER_GROUP." WHERE tbl_teacher_group_id='$tbl_teacher_group_id' ";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
	return $rs;
	}
}
?>

