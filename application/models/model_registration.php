<?php
include_once('include/common_functions.php');
/**
* @desc   	  	Registration Model
* @category   	Model
* @author     	Shanavas PK
* @version    	0.1
*/

class Model_registration extends CI_Model {
	var $cf;
	/**
	* @desc Default constructor for the Controller
	* @access default
	*/

	function model_registration() {
		$this->cf = new Common_functions();
	}

	/**
	* @desc	 Student registration 
	* @param	
	* @access	default
	* @return	array $rs
	*/
    // STUDENT REGISTRATION - MOBILE APP (STUDENT + PARENT REGISTRATION)
	function student_registration($tbl_student_id, $first_name, $last_name, $first_name_ar, $last_name_ar, $dob_month, $dob_day, $dob_year, 
		                                             $gender, $mobile, $email, $country, $emirates_id_father, $emirates_id_mother, $tbl_academic_year_id, $tbl_class_id, $tbl_school_id,$tbl_emirates_id,$is_active)
    {
		$tbl_student_id    = $this->cf->get_data($tbl_student_id);
		$first_name        = $this->cf->get_data($first_name);
		$last_name         = $this->cf->get_data($last_name);
		$first_name_ar     = $this->cf->get_data($first_name_ar);
		$last_name_ar      = $this->cf->get_data($last_name_ar);
		$dob_month         = $this->cf->get_data($dob_month);
		$dob_day           = $this->cf->get_data($dob_day);
		$dob_year          = $this->cf->get_data($dob_year);
		$gender            = $this->cf->get_data($gender);
		$mobile            = $this->cf->get_data($mobile);
		$email             = $this->cf->get_data($email);
		$country           = $this->cf->get_data($country);
		$emirates_id_father= $this->cf->get_data($emirates_id_father);
		$emirates_id_mother= $this->cf->get_data($emirates_id_mother);
		$tbl_academic_year_id  = $this->cf->get_data($tbl_academic_year_id);
		$tbl_class_id      = $this->cf->get_data($tbl_class_id);
		$tbl_school_id     = $this->cf->get_data($tbl_school_id);
		$tbl_emirates_id   = $this->cf->get_data($tbl_emirates_id);
		$tbl_emirates_id   = $this->cf->get_data($tbl_emirates_id);
		$is_active         = $this->cf->get_data($is_active);
		
		if($mobile<>"")
			$mobile 	       = "+971".$mobile;
		
		if($tbl_school_id<>"")
		{
			$qry_ins = "INSERT INTO ".TBL_STUDENT." (`tbl_student_id`, `first_name`, `last_name`, `first_name_ar`, `last_name_ar`, `dob_month`, 
			`dob_day`, `dob_year`, `gender`, `mobile`, `email`, `country`, `emirates_id_father`, `emirates_id_mother`, `tbl_academic_year_id` , `tbl_class_id`, `added_date`, `is_active`, `tbl_school_id`,`tbl_emirates_id`)
						VALUES ('$tbl_student_id', '$first_name', '$last_name', '$first_name_ar', '$last_name_ar', '$dob_month',
			'$dob_day', '$dob_year', '$gender', '$mobile ', '$email', '$country', '$emirates_id_father', '$emirates_id_mother', '$tbl_academic_year_id' , '$tbl_class_id', NOW(), '$is_active', '$tbl_school_id', '$tbl_emirates_id')";
			$this->cf->insertInto($qry_ins);
			
			
			$qry = "SELECT file_name_updated,file_name_original,file_type,file_size FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_student_id' AND is_active='Y' ";
			$result = $this->cf->selectMultiRecords($qry);
			if (count($result)>0) {
				$file_name_updated  = isset($result[0]['file_name_updated'])? $result[0]['file_name_updated']:'';
				$file_name_original = isset($result[0]['file_name_original'])? $result[0]['file_name_original']:'';
				$file_type          = isset($result[0]['file_type'])? $result[0]['file_type']:'';
				$file_size          = isset($result[0]['file_size'])? $result[0]['file_size']:'';
				$qry_update  = "UPDATE ".TBL_STUDENT." SET file_name_updated='$file_name_updated', 
				                file_name_original='$file_name_original', file_type='$file_type', file_size='$file_size' WHERE tbl_student_id='$tbl_student_id' ";
				$this->cf->update($qry_update);
			}
			return "Y";
		}else{
		   return "N";	
			
		}
	}
	
	// END STUDENT REGISTRATION
	

	 
	//END STUDENT DETAILS
	
	
	
		

}

?>