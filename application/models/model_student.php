<?php
include_once('include/common_functions.php');
/**
* @desc   	  	Student Model
* @category   	Model
* @author     	Shanavas PK
* @version    	0.1
*/

class Model_student extends CI_Model {
	var $cf;
	/**
	* @desc Default constructor for the Controller
	* @access default
	*/

	function model_student() {
		$this->cf = new Common_functions();
	}

	/**
	* @desc		Get students against class
	* @param	
	* @access	default
	* @return	array $rs
	*/

	function get_all_students_against_class($tbl_class_id) {
		$tbl_class_id = $this->cf->get_data(trim($tbl_class_id));
		$qry_sel = "SELECT * FROM ".TBL_STUDENT." WHERE tbl_class_id='$tbl_class_id' AND is_active='Y' ORDER BY first_name ASC ";
		//echo $qry_sel."<br />";
		$rs = $this->cf->selectMultiRecords($qry_sel);
		return $rs;	
	}

    /**
	* @desc		Save points
	* @param	String $tbl_teacher_id, $tbl_class_id, $tbl_student_id, $points_student, $comments_student
	* @access	default
	* @return	none
	*/
	function save_points($tbl_teacher_id, $tbl_class_id, $tbl_student_id, $points_student, $comments_student, $tbl_school_id,$tbl_point_category_id='',$tbl_semester_id='') {
		$tbl_teacher_id    = $this->cf->get_data(trim($tbl_teacher_id));
		$tbl_class_id      = $this->cf->get_data(trim($tbl_class_id));
		$tbl_student_id    = $this->cf->get_data(trim($tbl_student_id));
		$points_student    = $this->cf->get_data(trim($points_student));
		$comments_student  = $this->cf->get_data(trim($comments_student));
		$tbl_points_id     = substr(md5(uniqid(rand())),0,10);

		$qry = "INSERT INTO ".TBL_POINTS." (`tbl_points_id`, `tbl_teacher_id`, `tbl_class_id`, `tbl_student_id`, `points_student`, `comments_student`, `added_date`, `tbl_school_id`,`tbl_point_category_id`, `tbl_semester_id`)
				VALUES ('$tbl_points_id', '$tbl_teacher_id', '$tbl_class_id', '$tbl_student_id', '$points_student', '$comments_student', NOW(), '$tbl_school_id', '$tbl_point_category_id', '$tbl_semester_id') ";
		$this->cf->insertInto($qry);
	}


	/**
	* @desc		Issue card
	* @param	String $tbl_teacher_id, $tbl_class_id, $tbl_student_id, $card_type
	* @access	default
	* @return	none
	*/
	function issue_card($tbl_teacher_id, $tbl_class_id, $tbl_student_id, $card_type, $comments_student, $card_issue_type, $card_point, $tbl_school_id, $tbl_semester_id) {
		$tbl_teacher_id 	  = $this->cf->get_data(trim($tbl_teacher_id));
		$tbl_class_id 		= $this->cf->get_data(trim($tbl_class_id));
		$tbl_student_id 	  = $this->cf->get_data(trim($tbl_student_id));
		$comments_student    = $this->cf->get_data(trim($comments_student));
		$card_type           = $this->cf->get_data(trim($card_type));
		$tbl_card_id         = substr(md5(uniqid(rand())),0,10);

		$qry = "INSERT INTO ".TBL_CARDS." (`tbl_card_id`, `tbl_teacher_id`, `tbl_class_id`, `tbl_student_id`, `card_type`, `card_issue_type`, `card_point` ,`comments_student`, `added_date`, `tbl_school_id`, `tbl_semester_id`)

				VALUES ('$tbl_card_id', '$tbl_teacher_id', '$tbl_class_id', '$tbl_student_id', '$card_type', '$card_issue_type',  '$card_point', '$comments_student', NOW(), '$tbl_school_id', '$tbl_semester_id') ";
		$this->cf->insertInto($qry);

	}
	

	function issue_cancel_card($tbl_card_id,$tbl_teacher_id, $tbl_class_id, $tbl_student_id, $card_type, $comments_student, $card_issue_type, $card_point, $tbl_school_id, $tbl_semester_id) {
		$tbl_teacher_id 	= $this->cf->get_data(trim($tbl_teacher_id));
		$tbl_class_id 	  = $this->cf->get_data(trim($tbl_class_id));
		$tbl_student_id    = $this->cf->get_data(trim($tbl_student_id));
		$comments_student  = $this->cf->get_data(trim($comments_student));
		$card_type         = $this->cf->get_data(trim($card_type));

		$qry = "INSERT INTO ".TBL_CARDS." (`tbl_card_id`, `tbl_teacher_id`, `tbl_class_id`, `tbl_student_id`, `card_type`, `card_issue_type`, `card_point` ,`comments_student`, `added_date`, `tbl_school_id`, `tbl_semester_id`)
				VALUES ('$tbl_card_id', '$tbl_teacher_id', '$tbl_class_id', '$tbl_student_id', '$card_type', '$card_issue_type',  '$card_point', '$comments_student', NOW(), '$tbl_school_id', '$tbl_semester_id') ";
		$this->cf->insertInto($qry);
	}

	function card_added_point($tbl_card_id) {
		$qry_sel = "SELECT * FROM ".TBL_CARDS." WHERE tbl_CARD_id='".$tbl_card_id."' ";
		$rs = $this->cf->selectMultiRecords($qry_sel);
		return $rs;	
	}
	

	function cancel_card($tbl_card_id) {		
		$qry = "DELETE FROM ".TBL_CARDS." WHERE tbl_CARD_id='".$tbl_card_id."'";
		$this->cf->deleteFrom($qry);
	}


	/**
	* @desc		Register absent
	* @param	String $tbl_teacher_id, $tbl_class_id, $tbl_student_id 
	* @access	default
	* @return	none
	*/
	function register_absent($tbl_teacher_id, $tbl_class_id, $tbl_student_id) {
		$tbl_teacher_id = $this->cf->get_data(trim($tbl_teacher_id));
		$tbl_class_id = $this->cf->get_data(trim($tbl_class_id));
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		$tbl_absent_id = substr(md5(uniqid(rand())),0,10);
		$qry = "INSERT INTO ".TBL_ABSENT." (`tbl_absent_id`, `tbl_teacher_id`, `tbl_class_id`, `tbl_student_id`, `added_date`)
				VALUES ('$tbl_absent_id', '$tbl_teacher_id', '$tbl_class_id', '$tbl_student_id', NOW() ) ";
		//echo $qry;
		$this->cf->insertInto($qry);
	}

	/**
	* @desc		Get total points
	* @param	String $tbl_student_id 
	* @access	default
	* @return	none
	*/

	function get_total_points($tbl_student_id) {
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		$total_points = 0;
		$qry = "SELECT SUM(points_student) AS total_points FROM ".TBL_POINTS." WHERE tbl_student_id='$tbl_student_id' AND is_active='Y' ";
		$rs = $this->cf->SelectMultiRecords($qry);

		if ($rs[0]['total_points'] == "NULL" || !is_numeric($rs[0]['total_points'])) {
			$total_points = 0;
		} else {
			$total_points = $rs[0]['total_points'];
		}
		return $total_points;
	}

	/**
	* @desc		Get cards
	* @param	String $tbl_student_id 
	* @access	default
	* @return	none
	*/

	function get_cards($tbl_student_id, $tbl_semester_id='') {
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		//$qry = "SELECT COUNT(card_type) as total_cards, card_type FROM ".TBL_CARDS." WHERE tbl_student_id='$tbl_student_id' AND is_active='Y' GROUP BY card_type ";
		$qry = "SELECT count( * ) AS cnt , card_type, card_issue_type, sum(card_point) AS card_point
			FROM ".TBL_CARDS." WHERE tbl_student_id='$tbl_student_id' and card_issue_type = 'issue' ";
			if($tbl_semester_id <>"")
			{
				$qry .= " and tbl_semester_id = '$tbl_semester_id' ";
			}
			$qry .= " GROUP BY `card_type` , `card_issue_type`
			ORDER BY card_type";
		//echo $qry;
		$rs = $this->cf->SelectMultiRecords($qry);
		return $rs;	
	}
	

	function get_all_cards_student($tbl_student_id, $tbl_semester_id='', $card_type='') {

		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		//$qry = "SELECT COUNT(card_type) as total_cards, card_type FROM ".TBL_CARDS." WHERE tbl_student_id='$tbl_student_id' AND is_active='Y' GROUP BY card_type ";
		$qry = "SELECT count( * ) AS cnt , card_type, card_issue_type, sum(card_point) AS card_point
			FROM ".TBL_CARDS." WHERE tbl_student_id='$tbl_student_id' ";
			if($tbl_semester_id <>"")
			{
				$qry .= " and tbl_semester_id = '$tbl_semester_id' ";
			}
			if($card_type <>"")
			{
				$qry .= " and card_type = '$card_type' ";
			}
			$qry .= " GROUP BY `card_type` , `card_issue_type`
			ORDER BY card_type";
		$rs = $this->cf->SelectMultiRecords($qry);
		return $rs;	
	}

    function get_all_cancel_cards_student($tbl_student_id, $tbl_semester_id='', $card_type='') {
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		//$qry = "SELECT COUNT(card_type) as total_cards, card_type FROM ".TBL_CARDS." WHERE tbl_student_id='$tbl_student_id' AND is_active='Y' GROUP BY card_type ";
		$qry = "SELECT count( * ) AS cnt , card_type, card_issue_type, sum(card_point) AS card_point
			FROM ".TBL_CARDS." WHERE tbl_student_id='$tbl_student_id' ";
			if($tbl_semester_id <>"")
			{
				$qry .= " and tbl_semester_id = '$tbl_semester_id' ";
			}
			if($card_type <>"")
			{
				$qry .= " and card_type = '$card_type' ";
			}
				$qry .= " and card_issue_type = 'cancel' ";
			$qry .= " GROUP BY `card_type` 
			ORDER BY card_type";
		//echo $qry;
		$rs = $this->cf->SelectMultiRecords($qry);
		return $rs;	
	}

	function get_negative_cards($tbl_student_id,$tbl_class_id,$tbl_teacher_id) {
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		//$qry = "SELECT COUNT(card_type) as total_cards, card_type FROM ".TBL_CARDS." WHERE tbl_student_id='$tbl_student_id' AND is_active='Y' GROUP BY card_type ";
		$qry = "SELECT count( * ) AS cnt , CARDS.card_type, CARDS.card_issue_type, CARDS.tbl_card_id
			FROM ".TBL_CARDS." AS CARDS left join ".TBL_CARD_CATEGORY." AS POINTS ON CARDS.card_type=POINTS.tbl_card_category_id  
			WHERE CARDS.tbl_student_id='".$tbl_student_id."' 
			AND CARDS.tbl_class_id='".$tbl_class_id."' 
			AND CARDS.tbl_teacher_id='".$tbl_teacher_id."' 
			and POINTS.card_point < 0
			GROUP BY `card_type` , `card_issue_type`
			ORDER BY card_type"; 
		//echo $qry;
		$rs = $this->cf->SelectMultiRecords($qry);
		return $rs;	
	}

	/**
	* @desc		Get student object
	* @param	string $tbl_student_id  
	* @access	default
	* @return	object student
	*/
	function get_student_obj($tbl_student_id) {
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		$qry = "SELECT * FROM ".TBL_STUDENT." WHERE tbl_student_id='$tbl_student_id' ";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
	
	function get_assigned_parent($tbl_student_id) {
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		$qry = "SELECT * FROM ".TBL_PARENT_STUDENT." WHERE tbl_student_id='$tbl_student_id' AND is_active='Y' ORDER BY id DESC ";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
	

	/**
	* @desc		Get student object
	* @param	string $tbl_student_id  
	* @access	default
	* @return	object student
	*/

	function get_student_name($tbl_student_id) {
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		$qry = "SELECT first_name, first_name_ar, last_name, last_name_ar FROM ".TBL_STUDENT." WHERE tbl_student_id='$tbl_student_id' ";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}

	/**
	* @desc		Get student picture
	* @param	string $tbl_student_id  
	* @access	default
	* @return	object student
	*/

	function get_student_picture($tbl_student_id) {
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		$qry = "SELECT file_name_updated FROM ".TBL_STUDENT." WHERE tbl_student_id='$tbl_student_id' ";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs[0]["file_name_updated"];
	}

	/**
	* @desc		Get child report and other data uploaded by school
	* @param	 
	* @access	default
	* @return	record set
	*/

	function get_child_data() {
		$email = $this->cf->get_data(trim($email));
		$qry = "SELECT * FROM ".TBL_PARENTING." WHERE is_active='Y' ORDER BY parenting_order ASC";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		//print_r($rs);
		return $rs;
	}

	/**
	* @desc		Get students list from given class name
	* @param	
	* @access	default
	* @return	array $rs
	*/

	function get_student_list($tbl_class_id, $tbl_school_id, $tbl_student_id='') {
		$qry = "SELECT ST.*,P.first_name as parent_first_name,P.last_name as parent_last_name,P.first_name_ar as parent_first_name_ar,P.last_name_ar as parent_last_name_ar,P.mobile as parent_mobile,P.email as parent_email  FROM ".TBL_STUDENT." AS ST 
				LEFT JOIN ".TBL_PARENT_STUDENT." AS PS ON PS.tbl_student_id=ST.tbl_student_id 
				LEFT JOIN ".TBL_PARENT." AS P ON P.tbl_parent_id=PS.tbl_parent_id
				WHERE 1 ";
		if($tbl_student_id<>"")
		{
			$qry .= " AND ST.tbl_student_id<>'".$tbl_student_id."' ";
		}
		
		$qry .= " AND ST.tbl_class_id='".$tbl_class_id."' AND ST.tbl_school_id='".$tbl_school_id."' AND ST.is_active='Y' 
		          GROUP BY ST.tbl_student_id ORDER BY    ST.first_name ASC";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;	
	}

	/**
	* @desc		Get students bus
	* @param	
	* @access	default
	* @return	array $rs
	*/

	function get_student_bus($tbl_student_id) {
		$qry = "SELECT tbl_bus_id FROM ".TBL_BUS_STUDENT." WHERE tbl_student_id='".$tbl_student_id."' AND is_active='Y'";
		//echo "<br>".$qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs[0]["tbl_bus_id"];	
	}

	/**
	* @desc		Get all students that are registered with a particular bus
	* @param	tbl_bus_id
	* @access	default
	* @return	array $rs
	*/

	function get_all_bus_students($tbl_bus_id) {
		$qry = "SELECT * FROM ".TBL_STUDENT." WHERE is_active='Y' AND tbl_student_id IN (SELECT tbl_student_id FROM ".TBL_BUS_STUDENT." WHERE tbl_bus_id='".$tbl_bus_id."' AND is_active='Y')  ORDER BY first_name ASC ";
		//echo "<br>".$qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;	
	}

	/**
	* @desc		Get student's teacher
	* @param	tbl_bus_id
	* @access	default
	* @return	array $rs
	*/

	function get_students_teacher($tbl_student_id) {
		$qry = "SELECT * FROM ".TBL_STUDENT." WHERE is_active='Y' AND tbl_student_id IN (SELECT tbl_student_id FROM ".TBL_BUS_STUDENT." WHERE tbl_bus_id='".$tbl_bus_id."' AND is_active='Y')  ORDER BY first_name ASC ";
		//echo "<br>".$qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;	
	}

	/**
	* @desc		Get gallery images of a student
	* @param	string $email  
	* @access	default
	* @return	$tbl_teacher_id
	*/

	function get_gallery_student_images($tbl_student_id) {
		$tbl_gallery_category_id = $this->cf->get_data(trim($tbl_student_id));
		$qry = "SELECT * FROM ".TBL_IMAGE_GALLERYSTU." WHERE is_active='Y' AND tbl_student_id='$tbl_student_id' ORDER BY image_order ASC ";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;	
	}

	/**
	* @desc		Get gallery image
	* @param	string $tbl_image_gallery_id  
	* @access	default
	* @return	$rs
	*/

	function get_gallerystu_image($tbl_image_gallerystu_id) {
		$tbl_image_gallerystu_id = $this->cf->get_data(trim($tbl_image_gallerystu_id));
		$qry = "SELECT * FROM ".TBL_IMAGE_GALLERYSTU." WHERE tbl_image_gallerystu_id='$tbl_image_gallerystu_id' ";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;	
	}


	function list_all_students_against_class($tbl_class_id) {
		$tbl_class_id = $this->cf->get_data(trim($tbl_class_id));
		$qry_sel = "SELECT tbl_student_id FROM ".TBL_STUDENT." WHERE tbl_class_id='$tbl_class_id' AND is_active='Y' ORDER BY first_name ASC ";
		//echo $qry_sel."<br />";
		$rs = $this->cf->selectMultiRecords($qry_sel);
		return $rs;	
	}

	function get_semester_id($school_id,$current_date) {
		$school_id = $this->cf->get_data(trim($school_id));
		$qry_sel = "SELECT tbl_semester_id, title, title_ar FROM ".TBL_SEMESTER." WHERE tbl_school_id='$school_id' AND is_active='Y'  AND 
		             (('$current_date' BETWEEN `start_date` AND `end_date`))";

		//echo $qry_sel."<br />";
		$rs = $this->cf->selectMultiRecords($qry_sel);
		return $rs;	
	}

	function get_cards_semester($tbl_student_id, $tbl_semester_id, $school_id, $card_type) {
		$tbl_student_id 	= $this->cf->get_data(trim($tbl_student_id));
		$tbl_semester_id 	= $this->cf->get_data(trim($tbl_semester_id));
		//$qry = "SELECT COUNT(card_type) as total_cards, card_type FROM ".TBL_CARDS." WHERE tbl_student_id='$tbl_student_id' AND is_active='Y' GROUP BY card_type ";
		$qry = "SELECT count( * ) AS cnt , card_type, card_issue_type
			FROM ".TBL_CARDS." WHERE tbl_student_id='$tbl_student_id' AND tbl_semester_id='$tbl_semester_id' AND tbl_school_id='$school_id' AND card_type= '$card_type'
			GROUP BY `card_issue_type` ";
		//echo $qry;
		$rs = $this->cf->SelectMultiRecords($qry);
		return $rs;	
	}

	function get_cards_points($school_id, $card_count_range) {
	    $qry = "SELECT * FROM ".TBL_CARD_POINTS_CONFIG." WHERE tbl_school_id='$school_id' AND 
							('$card_count_range' BETWEEN `range_from` AND `range_to`) AND is_active='Y'";
		//echo $qry; exit;
		$rs = $this->cf->SelectMultiRecords($qry);
		return $rs;	
	}

	function get_total_mark_cards($school_id) {
	    $qry = "SELECT * FROM ".TBL_SCHOOL_COMMON_SETTINGS." WHERE tbl_school_id='$school_id' AND is_active='Y'";
		//echo $qry; exit;
		$rs = $this->cf->SelectMultiRecords($qry);
		return $rs;	
	}
	
	/************************************************************************************************/
	/************************************************************************************************/
	/************************************************************************************************/
	//BACKEND FUNCTIONALITIES 
	
	function get_total_students_against_school($school_id) {
		$school_id = $this->cf->get_data(trim($school_id));
		$qry_sel = "SELECT tbl_student_id FROM ".TBL_STUDENT." WHERE tbl_school_id='$school_id' AND is_active='Y' ";
		$rs = $this->cf->selectMultiRecords($qry_sel);
		return count($rs);
	}
	
	
	/**
	* @desc		Get all students
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function get_all_students($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id='',$tbl_class_id='',$tbl_parent_id='', $is_approved='') {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		//if (trim($offset) == "") {$offset = 0;}
		
		$qry = "SELECT ST.id,ST.tbl_student_id,ST.first_name,ST.last_name,ST.first_name_ar,ST.last_name_ar,ST.mobile,ST.email,ST.added_date,ST.tbl_school_id
			   ,ST.is_active,ST.dob_month,ST.dob_day,ST.dob_year, ST.tbl_class_id, ST.gender, ST.file_name_updated, ST.country, ST.is_approved, P.tbl_parent_id, P.user_id, P.pass_code, P.first_name as parent_first_name,P.last_name as parent_last_name
			   ,P.first_name_ar AS parent_first_name_ar, P.last_name_ar AS parent_last_name_ar
			   ,C.class_name, C.class_name_ar, S.section_name, S.section_name_ar, T.school_type, T.school_type_ar
			   ,UNT.device,UNT.token,P.is_logged
				FROM ".TBL_STUDENT." AS ST 
				LEFT JOIN ".TBL_PARENT_STUDENT." AS STP ON STP.tbl_student_id=ST.tbl_student_id
				LEFT JOIN ".TBL_PARENT." AS P ON  P.tbl_parent_id = STP.tbl_parent_id
				LEFT JOIN ".TBL_CLASS."  AS C ON  C.tbl_class_id = ST.tbl_class_id
		        LEFT JOIN  ".TBL_SECTION." AS S  ON  C.tbl_section_id = S.tbl_section_id  
				LEFT JOIN  ".TBL_SCHOOL_TYPE." AS T  ON  T.tbl_school_type_id= C.tbl_school_type_id  
				LEFT JOIN ".TBL_USER_NOTIFY_TOKEN." AS UNT  ON  UNT.user_id= STP.tbl_parent_id  
				WHERE  1 ";
				
		if($tbl_school_id<>""){
			$qry .= " AND ST.tbl_school_id= '".$tbl_school_id."' ";
		}
		if($tbl_parent_id<>""){
			$qry .= " AND P.tbl_parent_id= '".$tbl_parent_id."' ";
		}
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND ST.is_active='$is_active' ";
		}
		$qry .= " AND ST.is_active<>'D' "; 

		if (trim($is_approved) != "") {
			$qry .= " AND ST.is_approved='$is_approved' "; 
		}
		
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( CONCAT(ST.first_name,' ',ST.last_name) LIKE '%$q%' 
					   	OR CONCAT(TRIM(ST.first_name_ar),' ',TRIM(ST.last_name_ar)) LIKE '%$q%' 
					   	OR CONCAT(TRIM(P.first_name),' ',TRIM(P.last_name)) LIKE '%$q%' 
					   	OR CONCAT(TRIM(P.first_name_ar),' ',TRIM(P.last_name_ar)) LIKE '%$q%'
						OR ST.emirates_id_father LIKE '%$q%'
						OR ST.emirates_id_mother LIKE '%$q%'
						OR ST.email LIKE '%$q%'
						OR ST.mobile LIKE '%$q%'
						OR ST.country LIKE '%$q%'
						OR ST.gender LIKE '%$q%'
						) ";

		}
	   if ($tbl_class_id<>"") {
			$qry .= " AND ST.`tbl_class_id` = '$tbl_class_id' ";
		}
		
		$qry .= " GROUP BY tbl_student_id ";
		
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY C.class_name ASC, S.section_name ASC, first_name ASC ";
		}
		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_STUDENT_PAGING;
		}
		//echo $qry; 
		$results = $this->cf->selectMultiRecords($qry);
	
	return $results;
	}
	
	/**
	* @desc		Get total students
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_students($q, $is_active, $tbl_school_id='',$tbl_class_id='',$tbl_parent_id='', $is_approved='') {
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry =  "SELECT COUNT(distinct(ST.tbl_student_id)) as total_students
				FROM ".TBL_STUDENT." AS ST 
				LEFT JOIN ".TBL_PARENT_STUDENT." AS STP ON STP.tbl_student_id=ST.tbl_student_id
				LEFT JOIN ".TBL_PARENT." AS P ON  P.tbl_parent_id=STP.tbl_parent_id
				LEFT JOIN ".TBL_CLASS."  AS C ON  C.tbl_class_id = ST.tbl_class_id
		        LEFT JOIN  ".TBL_SECTION." AS S  ON  C.tbl_section_id = S.tbl_section_id  
				LEFT JOIN  ".TBL_SCHOOL_TYPE." AS T  ON  T.tbl_school_type_id= C.tbl_school_type_id  
			    WHERE  1   ";
				
		if($tbl_school_id<>""){
			$qry .= " AND ST.tbl_school_id= '".$tbl_school_id."' ";
		}
		if($tbl_parent_id<>""){
			$qry .= " AND P.tbl_parent_id= '".$tbl_parent_id."' ";
		}
		//Active/Deactive
		if(trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND ST.is_active='$is_active' ";
		}
		$qry .= " AND ST.is_active<>'D' "; 
		
		if (trim($is_approved) != "") {
			$qry .= " AND ST.is_approved='$is_approved' "; 
		}
		
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( CONCAT(ST.first_name,' ',ST.last_name) LIKE '%$q%' 
					   	OR CONCAT(TRIM(ST.first_name_ar),' ',TRIM(ST.last_name_ar)) LIKE '%$q%' 
					   	OR CONCAT(TRIM(P.first_name),' ',TRIM(P.last_name)) LIKE '%$q%' 
					   	OR CONCAT(TRIM(P.first_name_ar),' ',TRIM(P.last_name_ar)) LIKE '%$q%'
						OR ST.emirates_id_father LIKE '%$q%'
						OR ST.emirates_id_mother LIKE '%$q%'
						OR ST.email LIKE '%$q%'
						OR ST.mobile LIKE '%$q%'
						OR ST.country LIKE '%$q%'
						OR ST.gender LIKE '%$q%'
						) ";

		}
	   if ($tbl_class_id<>"") {
			$qry .= " AND ST.`tbl_class_id` = '$tbl_class_id' ";
		}
		//echo $qry;
		$results = $this->cf->selectMultiRecords($qry);
	    return $results[0]['total_students'];
	}
	

	function is_exist_student($tbl_student_id, $first_name, $last_name, $tbl_class_id, $tbl_school_id, $tbl_emirates_id) {
		$tbl_student_id  	   = $this->cf->get_data($tbl_student_id);
		$first_name 		   = $this->cf->get_data($first_name);
		$last_name            = $this->cf->get_data($last_name);
		$tbl_class_id         = $this->cf->get_data($tbl_class_id);
		$tbl_emirates_id      = $this->cf->get_data($tbl_emirates_id);

		$qry = "SELECT * FROM ".TBL_STUDENT." WHERE 1 ";
		/*if (trim($first_name) != "") {
			 $qry .= " AND first_name='$first_name' ";
		}
		if (trim($last_name) != "") {
			 $qry .= " AND last_name='$last_name' ";
		}
		
		if (trim($tbl_class_id) != "") {
			 $qry .= " AND tbl_class_id='$tbl_class_id' ";
		}*/
		
		if (trim($tbl_emirates_id) != "") {
			 $qry .= " AND tbl_emirates_id='$tbl_emirates_id' ";
		}
		if (trim($tbl_student_id) != "") {
			$qry .= " AND tbl_student_id <> '$tbl_student_id'";
		}
	    $qry .= " AND is_active<>'D' "; 
		$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}
	
	
	/**
	* @desc		Add Student
	* @param	String params
	* @access	default
	*/
	function add_student($tbl_student_id, $first_name, $last_name, $first_name_ar, $last_name_ar, $dob_month, $dob_day, $dob_year, 
		                                             $gender, $mobile, $email, $country, $emirates_id_father, $emirates_id_mother, $tbl_academic_year_id, $tbl_class_id, $tbl_school_id,$tbl_emirates_id,$is_active="")
    {
		$tbl_student_id    = $this->cf->get_data($tbl_student_id);
		$first_name        = $this->cf->get_data($first_name);
		$last_name         = $this->cf->get_data($last_name);
		$first_name_ar     = $this->cf->get_data($first_name_ar);
		$last_name_ar      = $this->cf->get_data($last_name_ar);
		$dob_month         = $this->cf->get_data($dob_month);
		$dob_day           = $this->cf->get_data($dob_day);
		$dob_year          = $this->cf->get_data($dob_year);
		$gender            = $this->cf->get_data($gender);
		$mobile            = $this->cf->get_data($mobile);
		$email             = $this->cf->get_data($email);
		$country           = $this->cf->get_data($country);
		$emirates_id_father= $this->cf->get_data($emirates_id_father);
		$emirates_id_mother= $this->cf->get_data($emirates_id_mother);
		$tbl_academic_year_id  = $this->cf->get_data($tbl_academic_year_id);
		$tbl_class_id      = $this->cf->get_data($tbl_class_id);
		$tbl_school_id     = $this->cf->get_data($tbl_school_id);
		$tbl_emirates_id     = $this->cf->get_data($tbl_emirates_id);
		
		if($mobile<>"")
			$mobile 	       = "+".$mobile;
		
		if($tbl_emirates_id<>"")
		{
			$student_user_id = $tbl_emirates_id;
		}else{
			$student_user_id = $first_name;
		}
		$student_password               = substr(md5(uniqid(rand(), true)), 0, 6);
		$student_hash = sha1($student_password);
		$student_salt = substr(md5(uniqid(rand(), true)), 0, 3);
		$student_hash = sha1($student_salt . $student_hash);
		$student_pass_code	    =	base64_encode($student_password);	
		
	
		if($tbl_school_id<>"")
		{
			$qry_ins = "INSERT INTO ".TBL_STUDENT." (`tbl_student_id`, `first_name`, `last_name`, `first_name_ar`, `last_name_ar`, `dob_month`, 
			`dob_day`, `dob_year`, `gender`, `mobile`, `email`, `country`, `emirates_id_father`, `emirates_id_mother`, `tbl_academic_year_id` , `tbl_class_id`, `added_date`, `is_active`, `tbl_school_id`,`tbl_emirates_id`,`student_user_id`,`student_password`,`student_salt`,`student_pass_code`)
						VALUES ('$tbl_student_id', '$first_name', '$last_name', '$first_name_ar', '$last_name_ar', '$dob_month',
			'$dob_day', '$dob_year', '$gender', '$mobile ', '$email', '$country', '$emirates_id_father', '$emirates_id_mother', '$tbl_academic_year_id' , '$tbl_class_id', NOW(), '$is_active', '$tbl_school_id', '$tbl_emirates_id','$student_user_id','$student_hash','$student_salt','$student_pass_code')";
			$this->cf->insertInto($qry_ins);
			
			
			$qry = "SELECT file_name_updated,file_name_original,file_type,file_size FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_student_id' AND is_active='Y' ";
			$result = $this->cf->selectMultiRecords($qry);
			if (count($result)>0) {
				$file_name_updated  = isset($result[0]['file_name_updated'])? $result[0]['file_name_updated']:'';
				$file_name_original = isset($result[0]['file_name_original'])? $result[0]['file_name_original']:'';
				$file_type          = isset($result[0]['file_type'])? $result[0]['file_type']:'';
				$file_size          = isset($result[0]['file_size'])? $result[0]['file_size']:'';
				$qry_update  = "UPDATE ".TBL_STUDENT." SET file_name_updated='$file_name_updated', 
				                file_name_original='$file_name_original', file_type='$file_type', file_size='$file_size' WHERE tbl_student_id='$tbl_student_id' ";
				$this->cf->update($qry_update);
			}
			
			
			return "Y";
		}else{
		   return "N";	
			
		}
	}
	
	/**
	* @desc		Activate
	* @param	string $tbl_student_id
	* @access	default
	*/
	function activate_student($student_id_enc,$tbl_school_id) {
		$tbl_student_id = $this->cf->get_data(trim($student_id_enc));
		
		$qry = "UPDATE ".TBL_STUDENT." SET is_active='Y' WHERE tbl_student_id='$tbl_student_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate
	* @param	string $tbl_student_id
	* @access	default
	*/
	function deactivate_student($student_id_enc,$tbl_school_id) {
		$tbl_student_id = $this->cf->get_data(trim($student_id_enc));
		
		$qry = "UPDATE ".TBL_STUDENT." SET is_active='N' WHERE tbl_student_id='$tbl_student_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete
	* @param	string $tbl_student_id
	* @access	default
	*/
	function delete_student($student_id_enc,$tbl_school_id) {
		$tbl_student_id = $this->cf->get_data(trim($student_id_enc));
        $qry = "UPDATE ".TBL_STUDENT." SET is_active='D' WHERE tbl_student_id='$tbl_student_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_CATEGORY." WHERE tbl_category_id='$tbl_category_id' ";
		//$this->cf->deleteFrom($qry);
	}
	
	
	function get_country_list($is_active='')
	{
		$qry = "SELECT * FROM ".TBL_COUNTRY." WHERE 1 ";
		if($is_active<>"")
		{
			$qry .= " AND is_active='$is_active'  ";
		}
		$qry .= " ORDER BY sorting_order ASC ";
		$resObj      = $this->cf->selectMultiRecords($qry);
		return $resObj;
	}
	
	function get_academic_year($is_active='',$tbl_school_id)
	{
		$qry  = "SELECT * FROM ".TBL_ACADEMIC_YEAR." WHERE 1 ";
		$qry .= " AND tbl_school_id='$tbl_school_id' ";
		if($is_active<>"")
		{
			$qry .= " AND is_active='$is_active'  ";
		}
		
		$qry .= " ORDER BY academic_start DESC ";
		$resObj      = $this->cf->selectMultiRecords($qry);
		return $resObj;
	}
	
	
	function get_current_academic_year($tbl_school_id)
	{
		$current_date = date("Y-m-d");
		$qry  = "SELECT AY.tbl_academic_year_id FROM ".TBL_ACADEMIC_YEAR." AS AY LEFT JOIN  ".TBL_SEMESTER." AS S ON  S.tbl_academic_year_id = AY.tbl_academic_year_id  WHERE 1  ";
		$qry .= " AND AY.tbl_school_id='$tbl_school_id' ";
		$qry .= " AND AY.is_active='Y'  ";
		$qry .= " AND (('$current_date' BETWEEN S.start_date AND S.end_date))"; 
		$resObj      = $this->cf->selectMultiRecords($qry);
		return $resObj;
	}
	
	
	function save_student_changes($tbl_student_id, $first_name, $last_name, $first_name_ar, $last_name_ar, $dob_month, $dob_day, $dob_year,$gender, $mobile, $email, $country, $emirates_id_father, $emirates_id_mother, $tbl_academic_year_id, $tbl_class_id, $tbl_school_id, $tbl_emirates_id, $is_approved="")
	{
	   $tbl_student_id    = $this->cf->get_data($tbl_student_id);
		$first_name        = $this->cf->get_data($first_name);
		$last_name         = $this->cf->get_data($last_name);
		$first_name_ar     = $this->cf->get_data($first_name_ar);
		$last_name_ar      = $this->cf->get_data($last_name_ar);
		$dob_month         = $this->cf->get_data($dob_month);
		$dob_day           = $this->cf->get_data($dob_day);
		$dob_year          = $this->cf->get_data($dob_year);
		$gender            = $this->cf->get_data($gender);
		$mobile            = $this->cf->get_data($mobile);
		$email             = $this->cf->get_data($email);
		$country           = $this->cf->get_data($country);
		$emirates_id_father= $this->cf->get_data($emirates_id_father);
		$emirates_id_mother= $this->cf->get_data($emirates_id_mother);
		$tbl_academic_year_id  = $this->cf->get_data($tbl_academic_year_id);
		$tbl_class_id      = $this->cf->get_data($tbl_class_id);
		$tbl_school_id     = $this->cf->get_data($tbl_school_id);
		$tbl_emirates_id     = $this->cf->get_data($tbl_emirates_id);
		$is_approved     = $this->cf->get_data($is_approved);
		
		if($mobile<>"")
			$mobile 	       = "+".$mobile;
		
		if($tbl_school_id<>"")
		{
			$qry  = "UPDATE ".TBL_STUDENT." SET first_name='$first_name', first_name_ar='$first_name_ar', last_name='$last_name', last_name_ar='$last_name_ar', mobile='$mobile', dob_month='$dob_month', dob_day='$dob_day', dob_year='$dob_year', gender='$gender', email='$email', country='$country', tbl_class_id='$tbl_class_id', is_active='Y', emirates_id_father='$emirates_id_father', emirates_id_mother='$emirates_id_mother', tbl_academic_year_id='$tbl_academic_year_id', tbl_emirates_id='$tbl_emirates_id', is_approved='$is_approved' WHERE tbl_student_id = '$tbl_student_id' and tbl_school_id = '$tbl_school_id' ";
			$this->cf->update($qry);
			$qry = "SELECT file_name_updated,file_name_original,file_type,file_size FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_student_id' AND is_active='Y' ";
			$result = $this->cf->selectMultiRecords($qry);
			if (count($result)>0) {
				$file_name_updated  = isset($result[0]['file_name_updated'])? $result[0]['file_name_updated']:'';
				$file_name_original = isset($result[0]['file_name_original'])? $result[0]['file_name_original']:'';
				$file_type          = isset($result[0]['file_type'])? $result[0]['file_type']:'';
				$file_size          = isset($result[0]['file_size'])? $result[0]['file_size']:'';
				$qry_update  = "UPDATE ".TBL_STUDENT." SET file_name_updated='$file_name_updated', 
				                file_name_original='$file_name_original', file_type='$file_type', file_size='$file_size' WHERE tbl_student_id='$tbl_student_id' ";
				$this->cf->update($qry_update);
			}
		    return "Y";
		}else{
		   return "N";	
		}
	}
	
	// BEVIOUR POINTS CONFIGURATION
	//get_all_bevaviour_points($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id);
		/**
	* @desc		Get all students
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function get_all_bevaviour_points($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id, $mode) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry = "SELECT BP.tbl_point_category_id,BP.point_name_en,BP.point_name_ar,BP.behaviour_point,BP.added_date,BP.is_active
				FROM ".TBL_BEHAVIOUR_POINTS." AS BP WHERE  1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND BP.tbl_school_id= '".$tbl_school_id."' ";
	    }
		
		if($mode=="ministry")
		{
			$qry .= " AND BP.tbl_school_id= '' ";
		}
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND BP.is_active='$is_active' ";
		}
		$qry .= " AND BP.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( BP.point_name_en LIKE '%$q%' 
					   	   OR BP.point_name_ar LIKE '%$q%' ) ";
		}
	 
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY id DESC ";
		}
		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_CARD_CATEGORY_PAGING;
		}
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	/**
	* @desc		Get total behaviour points
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_behaviour_points($q, $is_active, $tbl_school_id, $mode) {
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT BP.tbl_point_category_id FROM ".TBL_BEHAVIOUR_POINTS." AS BP WHERE  1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND BP.tbl_school_id= '".$tbl_school_id."' ";
		}	
		if($mode=="ministry")
		{
			$qry .= " AND BP.tbl_school_id= '' ";
		}
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND BP.is_active='$is_active' ";
		}
		$qry .= " AND BP.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( BP.point_name_en LIKE '%$q%' 
					   	   OR BP.point_name_ar LIKE '%$q%' ) ";
		}
		
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
	}
	

	function is_exist_behavior_point($tbl_point_category_id, $point_name_en, $point_name_ar, $tbl_school_id) {
		$tbl_point_category_id    = $this->cf->get_data($tbl_point_category_id);
		$point_name_en 		    = $this->cf->get_data($point_name_en);
		$point_name_ar            = $this->cf->get_data($point_name_ar);
		$tbl_school_id            = $this->cf->get_data($tbl_school_id);

		$qry = "SELECT * FROM ".TBL_BEHAVIOUR_POINTS." WHERE 1 ";
		if (trim($point_name_en) != "") {
			 $qry .= " AND point_name_en='$point_name_en' ";
		}
	
		if (trim($tbl_point_category_id) != "") {
			 $qry .= " AND tbl_point_category_id <> '$tbl_point_category_id' ";
		}
		
	    $qry .= " AND is_active<>'D' "; 
		if($tbl_school_id<>""){
			$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
	    }
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}
	
	
	// GET BEHAVIOUR POINT INFORMATION
	function get_bevaviour_point_info($is_active,$tbl_point_category_id, $tbl_school_id) {
		$$tbl_point_category_id  = $this->cf->get_data($$tbl_point_category_id);
		
		$qry = "SELECT BP.tbl_point_category_id,BP.point_name_en,BP.point_name_ar,BP.behaviour_point,BP.added_date,BP.is_active
				FROM ".TBL_BEHAVIOUR_POINTS." AS BP WHERE  1 ";
		if($tbl_school_id<>""){
			$qry .= " AND BP.tbl_school_id= '".$tbl_school_id."' ";
		}
		
		if (trim($tbl_point_category_id != "")) {
			$qry .= " AND BP.tbl_point_category_id='$tbl_point_category_id' ";
		}
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND BP.is_active='$is_active' ";
		}
		$qry .= " AND BP.is_active<>'D' "; 
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	
	
	
	
	/**
	* @desc		Add Behaviour Point
	* @param	String params
	* @access	default
	*/
	function add_behavior_point($tbl_point_category_id,$point_name_en,$point_name_ar,$behaviour_point,$tbl_school_id)
    {
		$tbl_point_category_id= $this->cf->get_data($tbl_point_category_id);
		$point_name_en        = $this->cf->get_data($point_name_en);
		$point_name_ar        = $this->cf->get_data($point_name_ar);
		$behaviour_point      = $this->cf->get_data($behaviour_point);
		$tbl_school_id        = $this->cf->get_data($tbl_school_id);
	
      /*if($tbl_school_id<>"")
		{*/
			$qry_ins = "INSERT INTO ".TBL_BEHAVIOUR_POINTS." (`tbl_point_category_id`, `point_name_en`, `point_name_ar`, `behaviour_point`, `added_date`, `is_active`, `tbl_school_id`)
						VALUES ('$tbl_point_category_id', '$point_name_en', '$point_name_ar', '$behaviour_point', NOW(), 'Y', '$tbl_school_id')";
			$res = $this->cf->insertInto($qry_ins);
			return "Y";
		
	/*	} */
	}
	
	/**
	* @desc		Activate
	* @param	string $tbl_student_id
	* @access	default
	*/
	function activateBehaviorPoint($tbl_point_category_id,$tbl_school_id) {
		$tbl_student_id = $this->cf->get_data(trim($student_id_enc));
		
		$qry = "UPDATE ".TBL_BEHAVIOUR_POINTS." SET is_active='Y' WHERE tbl_point_category_id='$tbl_point_category_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate
	* @param	string $tbl_student_id
	* @access	default
	*/
	function deactivateBehaviorPoint($tbl_point_category_id,$tbl_school_id) {
		$tbl_student_id = $this->cf->get_data(trim($student_id_enc));
		
		$qry = "UPDATE ".TBL_BEHAVIOUR_POINTS." SET is_active='N' WHERE tbl_point_category_id='$tbl_point_category_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete
	* @param	string $tbl_student_id
	* @access	default
	*/
	function deleteBehaviorPoint($tbl_point_category_id,$tbl_school_id) {
		$tbl_point_category_id = $this->cf->get_data(trim($tbl_point_category_id));
        $qry = "UPDATE ".TBL_BEHAVIOUR_POINTS." SET is_active='D' WHERE tbl_point_category_id='$tbl_point_category_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_CATEGORY." WHERE tbl_category_id='$tbl_category_id' ";
		//$this->cf->deleteFrom($qry);
	}
	

	
	function save_behavior_point_changes($tbl_point_category_id,$point_name_en,$point_name_ar,$behaviour_point,$tbl_school_id)
	{
	    $tbl_point_category_id= $this->cf->get_data($tbl_point_category_id);
		$point_name_en        = $this->cf->get_data($point_name_en);
		$point_name_ar        = $this->cf->get_data($point_name_ar);
		$behaviour_point      = $this->cf->get_data($behaviour_point);
		$tbl_school_id        = $this->cf->get_data($tbl_school_id);
	
      /*if($tbl_school_id<>"")
		{*/
			$qry  = "UPDATE ".TBL_BEHAVIOUR_POINTS." SET point_name_en='$point_name_en', point_name_ar='$point_name_ar', behaviour_point='$behaviour_point' 
			         WHERE tbl_point_category_id = '$tbl_point_category_id' and tbl_school_id = '$tbl_school_id' ";
			$res = $this->cf->update($qry);
			return "Y";
		/*}*/
	
	}
	
	//END BEHAVIOUR POINTS CONFIGURATION
	
	
	// STUDENT CARDS CONFIGURATION
   /**
	* @desc		Get all student cards
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function get_all_student_cards($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry = "SELECT CP.tbl_card_category_id,CP.category_name_en,CP.category_name_ar,CP.card_point,CP.tbl_school_type_id,CP.added_date,CP.is_active
				
				FROM ".TBL_CARD_POINTS." AS CP WHERE  1 ";
		$qry .= " AND CP.tbl_school_id= '".$tbl_school_id."' ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND CP.is_active='$is_active' ";
		}
		$qry .= " AND CP.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( CP.category_name_en LIKE '%$q%' 
					   	   OR CP.category_name_ar LIKE '%$q%' ) ";
		}
		
		if($category<>""){
			$qry .= " AND CP.tbl_school_type_id $LIKE '%$category%' ";
		}
	 
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY id DESC ";
		}
		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_CARD_CATEGORY_PAGING;
		}
		$results = $this->cf->selectMultiRecords($qry);
		for($v=0;$v<count($results);$v++){
				$school_type_id_array    = array(); 
				$school_type_id_array	= explode("||",$results[$v]["tbl_school_type_id"]);
				$classTypes = "";
				for($k=0;$k<count($school_type_id_array);$k++)
				{
					if($school_type_id_array[$k]<>""){
						if($k<>0){
							$classTypes .=" &nbsp;, <br>";
						}
					if($school_type_id_array[$k] =="general")
					{
						$classTypes .= "General&nbsp;[::]&nbsp;جنرال لواء" ;
					}
					else{
						$Query = "SELECT school_type, school_type_ar FROM ".TBL_SCHOOL_TYPE." WHERE tbl_school_id='$tbl_school_id' and tbl_school_type_id='$school_type_id_array[$k]'";	
						$dataSchoolType = $this->cf->selectMultiRecords($Query);
						$classTypes .= $dataSchoolType[0]['school_type']."&nbsp;[::]&nbsp;".$dataSchoolType[0]['school_type_ar'];
					}
		         
				 }
		         $results[$v]['classTypes'] = $classTypes;
			  }
		   
		}
	return $results;
	}
	
	/**
	* @desc		Get total student cards
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_student_cards($q, $is_active, $tbl_school_id) {
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT CP.tbl_card_category_id FROM ".TBL_CARD_POINTS." AS CP WHERE  1 ";
		$qry .= " AND CP.tbl_school_id= '".$tbl_school_id."' ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND CP.is_active='$is_active' ";
		}
		$qry .= " AND CP.is_active<>'D' "; 
		
		
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( CP.category_name_en LIKE '%$q%' 
					   	   OR CP.category_name_ar LIKE '%$q%' ) ";
		}
		if($category<>""){
			$qry .= " AND CP.tbl_school_type_id $LIKE '%$category%' ";
		}
		
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
	}

	function is_exist_student_card($tbl_card_category_id, $category_name_en, $category_name_ar, $tbl_school_id){
		$tbl_card_category_id     = $this->cf->get_data($tbl_card_category_id);
		$category_name_en 		 = $this->cf->get_data($category_name_en);
		$category_name_ar         = $this->cf->get_data($category_name_ar);
		$tbl_school_id            = $this->cf->get_data($tbl_school_id);

		$qry = "SELECT * FROM ".TBL_CARD_POINTS." WHERE 1 ";
		if (trim($category_name_en) != "") {
			 $qry .= " AND category_name_en='$category_name_en' ";
		}
	
		if (trim($tbl_card_category_id) != "") {
			 $qry .= " AND tbl_card_category_id <> '$tbl_card_category_id' ";
		}
		
	    $qry .= " AND is_active<>'D' "; 
		$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}
	
	
	// GET student card information
	function get_student_card_info($is_active, $tbl_card_category_id, $tbl_school_id) {
		$tbl_card_category_id  = $this->cf->get_data($tbl_card_category_id);
		
		$qry = "SELECT CP.tbl_card_category_id,CP.category_name_en,CP.category_name_ar,CP.card_point,CP.tbl_school_type_id,CP.added_date,CP.is_active
				FROM ".TBL_CARD_POINTS." AS CP WHERE  1 ";
		$qry .= " AND CP.tbl_school_id= '".$tbl_school_id."' ";
		
		if (trim($tbl_card_category_id != "")) {
			$qry .= " AND CP.tbl_card_category_id='$tbl_card_category_id' ";
		}
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND CP.is_active='$is_active' ";
		}
		$qry .= " AND CP.is_active<>'D' "; 
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	
	
	
	
	/**
	* @desc		Add student card
	* @param	String params
	* @access	default
	*/
	function add_student_card($tbl_card_category_id,$category_name_en,$category_name_ar,$card_point,$tbl_school_type_id,$tbl_school_id)
    {
		$tbl_card_category_id	= $this->cf->get_data($tbl_card_category_id);
		$category_name_en        = $this->cf->get_data($category_name_en);
		$category_name_ar        = $this->cf->get_data($category_name_ar);
		$card_point              = $this->cf->get_data($card_point);
		$tbl_school_id           = $this->cf->get_data($tbl_school_id);
		$tbl_school_type_id      = $this->cf->get_data($tbl_school_type_id);
	    $color                   = '#FFF';
		if($tbl_school_id<>"")
		{
			$qry_ins = "INSERT INTO ".TBL_CARD_POINTS." (`tbl_card_category_id`, `category_name_en`, `category_name_ar`, `card_point`, `color`, `added_date`, `is_active`,`tbl_school_type_id`, `tbl_school_id`)
						VALUES ('$tbl_card_category_id', '$category_name_en', '$category_name_ar', '$card_point', '$color', NOW(), 'Y', '$tbl_school_type_id', '$tbl_school_id')";
			$this->cf->insertInto($qry_ins);
		
			return "Y";
		}else{
		   return "N";	
			
		}
	}
	
	/**
	* @desc		Activate student card
	* @param	string $tbl_student_id
	* @access	default
	*/
	function activateStudentCard($tbl_card_category_id,$tbl_school_id) {
		$tbl_student_id = $this->cf->get_data(trim($student_id_enc));
		
		$qry = "UPDATE ".TBL_CARD_POINTS." SET is_active='Y' WHERE tbl_card_category_id='$tbl_card_category_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate student card
	* @param	string $tbl_student_id
	* @access	default
	*/
	function deactivateStudentCard($tbl_card_category_id,$tbl_school_id) {
		$tbl_student_id = $this->cf->get_data(trim($student_id_enc));
		
		$qry = "UPDATE ".TBL_CARD_POINTS." SET is_active='N' WHERE tbl_card_category_id='$tbl_card_category_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}
	
	/**
	* @desc		Approve student card
	* @param	string $tbl_student_id, string $parent_id_enc
	* @access	default
	*/
	function approve_student_parent($tbl_student_id, $tbl_parent_id, $tbl_school_id) {

		$qry = "UPDATE ".TBL_STUDENT." SET is_active='Y', is_approved='Y' WHERE tbl_student_id='$tbl_student_id'";
		//echo $qry;
		$this->cf->update($qry);
		
		$qry = "UPDATE ".TBL_PARENT." SET is_active='Y', is_approved='Y' WHERE tbl_parent_id='$tbl_parent_id'";
		//echo $qry;
		$this->cf->update($qry);
	
	}
	
	/**
	* @desc		Delete student card
	* @param	string $tbl_student_id
	* @access	default
	*/
	function deleteStudentCard($tbl_card_category_id,$tbl_school_id) {
		$tbl_card_category_id = $this->cf->get_data(trim($tbl_card_category_id));
        $qry = "UPDATE ".TBL_CARD_POINTS." SET is_active='D' WHERE tbl_card_category_id='$tbl_card_category_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_CATEGORY." WHERE tbl_category_id='$tbl_category_id' ";
		//$this->cf->deleteFrom($qry);
	}
	
    /**
	* @desc		Update student card 
	* @param	string $tbl_student_id
	* @access	default
	*/
	function save_student_card_changes($tbl_card_category_id,$category_name_en,$category_name_ar,$tbl_school_type_id,$tbl_school_id)
	{
	    $tbl_card_category_id	= $this->cf->get_data($tbl_card_category_id);
		$category_name_en        = $this->cf->get_data($category_name_en);
		$category_name_ar        = $this->cf->get_data($category_name_ar);
		$tbl_school_id           = $this->cf->get_data($tbl_school_id);
		$tbl_school_type_id      = $this->cf->get_data($tbl_school_type_id);
	
		if($tbl_school_id<>"")
		{
			$qry  = "UPDATE ".TBL_CARD_POINTS." SET category_name_en='$category_name_en', category_name_ar='$category_name_ar',tbl_school_type_id='$tbl_school_type_id'
			         WHERE tbl_card_category_id = '$tbl_card_category_id' and tbl_school_id = '$tbl_school_id' ";
			$this->cf->update($qry);
			return "Y";
		}else{
		   return "N";	
			
		}
	
	}
	
	 //STUDENT CARDS CONFIGURATION

	
	// STUDENT CARDS POINTS CONFIGURATION
   /**
	* @desc		Get all student cards points
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function get_all_student_card_points($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = $this->cf->get_data($q);
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry = "SELECT CP.tbl_card_point_id,CP.range_from,CP.range_to,CP.card_point,CP.added_date,CP.is_active
				
				FROM ".TBL_CARD_POINTS_CONFIG." AS CP WHERE  1 ";
		$qry .= " AND CP.tbl_school_id= '".$tbl_school_id."' ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND CP.is_active='$is_active' ";
		}
		$qry .= " AND CP.is_active<>'D' "; 
		
	
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY CP.range_from ASC ";
		}
		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_CARD_CATEGORY_PAGING;
		}
		$results = $this->cf->selectMultiRecords($qry);
	
	return $results;
	}
	
	/**
	* @desc		Get total student cards points
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_student_card_points($q, $is_active, $tbl_school_id) {
		$q         = $this->cf->get_data($q);
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT CP.tbl_card_point_id FROM ".TBL_CARD_POINTS_CONFIG." AS CP WHERE  1 ";
		$qry .= " AND CP.tbl_school_id= '".$tbl_school_id."' ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND CP.is_active='$is_active' ";
		}
		$qry .= " AND CP.is_active<>'D' "; 
		
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
	}
	

	function is_exist_student_card_point($tbl_card_point_id, $range_from, $range_to, $tbl_school_id){
		$tbl_card_point_id        = $this->cf->get_data($tbl_card_point_id);
		$range_from 		       = $this->cf->get_data($range_from);
		$range_to                 = $this->cf->get_data($range_to);
		$tbl_school_id            = $this->cf->get_data($tbl_school_id);

		$qry = "SELECT * FROM ".TBL_CARD_POINTS_CONFIG." WHERE 1 ";
	
		$qry .= " AND (('$range_from' BETWEEN `range_from` AND `range_to`) OR ( '$range_to' between `range_from` AND `range_to`)) ";
	
		if (trim($tbl_card_point_id) != "") {
			 $qry .= " AND tbl_card_point_id <> '$tbl_card_point_id' ";
		}
		
	    $qry .= " AND is_active<>'D' "; 
		$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}
	
	/**
	* @desc		Add student card point
	* @param	String params
	* @access	default
	*/
	function add_student_card_point($tbl_card_point_id,$total_points,$range_from,$range_to,$card_point,$tbl_school_id)
    {
		$tbl_card_point_id	   = $this->cf->get_data($tbl_card_point_id);
		$total_points              = $this->cf->get_data($total_points);
		$range_from              = $this->cf->get_data($range_from);
		$range_to                = $this->cf->get_data($range_to);
		$card_point              = $this->cf->get_data($card_point);
		$tbl_school_id           = $this->cf->get_data($tbl_school_id);
		if($card_point<>0)
		{
			$card_point = "-".$card_point;
		}
		
		
		if($tbl_school_id<>"")
		{
			
			$qry_points = "SELECT * FROM ".TBL_CARD_POINTS_CONFIG." WHERE tbl_school_id='$tbl_school_id' AND 
							(('$range_from' BETWEEN `range_from` AND `range_to`) OR ( '$range_to' between `range_from` AND `range_to`)) AND is_active='Y'";
		    $existPoints = $this->cf->selectMultiRecords($qry_points);
			if(empty($existSession))
			{
			
				$qry_ins = "INSERT INTO ".TBL_CARD_POINTS_CONFIG." (`tbl_card_point_id`, `range_from`, `range_to`, `card_point`, `added_date`, `is_active`, `tbl_school_id`)
							VALUES ('$tbl_card_point_id', '$range_from', '$range_to', '$card_point', NOW(), 'Y', '$tbl_school_id')";
				$this->cf->insertInto($qry_ins);
				
				$qry_school_settings = "SELECT * FROM ".TBL_SCHOOL_COMMON_SETTINGS." WHERE tbl_school_id='$tbl_school_id' ";
				$existSettings = $this->cf->selectMultiRecords($qry_school_settings);
				if(empty($existSettings)){
					$qrySettings = " INSERT INTO ".TBL_SCHOOL_COMMON_SETTINGS." (`tbl_settings_id` ,`total_points` ,`is_active` ,`added_date` ,`tbl_school_id`)
							VALUES ('$tbl_card_point_id', '$total_points','Y', NOW(), '$tbl_school_id')";	
					$this->cf->insertInto($qrySettings);
	
				}else{
					$qrySettings = "UPDATE ".TBL_SCHOOL_COMMON_SETTINGS." SET total_points='$total_points' WHERE tbl_school_id = '$tbl_school_id' ";
					//echo $qry;
					$this->cf->update($qrySettings);
				}
			   return "Y";
			}else{
			 return "X";		
			}
		}else{
		   return "N";	
			
		}
	}
	
	
	/**
	* @desc		Delete student card
	* @param	string $tbl_student_id
	* @access	default
	*/
	function deleteStudentCardPoint($card_point_id_enc,$tbl_school_id) {
		$card_point_id_enc = $this->cf->get_data(trim($card_point_id_enc));
        $qry = "UPDATE ".TBL_CARD_POINTS_CONFIG." SET is_active='D' WHERE tbl_card_point_id='$card_point_id_enc' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_CATEGORY." WHERE tbl_category_id='$tbl_category_id' ";
		//$this->cf->deleteFrom($qry);
	}
	
	function getPointSettings($tbl_school_id) {
		$qry_school_settings = "SELECT * FROM ".TBL_SCHOOL_COMMON_SETTINGS." WHERE is_active='Y' AND tbl_school_id='$tbl_school_id' ";
        $dataSchoolSettings  = $this->cf->selectMultiRecords($qry_school_settings);
		$total_points        = $dataSchoolSettings[0]['total_points'];
		return $total_points;
	}
  
	
	 //STUDENT CARDS POINTS CONFIGURATION

	//UPDATE STUDENT PROMOTION
	/**
	* @desc		Update Student Promotion
	* @param	string $tbl_student_id
	* @access	default
	*/
	function update_promotion($tbl_student_id,$tbl_class_id_promotion,$tbl_academic_year_promotion,$tbl_school_id){
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
        $qry = "UPDATE ".TBL_STUDENT." SET tbl_class_id='".$tbl_class_id_promotion."', tbl_academic_year_id='".$tbl_academic_year_promotion."' 
		        WHERE tbl_student_id='".$tbl_student_id."' AND  tbl_school_id='$tbl_school_id' ";
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_CATEGORY." WHERE tbl_category_id='$tbl_category_id' ";
		//$this->cf->deleteFrom($qry);
	}
	//END STUDENT PROMOTION
	
	// STUDENT DETAILS
	function student_info($tbl_student_id,$tbl_school_id){
		 $qry = "SELECT ST.id,ST.tbl_student_id,ST.first_name,ST.last_name,ST.first_name_ar,ST.last_name_ar,ST.mobile,ST.email,ST.country,ST.added_date,ST.tbl_school_id
		           ,ST.student_user_id , ST.tbl_emirates_id as student_emirates_id, ST.student_pass_code
				   ,ST.is_active,ST.dob_month,ST.dob_day,ST.dob_year, ST.tbl_class_id, ST.gender, ST.file_name_updated, P.tbl_parent_id, P.first_name as parent_first_name,P.last_name as parent_last_name
				   ,P.first_name_ar AS parent_first_name_ar, P.last_name_ar AS parent_last_name_ar
				   ,P.dob_month as parent_dob_month,P.dob_day as parent_dob_day ,P.dob_year as parent_dob_year , P.gender as parent_gender, P.mobile as parent_mobile
				   ,P.email as parent_email , P.user_id as parent_user_id , P.emirates_id as parent_emirates_id, P.pass_code as parent_pass_code
				   ,C.class_name, C.class_name_ar, S.section_name, S.section_name_ar, T.school_type, T.school_type_ar 
					FROM ".TBL_STUDENT." AS ST 
					LEFT JOIN ".TBL_PARENT_STUDENT." AS STP ON STP.tbl_student_id=ST.tbl_student_id
					LEFT JOIN ".TBL_PARENT." AS P ON  P.tbl_parent_id = STP.tbl_parent_id
					LEFT JOIN ".TBL_CLASS."  AS C ON  C.tbl_class_id = ST.tbl_class_id
					LEFT JOIN  ".TBL_SECTION." AS S  ON  C.tbl_section_id = S.tbl_section_id  
					LEFT JOIN  ".TBL_SCHOOL_TYPE." AS T  ON  T.tbl_school_type_id= C.tbl_school_type_id  
					WHERE  1 ";
		$qry .= " AND ST.tbl_student_id  = '".$tbl_student_id."' ";;
		if($tbl_school_id<>"")
		{
			$qry .= " AND ST.tbl_school_id= '".$tbl_school_id."' ";
		}
		$res = $this->cf->selectMultiRecords($qry);
		return $res;
	}
	
 // Student Point Information (Admin/Parent - First Tab)
   function student_points_details($q, $tbl_student_id,$tbl_class_id,$tbl_school_id,$offset='') {
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		$tbl_class_id   = $this->cf->get_data(trim($tbl_class_id));
		$tbl_school_id  = $this->cf->get_data(trim($tbl_school_id));
		$qry = "SELECT P.points_student, P.comments_student, P.added_date, T.first_name,T.last_name, 
		       T.first_name_ar, T.last_name_ar ,C.class_name,C.class_name_ar,S.section_name,S.section_name_ar
				    FROM ".TBL_POINTS." AS P 
					LEFT JOIN ".TBL_TEACHER." AS T ON  T.tbl_teacher_id = P.tbl_teacher_id
					LEFT JOIN ".TBL_CLASS."  AS C ON  C.tbl_class_id = P.tbl_class_id
					LEFT JOIN  ".TBL_SECTION." AS S  ON  S.tbl_section_id = C.tbl_section_id    
			        WHERE tbl_student_id='$tbl_student_id' AND P.is_active='Y'  ";
		if($tbl_class_id <>"")
		{
			$qry .= " AND P.tbl_class_id= '$tbl_class_id' ";
		}
		if($tbl_school_id <>"")
		{
			$qry .= " AND P.tbl_school_id= '$tbl_school_id' ";
		}
		$qry .= " GROUP BY P.tbl_class_id ORDER BY P.added_date DESC ";
		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_CARD_CATEGORY_PAGING;
		}
		$rs = $this->cf->selectMultiRecords($qry);
	return $rs;
	}
	
	function total_points_details($q, $tbl_student_id,$tbl_class_id,$tbl_school_id) {
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		$tbl_class_id   = $this->cf->get_data(trim($tbl_class_id));
		$tbl_school_id  = $this->cf->get_data(trim($tbl_school_id));
		$qry = "SELECT P.points_student
				    FROM ".TBL_POINTS." AS P 
					LEFT JOIN ".TBL_TEACHER." AS T ON  T.tbl_teacher_id = P.tbl_teacher_id
					LEFT JOIN ".TBL_CLASS."  AS C ON  C.tbl_class_id = P.tbl_class_id
					LEFT JOIN  ".TBL_SECTION." AS S  ON  S.tbl_section_id = C.tbl_section_id    
			        WHERE tbl_student_id='$tbl_student_id' AND P.is_active='Y'  ";
		if($tbl_class_id <>"")
		{
			$qry .= " AND P.tbl_class_id= '$tbl_class_id' ";
		}
		if($tbl_school_id <>"")
		{
			$qry .= " AND P.tbl_school_id= '$tbl_school_id' ";
		}
		$qry .= " GROUP BY P.tbl_class_id ORDER BY P.added_date DESC ";
		$rs = $this->cf->selectMultiRecords($qry);
	return count($rs);
	}
	// End Student Point Information (Admin/Parent - First Tab)
	
	// Student Card Information (Admin/Parent - Second Tab)
	function student_cards_details($q, $tbl_student_id,$tbl_class_id,$tbl_school_id,$offset='') {
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		$tbl_class_id   = $this->cf->get_data(trim($tbl_class_id));
		$tbl_school_id  = $this->cf->get_data(trim($tbl_school_id));
		$offset         = $this->cf->get_data($offset);
	
		
		$qry =  "SELECT CP.category_name_en, CP.category_name_ar, CARDS.card_type, CARDS.card_issue_type, CARDS.card_point, CARDS.added_date, CARDS.tbl_student_id,
		            T.first_name,T.last_name,T.first_name_ar, T.last_name_ar , C.class_name, C.class_name_ar, S.section_name, S.section_name_ar
				    FROM ".TBL_CARDS." AS CARDS
					LEFT JOIN ".TBL_CARD_POINTS." AS CP ON  CP.tbl_card_category_id = CARDS.card_type
					LEFT JOIN ".TBL_TEACHER." AS T ON  T.tbl_teacher_id = CARDS.tbl_teacher_id
					LEFT JOIN ".TBL_CLASS."  AS C ON  C.tbl_class_id = CARDS.tbl_class_id
					LEFT JOIN  ".TBL_SECTION." AS S  ON  S.tbl_section_id = C.tbl_section_id    
			        WHERE CARDS.tbl_student_id='$tbl_student_id' AND CARDS.is_active='Y'  ";
		if($tbl_class_id <>"")
		{
			$qry .= " AND CARDS.tbl_class_id= '$tbl_class_id' ";
		}
		if($tbl_school_id <>"")
		{
			$qry .= " AND CARDS.tbl_school_id= '$tbl_school_id' ";
		}
		$qry .= " ORDER BY CARDS.added_date DESC ";
		
		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_CARD_CATEGORY_PAGING;
		}
		
		$rs = $this->cf->selectMultiRecords($qry);
	return $rs;
	}
	
	function total_student_cards($q, $tbl_student_id,$tbl_class_id,$tbl_school_id) {
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		$tbl_class_id   = $this->cf->get_data(trim($tbl_class_id));
		$tbl_school_id  = $this->cf->get_data(trim($tbl_school_id));
		
		$qry =  "SELECT CARDS.card_point
				    FROM ".TBL_CARDS." AS CARDS
					LEFT JOIN ".TBL_CARD_POINTS." AS CP ON  CP.tbl_card_category_id = CARDS.card_type
					LEFT JOIN ".TBL_TEACHER." AS T ON  T.tbl_teacher_id = CARDS.tbl_teacher_id
					LEFT JOIN ".TBL_CLASS."  AS C ON  C.tbl_class_id = CARDS.tbl_class_id
					LEFT JOIN  ".TBL_SECTION." AS S  ON  S.tbl_section_id = C.tbl_section_id    
			        WHERE CARDS.tbl_student_id='$tbl_student_id' AND CARDS.is_active='Y'  ";
		if($tbl_class_id <>"")
		{
			$qry .= " AND CARDS.tbl_class_id= '$tbl_class_id' ";
		}
		if($tbl_school_id <>"")
		{
			$qry .= " AND CARDS.tbl_school_id= '$tbl_school_id' ";
		}
		$qry .= " ORDER BY CARDS.added_date DESC ";
		$rs = $this->cf->selectMultiRecords($qry);
	return count($rs);
	}
	// Student Card Information (Admin/Parent - Second Tab)
	
	// Student Record Information (Admin/Parent - Third Tab)
	function get_student_records($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parenting_school_cat_id,$student_id,$tbl_class_id) {
		$sort_name      = $this->cf->get_data($sort_name);
		$sort_by 	    = $this->cf->get_data($sort_by);
		$offset         = $this->cf->get_data($offset);
		$student_id     = $this->cf->get_data($student_id);
		$tbl_class_id   = $this->cf->get_data($tbl_class_id);
		$q              = urldecode($this->cf->get_data($q));
		$is_active      = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry  = " SELECT PS.*,PSC.title_en,PSC.title_ar FROM ".TBL_PARENTING_SCHOOL." AS PS LEFT JOIN ".TBL_PARENTING_SCHOOL_CAT." AS PSC ON 
		          PSC.tbl_parenting_school_cat_id= PS.tbl_parenting_school_cat_id  WHERE 1  ";
		if($tbl_school_id<>""){
			$qry .= " AND PS.tbl_school_id='".$tbl_school_id."'";
		}
		if($tbl_parenting_school_cat_id<>"")
			$qry .= " AND PS.tbl_parenting_school_cat_id='$tbl_parenting_school_cat_id' "; 
		
		if($student_id<>"")
		{
			$qry .= " AND PS.tbl_parenting_school_id IN  (SELECT tbl_parenting_school_id FROM ".TBL_ASSIGN_RECORDS." WHERE tbl_student_id='$student_id' AND tbl_school_id='".$tbl_school_id."' 
			
			AND tbl_class_id='".$tbl_class_id."' )";
		}
		
		//Active/Deactive
		if(trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PS.is_active='$is_active' ";
		}
		$qry .= " AND PS.is_active<>'D' "; 
			
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( PS.parenting_title_en LIKE '%$q%' 
					   	OR PS.parenting_title_ar LIKE '%$q%' 
					   	OR PSC.title_en LIKE '%$q%' 
					   	OR PSC.title_ar LIKE '%$q%' ) ";
		}
		
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY PS.$sort_name $sort_by";
		} else {
			$qry .= " ORDER BY PS.id DESC";
		}
		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_CARD_CATEGORY_PAGING;
		}
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
      }
	  
	  
	  function get_total_student_records($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parenting_school_cat_id,$student_id,$tbl_class_id) {
		$sort_name      = $this->cf->get_data($sort_name);
		$sort_by 	    = $this->cf->get_data($sort_by);
		$offset         = $this->cf->get_data($offset);
		$student_id     = $this->cf->get_data($student_id);
		$tbl_class_id   = $this->cf->get_data($tbl_class_id);
		$q              = urldecode($this->cf->get_data($q));
		$is_active      = $this->cf->get_data($is_active);
		
		$qry  = " SELECT PS.*,PSC.title_en,PSC.title_ar FROM ".TBL_PARENTING_SCHOOL." AS PS LEFT JOIN ".TBL_PARENTING_SCHOOL_CAT." AS PSC ON 
		          PSC.tbl_parenting_school_cat_id= PS.tbl_parenting_school_cat_id  WHERE 1  ";
		if($tbl_school_id<>""){
			$qry .= " AND PS.tbl_school_id='".$tbl_school_id."'";
		}
		if($tbl_parenting_school_cat_id<>"")
			$qry .= " AND PS.tbl_parenting_school_cat_id='$tbl_parenting_school_cat_id' "; 
		
		if($student_id<>"")
		{
			$qry .= " AND PS.tbl_parenting_school_id IN  (SELECT tbl_parenting_school_id FROM ".TBL_ASSIGN_RECORDS." WHERE tbl_student_id='$student_id' AND tbl_school_id='".$tbl_school_id."' 
			
			AND tbl_class_id='".$tbl_class_id."' )";
		}
		
		//Active/Deactive
		if(trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PS.is_active='$is_active' ";
		}
		$qry .= " AND PS.is_active<>'D' "; 
			
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( PS.parenting_title_en LIKE '%$q%' 
					   	OR PS.parenting_title_ar LIKE '%$q%' 
					   	OR PSC.title_en LIKE '%$q%' 
					   	OR PSC.title_ar LIKE '%$q%' ) ";
		}
		
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
      }
	 // End Student Record Information (Admin/Parent - Third Tab) 
	 
	 // Student - Parent Messages Information (Admin/Parent - Fourth Tab)
	 function list_parent_messages_of_student($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parent_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry = "SELECT C.tbl_contact_us_id, C.contact_us_comments, P.first_name,P.last_name,P.first_name_ar,P.last_name_ar,P.email,C.added_date FROM ".TBL_CONTACT_US." AS C LEFT JOIN ".TBL_PARENT." AS P ON P.tbl_parent_id=C.tbl_parent_id WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND C.tbl_school_id= '".$tbl_school_id."' ";
		}
		
		if($tbl_parent_id<>"")
		{
			$qry .= " AND C.tbl_parent_id= '".$tbl_parent_id."' ";
		}
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND C.is_active='$is_active' ";
		}
		$qry .= " AND C.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " AND  ( C.contact_us_comments LIKE '%$q%'
				        OR CONCAT(P.first_name,' ',P.last_name) LIKE '%$q%'
					   	OR CONCAT(TRIM(P.first_name_ar),' ',TRIM(P.last_name_ar)) LIKE '%$q%' ) ";
		}
        
		$qry .= "  GROUP BY C.added_date ";
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY C.$sort_name $sort_by";
		} else {
			$qry .= " ORDER BY C.id DESC";
		}
		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_CARD_CATEGORY_PAGING;
		}
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}  
	  
	 function get_total_parent_messages_of_student($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id, $tbl_parent_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry = "SELECT C.tbl_contact_us_id, C.contact_us_comments, P.first_name,P.last_name,P.first_name_ar,P.last_name_ar,P.email,C.added_date FROM ".TBL_CONTACT_US." AS C LEFT JOIN ".TBL_PARENT." AS P ON P.tbl_parent_id=C.tbl_parent_id WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND C.tbl_school_id= '".$tbl_school_id."' ";
		}
		
		if($tbl_parent_id<>"")
		{
			$qry .= " AND C.tbl_parent_id= '".$tbl_parent_id."' ";
		}
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND C.is_active='$is_active' ";
		}
		$qry .= " AND C.is_active<>'D' "; 
		//Search
		if (trim($q) != "") {
			$qry    .= " AND  ( C.contact_us_comments LIKE '%$q%'
				        OR CONCAT(P.first_name,' ',P.last_name) LIKE '%$q%'
					   	OR CONCAT(TRIM(P.first_name_ar),' ',TRIM(P.last_name_ar)) LIKE '%$q%' ) ";
		}
		$qry .= "  GROUP BY C.added_date ";
		
		$results = $this->cf->selectMultiRecords($qry);
	return count($results);
	}  
	 
	// Student - Parent Messages Information (Admin/Parent - Fourth Tab) 
	 
	//END STUDENT DETAILS
	
	/**
	* @desc		Check if student's emirates id already exists in the system
	* @param	String params
	* @access	default
	*/
	function is_exist_student_eid($tbl_emirates_id, $tbl_school_id="") {
		$qry = "SELECT tbl_emirates_id FROM ".TBL_STUDENT." WHERE 1 ";
	
		if (trim($tbl_emirates_id) != "") {
			$qry .= " AND tbl_emirates_id = '".$tbl_emirates_id."' ";
		}
		
		if (trim($tbl_school_id) != "") {
			$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		}
		//echo $qry;
		//exit();
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}
	
	
	/**
	* @desc		Check if tbl_student_id already exists in the system
	* @param	String params
	* @access	default
	*/
	function is_exist_student_id($tbl_student_id) {
		$qry = "SELECT id FROM ".TBL_STUDENT." WHERE tbl_student_id='".$tbl_student_id."'";
		$results = $this->cf->selectMultiRecords($qry);
		return $results;
	}
	
	
	// STUDENT ACTIVITY - PERFORMANCE CONFIGURATION
   /**
	* @desc		Get all student topics
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function get_all_student_topics($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id, $tbl_class_id='') {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry = "SELECT PM.*, PC.performance_category_en, PC.performance_category_ar  FROM ".TBL_STUDENT_PERFORMANCE_MASTER." AS PM LEFT JOIN ".TBL_STUDENT_PERFORMANCE_CATEGORY." AS PC ON PC.tbl_performance_category_id=PM.tbl_performance_category_id WHERE 1 ";
		$qry .= " AND PM.tbl_school_id= '".$tbl_school_id."' ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PM.is_active='$is_active' ";
		}
		$qry .= " AND PM.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( PM.topic_en LIKE '%$q%' 
					   	   OR PM.topic_ar LIKE '%$q%' OR PC.performance_category_en LIKE '%$q%'  OR PC.performance_category_ar LIKE '%$q%' ) ";
		}
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY PM.id DESC ";
		}
		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_CARD_CATEGORY_PAGING;
		}
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	/**
	* @desc		Get total student topics
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_student_topics($q, $is_active, $tbl_school_id, $tbl_class_id='') {
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);

		$qry = "SELECT PM.tbl_performance_id  FROM ".TBL_STUDENT_PERFORMANCE_MASTER." AS PM LEFT JOIN ".TBL_STUDENT_PERFORMANCE_CATEGORY." AS PC ON PC.tbl_performance_category_id=PM.tbl_performance_category_id WHERE 1 ";
		$qry .= " AND PM.tbl_school_id= '".$tbl_school_id."' ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PM.is_active='$is_active' ";
		}
		$qry .= " AND PM.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( PM.topic_en LIKE '%$q%' 
					   	   OR PM.topic_ar LIKE '%$q%' OR PC.performance_category_en LIKE '%$q%'  OR PC.performance_category_ar LIKE '%$q%' ) ";
		}
	
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
	}
	
	
	function get_student_topic_category($is_active)
	{
		$qry = "SELECT * FROM ".TBL_STUDENT_PERFORMANCE_CATEGORY." AS PC WHERE 1 ";
		//$qry .= " AND PC.tbl_school_id= '".$tbl_school_id."' ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PC.is_active='$is_active' ";
		}
		$qry .= " AND PC.is_active<>'D' "; 
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
	}
	
	

	function is_exist_student_topic($tbl_performance_id, $topic_name_en, $topic_name_ar, $tbl_school_id){
		$tbl_performance_id       = $this->cf->get_data($tbl_performance_id);
		$topic_name_en 		      = $this->cf->get_data($topic_name_en);
		$topic_name_ar            = $this->cf->get_data($topic_name_ar);
		$tbl_school_id            = $this->cf->get_data($tbl_school_id);

		$qry = "SELECT * FROM ".TBL_STUDENT_PERFORMANCE_MASTER." WHERE 1 ";
		if (trim($topic_name_en) != "") {
			 $qry .= " AND topic_en='$topic_name_en' ";
		}
	
		if (trim($tbl_performance_id) != "") {
			 $qry .= " AND tbl_performance_id <> '$tbl_performance_id' ";
		}
		
	    $qry .= " AND is_active<>'D' "; 
		$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}

	/**
	* @desc		Add student topic
	* @param	String params
	* @access	default
	*/
	function add_student_topic($tbl_performance_id,$topic_name_en,$topic_name_ar,$tbl_performance_category_id,$tbl_school_id,$str)
    {
	  $tbl_performance_id       			= $this->cf->get_data($tbl_performance_id);
	  $tbl_performance_category_id        = $this->cf->get_data($tbl_performance_category_id);
	  $topic_name_en 		      			= $this->cf->get_data($topic_name_en);
	  $topic_name_ar            			= $this->cf->get_data($topic_name_ar);
	  $tbl_school_id            			= $this->cf->get_data($tbl_school_id);
			
	  $qry_ins = "INSERT INTO ".TBL_STUDENT_PERFORMANCE_MASTER." (`tbl_performance_id`, `topic_en`, `topic_ar`, `is_active`,`tbl_performance_category_id`, `tbl_school_id`)
						VALUES ('$tbl_performance_id', '$topic_name_en', '$topic_name_ar', 'Y', '$tbl_performance_category_id', '$tbl_school_id')";
	  $this->cf->insertInto($qry_ins);
		
	  $str     = explode("&", $str);
	  for($m=0; $m<count($str); $m++) {
		  if($str[$m]<>""){
				$tbl_performance_class_id      	= md5(uniqid(rand()));
				$tbl_class_id = $str[$m];
				$tbl_student_group_students_id = substr(md5(uniqid(rand())),0,10);
				$qryIns = "INSERT INTO ".TBL_PERFORMANCE_CLASS." (`tbl_performance_class_id`,`tbl_performance_id`,`tbl_class_id`,`tbl_school_id`,`is_active`) 
										 VALUES ('$tbl_performance_class_id','$tbl_performance_id','$tbl_class_id', '$tbl_school_id' ,'Y') ";
				$this->cf->insertInto($qryIns);
			}
	   }
		
		return "Y";
	} 
	
	/**
	* @desc		Activate student topic
	* @param	string $tbl_performance_id
	* @access	default
	*/
	function activateStudentTopic($tbl_performance_id) {
		$tbl_performance_id = $this->cf->get_data(trim($tbl_performance_id));
		$qry = "UPDATE ".TBL_STUDENT_PERFORMANCE_MASTER." SET is_active='Y' WHERE tbl_performance_id='$tbl_performance_id' ";
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate student topic
	* @param	string $tbl_performance_id
	* @access	default
	*/
	function deactivateStudentTopic($tbl_performance_id) {
		$tbl_performance_id = $this->cf->get_data(trim($tbl_performance_id));
		$qry = "UPDATE ".TBL_STUDENT_PERFORMANCE_MASTER." SET is_active='N' WHERE tbl_performance_id='$tbl_performance_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}
	
	
	/**
	* @desc		Delete student card
	* @param	string $tbl_student_id
	* @access	default
	*/
	function deleteStudentTopic($tbl_performance_id) {
		$tbl_performance_id = $this->cf->get_data(trim($tbl_performance_id));
        $qry = "UPDATE ".TBL_STUDENT_PERFORMANCE_MASTER." SET is_active='D' WHERE tbl_performance_id='$tbl_performance_id' ";
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_CATEGORY." WHERE tbl_category_id='$tbl_category_id' ";
		//$this->cf->deleteFrom($qry);
	}
	
    /**
	* @desc		Update student card 
	* @param	string $tbl_student_id
	* @access	default
	*/
	function save_student_topic_changes($tbl_performance_id,$topic_name_en,$topic_name_ar,$tbl_performance_category_id,$tbl_school_id,$str)
	{
	    $tbl_performance_id       			= $this->cf->get_data($tbl_performance_id);
		$tbl_performance_category_id        = $this->cf->get_data($tbl_performance_category_id);
		$topic_name_en 		      			= $this->cf->get_data($topic_name_en);
		$topic_name_ar            			= $this->cf->get_data($topic_name_ar);
		$tbl_school_id            			= $this->cf->get_data($tbl_school_id);
	
		if($tbl_school_id<>"")
		{
		 $qry  = "UPDATE ".TBL_STUDENT_PERFORMANCE_MASTER." SET topic_en='$topic_name_en', topic_ar='$topic_name_ar',tbl_performance_category_id='$tbl_performance_category_id'
			         WHERE tbl_performance_id = '$tbl_performance_id' and  tbl_school_id = '$tbl_school_id' ";
		 $this->cf->update($qry);
			   
		 $qryDel = "DELETE FROM ".TBL_PERFORMANCE_CLASS." WHERE tbl_performance_id='$tbl_performance_id' ";
		 $this->cf->deleteFrom($qryDel);
		   
		 $str     = explode("&", $str);
		 for($m=0; $m<count($str); $m++) {
			  if($str[$m]<>""){
					$tbl_performance_class_id      	= md5(uniqid(rand()));
					$tbl_class_id = $str[$m];
					$tbl_student_group_students_id = substr(md5(uniqid(rand())),0,10);
					$qryIns = "INSERT INTO ".TBL_PERFORMANCE_CLASS." (`tbl_performance_class_id`,`tbl_performance_id`,`tbl_class_id`, `tbl_school_id`, `is_active`) 
											 VALUES ('$tbl_performance_class_id','$tbl_performance_id','$tbl_class_id','$tbl_school_id','Y') ";
					$this->cf->insertInto($qryIns);
				}
		   }
			
			return "Y";
		}else{
		   return "N";	
			
		}
	}
	
	// GET student topic information
	function get_student_topic_info($is_active, $tbl_performance_id, $tbl_school_id) {
		$tbl_performance_id  = $this->cf->get_data($tbl_performance_id);
		
		$qry = "SELECT *  FROM ".TBL_STUDENT_PERFORMANCE_MASTER." AS PM LEFT JOIN ".TBL_STUDENT_PERFORMANCE_CATEGORY." AS PC ON PC.tbl_performance_category_id=PM.tbl_performance_category_id WHERE 1 ";
		$qry .= " AND PM.tbl_school_id= '".$tbl_school_id."' ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PM.is_active='$is_active' ";
		}
		$qry .= " AND PM.is_active<>'D' "; 
		$qry .= " AND PM.tbl_performance_id = '".$tbl_performance_id."' "; 
	
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	
    function save_student_performance_rate($tbl_performance_student_id,$topic_id,$topic_val,$tbl_student_id,$tbl_class_id,$tbl_teacher_id,$tbl_school_id)
    {
		$tbl_performance_student_id       			= $this->cf->get_data($tbl_performance_student_id);
		$tbl_performance_id                         = $this->cf->get_data($topic_id);
		$performance_value		      			    = $this->cf->get_data($topic_val);
		$tbl_student_id            			        = $this->cf->get_data($tbl_student_id);
		$tbl_class_id 		      			        = $this->cf->get_data($tbl_class_id);
		$tbl_teacher_id            			        = $this->cf->get_data($tbl_teacher_id);
		$tbl_school_id            			        = $this->cf->get_data($tbl_school_id);
			
		$qry_ins = "INSERT INTO ".TBL_PERFORMANCE_STUDENT." (`tbl_performance_student_id`, `tbl_performance_id`, `tbl_student_id`, `tbl_class_id`,`tbl_teacher_id`, `performance_value`, `reported_date`, `tbl_school_id`)
						VALUES ('$tbl_performance_student_id', '$tbl_performance_id', '$tbl_student_id', '$tbl_class_id', '$tbl_teacher_id', '$performance_value', NOW(), '$tbl_school_id')";
		$this->cf->insertInto($qry_ins);
		return "Y";
	} 
	
	function get_list_students_group($tbl_school_id,$is_active)	
	{
	  $qry = " SELECT * FROM ".TBL_STUDENT_GROUP."  AS SG WHERE 1 ";
	  if($tbl_school_id<>"")
		{
			$qry .= " AND SG.tbl_school_id= '".$tbl_school_id."' ";
		}
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND SG.is_active='$is_active' ";
		}
	  $qry .= " AND SG.is_active<>'D' "; 
	  $qry .= " ORDER BY SG.group_name_en ASC  ";
	  $results = $this->cf->selectMultiRecords($qry);
	  return $results;
	  
	}
	
	
	function get_selected_class_by_performance($tbl_performance_id,$tbl_school_id) {
	    $qry = " SELECT tbl_class_id FROM  ".TBL_PERFORMANCE_CLASS." WHERE tbl_performance_id='".$tbl_performance_id."' ";
		 if($tbl_school_id<>"")
		{
			$qry .= " AND tbl_school_id= '".$tbl_school_id."' ";
		}
		$qry .= " GROUP BY tbl_class_id ORDER BY id ASC ";
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
	}
	
	
	//STUDENT ACTIVITY - PERFORMANCE CONFIGURATION
	
	// BACKEND STUDENT LOGIN

	function authenticate_student($username, $password, $is_approved="") {
		$username    = $this->cf->get_data(trim($username));
		$password = $this->cf->get_data(trim($password));
		$qry = "SELECT tbl_student_id, student_password, student_salt, student_pass_code FROM ".TBL_STUDENT." WHERE student_user_id='$username' AND is_active='Y' ";
		if ($is_approved != "") {
			$qry .= " AND is_approved='".$is_approved."'";
		}
		$result = $this->cf->selectMultiRecords($qry);
		//print_r($result);
		if ($result) {
			$salt 				= $result[0]['student_salt'];
			$db_password 		= $result[0]['student_password'];
			$student_pass_code 	= $result[0]['student_pass_code'];
		} else {
			return "D";//User does not exists
		}

		$hash = sha1($salt . sha1($password));
		
		if (trim($hash) != trim($db_password)) {                
			return "N";
		} else { 
		    $tbl_student_id = $result[0]['tbl_student_id'];
		    $qry_update = "UPDATE ".TBL_STUDENT." SET is_logged='Y' WHERE tbl_student_id='$tbl_student_id' ";
			$this->cf->update($qry_update);    
			return "Y";
		}
	}
	
	function get_student_id($username) {
		$username  = $this->cf->get_data(trim($username));
		$qry = "SELECT tbl_student_id FROM ".TBL_STUDENT." WHERE student_user_id='$username' ";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs[0]['tbl_student_id'];
	}
	
	
	function get_student_info_by_email($email) {
		$email = $this->cf->get_data(trim($email));
		$rs    =  array();
		if($email<>""){
			$qry = "SELECT * FROM ".TBL_STUDENT." WHERE email='$email' ";
			$rs = $this->cf->selectMultiRecords($qry);
	   }
		return $rs;
	}
	

   // Change Password For all type of users
   function change_password($password,$tbl_user_id,$user_type='') {
		$hash = sha1($password);
		$salt = substr(md5(uniqid(rand(), true)), 0, 3);
		$hash = sha1($salt . $hash);
		$pass_code	   =	base64_encode($password);
		if($user_type=="student")
		{
			$qry_update = "UPDATE ".TBL_STUDENT." SET student_password='$hash', student_salt='$salt', student_pass_code='$pass_code' WHERE tbl_student_id='".$tbl_user_id."'";
			$this->cf->update($qry_update);
		}else if($user_type=="parent")
		{
			$qry_update = "UPDATE ".TBL_PARENT." SET password='$hash', salt='$salt', pass_code='$pass_code' WHERE tbl_parent_id='".$tbl_user_id."'";
			$this->cf->update($qry_update);
		}
		else if($user_type=="teacher")
		{
			$qry_update = "UPDATE ".TBL_TEACHER." SET password='$hash', salt='$salt', pass_code='$pass_code' WHERE tbl_teacher_id='".$tbl_user_id."'";
			$this->cf->update($qry_update);
		}else if($user_type=="bus_supervisor")
		{
			$qry_update = "UPDATE ".TBL_BUS." SET bus_password='$hash', salt='$salt', pass_code='$pass_code' WHERE tbl_bus_id='".$tbl_user_id."'";
			$this->cf->update($qry_update);
		}else 
		{
			$qry_update = "UPDATE ".TBL_ADMIN_USERS." SET password='$hash', salt='$salt', password_code='$pass_code' WHERE tbl_admin_id='".$tbl_user_id."'";
			$this->cf->update($qry_update);
		}
		return true;
	}
	
// leave indication for student list view
   function student_leave_today($tbl_student_id,$tbl_school_id)
   {
	   $current_date = date("Y-m-d");
	   $qry = " SELECT tbl_leave_application_id,comments_parent FROM `tbl_leave_application` WHERE 1 ";
	   $qry .= " AND `start_date` <= '".$current_date."' AND `end_date` >= '".$current_date."' ";
	   if($tbl_student_id<>"") 
			$qry .= " AND `tbl_student_id` = '".$tbl_student_id."' ";
	
	    if($tbl_school_id<>"") 
			$qry .= " AND `tbl_school_id` = '".$tbl_school_id."' ";
	   
	   $qry .= " ORDER BY id desc "; 
	   $rs = $this->cf->selectMultiRecords($qry);
	   return $rs;
	   
   }
   
   
   	function get_student_group($tbl_school_id, $tbl_teacher_id) {
		
		$qry = "SELECT * FROM ".TBL_STUDENT_GROUP." AS SG WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND SG.tbl_school_id= '".$tbl_school_id."' ";
		}
		if($tbl_teacher_id<>"")
		{
			$qry .= " AND SG.added_by = '".$tbl_teacher_id."' ";
		}
		//Active/Deactive
		$qry .= " AND SG.is_active='Y' ";
		$qry .= " ORDER BY SG.id DESC";
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	
	/******************* students profile - mobile app *******************/
	
	function get_total_issued_cards_student($tbl_class_id, $tbl_student_id, $tbl_school_id) {
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$arr = array();
		/*$qry = "SELECT count(DISTINCT(card_type)) AS cntCards FROM ".TBL_CARDS." AS C WHERE  1 ";*/
		
		$qry = "SELECT 
		        count(case when C.card_issue_type ='cancel'  then 1 end) as cancel_count,
			    count(case when C.card_issue_type ='issue' then 1 end) as issue_count FROM ".TBL_CARDS." AS C WHERE  1 ";
		$qry .= " AND C.tbl_school_id= '".$tbl_school_id."' ";
		$qry .= " AND C.tbl_student_id= '".$tbl_student_id."' ";
		$qry .= " AND C.tbl_class_id= '".$tbl_class_id."' ";
		$qry .= " AND C.is_active='Y' "; 
		$results = $this->cf->selectMultiRecords($qry);
		
		$cntIssue        = $results[0]['issue_count'];
		$cntCancel       = $results[0]['cancel_count'];
		$cntIssuedCards  = $cntIssue - $cntCancel;
		$arr[0]['cntCards']    = $cntIssuedCards;
	    return $arr;
	}
	
    function get_total_issued_points_student($tbl_class_id, $tbl_student_id, $tbl_school_id) {
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT SUM(points_student) AS totPoints FROM ".TBL_POINTS." AS P WHERE  1 ";
		$qry .= " AND P.tbl_school_id= '".$tbl_school_id."' ";
		$qry .= " AND P.tbl_student_id= '".$tbl_student_id."' ";
		$qry .= " AND P.tbl_class_id= '".$tbl_class_id."' ";
		$qry .= " AND P.is_active='Y' "; 
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
	}
	
	
	 function get_total_student_absent($tbl_class_id, $tbl_student_id, $tbl_school_id) {
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT COUNT(is_present) AS totAbsent FROM ".TBL_TEACHER_ATTENDANCE." AS TA WHERE  1 ";
		$qry .= " AND TA.tbl_school_id= '".$tbl_school_id."' ";
		$qry .= " AND TA.tbl_student_id= '".$tbl_student_id."' ";
		$qry .= " AND TA.tbl_class_id= '".$tbl_class_id."' ";
		$qry .= " AND TA.is_active='Y' "; 
		$qry .= " AND TA.is_present='N' "; 
		//$qry .= " GROUP BY TA.attendance_date "; 
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
	}
	
	
	
	function get_overview_student_points_detail($tbl_student_id,$tbl_class_id,$tbl_semester_id,$tbl_school_id, $is_active='',$from_date='',$to_date='',$range) {
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		$qry = "SELECT SUM( P.points_student) as student_point, P.comments_student, BP.point_name_en, BP.point_name_ar FROM ".TBL_POINTS." AS P LEFT JOIN ".TBL_BEHAVIOUR_POINTS." AS BP 
		        ON BP.tbl_point_category_id = P.tbl_point_category_id
		WHERE 1 ";
		$qry .=  "  AND P.tbl_student_id='$tbl_student_id' ";
		if($tbl_class_id<>"") {
			$qry .=  "  AND P.tbl_class_id='$tbl_class_id' ";
		}
		if($tbl_semester_id<>"") {
			$qry .=  "  AND P.tbl_semester_id='$tbl_semester_id' ";
		}
		if($tbl_school_id<>"") {
			$qry .=  "  AND P.tbl_school_id='$tbl_school_id' ";
		}
		
		if($from_date<>"" && $from_date<>'""' )
		{
			$from_date       = $from_date." 00:00:00";
			$to_date         = $to_date." 23:59:60";
			$qry .= " AND ( P.added_date >=  '".$from_date."' AND  P.added_date <='".$to_date."' ) ";
		}
			
		 $qry .= " AND P.is_active='Y'  ";
		 $qry .= " AND P.tbl_point_category_id<>'' ";  
		 $qry .= " GROUP BY P.tbl_point_category_id ";
		 $qry .= " ORDER BY P.added_date DESC, P.is_active ASC ";
		 $rs = $this->cf->selectMultiRecords($qry);
	return $rs;
	}
	
	
	 function get_total_student_attendance($tbl_student_id,$tbl_class_id,$tbl_school_id, $is_active='',$from_date='',$to_date='',$range) {
	
		
		/*$qry = " SELECT TA.tbl_student_id
      			,count(case when TA.is_present ='N'  then 1 end) as absent_count
      			,count(case when TA.is_present ='Y' then 1 end) as present_count
      			,count(distinct TA.attendance_date) as total_count ";
	    $qry .= " FROM ".TBL_TEACHER_ATTENDANCE." AS TA WHERE  1 ";
	    $qry .= " AND TA.tbl_school_id= '".$tbl_school_id."' ";
		$qry .= " AND TA.tbl_student_id= '".$tbl_student_id."' ";
		$qry .= " AND TA.tbl_class_id= '".$tbl_class_id."' ";
		$qry .= " AND TA.is_active='Y' "; 
 		$qry .= " GROUP BY TA.tbl_student_id " ;
		$results = $this->cf->selectMultiRecords($qry);
		return $results;*/
		
		 $qry = "SELECT  TA.attendance_date, TA.is_present
				, MONTHNAME(TA.attendance_date) AS attendance_month FROM ".TBL_TEACHER_ATTENDANCE." AS TA WHERE 1 ";
			
		if($tbl_student_id<>"")                                                 
		{
			$qry .= " AND TA.tbl_student_id = '".$tbl_student_id."' "; 
		}
		if($tbl_class_id<>"")
		{
			$qry .= " AND TA.tbl_class_id = '".$tbl_class_id."' "; 
		}
		if($tbl_school_id<>"")
		{
			$qry .= " AND TA.tbl_school_id = '".$tbl_school_id."' "; 
		}
	
		if($from_date<>"" && $from_date<>'""' )
		{
			$from_date       = $from_date." 00:00:00";
			$to_date         = $to_date." 23:59:60";
			$qry .= " AND ( TA.attendance_date >=  '".$from_date."' AND  TA.attendance_date <='".$to_date."' ) ";
		}
		
	   $qry .= " AND TA.is_active='Y'  "; 
	   $qry .= " GROUP BY TA.attendance_date ";
	   $qry .= " ORDER BY TA.attendance_date DESC, TA.is_present ASC ";
	   $results         = $this->cf->selectMultiRecords($qry);
		$x = 0;
		$totCnt = 0;
		$prCnt  = 0;
		$absCnt = 0;
		$b=0;
		for($t=0;$t<count($results);$t++)
		{   
			   
			$totCnt = $totCnt  + 1;
			if($results[$t]['is_present'] == "Y")
				$prCnt  = $prCnt   + 1;
			
			if($results[$t]['is_present'] == "N")	
				$absCnt  = $absCnt + 1;
			
			$attendance_date_array = explode(" ",$results[$t]['attendance_date']);
			
			$newResultsArray['attendance_month']    = $results[$t]['attendance_month'];
			$newResultsArray['tbl_student_id']      = $results[$t]['tbl_student_id'];
			$newResultsArray['tbl_class_id']        = $results[$t]['tbl_class_id'];
			$newResultsArray['tbl_teacher_id']      = $results[$t]['tbl_teacher_id'];
			$newResultsArray['absent_count']        = $absCnt;
			$newResultsArray['present_count']       = $prCnt;
			$newResultsArray['total_count']         = $totCnt;
			$newResultsArray['attendance_date']     = $attendance_date_array[0];
			$b = 1;
		}
		
		if($b==0)
		{
			$newResultsArray['attendance_month']      = $sel_month;
			$newResultsArray['tbl_student_id']      = '';
			$newResultsArray['tbl_class_id']        = '';
			$newResultsArray['tbl_teacher_id']      = '';
			$newResultsArray['absent_count']        = '1000';
			$newResultsArray['present_count']       = '1000';
			$newResultsArray['total_count']         = '1000';
			$newResultsArray['attendance_date']     = '';
		}
		return $newResultsArray;	
	}
	
	
	function get_student_attendance_graph($tbl_student_id,$tbl_class_id,$tbl_school_id,$is_active='',$from_date='',$to_date='',$range='') {
			  
		  $qry = "SELECT  TA.attendance_date, TA.is_present
				, MONTHNAME(TA.attendance_date) AS attendance_month FROM ".TBL_TEACHER_ATTENDANCE." AS TA WHERE 1 ";
			
			if($tbl_student_id<>"")                                                 
			{
				$qry .= " AND TA.tbl_student_id = '".$tbl_student_id."' "; 
			}
			if($tbl_class_id<>"")
			{
				$qry .= " AND TA.tbl_class_id = '".$tbl_class_id."' "; 
			}
			if($tbl_school_id<>"")
			{
				$qry .= " AND TA.tbl_school_id = '".$tbl_school_id."' "; 
			}
		
			if($from_date<>"" && $from_date<>'""' )
			{
				$from_date       = $from_date." 00:00:00";
				$to_date         = $to_date." 23:59:60";
				$qry .= " AND ( TA.attendance_date >=  '".$from_date."' AND  TA.attendance_date <='".$to_date."' ) ";
			}
			
		   $qry .= " AND TA.is_active='Y'  "; 
		   $qry .= " GROUP BY TA.attendance_date ";
		   $qry .= " ORDER BY TA.attendance_date DESC, TA.is_present ASC ";
		   $results         = $this->cf->selectMultiRecords($qry);
		  
		   $newResultsArray = array(); 
		  
		   if($range=="year")
		   {
					   $sel_month = date("F");
					   $prevMonth = "";
					   $x = 0;
					   for($c=0;$c<12;$c++)
					   { 
					        $totCnt = 0;
							$prCnt  = 0;
							$absCnt = 0;
							$b=0;
							for($t=0;$t<count($results);$t++)
							{   
								if($sel_month==$results[$t]['attendance_month'])
								{
								   
										$totCnt = $totCnt  + 1;
										if($results[$t]['is_present'] == "Y")
											$prCnt  = $prCnt   + 1;
										
										if($results[$t]['is_present'] == "N")	
											$absCnt  = $absCnt + 1;
										
										$attendance_date_array = explode(" ",$results[$t]['attendance_date']);
										
										$newResultsArray[$c]['attendance_month']    = $results[$t]['attendance_month'];
										$newResultsArray[$c]['tbl_student_id']      = $results[$t]['tbl_student_id'];
										$newResultsArray[$c]['tbl_class_id']        = $results[$t]['tbl_class_id'];
										$newResultsArray[$c]['tbl_teacher_id']      = $results[$t]['tbl_teacher_id'];
										$newResultsArray[$c]['absent_count']        = $absCnt;
										$newResultsArray[$c]['present_count']       = $prCnt;
										$newResultsArray[$c]['total_count']         = $totCnt;
										$newResultsArray[$c]['attendance_date']     = $attendance_date_array[0];
										$b = 1;
								}
							}
							
							if($b==0)
							{
								$newResultsArray[$c]['attendance_month']      = $sel_month;
								$newResultsArray[$c]['tbl_student_id']      = '';
								$newResultsArray[$c]['tbl_class_id']        = '';
								$newResultsArray[$c]['tbl_teacher_id']      = '';
								$newResultsArray[$c]['absent_count']        = '1000';
								$newResultsArray[$c]['present_count']       = '1000';
								$newResultsArray[$c]['total_count']         = '1000';
								$newResultsArray[$c]['attendance_date']     = '';
							}
							
							$sel_month = date("F", strtotime( date( 'Y-m-01' )." -$c months"));
			
							
							
							
					   }
		   } 
		   else if($range=="month")
		   {
					   $sel_date = date("Y-m");
					   $sel_date = $sel_date."-31";
					   $prevDate = "";
					   $x = 0;
					  
					   for($c=0;$c<31;$c++)
					   {
							for($t=0;$t<count($results);$t++)
							{   
								$dateArray =  explode(" ",$results[$t]['attendance_date']);
								
								$b=0;
								if($sel_date==$dateArray[0])
								{
									if($prevDate <> $sel_date)
									{
										$attendance_date_array = explode(" ",$results[$t]['attendance_date']);
										
										$newResultsArray[$x]['attendance_month']      = $results[$t]['attendance_month'];
										$newResultsArray[$x]['tbl_student_id']        = $results[$t]['tbl_student_id'];
										$newResultsArray[$x]['tbl_class_id']          = $results[$t]['tbl_class_id'];
										$newResultsArray[$x]['tbl_teacher_id']        = $results[$t]['tbl_teacher_id'];
										$newResultsArray[$x]['absent_count']          = $results[$t]['absent_count'];
										$newResultsArray[$x]['present_count']         = $results[$t]['present_count'];
										$newResultsArray[$x]['total_count']           = $results[$t]['total_count'];
										$newResultsArray[$x]['attendance_date']       = date("d M ",strtotime($attendance_date_array[0]));
										$x =$x+1;
										$t = count($results);
										$prevDate  =  $results[$t]['reported_date'];
										$b = 1;
									}
								}
							}
							
							if($b==0)
							{
								$newResultsArray[$x]['attendance_month']    = '';
								$newResultsArray[$x]['attendance_date']     = date("d M ",strtotime($sel_date));
								$newResultsArray[$x]['tbl_student_id']      = '';
								$newResultsArray[$x]['tbl_class_id']        = '';
								$newResultsArray[$x]['tbl_teacher_id']      = '';
								$newResultsArray[$x]['absent_count']        = '1000';
								$newResultsArray[$x]['present_count']       = '1000';
								$newResultsArray[$x]['total_count']         = '1000';
								$x =$x+1;
								$prevDate     =  $sel_date;
							}
							
							$startdateStr     = strtotime("-1 day", strtotime($sel_date));
							$sel_date         = date("Y-m-d",$startdateStr); 
							
					   }  
			   
		   }
		   else if($range=="week")
		   {
					   $sel_date = date("Y-m-d");
					   $prevDate = "";
					   $x = 0;
					  
					   for($c=0;$c<7;$c++)
					   {
							for($t=0;$t<count($results);$t++)
							{   
								$dateArray =  explode(" ",$results[$t]['attendance_date']);
								
								$b=0;
								if($sel_date==$dateArray[0])
								{
									if($prevDate <> $sel_date)
									{
										$attendance_date_array = explode(" ",$results[$t]['attendance_date']);
										
										$newResultsArray[$x]['attendance_month']    = $results[$t]['attendance_month'];
										$newResultsArray[$x]['tbl_student_id']      = $results[$t]['tbl_student_id'];
										$newResultsArray[$x]['tbl_class_id']        = $results[$t]['tbl_class_id'];
										$newResultsArray[$x]['tbl_teacher_id']      = $results[$t]['tbl_teacher_id'];
										$newResultsArray[$x]['absent_count']        = $results[$t]['absent_count'];
										$newResultsArray[$x]['present_count']       = $results[$t]['present_count'];
										$newResultsArray[$x]['total_count']         = $results[$t]['total_count'];
										$newResultsArray[$x]['attendance_date']     = date("d M ",strtotime($attendance_date_array[0]));
										$x =$x+1;
										$t = count($results);
										$prevDate  =  $results[$t]['attendance_date'];
										$b = 1;
									}
								}
							}
							
							if($b==0)
							{
								$newResultsArray[$x]['attendance_month']    = '';
								$newResultsArray[$x]['attendance_date']     = date("d M ",strtotime($sel_date)); 
								$newResultsArray[$x]['tbl_student_id']      = '';
								$newResultsArray[$x]['tbl_class_id']        = '';
								$newResultsArray[$x]['tbl_teacher_id']      = '';
								$newResultsArray[$x]['absent_count']        = '1000';
								$newResultsArray[$x]['present_count']       = '1000';
								$newResultsArray[$x]['total_count']         = '1000';
								$x =$x+1;
								$prevDate     =  $sel_date;
							}
							
							$startdateStr     = strtotime("-1 day", strtotime($sel_date));
							$sel_date         = date("Y-m-d",$startdateStr); 
					   }  
		   }

		return $newResultsArray;	
	}
	
	
	
	/*function get_overview_student_points_detail($tbl_student_id,$tbl_class_id,$tbl_semester_id,$tbl_school_id) {
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		$qry = "SELECT SUM(points_student) as student_point, comments_student FROM ".TBL_POINTS." WHERE 1 ";
		$qry .=  "  AND tbl_student_id='$tbl_student_id' ";
		if($tbl_class_id<>"") {
			$qry .=  "  AND tbl_class_id='$tbl_class_id' ";
		}
		if($tbl_semester_id<>"") {
			$qry .=  "  AND tbl_semester_id='$tbl_semester_id' ";
		}
		if($tbl_school_id<>"") {
			$qry .=  "  AND tbl_school_id='$tbl_school_id' ";
		}
		
		$qry .=  " AND is_active='Y' GROUP BY comments_student ORDER BY added_date DESC ";
		$rs = $this->cf->selectMultiRecords($qry);
	return $rs;
	}
	*/
	function get_student_points_graph($tbl_student_id,$tbl_class_id,$tbl_semester_id,$tbl_school_id,$is_active='',$from_date='',$to_date='',$range='') {
		 
		    $qryPoints = "SELECT  P.comments_student,P.tbl_point_category_id, BP.point_name_en, BP.point_name_ar FROM ".TBL_POINTS." AS P LEFT JOIN ".TBL_BEHAVIOUR_POINTS." AS BP  
			             ON BP.tbl_point_category_id = P.tbl_point_category_id WHERE 1 ";
			
			if($tbl_student_id<>"")                                                 
			{
				$qryPoints .= " AND P.tbl_student_id = '".$tbl_student_id."' "; 
			}
			if($tbl_class_id<>"")
			{
				$qryPoints .= " AND P.tbl_class_id = '".$tbl_class_id."' "; 
			}
			if($tbl_school_id<>"")
			{
				$qryPoints .= " AND P.tbl_school_id = '".$tbl_school_id."' "; 
			}
		
			if($from_date<>"" && $from_date<>'""' )
			{
				$from_date       = $from_date." 00:00:00";
				$to_date         = $to_date." 23:59:60";
				$qryPoints .= " AND ( P.added_date >=  '".$from_date."' AND  P.added_date <='".$to_date."' ) ";
			}
			
			//Extra added
			$qryPoints .= " AND P.tbl_point_category_id <> '' "; 
			//End extra added
			
		   $qryPoints .= " AND P.is_active='Y'  "; 
		   $qryPoints .= " GROUP BY P.tbl_point_category_id ";
		   $qryPoints .= " ORDER BY P.added_date DESC, P.is_active ASC ";
		   $resultsPoints         = $this->cf->selectMultiRecords($qryPoints);
		   
		  $m = 0;
		  for($d=0;$d<count($resultsPoints);$d++)
		  {
			 
			   $tbl_point_category_id     = $resultsPoints[$d]['tbl_point_category_id'];
		 
			   $qry = "SELECT  P.added_date,  SUM(P.points_student) as student_point, P.comments_student
					, MONTHNAME(P.added_date) AS point_month FROM ".TBL_POINTS." AS P WHERE 1 ";
				
				if($tbl_student_id<>"")                                                 
				{
					$qry .= " AND P.tbl_student_id = '".$tbl_student_id."' "; 
				}
				if($tbl_class_id<>"")
				{
					$qry .= " AND P.tbl_class_id = '".$tbl_class_id."' "; 
				}
				if($tbl_school_id<>"")
				{
					$qry .= " AND P.tbl_school_id = '".$tbl_school_id."' "; 
				}
				if($tbl_point_category_id<>"")
				{
					$qry .= " AND P.tbl_point_category_id = '".$tbl_point_category_id."' "; 
				}
			
				if($from_date<>"" && $from_date<>'""' )
				{
					$from_date       = $from_date." 00:00:00";
					$to_date         = $to_date." 23:59:60";
					$qry .= " AND ( P.added_date >=  '".$from_date."' AND  P.added_date <='".$to_date."' ) ";
				}
				
			   $qry .= " AND P.is_active='Y'  "; 
			   $qry .= " GROUP BY P.added_date ";
			   $qry .= " ORDER BY P.added_date DESC, P.is_active ASC ";
			   $results         = $this->cf->selectMultiRecords($qry);
		       $newResultsArray = array(); 
		  
		   if($range=="year")
		   {
					   $sel_month = date("F");
					   $prevMonth = "";
					   $x = 0;
					   for($c=0;$c<12;$c++)
					   { 
					        $totPoint = 0;
							$b=0;
							for($t=0;$t<count($results);$t++)
							{   
								if($sel_month==$results[$t]['point_month'])
								{
								   
										$totPoint = $totPoint  + $results[$t]['student_point'];
										
										$added_date_array = explode(" ",$results[$t]['added_date']);
										
										$newResultsArray[$c]['point_month']         = $results[$t]['point_month'];
										$newResultsArray[$c]['comments_student']    = $results[$t]['comments_student'];
										$newResultsArray[$c]['total_point']         = $totPoint;
										$newResultsArray[$c]['added_date']     		= $added_date_array[0];
										$b = 1;
								}
							}
							
							if($b==0)
							{
								$newResultsArray[$c]['point_month']         = $sel_month;
								$newResultsArray[$c]['comments_student']    = $results[$t]['comments_student'];
								$newResultsArray[$c]['total_point']         = '1000';
								$newResultsArray[$c]['added_date']     		= $added_date_array[0];
								
							}
							
							$sel_month = date("F", strtotime( date( 'Y-m-01' )." -$c months"));
					   }
		   } 
		   else if($range=="month")
		   {
					   $sel_date = date("Y-m");
					   $sel_date = $sel_date."-31";
					   $prevDate = "";
					   $x = 0;
					  
					   for($c=0;$c<31;$c++)
					   {
							for($t=0;$t<count($results);$t++)
							{   
								$dateArray =  explode(" ",$results[$t]['point_month']);
								
								$b=0;
								if($sel_date==$dateArray[0])
								{
									if($prevDate <> $sel_date)
									{
										$added_date_array = explode(" ",$results[$t]['added_date']);
										
										$totPoint = $totPoint  + $results[$t]['student_point'];
										
										$added_date_array = explode(" ",$results[$t]['added_date']);
										
										$newResultsArray[$c]['point_month']         = $results[$t]['point_month'];
										$newResultsArray[$c]['comments_student']    = $results[$t]['comments_student'];
										$newResultsArray[$c]['total_point']         = $totPoint;
										$newResultsArray[$x]['added_date']       = date("d M ",strtotime($added_date_array[0]));
										$x =$x+1;
										$t = count($results);
										$prevDate  =  $results[$t]['added_date'];
										$b = 1;
									}
								}
							}
							
							if($b==0)
							{
								$newResultsArray[$x]['point_month']         = $sel_month;
								$newResultsArray[$c]['comments_student']    = $results[$t]['comments_student'];
								$newResultsArray[$c]['total_point']         = '1000';
								$newResultsArray[$x]['added_date']          = date("d M ",strtotime($sel_date));
								$x =$x+1;
								$prevDate     =  $sel_date;
							}
							
							$startdateStr     = strtotime("-1 day", strtotime($sel_date));
							$sel_date         = date("Y-m-d",$startdateStr); 
							
					   }  
			   
		   }
		   else if($range=="week")
		   {
					   $sel_date = date("Y-m-d");
					   $prevDate = "";
					   $x = 0;
					  
					   for($c=0;$c<7;$c++)
					   {
							for($t=0;$t<count($results);$t++)
							{   
								$dateArray =  explode(" ",$results[$t]['added_date']);
								
								$b=0;
								if($sel_date==$dateArray[0])
								{
									if($prevDate <> $sel_date)
									{
										$added_date_array = explode(" ",$results[$t]['added_date']);
										
										$newResultsArray[$c]['point_month']         = $results[$t]['point_month'];
										$newResultsArray[$c]['comments_student']    = $results[$t]['comments_student'];
										$newResultsArray[$c]['total_point']         = $totPoint;
										$newResultsArray[$x]['added_date']     = date("d M ",strtotime($added_date_array[0]));
										$x =$x+1;
										$t = count($results);
										$prevDate  =  $results[$t]['added_date'];
										$b = 1;
									}
								}
							}
							
							if($b==0)
							{
								$newResultsArray[$c]['point_month']         = $sel_month;
								$newResultsArray[$c]['added_date']         =  date("d M ",strtotime($sel_date)); 
								$newResultsArray[$c]['comments_student']    = $results[$t]['comments_student'];
								$newResultsArray[$c]['total_point']         = '1000';
								
								$x =$x+1;
								$prevDate     =  $sel_date;
							}
							
							$startdateStr     = strtotime("-1 day", strtotime($sel_date));
							$sel_date         = date("Y-m-d",$startdateStr); 
					   }  
		     }
			    $resultsPoints[$d]['points_data']  = $newResultsArray;
	   	}   
		return  $resultsPoints ;
	}
	
	// Get Progress Reports List
	function get_progress_report_list($tbl_school_id, $tbl_teacher_id) {
		
		$qry = "SELECT * FROM ".TBL_PROGRESS_REPORTS." AS PR WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND PR.tbl_school_id= '".$tbl_school_id."' ";
		}
		if($tbl_teacher_id<>"")
		{
			$qry .= " AND PR.added_by = '".$tbl_teacher_id."' ";
		}
		//Active/Deactive
		$qry .= " AND PR.is_active='Y' ";
		$qry .= " ORDER BY PR.id DESC";
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
		
    function save_student_progress_report($tbl_progress_id, $tbl_progress_report_id, $report_name, $report_name_ar, $id_data, $sub_mark_data,$tot_mark_data, $tbl_student_id, $tbl_class_id, $tbl_teacher_id, $tbl_school_id)
    {
		$tbl_progress_id       			            = $this->cf->get_data($tbl_progress_id);
		$tbl_progress_report_id                     = $this->cf->get_data($tbl_progress_report_id);
		$report_name		      			        = trim($this->cf->get_data($report_name));
		$report_name_ar       			            = trim($this->cf->get_data($report_name_ar));
		$id_data                                    = $this->cf->get_data($id_data);
		$sub_mark_data		      			        = $this->cf->get_data($sub_mark_data);
		$tot_mark_data       			            = $this->cf->get_data($tot_mark_data);
		$tbl_student_id            			        = $this->cf->get_data($tbl_student_id);
		$tbl_class_id 		      			        = $this->cf->get_data($tbl_class_id);
		$tbl_teacher_id            			        = $this->cf->get_data($tbl_teacher_id);
		$tbl_school_id            			        = $this->cf->get_data($tbl_school_id);
		
		if($report_name <>"")
		{
			$qry = "SELECT * FROM ".TBL_PROGRESS_REPORTS." AS PR WHERE 1 ";
			if($report_name<>"")
			{
				$qry .= " AND PR.report_name= '".$report_name."' ";
			}
			if($tbl_school_id<>"")
			{
				$qry .= " AND PR.tbl_school_id= '".$tbl_school_id."' ";
			}
			$results = $this->cf->selectMultiRecords($qry);
			if(empty($results))
			{
				$qry_ins = "INSERT INTO ".TBL_PROGRESS_REPORTS." (`tbl_progress_report_id`, `report_name`, `report_name_ar`, `tbl_school_id`)
						VALUES ('$tbl_progress_report_id', '$report_name', '$report_name_ar', '$tbl_school_id')";
				$this->cf->insertInto($qry_ins);
			}else{
				$tbl_progress_report_id = $results[0]['tbl_progress_report_id'];
			}
		}
		
		$subject_array 			= explode("||", $id_data);
		$sub_mark_data_array 	= explode("||", $sub_mark_data);
		$tot_mark_data_array    = explode("||", $tot_mark_data);	
			
	    
		if(!empty($subject_array))
		{
			
			$qrySt = "SELECT * FROM ".TBL_PROGRESS_REPORT_STUDENT." AS PRS WHERE 1 ";
			if($tbl_progress_report_id<>"")
			{
				$qrySt .= " AND PRS.tbl_progress_report_id= '".$tbl_progress_report_id."' ";
			}
			if($tbl_school_id<>"")
			{
				$qrySt .= " AND PRS.tbl_school_id= '".$tbl_school_id."' ";
			}
			if($tbl_class_id<>"")
			{
				$qrySt .= " AND PRS.tbl_class_id= '".$tbl_class_id."' ";
			}
			if($tbl_student_id<>"")
			{
				$qrySt .= " AND PRS.tbl_student_id= '".$tbl_student_id."' ";
			}
			$resultsSt = $this->cf->selectMultiRecords($qrySt);
			if(empty($resultsSt))
			{
				for($y=0;$y<count($subject_array);$y++)
				{ 	
					if($subject_array[$y]<>"")
					{
						$tbl_subject_id = $subject_array[$y];
						$mark           = $sub_mark_data_array[$y];
						$total_mark     = $tot_mark_data_array[$y];
						$qry_ins = "INSERT INTO ".TBL_PROGRESS_REPORT_STUDENT." (`tbl_progress_id`, `tbl_progress_report_id`, `tbl_student_id`, `tbl_class_id`, `tbl_teacher_id`, `tbl_subject_id`, `mark`, `total_mark`, `reported_date`, `tbl_school_id`)
								VALUES ('$tbl_progress_id', '$tbl_progress_report_id', '$tbl_student_id', '$tbl_class_id', '$tbl_teacher_id', '$tbl_subject_id', '$mark', '$total_mark', NOW(), '$tbl_school_id')";
						$this->cf->insertInto($qry_ins);
					}
				}
			}else{
				return "E";
			}
		}
		return "Y";
	} 
	
	// End Save Progress Report
	
	/**
	* @desc		Get all progress reports against student with class
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function get_all_progress_reports_student($sort_name, $sort_by, $offset, $q, $is_active, $tbl_class_id, $tbl_student_id, $tbl_school_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = $this->cf->get_data($q);
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
			$qry = "SELECT * FROM ".TBL_PROGRESS_REPORT_STUDENT." AS PRS INNER JOIN  ".TBL_PROGRESS_REPORTS." AS PR ON  
			        PR.tbl_progress_report_id=PRS.tbl_progress_report_id   WHERE 1 ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PRS.is_active='$is_active' ";
		}
		$qry .= " AND PRS.is_active<>'D' "; 
		//Search
		if (trim($q) != "") {
			$qry .= " AND (PR.report_name LIKE '%$q%' || PR.report_name_ar LIKE '%$q%')";
		} 
		
		if (trim($tbl_class_id) != "") {
			$qry .= " AND PRS.tbl_class_id = '".$tbl_class_id."' ";
		} 
		
		if (trim($tbl_student_id) != "") {
			$qry .= " AND PRS.tbl_student_id = '".$tbl_student_id."' ";
		} 
		
		$qry .= " AND PRS.tbl_school_id = '".$tbl_school_id."' ";
		
		$qry .= " GROUP BY PR.tbl_progress_report_id ";
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY PR.$sort_name $sort_by";
		} else {
			$qry .= " ORDER BY PR.id DESC ";
		}
		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_SCHOOL_ROLES_PAGING;
		}
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}

	
	/**
	* @desc		Get total progress reports for a student
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_progress_reports_student($q, $is_active, $tbl_class_id, $tbl_student_id, $tbl_school_id) {
		$q         = $this->cf->get_data($q);
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT COUNT(DISTINCT(PRS.tbl_progress_report_id)) as total_records FROM ".TBL_PROGRESS_REPORT_STUDENT." AS PRS INNER JOIN  ".TBL_PROGRESS_REPORTS." AS PR ON 
		        PR.tbl_progress_report_id=PRS.tbl_progress_report_id   WHERE 1 ";
	//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PRS.is_active='$is_active' ";
		}
		$qry .= " AND PRS.is_active<>'D' "; 
		//Search
		if (trim($q) != "") {
			$qry .= " AND (PR.report_name LIKE '%$q%' || PR.report_name_ar LIKE '%$q%')";
		} 
		
		if (trim($tbl_class_id) != "") {
			$qry .= " AND PRS.tbl_class_id = '".$tbl_class_id."' ";
		} 
		
		if (trim($tbl_student_id) != "") {
			$qry .= " AND PRS.tbl_student_id = '".$tbl_student_id."' ";
		} 
		
		$qry .= " AND PRS.tbl_school_id = '".$tbl_school_id."' ";
		$results = $this->cf->selectMultiRecords($qry);
		
	return $results[0]['total_records'];
	}
	
	
	
	function get_progress_report_details($tbl_progress_report_id, $tbl_progress_id,  $tbl_class_id, $tbl_student_id, $tbl_school_id)
	{
		$qry = "SELECT * FROM ".TBL_PROGRESS_REPORT_STUDENT." AS PRS 
				INNER JOIN  ".TBL_SUBJECT." AS S ON S.tbl_subject_id=PRS.tbl_subject_id 
				INNER JOIN  ".TBL_PROGRESS_REPORTS." AS PR ON 
		        PR.tbl_progress_report_id=PRS.tbl_progress_report_id
			    WHERE 1 ";
	
		if (trim($tbl_progress_report_id) != "") {
			$qry .= " AND PRS.tbl_progress_report_id = '".$tbl_progress_report_id."' ";
		} 
		
		if (trim($tbl_class_id) != "") {
			$qry .= " AND PRS.tbl_class_id = '".$tbl_class_id."' ";
		} 
		
		if (trim($tbl_student_id) != "") {
			$qry .= " AND PRS.tbl_student_id = '".$tbl_student_id."' ";
		} 
		
		$qry .= " AND PRS.tbl_school_id = '".$tbl_school_id."' ORDER BY PRS.id ASC ";
		$res  = $this->cf->selectMultiRecords($qry);
		return $res;
	}
	
	
	function update_student_progress_report($tbl_progress_id, $tbl_progress_report_id, $report_name, $report_name_ar, $id_data, $sub_mark_data,$tot_mark_data, $tbl_student_id, $tbl_class_id, $tbl_teacher_id, $tbl_school_id)
    {
		$tbl_progress_id       			            = $this->cf->get_data($tbl_progress_id);
		$tbl_progress_report_id                     = $this->cf->get_data($tbl_progress_report_id);
		$report_name		      			        = trim($this->cf->get_data($report_name));
		$report_name_ar       			            = trim($this->cf->get_data($report_name_ar));
		$id_data                                    = $this->cf->get_data($id_data);
		$sub_mark_data		      			        = $this->cf->get_data($sub_mark_data);
		$tot_mark_data       			            = $this->cf->get_data($tot_mark_data);
		$tbl_student_id            			        = $this->cf->get_data($tbl_student_id);
		$tbl_class_id 		      			        = $this->cf->get_data($tbl_class_id);
		$tbl_teacher_id            			        = $this->cf->get_data($tbl_teacher_id);
		$tbl_school_id            			        = $this->cf->get_data($tbl_school_id);
		
		$subject_array 			= explode("||", $id_data);
		$sub_mark_data_array 	= explode("||", $sub_mark_data);
		$tot_mark_data_array    = explode("||", $tot_mark_data);	
			
	    
		if(!empty($subject_array))
		{
			
			$qrySt = "SELECT * FROM ".TBL_PROGRESS_REPORT_STUDENT." AS PRS WHERE 1 ";
			if($tbl_progress_report_id<>"")
			{
				$qrySt .= " AND PRS.tbl_progress_report_id= '".$tbl_progress_report_id."' ";
			}
			if($tbl_school_id<>"")
			{
				$qrySt .= " AND PRS.tbl_school_id= '".$tbl_school_id."' ";
			}
			if($tbl_class_id<>"")
			{
				$qrySt .= " AND PRS.tbl_class_id= '".$tbl_class_id."' ";
			}
			if($tbl_student_id<>"")
			{
				$qrySt .= " AND PRS.tbl_student_id= '".$tbl_student_id."' ";
			}
			$resultsSt = $this->cf->selectMultiRecords($qrySt);
			if(empty($resultsSt))
			{
				for($y=0;$y<count($subject_array);$y++)
				{ 	
					if($subject_array[$y]<>"")
					{
						$tbl_subject_id = $subject_array[$y];
						$mark           = $sub_mark_data_array[$y];
						$total_mark     = $tot_mark_data_array[$y];
						$qry_ins = "INSERT INTO ".TBL_PROGRESS_REPORT_STUDENT." (`tbl_progress_id`, `tbl_progress_report_id`, `tbl_student_id`, `tbl_class_id`, `tbl_teacher_id`, `tbl_subject_id`, `mark`, `total_mark`, `reported_date`, `tbl_school_id`)
								VALUES ('$tbl_progress_id', '$tbl_progress_report_id', '$tbl_student_id', '$tbl_class_id', '$tbl_teacher_id', '$tbl_subject_id', '$mark', '$total_mark', NOW(), '$tbl_school_id')";
						$this->cf->insertInto($qry_ins);
					}
				}
			}else{
				$qry = "DELETE FROM ".TBL_PROGRESS_REPORT_STUDENT." WHERE tbl_progress_report_id='".$tbl_progress_report_id."' 
				        AND tbl_student_id='".$tbl_student_id."' AND tbl_class_id='".$tbl_class_id."' AND tbl_school_id='".$tbl_school_id."' ";
				$this->cf->deleteFrom($qry);
				
				for($y=0;$y<count($subject_array);$y++)
				{ 	
					if($subject_array[$y]<>"")
					{
						$tbl_subject_id = $subject_array[$y];
						$mark           = $sub_mark_data_array[$y];
						$total_mark     = $tot_mark_data_array[$y];
						$qry_ins = "INSERT INTO ".TBL_PROGRESS_REPORT_STUDENT." (`tbl_progress_id`, `tbl_progress_report_id`, `tbl_student_id`, `tbl_class_id`, `tbl_teacher_id`, `tbl_subject_id`, `mark`, `total_mark`, `reported_date`, `tbl_school_id`)
								VALUES ('$tbl_progress_id', '$tbl_progress_report_id', '$tbl_student_id', '$tbl_class_id', '$tbl_teacher_id', '$tbl_subject_id', '$mark', '$total_mark', NOW(), '$tbl_school_id')";
						$this->cf->insertInto($qry_ins);
					}
				}
				//return "E";
			}
		}
		return "Y";
	} 
	
	
	
	
	
	

}

?>