<?php
include_once('include/common_functions.php');

/**
 * @desc   	  	Bus Model
 *
 * @category   	Model
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Model_bus extends CI_Model {
	var $cf;


	/**
	* @desc Default constructor for the Controller
	*
	* @access default
	*/
    function model_bus() {
		$this->cf = new Common_functions();
    }


	/**
	* @desc		Get students against class
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function authenticate_bus($bus_code, $bus_password) {
		//$qry_sel = "SELECT * FROM ".TBL_BUS." WHERE bus_code='$bus_code' AND bus_password='$bus_password'";
		$qry = "SELECT bus_password, salt FROM ".TBL_BUS." WHERE bus_code='$bus_code' AND is_active='Y'";
		//echo $qry;
		$result = $this->cf->selectMultiRecords($qry);
		
		if ($result) {
			$salt = $result[0]['salt'];
			$db_password = $result[0]['bus_password'];
		} else {
			return "D";//User does not exists
		}
		
		$hash = sha1($salt . sha1($bus_password));
								
		if (trim($hash) != trim($db_password)) {                
			return "N";
		} else {     
			return "Y";
		}
	return $rs;	
	}
	
	
	/**	
	* @desc Function to get bus id
	*
	* @access default
	*/
    function get_bus_id($bus_code) {
		$qry = "SELECT tbl_bus_id FROM ".TBL_BUS." WHERE bus_code='$bus_code' AND is_active='Y'";
		//echo $qry;
		$result = $this->cf->selectMultiRecords($qry);
		return $result[0]["tbl_bus_id"];
    }
	
	
	/**	
	* @desc Function to get bus id
	*
	* @access default
	*/
    function get_tbl_school_id($bus_code) {
		$qry = "SELECT tbl_school_id FROM ".TBL_BUS." WHERE bus_code='$bus_code'";
		//echo $qry;
		$result = $this->cf->selectMultiRecords($qry);
		return $result[0]["tbl_school_id"];
    }
	
	
	/**	
	* @desc Function to start bus tracking session
	*
	* @access default
	*/
    function start_bus_tracking_session($tbl_bus_id, $device_uid, $device, $tbl_school_id) {
		
		// Check if tbl_bus_id is already there in bus tracking session table.
		$qry = "SELECT id FROM ".TBL_BUS_TRACKING_SESSION." WHERE tbl_bus_id='$tbl_bus_id'";
		$data_rs = $this->cf->selectMultiRecords($qry);
		//echo $qry;
		
		if ($data_rs[0]["id"] != "") {
			// Delete previous record if already there in session
			$qry = "DELETE FROM ".TBL_BUS_TRACKING_SESSION." WHERE tbl_bus_id='$tbl_bus_id'";
			$this->cf->deleteFrom($qry);
		}
		
		$tbl_bus_tracking_session_id = substr(md5(uniqid(rand())),0,10);
		$qry = "INSERT INTO ".TBL_BUS_TRACKING_SESSION." (
			`tbl_bus_tracking_session_id` ,
			`tbl_bus_id` ,
			`device_uid` ,
			`device` ,
			`is_active` ,
			`added_date` ,
			`tbl_school_id`
			)
			VALUES (
			'$tbl_bus_tracking_session_id', '$tbl_bus_id', '$device_uid', '$device', 'Y', NOW(), '$tbl_school_id'
			)";
		//echo $qry;
		$this->cf->insertInto($qry);
    }
	
	
	/**	
	* @desc Function to check bus and device uid session from
	*
	* @access default
	*/
    function check_bus_device_session($tbl_bus_id, $device_uid) {
		$qry = "SELECT id FROM ".TBL_BUS_TRACKING_SESSION." WHERE tbl_bus_id='".$tbl_bus_id."' AND device_uid='".$device_uid."'";
		$data_rs = $this->cf->selectMultiRecords($qry);
		if ($data_rs[0]["id"] == "") {
			return "N";
		} else {
			return "Y";
		}
	}
	
	
	/**	
	* @desc Function to stop bus track logging
	*
	* @access default
	*/
	function stop_bus_tracking($tbl_bus_id, $device_uid) {
		$qry = "DELETE FROM ".TBL_BUS_TRACKING_SESSION." WHERE tbl_bus_id='".$tbl_bus_id."' AND device_uid='".$device_uid."'";
		$this->cf->deleteFrom($qry);
		//echo $qry;
	}
	
	
	/**	
	* @desc Function to delte previous bus track session
	*
	* @access default
	*/
	function set_bus_tracking_session($tbl_bus_id, $device_uid, $device, $tbl_school_id) {
		$qry = "DELETE FROM ".TBL_BUS_TRACKING_SESSION." WHERE tbl_bus_id='".$tbl_bus_id."'";
		$this->cf->deleteFrom($qry);
		//echo $qry;
		
		$tbl_bus_tracking_session_id = substr(md5(uniqid(rand())),0,10);
		$qry = "INSERT INTO ".TBL_BUS_TRACKING_SESSION." (
			`tbl_bus_tracking_session_id` ,
			`tbl_bus_id` ,
			`device_uid` ,
			`device` ,
			`is_active` ,
			`added_date` ,
			`tbl_school_id`
			)
			VALUES (
			'$tbl_bus_tracking_session_id', '$tbl_bus_id', '$device_uid', '$device', 'Y', NOW(), '$tbl_school_id'
			)";
		//echo $qry;
		$this->cf->insertInto($qry);
	}
	
	
	/**	
	* @desc Function to log bus tracking
	*
	* @access default
	*/
	function save_bus_lat_long($tbl_bus_id, $latitude, $longitude, $device_uid, $tbl_school_id) {
		$tbl_bus_tracking_id = substr(md5(uniqid(rand())),0,10);
		$qry = "INSERT INTO ".TBL_BUS_TRACKING." (
			`tbl_bus_tracking_id` ,
			`tbl_bus_id` ,
			`latitude` ,
			`longitude` ,
			`device_uid` ,
			`is_active` ,
			`added_date` ,
			`tbl_school_id`
			)
			VALUES (
			'$tbl_bus_tracking_id', '$tbl_bus_id', '$latitude', '$longitude', '$device_uid', 'Y', NOW(), '$tbl_school_id'
			)";
		//echo $qry;
		$this->cf->insertInto($qry);
	}
	
	
	/**	
	* @desc Function to log out bus user
	*
	* @access default
	*/
	function bus_log_out($tbl_bus_id) {
		$qry = "DELETE FROM ".TBL_BUS_TRACKING_SESSION." WHERE tbl_bus_id='".$tbl_bus_id."'";
		$this->cf->deleteFrom($qry);
	}
	

	/**	
	* @desc Function to get bus number
	*
	* @access default
	*/
	function get_bus_number($tbl_bus_id) {
		$qry = "SELECT bus_number FROM ".TBL_BUS." WHERE tbl_bus_id='".$tbl_bus_id."' AND is_active='Y'";
		//echo $qry;
		$data_rs = $this->cf->selectMultiRecords($qry);
		return $data_rs[0]["bus_number"];
	}
	
	
	/**	
	* @desc Function to get student bus tracking
	*
	* @access default
	*/
	function get_student_bus_tracking_live($tbl_bus_id) {
		$qry = "SELECT latitude, longitude FROM ".TBL_BUS_TRACKING." WHERE tbl_bus_id='".$tbl_bus_id."' AND is_active='Y' ORDER BY added_date DESC LIMIT 0, 1";
		//echo $qry;
		$data_rs = $this->cf->selectMultiRecords($qry);
		return $data_rs;
	}
	
	
	/**	
	* @desc Function to get student bus tracking
	*
	* @access default
	*/
	function get_school_of_bus($tbl_bus_id) {
		$qry = "SELECT tbl_school_id FROM ".TBL_BUS." WHERE tbl_bus_id='".$tbl_bus_id."'";
		//echo $qry;
		$data_rs = $this->cf->selectMultiRecords($qry);
		return $data_rs[0]["tbl_school_id"];
	}
	
	
	
	/**	
	* @desc Function to get bus details
	*
	* @access default
	*/
	function get_bus_details($tbl_bus_id) {
		$qry = "SELECT * FROM ".TBL_BUS." WHERE tbl_bus_id='".$tbl_bus_id."'";
		//echo $qry;
		$data_rs = $this->cf->selectMultiRecords($qry);
		return $data_rs;
	}
	
}
?>

