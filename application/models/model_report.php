<?php
include_once('include/common_functions.php');
/**
 * @desc   	  	Report Model
 * @category   	Model
 * @author     	Shanavas.PK
 * @version    	0.1
 */

class Model_report extends CI_Model {
	var $cf;
	/**
	* @desc Default constructor for the Controller
	* @access default
	*/

    function model_report() {
		$this->cf = new Common_functions();
    }


    // Summary of School Cards Report
	function school_card_reports($sort_name, $sort_by, $offset, $q="", $tbl_category_id="", $is_active='', $tbl_school_id, $tbl_gender, $tbl_country_id, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id, $card_type)
	{
		
		$qry_sub = "";
		if($tbl_gender<>"")
		{
			$qry_sub .= " AND gender='".$tbl_gender."' ";
		}
		
		if($tbl_country_id<>"")
		{
		    $qry_sub .= " AND country='".$tbl_country_id."' ";
		}
		
		if($tbl_school_id<>"")
		{
		    $qry_sub .= " AND tbl_school_id='".$tbl_school_id."' ";
		}
		
		if($tbl_class_id<>"")
		{
		    $qry_sub .= " AND tbl_class_id='".$tbl_class_id."' ";
		}
		
		if($tbl_student_id<>"")
		{
		    $qry_sub .= " AND tbl_student_id='".$tbl_student_id."' ";
		}
		
		
		$qry = " SELECT SUM(A.card_point) AS total, A.card_type AS card_type, B.category_name_en, B.category_name_ar FROM ".TBL_CARDS."  as A LEFT JOIN ".TBL_CARD_POINTS." as B ON A.card_type = B.tbl_card_category_id 
		         LEFT JOIN ".TBL_SEMESTER." as C ON C.tbl_semester_id = A.tbl_semester_id  ";
		$qry .= " WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND A.tbl_school_id='".$tbl_school_id."' ";
		}
		if($tbl_student_id<>"")
		{
			$qry .= " AND A.tbl_student_id='".$tbl_student_id."' ";
		}
		if($tbl_semester_id<>"")
		{
		    $qry .= " AND A.tbl_semester_id='".$tbl_semester_id."' ";
		}
		if($tbl_academic_year<>"")
		{
		    $qry .= " AND C.tbl_academic_year_id='".$tbl_academic_year."' ";
		}
		if($tbl_teacher_id<>"")
		{
		    $qry .= " AND A.tbl_teacher_id='".$tbl_teacher_id."' ";
		}
		if($tbl_class_id<>"")
		{
		    $qry .= " AND A.tbl_class_id='".$tbl_class_id."' ";
		}
		if($card_type<>"")
		{
		    $qry .= " AND B.tbl_card_category_id='".$card_type."' ";
		}
		
		$qry .= " AND A.tbl_student_id IN (SELECT tbl_student_id FROM ".TBL_STUDENT." WHERE 1 ".$qry_sub." ) ";
		
		$qry .= " GROUP BY A.card_type ORDER BY total ASC ";
        $qry .= "LIMIT $offset , 50 ";
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
	}
	
	
	function total_school_card_reports($q="", $tbl_category_id="", $is_active='', $tbl_school_id, $gender, $country, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id, $card_type)
	{
		
		$qry_sub = "";
		if($gender<>"")
		{
			$qry_sub .= " AND gender='".$gender."' ";
		}
		
		if($country<>"")
		{
		    $qry_sub .= " AND country='".$country."' ";
		}
		
		if($tbl_school_id<>"")
		{
		    $qry_sub .= " AND tbl_school_id='".$tbl_school_id."' ";
		}
		
		if($tbl_class_id<>"")
		{
		    $qry_sub .= " AND tbl_class_id='".$tbl_class_id."' ";
		}
		
		if($tbl_student_id<>"")
		{
		    $qry_sub .= " AND tbl_student_id='".$tbl_student_id."' ";
		}
		
		
		$qry = " SELECT SUM(A.card_point) AS total, A.card_type AS card_type FROM ".TBL_CARDS."  as A LEFT JOIN ".TBL_CARD_POINTS." as B ON A.card_type = B.tbl_card_category_id 
		         LEFT JOIN ".TBL_SEMESTER." as C ON C.tbl_semester_id = A.tbl_semester_id  ";
		$qry .= " WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND A.tbl_school_id='".$tbl_school_id."' ";
		}
		if($tbl_student_id<>"")
		{
			$qry .= " AND A.tbl_student_id='".$tbl_student_id."' ";
		}
		if($tbl_semester_id<>"")
		{
		    $qry .= " AND A.tbl_semester_id='".$tbl_semester_id."' ";
		}
		if($tbl_academic_year<>"")
		{
		    $qry .= " AND C.tbl_academic_year_id='".$tbl_academic_year."' ";
		}
		if($tbl_teacher_id<>"")
		{
		    $qry .= " AND A.tbl_teacher_id='".$tbl_teacher_id."' ";
		}
		if($tbl_class_id<>"")
		{
		    $qry .= " AND A.tbl_class_id='".$tbl_class_id."' ";
		}
		if($card_type<>"")
		{
		    $qry .= " AND B.tbl_card_category_id='".$card_type."' ";
		}
		$qry .= " AND A.tbl_student_id IN (SELECT tbl_student_id FROM ".TBL_STUDENT." WHERE 1 ".$qry_sub." ) ";
		
		$qry .= " GROUP BY A.card_type ";
		
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
	}
	// End School Summary Cards Report
	
	
	// Start School Cards Report Detailed
	function school_card_reports_detailed($sort_name, $sort_by, $offset, $q="", $tbl_category_id="", $is_active='', $tbl_school_id, $tbl_gender, $tbl_country_id, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id)
	{
		
		$qry_sub = "";
		/*if($tbl_gender<>"")
		{
			$qry_sub .= " AND gender='".$tbl_gender."' ";
		}*/
		
		if($tbl_country_id<>"")
		{
		    $qry_sub .= " AND country='".$tbl_country_id."' ";
		}
		
		if($tbl_school_id<>"")
		{
		    $qry_sub .= " AND tbl_school_id='".$tbl_school_id."' ";
		}
		
		if($tbl_class_id<>"")
		{
		    $qry_sub .= " AND tbl_class_id='".$tbl_class_id."' ";
		}
		
		if($tbl_student_id<>"")
		{
		    $qry_sub .= " AND tbl_student_id='".$tbl_student_id."' ";
		}
		
		
		$qry = " SELECT A.card_point, A.card_type AS card_type, A.card_issue_type, A.added_date,  B.category_name_en, B.category_name_ar
		         , ST.first_name,ST.last_name,ST.first_name_ar,ST.last_name_ar
				 , T.first_name as teacher_first_name,T.last_name as teacher_last_name,T.first_name_ar as teacher_first_name_ar,T.last_name_ar as teacher_last_name_ar
		         ,C.class_name, C.class_name_ar, S.section_name, S.section_name_ar, SCT.school_type, SCT.school_type_ar 
				 , SEM.title,SEM.title_ar
		        FROM ".TBL_CARDS."  as A LEFT JOIN ".TBL_CARD_POINTS." as B ON A.card_type = B.tbl_card_category_id 
		        LEFT JOIN ".TBL_STUDENT." AS ST ON A.tbl_student_id = ST.tbl_student_id
				LEFT JOIN ".TBL_TEACHER." AS T ON A.tbl_teacher_id = T.tbl_teacher_id
				LEFT JOIN ".TBL_CLASS."  AS C ON  C.tbl_class_id = A.tbl_class_id
		        LEFT JOIN  ".TBL_SECTION." AS S  ON  C.tbl_section_id = S.tbl_section_id  
				LEFT JOIN  ".TBL_SCHOOL_TYPE." AS SCT  ON  SCT.tbl_school_type_id= C.tbl_school_type_id  
		        LEFT JOIN ".TBL_SEMESTER." as SEM ON SEM.tbl_semester_id = A.tbl_semester_id  ";
		$qry .= " WHERE 1 ";
		
		
		if($tbl_category_id<>"")
		{
			$qry .= " AND A.card_type='".$tbl_category_id."' ";
		}
		
		if($tbl_school_id<>"")
		{
			$qry .= " AND A.tbl_school_id='".$tbl_school_id."' ";
		}
		
		if($tbl_student_id<>"")
		{
			$qry .= " AND A.tbl_student_id='".$tbl_student_id."' ";
		}
		if($tbl_semester_id<>"")
		{
		    $qry .= " AND A.tbl_semester_id='".$tbl_semester_id."' ";
		}
		if($tbl_academic_year<>"")
		{
		    $qry .= " AND SEM.tbl_academic_year_id='".$tbl_academic_year."' ";
		}
		if($tbl_teacher_id<>"")
		{
		    $qry .= " AND A.tbl_teacher_id='".$tbl_teacher_id."' ";
		}
		if($tbl_class_id<>"")
		{
		    $qry .= " AND A.tbl_class_id='".$tbl_class_id."' ";
		}
		
		$qry .= " AND A.tbl_student_id IN (SELECT tbl_student_id FROM ".TBL_STUDENT." WHERE 1 ".$qry_sub." ) ";
		
		$qry .= " ORDER BY added_date DESC ";
		
		if($offset<>"")
        	$qry .= "LIMIT $offset, ".TBL_TEACHER_PAGING;
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
	}
	
	
	function total_school_card_reports_detailed($q="", $tbl_category_id="", $is_active='', $tbl_school_id, $gender, $country, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id)
	{
		
		$qry_sub = "";
		/*if($gender<>"")
		{
			$qry_sub .= " AND gender='".$gender."' ";
		}*/
		
		if($country<>"")
		{
		    $qry_sub .= " AND country='".$country."' ";
		}
		
		if($tbl_school_id<>"")
		{
		    $qry_sub .= " AND tbl_school_id='".$tbl_school_id."' ";
		}
		
		if($tbl_class_id<>"")
		{
		    $qry_sub .= " AND tbl_class_id='".$tbl_class_id."' ";
		}
		
		if($tbl_student_id<>"")
		{
		    $qry_sub .= " AND tbl_student_id='".$tbl_student_id."' ";
		}
		
		$qry = " SELECT A.card_type AS card_type FROM ".TBL_CARDS."  as A 
		        LEFT JOIN ".TBL_CARD_POINTS." as B ON A.card_type = B.tbl_card_category_id 
		        LEFT JOIN ".TBL_STUDENT." AS ST ON A.tbl_student_id = ST.tbl_student_id
				LEFT JOIN ".TBL_TEACHER." AS T ON A.tbl_teacher_id = T.tbl_teacher_id
				LEFT JOIN ".TBL_CLASS."  AS C ON  C.tbl_class_id = A.tbl_class_id
		        LEFT JOIN  ".TBL_SECTION." AS S  ON  C.tbl_section_id = S.tbl_section_id  
				LEFT JOIN  ".TBL_SCHOOL_TYPE." AS SCT  ON  SCT.tbl_school_type_id= C.tbl_school_type_id  
		        LEFT JOIN ".TBL_SEMESTER." as SEM ON SEM.tbl_semester_id = A.tbl_semester_id  ";
		$qry .= " WHERE 1 ";
		
		if($tbl_category_id<>"")
		{
			$qry .= " AND A.card_type='".$tbl_category_id."' ";
		}
		
		if($tbl_school_id<>"")
		{
			$qry .= " AND A.tbl_school_id='".$tbl_school_id."' ";
		}
		if($tbl_student_id<>"")
		{
			$qry .= " AND A.tbl_student_id='".$tbl_student_id."' ";
		}
		if($tbl_semester_id<>"")
		{
		    $qry .= " AND A.tbl_semester_id='".$tbl_semester_id."' ";
		}
		if($tbl_academic_year<>"")
		{
		    $qry .= " AND SEM.tbl_academic_year_id='".$tbl_academic_year."' ";
		}
		if($tbl_teacher_id<>"")
		{
		    $qry .= " AND A.tbl_teacher_id='".$tbl_teacher_id."' ";
		}
		if($tbl_class_id<>"")
		{
		    $qry .= " AND A.tbl_class_id='".$tbl_class_id."' ";
		}
		
		$qry .= " AND A.tbl_student_id IN (SELECT tbl_student_id FROM ".TBL_STUDENT." WHERE 1 ".$qry_sub." ) ";
		
		$qry .= " order BY A.added_date Desc ";
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
	}
	
	
	// End School Cards Report Detailed
	
	// Start School Point Report List
	// Summary of School Points Report
	function school_point_reports($sort_name, $sort_by, $offset, $q="", $tbl_category_id="", $is_active='', $tbl_school_id, $tbl_gender, $tbl_country_id, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id)
	{
		
		$qry_sub = "";
		if($tbl_gender<>"")
		{
			$qry_sub .= " AND gender='".$tbl_gender."' ";
		}
		
		if($tbl_country_id<>"")
		{
		    $qry_sub .= " AND country='".$tbl_country_id."' ";
		}
		
		if($tbl_school_id<>"")
		{
		    $qry_sub .= " AND tbl_school_id='".$tbl_school_id."' ";
		}
		
		if($tbl_class_id<>"")
		{
		    $qry_sub .= " AND tbl_class_id='".$tbl_class_id."' ";
		}
		
		if($tbl_student_id<>"")
		{
		    $qry_sub .= " AND tbl_student_id='".$tbl_student_id."' ";
		}
		
		
		$qry = " SELECT SUM(A.points_student) AS total, A.tbl_point_category_id, B.point_name_en, B.point_name_ar FROM ".TBL_POINTS."  as A LEFT JOIN ".TBL_BEHAVIOUR_POINTS." as B ON A.tbl_point_category_id = B.tbl_point_category_id 
		         LEFT JOIN ".TBL_SEMESTER." as C ON C.tbl_semester_id = A.tbl_semester_id  ";
		$qry .= " WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND A.tbl_school_id='".$tbl_school_id."' ";
		}
		if($tbl_student_id<>"")
		{
			$qry .= " AND A.tbl_student_id='".$tbl_student_id."' ";
		}
		if($tbl_semester_id<>"")
		{
		    $qry .= " AND A.tbl_semester_id='".$tbl_semester_id."' ";
		}
		if($tbl_academic_year<>"")
		{
		    $qry .= " AND C.tbl_academic_year_id='".$tbl_academic_year."' ";
		}
		if($tbl_teacher_id<>"")
		{
		    $qry .= " AND A.tbl_teacher_id='".$tbl_teacher_id."' ";
		}
		if($tbl_class_id<>"")
		{
		    $qry .= " AND A.tbl_class_id='".$tbl_class_id."' ";
		}
		
		$qry .= " AND A.tbl_student_id IN (SELECT tbl_student_id FROM ".TBL_STUDENT." WHERE 1 ".$qry_sub." ) ";
		
		$qry .= " GROUP BY A.tbl_point_category_id ORDER BY total ASC ";
		
        $qry .= "LIMIT $offset , 50 ";
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
	}
	
	
	function total_school_point_reports($q="", $tbl_category_id="", $is_active='', $tbl_school_id, $gender, $country, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id)
	{
		
		$qry_sub = "";
		if($gender<>"")
		{
			$qry_sub .= " AND gender='".$gender."' ";
		}
		
		if($country<>"")
		{
		    $qry_sub .= " AND country='".$country."' ";
		}
		
		if($tbl_school_id<>"")
		{
		    $qry_sub .= " AND tbl_school_id='".$tbl_school_id."' ";
		}
		
		if($tbl_class_id<>"")
		{
		    $qry_sub .= " AND tbl_class_id='".$tbl_class_id."' ";
		}
		
		if($tbl_student_id<>"")
		{
		    $qry_sub .= " AND tbl_student_id='".$tbl_student_id."' ";
		}
		
		
		$qry = " SELECT A.tbl_point_category_id FROM ".TBL_POINTS."  as A LEFT JOIN ".TBL_BEHAVIOUR_POINTS." as B ON A.tbl_point_category_id = B.tbl_point_category_id 
		         LEFT JOIN ".TBL_SEMESTER." as C ON C.tbl_semester_id = A.tbl_semester_id  ";
		$qry .= " WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND A.tbl_school_id='".$tbl_school_id."' ";
		}
		if($tbl_student_id<>"")
		{
			$qry .= " AND A.tbl_student_id='".$tbl_student_id."' ";
		}
		if($tbl_semester_id<>"")
		{
		    $qry .= " AND A.tbl_semester_id='".$tbl_semester_id."' ";
		}
		if($tbl_academic_year<>"")
		{
		    $qry .= " AND C.tbl_academic_year_id='".$tbl_academic_year."' ";
		}
		if($tbl_teacher_id<>"")
		{
		    $qry .= " AND A.tbl_teacher_id='".$tbl_teacher_id."' ";
		}
		if($tbl_class_id<>"")
		{
		    $qry .= " AND A.tbl_class_id='".$tbl_class_id."' ";
		}
		
		$qry .= " AND A.tbl_point_category_id IN (SELECT tbl_student_id FROM ".TBL_STUDENT." WHERE 1 ".$qry_sub." ) ";
		
		$qry .= " GROUP BY A.tbl_point_category_id ";
		
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
	}
	// End School Summary Points Report
	
	// Start School Point Reports Detailed
	
	// Start School Cards Report Detailed
	function school_point_reports_detailed($sort_name, $sort_by, $offset, $q="", $tbl_category_id="", $is_active='', $tbl_school_id, $tbl_gender, $tbl_country_id, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id)
	{
		
		$qry_sub = "";
		if($tbl_gender<>"")
		{
			$qry_sub .= " AND gender='".$tbl_gender."' ";
		}
		
		if($tbl_country_id<>"")
		{
		    $qry_sub .= " AND country='".$tbl_country_id."' ";
		}
		
		if($tbl_school_id<>"")
		{
		    $qry_sub .= " AND tbl_school_id='".$tbl_school_id."' ";
		}
		
		if($tbl_class_id<>"")
		{
		    $qry_sub .= " AND tbl_class_id='".$tbl_class_id."' ";
		}
		
		if($tbl_student_id<>"")
		{
		    $qry_sub .= " AND tbl_student_id='".$tbl_student_id."' ";
		}
		
		$qry = " SELECT A.points_student, A.tbl_point_category_id, A.added_date,  B.point_name_en, B.point_name_ar
		         , ST.first_name,ST.last_name,ST.first_name_ar,ST.last_name_ar
				 , T.first_name as teacher_first_name,T.last_name as teacher_last_name,T.first_name_ar as teacher_first_name_ar,T.last_name_ar as teacher_last_name_ar
		         ,C.class_name, C.class_name_ar, S.section_name, S.section_name_ar, SCT.school_type, SCT.school_type_ar 
				 , SEM.title,SEM.title_ar
		        FROM ".TBL_POINTS."  as A LEFT JOIN ".TBL_BEHAVIOUR_POINTS." as B ON A.tbl_point_category_id = B.tbl_point_category_id 
		        LEFT JOIN ".TBL_STUDENT." AS ST ON A.tbl_student_id = ST.tbl_student_id
				LEFT JOIN ".TBL_TEACHER." AS T ON A.tbl_teacher_id = T.tbl_teacher_id
				LEFT JOIN ".TBL_CLASS."  AS C ON  C.tbl_class_id = A.tbl_class_id
		        LEFT JOIN  ".TBL_SECTION." AS S  ON  C.tbl_section_id = S.tbl_section_id  
				LEFT JOIN  ".TBL_SCHOOL_TYPE." AS SCT  ON  SCT.tbl_school_type_id= C.tbl_school_type_id  
		        LEFT JOIN ".TBL_SEMESTER." as SEM ON SEM.tbl_semester_id = A.tbl_semester_id  ";
		$qry .= " WHERE 1 ";
		
		
		if($tbl_category_id<>"")
		{
			$qry .= " AND A.tbl_point_category_id='".$tbl_category_id."' ";
		}
		
		if($tbl_school_id<>"")
		{
			$qry .= " AND A.tbl_school_id='".$tbl_school_id."' ";
		}
		
		if($tbl_student_id<>"")
		{
			$qry .= " AND A.tbl_student_id='".$tbl_student_id."' ";
		}
		if($tbl_semester_id<>"")
		{
		    $qry .= " AND A.tbl_semester_id='".$tbl_semester_id."' ";
		}
		if($tbl_academic_year<>"")
		{
		    $qry .= " AND SEM.tbl_academic_year_id='".$tbl_academic_year."' ";
		}
		if($tbl_teacher_id<>"")
		{
		    $qry .= " AND A.tbl_teacher_id='".$tbl_teacher_id."' ";
		}
		if($tbl_class_id<>"")
		{
		    $qry .= " AND A.tbl_class_id='".$tbl_class_id."' ";
		}
		
		$qry .= " AND A.tbl_student_id IN (SELECT tbl_student_id FROM ".TBL_STUDENT." WHERE 1 ".$qry_sub." ) ";
		
		$qry .= " ORDER BY added_date DESC ";
		
        $qry .= "LIMIT $offset, ".TBL_TEACHER_PAGING;
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
	}
	
	
	function total_school_point_reports_detailed($q="", $tbl_category_id="", $is_active='', $tbl_school_id, $gender, $country, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id)
	{
		
		$qry_sub = "";
		if($gender<>"")
		{
			$qry_sub .= " AND gender='".$gender."' ";
		}
		
		if($country<>"")
		{
		    $qry_sub .= " AND country='".$country."' ";
		}
		
		if($tbl_school_id<>"")
		{
		    $qry_sub .= " AND tbl_school_id='".$tbl_school_id."' ";
		}
		
		if($tbl_class_id<>"")
		{
		    $qry_sub .= " AND tbl_class_id='".$tbl_class_id."' ";
		}
		
		if($tbl_student_id<>"")
		{
		    $qry_sub .= " AND tbl_student_id='".$tbl_student_id."' ";
		}
		$qry = " SELECT A.tbl_point_category_id FROM ".TBL_POINTS."  as A 
		        LEFT JOIN ".TBL_BEHAVIOUR_POINTS." as B ON A.tbl_point_category_id = B.tbl_point_category_id 
		        LEFT JOIN ".TBL_STUDENT." AS ST ON A.tbl_student_id = ST.tbl_student_id
				LEFT JOIN ".TBL_TEACHER." AS T ON A.tbl_teacher_id = T.tbl_teacher_id
				LEFT JOIN ".TBL_CLASS."  AS C ON  C.tbl_class_id = A.tbl_class_id
		        LEFT JOIN  ".TBL_SECTION." AS S  ON  C.tbl_section_id = S.tbl_section_id  
				LEFT JOIN  ".TBL_SCHOOL_TYPE." AS SCT  ON  SCT.tbl_school_type_id= C.tbl_school_type_id  
		        LEFT JOIN ".TBL_SEMESTER." as SEM ON SEM.tbl_semester_id = A.tbl_semester_id  ";
		$qry .= " WHERE 1 ";
		
		if($tbl_category_id<>"")
		{
			$qry .= " AND A.tbl_point_category_id='".$tbl_category_id."' ";
		}
		
		if($tbl_school_id<>"")
		{
			$qry .= " AND A.tbl_school_id='".$tbl_school_id."' ";
		}
		if($tbl_student_id<>"")
		{
			$qry .= " AND A.tbl_student_id='".$tbl_student_id."' ";
		}
		if($tbl_semester_id<>"")
		{
		    $qry .= " AND A.tbl_semester_id='".$tbl_semester_id."' ";
		}
		if($tbl_academic_year<>"")
		{
		    $qry .= " AND SEM.tbl_academic_year_id='".$tbl_academic_year."' ";
		}
		if($tbl_teacher_id<>"")
		{
		    $qry .= " AND A.tbl_teacher_id='".$tbl_teacher_id."' ";
		}
		if($tbl_class_id<>"")
		{
		    $qry .= " AND A.tbl_class_id='".$tbl_class_id."' ";
		}
		
		$qry .= " AND A.tbl_student_id IN (SELECT tbl_student_id FROM ".TBL_STUDENT." WHERE 1 ".$qry_sub." ) ";
		
		$qry .= " order BY A.added_date Desc ";
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
	}
	// End School Point Reports Detailed
	// End School Point Report List
	/******************************************************************************************/
	
	// Start School Students Attendance Report
	
	//Attendance Reported Dates and Sessions
	function school_attendance_date_reports($sort_name, $sort_by, $offset, $q="", $tbl_category_id="", $is_active='', $tbl_school_id, $tbl_gender, $attendance_date, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id, $start_date, $end_date)
	{

	   $qry = " SELECT count(A.tbl_student_id ) total 
			,count(case when A.is_present ='N'  then 1 end) as absent_count
			,count(case when A.is_present ='Y' then 1 end) as present_count
			,count(case when A.is_present ='LC' then 1 end) as late_coming_count
			,count(case when A.is_present ='EG' then 1 end) as early_going_count
			,A.attendance_date, A.tbl_class_sessions_id, A.tbl_school_id, B.title, B.title_ar, B.start_time, B.end_time, C.id, C.class_name, C.class_name_ar, S.section_name, S.section_name_ar, C.tbl_class_id 
			FROM ".TBL_TEACHER_ATTENDANCE." as A 
			LEFT JOIN ".TBL_CLASS_SESSIONS." as B ON A.tbl_class_sessions_id = B.tbl_class_sessions_id 
			LEFT JOIN ".TBL_CLASS."  AS C ON  C.tbl_class_id = A.tbl_class_id
			LEFT JOIN  ".TBL_SECTION." AS S  ON  C.tbl_section_id = S.tbl_section_id  
			LEFT JOIN  ".TBL_SCHOOL_TYPE." AS SCT  ON  SCT.tbl_school_type_id= C.tbl_school_type_id  ";
	   $qry .= " WHERE 1 ";	
	
		if($tbl_school_id<>"")
		{
			$qry .= " AND A.tbl_school_id='".$tbl_school_id."' ";
		}
		if($tbl_student_id<>"")
		{
			$qry .= " AND A.tbl_student_id='".$tbl_student_id."' ";
		}
		if($tbl_semester_id<>"" || $tbl_academic_year<>"" )
		{
		   // $qry .= " AND A.tbl_semester_id='".$tbl_semester_id."' ";
		    $qry .= " AND A.attendance_date BETWEEN '".$start_date."' AND  '".$end_date."' ";
		}
		if($tbl_teacher_id<>"")
		{
		    $qry .= " AND A.tbl_teacher_id='".$tbl_teacher_id."' ";
		}
		if($tbl_class_id<>"")
		{
		    $qry .= " AND A.tbl_class_id='".$tbl_class_id."' ";
		}
		if($attendance_date<>"")
		{
		    $qry .= " AND A.attendance_date='".$attendance_date."' ";
		}
		
		$qry .= "  GROUP BY A.tbl_class_sessions_id,A.attendance_date ORDER BY A.attendance_date DESC, C.priority ASC, B.start_time ASC, A.added_date DESC ";
		
        $qry .= "LIMIT $offset , ".PAGING_TBL_DATE_ATTENDANCE;
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
	}
	
	
	function total_school_attendance_date_reports($q="", $tbl_category_id="", $is_active='', $tbl_school_id, $gender, $attendance_date, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id, $start_date, $end_date)
	{
		
		$qry = " SELECT A.tbl_class_sessions_id FROM ".TBL_TEACHER_ATTENDANCE."  as A 
		         LEFT JOIN ".TBL_CLASS_SESSIONS." as B ON A.tbl_class_sessions_id = B.tbl_class_sessions_id 
				 LEFT JOIN ".TBL_CLASS."  AS C ON  C.tbl_class_id = A.tbl_class_id
				 LEFT JOIN  ".TBL_SECTION." AS S  ON  C.tbl_section_id = S.tbl_section_id  
				 LEFT JOIN  ".TBL_SCHOOL_TYPE." AS SCT  ON  SCT.tbl_school_type_id= C.tbl_school_type_id "; 
		$qry .= " WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND A.tbl_school_id='".$tbl_school_id."' ";
		}
		if($tbl_student_id<>"")
		{
			$qry .= " AND A.tbl_student_id='".$tbl_student_id."' ";
		}
		if($tbl_semester_id<>"" || $tbl_academic_year<>"" )
		{
		   // $qry .= " AND A.tbl_semester_id='".$tbl_semester_id."' ";
		    $qry .= " AND A.attendance_date BETWEEN '".$start_date."' AND  '".$end_date."' ";
		}
	/*	if($tbl_academic_year<>"")
		{
		    $qry .= " AND SEM.tbl_academic_year_id='".$tbl_academic_year."' ";
		}*/
		if($tbl_teacher_id<>"")
		{
		    $qry .= " AND A.tbl_teacher_id='".$tbl_teacher_id."' ";
		}
		if($tbl_class_id<>"")
		{
		    $qry .= " AND A.tbl_class_id='".$tbl_class_id."' ";
		}
		if($attendance_date<>"")
		{
		    $qry .= " AND A.attendance_date='".$attendance_date."' ";
		}
		
		$qry .= "  GROUP BY A.tbl_class_sessions_id,A.attendance_date";
		
		
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
	}
	
	
	
	function total_attendance_date_summary($q="", $tbl_category_id="", $is_active='', $tbl_school_id, $gender, $attendance_date, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id, $start_date, $end_date)
	{
		
		$qry = " SELECT A.tbl_class_sessions_id FROM ".TBL_TEACHER_ATTENDANCE."  as A 
		         LEFT JOIN ".TBL_CLASS_SESSIONS." as B ON A.tbl_class_sessions_id = B.tbl_class_sessions_id 
				 LEFT JOIN ".TBL_CLASS."  AS C ON  C.tbl_class_id = A.tbl_class_id
				 LEFT JOIN  ".TBL_SECTION." AS S  ON  C.tbl_section_id = S.tbl_section_id  
				 LEFT JOIN  ".TBL_SCHOOL_TYPE." AS SCT  ON  SCT.tbl_school_type_id= C.tbl_school_type_id "; 
		$qry .= " WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND A.tbl_school_id='".$tbl_school_id."' ";
		}
		if($tbl_student_id<>"")
		{
			$qry .= " AND A.tbl_student_id='".$tbl_student_id."' ";
		}
		if($tbl_semester_id<>"" || $tbl_academic_year<>"" )
		{
		   // $qry .= " AND A.tbl_semester_id='".$tbl_semester_id."' ";
		    $qry .= " AND A.attendance_date BETWEEN '".$start_date."' AND  '".$end_date."' ";
		}
	/*	if($tbl_academic_year<>"")
		{
		    $qry .= " AND SEM.tbl_academic_year_id='".$tbl_academic_year."' ";
		}*/
		if($tbl_teacher_id<>"")
		{
		    $qry .= " AND A.tbl_teacher_id='".$tbl_teacher_id."' ";
		}
		if($tbl_class_id<>"")
		{
		    $qry .= " AND A.tbl_class_id='".$tbl_class_id."' ";
		}
		if($attendance_date<>"")
		{
		    $qry .= " AND A.attendance_date='".$attendance_date."' ";
		}
		
		$qry .= "  GROUP BY A.tbl_class_sessions_id,A.attendance_date";
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
	}
	
	
	
	// Start Summary of Student Attendance
	function school_attendance_reports($sort_name, $sort_by, $offset, $q="", $tbl_category_id="", $is_active='', $tbl_school_id, $tbl_gender, $attendance_date, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id)
	{
		
		$qry_sub = "";
		if($tbl_gender<>"")
		{
			$qry_sub .= " AND gender='".$tbl_gender."' ";
		}
		
		if($tbl_school_id<>"")
		{
		    $qry_sub .= " AND tbl_school_id='".$tbl_school_id."' ";
		}
		
		if($tbl_class_id<>"")
		{
		    $qry_sub .= " AND tbl_class_id='".$tbl_class_id."' ";
		}
		
		if($tbl_student_id<>"")
		{
		    $qry_sub .= " AND tbl_student_id='".$tbl_student_id."' ";
		}
		
		
		$qry = " SELECT count(A.tbl_student_id ) total , A.attendance_date, C.id, C.class_name, C.class_name_ar, S.section_name, S.section_name_ar, C.tbl_class_id FROM ".TBL_TEACHER_ATTENDANCE."  as A 
				 LEFT JOIN ".TBL_CLASS_SESSIONS." as B ON A.tbl_class_sessions_id = B.tbl_class_sessions_id 
				 LEFT JOIN ".TBL_CLASS."  AS C ON  C.tbl_class_id = A.tbl_class_id
				LEFT JOIN  ".TBL_SECTION." AS S  ON  C.tbl_section_id = S.tbl_section_id  
				LEFT JOIN  ".TBL_SCHOOL_TYPE." AS SCT  ON  SCT.tbl_school_type_id= C.tbl_school_type_id  
				LEFT JOIN ".TBL_SEMESTER." as SEM ON SEM.tbl_semester_id = A.tbl_semester_id  ";
		$qry .= " WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND A.tbl_school_id='".$tbl_school_id."' ";
		}
		if($tbl_student_id<>"")
		{
			$qry .= " AND A.tbl_student_id='".$tbl_student_id."' ";
		}
		if($tbl_semester_id<>"")
		{
		    $qry .= " AND A.tbl_semester_id='".$tbl_semester_id."' ";
		}
		/*if($tbl_academic_year<>"")
		{
		    $qry .= " AND SEM.tbl_academic_year_id='".$tbl_academic_year."' ";
		}*/
		
		if($tbl_teacher_id<>"")
		{
		    $qry .= " AND A.tbl_teacher_id='".$tbl_teacher_id."' ";
		}
		if($tbl_class_id<>"")
		{
		    $qry .= " AND A.tbl_class_id='".$tbl_class_id."' ";
		}
		if($attendance_date<>"")
		{
		    $qry .= " AND A.attendance_date='".$attendance_date."' ";
		}
		
		//$qry .= " AND A.is_present='Y' ";
		
		$qry .= " AND A.tbl_student_id IN (SELECT tbl_student_id FROM ".TBL_STUDENT." WHERE 1 ".$qry_sub." ) ";
		
		$qry .= " group by A.tbl_class_id ORDER BY  C.id ASC ";
		
        $qry .= "LIMIT $offset , ".TBL_SCHOOL_TYPE_PAGING;
		
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
	}
	
	
	function total_school_attendance_reports($q="", $tbl_category_id="", $is_active='', $tbl_school_id, $gender, $attendance_date, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $tbl_academic_year, $tbl_teacher_id)
	{
		
		$qry_sub = "";
		if($gender<>"")
		{
			$qry_sub .= " AND gender='".$gender."' ";
		}
		
		
		if($tbl_school_id<>"")
		{
		    $qry_sub .= " AND tbl_school_id='".$tbl_school_id."' ";
		}
		
		if($tbl_class_id<>"")
		{
		    $qry_sub .= " AND tbl_class_id='".$tbl_class_id."' ";
		}
		
		if($tbl_student_id<>"")
		{
		    $qry_sub .= " AND tbl_student_id='".$tbl_student_id."' ";
		}
		
		
		$qry = " SELECT A.tbl_class_id FROM ".TBL_TEACHER_ATTENDANCE."  as A 
		         LEFT JOIN ".TBL_CLASS_SESSIONS." as B ON A.tbl_class_sessions_id = B.tbl_class_sessions_id 
				 LEFT JOIN ".TBL_CLASS."  AS C ON  C.tbl_class_id = A.tbl_class_id
				 LEFT JOIN  ".TBL_SECTION." AS S  ON  C.tbl_section_id = S.tbl_section_id  
				 LEFT JOIN  ".TBL_SCHOOL_TYPE." AS SCT  ON  SCT.tbl_school_type_id= C.tbl_school_type_id  
				 LEFT JOIN ".TBL_SEMESTER." as SEM ON SEM.tbl_semester_id = A.tbl_semester_id  ";
		$qry .= " WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND A.tbl_school_id='".$tbl_school_id."' ";
		}
		if($tbl_student_id<>"")
		{
			$qry .= " AND A.tbl_student_id='".$tbl_student_id."' ";
		}
		if($tbl_semester_id<>"")
		{
		    $qry .= " AND A.tbl_semester_id='".$tbl_semester_id."' ";
		}
	/*	if($tbl_academic_year<>"")
		{
		    $qry .= " AND SEM.tbl_academic_year_id='".$tbl_academic_year."' ";
		}*/
		if($tbl_teacher_id<>"")
		{
		    $qry .= " AND A.tbl_teacher_id='".$tbl_teacher_id."' ";
		}
		if($tbl_class_id<>"")
		{
		    $qry .= " AND A.tbl_class_id='".$tbl_class_id."' ";
		}
		if($attendance_date<>"")
		{
		    $qry .= " AND A.attendance_date='".$attendance_date."' ";
		}
		
		$qry .= " AND A.tbl_student_id IN (SELECT tbl_student_id FROM ".TBL_STUDENT." WHERE 1 ".$qry_sub." ) ";
		
		$qry .= " GROUP BY A.tbl_class_id ";
		
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
	}
	
	// End Summary of Student Attendance
	
	// Detailed Attendance Report - Daily
	// Start School Cards Report Detailed
	function school_attendance_reports_detailed($sort_name, $sort_by, $offset, $q="", $tbl_category_id="", $is_active='', $tbl_school_id, $tbl_gender, $tbl_country_id, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $attendance_date, $tbl_teacher_id)
	{
		
		$qry_sub = "";
		if($tbl_gender<>"")
		{
			$qry_sub .= " AND gender='".$tbl_gender."' ";
		}
		
		if($tbl_country_id<>"")
		{
		    $qry_sub .= " AND country='".$tbl_country_id."' ";
		}
		
		if($tbl_school_id<>"")
		{
		    $qry_sub .= " AND tbl_school_id='".$tbl_school_id."' ";
		}
		
		if($tbl_class_id<>"")
		{
		    $qry_sub .= " AND tbl_class_id='".$tbl_class_id."' ";
		}
		
		if($tbl_student_id<>"")
		{
		    $qry_sub .= " AND tbl_student_id='".$tbl_student_id."' ";
		}
		
		/*$qry = " SELECT A.points_student, A.tbl_point_category_id, A.added_date,  B.point_name_en, B.point_name_ar
		         , ST.first_name,ST.last_name,ST.first_name_ar,ST.last_name_ar
				 , T.first_name as teacher_first_name,T.last_name as teacher_last_name,T.first_name_ar as teacher_first_name_ar,T.last_name_ar as teacher_last_name_ar
		         ,C.class_name, C.class_name_ar, S.section_name, S.section_name_ar, SCT.school_type, SCT.school_type_ar 
				 , SEM.title,SEM.title_ar
		        FROM ".TBL_TEACHER_ATTENDANCE."  as A 
		        LEFT JOIN ".TBL_CLASS_SESSIONS." as B ON A.tbl_class_sessions_id = B.tbl_class_sessions_id 
		        LEFT JOIN ".TBL_STUDENT." AS ST ON A.tbl_student_id = ST.tbl_student_id
				LEFT JOIN ".TBL_TEACHER." AS T ON A.tbl_teacher_id = T.tbl_teacher_id
				LEFT JOIN ".TBL_CLASS."  AS C ON  C.tbl_class_id = A.tbl_class_id
		        LEFT JOIN  ".TBL_SECTION." AS S  ON  C.tbl_section_id = S.tbl_section_id  
				LEFT JOIN  ".TBL_SCHOOL_TYPE." AS SCT  ON  SCT.tbl_school_type_id= C.tbl_school_type_id  
		        LEFT JOIN ".TBL_SEMESTER." as SEM ON SEM.tbl_semester_id = A.tbl_semester_id  ";*/
		
		
		$qry = " SELECT A.tbl_student_id, ST.first_name,ST.last_name,ST.first_name_ar,ST.last_name_ar
		        FROM ".TBL_TEACHER_ATTENDANCE."  as A 
				LEFT JOIN ".TBL_STUDENT." AS ST ON A.tbl_student_id = ST.tbl_student_id
		        LEFT JOIN ".TBL_SEMESTER." as SEM ON SEM.tbl_semester_id = A.tbl_semester_id  ";
		$qry .= " WHERE 1 ";
		
		if($tbl_school_id<>"")
		{
			$qry .= " AND A.tbl_school_id='".$tbl_school_id."' ";
		}
		
		if($tbl_student_id<>"")
		{
			$qry .= " AND A.tbl_student_id='".$tbl_student_id."' ";
		}
		if($tbl_semester_id<>"")
		{
		    $qry .= " AND A.tbl_semester_id='".$tbl_semester_id."' ";
		}
		if($tbl_teacher_id<>"")
		{
		    $qry .= " AND A.tbl_teacher_id='".$tbl_teacher_id."' ";
		}
		if($tbl_class_id<>"")
		{
		    $qry .= " AND A.tbl_class_id='".$tbl_class_id."' ";
		}
		
		if($attendance_date<>"")
		{
		    $qry .= " AND A.attendance_date='".$attendance_date."' ";
		}
		
		$qry .= " AND A.tbl_student_id IN (SELECT tbl_student_id FROM ".TBL_STUDENT." WHERE 1 ".$qry_sub." ) ";
		
		$qry .= " ORDER BY ST.first_name ASC ";
		
        $qry .= "LIMIT $offset, ".TBL_TEACHER_PAGING;
		$results = $this->cf->selectMultiRecords($qry);
		
		for($j=0;$j<count($results); $j++)
		{
			$tbl_student_id = $results[$j]['tbl_student_id'];
			$data  = "";
			$session_data = "";
			$session_time = "";
			$attendance   = "";
			$added_by     = "";
			//$attendance_date
			$sess_qry = "SELECT A.tbl_class_sessions_id, A.is_present, B.title AS session_title, B.title_ar AS session_title_ar, B.start_time, B.end_time, T.first_name,T.first_name_ar,T.last_name,T.last_name_ar, SEM.title,SEM.title_ar
		        FROM ".TBL_TEACHER_ATTENDANCE."  as A 
		        LEFT JOIN ".TBL_CLASS_SESSIONS." as B ON A.tbl_class_sessions_id = B.tbl_class_sessions_id 
				LEFT JOIN ".TBL_TEACHER." AS T ON A.tbl_teacher_id = T.tbl_teacher_id
		        LEFT JOIN ".TBL_SEMESTER." as SEM ON SEM.tbl_semester_id = A.tbl_semester_id WHERE 1 and A.tbl_student_id = '".$tbl_student_id."' AND A.attendance_date='".$attendance_date."'  ";
			$resultSess = $this->cf->selectMultiRecords($sess_qry);
			for($k=0;$k<count($resultSess);$k++)
			{
				if($resultSess[$k]['is_present']=="Y")
				{
					$is_present = "Present";
					$color      = "Green";
				}else if($resultSess[$k]['is_present']=="LC")
				{
					$is_present = "Late Coming";
					$color      = "Orange";
				}else if($resultSess[$k]['is_present']=="EG")
				{
					$is_present = "Early Going";
					$color      = "Blue";
				}else{
					$is_present = "Absent";
					$color      = "Red";
				}
				
				$session_data .= '<div class="txt_en">'.$resultSess[$k]['session_title'].'</div>';
				$session_data .= '<div class="txt_ar">'.$resultSess[$k]['session_title_ar'].'</div><br>';
				
				$session_time .= '<div class="txt_en">'.$resultSess[$k]['start_time'].' - '.$resultSess[$k]['end_time'].'</div><div class="txt_ar"></div><br>';
				
				$attendance   .= '<div class="txt_en" style="color:'.$color.'">&nbsp;'.$is_present.'&nbsp;</div><div class="txt_ar"></div>';
				
				$added_by     .= '<div class="txt_en">'.$resultSess[$k]['first_name'].'&nbsp;'.$resultSess[$k]['last_name'].'</div><div class="txt_ar">'.$resultSess[$k]['first_name_ar'].'&nbsp'.$resultSess[$k]['last_name_ar'].'</div>';
				
			}
			$results[$j]['session_data'] = $session_data;
			$results[$j]['session_time'] = $session_time;
			$results[$j]['attendance']   = $attendance;
			$results[$j]['added_by']     = $added_by;
			
		}
		
	    return $results;
	}
	
	
	function total_school_attendance_reports_detailed($q="", $tbl_category_id="", $is_active='', $tbl_school_id, $gender, $country, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $attendance_date, $tbl_teacher_id)
	{
		
		$qry_sub = "";
		if($gender<>"")
		{
			$qry_sub .= " AND gender='".$gender."' ";
		}
		
		if($tbl_school_id<>"")
		{
		    $qry_sub .= " AND tbl_school_id='".$tbl_school_id."' ";
		}
		
		if($tbl_class_id<>"")
		{
		    $qry_sub .= " AND tbl_class_id='".$tbl_class_id."' ";
		}
		
		if($tbl_student_id<>"")
		{
		    $qry_sub .= " AND tbl_student_id='".$tbl_student_id."' ";
		}
		$qry = " SELECT A.tbl_student_id 
		        FROM ".TBL_TEACHER_ATTENDANCE."  as A 
		        LEFT JOIN ".TBL_CLASS_SESSIONS." as B ON A.tbl_class_sessions_id = B.tbl_class_sessions_id 
				LEFT JOIN ".TBL_TEACHER." AS T ON A.tbl_teacher_id = T.tbl_teacher_id
		        LEFT JOIN ".TBL_SEMESTER." as SEM ON SEM.tbl_semester_id = A.tbl_semester_id WHERE 1 ";
		
		
		if($tbl_school_id<>"")
		{
			$qry .= " AND A.tbl_school_id='".$tbl_school_id."' ";
		}
		if($tbl_student_id<>"")
		{
			$qry .= " AND A.tbl_student_id='".$tbl_student_id."' ";
		}
		if($tbl_semester_id<>"")
		{
		    $qry .= " AND A.tbl_semester_id='".$tbl_semester_id."' ";
		}
		
		if($tbl_teacher_id<>"")
		{
		    $qry .= " AND A.tbl_teacher_id='".$tbl_teacher_id."' ";
		}
		if($tbl_class_id<>"")
		{
		    $qry .= " AND A.tbl_class_id='".$tbl_class_id."' ";
		}
		
		if($attendance_date<>"")
		{
		    $qry .= " AND A.attendance_date='".$attendance_date."' ";
		}
		$qry .= " AND A.tbl_student_id IN (SELECT tbl_student_id FROM ".TBL_STUDENT." WHERE 1 ".$qry_sub." ) ";
		
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
	}
	
	
	// End Attendance Report Daily
	
	
	
	// End School Students Attendance Report
	
	
	// DASHBOARD GRAPHICAL REPORT
	    // students in classes
	   function getStudentCntBasedOnClasses($tbl_school_id)
	   {
		   $qry = " SELECT count(A.tbl_student_id ) as  total , C.class_name, C.class_name_ar, C.tbl_class_id FROM ".TBL_STUDENT."  as A 
					LEFT JOIN ".TBL_CLASS."  AS C ON  C.tbl_class_id = A.tbl_class_id
					LEFT JOIN  ".TBL_SECTION." AS S  ON  C.tbl_section_id = S.tbl_section_id  
					LEFT JOIN  ".TBL_SCHOOL_TYPE." AS SCT  ON  SCT.tbl_school_type_id= C.tbl_school_type_id  ";
			$qry .= " WHERE 1 ";
			if($tbl_school_id<>"")
			{
				$qry .= " AND A.tbl_school_id='".$tbl_school_id."' ";
			}
			$qry .= " GROUP BY  C.class_name ORDER BY  C.class_name ASC";
			$results = $this->cf->selectMultiRecords($qry);
			return $results;
	    }
		
		
		 // students in classes
	   function getPointsBasedOnClasses($tbl_school_id)
	   {
			
			$qry = " SELECT SUM(A.points_student) AS total, C.class_name, C.class_name_ar FROM ".TBL_POINTS."  as A 
					LEFT JOIN ".TBL_CLASS."  AS C ON  C.tbl_class_id = A.tbl_class_id
					LEFT JOIN  ".TBL_SECTION." AS S  ON  C.tbl_section_id = S.tbl_section_id  
					LEFT JOIN  ".TBL_SCHOOL_TYPE." AS SCT  ON  SCT.tbl_school_type_id= C.tbl_school_type_id 
					LEFT JOIN ".TBL_SEMESTER." as SEM ON SEM.tbl_semester_id = A.tbl_semester_id 
					LEFT JOIN ".TBL_ACADEMIC_YEAR." as AY ON AY.tbl_academic_year_id = SEM.tbl_academic_year_id  
					
					 ";
			$qry .= " WHERE 1 ";
			if($tbl_school_id<>"")
			{
				$qry .= " AND A.tbl_school_id='".$tbl_school_id."' ";
			}
			if($tbl_semester_id<>"")
			{
				$qry .= " AND A.tbl_semester_id='".$tbl_semester_id."' ";
			}
			$qry .= "  AND ( AY.academic_start >='".$currentDate."' AND  AY.academic_end >='".$currentDate."' )";
			$qry .= " GROUP BY  C.class_name ORDER BY  C.class_name ASC";
			$results = $this->cf->selectMultiRecords($qry);
			return $results;
     }
	
	//END BACKEND FUNCTIONALITY
	
	// FOR PARENT - ATTENDANCE REPORT
	
	// Detailed Attendance Report - Daily
	function mychild_attendance_reports_detailed($sort_name, $sort_by, $offset, $q="", $tbl_category_id="", $is_active='', $tbl_school_id, $tbl_gender, $tbl_country_id, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $attendance_date, $tbl_teacher_id)
	{
			$data  = "";
			$session_data = "";
			$session_time = "";
			$attendance   = "";
			$added_by     = "";
			
			$sess_att = "SELECT A.attendance_date
		        FROM ".TBL_TEACHER_ATTENDANCE."  as A 
		        LEFT JOIN ".TBL_CLASS_SESSIONS." as B ON A.tbl_class_sessions_id = B.tbl_class_sessions_id 
				LEFT JOIN ".TBL_TEACHER." AS T ON A.tbl_teacher_id = T.tbl_teacher_id
		        LEFT JOIN ".TBL_SEMESTER." as SEM ON SEM.tbl_semester_id = A.tbl_semester_id WHERE 1  ";
			$sess_att .=	" AND A.tbl_student_id = '".$tbl_student_id."'  ";
			
			if($attendance_date<>"")
				$sess_att .=	" AND A.attendance_date='".$attendance_date."'  ";
			$sess_att .=	" GROUP BY  A.attendance_date  ORDER BY A.attendance_date DESC ";
			
			$sess_att .=    "LIMIT $offset, ".TBL_SCHOOL_TYPE_PAGING;
			
			$resultAtt = $this->cf->selectMultiRecords($sess_att);
			for($x=0;$x<count($resultAtt);$x++)
			{
			    $tbl_attendance_date =  $resultAtt[$x]['attendance_date'];
				//$attendance_date
				$sess_qry = "SELECT A.attendance_date, A.tbl_class_sessions_id, A.is_present, B.title AS session_title, B.title_ar AS session_title_ar, B.start_time, B.end_time, T.first_name,T.first_name_ar,T.last_name,T.last_name_ar, SEM.title,SEM.title_ar
					FROM ".TBL_TEACHER_ATTENDANCE."  as A 
					LEFT JOIN ".TBL_CLASS_SESSIONS." as B ON A.tbl_class_sessions_id = B.tbl_class_sessions_id 
					LEFT JOIN ".TBL_TEACHER." AS T ON A.tbl_teacher_id = T.tbl_teacher_id
					LEFT JOIN ".TBL_SEMESTER." as SEM ON SEM.tbl_semester_id = A.tbl_semester_id WHERE 1  ";
				$sess_qry .=	" AND A.tbl_student_id = '".$tbl_student_id."'  ";
				
				if($tbl_attendance_date<>"")
					$sess_qry .=	" AND A.attendance_date='".$tbl_attendance_date."'  ";
				
				$resultSess = $this->cf->selectMultiRecords($sess_qry);
				
				for($k=0;$k<count($resultSess);$k++)
				{
					if($resultSess[$k]['is_present']=="Y")
					{
						$is_present = "Present";
						$color      = "Green";
					}else{
						$is_present = "Absent";
						$color      = "Red";
					}
					
					$attendance_date = date("d-M-Y", strtotime($resultSess[$k]['attendance_date']));
					
					$session_data .= '<div style="float:left;">'.$resultSess[$k]['session_title'].'</div>';
					$session_data .= '<div style="float:right;">'.$resultSess[$k]['session_title_ar'].'</div><br>';
					
					$session_time .= '<div class="txt_en">'.$resultSess[$k]['start_time'].' - '.$resultSess[$k]['end_time'].'</div>';
					
					$attendance   .= '<div class="txt_en" style="color:'.$color.'">&nbsp;'.$is_present.'&nbsp;</div>';
					
					$added_by     .= '<div style="float:left;">'.$resultSess[$k]['first_name'].'&nbsp;'.$resultSess[$k]['last_name'].'</div><div style="float:right;">'.$resultSess[$k]['first_name_ar'].'&nbsp'.$resultSess[$k]['last_name_ar'].'</div><br>';
					$attendance_dates   .= '<div class="txt_en" >&nbsp;'.$attendance_date.'&nbsp;</div>';
			    }
			$results[$x]['session_data'] 		= $session_data;
			$results[$x]['session_data'] 		= $session_data;
			$results[$x]['session_time'] 		= $session_time;
			$results[$x]['attendance']   		= $attendance;
			$results[$x]['added_by']     		= $added_by;
			$results[$x]['attendance_date']     = $attendance_dates;
			
			$session_data = "";
			$session_time = "";
			$attendance = "";
			$added_by = "";
			$attendance_dates = "";
			
			
		 }
		 
	    return $results;
	}
	
	
	function mychild_total_attendance_reports_detailed($q="", $tbl_category_id="", $is_active='', $tbl_school_id, $gender, $country, $tbl_class_id, $tbl_student_id, 
	                             $tbl_semester_id, $attendance_date, $tbl_teacher_id)
	{
		
		$sess_att = "SELECT A.attendance_date
		        FROM ".TBL_TEACHER_ATTENDANCE."  as A 
		        LEFT JOIN ".TBL_CLASS_SESSIONS." as B ON A.tbl_class_sessions_id = B.tbl_class_sessions_id 
				LEFT JOIN ".TBL_TEACHER." AS T ON A.tbl_teacher_id = T.tbl_teacher_id
		        LEFT JOIN ".TBL_SEMESTER." as SEM ON SEM.tbl_semester_id = A.tbl_semester_id WHERE 1  ";
			$sess_att .=	" AND A.tbl_student_id = '".$tbl_student_id."'  ";
			
			if($attendance_date<>"")
				$sess_att .=	" AND A.attendance_date='".$attendance_date."'  ";
			$sess_att .=	" GROUP BY  A.attendance_date  ORDER BY A.attendance_date DESC ";
		
		$results = $this->cf->selectMultiRecords($sess_att);
	    return count($results);
	}
	
	
	 // Get All School Cards 
	function all_school_cards($tbl_category_id="", $is_active='', $tbl_school_id)
	{
		$qry = " SELECT B.tbl_card_category_id, B.category_name_en, B.category_name_ar FROM ".TBL_CARD_POINTS." as B WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND B.tbl_school_id='".$tbl_school_id."' ";
		}
		$qry .= " AND B.is_active<>'D' ";
		$qry .= " ORDER BY B.card_category_order ASC ";
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
	}
	
	// END FOR PARENT - ATTENDANCE REPORT
	
	function get_attendance_report_details($attendance_date, $tbl_class_sessions_id, $tbl_class_id, $tbl_student_id, $tbl_school_id)
	{
		
		$qry = "SELECT A.attendance_date,A.tbl_class_sessions_id,S.first_name,S.last_name,S.first_name_ar,S.last_name_ar,A.added_date, A.is_present, B.title, B.title_ar, B.start_time, B.end_time FROM ".TBL_TEACHER_ATTENDANCE." AS A 
				INNER JOIN  ".TBL_STUDENT." AS S ON S.tbl_student_id=A.tbl_student_id 
				LEFT JOIN ".TBL_CLASS_SESSIONS." as B ON A.tbl_class_sessions_id = B.tbl_class_sessions_id 
		        LEFT JOIN ".TBL_CLASS." as C ON C.tbl_class_id = A.tbl_class_id 
			    WHERE 1 ";
	
		if (trim($tbl_school_id) != "") {
			$qry .= " AND A.tbl_school_id = '".$tbl_school_id."' ";
		} 
		
		if (trim($tbl_class_id) != "") {
			$qry .= " AND A.tbl_class_id = '".$tbl_class_id."' ";
		} 
		
		if (trim($tbl_student_id) != "") {
			$qry .= " AND A.tbl_student_id = '".$tbl_student_id."' ";
		} 
		
		if (trim($tbl_class_sessions_id) != "") {
			$qry .= " AND A.tbl_class_sessions_id = '".$tbl_class_sessions_id."' ";
		} 
		
		if($attendance_date<>"")
		{
				$qry .=	" AND A.attendance_date='".$attendance_date."'  ";
		}
			
		$qry .=	" ORDER BY S.first_name ASC, S.first_name ASC, A.added_date ASC ";
		$res  = $this->cf->selectMultiRecords($qry);
		return $res;
	}
	
	
  function get_semester_duration($tbl_semester_id,$tbl_school_id)
   {
	   $qry = "SELECT SEM.*  FROM ".TBL_SEMESTER." AS SEM WHERE 1 ";
	   $qry .= " AND SEM.is_active<>'D' "; 
	   
	   
	   if($tbl_semester_id<>"")
	   {
		  $qry    .= " AND SEM.tbl_semester_id = '".trim($tbl_semester_id)."' ";
	   }
	
	   $qry .= " AND SEM.tbl_school_id = '".$tbl_school_id."' ";
	   $results = $this->cf->selectMultiRecords($qry);
	return $results; 
   }
	
   function get_academic_duration($tbl_semester_id, $academic_year_id,$tbl_school_id)
   {
	   $qry = "SELECT MIN(SEM.start_date) AS start_date,MAX(SEM.end_date) as end_date FROM ".TBL_SEMESTER." AS SEM WHERE 1 ";
	   $qry .= " AND SEM.is_active<>'D' "; 
	   
	   if($tbl_semester_id<>"")
	   {
		  $qry    .= " AND SEM.tbl_semester_id = '".trim($tbl_semester_id)."' ";
	   }
	   
	   if($academic_year_id<>"")
	   {
		  $qry    .= " AND SEM.tbl_academic_year_id = '".trim($academic_year_id)."' ";
	   }
	
	   $qry .= " AND SEM.tbl_school_id = '".$tbl_school_id."' ";
	   $results = $this->cf->selectMultiRecords($qry);
	return $results; 
   }
		
	 
	
	

    }

?>