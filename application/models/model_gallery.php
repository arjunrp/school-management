<?php
include_once('include/common_functions.php');
/**
 * @desc   	  	Gallery Model
 * @category   	Model
 * @author     	Shanavas.PK
 * @version    	0.1
 */

class Model_gallery extends CI_Model {

	var $cf;
	/**
	* @desc Default constructor for the Controller
	* @access default
	*/
    function model_gallery() {
		$this->cf = new Common_functions();
    }

	/**
	* @desc		Get teachers id
	* @param	string $email  
	* @access	default
	* @return	$tbl_teacher_id
	*/
	function get_gallery_categories($tbl_school_id) {
		$email = $this->cf->get_data(trim($email));
		$qry = "SELECT * FROM ".TBL_GALLERY_CATEGORY." WHERE is_active='Y' AND tbl_school_id='$tbl_school_id' ORDER BY gallery_category_order ASC ";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
	

	/**
	* @desc		Get image gallery category name
	* @param	string $email  
	* @access	default
	* @return	$tbl_teacher_id
	*/
	function get_gallery_category_name($tbl_gallery_category_id) {
		$qry = "SELECT category_name_en, category_name_ar FROM ".TBL_GALLERY_CATEGORY." WHERE is_active='Y' AND tbl_gallery_category_id='$tbl_gallery_category_id' ";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}

	/**
	* @desc		Get gallery category images
	* @param	string $email  
	* @access	default
	* @return	$tbl_teacher_id
	*/
	function get_gallery_category_images($tbl_gallery_category_id) {
		$tbl_gallery_category_id = $this->cf->get_data(trim($tbl_gallery_category_id));
		$qry = "SELECT * FROM ".TBL_IMAGE_GALLERY." WHERE is_active='Y' AND tbl_gallery_category_id='$tbl_gallery_category_id' ORDER BY image_order ASC ";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;	
	}
	
	/**
	* @desc		Get gallery category images
	* @param	string $email  
	* @access	default
	* @return	$tbl_teacher_id
	*/
	function get_gallery_image($tbl_image_gallery_id) {
		$tbl_image_gallery_id = $this->cf->get_data(trim($tbl_image_gallery_id));
		$qry = "SELECT * FROM ".TBL_IMAGE_GALLERY." WHERE tbl_image_gallery_id='$tbl_image_gallery_id' ";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;	
	}
	
	 //BACKEND FUNCTIONALITY
		
	function get_school_gallery($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_gallery_category_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry  = " SELECT IG.*,GC.category_name_en,GC.category_name_ar, GC.file_name_updated AS category_image, COUNT(IG.tbl_image_gallery_id) AS cntGallery FROM ".TBL_IMAGE_GALLERY." AS IG LEFT JOIN ".TBL_GALLERY_CATEGORY." AS GC ON 
		          GC.tbl_gallery_category_id= IG.tbl_gallery_category_id  WHERE 1  ";
		if($tbl_school_id<>""){
			$qry .= " AND IG.tbl_school_id='".$tbl_school_id."'";
		}
		if($tbl_gallery_category_id<>"")
			$qry .= " AND IG.tbl_gallery_category_id='$tbl_gallery_category_id' "; 
	
		
		//Active/Deactive
		if(trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND IG.is_active='$is_active' ";
		}
		$qry .= " AND IG.is_active<>'D' "; 
		
			//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( GC.category_name_en LIKE '%$q%' OR GC.category_name_ar LIKE '%$q%' ) ";
		}
		
		$qry .= " GROUP BY IG.tbl_gallery_category_id ";
		
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY IG.$sort_name $sort_by";
		} else {
			$qry .= " ORDER BY IG.id DESC";
		}
		$qry .=" LIMIT $offset, ".TBL_IMAGE_GALLERY_PAGING;
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
		
      }

      function get_total_school_gallery($q, $is_active, $tbl_school_id, $tbl_gallery_category_id){
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
		
		$qry  = " SELECT IG.tbl_image_gallery_id FROM ".TBL_IMAGE_GALLERY." AS IG LEFT JOIN ".TBL_GALLERY_CATEGORY." AS GC ON 
		          GC.tbl_gallery_category_id= IG.tbl_gallery_category_id  WHERE 1  ";
		if($tbl_school_id<>""){
			$qry .= " AND IG.tbl_school_id='".$tbl_school_id."'";
		}
		if($tbl_gallery_category_id<>"")
			$qry .= " AND IG.tbl_gallery_category_id='$tbl_gallery_category_id' "; 
		
		//Active/Deactive
		if(trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND IG.is_active='$is_active' ";
		}
		$qry .= " AND IG.is_active<>'D' "; 
		
			//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( GC.category_name_en LIKE '%$q%' OR GC.category_name_ar LIKE '%$q%' ) ";
		}
		$qry .= " GROUP BY IG.tbl_gallery_category_id ";
		
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
		
      }

	  function exist_gallery($tbl_image_gallery_id, $tbl_school_id)  {
		$tbl_item_id  	       = $this->cf->get_data($tbl_image_gallery_id);
		$qry = "SELECT * FROM ".TBL_UPLOADS." WHERE 1 ";
		if (trim($tbl_item_id) != "") {
			 $qry .= " AND tbl_item_id='$tbl_item_id' ";
		}
		
	    $qry .= " AND is_active<>'D' "; 
		$results = $this->cf->selectMultiRecords($qry);
		if (count($results)>0) {
				echo "Y";
			} else {
				echo "N";
			}
	}
	  
       /**
	* @desc		Add School Gallery
	* @param	String 
	* @access	default
	* @return	none
	*/
	function add_school_gallery($tbl_item_id,$tbl_gallery_category_id,$is_active,$tbl_school_id)
	{
			$tbl_item_id              = $this->cf->get_data(trim($tbl_item_id));
			$tbl_gallery_category_id  = $this->cf->get_data(trim($tbl_gallery_category_id));
			$is_active                = $this->cf->get_data(trim($is_active));
	        
			$qry = "SELECT * FROM ".TBL_UPLOADS." WHERE 1 ";
			if (trim($tbl_item_id) != "") {
				 $qry .= " AND tbl_item_id='$tbl_item_id' ";
			}
	        $qry .= " AND is_active<>'D' ORDER BY id ASC "; 
		    $results = $this->cf->selectMultiRecords($qry);
			
			for($m=0;$m<count($results);$m++)
			{
			    $tbl_image_gallery_id= md5(uniqid(rand()));
				$tbl_uploads_id      = $results[$m]['tbl_uploads_id'];
				$file_name_original  = $results[$m]['file_name_original'];
				$file_name_updated   = $results[$m]['file_name_updated'];
	
				$qry = "INSERT INTO ".TBL_IMAGE_GALLERY." (`tbl_image_gallery_id`, `tbl_gallery_category_id`, `file_name_original`, `file_name_updated`, `file_name_updated_thumb`, `is_active`, `added_date`, `tbl_school_id`)
						VALUES ('$tbl_image_gallery_id', '$tbl_gallery_category_id', '$file_name_original', '$file_name_updated', '$file_name_updated', '$is_active', NOW(), '$tbl_school_id') ";
				//echo $qry;
				$this->cf->insertInto($qry);
				
				$qryDel = "DELETE FROM ".TBL_UPLOADS." WHERE tbl_uploads_id='$tbl_uploads_id' ";
		        $this->cf->deleteFrom($qryDel);
		
			}
			
	}
	
	
    function get_gallery_info($tbl_image_gallery_id,$tbl_school_id)
	{
		$qry_msg = "SELECT * FROM ".TBL_IMAGE_GALLERY." WHERE tbl_image_gallery_id='$tbl_image_gallery_id' AND  tbl_school_id='$tbl_school_id'"; 
		$rs_msg  = $this->cf->selectMultiRecords($qry_msg);
		return  $rs_msg;
	}
	
	/**
	* @desc		Activate Gallery
	* @access	default
	*/
	function activate_gallery($tbl_image_gallery_id,$tbl_school_id) {
		$tbl_image_gallery_id = $this->cf->get_data(trim($tbl_image_gallery_id));
		$qry = "UPDATE ".TBL_IMAGE_GALLERY." SET is_active='Y' WHERE tbl_image_gallery_id='$tbl_image_gallery_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate Gallery
	* @param	string $tbl_message_group_id
	* @access	default
	*/
	function deactivate_gallery($tbl_image_gallery_id,$tbl_school_id) {
		$tbl_image_gallery_id = $this->cf->get_data(trim($tbl_image_gallery_id));
		$qry = "UPDATE ".TBL_IMAGE_GALLERY." SET is_active='N' WHERE tbl_image_gallery_id='$tbl_image_gallery_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete Gallery
	* @param	string $tbl_message_group_id
	* @access	default
	*/
	function delete_gallery($tbl_image_gallery_id,$tbl_school_id) {
		$tbl_image_gallery_id = $this->cf->get_data(trim($tbl_image_gallery_id));
        $qry = "UPDATE ".TBL_IMAGE_GALLERY." SET is_active='D' WHERE tbl_image_gallery_id='$tbl_image_gallery_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_PARENTING_SCHOOL." WHERE tbl_parenting_school_id='$tbl_parenting_school_id' AND  tbl_school_id='$tbl_school_id'";
		//$this->cf->deleteFrom($qry);
	}
    // END SCHOOL GALLERY
	
	//START SCHOOL GALLERY CATEGORIES

   	function get_gallery_categories_list($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_gallery_category_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = $this->cf->get_data($q);
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry  = " SELECT GC.* FROM ".TBL_GALLERY_CATEGORY." AS GC  WHERE 1  ";
		if($tbl_school_id<>""){
			$qry .= " AND GC.tbl_school_id='".$tbl_school_id."'";
		}
		if($tbl_gallery_category_id<>"")
			$qry .= " AND GC.tbl_gallery_category_id='$tbl_gallery_category_id' "; 
		
		//Active/Deactive
		if(trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND GC.is_active='$is_active' ";
		}
		$qry .= " AND GC.is_active<>'D' "; 
		
		
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY GC.$sort_name $sort_by";
		} else {
			$qry .= " ORDER BY GC.id DESC";
		}
		$qry .=" LIMIT $offset, ".TBL_GALLERY_CATEGORY_PAGING;
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
		
      }

      function get_total_gallery_categories_list($q, $is_active, $tbl_school_id, $tbl_gallery_category_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = $this->cf->get_data($q);
		$is_active  = $this->cf->get_data($is_active);
		
		$qry  = " SELECT GC.tbl_gallery_category_id FROM ".TBL_GALLERY_CATEGORY." AS GC  WHERE 1  ";
		if($tbl_school_id<>""){
			$qry .= " AND GC.tbl_school_id='".$tbl_school_id."'";
		}
		if($tbl_gallery_category_id<>"")
			$qry .= " AND GC.tbl_gallery_category_id='$tbl_gallery_category_id' "; 
		
		//Active/Deactive
		if(trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND GC.is_active='$is_active' ";
		}
		$qry .= " AND GC.is_active<>'D' "; 
		
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
		
      }
	  
	  
	  function is_exist_gallery_category($tbl_gallery_category_id, $category_name_en, $category_name_ar, $tbl_school_id) {
		$tbl_gallery_category_id  	= $this->cf->get_data($tbl_gallery_category_id);
		$category_name_en 		   = $this->cf->get_data($category_name_en);
		$category_name_ar           = $this->cf->get_data($category_name_ar);

		$qry = "SELECT * FROM ".TBL_GALLERY_CATEGORY." WHERE 1 ";
		if (trim($category_name_en) != "") {
			 $qry .= " AND category_name_en='$category_name_en' ";
		}
		
		if (trim($tbl_gallery_category_id) != "") {
			$qry .= " AND tbl_gallery_category_id <> '$tbl_gallery_category_id'";
		}
	    $qry .= " AND is_active<>'D' "; 
		$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}
	  
       /**
	* @desc		Add Gallery Category
	* @param	String tbl_gallery_category_id
	* @access	default
	* @return	none
	*/
	function add_gallery_category($tbl_gallery_category_id,$category_name_en,$category_name_ar,$tbl_school_id)
	{
			$tbl_gallery_category_id      = $this->cf->get_data(trim($tbl_gallery_category_id));
			$category_name_en             = $this->cf->get_data(trim($category_name_en));
			$category_name_ar             = $this->cf->get_data(trim($category_name_ar));
			$tbl_school_id                = $this->cf->get_data(trim($tbl_school_id));
	
			$qry = "INSERT INTO ".TBL_GALLERY_CATEGORY." ( `tbl_gallery_category_id`, `category_name_en`, `category_name_ar`, `added_date`, `tbl_school_id`)
					VALUES ('$tbl_gallery_category_id', '$category_name_en', '$category_name_ar', NOW(), '$tbl_school_id') ";
			//echo $qry;
			$this->cf->insertInto($qry);
			
			$qry = "SELECT * FROM ".TBL_UPLOADS." WHERE 1 ";
			if (trim($tbl_gallery_category_id) != "") {
				 $qry .= " AND tbl_item_id='$tbl_gallery_category_id' ";
			}
	        $qry .= " AND is_active<>'D' "; 
		    $results = $this->cf->selectMultiRecords($qry);
			if(count($results)>0){
				$tbl_uploads_id      = $results[0]['tbl_uploads_id'];
				$file_name_original  = $results[0]['file_name_original'];
				$file_name_updated   = $results[0]['file_name_updated'];
				
				$qryUp = "UPDATE ".TBL_GALLERY_CATEGORY." SET file_name_updated= '$file_name_updated' WHERE tbl_gallery_category_id='$tbl_gallery_category_id' AND  tbl_school_id='$tbl_school_id' ";
		        $this->cf->update($qryUp);
				
				//$qryDel = "DELETE FROM ".TBL_UPLOADS." WHERE tbl_uploads_id='$tbl_uploads_id' ";
		       // $this->cf->deleteFrom($qryDel);
			}
			
	}
	
    function get_school_gallery_category($tbl_gallery_category_id,$tbl_school_id)
	{
		$qry_msg = "SELECT GC.*, U.tbl_uploads_id FROM ".TBL_GALLERY_CATEGORY." AS GC LEFT JOIN ".TBL_UPLOADS."  AS U ON U.tbl_item_id = GC.tbl_gallery_category_id  
		            WHERE GC.tbl_gallery_category_id='$tbl_gallery_category_id' AND  GC.tbl_school_id='$tbl_school_id'"; 
		$rs_msg  = $this->cf->selectMultiRecords($qry_msg);
		return  $rs_msg;
	}

	
	
	function update_school_gallery_category($tbl_gallery_category_id,$category_name_en,$category_name_ar,$tbl_school_id)
	{
			$tbl_gallery_category_id      = $this->cf->get_data(trim($tbl_gallery_category_id));
			$category_name_en             = $this->cf->get_data(trim($category_name_en));
			$category_name_ar             = $this->cf->get_data(trim($category_name_ar));
			$tbl_school_id                = $this->cf->get_data(trim($tbl_school_id));
			
			$qry_update  = "UPDATE ".TBL_GALLERY_CATEGORY." SET category_name_en='$category_name_en', category_name_ar='$category_name_ar'
							WHERE tbl_gallery_category_id='$tbl_gallery_category_id' AND tbl_school_id= '$tbl_school_id' ";
			$this->cf->update($qry_update);
			
			$qry = "SELECT * FROM ".TBL_UPLOADS." WHERE 1 ";
			if (trim($tbl_gallery_category_id) != "") {
				 $qry .= " AND tbl_item_id='$tbl_gallery_category_id' ";
			}
	        $qry .= " AND is_active<>'D' ORDER BY added_date DESC "; 
			echo $qry ;
			
		    $results = $this->cf->selectMultiRecords($qry);
			
			if(count($results)>0){
				$tbl_uploads_id      = $results[0]['tbl_uploads_id'];
				$file_name_original  = $results[0]['file_name_original'];
				$file_name_updated   = $results[0]['file_name_updated'];
				
				$qry = "UPDATE ".TBL_GALLERY_CATEGORY." SET file_name_updated= '$file_name_updated' WHERE tbl_gallery_category_id='$tbl_gallery_category_id' AND tbl_school_id='$tbl_school_id' ";
		        
				echo "case1: ".$qry;
				$this->cf->update($qry);
				
				//$qryDel = "DELETE FROM ".TBL_UPLOADS." WHERE tbl_uploads_id='$tbl_uploads_id' ";
		        //$this->cf->deleteFrom($qryDel); 
			} else {
				$qry = "UPDATE ".TBL_GALLERY_CATEGORY." SET file_name_updated= '' WHERE tbl_gallery_category_id='$tbl_gallery_category_id' AND tbl_school_id='$tbl_school_id' ";
				echo "case2: ".$qry;

		        $this->cf->update($qry);
			}
	}
	
	/**
	* @desc		Activate School galleries Category
	* @access	default
	*/
	function activate_school_gallery_category($tbl_gallery_category_id,$tbl_school_id) {
		$tbl_gallery_category_id = $this->cf->get_data(trim($tbl_gallery_category_id));
		$qry = "UPDATE ".TBL_GALLERY_CATEGORY." SET is_active='Y' WHERE tbl_gallery_category_id='$tbl_gallery_category_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate School galleries
	* @param	string $tbl_message_group_id
	* @access	default
	*/
	function deactivate_school_gallery_category($tbl_gallery_category_id,$tbl_school_id) {
		$tbl_gallery_category_id = $this->cf->get_data(trim($tbl_gallery_category_id));
		$qry = "UPDATE ".TBL_GALLERY_CATEGORY." SET is_active='N' WHERE tbl_gallery_category_id='$tbl_gallery_category_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete School galleris
	* @param	string $tbl_message_group_id
	* @access	default
	*/
	function delete_school_gallery_category($tbl_gallery_category_id,$tbl_school_id) {
		$tbl_gallery_category_id = $this->cf->get_data(trim($tbl_gallery_category_id));
        $qry = "UPDATE ".TBL_GALLERY_CATEGORY." SET is_active='D' WHERE tbl_gallery_category_id='$tbl_gallery_category_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_PARENTING_SCHOOL." WHERE tbl_parenting_school_id='$tbl_parenting_school_id' AND  tbl_school_id='$tbl_school_id'";
		//$this->cf->deleteFrom($qry);
	}
	
	function update_gallery_category_order($sectionids) {
		
		$count = 1;
        if (is_array($sectionids)) {
            foreach ($sectionids as $sectionid) {
                $query  = "Update ".TBL_GALLERY_CATEGORY."  SET gallery_category_order = $count";
                $query .= " WHERE tbl_gallery_category_id='".$sectionid."'";
				$this->cf->update($query);
                $count++;
            }
           return 1;
        } else {
           return 0;
        }
	}
	
	

	/**
	* @desc		Delete School galleris
	* @param	string $tbl_message_group_id
	* @access	default
	*/
	function delete_gallery_category_image($tbl_gallery_category_id) {
		$tbl_gallery_category_id = $this->cf->get_data(trim($tbl_gallery_category_id));
		
        $qry = "UPDATE ".TBL_GALLERY_CATEGORY." SET file_name_updated='' WHERE tbl_gallery_category_id='$tbl_gallery_category_id' "; 
		$this->cf->update($qry);
		
        $qry = "DELETE FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_gallery_category_id' "; 
		$this->cf->update($qry);
	}
	
	
	//END SCHOOL GALLERY CATEGORIES
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	

    }

?>



