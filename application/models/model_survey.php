<?php
include_once('include/common_functions.php');
/**
 * @desc   	  	Survey Model
 * @category   	Model
 * @author     	Shanavas.PK
 * @version    	0.1
 */

class Model_survey extends CI_Model {
	var $cf;
	/**
	* @desc Default constructor for the Controller
	* @access default
	*/

    function model_survey() {
		$this->cf = new Common_functions();
    }

	/**
	* @desc Function to return survey questions
	* @access default
	*/

    function get_survey_questions($questions_per_page, $survey_page_no, $tbl_school_id) {
		$offset = $questions_per_page *  $survey_page_no;
        //$qry = "SELECT * FROM ".TBL_SURVEY_QUESTIONS." WHERE is_active='Y' ORDER BY question_order ASC LIMIT ".$offset.",".$questions_per_page;
		$qry = "SELECT * FROM ".TBL_SURVEY_QUESTIONS." WHERE is_active='Y' AND tbl_school_id='$tbl_school_id' ORDER BY question_order ASC ";
		//echo "----->".$qry;
		$data_rs = $this->cf->SelectMultiRecords($qry);
		return $data_rs;
	}

	/**
	* @desc Function to return options against a given question
	* @access default
	*/

    function get_survey_options($tbl_survey_questions_id) {
        $qry = "SELECT * FROM ".TBL_SURVEY_OPTIONS." WHERE is_active='Y' AND tbl_survey_questions_id='$tbl_survey_questions_id' ORDER BY option_order ASC";
		//echo "----->".$qry;
		$data_rs = $this->cf->SelectMultiRecords($qry);
		return $data_rs;
	}

	/**
	* @desc Function to save survey options selected by user along with his name and email address
	* @access default
	*/

    function save_survey_options($survey_options_str, $tbl_parent_id, $tbl_school_id) {
		$tbl_user_id = $this->cf->getMD5_Id();
		$survey_options_arr = explode(",", $survey_options_str);
		for ($i=0; $i<count($survey_options_arr); $i++) {
			$tbl_survey_options_id = $survey_options_arr[$i];
			if (trim($tbl_survey_options_id) == "") {
				continue;
			}

			$qry = "SELECT tbl_survey_questions_id FROM ".TBL_SURVEY_OPTIONS." WHERE tbl_survey_options_id ='".$tbl_survey_options_id."'";
			//echo $qry;
			//return;
			$data_quest = $this->cf->SelectMultiRecords($qry);
			$tbl_survey_questions_id = $data_quest[0]["tbl_survey_questions_id"];

			$qry = "INSERT INTO ".TBL_SURVEY_ANSWERS." (`tbl_user_id` ,`tbl_parent_id` ,`name` ,`email` ,`tbl_survey_options_id` ,`tbl_survey_questions_id` ,`is_active` ,`added_date` ,`tbl_school_id`)
					VALUES ('$tbl_user_id', '$tbl_parent_id', '$name', '$email', '$tbl_survey_options_id', '$tbl_survey_questions_id', 'Y', NOW(), '$tbl_school_id')";
			$this->cf->insertInto($qry);
		}
		return;
	}

	/**
	* @desc Function to return survey options
	* @access default
	*/

    function get_survey_answers($tbl_user_id, $tbl_survey_questions_id) {
        $qry = "SELECT * FROM ".TBL_SURVEY_ANSWERS." WHERE is_active='Y' AND tbl_user_id='$tbl_user_id' AND tbl_survey_questions_id='$tbl_survey_questions_id'";
		$data_rs = $this->cf->SelectMultiRecords($qry);

		for ($i=0; $i<count($data_rs); $i++) {
			$tbl_survey_options_id = $data_rs[$i]["tbl_survey_options_id"];
			$options_str           = $options_str."'".$tbl_survey_options_id."',";
		}

		$options_str = substr($options_str,0,-1);	// Remove last ,
		$qry = "SELECT * FROM ".TBL_SURVEY_OPTIONS." WHERE tbl_survey_options_id IN (".$options_str.")";
		//echo $options_str;
		$data_rs = $this->cf->SelectMultiRecords($qry);

		for ($i=0; $i<count($data_rs); $i++) {
			$option_text = $data_rs[$i]["option_text"];
			$options_text_str = $options_text_str.$option_text."<br>";
		}

		return $options_text_str;
	}

	/**
	* @desc Function to get questions count
	* @access default
	*/	

	function get_questions_count() {
        $qry = "SELECT COUNT(*) AS cnt FROM ".TBL_SURVEY_QUESTIONS." WHERE is_active='Y'";
		$data_rs = $this->cf->SelectMultiRecords($qry);
		return $data_rs[0]["cnt"];
	}
	
	
	// BACKEND FUNCTIONALITY
		/**
	* @desc		Get all survey
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function get_all_survey($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry = "SELECT * FROM ".TBL_SURVEY_QUESTIONS." AS SQ WHERE 1 ";
		$qry .= " AND SQ.tbl_school_id= '".$tbl_school_id."' ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND SQ.is_active='$is_active' ";
		}
		$qry .= " AND SQ.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( SQ.question_text_en LIKE '%$q%' OR SQ.question_text_ar LIKE '%$q%' ) ";
		}
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY SQ.id DESC ";
		}
		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_SURVEY_QUESTIONS_PAGING;
		}
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	/**
	* @desc		Get total survey
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_survey($q, $is_active, $tbl_school_id) {
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry =  "SELECT COUNT(distinct(SQ.tbl_survey_questions_id)) as total_questions FROM ".TBL_SURVEY_QUESTIONS." AS SQ WHERE 1 ";
		$qry .= " AND SQ.tbl_school_id= '".$tbl_school_id."' ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND SQ.is_active='$is_active' ";
		}
		$qry .= " AND SQ.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( SQ.question_text_en LIKE '%$q%' OR SQ.question_text_ar LIKE '%$q%' ) ";
		}
		$results = $this->cf->selectMultiRecords($qry);
	    return $results[0]['total_questions'];
	}
	

	function is_exist_survey_question($tbl_survey_questions_id, $question_text_en, $tbl_school_id) {
		$tbl_survey_questions_id  	    = $this->cf->get_data($tbl_survey_questions_id);
		$question_text_en 		       = $this->cf->get_data($question_text_en);
		$tbl_school_id                  = $this->cf->get_data($tbl_school_id);

		$qry = "SELECT * FROM ".TBL_SURVEY_QUESTIONS." AS SQ WHERE 1 ";
		if (trim($question_text_en) != "") {
			 $qry .= " AND SQ.question_text_en='$question_text_en' ";
		}
		if (trim($tbl_survey_questions_id) != "") {
			 $qry .= " AND SQ.tbl_survey_questions_id<>'$tbl_survey_questions_id' ";
		}
	    $qry .= " AND SQ.is_active<>'D' "; 
		$qry .= " AND SQ.tbl_school_id = '".$tbl_school_id."' ";
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}
	
	
	/**
	* @desc		Add survey question
	* @param	String params
	* @access	default
	*/
	function add_survey_question($tbl_survey_questions_id, $question_text_en, $question_text_ar, $option_1_en, $option_1_ar, $option_2_en, $option_2_ar, 
		                                           $option_3_en, $option_3_ar, $option_4_en, $option_4_ar, $tbl_school_id)
    {
		$tbl_survey_questions_id      = $this->cf->get_data($tbl_survey_questions_id);
		$question_text_en        	 = $this->cf->get_data($question_text_en);
		$question_text_ar             = $this->cf->get_data($question_text_ar);
		$option_1_en                  = $this->cf->get_data($option_1_en);
		$option_1_ar                  = $this->cf->get_data($option_1_ar);
		$option_2_en                  = $this->cf->get_data($option_2_en);
		$option_2_ar                  = $this->cf->get_data($option_2_ar);
		$option_3_en                  = $this->cf->get_data($option_3_en);
		$option_3_ar                  = $this->cf->get_data($option_3_ar);
		$option_4_en                  = $this->cf->get_data($option_4_en);
		$option_4_ar                  = $this->cf->get_data($option_4_ar);
		$tbl_school_id                = $this->cf->get_data($tbl_school_id);
		
		if($tbl_school_id<>"")
		{
			$qry_ins = "INSERT INTO ".TBL_SURVEY_QUESTIONS." (`tbl_survey_questions_id`, `question_text_en`, `question_text_ar`, `added_date`, `is_active`, `tbl_school_id`)
						VALUES ('$tbl_survey_questions_id', '$question_text_en', '$question_text_ar', NOW(), 'Y', '$tbl_school_id')";
			$this->cf->insertInto($qry_ins);
			
		    for ($i=1; $i<5; $i++) {
				$tbl_survey_options_id = md5(uniqid(rand()));	
				$tbl_survey_options_id = substr($tbl_survey_options_id,0,10);
				
				if($i=="1")
				{
				   $option_text_en   = $option_1_en;
				   $option_text_ar   = $option_1_ar;
				}
				if($i=="2")
				{
				   $option_text_en   = $option_2_en;
				   $option_text_ar   = $option_2_ar;
				}if($i=="3")
				{
				   $option_text_en   = $option_3_en;
				   $option_text_ar   = $option_3_ar;
				}if($i=="4")
				{
				   $option_text_en   = $option_4_en;
				   $option_text_ar   = $option_4_ar;
				}
	
				$qry = "INSERT INTO ".TBL_SURVEY_OPTIONS." 
						   (`tbl_survey_options_id` ,`tbl_survey_questions_id` ,`option_text_en` ,`option_text_ar`  ,`option_order` ,`is_active` ,`added_date`,`tbl_school_id`)
							VALUES ('$tbl_survey_options_id', '$tbl_survey_questions_id', '$option_text_en', '$option_text_ar', '1000', 'Y', NOW(), '$tbl_school_id')";
				$this->cf->insertInto($qry);
			}
			return "Y";
		}else{
		   return "N";	
			
		}
	}
	
	/**
	* @desc		Activate survey
	* @param	string $tbl_event_id
	* @access	default
	*/
	function activate_survey_question($tbl_survey_questions_id,$tbl_school_id) {
		$tbl_survey_questions_id = $this->cf->get_data(trim($tbl_survey_questions_id));
		$qry = "UPDATE ".TBL_SURVEY_QUESTIONS." SET is_active='Y' WHERE tbl_survey_questions_id='$tbl_survey_questions_id' AND  tbl_school_id='$tbl_school_id' ";
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate event
	* @param	string $tbl_student_id
	* @access	default
	*/
	function deactivate_survey_question($tbl_survey_questions_id,$tbl_school_id) {
		$tbl_survey_questions_id = $this->cf->get_data(trim($tbl_survey_questions_id));
		$qry = "UPDATE ".TBL_SURVEY_QUESTIONS." SET is_active='N' WHERE tbl_survey_questions_id='$tbl_survey_questions_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete event
	* @param	string $tbl_student_id
	* @access	default
	*/
	function delete_survey_question($tbl_survey_questions_id,$tbl_school_id) {
		$tbl_survey_questions_id = $this->cf->get_data(trim($tbl_survey_questions_id));
        $qry = "UPDATE ".TBL_SURVEY_QUESTIONS." SET is_active='D' WHERE tbl_survey_questions_id='$tbl_survey_questions_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_CATEGORY." WHERE tbl_category_id='$tbl_category_id' ";
		//$this->cf->deleteFrom($qry);
	}
	
	
	
	function get_survey_question_obj($tbl_survey_questions_id,$tbl_school_id) {
		$qry = "SELECT * FROM ".TBL_SURVEY_QUESTIONS." WHERE 1  AND tbl_survey_questions_id='$tbl_survey_questions_id' AND  tbl_school_id='$tbl_school_id'";
		$results = $this->cf->selectMultiRecords($qry);
		$questions_obj = array();
		$questions_obj['tbl_survey_questions_id']      = $results[0]['tbl_survey_questions_id'];
		$questions_obj['question_text_en']             = trim($results[0]['question_text_en']);
		$questions_obj['question_text_ar']             = trim($results[0]['question_text_ar']);
		$questions_obj['added_date']                   = $results[0]['added_date'];
	    return $questions_obj;
	}
	
	// SURVEY QUESTION OPTIONS
	function get_survey_question_option_obj($tbl_survey_questions_id,$tbl_school_id) {
		$qryOption      = "SELECT * FROM ".TBL_SURVEY_OPTIONS." WHERE 1 AND tbl_survey_questions_id='$tbl_survey_questions_id' AND  tbl_school_id='$tbl_school_id'";
		$resultsOption = $this->cf->selectMultiRecords($qryOption);
		$j = 1;
		for($i=0;$i<count($resultsOption);$i++)
		{
			$results['option_'.$j.'_id'] = $resultsOption[$i]['tbl_survey_options_id'];
			$results['option_'.$j.'_en'] = $resultsOption[$i]['option_text_en'];
			$results['option_'.$j.'_ar'] = $resultsOption[$i]['option_text_ar'];
			$j = $j+1;
		}
		return $results;
	}
	
	
	function get_survey_question_option_cnt($tbl_survey_questions_id,$tbl_school_id) {
		$qryOption      = "SELECT * FROM ".TBL_SURVEY_OPTIONS." WHERE 1 AND tbl_survey_questions_id='$tbl_survey_questions_id' AND  tbl_school_id='$tbl_school_id'";
		$resultsOption = $this->cf->selectMultiRecords($qryOption);
		for($i=0;$i<count($resultsOption);$i++)
		{
			$option_id                = $resultsOption[$i]['tbl_survey_options_id'];
			$results[$i]['option_en'] = $resultsOption[$i]['option_text_en'];
			$results[$i]['option_ar'] = $resultsOption[$i]['option_text_ar'];
			
			$option_count  = $this->get_option_cnt($option_id,$tbl_school_id);
			$results[$i]['option_count'] = $option_count;
		}
		return $results;
	}
	
	
	function get_option_cnt($option_id,$tbl_school_id) 
	{
	      $qryCnt = "SELECT COUNT(*) AS cnt FROM ".TBL_SURVEY_ANSWERS." WHERE tbl_survey_options_id='".$option_id."' AND is_active='Y'";
          $resultsOption   = $this->cf->selectMultiRecords($qryCnt);
		  $option_count    = $resultsOption[0]["cnt"];
		  return $option_count;
	}
	
	

	function save_survey_question_changes($tbl_survey_questions_id, $question_text_en, $question_text_ar, $option_1_id, $option_1_en, $option_1_ar, $option_2_id, $option_2_en, $option_2_ar, $option_3_id, $option_3_en, $option_3_ar, $option_4_id, $option_4_en, $option_4_ar, $tbl_school_id)
	{
	    $tbl_survey_questions_id      = $this->cf->get_data($tbl_survey_questions_id);
		$question_text_en        	 = $this->cf->get_data($question_text_en);
		$question_text_ar             = $this->cf->get_data($question_text_ar);
		
		$option_1_id                  = $this->cf->get_data($option_1_id);
		$option_1_en                  = $this->cf->get_data($option_1_en);
		$option_1_ar                  = $this->cf->get_data($option_1_ar);
		
		$option_2_id                  = $this->cf->get_data($option_2_id);
		$option_2_en                  = $this->cf->get_data($option_2_en);
		$option_2_ar                  = $this->cf->get_data($option_2_ar);
		
		$option_3_id                  = $this->cf->get_data($option_3_id);
		$option_3_en                  = $this->cf->get_data($option_3_en);
		$option_3_ar                  = $this->cf->get_data($option_3_ar);
		
		$option_4_id                  = $this->cf->get_data($option_4_id);
		$option_4_en                  = $this->cf->get_data($option_4_en);
		$option_4_ar                  = $this->cf->get_data($option_4_ar);
		$tbl_school_id                = $this->cf->get_data($tbl_school_id);
		
		if($tbl_school_id<>"")
		{
			$qry  = "UPDATE ".TBL_SURVEY_QUESTIONS." SET question_text_en='$question_text_en', question_text_ar='$question_text_ar' WHERE tbl_survey_questions_id = '$tbl_survey_questions_id' and tbl_school_id = '$tbl_school_id' ";
			$this->cf->update($qry);
			
		     for ($i=1; $i<5; $i++) {
			
				if($i=="1")
				{
				   $tbl_survey_options_id = $option_1_id;
				   $option_text_en        = $option_1_en;
				   $option_text_ar        = $option_1_ar;
				}
				if($i=="2")
				{
				   $tbl_survey_options_id = $option_2_id;
				   $option_text_en        = $option_2_en;
				   $option_text_ar        = $option_2_ar;
				}if($i=="3")
				{
				   $tbl_survey_options_id = $option_3_id;
				   $option_text_en        = $option_3_en;
				   $option_text_ar        = $option_3_ar;
				}if($i=="4")
				{
				   $tbl_survey_options_id = $option_4_id;
				   $option_text_en        = $option_4_en;
				   $option_text_ar        = $option_4_ar;
				}
				
				if($tbl_survey_options_id<>"")
				{
					$qry  = "UPDATE ".TBL_SURVEY_OPTIONS." SET option_text_en='$option_text_en', option_text_ar='$option_text_ar' WHERE tbl_survey_questions_id = '$tbl_survey_questions_id' and tbl_school_id = '$tbl_school_id' AND  tbl_survey_options_id='$tbl_survey_options_id' ";
					$this->cf->update($qry);
				}
			}
			
		    return "Y";
		}else{
		   return "N";	
			
		}
	}
	
	//END BACKEND FUNCTIONALITY
	
	function is_already_attended($tbl_user_id,$tbl_survey_questions_id)
	{
		$sql4AttendSurvey = "SELECT tbl_parent_id from " . TBL_SURVEY_ANSWERS . " where tbl_survey_questions_id='$tbl_survey_questions_id' and tbl_parent_id='$tbl_user_id'";
		if ($this->cf->isExist($sql4AttendSurvey)){
			return "Y";
		}else{	
			return "N";
		}
	}
	
	
	

    }

?>