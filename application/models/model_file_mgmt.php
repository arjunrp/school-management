<?php
include_once('include/common_functions.php');

/**
 * @desc   	  	File Management Model
 *
 * @category   	Model
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Model_file_mgmt extends CI_Model {
	var $cf;

	/**
	* @desc Default constructor for the Controller
	*
	* @access default
	*/
    function model_file_mgmt() {
		$this->cf = new Common_functions();
    }


	/**
	* @desc		Save file
	* @param	String $tbl_uploads_id, $module_name, $item_id, $file_name_original, $file_name_updated, $file_type, $file_size
	* @access	default
	*/
	function save_file($tbl_uploads_id, $module_name, $item_id, $file_name_original, $file_name_updated, $file_type, $file_size) {
		$tbl_uploads_id = $this->cf->get_data(trim($tbl_uploads_id));
		$module_name    = $this->cf->get_data(trim($module_name));
		$item_id        = $this->cf->get_data(trim($item_id));
		$file_name_original  = $this->cf->get_data(trim($file_name_original));
		$file_name_updated   = $this->cf->get_data(trim($file_name_updated));
		$file_type      = $this->cf->get_data(trim($file_type));
		$file_size      = $this->cf->get_data(trim($file_size));
		/*if($module_name=="student")
		{
			$qry = "SELECT * FROM ".TBL_UPLOADS." WHERE `tbl_item_id`='$item_id' ";
			$results = $this->cf->selectMultiRecords($qry);
			if(count($results)>0)
			{
				$file_name_updated = $results[0]['file_name_updated'];
				unlink(STUDENT_IMG_UPLOAD_PATH."/".$file_name_updated);
			
				$delQuery      = " UPDATE FROM ".TBL_UPLOADS." where tbl_item_id = '".$item_id."' ";
		    	$this->cf->deleteFrom($delQuery);
				$sql4Update = "UPDATE " . TBL_UPLOADS . " SET module_name='".$module_name."',  file_name_original='".$file_name_original."', 
								file_name_updated='".$file_name_updated."',  file_type='".$file_type."', file_size='".$file_size."',
								WHERE item_id = '".$item_id."'" ;
				$this->cf->update($sql4Update);
			}else{
				
				$qry = "INSERT INTO ".TBL_UPLOADS." (`tbl_uploads_id`, `module_name`, `tbl_item_id`, `file_name_original`, `file_name_updated`, `file_type`, `file_size`, `is_active`, `added_date`)
				VALUES ('$tbl_uploads_id', '$module_name', '$item_id', '$file_name_original', '$file_name_updated', '$file_type', '$file_size', 'Y', NOW() )";
			}
		}else{*/
			$qry = "INSERT INTO ".TBL_UPLOADS." (`tbl_uploads_id`, `module_name`, `tbl_item_id`, `file_name_original`, `file_name_updated`, `file_type`, `file_size`, `is_active`, `added_date`)
				VALUES ('$tbl_uploads_id', '$module_name', '$item_id', '$file_name_original', '$file_name_updated', '$file_type', '$file_size', 'Y', NOW() )";
		//}
		//echo $qry."<br />";
		$this->cf->insertInto($qry);
	}


	/**
	* @desc		Delete file
	* 
	* @param	String $tbl_uploads_id 
	* @access	default
	*/
	function delete_file($tbl_uploads_id) {
		$tbl_uploads_id = $this->cf->get_data(trim($tbl_uploads_id));
		
		$qry = "DELETE FROM ".TBL_UPLOADS." WHERE tbl_uploads_id='$tbl_uploads_id' ";
		//echo $qry."<br />";
				
		$this->cf->deleteFrom($qry);
	}


	/**
	* @desc		Get file object
	* 
	* @param	String $item_id 
	* @access	default
	*/
	function get_file_mgmt_obj($item_id) {
		$tbl_uploads_id =$this->cf->get_data(trim($item_id));
		
		$qry = "SELECT * FROM ".TBL_UPLOADS." WHERE `tbl_item_id`='$item_id' ";
		//echo $qry;
		$results = $this->cf->selectMultiRecords($qry);
		
		$file_mgmt_obj = array();
		
		if (count($results)>0) { 
			$file_mgmt_obj['tbl_uploads_id'] = $results[0]['tbl_uploads_id'];
			$file_mgmt_obj['module_name'] = $results[0]['module_name'];
			$file_mgmt_obj['item_id'] = $results[0]['tbl_item_id'];
			$file_mgmt_obj['file_name_original'] = $results[0]['file_name_original'];
			$file_mgmt_obj['file_name_updated'] = $results[0]['file_name_updated'];
			$file_mgmt_obj['file_type'] = $results[0]['file_type'];
			$file_mgmt_obj['file_size'] = $results[0]['file_size'];
			$file_mgmt_obj['added_date'] = $results[0]['added_date'];
			$file_mgmt_obj['is_active'] = $results[0]['is_active'];
		}
	return $file_mgmt_obj;
	}
	

}
?>