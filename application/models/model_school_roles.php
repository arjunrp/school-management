<?php
include_once('include/common_functions.php');
/**
 * @desc   	  	School Roles Model
 * @category   	Model
 * @author     	Shanavas.PK
 * @version    	0.1
 */

class Model_school_roles extends CI_Model {
	var $cf;
	
	/**
	* @desc Default constructor for the Controller
	* @access default
	*/
    function model_school_roles() {
		$this->cf = new Common_functions();
    }

	/**
	* @desc		Get roles details
	* @param	none 	
	* @access	default
	* @return	array $rs
	*/
	function get_school_roles_obj($tbl_school_roles_id, $tbl_school_id) {
		$qry_sel = "SELECT * FROM ".TBL_SCHOOL_ROLES." WHERE tbl_school_roles_id='$tbl_school_roles_id' AND tbl_school_id='$tbl_school_id'";
		//echo $qry_sel;
		$rs = $this->cf->selectMultiRecords($qry_sel);
	return $rs;	
	}
	
	//BACKEND FUNCTIONALITY
	
	/**
	* @desc		Get all school roles
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function get_all_school_roles($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = $this->cf->get_data($q);
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		$qry = "SELECT * FROM ".TBL_SCHOOL_ROLES."  WHERE 1 ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND is_active='$is_active' ";
		}
		$qry .= " AND is_active<>'D' "; 
		//Search
		if (trim($q) != "") {
			$qry .= " AND (role LIKE '%$q%' || role_ar LIKE '%$q%')";
		} 
		
		$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY role ASC ";
		}
		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_SCHOOL_ROLES_PAGING;
		}
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}

	
	/**
	* @desc		Get total roles
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_school_roles($q, $is_active, $tbl_school_id) {
		$q         = $this->cf->get_data($q);
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT COUNT(*) as total_roles FROM ".TBL_SCHOOL_ROLES." WHERE 1 ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND is_active='$is_active' ";
		}
		$qry .= " AND is_active<>'D' "; 
		
		$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		//Search
		if (trim($q) != "") {
			$qry .= " AND (role LIKE '%$q%' || role_ar LIKE '%$q%')";
		} 
		//echo $qry;
		$results = $this->cf->selectMultiRecords($qry);
		
	return $results[0]['total_roles'];
	}
	
	
	function is_exist_role($tbl_school_roles_id, $role, $role_ar, $tbl_school_id) {
		
		$tbl_school_roles_id  	= $this->cf->get_data($tbl_school_roles_id);
		$role 		           = $this->cf->get_data($role);
		$role_ar                = $this->cf->get_data($role_ar);

		$qry = "SELECT * FROM ".TBL_SCHOOL_ROLES." WHERE 1 ";
		if (trim($role) != "") {
			 $qry .= " AND role='$role' ";
		}
		
		if (trim($tbl_school_roles_id) != "") {
			$qry .= " AND tbl_school_roles_id <> '$tbl_school_roles_id'";
		}
	    $qry .= " AND is_active<>'D' "; 
		$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}
	
	/**
	* @desc		Create school role
	* @param	String params
	* @access	default
	*/
	function create_school_role($tbl_school_roles_id, $role, $role_ar, $tbl_school_id) {
		$tbl_school_roles_id = $this->cf->get_data($tbl_school_roles_id);
		$role                = $this->cf->get_data($role);
		$role_ar             = $this->cf->get_data($role_ar);
		if($tbl_school_id<>"")
		{
			$qry_ins = "INSERT INTO ".TBL_SCHOOL_ROLES." (`tbl_school_roles_id`, `role`, `role_ar`, `added_date`, `is_active`, `tbl_school_id`)
						VALUES ('$tbl_school_roles_id', '$role', '$role_ar', NOW(), 'Y', '$tbl_school_id')";
			$this->cf->insertInto($qry_ins);
			return "Y";
		}else{
		   return "N";	
			
		}
	}
	
	/**
	* @desc		Activate
	* @param	string 
	* @access	default
	*/
	function activate_role($school_roles_id_enc,$tbl_school_id) {
		$tbl_school_roles_id = $this->cf->get_data(trim($school_roles_id_enc));
		$qry = "UPDATE ".TBL_SCHOOL_ROLES." SET is_active='Y' WHERE tbl_school_roles_id='$tbl_school_roles_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate
	* @param	string $school_roles_id_enc
	* @access	default
	*/
	function deactivate_role($school_roles_id_enc,$tbl_school_id) {
		$tbl_school_roles_id = $this->cf->get_data(trim($school_roles_id_enc));
		$qry = "UPDATE ".TBL_SCHOOL_ROLES." SET is_active='N' WHERE tbl_school_roles_id='$tbl_school_roles_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete
	* @param	string $tbl_school_type_id
	* @access	default
	*/
	function delete_role($school_roles_id_enc,$tbl_school_id) {
		$tbl_school_roles_id = $this->cf->get_data(trim($school_roles_id_enc));
        $qry = "UPDATE ".TBL_SCHOOL_ROLES." SET is_active='D' WHERE tbl_school_roles_id='$tbl_school_roles_id' AND  tbl_school_id='$tbl_school_id'";
		echo $qry;
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_CATEGORY." WHERE tbl_category_id='$tbl_category_id' ";
		//$this->cf->deleteFrom($qry);
	}
	
	/**
	* @desc		Get a role object based id
	* @param	string $tbl_school_roles_id
	* @access	default
	*/
	function get_school_role_obj($tbl_school_roles_id) {
		$qry = "SELECT * FROM ".TBL_SCHOOL_ROLES." WHERE 1  AND tbl_school_roles_id='$tbl_school_roles_id'";
		$results = $this->cf->selectMultiRecords($qry);
		$school_role_obj = array();
		$school_role_obj['tbl_school_roles_id'] = $results[0]['tbl_school_roles_id'];
		$school_role_obj['role']                = $results[0]['role'];
		$school_role_obj['role_ar']             = $results[0]['role_ar'];
		$school_role_obj['added_date']          = $results[0]['added_date'];
		$school_role_obj['is_active']           = $results[0]['is_active'];
		
	return $school_role_obj;
	}

	/**
	* @desc		Save changes
	* @param	String params
	* @access	default
	*/
	function save_role_changes($tbl_school_roles_id, $role, $role_ar, $tbl_school_id) {
		$tbl_school_roles_id        = $this->cf->get_data($tbl_school_roles_id);
		$role                       = $this->cf->get_data($role);
		$role_ar                    = $this->cf->get_data($role_ar);
		$tbl_school_id              = $this->cf->get_data($tbl_school_id);

		$qry_update = "UPDATE ".TBL_SCHOOL_ROLES." SET role='$role', role_ar='$role_ar' "; 
		$qry_update .= " WHERE tbl_school_roles_id='$tbl_school_roles_id' AND  tbl_school_id='$tbl_school_id'  " ;
		$this->cf->update($qry_update);
	}
	
	//end backend functionality
	
	
	

}

?>



