<?php
include_once('include/common_functions.php');

/**
 * @desc   	  	Section Model
 *
 * @category   	Model
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Model_section extends CI_Model {
	var $cf;


	/**
	* @desc Default constructor for the Controller
	*
	* @access default
	*/
    function model_section() {
		$this->cf = new Common_functions();
    }


	/**
	* @desc		Get event details
	* 
	* @param	none 	
	* @access	default
	* @return	array $rs
	*/
	function get_section_obj($tbl_section_id) {
		$qry_sel = "SELECT * FROM ".TBL_SECTION." WHERE tbl_section_id='$tbl_section_id' ";
		//echo $qry_sel;
		$rs = $this->cf->selectMultiRecords($qry_sel);
	return $rs;	
	}
    }
?>

