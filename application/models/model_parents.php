<?php
include_once('include/common_functions.php');
/**
* @desc   	  	Parents Model
* @category   	Model
* @author     	Shanavas PK
* @version    	0.1
*/

class Model_parents extends CI_Model {
	var $cf;
	
	/**
	* @desc Default constructor for the Controller
	* @access default
	*/

	function model_parents() {
		$this->cf = new Common_functions();
	}

	/**
	* @desc		Get parent id
	* @param	string $email  
	* @access	default
	* @return	$tbl_parent_id
	*/

	function get_parent_id($email) {
		$email = $this->cf->get_data(trim($email));
		$qry = "SELECT tbl_parent_id FROM ".TBL_PARENT." WHERE user_id='$email' ";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs[0]['tbl_parent_id'];
	}


	/**
	* @desc		Check for user record in database and validate with user credentils
	* @param	string $email, string $password 
	* @access	default
	* @return	Y:valid email/password, N:invalid email/password, D:user does not exist
	*/

	function authenticate($email, $password, $is_approved="") {
		$email    = $this->cf->get_data(trim($email));
		$password = $this->cf->get_data(trim($password));
		$qry = "SELECT tbl_parent_id, password, salt FROM ".TBL_PARENT." WHERE user_id='$email' AND is_active='Y' ";
		if ($is_approved != "") {
			$qry .= " AND is_approved='".$is_approved."'";
		}
		$result = $this->cf->selectMultiRecords($qry);
		if ($result) {
			$salt = $result[0]['salt'];
			$db_password = $result[0]['password'];
		} else {
			return "D";//User does not exists
		}

		$hash = sha1($salt . sha1($password));
		//echo $hash;
		if (trim($hash) != trim($db_password)) {                
			return "N";
		} else { 
		    $tbl_parent_id = $result[0]['tbl_parent_id'];
		    $qry_update = "UPDATE ".TBL_PARENT." SET is_logged='Y' WHERE tbl_parent_id='$tbl_parent_id' ";
			$this->cf->update($qry_update);    
			return "Y";
		}
	}
	

	/**
	* @desc		Generate DB session
	* @param	string $email, string $password 
	* @access	default
	* @return	string $session_id
	*/

	function generate_session($email, $password) {
		$email    = $this->cf->get_data(trim($email));
		$password = $this->cf->get_data(trim($password));
		$session_id = "";
		$user_status = $this->authenticate($email, $password);
		if (trim($user_status) == "Y") {
			$tbl_parent_id = $this->get_parent_id($email);
			$qry_del = "DELETE FROM ".TBL_SESSION_PARENT." WHERE tbl_parent_id='$tbl_parent_id' ";
			$this->cf->deleteFrom($qry_del);
			$session_id = substr(md5(uniqid(rand())),0,20);

			$qry = "INSERT INTO ".TBL_SESSION_PARENT." (`tbl_parent_id`, `session_id`, `added_date`)
					VALUES ('$tbl_parent_id', '$session_id', NOW() ) ";
			//echo $qry;
			$this->cf->insertInto($qry);
			return $session_id;
		} else {
			$session_id = "";
		return $session_id;
		}
	return;	
	}

	/**
	* @desc		Get all children of parents
	* @param	String $tbl_student_id 
	* @access	default
	* @return	none
	*/

	function get_students_of_parent($tbl_parent_id) {
		$tbl_parent_id = $this->cf->get_data(trim($tbl_parent_id));
		$qry = "SELECT PS.tbl_student_id FROM ".TBL_PARENT_STUDENT." AS PS INNER JOIN ".TBL_STUDENT." AS S  ON S.tbl_student_id = PS.tbl_student_id  WHERE PS.tbl_parent_id='$tbl_parent_id' AND PS.is_active='Y' AND S.is_active='Y' ";
		//echo $qry;
		$rs = $this->cf->SelectMultiRecords($qry);
		return $rs;	
	}

	/**
	* @desc		Get parent of student
	* @param	String $tbl_student_id 
	* @access	default
	* @return	none
	*/

	function get_parent_of_student($tbl_student_id) {
		//$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		$qry = "SELECT tbl_parent_id FROM ".TBL_PARENT_STUDENT." WHERE tbl_student_id='$tbl_student_id' AND is_active='Y' ";
		//echo "<br>".$qry."<br>";
		$rs = $this->cf->SelectMultiRecords($qry);
	return $rs[0]['tbl_parent_id'];	
	}

	/**
	* @desc		Get parent object
	* @param	string $tbl_parent_id  
	* @access	default
	* @return	object parent
	*/

	function get_parent_obj($tbl_parent_id, $emirates_id="", $tbl_school_id="") {
		$tbl_parent_id = $this->cf->get_data(trim($tbl_parent_id));
		
		$qry = "SELECT * FROM ".TBL_PARENT." WHERE 1 ";
		
		if ($tbl_parent_id != "") {
			$qry .= " AND tbl_parent_id='$tbl_parent_id' ";
		}
		
		if ($emirates_id != "") {
			$qry .= " AND emirates_id='$emirates_id' ";
		}
		
		if ($tbl_school_id != "") {
			$qry .= " AND tbl_school_id='$tbl_school_id' ";
		}
		
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
	return $rs;
	}

	/**
	* @desc		Get all children
	* @param	string $tbl_parent_id  
	* @access	default
	* @return	object parent
	*/

	function fetch_all_children($tbl_parent_id) {

	    $tbl_parent_id = $this->cf->get_data(trim($tbl_parent_id));
		$qry = "SELECT tbl_student_id FROM ".TBL_PARENT_STUDENT." WHERE tbl_parent_id='$tbl_parent_id' AND is_active='Y' ";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
	return $rs;
	}

	/**
	* @desc		Save push token
	* @param	string $tbl_parent_id, $token 
	* @access	default
	* @return	string $session_id
	*/

	function save_token_ios($tbl_parent_id, $token) {
		$tbl_parent_id = $this->cf->get_data(trim($tbl_parent_id));
		$token = $this->cf->get_data(trim($token));
		if (trim($token) != "") {
			$qry_update = "UPDATE ".TBL_PARENT." SET token_ios='$token' WHERE tbl_parent_id='$tbl_parent_id' ";
			$this->cf->update($qry_update);
		}
	}


	/**
	* @desc		Get token
	* @param	string $tbl_parent_id  
	* @access	default
	* @return	object parent
	*/

	function get_token_ios($tbl_parent_id) {
		$tbl_parent_id = $this->cf->get_data(trim($tbl_parent_id));
		$qry = "SELECT token_ios FROM ".TBL_PARENT." WHERE tbl_parent_id='$tbl_parent_id' AND is_active='Y' ";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
	return $rs[0]['token_ios'];
	}


	/**
	* @desc		Get children data
	* @param	 
	* @access	default
	* @return	record set
	*/

	function get_children_data($tbl_parent_id, $tbl_student_id) {
		$qry = "SELECT * FROM ".TBL_CHILD_DATA." WHERE is_active='Y' AND tbl_student_id='$tbl_student_id' ORDER BY child_data_order ASC";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		//print_r($rs);
		return $rs;
	}

	/**
	* @desc		Get parenting videos
	* @param	 
	* @access	default
	* @return	record set
	*/

	function get_child_data_video($tbl_child_data_id) {
		$qry = "SELECT * FROM ".TBL_UPLOADS." WHERE tbl_item_id='".$tbl_child_data_id."'";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs[0]["file_name_updated"];
	}


	/**
	* @desc		Get parenting text
	* @param	 
	* @access	default
	* @return	record set
	*/

	function get_child_data_text($tbl_child_data_id) {
		$qry = "SELECT child_data_text_en, child_data_text_ar FROM ".TBL_CHILD_DATA." WHERE tbl_child_data_id='".$tbl_child_data_id."'";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}

	//added by shanavas 25_01_2016
	//list parent group
	function get_list_parent_groups($tbl_school_id,$tbl_parent_id) {
		 $qry = "SELECT distinct GROUPS.group_name_en, GROUPS.group_name_ar, GROUPS.tbl_parent_group_id FROM ".TBL_PARENT_GROUP_PARENTS." AS PARENTS LEFT JOIN  ".TBL_PARENT_GROUP." AS GROUPS ON GROUPS.tbl_parent_group_id=PARENTS.tbl_parent_group_id WHERE GROUPS.tbl_school_id='".$tbl_school_id."' 
		 AND  PARENTS.tbl_parent_id='".$tbl_parent_id."'";
		 $rs = $this->cf->selectMultiRecords($qry);
		 return $rs;		
	}

	//add topics
	function saveParentGroupForum($school_id,$tbl_parent_group_id,$message,$item_id,$tbl_parent_id){
	  $tbl_parent_forum_id = substr(md5(uniqid(rand())),0,10);
	  $qry = "INSERT INTO ".TBL_PARENT_FORUM." (
			`tbl_parent_forum_id` ,
			`forum_topic` ,
			`tbl_parent_group_id` ,
			`tbl_school_id` ,
			`item_id` ,
			`posted_by`,
			`added_date`,
			`is_status`
			)
			VALUES (
				'$tbl_parent_forum_id', '$message', '$tbl_parent_group_id', '$school_id', '$item_id', '$tbl_parent_id', NOW(),'Y'
			)";
		$rs = $this->cf->insertInto($qry);
		$MSG = "Success";	
		$arr['msg'] = $MSG;
		return $arr;
	}

	//list topics
	function get_list_parent_topics($tbl_school_id,$tbl_parent_id,$tbl_parent_group_id,$tbl_class_id='') {
		 $qry .= "SELECT tbl_parent_forum_id, forum_topic, item_id,posted_by,added_date,is_status FROM ".TBL_PARENT_FORUM." WHERE tbl_parent_group_id = '".$tbl_parent_group_id."'  AND tbl_school_id='".$tbl_school_id."'  ";
		 if($tbl_class_id<>""){
			$qry .= " AND tbl_parent_group_id IN( SELECT tbl_parent_group_id FROM ".TBL_PARENT_GROUP_PARENTS." WHERE tbl_class_id ='".$tbl_class_id."' ) ";
		 }
		 $qry .= " order by added_date DESC  ";
		 //echo $qry; exit;
		 $rs = $this->cf->selectMultiRecords($qry);
		 return $rs;
	}

	// save forum comments
	function saveParentForumComments($school_id,$tbl_parent_forum_id,$comments,$item_id,$tbl_parent_id){
	  $tbl_parent_comment_id = substr(md5(uniqid(rand())),0,10);
	  $qry = "INSERT INTO ".TBL_PARENT_FORUM_COMMENTS." (
			`tbl_parent_comment_id` ,
			`forum_comment` ,
			`tbl_parent_forum_id` ,
			`tbl_school_id` ,
			`item_id` ,
			`commented_by`,
			`added_date`,
			`is_status`
			)
			VALUES (
				'$tbl_parent_comment_id', '$comments', '$tbl_parent_forum_id', '$school_id', '$item_id', '$tbl_parent_id', NOW(),'Y'
			)";
		$rs = $this->cf->insertInto($qry);
		$MSG = "Success";	
		$arr['msg'] = $MSG;
		return $arr;
	}


	//list forum comments
	function get_list_forum_comments($tbl_school_id,$tbl_parent_id,$tbl_parent_forum_id) {
		 $qry = "SELECT tbl_parent_comment_id, forum_comment, item_id, commented_by, added_date, is_status FROM ".TBL_PARENT_FORUM_COMMENTS." WHERE tbl_parent_forum_id ='".$tbl_parent_forum_id."'  AND tbl_school_id='".$tbl_school_id."' ";
		 $rs = $this->cf->selectMultiRecords($qry);
		 return $rs;
	}

	function get_list_parent_group_members($tbl_school_id,$tbl_parent_id,$tbl_parent_group_id) {
		 $qry = " SELECT first_name,first_name_ar,last_name,last_name_ar,tbl_parent_id FROM ".TBL_PARENT." WHERE tbl_parent_id IN (
		  SELECT PARENTS.tbl_parent_id FROM ".TBL_PARENT_GROUP_PARENTS." AS PARENTS LEFT JOIN  ".TBL_PARENT_GROUP." AS GROUPS ON GROUPS.tbl_parent_group_id=PARENTS.tbl_parent_group_id WHERE PARENTS.tbl_school_id='".$tbl_school_id."' 
		  and PARENTS.tbl_parent_group_id= '".$tbl_parent_group_id."' )";
		 $rs = $this->cf->selectMultiRecords($qry);
		 return $rs;		
	}

	function time_difference ($current_datetime, $posted_datetime) {   
		$diff         = abs(strtotime($current_datetime) - strtotime($posted_datetime));
		$years 		= floor($diff / (365*60*60*24));
		$months 	= floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days 		= floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
		$hours  	= floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60)); 
		$minutes  	= floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60); 
		//$seconds 	= floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60)); 
		$seconds = 0;
		$output   = '';
		if($years > 0){
			if ($years > 1){
				$output .= $years." years ";     
			} else {
				$output .= $years." year ";
			}
		}
		if($months > 0){
			if ($months > 1){
				$output .= $months." months ";       
			} else {
				$output .= $months." month ";
			}
		}

		if($days > 0){
			if ($days > 1){
				$output .= $days." days ";       
			} else {
				$output .= $days." day ";
			}
		}

		if($hours > 0){
			if ($hours > 1){
				$output .= $hours." hours ";     
			} else {
				$output .= $hours." hour ";
			}
		}

		if($minutes > 0){
			if ($minutes > 1){
				$output .= $minutes." minutes ";     
			} else {
				$output .= $minutes." minute ";
			}
		}

		if($seconds > 0){
			if ($seconds > 1){
				$output .= $seconds." seconds";      
			} else {
				$output .= $seconds." second";
			}
		}
		return $output;
	}

	function get_parentInfo_of_student($tbl_student_id) {
		$qry = "SELECT * FROM ".TBL_PARENT." WHERE tbl_parent_id = (SELECT tbl_parent_id FROM ".TBL_PARENT_STUDENT." WHERE tbl_student_id='".$tbl_student_id."' AND is_active='Y' limit 0,1) ";
		//echo $qry; exit;
		$rs = $this->cf->SelectMultiRecords($qry);
		return $rs;	
	}
	
	/**
	* @desc		Save parent location
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function save_location($latitude, $longitude, $tbl_parent_id, $tbl_school_id) {
		$qry = "UPDATE ".TBL_PARENT." SET latitude='".$latitude."', longitude='".$longitude."' WHERE tbl_parent_id='$tbl_parent_id' AND tbl_school_id='$tbl_school_id'";
		//echo $qry;
		$this->cf->update($qry);
	}

   function get_parentgroup_byforum($tbl_school_id,$tbl_parent_forum_id) {
		 $qry .= "SELECT tbl_parent_group_id FROM ".TBL_PARENT_FORUM." WHERE tbl_parent_forum_id ='".$tbl_parent_forum_id."'  AND tbl_school_id='".$tbl_school_id."' and is_status='Y' ";
		 //echo $qry; exit;
		 $rs = $this->cf->selectMultiRecords($qry);
		 return $rs;
	}
	
	//BACKEND FUNCTIONALITY
	/**
	* @desc		Add Student
	* @param	String params
	* @access	default
	*/
	function add_parent($tbl_parent_id, $first_name_parent, $last_name_parent, $first_name_parent_ar, $last_name_parent_ar, $dob_month_parent, $dob_day_parent, $dob_year_parent, $gender_parent, $mobile_parent, $email_parent, $emirates_id_parent, $parent_user_id, $password, $tbl_school_id, $is_active="")
    {
		$tbl_parent_id     	= $this->cf->get_data($tbl_parent_id);
		$first_name_parent 	= $this->cf->get_data($first_name_parent);
		$last_name_parent     = $this->cf->get_data($last_name_parent);
		$first_name_parent_ar = $this->cf->get_data($first_name_parent_ar);
		$last_name_parent_ar  = $this->cf->get_data($last_name_parent_ar);
		$dob_month_parent     = $this->cf->get_data($dob_month_parent);
		$dob_day_parent       = $this->cf->get_data($dob_day_parent);
		$dob_year_parent      = $this->cf->get_data($dob_year_parent);
		$gender_parent        = $this->cf->get_data($gender_parent);
		$mobile_parent        = $this->cf->get_data($mobile_parent);
		$email_parent         = $this->cf->get_data($email_parent);
		$emirates_id_parent   = $this->cf->get_data($emirates_id_parent);
		$parent_user_id       = $this->cf->get_data($parent_user_id);
		$password             = $this->cf->get_data($password);
		$tbl_school_id        = $this->cf->get_data($tbl_school_id);
		
		$hash = sha1($password);
		$salt = substr(md5(uniqid(rand(), true)), 0, 3);
		$hash = sha1($salt . $hash);
		$pass_code	    =	base64_encode($password);
        $mobile_parent 	= isset($mobile_parent)? "+".$mobile_parent:'';		
		
		if($tbl_school_id<>"")
		{
			$qry_ins = "INSERT INTO ".TBL_PARENT." (`tbl_parent_id`, `first_name`, `last_name`, `first_name_ar`, `last_name_ar`, `dob_month`, 
			`dob_day`, `dob_year`, `gender`, `mobile`, `email`, `emirates_id`, `user_id` , `password`,`salt`,`pass_code`, `added_date`, `is_active`, `tbl_school_id`)
						VALUES ('$tbl_parent_id', '$first_name_parent', '$last_name_parent', '$first_name_parent_ar', '$last_name_parent_ar', '$dob_month_parent',
			'$dob_day_parent', '$dob_year_parent', '$gender_parent', '$mobile_parent', '$email_parent', '$emirates_id_parent', '$parent_user_id', '$hash', '$salt' , '$pass_code', NOW(), '$is_active', '$tbl_school_id')";
			$this->cf->insertInto($qry_ins);
			return "Y";
		}else{
		   return "N";	
			
		}
	}
	
	function is_exist_parent($emirates_id_parent, $tbl_school_id="") {
		$emirates_id_parent  = $this->cf->get_data($emirates_id_parent);
		$qry = "SELECT tbl_parent_id FROM ".TBL_PARENT." WHERE 1 ";
	
		if (trim($emirates_id_parent) != "") {
			$qry .= " AND emirates_id = '".$emirates_id_parent."' ";
		}
		
		if (trim($tbl_school_id) != "") {
			$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		}
		
	    $qry .= " AND is_active<>'D' "; 
		//echo $qry;
		//exit();
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}
	
	function assign_parents($tbl_parent_id, $tbl_student_id, $tbl_school_id)
	{
	  $tbl_parent_student_id   = md5(uniqid(rand()));
	 
	  $qry = "SELECT * FROM ".TBL_PARENT_STUDENT." WHERE 1 ";
	  $qry .= " AND tbl_parent_id = '".$tbl_parent_id."' AND tbl_student_id='".$tbl_student_id."' ";
	  $qry .= " AND is_active<>'D' "; 
	  $results = $this->cf->selectMultiRecords($qry);
	  if(count($results)==0)
	  {
		  $qry = "INSERT INTO ".TBL_PARENT_STUDENT." (
				`tbl_parent_student_id` ,
				`tbl_parent_id` ,
				`tbl_student_id` ,
				`added_date`,
				`is_active`,
				`tbl_school_id` 
				)
				VALUES (
					'$tbl_parent_student_id', '$tbl_parent_id', '$tbl_student_id', NOW(), 'Y', '$tbl_school_id'
				)";
			$rs = $this->cf->insertInto($qry);
	  }
		//return $results;	
	}
	
	
	function is_exist_parent_info($emirates_id_father,$emirates_id_mother,$assigned_parent_id_enc) {
		$emirates_id_father      = $this->cf->get_data($emirates_id_father);
		$emirates_id_mother      = $this->cf->get_data($emirates_id_mother);
		$assigned_parent_id_enc  = $this->cf->get_data($assigned_parent_id_enc);
		$qry = "SELECT * FROM ".TBL_PARENT." WHERE 1 AND (";
	
		if (trim($emirates_id_father) != "") {
			$qry .= " emirates_id = '".$emirates_id_father."' ";
		}
		if (trim($emirates_id_mother) != "") {
			$qry .= " OR emirates_id = '".$emirates_id_mother."' ";
		}
		if (trim($assigned_parent_id_enc) != "") {
			$qry .= " OR tbl_parent_id = '".$assigned_parent_id_enc."' ";
		}
	    $qry .= " ) AND is_active<>'D' "; 
		
		$results = $this->cf->selectMultiRecords($qry);
		//print_r($results);
	return $results;	
	}
	
	
	function get_all_parents($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id, $tbl_class_id='', $is_approved='')
	{
		$sort_name      = $this->cf->get_data($sort_name);
		$sort_by 	    = $this->cf->get_data($sort_by);
		$offset         = $this->cf->get_data($offset);
		$q              = urldecode($this->cf->get_data($q));
		$is_active      = $this->cf->get_data($is_active);
		$tbl_class_id   = $this->cf->get_data($tbl_class_id);
		$tbl_school_id  = $this->cf->get_data($tbl_school_id);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry = "SELECT P.* FROM ".TBL_PARENT." AS P 
					  LEFT JOIN ".TBL_PARENT_STUDENT." AS PS ON P.tbl_parent_id=PS.tbl_parent_id 
					  LEFT JOIN ".TBL_STUDENT." AS ST ON  ST.tbl_student_id=PS.tbl_student_id   
					  WHERE ( PS.tbl_school_id='".$tbl_school_id."' OR P.tbl_school_id='".$tbl_school_id."' ) ";
		//Active/Deactive
		if(trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND P.is_active='$is_active' ";
		}
		$qry .= " AND P.is_active<>'D' "; 
		
		if (trim($is_approved) != "") {
			$qry .= " AND P.is_approved='$is_approved' "; 
		}
		
		
		if (trim($q) != "") {
			$qry .= " AND ( CONCAT(TRIM(ST.first_name),' ',TRIM(ST.last_name)) LIKE '%$q%' 
					  OR CONCAT(TRIM(ST.first_name_ar),' ',TRIM(ST.last_name_ar)) LIKE '%$q%' 
					  OR CONCAT(TRIM(P.first_name),' ',TRIM(P.last_name)) LIKE '%$q%' 
					  OR CONCAT(TRIM(P.first_name_ar),' ',TRIM(P.last_name_ar)) LIKE '%$q%'
					  OR P.emirates_id LIKE '%$q%'
					  OR P.email LIKE '%$q%'
					  OR P.mobile LIKE '%$q%'
					  OR ST.country LIKE '%$q%'
					  OR P.gender LIKE '%$q%'
							) ";
		}
		
		if ($tbl_class_id && trim($tbl_class_id)!="") {
			$qry .= " AND P.`tbl_parent_id` IN (select PS.tbl_parent_id from ".TBL_PARENT_STUDENT." AS PS LEFT JOIN ".TBL_STUDENT." AS ST ON  ST.tbl_student_id=PS.tbl_student_id  
			               WHERE ST.tbl_school_id='".$tbl_school_id."' and ST.tbl_class_id='".$tbl_class_id."' )";
		}
		
		if ($tbl_class_id && trim($tbl_class_id)!="") {
			$qry .= " AND ST.`tbl_class_id` = '$tbl_class_id' ";
		}
		$qry .= " GROUP BY P.`tbl_parent_id` ";
		
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY P.first_name ASC, P.first_name_ar ASC";
		}
		
		 $qry .=" LIMIT $offset, ".TBL_PARENT_PAGING; 
		 //echo $qry."<br />";
		 $result = $this->cf->selectMultiRecords($qry);
		 return $result;	
		
	}
	
	
	/**
	* @desc		Get total parents
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_parents($q, $is_active, $tbl_school_id,$tbl_class_id, $is_approved='') {
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry =  "SELECT COUNT(distinct(P.tbl_parent_id)) as total_parents
				     FROM ".TBL_PARENT." AS P 
					 LEFT JOIN ".TBL_PARENT_STUDENT." AS PS ON P.tbl_parent_id=PS.tbl_parent_id 
					 LEFT JOIN ".TBL_STUDENT." AS ST ON  ST.tbl_student_id=PS.tbl_student_id   
					 WHERE (PS.tbl_school_id='".$tbl_school_id."' OR P.tbl_school_id='".$tbl_school_id."' ) ";	
		//Active/Deactive
		if(trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND P.is_active='$is_active' ";
		}
		
		
		$qry .= " AND P.is_active<>'D' "; 
		//Search
		if (trim($q) != "") {

		    $qry .= " 	AND ( CONCAT(TRIM(ST.first_name),' ',TRIM(ST.last_name)) LIKE '%$q%' 
					   	OR CONCAT(TRIM(ST.first_name_ar),' ',TRIM(ST.last_name_ar)) LIKE '%$q%' 
						OR CONCAT(TRIM(P.first_name),' ',TRIM(P.last_name)) LIKE '%$q%' 
					   	OR CONCAT(TRIM(P.first_name_ar),' ',TRIM(P.last_name_ar)) LIKE '%$q%'
						OR P.emirates_id LIKE '%$q%'
						OR P.email LIKE '%$q%'
						OR P.mobile LIKE '%$q%'
						OR ST.country LIKE '%$q%'
						OR P.gender LIKE '%$q%'
						) ";
		}
		
		if ($tbl_class_id && trim($tbl_class_id)!="") {

			$qry .= " AND P.`tbl_parent_id` IN (select PS.tbl_parent_id from ".TBL_PARENT_STUDENT." AS PS LEFT JOIN ".TBL_STUDENT." AS ST ON  ST.tbl_student_id=PS.tbl_student_id 
			               WHERE ST.tbl_school_id='".$tbl_school_id."' and ST.tbl_class_id='".$tbl_class_id."' )";
		}

		if ($tbl_class_id && trim($tbl_class_id)!="") {
			$qry .= " AND ST.`tbl_class_id` = '$tbl_class_id' ";
		}

		 //   $qry .= " GROUP BY P.`tbl_parent_id` ";

		if($field && $by)
		{
			$qry .= " ORDER BY P.$field $by ";
		}else{
         	$qry .= " ORDER BY P.first_name ASC, P.first_name_ar ASC";
		}
	
		//echo $qry."<br />";

		$results = $this->cf->selectMultiRecords($qry);
	    return $results[0]['total_parents'];
	}
	
	
	/**
	* @desc		Activate
	* @param	string $tbl_parent_id
	* @access	default
	*/
	function activate_parent($parent_id_enc,$tbl_school_id) {
		$tbl_parent_id = $this->cf->get_data(trim($parent_id_enc));
		$qry = "UPDATE ".TBL_PARENT." SET is_active='Y' WHERE tbl_parent_id='$tbl_parent_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate
	* @param	string $tbl_student_id
	* @access	default
	*/
	function deactivate_parent($parent_id_enc,$tbl_school_id) {
		$tbl_parent_id = $this->cf->get_data(trim($parent_id_enc));
		$qry = "UPDATE ".TBL_PARENT." SET is_active='N' WHERE tbl_parent_id='$tbl_parent_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete
	* @param	string $tbl_student_id
	* @access	default
	*/
	function delete_parent($parent_id_enc,$tbl_school_id) {
		$tbl_parent_id = $this->cf->get_data(trim($parent_id_enc));
        $qry = "UPDATE ".TBL_PARENT." SET is_active='D' WHERE tbl_parent_id='$tbl_parent_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_CATEGORY." WHERE tbl_category_id='$tbl_category_id' ";
		//$this->cf->deleteFrom($qry);
	}
	
	
	function update_parent($tbl_parent_id, $first_name_parent, $last_name_parent, $first_name_parent_ar, $last_name_parent_ar, $dob_month_parent, $dob_day_parent, $dob_year_parent, $gender_parent, $mobile_parent, $email_parent, $emirates_id_parent, $parent_user_id, $password, $tbl_school_id, $is_approved)
    {
		$tbl_parent_id     	= $this->cf->get_data($tbl_parent_id);
		$first_name_parent 	= $this->cf->get_data($first_name_parent);
		$last_name_parent     = $this->cf->get_data($last_name_parent);
		$first_name_parent_ar = $this->cf->get_data($first_name_parent_ar);
		$last_name_parent_ar  = $this->cf->get_data($last_name_parent_ar);
		$dob_month_parent     = $this->cf->get_data($dob_month_parent);
		$dob_day_parent       = $this->cf->get_data($dob_day_parent);
		$dob_year_parent      = $this->cf->get_data($dob_year_parent);
		$gender_parent        = $this->cf->get_data($gender_parent);
		$mobile_parent        = $this->cf->get_data($mobile_parent);
		$email_parent         = $this->cf->get_data($email_parent);
		$emirates_id_parent   = $this->cf->get_data($emirates_id_parent);
		$parent_user_id       = $this->cf->get_data($parent_user_id);
		$password             = $this->cf->get_data($password);
		$tbl_school_id        = $this->cf->get_data($tbl_school_id);
		$is_approved        = $this->cf->get_data($is_approved);
		
		//echo $gender_parent; 
		if($password<>"")
		{
			$hash = sha1($password);
			$salt = substr(md5(uniqid(rand(), true)), 0, 3);
			$hash = sha1($salt . $hash);
			$pass_code	    =	base64_encode($password);
		}
        $mobile_parent 	= isset($mobile_parent)? "+".$mobile_parent:'';		
		
		if($tbl_school_id<>"")
		{
			
			if($password<>"")
		    {
				$qryUpdate = "UPDATE ".TBL_PARENT." SET `first_name` = '$first_name_parent', `last_name`= '$last_name_parent', `first_name_ar` = '$first_name_parent_ar', `last_name_ar`= '$last_name_parent_ar', `dob_month` = '$dob_month_parent',  `dob_day` = '$dob_day_parent',  `dob_year` = '$dob_year_parent', `gender` = '$gender_parent', `mobile` = '$mobile_parent', `email` = '$email_parent', `emirates_id` = '$emirates_id_parent', `user_id` = '$parent_user_id', `password`= '$hash', `salt`= '$salt', `pass_code`= '$pass_code' , is_approved='$is_approved' where `tbl_school_id` = '$tbl_school_id' AND tbl_parent_id = '$tbl_parent_id'";
			   
			}else{
			    $qryUpdate = "UPDATE ".TBL_PARENT." SET `first_name` = '$first_name_parent', `last_name`= '$last_name_parent', `first_name_ar` = '$first_name_parent_ar', `last_name_ar`= '$last_name_parent_ar', `dob_month` = '$dob_month_parent',  `dob_day` = '$dob_day_parent',  `dob_year` = '$dob_year_parent', `gender` = '$gender_parent', `mobile` = '$mobile_parent', `email` = '$email_parent', `emirates_id` = '$emirates_id_parent', `user_id` = '$parent_user_id', is_approved='$is_approved' where `tbl_school_id` = '$tbl_school_id' AND tbl_parent_id = '$tbl_parent_id'";	
				
			}
			//echo $qry;
			//exit();
			$this->cf->update($qryUpdate);
		    return "Y";
		}else{
		   return "N";	
			
		}
	}
	
	function is_exist_parent_user_id($tbl_parent_id, $parent_user_id, $emirates_id_parent) {
		$tbl_parent_id  = $this->cf->get_data($tbl_parent_id);
		$parent_user_id  = $this->cf->get_data($parent_user_id);
		$qry = "SELECT tbl_parent_id FROM ".TBL_PARENT." WHERE 1 ";
	
	    if (trim($parent_user_id) != "" && $emirates_id_parent<> "") {
			$qry .= " AND ( user_id = '".$parent_user_id."' AND emirates_id <> '".$emirates_id_parent."' )  ";
		}else if (trim($parent_user_id) != "") {
			$qry .= " AND user_id = '".$parent_user_id."' ";
		}
		if (trim($tbl_parent_id) != "") {
			$qry .= " AND tbl_parent_id <> '".$tbl_parent_id."' ";
		}
		
	    $qry .= " AND is_active<>'D' "; 
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}
	
	/*********** function to get parents of particular class *************/
	function get_parents_based_on_class($tbl_school_id, $tbl_class_id)
	{
		
		$tbl_class_id   = $this->cf->get_data($tbl_class_id);
		$tbl_school_id  = $this->cf->get_data($tbl_school_id);
		
		$qry = "SELECT P.* FROM ".TBL_PARENT." AS P 
					  LEFT JOIN ".TBL_PARENT_STUDENT." AS PS ON P.tbl_parent_id=PS.tbl_parent_id 
					  LEFT JOIN ".TBL_STUDENT." AS ST ON  ST.tbl_student_id=PS.tbl_student_id   
					  WHERE ( PS.tbl_school_id='".$tbl_school_id."' OR P.tbl_school_id='".$tbl_school_id."' ) ";
		//Active/Deactive
		$qry .= " AND P.is_active='Y' ";
		
		if ($tbl_class_id && trim($tbl_class_id)!="") {
			$qry .= " AND P.`tbl_parent_id` IN (select PS.tbl_parent_id from ".TBL_PARENT_STUDENT." AS PS LEFT JOIN ".TBL_STUDENT." AS ST ON  ST.tbl_student_id=PS.tbl_student_id  
			               WHERE ST.tbl_school_id='".$tbl_school_id."' and ST.tbl_class_id='".$tbl_class_id."' )";
		}
		if ($tbl_class_id && trim($tbl_class_id)!="") {
		$qry .= " AND ST.`tbl_class_id` = '$tbl_class_id' ";
		}
		 $qry .= " GROUP BY P.`tbl_parent_id` ";
		 $qry .= " ORDER BY P.first_name ASC, P.first_name_ar ASC";
		 $result = $this->cf->selectMultiRecords($qry);
		 return $result;	
	}
	
	
	// List of Parent Groups
	function get_list_parents_group($tbl_school_id,$is_active)	
	{
	  $qry = " SELECT * FROM ".TBL_PARENT_GROUP."  AS PG WHERE 1 ";
	  if($tbl_school_id<>"")
		{
			$qry .= " AND PG.tbl_school_id= '".$tbl_school_id."' ";
		}
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PG.is_active='$is_active' ";
		}
	  $qry .= " AND PG.is_active<>'D' "; 
	  $qry .= " ORDER BY PG.group_name_en ASC  ";
	  $results = $this->cf->selectMultiRecords($qry);
	  return $results;
	  
	}
	
	//update parent information for parent list
	function is_exist_parent_details($emirates_id_parent) {
		$emirates_id_parent  = $this->cf->get_data($emirates_id_parent);
		$qry = "SELECT tbl_parent_id FROM ".TBL_PARENT." WHERE 1 ";
	
		if (trim($emirates_id_parent) != "") {
			$qry .= " AND emirates_id = '".$emirates_id_parent."' ";
		}
	    $qry .= " AND is_active<>'D' "; 
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}
	
	
	function get_my_children($tbl_parent_id,$tbl_school_id)
	{
		$qry_ps  = "SELECT S.first_name,S.last_name,S.first_name_ar,S.last_name_ar ,C.class_name, C.class_name_ar, SEC.section_name, SEC.section_name_ar
		            FROM ".TBL_STUDENT." AS S LEFT JOIN ".TBL_PARENT_STUDENT." AS PS ON PS.tbl_student_id=S.tbl_student_id 
					LEFT JOIN ".TBL_CLASS."  AS C ON  C.tbl_class_id = S.tbl_class_id
		            LEFT JOIN  ".TBL_SECTION." AS SEC  ON  C.tbl_section_id = SEC.tbl_section_id  
		            WHERE tbl_parent_id='$tbl_parent_id' AND S.is_active='Y' AND S.tbl_school_id = '$tbl_school_id' ";
		//echo $qry_ps;
		$results = $this->cf->selectMultiRecords($qry_ps);
		return $results;	
	}
	
	
	function get_my_parent_id($tbl_student_id) {
		$tbl_student_id  = $this->cf->get_data($tbl_student_id);
		$qry = "SELECT tbl_parent_id FROM ".TBL_PARENT_STUDENT." WHERE 1 ";
	
		if (trim($tbl_student_id) != "") {
			$qry .= " AND tbl_student_id = '".$tbl_student_id."' ";
		}
	    $qry .= " AND is_active<>'D' "; 
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	
	/**
	* @desc		Check if parent email already exists
	* @param	string $email
	* @access	default
	* @return	Y:Email exists, N:Email not exists
	*/
	function is_exists_email($email) {
		$email = $this->cf->get_data(trim($email));
		$qry = "SELECT id FROM ".TBL_PARENT." WHERE email='$email'";
		$arr["qry"] = $qry;
		//echo json_encode($arr);
		//exit();
		//echo $qry."<br>";
		$data_rs = $this->cf->selectMultiRecords($qry);
		if ($data_rs[0]["id"] == "") {
			return "N";
		} else {
			return "Y";
		}
	}
	
	/**
	* @desc		Check if parent user id already exists
	* @param	string $email
	* @access	default
	* @return	Y:Already exists, N:Does not exists
	*/
	function is_exist_user_id($user_id) {
		$tbl_student_id  = $this->cf->get_data($tbl_student_id);
		$qry = "SELECT id FROM ".TBL_PARENT." WHERE user_id='".$user_id."'";
		$results = $this->cf->selectMultiRecords($qry);
		return $results;
	}
	
	
	function get_parent_info_by_email($email) {
		$email = $this->cf->get_data(trim($email));
		$rs    =  array();
		if($email<>""){
			$qry = "SELECT * FROM ".TBL_STUDENT." WHERE email='$email' ";
			$rs = $this->cf->selectMultiRecords($qry);
	   }
		return $rs;
	}
	
}



?>







