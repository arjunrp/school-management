<?php
include_once('include/common_functions.php');

/**
 * @desc   	  	Ministry card Model
 *
 * @category   	Model
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Model_ministry_card extends CI_Model {
	var $cf;

	/**
	* @desc Default constructor for the Controller
	*
	* @access default
	*/
    function model_ministry_card() {
		$this->cf = new Common_functions();
    }


	/**
	* @desc		Activate
	* 
	* @param	string $tbl_card_category_id
	* @access	default
	*/
	function activate($tbl_card_category_id) {
		$tbl_card_category_id = mysql_real_escape_string(trim($tbl_card_category_id));
		
		$qry = "UPDATE ".TBL_CARD_POINTS." SET is_active='Y' WHERE tbl_card_category_id='$tbl_card_category_id' ";
		//echo $qry;
		
		$this->cf->update($qry);
	}


	/**
	* @desc		De-Activate
	* 
	* @param	string $tbl_card_category_id
	* @access	default
	*/
	function deactivate($tbl_card_category_id) {
		$tbl_card_category_id = mysql_real_escape_string(trim($tbl_card_category_id));
		
		$qry = "UPDATE ".TBL_CARD_POINTS." SET is_active='N' WHERE tbl_card_category_id='$tbl_card_category_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}


	/**
	* @desc		Delete
	* 
	* @param	string $tbl_card_category_id
	* @access	default
	*/
	function delete($tbl_card_category_id) {
		$tbl_card_category_id = mysql_real_escape_string(trim($tbl_card_category_id));

		$qry = "DELETE FROM ".TBL_CARD_POINTS." WHERE tbl_card_category_id='$tbl_card_category_id' ";
		//echo $qry;
		$this->cf->deleteFrom($qry);
	}
	

	/**
	* @desc		Get a country object based id
	* 
	* @param	string $tbl_card_category_id
	* @access	default
	*/
	function get_ministry_card_obj($tbl_card_category_id) {
		$email = mysql_real_escape_string($email);
	
		$qry = "SELECT * FROM ".TBL_CARD_POINTS." WHERE 1 AND tbl_card_category_id='$tbl_card_category_id'";

		$results = $this->cf->selectMultiRecords($qry);
		
		$country_obj = array();
		$country_obj['tbl_card_category_id'] = $results[0]['tbl_card_category_id'];
		$country_obj['category_name_en'] = $results[0]['category_name_en'];
		$country_obj['category_name_ar'] = $results[0]['category_name_ar'];
		$country_obj['added_date'] = $results[0]['added_date'];
		$country_obj['is_active'] = $results[0]['is_active'];
		
	return $country_obj;
	}

	
	/**
	* @desc		Get all ministry cards
	* 
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function get_all_ministry_cards($sort_name, $sort_by, $offset, $q, $is_active) {
		$sort_name = mysql_real_escape_string($sort_name);
		$sort_by = mysql_real_escape_string($sort_by);
		$offset = mysql_real_escape_string($offset);
		$q = mysql_real_escape_string($q);
		$is_active = mysql_real_escape_string($is_active);
	
		$qry = "SELECT * FROM ".TBL_CARD_POINTS." WHERE 1 AND tbl_school_id='' ";//only minister card has tbl_school_id=''
		
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND is_active='$is_active' ";
		}
		 
		//Search
		if (trim($q) != "") {
			$qry .= " AND (category_name_en LIKE '%$q%' || category_name_ar LIKE '%$q%')";
		} 

		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY first_name ASC";
		}

		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_CARD_POINTS_PAGING;
		}
		//echo $qry;
		
		$results = $this->cf->selectMultiRecords($qry);
		
	return $results;
	}

	
	/**
	* @desc		Get total ministry cards
	* 
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_ministry_cards($q, $is_active) {
		$q = mysql_real_escape_string($q);
		$is_active = mysql_real_escape_string($is_active);
	
		$qry = "SELECT COUNT(*) as total_ministry_cards FROM ".TBL_CARD_POINTS." WHERE 1 AND tbl_school_id='' ";//only minister card has tbl_school_id=''
		
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND is_active='$is_active' ";
		}
		 
		//Search
		if (trim($q) != "") {
			$qry .= " AND (category_name_en LIKE '%$q%' || category_name_ar LIKE '%$q%')";
		} 

		//echo $qry;
		
		$results = $this->cf->selectMultiRecords($qry);
		
	return $results[0]['total_ministry_cards'];
	}

	
	/**
	* @desc		Check if job category already exists
	* 
	* @param	String params
	* @access	default
	*/
	function is_exist($tbl_card_category_id, $category_name_en, $category_name_ar) {
		$tbl_card_category_id = mysql_real_escape_string($tbl_card_category_id);
		$category_name_en = mysql_real_escape_string($category_name_en);
		$category_name_ar = mysql_real_escape_string($category_name_ar);

		$qry = "SELECT * FROM ".TBL_CARD_POINTS." WHERE 1 AND tbl_school_id='' ";
		if (trim($category_name_en) != "") {
			$qry .= " AND category_name_en='$category_name_en'";
		}
		if (trim($tbl_card_category_id) != "") {
			$qry .= " AND tbl_card_category_id <> '$tbl_card_category_id'";
		}
		
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}

	
	/**
	* @desc		Create job category
	* 
	* @param	String params
	* @access	default
	*/
	function create_ministry_card($tbl_card_category_id, $category_name_en, $category_name_ar) {
		$tbl_card_category_id = mysql_real_escape_string($tbl_card_category_id);
		$category_name_en = mysql_real_escape_string($category_name_en);
		$category_name_ar = mysql_real_escape_string($category_name_ar);

		$qry_ins = "INSERT INTO ".TBL_CARD_POINTS." (`tbl_card_category_id`, `category_name_en`, `category_name_ar`, `added_date`, `is_active`)
					VALUES ('$tbl_card_category_id', '$category_name_en', '$category_name_ar', NOW(), 'Y')";
		//echo $qry_ins."<br>";
		$this->cf->insertInto($qry_ins);
	}

	
	/**
	* @desc		Save changes
	* 
	* @param	String params
	* @access	default
	*/
	function save_changes($tbl_card_category_id, $category_name_en, $category_name_ar) {
		$tbl_card_category_id = mysql_real_escape_string($tbl_card_category_id);
		$category_name_en = mysql_real_escape_string($category_name_en);
		$category_name_ar = mysql_real_escape_string($category_name_ar);

		$qry_update = "UPDATE ".TBL_CARD_POINTS." SET category_name_en='$category_name_en', category_name_ar='$category_name_ar' "; 
		$qry_update .= " WHERE tbl_card_category_id='$tbl_card_category_id' " ;

		//echo $qry_update."<br>";
		
		$this->cf->update($qry_update);
	}

}
?>