<?php
include_once('include/common_functions.php');

/**
 * @desc   	  	Sessions Model
 * @category   	Model
 * @author      Shanavas.PK
 * @version    	0.1
 */
 
class Model_class_sessions extends CI_Model {
	var $cf;
	/**
	* @desc Default constructor for the Controller
	* @access default
	*/

    function model_class_sessions() {
		$this->cf = new Common_functions();
    }

	/**
	* @desc		Get school session timings
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_class_sessions($tbl_school_id,$tbl_class_id='') {
 		$qry_sel = "SELECT tbl_class_sessions_id, title, title_ar, start_time, end_time FROM ".TBL_CLASS_SESSIONS." WHERE is_active='Y' 
		AND tbl_school_id='".$tbl_school_id."' ";
		$qry_sel .= " AND tbl_class_id='".$tbl_class_id."' ";
		$qry_sel .= " ORDER BY sessions_order ASC";
		$rs = $this->cf->selectMultiRecords($qry_sel);
		return $rs;
	}

	/**
	* @desc		Get school session timings
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_class_sessions_all($tbl_school_id,$tbl_class_id='') {
 		$qry_sel = "SELECT * FROM ".TBL_CLASS_SESSIONS." WHERE is_active='Y' AND tbl_school_id='".$tbl_school_id."' ";
		if($tbl_class_id<>""){
			$qry_sel .= " AND tbl_class_id = '".$tbl_class_id."' ";
		}
		$qry_sel .= " ORDER BY sessions_order ASC";
		//echo $qry_sel."<br />";
		$rs = $this->cf->selectMultiRecords($qry_sel);
		return $rs;
	}
	
  /************************************************************************************************/
  /************************************************************************************************/
  /************************************************************************************************/
	
	// WEBSITE FUNCTIONS - CLASS SESSIONS
	// CLASS SESSIONS / CLASS SESSION FUNCTION 
   
     /**
	* @desc		Get all class sessions 
	* 
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function get_all_class_sessions($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id, $tbl_class_id='') {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		$qry = "SELECT CS.*, C.class_name, C.class_name_ar, SEC.section_name, SEC.section_name_ar  FROM ".TBL_CLASS_SESSIONS."  AS CS 
		        LEFT JOIN  ".TBL_CLASS." AS C ON  C.tbl_class_id = CS.tbl_class_id  
				LEFT JOIN  ".TBL_SECTION." AS SEC  ON  SEC.tbl_section_id= C.tbl_section_id  
				WHERE 1 ";
		
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND CS.is_active='$is_active' ";
		}
		
		if($tbl_class_id<>"")
		{
			$qry .=  " AND CS.tbl_class_id = '".$tbl_class_id."' ";
		}
		$qry .= " AND CS.is_active<>'D' "; 
		//Search
		if (trim($q) != "") {
			$qry .= " AND ((CS.title LIKE '%$q%' || CS.title_ar LIKE '%$q%')
			               OR (C.class_name LIKE '%$q%' || C.class_name_ar LIKE '%$q%')
						   OR (SEC.section_name LIKE '%$q%' || SEC.section_name_ar LIKE '%$q%')) ";
		} 
		$qry .= " AND C.tbl_school_id = '".$tbl_school_id."' ";
		
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY CS.start_time ASC, CS.$sort_name $sort_by , SEC.section_name ASC  ";
		} else {
			$qry .= " ORDER BY CS.start_time ASC, CS.class_name ASC, SEC.section_name ASC,  CS.title ASC ";
		}
		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_CLASS_SESSIONS_PAGING;
		}
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	/**
	* @desc		Get total categories
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_class_sessions($q, $is_active, $tbl_school_id, $tbl_class_id='') {
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT COUNT(*) as total_session FROM ".TBL_CLASS_SESSIONS."  AS CS 
		        LEFT JOIN  ".TBL_CLASS." AS C ON  C.tbl_class_id = CS.tbl_class_id  
				LEFT JOIN  ".TBL_SECTION." AS SEC  ON  SEC.tbl_section_id= C.tbl_section_id  
				WHERE 1 ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND CS.is_active='$is_active' ";
		}
		if($tbl_class_id<>"")
		{
			$qry .=  " AND CS.tbl_class_id = '".$tbl_class_id."' ";
		}
		$qry .= " AND CS.is_active<>'D' "; 
		//Search
		if (trim($q) != "") {
			$qry .= " AND ((CS.title LIKE '%$q%' || CS.title_ar LIKE '%$q%')
			               OR (C.class_name LIKE '%$q%' || C.class_name_ar LIKE '%$q%')
						   OR (SEC.section_name LIKE '%$q%' || SEC.section_name_ar LIKE '%$q%')) ";
		} 
		$qry .= " AND C.tbl_school_id = '".$tbl_school_id."' ";
		
		//echo $qry;
		$results = $this->cf->selectMultiRecords($qry);
		
	return $results[0]['total_session'];
	}
	
	function is_exist_session($tbl_class_sessions_id, $tbl_class_id, $title, $title_ar, $start_time, $end_time, $tbl_school_id) {
		$tbl_class_sessions_id 	= $this->cf->get_data($tbl_class_sessions_id);
		$tbl_class_id  			 = $this->cf->get_data($tbl_class_id);
		$title 		            = $this->cf->get_data($title);
		$title_ar                 = $this->cf->get_data($title_ar);
		$start_time  	           = $this->cf->get_data($start_time);
		$end_time  	             = $this->cf->get_data($end_time);
		
		

		$newTime = date("H:i:s",strtotime(date($start_time)." +1 minutes"));
		$qry_exit_time = "SELECT * FROM ".TBL_CLASS_SESSIONS." WHERE tbl_school_id='".$tbl_school_id."' 
						 AND tbl_class_id='".$tbl_class_id."' 
						 AND ( (start_time BETWEEN '".$newTime."' AND '".$end_time."') OR (end_time BETWEEN '".$newTime."' AND '".$end_time."') )
		                 AND is_active='Y'";
		if($tbl_class_sessions_id<>"")
		{
			$qry_exit_time .=  " AND tbl_class_sessions_id <> '".$tbl_class_sessions_id."' ";
		}
		$existTime = $this->cf->selectMultiRecords($qry_exit_time);
		if(count($existTime)>0)
		{
			return "T";
		}else{
			$qry = "SELECT * FROM ".TBL_CLASS_SESSIONS." WHERE 1 ";
			if (trim($title) != "") {
				 $qry .= " AND title='".$title."' ";
			}
			
			if (trim($tbl_class_id) != "") {
				$qry .= " AND tbl_class_id = '".$tbl_class_id."'";
			}
			
			$qry .= " AND is_active<>'D' "; 
			$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
			if($tbl_class_sessions_id<>"")
			{
				$qry .=  " AND tbl_class_sessions_id <> '".$tbl_class_sessions_id."' ";
			}
				
			$results = $this->cf->selectMultiRecords($qry);
			if(count($results)>0)
		    {
				return "Y";
			}else{
				return "N";
			}
			
		}
		
	}
	
	/**
	* @desc		create_class_grade
	* 
	* @param	String params
	* @access	default
	*/
	function create_class_session($tbl_class_sessions_id, $tbl_class_id, $title, $title_ar, $start_time, $end_time, $tbl_school_id) {
		$tbl_class_sessions_id 	= $this->cf->get_data($tbl_class_sessions_id);
		$tbl_class_id  			 = $this->cf->get_data($tbl_class_id);
		$title 		            = $this->cf->get_data($title);
		$title_ar                 = $this->cf->get_data($title_ar);
		$start_time  	           = $this->cf->get_data($start_time);
		$end_time  	             = $this->cf->get_data($end_time);
		$tbl_class_sessions_id 	= md5(uniqid(rand()));
		$existData = $this->is_exist_session($tbl_class_sessions_id, $tbl_class_id, $title, $title_ar, $start_time, $end_time, $tbl_school_id);
		if($existData=="N")
		{
			if($tbl_school_id<>"")
			{
				$qry_ins = "INSERT INTO ".TBL_CLASS_SESSIONS." (`tbl_class_sessions_id`,`tbl_class_id`, `title`, `title_ar`, `start_time`, `end_time`, `added_date`, `is_active`, `tbl_school_id`)
								VALUES ('$tbl_class_sessions_id', '$tbl_class_id', '$title', '$title_ar', '$start_time', '$end_time', NOW(), 'Y', '$tbl_school_id')";
				$this->cf->insertInto($qry_ins);
					
			}
		}
	 return "Y";
	}
	
	/**
	* @desc		Activate
	* @param	string $class_id_enc
	* @access	default
	*/
	function activate_session($class_sessions_id_enc,$tbl_school_id) {
		$tbl_class_sessions_id = $this->cf->get_data(trim($class_sessions_id_enc));
		
		$qry = "UPDATE ".TBL_CLASS_SESSIONS." SET is_active='Y' WHERE tbl_class_sessions_id='$tbl_class_sessions_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate
	* @param	string $class_id_enc
	* @access	default
	*/
	function deactivate_session($class_sessions_id_enc,$tbl_school_id) {
		$tbl_class_sessions_id = $this->cf->get_data(trim($class_sessions_id_enc));
		$qry = "UPDATE ".TBL_CLASS_SESSIONS." SET is_active='N' WHERE tbl_class_sessions_id='$tbl_class_sessions_id' AND  tbl_school_id='$tbl_school_id' ";
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete
	* @param	string $class_id_enc
	* @access	default
	*/
	function delete_session($class_sessions_id_enc,$tbl_school_id) {
		$tbl_class_sessions_id = $this->cf->get_data(trim($class_sessions_id_enc));
        $qry = "UPDATE ".TBL_CLASS_SESSIONS." SET is_active='D' WHERE tbl_class_sessions_id='$tbl_class_sessions_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
	}
	
	/**
	* @desc		Get a category object based id
	* @param	string $tbl_class_id
	* @access	default
	*/
	function get_class_session_obj($tbl_class_session_id) {
		$qry = "SELECT * FROM ".TBL_CLASS_SESSIONS." WHERE 1  AND tbl_class_sessions_id='$tbl_class_session_id'";
		$results = $this->cf->selectMultiRecords($qry);
		$class_session_obj = array();
		$class_session_obj['tbl_class_sessions_id']     	= $results[0]['tbl_class_sessions_id'];
		$class_session_obj['tbl_class_id']  		         = $results[0]['tbl_class_id'];
		$class_session_obj['title']                        = $results[0]['title'];
		$class_session_obj['title_ar']                     = $results[0]['title_ar'];
		$class_session_obj['start_time']                   = $results[0]['start_time'];
		$class_session_obj['end_time']                     = $results[0]['end_time'];
	return $class_session_obj;
	}

	/**
	* @desc		Save changes
	* @param	String params
	* @access	default
	*/
	function save_session_changes($tbl_class_sessions_id, $tbl_class_id, $title, $title_ar, $start_time, $end_time, $tbl_school_id) {
		$tbl_class_sessions_id 	= $this->cf->get_data($tbl_class_sessions_id);
		$tbl_class_id  			 = $this->cf->get_data($tbl_class_id);
		$title 		            = $this->cf->get_data($title);
		$title_ar                 = $this->cf->get_data($title_ar);
		$start_time  	           = $this->cf->get_data($start_time);
		$end_time  	             = $this->cf->get_data($end_time);

		$qry_update = "UPDATE ".TBL_CLASS_SESSIONS." SET title='$title', title_ar='$title_ar', start_time='$start_time', end_time= '$end_time' "; 
		$qry_update .= " WHERE tbl_class_sessions_id='$tbl_class_sessions_id' AND  tbl_school_id='$tbl_school_id' AND tbl_class_id ='$tbl_class_id'  " ;
		$this->cf->update($qry_update);
	}

   function get_all_classes($tbl_school_id, $active)
   {
	   $qry = "SELECT C.*, S.section_name, S.section_name_ar, T.school_type, T.school_type_ar  FROM ".TBL_CLASS."  AS C 
		        LEFT JOIN  ".TBL_SECTION." AS S  ON  C.tbl_section_id = S.tbl_section_id  
				LEFT JOIN  ".TBL_SCHOOL_TYPE." AS T  ON  T.tbl_school_type_id= C.tbl_school_type_id  
				WHERE 1 ";
		$qry .= " AND C.is_active='Y' ";
		$qry .= " AND C.tbl_school_id = '".$tbl_school_id."' ";
		$qry .= " ORDER BY class_name ASC, S.section_name ASC  ";
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;	
   }
   


	
  /************************************************************************************************/
  /************************************************************************************************/
  /************************************************************************************************/
	


}



?>







