<?php
include_once('include/common_functions.php');

/**
 * @desc   	  	Bus Student Model
 *
 * @category   	Model
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Model_bus_student extends CI_Model {
	var $cf;


	/**
	* @desc Default constructor for the Controller
	*
	* @access default
	*/
    function model_bus_student() {
		$this->cf = new Common_functions();
    }


	/**
	* @desc		Get bus sessions
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_bus_students($tbl_bus_id, $tbl_school_id) { 
		$qry = "SELECT * FROM ".TBL_BUS_STUDENT." WHERE tbl_bus_id='".$tbl_bus_id."' AND tbl_school_id='$tbl_school_id' AND is_active='Y'";
		//echo $qry;
		$data_rs = $this->cf->selectMultiRecords($qry);
		return $data_rs;
	}
}
?>

