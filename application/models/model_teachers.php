<?php
include_once('include/common_functions.php');

/**
* @desc   	  	Teachers Model
* @category   	Model
* @author     	Shanavas PK
* @version    	0.1
*/

class Model_teachers extends CI_Model {
	
	var $cf;
	/**
	* @desc Default constructor for the Controller
	* @access default
	*/
	function model_teachers() {
		$this->cf = new Common_functions();
	}

	/**
	* @desc		Get teachers id
	* @param	string $email  
	* @access	default
	* @return	$tbl_teacher_id
	*/
	function get_teachers_id($email) {
		$email = $this->cf->get_data(trim($email));
		$qry = "SELECT tbl_teacher_id FROM ".TBL_TEACHER." WHERE user_id='$email' ";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs[0]['tbl_teacher_id'];
	}

	/**
	* @desc		Get teachers object
	* @param	string $tbl_teacher_id  
	* @access	default
	* @return	object teachers
	*/
	function get_teachers_obj($tbl_teacher_id, $email) {
		
		$qry = "SELECT * FROM ".TBL_TEACHER." WHERE 1 ";
	
		if (trim($tbl_teacher_id) != "") {
			$qry .= " AND tbl_teacher_id='$tbl_teacher_id'";
		}
		if (trim($email) != "") {
			$qry .= " AND email='$email'";
		}


		$results = $this->cf->selectMultiRecords($qry);
		
		$techer_obj = array();
		$techer_obj['tbl_teacher_id'] = $results[0]['tbl_teacher_id'];
		$techer_obj['tbl_school_roles_id'] = $results[0]['tbl_school_roles_id'];
		$techer_obj['first_name'] = $results[0]['first_name'];
		$techer_obj['first_name_ar'] = $results[0]['first_name_ar'];
		$techer_obj['last_name'] = $results[0]['last_name'];
		$techer_obj['last_name_ar'] = $results[0]['last_name_ar'];
		$techer_obj['tbl_class_id'] = $results[0]['tbl_class_id'];
		$techer_obj['mobile'] = $results[0]['mobile'];
		$techer_obj['dob_month'] = $results[0]['dob_month'];
		$techer_obj['dob_day'] = $results[0]['dob_day'];
		$techer_obj['dob_year'] = $results[0]['dob_year'];
		$techer_obj['gender'] = $results[0]['gender'];
		$techer_obj['email'] = $results[0]['email'];
		$techer_obj['country'] = $results[0]['country'];
		$techer_obj['file_name_updated'] = $results[0]['file_name_updated'];
		$techer_obj['file_type'] = $results[0]['file_type'];
		$techer_obj['file_size'] = $results[0]['file_size'];
		$techer_obj['user_id'] = $results[0]['user_id'];
		$techer_obj['token_ios'] = $results[0]['token_ios'];
		$techer_obj['tbl_school_id'] = $results[0]['tbl_school_id'];
		$techer_obj['emirates_id'] = $results[0]['emirates_id'];
		$techer_obj['pass_code'] = $results[0]['pass_code'];
	
		return $techer_obj;
	}

	/**
	* @desc		Get classes against teachers
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_all_classes_against_teacher_t($tbl_teacher_id) {
		$tbl_teacher_id = $this->cf->get_data(trim($tbl_teacher_id));
		$qry_sel = "SELECT * FROM ".TBL_TEACHER_CLASS." WHERE tbl_teacher_id='$tbl_teacher_id' AND is_active='Y' ";
		//echo $qry_sel."<br />";
		$rs = $this->cf->selectMultiRecords($qry_sel);
		return $rs;	
	}

	/**
	* @desc		Check for user record in database and validate with user credentils
	* @param	string $email, string $password 
	* @access	default
	* @return	Y:valid email/password, N:invalid email/password, D:user does not exist
	*/
	function authenticate_t($email, $password) {
		$email = $this->cf->get_data(trim($email));
		$password = $this->cf->get_data(trim($password));
		//echo "sss";
		$qry = "SELECT tbl_teacher_id, password, salt FROM ".TBL_TEACHER." WHERE user_id='$email' AND is_active='Y' ";
		//echo $qry;
		$result = $this->cf->selectMultiRecords($qry);
		if ($result) {
			$salt = $result[0]['salt'];
			$db_password = $result[0]['password'];
		} else {
			return "D";//User does not exists
		}

		$hash = sha1($salt . sha1($password));
		if (trim($hash) != trim($db_password)) {                
			return "N";
		} else {
			$tbl_teacher_id = $result[0]['tbl_teacher_id'];
		    $qry_update = "UPDATE ".TBL_TEACHER." SET is_logged='Y' WHERE tbl_teacher_id='$tbl_teacher_id' ";
			$this->cf->update($qry_update);       
			return "Y";
		}
	}

	/**
	* @desc		Generate DB session
	* @param	string $email, string $password 
	* @access	default
	* @return	string $session_id
	*/
	function generate_session_t($email, $password) {
		$email = $this->cf->get_data(trim($email));
		$password = $this->cf->get_data(trim($password));
		$session_id = "";
		$user_status = $this->authenticate($email, $password);
		if (trim($user_status) == "Y") {
			$tbl_teacher_id = $this->get_teachers_id($email);
			$qry_del = "DELETE FROM ".TBL_SESSION_TEACHER." WHERE tbl_teacher_id='$tbl_teacher_id' ";
			$this->cf->deleteFrom($qry_del);

			$session_id = substr(md5(uniqid(rand())),0,20);
			$qry = "INSERT INTO ".TBL_SESSION_TEACHER." (`tbl_teacher_id`, `session_id`, `added_date`)
					VALUES ('$tbl_teacher_id', '$session_id', NOW() ) ";
			//echo $qry;
			$this->cf->insertInto($qry);
			return $session_id;
		} else {
			$session_id = "";
		return $session_id;
		}
		return;	
	}

	/**
	* @desc		Save push token
	* @param	string $tbl_teacher_id, $token 
	* @access	default
	* @return	string $session_id
	*/
	function save_token_ios_t($tbl_teacher_id, $token) {
		$tbl_teacher_id = $this->cf->get_data(trim($tbl_teacher_id));
		$token = $this->cf->get_data(trim($token));
		if (trim($token) != "") {
			$qry_update = "UPDATE ".TBL_TEACHER." SET token_ios='$token' WHERE tbl_teacher_id='$tbl_teacher_id' ";
			$this->cf->update($qry_update);
		}
	}

	/**
	* @desc		Get token
	* @param	string $tbl_teacher_id  
	* @access	default
	* @return	object parent
	*/
	function get_token_ios_t($tbl_teacher_id) {
		$tbl_teacher_id = $this->cf->get_data(trim($tbl_teacher_id));
		$qry = "SELECT token_ios FROM ".TBL_TEACHER." WHERE tbl_teacher_id='$tbl_teacher_id' AND is_active='Y' ";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs[0]['token_ios'];
	}

	/**
	* @desc		Get all teachers
	* @param	  
	* @access	default
	* @return	RS Teachers
	*/
	function get_all_teachers($tbl_teacher_id, $tbl_school_id) {
		$qry = "SELECT * FROM ".TBL_TEACHER." WHERE is_active='Y' AND tbl_teacher_id <> '".$tbl_teacher_id."' AND tbl_school_id='".$tbl_school_id."' ORDER BY first_name ASC";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}

	/**
	* @desc		Get teacher's school id
	* @param	string $tbl_teacher_id
	* @access	default
	* @return	tbl_school_id
	*/
	function get_teachers_school_id($tbl_teacher_id) {
		$tbl_school_id = $this->cf->get_data(trim($tbl_school_id));
		$qry = "SELECT tbl_school_id FROM ".TBL_TEACHER." WHERE tbl_teacher_id='$tbl_teacher_id' ";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs[0]['tbl_school_id'];
	}

	/**
	* @desc		All teacher roles like principal, supervisor etc
	* @param	String $tbl_student_id
	* @access	default
	* @return	none
	*/

	function get_teachers_roles($tbl_school_id) {
		$qry = "SELECT * FROM ".TBL_SCHOOL_ROLES. " WHERE is_active='Y' AND tbl_school_id='".$tbl_school_id."' ORDER BY role ASC";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}

	/**
	* @desc		Get student's class teacher details
	* @param	String $tbl_student_id
	* @access	default
	* @return	none
	*/

	function get_class_teacher_details($tbl_student_id) {
		$qry = "SELECT * FROM ".TBL_TEACHER. " WHERE tbl_class_id IN(SELECT tbl_class_id FROM ".TBL_STUDENT." WHERE tbl_student_id='".$tbl_student_id."')";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
	
	//BACKEND FUNCTIONALITY
	/**
	* @desc		Get all teachers
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function list_all_teachers($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_class_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry = " SELECT T.*, UNT.device, UNT.token FROM ".TBL_TEACHER." AS T LEFT JOIN ".TBL_USER_NOTIFY_TOKEN." AS UNT ON  UNT.user_id = T.tbl_teacher_id  ";
		$qry .= " WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND T.tbl_school_id= '".$tbl_school_id."' ";
		}
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND T.is_active='$is_active' ";
		}
		$qry .= " AND T.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " AND ( CONCAT(T.first_name,' ',T.last_name) LIKE '%$q%' 
					   	OR CONCAT(TRIM(T.first_name_ar),' ',TRIM(T.last_name_ar)) LIKE '%$q%' 
						OR T.emirates_id LIKE '%$q%'
						OR T.email LIKE '%$q%'
						OR T.mobile LIKE '%$q%'
						OR T.country LIKE '%$q%'
						OR T.gender LIKE '%$q%'
						) ";
		}
		if ($tbl_class_id && trim($tbl_class_id)!="") {
			$qry .= " AND T.`tbl_teacher_id` IN (select C.tbl_teacher_id from ".TBL_TEACHER_CLASS." AS C 
			          WHERE C.tbl_school_id='".$tbl_school_id."' and C.tbl_class_id='".$tbl_class_id."' )";
		}
        
		$qry .= "  GROUP BY T.tbl_teacher_id ";
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY C.class_name ASC, S.section_name ASC, first_name ASC ";
		}
		$qry .=" LIMIT $offset, ".TBL_TEACHER_PAGING;
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	/**
	* @desc		Get total teachers
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_teachers($q, $is_active, $tbl_school_id,$tbl_class_id) {
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT distinct(T.tbl_teacher_id) FROM ".TBL_TEACHER." AS T WHERE 1 ";
			
		if($tbl_school_id<>"")
		{		
			$qry .= " AND T.tbl_school_id= '".$tbl_school_id."' ";	
		}
		//Active/Deactive
		if(trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND T.is_active='$is_active' ";
		}
		$qry .= " AND T.is_active<>'D' "; 
	//Search
		if (trim($q) != "") {
			$qry    .= " AND ( CONCAT(T.first_name,' ',T.last_name) LIKE '%$q%' 
					   	OR CONCAT(TRIM(T.first_name_ar),' ',TRIM(T.last_name_ar)) LIKE '%$q%' 
						OR T.emirates_id LIKE '%$q%'
						OR T.email LIKE '%$q%'
						OR T.mobile LIKE '%$q%'
						OR T.country LIKE '%$q%'
						OR T.gender LIKE '%$q%'
						) ";
		}
		if ($tbl_class_id && trim($tbl_class_id)!="") {
			$qry .= " AND T.`tbl_teacher_id` IN (select C.tbl_teacher_id from ".TBL_TEACHER_CLASS." AS C 
			          WHERE C.tbl_school_id='".$tbl_school_id."' and C.tbl_class_id='".$tbl_class_id."' )";
		}
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
	}
	
	/**
	* @desc		Activate
	* @access	default
	*/
	function activate_teacher($teacher_id_enc,$tbl_school_id) {
		$tbl_teacher_id = $this->cf->get_data(trim($teacher_id_enc));
		$qry = "UPDATE ".TBL_TEACHER." SET is_active='Y' WHERE tbl_teacher_id='$tbl_teacher_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate
	* @param	string $tbl_student_id
	* @access	default
	*/
	function deactivate_teacher($teacher_id_enc,$tbl_school_id) {
		$tbl_teacher_id = $this->cf->get_data(trim($teacher_id_enc));
		$qry = "UPDATE ".TBL_TEACHER." SET is_active='N' WHERE tbl_teacher_id='$tbl_teacher_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete
	* @param	string $teacher_id_enc
	* @access	default
	*/
	function delete_teacher($teacher_id_enc,$tbl_school_id) {
		$tbl_teacher_id = $this->cf->get_data(trim($teacher_id_enc));
        $qry = "UPDATE ".TBL_TEACHER." SET is_active='D' WHERE tbl_teacher_id='$tbl_teacher_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_CATEGORY." WHERE tbl_category_id='$tbl_category_id' ";
		//$this->cf->deleteFrom($qry);
	}
	

   function get_teacher_roles($active='',$tbl_school_id)
   {
   		$qry = "SELECT * FROM ".TBL_SCHOOL_ROLES." WHERE  1 ";
		if($active<>""){
			$qry .= " AND is_active='Y' ";
		}
		$qry .= " AND tbl_school_id='$tbl_school_id' ORDER BY role ASC"; 
		$results = $this->cf->selectMultiRecords($qry);
	 return $results;
   }

   function is_exist_teacher($tbl_teacher_id, $emirates_id, $tbl_school_id) {
		$emirates_id     = $this->cf->get_data($emirates_id);
		$tbl_teacher_id  = $this->cf->get_data($tbl_teacher_id);
		$tbl_school_id   = $this->cf->get_data($tbl_school_id);
		$qry = "SELECT * FROM ".TBL_TEACHER." WHERE 1 ";
	
		if (trim($emirates_id) != "") {
			$qry .= " AND emirates_id = '".$emirates_id."' ";
		}
		
		if (trim($tbl_teacher_id) != "") {
			$qry .= " AND tbl_teacher_id <> '".$tbl_teacher_id."' ";
		}
		
		if (trim($tbl_school_id) != "") {
			$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		}
	    $qry .= " AND is_active<>'D' "; 
		$results = $this->cf->selectMultiRecords($qry);
		//print_r($results);
	return $results;	
	}
   
	function add_teacher($tbl_teacher_id, $first_name, $last_name, $first_name_ar, $last_name_ar, $dob_month, $dob_day, $dob_year, 
		                                             $gender, $mobile, $email, $country, $emirates_id, $tbl_school_roles_id, $tbl_class_teacher_id, $user_id, $password, $tbl_school_id,$is_active='')
	{
		$tbl_teacher_id    			= $this->cf->get_data($tbl_teacher_id);
		$first_name        			= $this->cf->get_data($first_name);
		$last_name         		 	= $this->cf->get_data($last_name);
		$first_name_ar     		 	= $this->cf->get_data($first_name_ar);
		$last_name_ar      		  	= $this->cf->get_data($last_name_ar);
		$dob_month         		 	= $this->cf->get_data($dob_month);
		$dob_day           		   	= $this->cf->get_data($dob_day);
		$dob_year          		  	= $this->cf->get_data($dob_year);
		$gender            			= $this->cf->get_data($gender);
		$mobile            			= $this->cf->get_data($mobile);
		$email             		 	= $this->cf->get_data($email);
		$country           		   	= $this->cf->get_data($country);
		$emirates_id       		   	= $this->cf->get_data($emirates_id);
		$tbl_school_roles_id   	   	= $this->cf->get_data($tbl_school_roles_id);
		$tbl_class_teacher_id      	= $this->cf->get_data($tbl_class_teacher_id);
		$user_id                   	= $this->cf->get_data($user_id);
		$password                  	= $this->cf->get_data($password);
		$tbl_school_id             	= $this->cf->get_data($tbl_school_id);
		
		$hash = sha1($password);
		$salt = substr(md5(uniqid(rand(), true)), 0, 3);
		$hash = sha1($salt . $hash);
		$pass_code	    =	base64_encode($password);
		
		if($mobile<>"")
			$mobile 	       = "+".$mobile;
		
		if($tbl_school_id<>"")
		{
			$qry_ins = "INSERT INTO ".TBL_TEACHER." (`tbl_teacher_id`, `tbl_school_roles_id`, `first_name`, `first_name_ar`, `last_name`, `last_name_ar`, `tbl_class_id`,`mobile`, `dob_month`, `dob_day`, `dob_year`, `gender`, `email`, `country`, `user_id`, `password`, `salt`, `file_name_updated`,  `file_type`,  `file_size`, `added_date`, `is_active`, `tbl_school_id`, `emirates_id`, `pass_code`)
									VALUES ('$tbl_teacher_id', '$tbl_school_roles_id', '$first_name', '$first_name_ar', '$last_name', '$last_name_ar', '$tbl_class_teacher_id', '$mobile',  '$dob_month', '$dob_day', '$dob_year', '$gender', '$email',  '$country', '$user_id', '$hash', '$salt','$file_name_updated',  '$file_type',  '$file_size', NOW(), '$is_active', '$tbl_school_id', '$emirates_id', '$pass_code')";
			$this->cf->insertInto($qry_ins);
			
			$qry = "SELECT file_name_updated,file_name_original,file_type,file_size FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_teacher_id' AND is_active='Y' ";
			$result = $this->cf->selectMultiRecords($qry);
			if (count($result)>0) {
				$file_name_updated  = isset($result[0]['file_name_updated'])? $result[0]['file_name_updated']:'';
				$file_type          = isset($result[0]['file_type'])? $result[0]['file_type']:'';
				$file_size          = isset($result[0]['file_size'])? $result[0]['file_size']:'';
				$qry_update  = "UPDATE ".TBL_TEACHER." SET file_name_updated='$file_name_updated', file_type='$file_type', file_size='$file_size' WHERE tbl_teacher_id='$tbl_teacher_id' ";
				$this->cf->update($qry_update);
			}
			return "Y";
		}else{
		   return "N";	
			
		}
	}
	
	function assign_classes($tbl_teacher_class_id, $tbl_class_id_str, $tbl_teacher_id, $tbl_school_id)
	{
		$qry_ins =  "INSERT INTO ".TBL_TEACHER_CLASS." (`tbl_teacher_class_id`, `tbl_teacher_id`, `tbl_class_id`, `added_date`, `is_active`, `tbl_school_id`)
											VALUES ('$tbl_teacher_class_id', '$tbl_teacher_id', '$tbl_class_id_str', NOW(), 'Y', '$tbl_school_id')";
	    $this->cf->insertInto($qry_ins);
	}
	
	function delete_assign_classes($tbl_teacher_id, $tbl_school_id)
	{
		$qry = "DELETE FROM ".TBL_TEACHER_CLASS." WHERE `tbl_teacher_id`='$tbl_teacher_id'  AND `tbl_school_id` = '$tbl_school_id' ";
		$this->cf->deleteFrom($qry);
	}
	
	function is_exist_teacher_user_id($tbl_teacher_id, $user_id) {
		$tbl_teacher_id  = $this->cf->get_data($tbl_teacher_id);
		$user_id         = $this->cf->get_data($user_id);
		$qry = "SELECT tbl_teacher_id FROM ".TBL_TEACHER." WHERE 1 ";
	
		if (trim($user_id) != "") {
			$qry .= " AND user_id = '".$user_id."' ";
		}
		if (trim($tbl_teacher_id) != "") {
			$qry .= " AND tbl_teacher_id <> '".$tbl_teacher_id."' ";
		}
		
	    $qry .= " AND is_active<>'D' "; 
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}
	
	function get_assign_classes($tbl_teacher_id,$tbl_school_id)
	{
		$qry_exist = "SELECT TC.tbl_class_id FROM ".TBL_TEACHER_CLASS." AS TC INNER JOIN ".TBL_CLASS." AS C ON C.tbl_class_id=TC.tbl_class_id  WHERE TC.tbl_school_id='$tbl_school_id' AND TC.tbl_teacher_id='$tbl_teacher_id' AND TC.is_active='Y' ORDER BY  C.priority ASC ";
		$results = $this->cf->selectMultiRecords($qry_exist);
	    return $results;
	}
	
	function update_teacher($tbl_teacher_id, $first_name, $last_name, $first_name_ar, $last_name_ar, $dob_month, $dob_day, $dob_year, 
		                                             $gender, $mobile, $email, $country, $emirates_id, $tbl_school_roles_id, $tbl_class_teacher_id, $user_id, $password, $tbl_school_id)
	{
		$tbl_teacher_id    		= $this->cf->get_data($tbl_teacher_id);
		$first_name        		= $this->cf->get_data($first_name);
		$last_name         		 = $this->cf->get_data($last_name);
		$first_name_ar     		 = $this->cf->get_data($first_name_ar);
		$last_name_ar      		  = $this->cf->get_data($last_name_ar);
		$dob_month         		 = $this->cf->get_data($dob_month);
		$dob_day           		   = $this->cf->get_data($dob_day);
		$dob_year          		  = $this->cf->get_data($dob_year);
		$gender            		= $this->cf->get_data($gender);
		$mobile            		= $this->cf->get_data($mobile);
		$email             		 = $this->cf->get_data($email);
		$country           		   = $this->cf->get_data($country);
		$emirates_id       		   = $this->cf->get_data($emirates_id);
		$tbl_school_roles_id   	   = $this->cf->get_data($tbl_school_roles_id);
		$tbl_class_teacher_id      = $this->cf->get_data($tbl_class_teacher_id);
		$user_id                   = $this->cf->get_data($user_id);
		$password                  = $this->cf->get_data($password);
		$tbl_school_id             = $this->cf->get_data($tbl_school_id);
		
		$hash = sha1($password);
		$salt = substr(md5(uniqid(rand(), true)), 0, 3);
		$hash = sha1($salt . $hash);
		$pass_code	    =	base64_encode($password);
		
		if($mobile<>"")
			$mobile 	       = "+".$mobile;
		
		if($tbl_school_id<>"")
		{
			$qry_update = "UPDATE ".TBL_TEACHER." SET tbl_school_roles_id='$tbl_school_roles_id', first_name='$first_name', first_name_ar='$first_name_ar', last_name='$last_name', last_name_ar='$last_name_ar', tbl_class_id='$tbl_class_teacher_id', mobile='$mobile', dob_month='$dob_month', dob_day='$dob_day', dob_year='$dob_year', gender='$gender' ,country='$country', email='$email', emirates_id='$emirates_id', user_id= '$user_id' WHERE tbl_teacher_id= '$tbl_teacher_id' AND tbl_school_id = '$tbl_school_id' ";
			//echo $qry_update;
			$this->cf->update($qry_update);
			
			if (trim($password)!="") {
				$hash = sha1($password);
				$salt = substr(md5(uniqid(rand(), true)), 0, 3);
				$hash = sha1($salt . $hash);
				$pass_code =	base64_encode($password);
				$qry_pdate_pass = "UPDATE ".TBL_TEACHER." SET password='$hash', salt='$salt', `pass_code`='$pass_code'  WHERE tbl_teacher_id= '$tbl_teacher_id' AND tbl_school_id = '$tbl_school_id' ";
				$this->cf->update($qry_pdate_pass);			
			}
			
			$qry = "SELECT file_name_updated,file_name_original,file_type,file_size FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_teacher_id' AND is_active='Y' ";
			$result = $this->cf->selectMultiRecords($qry);
			if (count($result)>0) {
				$file_name_updated  = isset($result[0]['file_name_updated'])? $result[0]['file_name_updated']:'';
				$file_type          = isset($result[0]['file_type'])? $result[0]['file_type']:'';
				$file_size          = isset($result[0]['file_size'])? $result[0]['file_size']:'';
				$qry_update  = "UPDATE ".TBL_TEACHER." SET file_name_updated='$file_name_updated', file_type='$file_type', file_size='$file_size' WHERE tbl_teacher_id='$tbl_teacher_id' ";
				$this->cf->update($qry_update);
				
				$qry = "DELETE FROM ".TBL_UPLOADS." WHERE `tbl_item_id`='$tbl_teacher_id' ";
				$this->cf->deleteFrom($qry);
				
				
			}
			return "Y";
		}else{
		   return "N";	
			
		}
	}
	
	
	function get_list_teachers($tbl_school_id,$tbl_class_id,$is_active, $tbl_teacher_id='')	
	{
	    $qry = "SELECT T.tbl_teacher_id, T.first_name, T.first_name_ar, T.last_name, T.last_name_ar, T.file_name_updated FROM ".TBL_TEACHER." AS T WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND T.tbl_school_id= '".$tbl_school_id."' ";
		}
		
		if($tbl_teacher_id<>"")
		{
			$qry .= " AND T.tbl_teacher_id<> '".$tbl_teacher_id."' ";
		}
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND T.is_active='$is_active' ";
		}
		$qry .= " AND T.is_active<>'D' "; 
		if ($tbl_class_id && trim($tbl_class_id)!="") {
			$qry .= " AND T.`tbl_teacher_id` IN (select C.tbl_teacher_id from ".TBL_TEACHER_CLASS." AS C 
			          WHERE C.tbl_school_id='".$tbl_school_id."' and C.tbl_class_id='".$tbl_class_id."' )";
		}
        
		$qry .= "  GROUP BY T.tbl_teacher_id ";
		$qry .= " ORDER BY T.first_name ASC ";
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	function get_list_teacher_group($tbl_school_id,$is_active)	
	{
	  $qry = " SELECT * FROM ".TBL_TEACHER_GROUP."  AS G WHERE 1 ";
	  if($tbl_school_id<>"")
		{
			$qry .= " AND G.tbl_school_id= '".$tbl_school_id."' ";
		}
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND G.is_active='$is_active' ";
		}
	  $qry .= " AND G.is_active<>'D' "; 
	  $qry .= " ORDER BY G.group_name_en ASC  ";
	  $results = $this->cf->selectMultiRecords($qry);
	  return $results;
	  
	}
	
	
	
	function get_teacher_details($tbl_teacher_id) {
		//$tbl_teacher_id = $this->cf->get_data(trim($tbl_teacher_id));
		$qry = "SELECT * FROM ".TBL_TEACHER." WHERE tbl_teacher_id='$tbl_teacher_id' ";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
	
	//SCHOOL DASHBOARD
	
	function get_total_teachers_against_school($tbl_school_id) {
		$qry = "SELECT COUNT(tbl_teacher_id) as cntTeacher FROM ".TBL_TEACHER." WHERE is_active='Y'  AND tbl_school_id='".$tbl_school_id."' ";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $cntTeacher = $rs[0]['cntTeacher'];;
	}
	//END SCHOOL DASHBOARD
	
	// Teacher Profile 
	function teacher_info($tbl_teacher_id, $tbl_school_id) {
		
		$qry = "SELECT T.*, SR.role, SR.role_ar  FROM ".TBL_TEACHER." AS T LEFT JOIN ".TBL_SCHOOL_ROLES."  AS SR ON SR.tbl_school_roles_id=T.tbl_school_roles_id WHERE 1 ";
		if (trim($tbl_teacher_id) != "") {
			$qry .= " AND T.tbl_teacher_id='$tbl_teacher_id'";
		}
		if (trim($tbl_school_id) != "") {
			$qry .= " AND T.tbl_school_id='$tbl_school_id'";
		}
		$results = $this->cf->selectMultiRecords($qry);
		return $results;
	}
	
	
	
	function get_teacher_info_by_email($email) {
		$email = $this->cf->get_data(trim($email));
		$rs    =  array();
		if($email<>""){
			$qry = "SELECT * FROM ".TBL_TEACHER." WHERE email='$email' ";
			$rs = $this->cf->selectMultiRecords($qry);
	   }
		return $rs;
	}
	
	
	
	function assign_subjects($tbl_teacher_subject_id, $tbl_subject_id_str, $tbl_teacher_id, $tbl_school_id)
	{
		$qry_ins =  "INSERT INTO ".TBL_TEACHER_SUBJECT." (`tbl_teacher_subject_id`, `tbl_teacher_id`, `tbl_subject_id`, `added_date`, `is_active`, `tbl_school_id`)
											VALUES ('$tbl_teacher_subject_id', '$tbl_teacher_id', '$tbl_subject_id_str', NOW(), 'Y', '$tbl_school_id')";
	    $this->cf->insertInto($qry_ins);
	}
	
	function delete_assign_subjects($tbl_teacher_id, $tbl_school_id)
	{
		$qry = "DELETE FROM ".TBL_TEACHER_SUBJECT." WHERE `tbl_teacher_id`='$tbl_teacher_id'  AND `tbl_school_id` = '$tbl_school_id' ";
		$this->cf->deleteFrom($qry);
	}
	
	function get_assign_subjects($tbl_teacher_id,$tbl_school_id)
	{
		$qry_exist = "SELECT tbl_subject_id FROM ".TBL_TEACHER_SUBJECT." WHERE tbl_school_id='$tbl_school_id' AND tbl_teacher_id='$tbl_teacher_id' AND is_active='Y' ";
		$results = $this->cf->selectMultiRecords($qry_exist);
	    return $results;
	}
	
	
	
	// Update teacher profile - mobile app
	function update_teacher_profile($tbl_teacher_id, $first_name, $last_name, $first_name_ar, $last_name_ar, $dob_month, $dob_day, $dob_year, 
		                                             $gender, $mobile, $email, $country, $emirates_id, $tbl_school_roles_id, $tbl_class_teacher_id, $user_id, $password, $tbl_school_id)
	{
		$tbl_teacher_id    			= $this->cf->get_data($tbl_teacher_id);
		$first_name        			= $this->cf->get_data($first_name);
		$last_name         		 	= $this->cf->get_data($last_name);
		$first_name_ar     		 	= $this->cf->get_data($first_name_ar);
		$last_name_ar      		  	= $this->cf->get_data($last_name_ar);
		$dob_month         		 	= $this->cf->get_data($dob_month);
		$dob_day           		   	= $this->cf->get_data($dob_day);
		$dob_year          		  	= $this->cf->get_data($dob_year);
		$gender            			= $this->cf->get_data($gender);
		$mobile            			= $this->cf->get_data($mobile);
		$email             		 	= $this->cf->get_data($email);
		$country           		   	= $this->cf->get_data($country);
		$emirates_id       		   	= $this->cf->get_data($emirates_id);
		$tbl_school_roles_id   	   	= $this->cf->get_data($tbl_school_roles_id);
		$tbl_class_teacher_id      	= $this->cf->get_data($tbl_class_teacher_id);
		$user_id                   	= $this->cf->get_data($user_id);
		$password                  	= $this->cf->get_data($password);
		$tbl_school_id             	= $this->cf->get_data($tbl_school_id);
		
		$hash = sha1($password);
		$salt = substr(md5(uniqid(rand(), true)), 0, 3);
		$hash = sha1($salt . $hash);
		$pass_code	    =	base64_encode($password);
		
		/*if($mobile<>"")
			$mobile 	       = "+971".$mobile;*/
		
		
		if($tbl_school_id<>"")
		{
			$qry_update = "UPDATE ".TBL_TEACHER." SET  
			   first_name		=	'$first_name',
			   first_name_ar	=	'$first_name_ar',
			   last_name		=	'$last_name',
			   last_name_ar		=	'$last_name_ar',  
			   mobile			=	'$mobile',
			   dob_month		=	'$dob_month',
			   dob_day			=	'$dob_day',
			   dob_year			=	'$dob_year',
			   gender			=	'$gender' ,
			   email			=	'$email',
			   emirates_id		=	'$emirates_id' ";
			   
		   if($country<>"")
		   		$qry_update .=  ", country	= '$country' ";
			   
		   if($tbl_school_roles_id<>"")
				$qry_update .=  ",  tbl_school_roles_id	=	'$tbl_school_roles_id' ";
				
		   if($tbl_class_teacher_id<>"")
				$qry_update .=  ",   tbl_class_id			=	'$tbl_class_teacher_id' ";
			
		   if($user_id<>"")	
			$qry_update .=  ", user_id				= 	'$user_id' ";
			
			$qry_update .=  "   WHERE tbl_teacher_id	= 	'$tbl_teacher_id' AND tbl_school_id = '$tbl_school_id' ";
			//echo $qry_update;
			$this->cf->update($qry_update);
			
			if (trim($password)!="") {
				$hash = sha1($password);
				$salt = substr(md5(uniqid(rand(), true)), 0, 3);
				$hash = sha1($salt . $hash);
				$pass_code =	base64_encode($password);
				$qry_pdate_pass = "UPDATE ".TBL_TEACHER." SET password='$hash', salt='$salt', `pass_code`='$pass_code'  WHERE tbl_teacher_id= '$tbl_teacher_id' AND tbl_school_id = '$tbl_school_id' ";
				$this->cf->update($qry_pdate_pass);			
			}
			
			$qry = "SELECT file_name_updated,file_name_original,file_type,file_size FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_teacher_id' AND is_active='Y' ";
			$result = $this->cf->selectMultiRecords($qry);
			if (count($result)>0) {
				$file_name_updated  = isset($result[0]['file_name_updated'])? $result[0]['file_name_updated']:'';
				$file_type          = isset($result[0]['file_type'])? $result[0]['file_type']:'';
				$file_size          = isset($result[0]['file_size'])? $result[0]['file_size']:'';
				$qry_update  = "UPDATE ".TBL_TEACHER." SET file_name_updated='$file_name_updated', file_type='$file_type', file_size='$file_size' WHERE tbl_teacher_id='$tbl_teacher_id' ";
				$this->cf->update($qry_update);
				
				$qry = "DELETE FROM ".TBL_UPLOADS." WHERE `tbl_item_id`='$tbl_teacher_id' ";
				$this->cf->deleteFrom($qry);
			}
			return "Y";
		}else{
		   return "N";	
			
		}
	}
	
	
	
	

}



?>







