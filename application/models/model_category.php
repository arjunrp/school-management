<?php
include_once('include/common_functions.php');

/**
 * @desc   	  	User Model
 *
 * @category   	Model
 * @author     	Shanavas.PK
 * @version    	0.1
 */
class Model_category extends CI_Model {
	var $cf;

	/**
	* @desc Default constructor for the Controller
	*
	* @access default
	*/
    function model_category() {
		$this->cf = new Common_functions();
    }

    /************************* book category *****************************************************/
	
	function get_all_book_categories($sort_name, $sort_by, $offset, $q="",$list ='', $is_active='') {
	
		$qry = "SELECT * FROM ".TBL_BOOK_CATEGORY;
		if($is_active<>"")
		{
			$qry .= " WHERE is_active='Y' ";
		}
		$qry .= " ORDER BY category_name_en ASC ";
		if($list<>"Y")
		{
			$qry .= " LIMIT $offset , ".PAGING_TBL_BOOKS_CATEGORY;
		}
		//echo $qry;
		$results = $this->cf->selectMultiRecords($qry);
		for($y=0;$y<count($results);$y++)
		{
		    $category_parent_id = $results[$y]['category_parent_id'];
			if($category_parent_id<>""){
				$resCat = $this->getCategoryDetails($category_parent_id);
				$results[$y]['parent_category_name_en'] = $resCat[0]['category_name_en'];
				$results[$y]['parent_category_name_ar'] = $resCat[0]['category_name_ar'] ;
			}else{
				$results[$y]['parent_category_name_en'] = '';
				$results[$y]['parent_category_name_ar'] = '';
			}
		}
		
	return $results;
	}
	
	
	function get_total_book_categories($q="") {
	
		$qry = "SELECT * FROM ".TBL_BOOK_CATEGORY." WHERE is_active='Y' ";
		$qry .= " ORDER BY category_name_en ASC ";
		//echo $qry;
		$results = $this->cf->selectMultiRecords($qry);
	return count($results);
	}
	
	

	function save_book_category($tbl_book_category_id,$tbl_book_category_parent_id, $category_name_en, $category_name_ar){
		        
				$sql4CategoryPic   = "SELECT file_name_updated as picture from " . TBL_UPLOADS . " where tbl_item_id = '$tbl_book_category_id' ";
				$sql4DelUpload = "DELETE FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_book_category_id' ";
				$results = $this->cf->selectMultiRecords($sql4CategoryPic);
				if(count($results)>0){
					$picture = $results[0]['picture'];
					$this->cf->deleteFrom($sql4DelUpload);
				}else{
					$picture = "";
				}
				
				$sql4sess = "INSERT INTO " . TBL_BOOK_CATEGORY . "(tbl_book_category_id,category_parent_id, category_name_en, category_name_ar, picture, added_date) 
				             VALUES('$tbl_book_category_id','$tbl_book_category_parent_id','$category_name_en','$category_name_ar', '$picture', NOW())";
				$sql4userSession = "SELECT tbl_book_category_id from " . TBL_BOOK_CATEGORY . " where (category_name_en='$category_name_en' or category_name_ar= '$category_name_ar') and tbl_book_category_id='$tbl_book_category_id'  ";
			   
			    if ($this->cf->isExist($sql4userSession)){
					return "N";
					  // $this->deleteFrom($sql4DelUserSession);
				}else{	
					$this->cf->insertInto($sql4sess);
					return "Y";
				}
	}


	function getCategoryDetails($category_parent_id) {
		$qry = "SELECT * FROM ".TBL_BOOK_CATEGORY." WHERE tbl_book_category_id='$category_parent_id'";
		//echo $qry."<br />";
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	/** Activate book category ***/
	function activate_book_category($tbl_book_category_id) {
		$qry = "UPDATE ".TBL_BOOK_CATEGORY." SET is_active='Y' WHERE tbl_book_category_id='$tbl_book_category_id' ";
		$this->cf->update($qry);
	}

    /** Activate book category***/
	function deactivate_book_category($tbl_book_category_id) {
		$qry = "UPDATE ".TBL_BOOK_CATEGORY." SET is_active='N' WHERE tbl_book_category_id='$tbl_book_category_id' ";
		$this->cf->update($qry);
	}
	
	 /** Delete book category***/
	function delete_book_category($tbl_book_category_id) {
		/*$qry = "DELETE FROM ".TBL_BOOK_CATEGORY." WHERE tbl_book_category_id='$tbl_book_category_id' ";
		$this->cf->deleteFrom($qry);*/
		$qry = "UPDATE " . TBL_BOOK_CATEGORY . " SET is_active='D' WHERE tbl_book_category_id='$tbl_book_category_id' ";
		$this->cf->update($qry);
		
	}
	
	
	function is_exist_book_category($tbl_book_category_id, $category_name_en, $category_name_ar){
		$sql4existBook = "SELECT tbl_book_category_id from " . TBL_BOOK_CATEGORY . " where (category_name_en='$category_name_en' OR category_name_ar = '$category_name_ar') 
		and tbl_book_category_id<>'$tbl_book_category_id' AND is_active<>'D' ";
		
		if ($this->cf->isExist($sql4existBook)){
			return "Y";
		}else{	
			return "N";
		}
	}

    function update_book_category($tbl_book_category_id,$tbl_parent_category_id, $category_name_en, $category_name_ar)
	{
		
		$sql4NewsPic   = "SELECT file_name_updated as picture from " . TBL_UPLOADS . " where tbl_item_id = '$tbl_book_category_id' ";
		$sql4DelUpload = "DELETE FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_book_category_id' ";
		$results = $this->cf->selectMultiRecords($sql4NewsPic);
		if(count($results)>0){
			$picture = $results[0]['picture'];
			$this->cf->deleteFrom($sql4DelUpload);
		}else{
			$picture = "";
		}	
				
		if($picture<>"")
		{	
			$sql4sess = "UPDATE " . TBL_BOOK_CATEGORY . " SET category_parent_id='$tbl_parent_category_id', 
				category_name_en='$category_name_en',category_name_ar='$category_name_ar',picture='$picture' where tbl_book_category_id='$tbl_book_category_id'";
			//echo $sql4sess; exit;
			$this->cf->update($sql4sess);
		
		}else{
			$sql4sess = "UPDATE " . TBL_BOOK_CATEGORY . " SET category_parent_id='$tbl_parent_category_id', 
				category_name_en='$category_name_en',category_name_ar='$category_name_ar' where tbl_book_category_id='$tbl_book_category_id'";
			//echo $sql4sess; exit;
			$this->cf->update($sql4sess);
		}
	}
	
	function get_book_category_picture($tbl_book_category_id) {
		$qry = "SELECT picture FROM ".TBL_BOOK_CATEGORY." WHERE tbl_book_category_id='$tbl_book_category_id' ";
		$rs = $this->cf->selectMultiRecords($qry);
	    return $rs;
	}
	
	function update_book_category_pic($files, $tbl_book_category_id) {

		if ($files["file_category_pic"]["name"] != "") {
			//Image Upload
			$source_img =  $files["file_category_pic"]["tmp_name"];
			$orignal_filename_img = $files["file_category_pic"]["name"];
			$Ext = strchr($orignal_filename_img,".");
			$Ext = strtolower($Ext);
			$file_type = $_FILES["file_category_pic"]["type"];
			$file_size = $_FILES["file_category_pic"]["size"];
	
			if ($Ext==".jpg" || $Ext==".jpeg" || $Ext==".gif" || $Ext==".bmp" || $Ext==".png") {
				$file_category_pic_name = substr(md5(uniqid(rand())),0,15);
				$file_category_pic_name = $file_category_pic_name.$Ext;
				$copy = $this->cf->file_copy("$source_img", FILE_UPLOAD_PATH."/books/", "$file_category_pic_name", 1, 1);
			} else {
				$MSG = "Image extension is not valid.<br>";
				return $MSG;
			}
		}
		
		$tbl_uploads_id = substr(md5(uniqid(rand())),0,15);
		$tbl_item_id    = $tbl_book_category_id;
		$qry = "INSERT INTO ".TBL_UPLOADS." (`tbl_uploads_id` ,`tbl_item_id` ,`file_name_original` ,`file_name_updated` ,`file_type` ,`file_size` ,`is_active` ,`added_date`
			)
			VALUES (
				'$tbl_uploads_id', '$tbl_item_id', '$orignal_filename_img', '$file_category_pic_name', '$file_type', '$file_size', 'Y', NOW()
			)";
		$this->cf->insertInto($qry);
		$MSG = "Picture uploaded successfully.";
	    return $MSG;	
	}
	
	function get_book_category_picture_upload($tbl_book_category_id) {
		$qry = "SELECT  file_name_updated AS picture FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_book_category_id' ";
		$rs = $this->cf->selectMultiRecords($qry);
	    return $rs;
	}
	
	
	/**************************************END BOOK CATEGORY ************************************************************/
	
	
	/**************************************START NEWS CATEGORY **********************************************************/
	function getNewsCategoryDetails($tbl_news_category_id) {
		$qry = "SELECT * FROM ".TBL_NEWS_CATEGORY." WHERE tbl_news_category_id='$tbl_news_category_id'";
		//echo $qry."<br />";
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	
	function get_all_news_categories($sort_name, $sort_by, $offset, $q="",$list ='', $is_active='') {
	
		$qry = "SELECT * FROM ".TBL_NEWS_CATEGORY;
		if($is_active<>"")
		{
			$qry .= " WHERE is_active='Y' ";
		}
		$qry .= " ORDER BY category_name_en ASC ";
		if($list<>"Y")
		{
			$qry .= " LIMIT $offset , ".PAGING_TBL_NEWS_CATEGORY;
		}
		$results = $this->cf->selectMultiRecords($qry);
		
	return $results;
	}
	
	
	function get_total_news_categories($q="") {
	
		$qry = "SELECT * FROM ".TBL_NEWS_CATEGORY." WHERE is_active='Y' ";
		$qry .= " ORDER BY category_name_en ASC ";
		//echo $qry;
		$results = $this->cf->selectMultiRecords($qry);
	return count($results);
	}
	
	

	function save_news_category($unique_id, $category_name_en, $category_name_ar){
		        $sql4sess = "INSERT INTO " . TBL_NEWS_CATEGORY . "(tbl_news_category_id, category_name_en, category_name_ar,added_date) 
				             VALUES('$unique_id','$category_name_en','$category_name_ar',NOW())";
				$sql4userSession = "SELECT tbl_news_category_id from " . TBL_NEWS_CATEGORY . " where category_name_en='$category_name_en' or category_name_ar= '$category_name_ar' ";
			   
			    if ($this->cf->isExist($sql4userSession)){
					return "N";
					  // $this->deleteFrom($sql4DelUserSession);
				}else{	
					$this->cf->insertInto($sql4sess);
					return "Y";
				}
	}
	
	/** Activate news category ***/
	function activate_news_category($tbl_news_category_id) {
		$qry = "UPDATE ".TBL_NEWS_CATEGORY." SET is_active='Y' WHERE tbl_news_category_id='$tbl_news_category_id' ";
		$this->cf->update($qry);
	}

    /** Activate news category***/
	function deactivate_news_category($tbl_news_category_id) {
		$qry = "UPDATE ".TBL_NEWS_CATEGORY." SET is_active='N' WHERE tbl_news_category_id='$tbl_news_category_id' ";
		$this->cf->update($qry);
	}
	
	 /** Delete news category***/
	function delete_news_category($tbl_news_category_id) {
		$qry = "DELETE FROM ".TBL_NEWS_CATEGORY." WHERE tbl_news_category_id='$tbl_news_category_id' ";
		$this->cf->deleteFrom($qry);
	}
	
	
	function is_exist_news_category($tbl_news_category_id, $category_name_en, $category_name_ar){
		$sql4existNews = "SELECT tbl_news_category_id from " . TBL_NEWS_CATEGORY . " where (category_name_en='$category_name_en' OR category_name_ar = '$category_name_ar') 
		and tbl_news_category_id<>'$tbl_news_category_id'";
		if ($this->cf->isExist($sql4existNews)){
			return "Y";
		}else{	
			return "N";
		}
	}

    function update_news_category($tbl_news_category_id, $category_name_en, $category_name_ar)
	{
				$sql4sess = "UPDATE " . TBL_NEWS_CATEGORY . " SET 
				category_name_en='$category_name_en',category_name_ar='$category_name_ar' where tbl_news_category_id='$tbl_news_category_id'";
				//echo $sql4sess; exit;
				$this->cf->update($sql4sess);
	}
	
	
	/************************************ END NEWS CATEGORY *****************************************************/
	
	
	 /************************* book packages*****************************************************/
	
	function get_all_book_packages($sort_name, $sort_by, $offset, $q="",$list ='', $is_active='') {
	
		$qry = "SELECT * FROM ".TBL_BOOK_PACKAGE;
		if($is_active<>"")
		{
			$qry .= " WHERE is_active='Y' ";
		}
		$qry .= " ORDER BY package_name_en ASC ";
		if($list<>"Y")
		{
			$qry .= " LIMIT $offset , ".PAGING_TBL_BOOKS_CATEGORY;
		}
		//echo $qry;
		$results = $this->cf->selectMultiRecords($qry);
		
	return $results;
	}
	
	
	function get_total_book_packages($q="") {
	
		$qry = "SELECT * FROM ".TBL_BOOK_PACKAGE." WHERE is_active='Y' ";
		$qry .= " ORDER BY package_name_en ASC ";
		//echo $qry;
		$results = $this->cf->selectMultiRecords($qry);
	return count($results);
	}
	
	

	function save_book_package($tbl_book_package_id, $package_name_en, $package_name_ar, $book_point, $age_group){
		        
		$sql4CategoryPic   = "SELECT file_name_updated as picture from " . TBL_UPLOADS . " where tbl_item_id = '$tbl_book_package_id' ";
		$sql4DelUpload     = "DELETE FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_book_package_id' ";
		$results = $this->cf->selectMultiRecords($sql4CategoryPic);
		if(count($results)>0){
			$picture = $results[0]['picture'];
			$this->cf->deleteFrom($sql4DelUpload);
		}else{
			$picture = "";
		}
		
		$sql4sess = "INSERT INTO " . TBL_BOOK_PACKAGE . "(tbl_book_package_id, package_name_en, package_name_ar, picture, added_date,point, age_group) 
					 VALUES('$tbl_book_package_id','$package_name_en','$package_name_ar', '$picture', NOW(), '$book_point', '$age_group')";
		$sql4userSession = "SELECT tbl_book_package_id from " . TBL_BOOK_PACKAGE . " where (package_name_en='$package_name_en' or package_name_ar= '$package_name_ar')  ";
	   
		if ($this->cf->isExist($sql4userSession)){
			return "N";
			  // $this->deleteFrom($sql4DelUserSession);
		}else{	
			$this->cf->insertInto($sql4sess);
			return "Y";
		}
	}


	function getPackageDetails($tbl_book_package_id) {
		$qry = "SELECT * FROM ".TBL_BOOK_PACKAGE." WHERE tbl_book_package_id='$tbl_book_package_id'";
		//echo $qry."<br />";
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	/** Activate book package ***/
	function activate_book_package($tbl_book_package_id) {
		$qry = "UPDATE ".TBL_BOOK_PACKAGE." SET is_active='Y' WHERE tbl_book_package_id='$tbl_book_package_id' ";
		$this->cf->update($qry);
	}

    /** Activate book package***/
	function deactivate_book_package($tbl_book_package_id) {
		$qry = "UPDATE ".TBL_BOOK_PACKAGE." SET is_active='N' WHERE tbl_book_package_id='$tbl_book_package_id' ";
		$this->cf->update($qry);
	}
	
	 /** Delete book category***/
	function delete_book_package($tbl_book_package_id) {
		/*$qry = "DELETE FROM ".TBL_BOOK_PACKAGE." WHERE tbl_book_package_id='$tbl_book_package_id' ";
		$this->cf->deleteFrom($qry);*/
		$qry = "UPDATE ".TBL_BOOK_PACKAGE." SET is_active='D' WHERE tbl_book_package_id='$tbl_book_package_id' ";
		$this->cf->update($qry);
	}
	
	
	function is_exist_book_package($tbl_book_package_id, $package_name_en, $package_name_ar){
		$sql4existBook = "SELECT tbl_book_package_id from " . TBL_BOOK_PACKAGE . " where (package_name_en='$package_name_en' OR package_name_ar = '$package_name_ar') 
		and tbl_book_package_id<>'$tbl_book_package_id'";
		if ($this->cf->isExist($sql4existBook)){
			return "Y";
		}else{	
			return "N";
		}
	}

    function update_book_package($tbl_book_package_id, $package_name_en, $package_name_ar, $book_point, $age_group)
	{
		$sql4NewsPic   = "SELECT file_name_updated as picture from " . TBL_UPLOADS . " where tbl_item_id = '$tbl_book_package_id' ";
		$sql4DelUpload = "DELETE FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_book_package_id' ";
		$results = $this->cf->selectMultiRecords($sql4NewsPic);
		if(count($results)>0){
			$picture = $results[0]['picture'];
			$this->cf->deleteFrom($sql4DelUpload);
		}else{
			$picture = "";
		}	
				
		if($picture<>"")
		{	
			$sql4sess = "UPDATE " . TBL_BOOK_PACKAGE . " SET 
				package_name_en='$package_name_en',package_name_ar='$package_name_ar',picture='$picture',point = '$book_point', age_group='$age_group' where tbl_book_package_id='$tbl_book_package_id'";
			//echo $sql4sess; exit;
			$this->cf->update($sql4sess);
		
		}else{
			$sql4sess = "UPDATE " . TBL_BOOK_PACKAGE . " SET 
				package_name_en='$package_name_en',package_name_ar='$package_name_ar',point = '$book_point',age_group='$age_group' where tbl_book_package_id='$tbl_book_package_id'";
			//echo $sql4sess; exit;
			$this->cf->update($sql4sess);
		}
	}
	
	function get_book_package_picture($tbl_book_package_id) {
		$qry = "SELECT picture FROM ".TBL_BOOK_PACKAGE." WHERE tbl_book_package_id='$tbl_book_package_id' ";
		$rs = $this->cf->selectMultiRecords($qry);
	    return $rs;
	}
	
	function update_book_package_pic($files, $tbl_book_package_id) {

		if ($files["file_category_pic"]["name"] != "") {
			//Image Upload
			$source_img =  $files["file_category_pic"]["tmp_name"];
			$orignal_filename_img = $files["file_category_pic"]["name"];
			$Ext = strchr($orignal_filename_img,".");
			$Ext = strtolower($Ext);
			$file_type = $_FILES["file_category_pic"]["type"];
			$file_size = $_FILES["file_category_pic"]["size"];
	
			if ($Ext==".jpg" || $Ext==".jpeg" || $Ext==".gif" || $Ext==".bmp" || $Ext==".png") {
				$file_category_pic_name = substr(md5(uniqid(rand())),0,15);
				$file_category_pic_name = $file_category_pic_name.$Ext;
				$copy = $this->cf->file_copy("$source_img", FILE_UPLOAD_PATH."/books/", "$file_category_pic_name", 1, 1);
			} else {
				$MSG = "Image extension is not valid.<br>";
				return $MSG;
			}
		}
		
		$tbl_uploads_id = substr(md5(uniqid(rand())),0,15);
		$tbl_item_id    = $tbl_book_package_id;
		$qry = "INSERT INTO ".TBL_UPLOADS." (`tbl_uploads_id` ,`tbl_item_id` ,`file_name_original` ,`file_name_updated` ,`file_type` ,`file_size` ,`is_active` ,`added_date`
			)
			VALUES (
				'$tbl_uploads_id', '$tbl_item_id', '$orignal_filename_img', '$file_category_pic_name', '$file_type', '$file_size', 'Y', NOW()
			)";
		$this->cf->insertInto($qry);
		$MSG = "Picture uploaded successfully.";
	    return $MSG;	
	}
	
	function get_book_package_picture_upload($tbl_book_package_id) {
		$qry = "SELECT  file_name_updated AS picture FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_book_package_id' ";
		$rs = $this->cf->selectMultiRecords($qry);
	    return $rs;
	}
	
	
	
	function get_total_books_in_package($q,$tbl_book_package_id='',$is_active='') {
		$qry = "SELECT AB.tbl_book_id FROM ".TBL_PACKAGE_ASSIGNED_BOOKS." AS AB  WHERE 1 = 1  ";
		if($is_active<>"")
		{
			$qry .= " AND AB.is_active='Y' ";
		}
		$qry .= " AND AB.is_active <> 'D' ";
		
		if($tbl_book_package_id<>"")
		{
			$qry .= " AND AB.tbl_book_package_id= '".$tbl_book_package_id."' " ;
		}
	
		$results = $this->cf->selectMultiRecords($qry);
	return count($results);
	}
	
	function assign_books_to_package($tbl_book_id,$tbl_book_package_id)
	{
		
		$qry = "SELECT  tbl_package_assigned_book_id FROM ".TBL_PACKAGE_ASSIGNED_BOOKS." WHERE tbl_book_id='$tbl_book_id' AND tbl_book_package_id= '$tbl_book_package_id' AND is_active<>'D' ";
		$rs = $this->cf->selectMultiRecords($qry);
		$tbl_package_assigned_book_id = substr(md5(uniqid(rand())),0,15);
	    if(count($rs)==0)
		{
			$qry = "INSERT INTO ".TBL_PACKAGE_ASSIGNED_BOOKS." (`tbl_package_assigned_book_id` ,`tbl_book_id` ,`tbl_book_package_id` )
			        VALUES ('$tbl_package_assigned_book_id', '$tbl_book_id', '$tbl_book_package_id')";
		    $this->cf->insertInto($qry);
		}
		return "Y";
	}
	
	function update_age_category_to_package($tbl_book_package_id,$user_age_group)
	{
		$sql4sess = "UPDATE " . TBL_BOOK_PACKAGE . " SET  age_group = '$user_age_group' WHERE  tbl_book_package_id='$tbl_book_package_id'";
		$this->cf->update($sql4sess);
	}
	
	function del_assign_book_from_package($tbl_package_assigned_book_id,$tbl_book_package_id)
	{
		$sql4sess = "DELETE FROM " . TBL_PACKAGE_ASSIGNED_BOOKS . " WHERE tbl_package_assigned_book_id= '$tbl_package_assigned_book_id' AND tbl_book_package_id='$tbl_book_package_id'";
		$this->cf->deleteFrom($sql4sess);
	}
	
	
	
	function get_package_info($tbl_book_package_id)
	{
		$qry = "SELECT  * FROM ".TBL_BOOK_PACKAGE." WHERE tbl_book_package_id = '$tbl_book_package_id' ";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
	
	
	function get_assigned_books_of_package($tbl_book_package_id)
	{
		$qry = "SELECT  B.tbl_book_id,B.title_en,B.title_ar, PAB.tbl_package_assigned_book_id FROM  ".TBL_PACKAGE_ASSIGNED_BOOKS." AS PAB LEFT JOIN  ".TBL_BOOKS." AS B ON  B.tbl_book_id = PAB.tbl_book_id
		        WHERE PAB.tbl_book_package_id= '$tbl_book_package_id' AND PAB.is_active<>'D' ";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
	
	
	
	/************************* end book packages*****************************************************/
	
	
	

}
?>