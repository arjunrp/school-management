<?php
include_once('include/common_functions.php');
/**
* @desc   	  	Student Group Model
* @category   	Model
* @author     	Shanavas PK
* @version    	0.1
*/

class Model_student_group extends CI_Model {
	var $cf;
	/**
	* @desc Default constructor for the Controller
	* @access default
	*/
	function model_student_group() {
		$this->cf = new Common_functions();
	}
	
	// List of Student Groups
	function get_list_student_groups($tbl_school_id,$is_active)	
	{
	  $qry = " SELECT * FROM ".TBL_STUDENT_GROUP."  AS PG WHERE 1 ";
	  if($tbl_school_id<>"")
		{
			$qry .= " AND PG.tbl_school_id= '".$tbl_school_id."' ";
		}
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PG.is_active='$is_active' ";
		}
	  $qry .= " AND PG.is_active<>'D' "; 
	  $qry .= " ORDER BY PG.group_name_en ASC  ";
	  $results = $this->cf->selectMultiRecords($qry);
	  return $results;
	  
	}
	
	
	//added by shanavas 25_01_2016
	//list students group
	function get_list_students_of_group($tbl_school_id,$tbl_user_id, $role='') {
		 $qry = "SELECT distinct GROUPS.group_name_en, GROUPS.group_name_ar, GROUPS.tbl_student_group_id FROM ".TBL_STUDENT_GROUP_STUDENTS." AS STUDENTS LEFT JOIN  ".TBL_STUDENT_GROUP." AS GROUPS ON GROUPS.tbl_student_group_id=STUDENTS.tbl_student_group_id WHERE GROUPS.tbl_school_id='".$tbl_school_id."' "; 
		 if($role=="ST"){
		 	$qry .= " AND  STUDENTS.tbl_student_id='".$tbl_user_id."' ";
		 }else if($role=="T")
		 {
			$qry .= " AND  GROUPS.added_by='".$tbl_user_id."' "; 
		 }
		
		 $rs = $this->cf->selectMultiRecords($qry);
		 return $rs;		
	}
	
	//list topics
	function get_list_student_topics($tbl_school_id,$tbl_student_id,$tbl_student_group_id,$tbl_class_id='') {
		 $qry .= "SELECT tbl_student_forum_id, forum_topic, item_id,posted_by,added_date,is_status FROM ".TBL_STUDENT_FORUM." WHERE tbl_student_group_id = '".$tbl_student_group_id."'  AND tbl_school_id='".$tbl_school_id."'  ";
		 if($tbl_class_id<>""){
			$qry .= " AND tbl_student_group_id IN( SELECT tbl_student_group_id FROM ".TBL_STUDENT_GROUP_STUDENTS." WHERE tbl_class_id ='".$tbl_class_id."' ) ";
		 }
		 $qry .= " order by added_date DESC  ";
		 //echo $qry; exit;
		 $rs = $this->cf->selectMultiRecords($qry);
		 return $rs;
	}


   	//list forum comments
	function get_list_forum_comments($tbl_school_id,$tbl_student_id,$tbl_student_forum_id) {
		 $qry = "SELECT tbl_student_comment_id, forum_comment, item_id, commented_by, added_date, is_status FROM ".TBL_STUDENT_FORUM_COMMENTS." WHERE tbl_student_forum_id ='".$tbl_student_forum_id."'  AND tbl_school_id='".$tbl_school_id."' ";
		 $rs = $this->cf->selectMultiRecords($qry);
		 return $rs;
	}


	//add topics
	function saveStudentGroupForum($school_id,$tbl_student_group_id,$message,$item_id,$tbl_student_id){
	  $tbl_student_forum_id = substr(md5(uniqid(rand())),0,10);
	  $qry = "INSERT INTO ".TBL_STUDENT_FORUM." (
			`tbl_student_forum_id` ,
			`forum_topic` ,
			`tbl_student_group_id` ,
			`tbl_school_id` ,
			`item_id` ,
			`posted_by`,
			`added_date`,
			`is_status`
			)
			VALUES (
				'$tbl_student_forum_id', '$message', '$tbl_student_group_id', '$school_id', '$item_id', '$tbl_student_id', NOW(),'Y'
			)";
		$rs = $this->cf->insertInto($qry);
		$MSG = "Success";	
		$arr['msg'] = $MSG;
		return $arr;
	}

	// save forum comments
	function saveStudentForumComments($school_id,$tbl_student_forum_id,$comments,$item_id,$tbl_student_id){
	  $tbl_student_comment_id = substr(md5(uniqid(rand())),0,10);
	  $qry = "INSERT INTO ".TBL_STUDENT_FORUM_COMMENTS." (
			`tbl_student_comment_id` ,
			`forum_comment` ,
			`tbl_student_forum_id` ,
			`tbl_school_id` ,
			`item_id` ,
			`commented_by`,
			`added_date`,
			`is_status`
			)
			VALUES (
				'$tbl_student_comment_id', '$comments', '$tbl_student_forum_id', '$school_id', '$item_id', '$tbl_student_id', NOW(),'Y'
			)";
		$rs = $this->cf->insertInto($qry);
		$MSG = "Success";	
		$arr['msg'] = $MSG;
		return $arr;
	}

   
   function get_studentgroup_byforum($tbl_school_id,$tbl_student_forum_id) {
		 $qry .= "SELECT tbl_student_group_id FROM ".TBL_STUDENT_FORUM." WHERE tbl_student_forum_id ='".$tbl_student_forum_id."'  AND tbl_school_id='".$tbl_school_id."' and is_status='Y' ";
		 //echo $qry; exit;
		 $rs = $this->cf->selectMultiRecords($qry);
		 return $rs;
	}


	function get_list_student_group_members($tbl_school_id,$tbl_student_id,$tbl_student_group_id) {
		 $qry = " SELECT first_name,first_name_ar,last_name,last_name_ar,tbl_student_id FROM ".TBL_STUDENT." WHERE tbl_student_id IN (
		  SELECT STUDENTS.tbl_student_id FROM ".TBL_STUDENT_GROUP_STUDENTS." AS STUDENTS LEFT JOIN  ".TBL_STUDENT_GROUP." AS GROUPS ON GROUPS.tbl_student_group_id=STUDENTS.tbl_student_group_id WHERE STUDENTS.tbl_school_id='".$tbl_school_id."' 
		  and STUDENTS.tbl_student_group_id= '".$tbl_student_group_id."' )";
		 $rs = $this->cf->selectMultiRecords($qry);
		 return $rs;		
	}
	
	
	
	
	// START FUNCTION FOR STUDENT COMMUNITY MESSAGE - CHAT 
	
	function list_my_students_group($tbl_school_id, $tbl_student_id, $tbl_teacher_id ) {
		
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		
		$qry = "SELECT * FROM ".TBL_STUDENT_GROUP." AS SG LEFT JOIN ".TBL_STUDENT_GROUP_STUDENTS."  AS SGS ON SGS.tbl_student_group_id = SG.tbl_student_group_id WHERE 1 ";
		
		if($tbl_school_id<>"")
		{
			$qry .= " AND SG.tbl_school_id= '".$tbl_school_id."' ";
		}
		if($tbl_student_id<>"")
		{
			$qry .= " AND SGS.tbl_student_id= '".$tbl_student_id."' ";
		}
		if($tbl_teacher_id<>"")
		{
			$qry .= " AND SG.added_by= '".$tbl_teacher_id."' ";
		}
	
		//Active/Deactive
		$qry .= " AND SG.is_active='Y' ";
		$qry .= " GROUP BY SG.tbl_student_group_id";
		$qry .= " ORDER BY SG.id DESC";

        if($offset<>"")
			$qry .=" LIMIT $offset, ".TBL_STUDENT_GROUP_PAGING;
		
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	
	function get_students_list_in_group($tbl_student_group_id,$tbl_school_id) {
	    $qry = " SELECT tbl_student_id, first_name, first_name_ar, last_name, last_name_ar,file_name_updated FROM ".TBL_STUDENT." WHERE is_active='Y' AND tbl_school_id='$tbl_school_id' 
		         AND tbl_student_id IN (SELECT tbl_student_id FROM ".TBL_STUDENT_GROUP_STUDENTS." WHERE tbl_student_group_id='".$tbl_student_group_id."') ORDER BY first_name ASC ";
		
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
	}
	
	
	
	function get_student_community_messages($logged_user_id, $role, $tbl_school_id, $message_mode='', $tbl_student_group_id='', $message_to='') 
	{
	    if($message_mode=="P")  // P - Private
		{
			$qry = "SELECT * FROM ".TBL_STUDENT_COMMUNITY_MESSAGE. " WHERE is_active='Y' AND tbl_school_id='".$tbl_school_id."'";
			$qry .= " AND (( message_from='".$logged_user_id."' AND  message_to ='".$message_to."' ) ";
			$qry .= " OR ( message_from='".$message_to."' AND message_to ='".$logged_user_id."' ) ) ";
			$qry .= "  AND  message_mode = '".$message_mode."' ";
			
		}else{
			// G - Group
			$qry = "SELECT * FROM ".TBL_STUDENT_COMMUNITY_MESSAGE. " WHERE is_active='Y' AND tbl_school_id='".$tbl_school_id."'";
			if($role=="ST")
			{
				$qry .= " AND ( message_from='".$logged_user_id."' OR  message_to='".$logged_user_id."' ) ";
			}
			
			$qry .= "  AND  message_mode = '".$message_mode."' ";
			if($tbl_student_group_id<>"")
			{
				$qry .= "  AND tbl_student_group_id = '".$tbl_student_group_id."' ";
			}
		}
		$qry .= " GROUP BY tbl_message_id";
		$qry .= " ORDER BY id DESC ";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
	
	
	
	function save_student_community_message($message_from, $message_to, $message_type, $tbl_message, $tbl_school_id, $tbl_item_id, $message_mode, $tbl_student_group_id='', $message_dir) {
		$message_from 		= $this->cf->get_data(trim($message_from));
		$message_to 		= $this->cf->get_data(trim($message_to));
		$tbl_student_id 	= $this->cf->get_data(trim($tbl_student_id));
		$message_type 		= $this->cf->get_data(trim($message_type));
		$tbl_message_id 	= substr(md5(uniqid(rand())),0,10);
		
		if($tbl_student_group_id<>"")
		{
				$qryStudents = "SELECT SGS.tbl_student_id FROM ".TBL_STUDENT_GROUP_STUDENTS."  AS SGS LEFT JOIN ".TBL_STUDENT." AS S ON S.tbl_student_id=SGS.tbl_student_id  WHERE 1 ";
		
				if($tbl_school_id<>"")
				{
					$qryStudents .= " AND SGS.tbl_school_id= '".$tbl_school_id."' ";
				}
				if($tbl_student_group_id<>"")
				{
					$qryStudents .= " AND SGS.tbl_student_group_id= '".$tbl_student_group_id."' ";
				}
                $qryStudents .= " AND SGS.tbl_student_id <> '".$message_from."' ";
				$qryStudents .= " AND S.is_active='Y' ";
				$qryStudents .= " ORDER BY SGS.id DESC";
		        $rsStudents   = $this->cf->selectMultiRecords($qryStudents);
			for($k=0;$k<count($rsStudents);$k++)
			{
				$message_to = $rsStudents[$k]['tbl_student_id'];	
				$qry = "INSERT INTO ".TBL_STUDENT_COMMUNITY_MESSAGE." (`tbl_message_id`, `message_from`, `message_to`,`tbl_student_group_id`, `message_mode`, `message_type`, `message`, `tbl_item_id`, `added_date`, `tbl_school_id`, `message_dir`)
					VALUES ('$tbl_message_id', '$message_from', '$message_to', '$tbl_student_group_id', '$message_mode', '$message_type', '$tbl_message', '$tbl_item_id', NOW(), '$tbl_school_id', '$message_dir') ";
				$this->cf->insertInto($qry);
			}
			
		}else{
			$qry = "INSERT INTO ".TBL_STUDENT_COMMUNITY_MESSAGE." (`tbl_message_id`, `message_from`, `message_to`,`tbl_student_group_id`, `message_mode`, `message_type`, `message`, `tbl_item_id`, `added_date`, `tbl_school_id`)
					VALUES ('$tbl_message_id', '$message_from', '$message_to', '$tbl_student_group_id', '$message_mode', '$message_type', '$tbl_message', '$tbl_item_id', NOW(), '$tbl_school_id') ";
		    $this->cf->insertInto($qry);
		}
		
		if (trim($_POST["is_admin"]) == "Y" && ($message_type == "image" || $message_type == "audio")) {
			$tbl_uploads_id = substr(md5(uniqid(rand())),0,10);
			$file_name_original = $_POST["file_name_original"];
			$file_name_updated = $_POST["file_name_updated"];
			$file_name_updated_thumb = $_POST["file_name_updated_thumb"];
			$file_type = $_POST["file_type"];
			$file_size = $_POST["file_size"];
			
			
			$qry = "INSERT INTO ".TBL_UPLOADS." (
					`tbl_uploads_id` ,
					`module_name` ,
					`tbl_item_id` ,
					`file_name_original` ,
					`file_name_updated` ,
					`file_name_updated_thumb` ,
					`file_type` ,
					`file_size` ,
					`is_active` ,
					`added_date`
					)
					VALUES (
						'$tbl_uploads_id', '$module_name', '$tbl_item_id', '$file_name_original', '$file_name_updated','$file_name_updated_thumb', '$file_type', '$file_size', 'Y', NOW()
					)";
			$this->cf->insertInto($qry);
			echo $qry;
		}
	}

	
	
	// END FUNCTION FOR STUDENT COMMUNITY MESSAGE - CHAT
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
}



?>







