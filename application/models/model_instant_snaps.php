<?php
include_once('include/common_functions.php');

/**
 * @desc   	  	Instant Snaps Model
 *
 * @category   	Model
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Model_instant_snaps extends CI_Model {
	var $cf;


	/**
	* @desc Default constructor for the Controller
	*
	* @access default
	*/
    function model_instant_snaps() {
		$this->cf = new Common_functions();
    }


	/**
	* @desc Function to return instant snaps questions
	*
	* @access default
	*/
    function get_children_instant_snaps($tbl_parent_id, $tbl_student_id) {
		$this->cf = new Common_functions();
		
		//$qry = "SELECT * FROM ".TBL_INSTANT_PICS." WHERE is_active='Y' AND tbl_student_id IN (SELECT tbl_student_id FROM ".TBL_PARENT_STUDENT." WHERE tbl_parent_id='".$tbl_parent_id."') ORDER BY added_date DESC LIMIT 0,10";
		//echo "----->".$qry;
		$qry = "SELECT * FROM ".TBL_INSTANT_PICS." WHERE is_active='Y' AND tbl_student_id ='".$tbl_student_id."' ORDER BY added_date DESC LIMIT 0,10";
		$data_rs = $this->cf->SelectMultiRecords($qry);
		return $data_rs;
	}
	
	
	/**
	* @desc Function to return a picture
	*
	* @access default
	*/
    function get_snap($tbl_uploads_id) {
		$this->cf = new Common_functions();
		
		$qry = "SELECT file_name_updated FROM ".TBL_UPLOADS." WHERE is_active='Y' AND tbl_uploads_id='".$tbl_uploads_id."'";
		//echo "----->".$qry;
		$data_rs = $this->cf->SelectMultiRecords($qry);
		return $data_rs[0]["file_name_updated"];
	}
    }
?>