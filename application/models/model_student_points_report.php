<?php

include_once('include/common_functions.php');
/** @desc -- Student Attendance Report Model */

class Model_student_points_report extends CI_Model {
     var $cf;
     
	/** @desc Default constructor for the Controller	*/

    function model_student_points_report() {

		$this->cf = new Common_functions();

    }
	
	function getPointsInformation($tbl_student_id, $date_from, $date_to, $tbl_school_id, $tbl_teacher_id='',$tbl_class_id)
	{
			$qry = "SELECT P.tbl_class_id, SUM(P.points_student) AS points_student,P.comments_student
			        FROM ".TBL_POINTS." AS P 
					WHERE 
			        P.tbl_student_id='$tbl_student_id' AND P.tbl_class_id='$tbl_class_id' "; 
			if($tbl_teacher_id<>""){
				$qry .= " AND P.tbl_teacher_id='$tbl_teacher_id' ";
			}
            $qry .= "  AND P.tbl_school_id='$tbl_school_id' ";
			if($date_to<>"")
			{
				$qry .=  " AND (P.added_date>='$date_from' OR P.added_date<='$date_to') "; 
			}else{
				$qry .=  " AND P.added_date ='$date_from' ";
			}
			
			$qry .= "AND P.is_active='Y' GROUP BY P.comments_student order by P.comments_student ASC,  P.added_date ASC ";
		//echo $qry; exit;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;	
	}
}

?>



