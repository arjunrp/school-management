<?php
include_once('include/common_functions.php');
/**
 * @DESC   	  	Message Model
 * @category   	Model
 * @author     	Shanavas .PK
 * @version    	0.1
 */

class Model_message extends CI_Model {
	var $cf;
	/**
	* @DESC Default constructor for the Controller
	* @access default
	*/

    function model_message() {
		$this->cf = new Common_functions();
    }

	/**
	* @DESC		Save message
	* @param	String $message_from, $message_to, $tbl_student_id, $message_type, $message
	* @access	default
	* @return	none
	*/
	function save_message($message_from, $message_to, $tbl_student_id, $message_type, $message, $tbl_school_id, $j, $tbl_class_id, $message_mode, $tbl_message_group_id) {
		$message_from 				= $this->cf->get_data(trim($message_from));
		$message_to 				= $this->cf->get_data(trim($message_to));
		$tbl_student_id 			= $this->cf->get_data(trim($tbl_student_id));
		$message_type 				= $this->cf->get_data(trim($message_type));
		$message 					= $this->cf->get_data(trim($message));
		$tbl_message_id 			= $_REQUEST["tbl_item_id"];		// This will be tbl_message_id if sent by user
		if ($tbl_message_id == "") {
			$tbl_message_id = substr(md5(uniqid(rand())),0,10);
		} else {
			$tbl_message_id_arr = explode(":", $tbl_message_id);
			$tbl_message_id = $tbl_message_id_arr[$j];
		}
		
		//$message_type = $_POST["message_type"];
		//echo "message_type". $message_type;
		//exit();
		if (trim($message_type) == "picture") {
			$message = $_POST["picture_caption"];
		}

		//for($j=0; $j<count($tbl_message_id_arr); $j++) {
			if ($tbl_message_id == "") {continue;}
			    $qry = "INSERT INTO ".TBL_MESSAGE." (`tbl_message_id`, `message_from`, `message_to`, `tbl_student_id`, `message_type`, `message`, `added_date`, `tbl_school_id`, `tbl_class_id`, `message_mode`, `tbl_message_group_id`)
					VALUES ('$tbl_message_id', '$message_from', '$message_to', '$tbl_student_id', '$message_type', '$message', NOW(), '$tbl_school_id', '$tbl_class_id', '$message_mode', '$tbl_message_group_id') ";
			$this->cf->insertInto($qry);
		//}
		
		
		if ($message_type == "picture" || $message_type == "audio") {
			$tbl_uploads_id = substr(md5(uniqid(rand())),0,10);
			$file_name_original = $_POST["file_name_original"];
			$file_name_updated = $_POST["file_name_updated"];
			$file_name_updated_thumb = $_POST["file_name_updated_thumb"];
			$file_type = $_POST["file_type"];
			$file_size = $_POST["file_size"];
			
			
			$qry = "INSERT INTO ".TBL_UPLOADS." (
					`tbl_uploads_id` ,
					`module_name` ,
					`tbl_item_id` ,
					`file_name_original` ,
					`file_name_updated` ,
					`file_name_updated_thumb` ,
					`file_type` ,
					`file_size` ,
					`is_active` ,
					`added_date`
					)
					VALUES (
						'$tbl_uploads_id', '$module_name', '$tbl_message_id', '$file_name_original', '$file_name_updated','$file_name_updated_thumb', '$file_type', '$file_size', 'Y', NOW()
					)";
			$this->cf->insertInto($qry);
		}
	}

	/**
	* @DESC		Get all message
	* @param	String $tbl_parent_id
	* @access	default
	* @return	none
	*/
	function get_all_messages($tbl_parent_or_teacher_id, $tbl_student_id) { 
		$tbl_parent_or_teacher_id = $this->cf->get_data(trim($tbl_parent_or_teacher_id));
		$qry = "SELECT * FROM ".TBL_MESSAGE." WHERE message_from <>'' AND message_to='$tbl_parent_or_teacher_id' AND is_active='Y' AND tbl_student_id='$tbl_student_id' ORDER BY added_date DESC LIMIT 0, ".PAGING_MESSAGES;
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
	return $rs;
	}

   function get_school_parent_messages($tbl_parent_or_teacher_id,$currentDate='',$prevDate='') { 
		$tbl_parent_or_teacher_id = $this->cf->get_data(trim($tbl_parent_or_teacher_id));
		$qry = "SELECT * FROM ".TBL_MESSAGE_PARENT." WHERE message_from <>'' AND message_to='$tbl_parent_or_teacher_id' AND is_active='Y' ";
		$qry .= " AND (added_date >= '".$prevDate." 00:00:00' and added_date <='".$currentDate." 23:59:60' ) " ;
		$qry .= " ORDER BY added_date DESC LIMIT 0, ".PAGING_MESSAGES;
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
	return $rs;
	}

	/**
	* @DESC		Ger points detail
	* @param	String $tbl_student_id
	* @access	default
	* @return	none
	*/

	function get_points_detail($tbl_student_id) {
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		$qry = "SELECT * FROM ".TBL_POINTS." WHERE tbl_student_id='$tbl_student_id' AND is_active='Y' ORDER BY added_date DESC LIMIT 0, ".PAGING_POINTS;
		$rs = $this->cf->selectMultiRecords($qry);
	return $rs;
	}
	
	/**
	* @DESC		Get cards detail
	* @param	String $tbl_student_id
	* @access	default
	* @return	none
	*/
	function get_cards_detail($tbl_student_id) {
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		$qry = "SELECT * FROM ".TBL_CARDS." WHERE tbl_student_id='$tbl_student_id' AND is_active='Y' ORDER BY added_date DESC LIMIT 0, ".PAGING_CARDS;
		//$qry = "SELECT * FROM ".TBL_CARDS." WHERE tbl_student_id='$tbl_student_id' AND is_active='Y' ORDER BY added_date ";
		//echo "<br>".$qry;
		$rs = $this->cf->selectMultiRecords($qry);
	return $rs;
	}


	/**
	* @DESC		Get cards messages detail
	* @param	String $tbl_student_id
	* @access	default
	* @return	none
	*/
	function get_card_messages($tbl_student_id, $card_type) { 
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		$card_type = $this->cf->get_data(trim($card_type));
		$qry = "SELECT * FROM ".TBL_CARDS." WHERE tbl_student_id='$tbl_student_id' AND card_type = '$card_type' AND is_active='Y' ORDER BY added_date DESC";
		$rs = $this->cf->selectMultiRecords($qry);
	return $rs;
	}

	function get_non_cancel_cards($tbl_student_id, $tbl_semester_id='', $card_type='') { 
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		$card_type = $this->cf->get_data(trim($card_type));
		$qry = "SELECT * FROM ".TBL_CARDS." WHERE tbl_student_id='$tbl_student_id' AND card_type = '$card_type' AND is_active='Y' AND tbl_semester_id='$tbl_semester_id'
				AND tbl_card_id NOT IN  (SELECT tbl_card_id FROM ".TBL_CARDS." WHERE tbl_student_id='$tbl_student_id' AND card_type = '$card_type' AND card_issue_type='cancel')
		        ORDER BY added_date DESC";
		$rs = $this->cf->selectMultiRecords($qry);
	return $rs;
	}

    function get_cancelled_cards($tbl_student_id, $tbl_semester_id='', $card_type='') { 
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		$card_type = $this->cf->get_data(trim($card_type));
		$qry = "SELECT * FROM ".TBL_CARDS." WHERE tbl_student_id='$tbl_student_id' AND is_active='Y' AND tbl_semester_id='$tbl_semester_id' AND card_issue_type='cancel' ";
		if($card_type<>"") 
			$qry .= " AND card_type = '$card_type' "; 
		$qry .= "ORDER BY added_date DESC";
		$rs = $this->cf->selectMultiRecords($qry);
	return $rs;
	}

	/**
	* @DESC		Save leave application
	* @param	String $tbl_student_id
	* @access	default
	* @return	none
	*/

	function save_leave_application($tbl_teacher_id, $tbl_student_id, $tbl_parent_id, $application_message, $tbl_school_id, $start_date, $end_date) {
		$tbl_teacher_id = $this->cf->get_data(trim($tbl_teacher_id));
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		$tbl_parent_id = $this->cf->get_data(trim($tbl_parent_id));
		$application_message = $this->cf->get_data(trim($application_message));
		$tbl_leave_application_id = substr(md5(uniqid(rand())),0,10);

		$qry = "INSERT INTO ".TBL_LEAVE_APPLICATION." (`tbl_leave_application_id`, `tbl_teacher_id`, `tbl_student_id`, `tbl_parent_id`, `comments_parent`, `start_date`, `end_date`, `is_approved`, `added_date`, `tbl_school_id`)
				VALUES ('$tbl_leave_application_id', '$tbl_teacher_id', '$tbl_student_id', '$tbl_parent_id', '$application_message', '$start_date', '$end_date', 'N',  NOW(), '$tbl_school_id') ";
		//echo $qry;
		$this->cf->insertInto($qry);
	}
	
	function is_exist_leave_application($tbl_student_id, $tbl_school_id, $start_date, $end_date)
	{
		$qry = "SELECT * FROM ".TBL_LEAVE_APPLICATION." WHERE tbl_student_id='".$tbl_student_id."' AND tbl_school_id = '".$tbl_school_id."' 
		        AND  ( (start_date BETWEEN  '".$start_date."' AND  '".$end_date."') OR  ( end_date BETWEEN  '".$start_date."' AND  '".$end_date."') ) ";
		$rs = $this->cf->selectMultiRecords($qry);
		return count($rs);
		
	}

	/**
	* @DESC		Get all leave applications for which still the end_date has not elapsed
	* @param	String $tbl_student_id
	* @access	default
	* @return	none
	*/

	function get_leave_applications($tbl_teacher_id, $today_date) {
		//added_date DESC
		$qry = "SELECT * FROM ".TBL_LEAVE_APPLICATION." WHERE tbl_teacher_id='".$tbl_teacher_id."'  AND  (start_date>= '".$today_date."' OR end_date>='".$today_date."') ORDER BY start_date ASC ";
		//$qry = "SELECT * FROM ".TBL_STUDENT." WHERE 1";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}

	/**
	* @DESC		Save Message sent to teacher
	* @param	String $tbl_student_id
	* @access	default
	* @return	none
	*/

	function save_message_teacher($message_from, $message_to, $message, $tbl_school_id) {
		$message_from = $this->cf->get_data(trim($message_from));
		$message_to = $this->cf->get_data(trim($message_to));
		$message = $this->cf->get_data(trim($message));
		$tbl_message_teacher_id = substr(md5(uniqid(rand())),0,10);

		$qry = " INSERT INTO ".TBL_MESSAGE_TEACHER." (
			`tbl_message_teacher_id` ,
			`message_from` ,
			`message_to` ,
			`message` ,
			`added_date` ,
			`is_active` ,
			`tbl_school_id`
			)
			VALUES (
				'$tbl_message_teacher_id', '$message_from', '$message_to', '$message', NOW(), 'Y', '$tbl_school_id'
			)";
		//echo $qry;
		$this->cf->insertInto($qry);			
	}

	/**
	* @DESC		Save teacher messages
	* @param	String $tbl_student_id
	* @access	default
	* @return	none
	*/

	function get_teacher_messages($tbl_teacher_id_from, $tbl_teacher_id_to, $tbl_teacher_group_id,$tbl_school_id='') {
		// Get teacher messages sent from ministry
		if ($tbl_teacher_id_from == "ministry") {
			$qry = "SELECT * FROM ".TBL_NOTIFICATION_RPT_TEACHERS. " WHERE tbl_notification_rpt_id IN (SELECT DISTINCT(tbl_notification_rpt_id) FROM ".TBL_NOTIFICATION_DATA_RPT_TEACHERS." WHERE user_id='".$tbl_teacher_id_to."')";	
			if($tbl_school_id<>"")
			{
				$qry .= " AND schools like '%".$tbl_school_id."%'";
			}
			 $qry .= " ORDER BY added_date DESC";
		} else {
			// Get teacher messages sent from other teachers or from tbl_admin_id	
			$qry = "SELECT * FROM ".TBL_MESSAGE_TEACHER. " WHERE is_active='Y' AND  (message_to='".$tbl_teacher_id_to."' OR message_from='".$tbl_teacher_id_to."') ";
			$qry .= " AND tbl_teacher_group_id='$tbl_teacher_group_id' ";
			$qry .= " ORDER BY added_date DESC";
			//echo $qry;
		}

		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}

	/**
	* @DESC		Save teacher messages
	* @param	String $tbl_student_id
	* @access	default
	* @return	none
	*/
	function get_all_teacher_messages($tbl_teacher_id_to) {
		$qry = "SELECT * FROM ".TBL_MESSAGE_TEACHER. " WHERE is_active='Y' AND message_to='".$tbl_teacher_id_to."' ORDER BY added_date DESC";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}

	/**
	* @DESC		Get all messages based upon teacher, student and parent ids
	* @param	String $tbl_student_id
	* @access	default
	* @return	none
	*/
	
	function get_message_list($tbl_teacher_id, $tbl_student_id, $tbl_parent_id, $tbl_school_id) {
		if ($tbl_teacher_id == "") {
			$qry = "SELECT * FROM ".TBL_MESSAGE. " WHERE is_active='Y' AND tbl_school_id='".$tbl_school_id."'";
		} else {
			$qry = "SELECT * FROM ".TBL_MESSAGE. " WHERE is_active='Y' AND message_from='".$tbl_teacher_id."' AND tbl_school_id='".$tbl_school_id."'";
		}
		if ($tbl_student_id != "all_students") {
			$qry .= " AND message_to='".$tbl_parent_id."' AND tbl_student_id='".$tbl_student_id."'";
		}
		$qry .= " ORDER BY added_date DESC LIMIT 0, 50 ";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
	
	function get_teacher_message_list($tbl_teacher_id, $tbl_student_id, $tbl_parent_id, $tbl_school_id, $message_mode, $tbl_class_id) {

		$qry = "SELECT * FROM ".TBL_MESSAGE. " WHERE is_active='Y' AND tbl_school_id='".$tbl_school_id."' ";
		if($tbl_teacher_id<>"")
		  $qry .= " AND message_from='".$tbl_teacher_id."' ";
		  
		if ($tbl_student_id != "") {
			$qry .= " AND tbl_student_id='".$tbl_student_id."'";
		}
		if ($message_mode != "") {
			$qry .= " AND message_mode='".$message_mode."'";
		}
		if ($tbl_class_id != "") {
			$qry .= " AND tbl_class_id='".$tbl_class_id."'";
		}
		$qry .= " GROUP BY tbl_message_group_id ORDER BY added_date DESC LIMIT 0, 50 ";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
	

	/**
	* @DESC		Get message image
	* @param	String $tbl_student_id
	* @access	default
	* @return	none
	*/
	function get_message_image($tbl_message_id) {
		$qry = "SELECT * FROM ".TBL_UPLOADS. " WHERE tbl_item_id='".$tbl_message_id."' AND file_name_updated NOT LIKE '%.mp4%'";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}

	/**
	* @DESC		Get message audio
	* @param	String $tbl_student_id
	* @access	default
	* @return	none
	*/

	function get_message_audio($tbl_message_id) {
		$qry = "SELECT * FROM ".TBL_UPLOADS. " WHERE (tbl_item_id like '%".$tbl_message_id."%' or tbl_item_id='".$tbl_message_id."' )  AND file_name_updated LIKE '%.mp4%'";
		//echo $qry; 
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}

	/**
	* @DESC		Get number of unread messages based on roles e.g. Headmaster, Supervisor etc
	* @param	String $tbl_student_id
	* @access	default
	* @return	none
	*/

	function get_roles_unread_msg_count($tbl_school_roles_id, $tbl_teacher_id, $tbl_school_id) {
		$qry = "SELECT COUNT(*) AS cnt FROM ".TBL_MESSAGE_TEACHER. " WHERE tbl_school_roles_id='".$tbl_school_roles_id."' AND is_active='Y' AND tbl_school_id='".$tbl_school_id."' AND is_read='N' AND message_to='".$tbl_teacher_id."'";
		//echo $qry."<br>";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs[0]["cnt"];
	}


	/**
	* @DESC		Get number of unread messages from other teachers
	* @param	String $tbl_student_id
	* @access	default
	* @return	none
	*/

	function get_teacher_unread_msg_count($tbl_teacher_id, $tbl_school_id) {
		$qry = "SELECT COUNT(*) AS cnt FROM ".TBL_MESSAGE_TEACHER. " WHERE message_to='".$tbl_teacher_id."' AND is_active='Y' AND tbl_school_id='".$tbl_school_id."' AND is_read='N' ";
		//echo $qry."<br>";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs[0]["cnt"];
	}

	/**
	* @DESC		Get number of unread messages from other teachers
	* @param	String $tbl_student_id
	* @access	default
	* @return	none
	*/

	function update_unread_msg_count($message_from, $message_to, $tbl_school_id, $tbl_teacher_group_id) {
		if ($message_from == "ministry") {
			$qry = "UPDATE ".TBL_NOTIFICATION_DATA_RPT_TEACHERS. " SET is_read='Y' WHERE  user_id='".$message_to."'";
			//echo $qry."<br>";	
		} else {
			$qry = "UPDATE ".TBL_MESSAGE_TEACHER. " SET is_read='Y' WHERE message_to='".$message_to."' AND tbl_school_id='".$tbl_school_id."' AND tbl_teacher_group_id='$tbl_teacher_group_id' ";
			//echo $qry."<br>";
		}
		//echo $qry."<br>";
		$this->cf->update($qry);
		return;
	}


	/**
	* @DESC		Get number of unread messages sent by ministry to this teacher
	* @param	String $tbl_student_id
	* @access	default
	* @return	none
	*/

	function get_ministry_unread_msg_count($tbl_teacher_id, $tbl_school_id) {
		$qry = "SELECT COUNT(*) AS cnt FROM ".TBL_NOTIFICATION_RPT_TEACHERS. " WHERE tbl_notification_rpt_id IN (SELECT DISTINCT(tbl_notification_rpt_id) FROM ".TBL_NOTIFICATION_DATA_RPT_TEACHERS." WHERE user_id='".$tbl_teacher_id."' AND is_read='N')";
		//echo $qry."<br>";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs[0]["cnt"];
	}

	/**
	* @DESC		Function to update leave status like approved or rejected with comments
	* @param	String $tbl_student_id
	* @access	default
	* @return	none
	*/

	function update_leave_applications($tbl_leave_application_id, $is_approved, $approved_by, $authority_comments) {
		$qry = "UPDATE ".TBL_LEAVE_APPLICATION." SET is_approved='".$is_approved."', approved_by='".$approved_by."', authority_comments='".$authority_comments."' WHERE tbl_leave_application_id='".$tbl_leave_application_id."'";
		$this->cf->update($qry);	
		return;
	}

    function get_parent_messages($tbl_parent_id_from, $tbl_parent_id_to,$tbl_school_id='') {
		// Get teacher messages sent from ministry
		if ($tbl_parent_id_from == "ministry") {
			$qry = "SELECT * FROM ".TBL_NOTIFICATION_RPT. " WHERE tbl_notification_rpt_id IN (SELECT DISTINCT(tbl_notification_rpt_id) FROM ".TBL_NOTIFICATION_DATA_RPT." WHERE user_id='".$tbl_parent_id_to."')  ";
			if($tbl_school_id<>"")
			{
				$qry .= " AND schools like '%".$tbl_school_id."%'";
			}

			$qry .= " ORDER BY added_date DESC";	
		} else {
		// Get teacher messages sent from other teachers or from tbl_admin_id	
		$qry = "SELECT * FROM ".TBL_MESSAGE_PARENT. " WHERE is_active='Y' AND (message_from='".$tbl_parent_id_from."' OR message_to='".$tbl_parent_id_from."') AND (message_to='".$tbl_parent_id_to."' OR message_from='".$tbl_parent_id_to."') ORDER BY added_date DESC";
		//echo $qry;
		}
		//echo $qry; exit;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}

	function getTeacherMessagesFromSchool($tbl_teacher_id_to,$tbl_school_id,$page=''){
		    $qry = "SELECT * FROM ".TBL_MESSAGE_TEACHER. " WHERE is_active='Y' AND message_to='".$tbl_teacher_id_to."' AND tbl_school_id='".$tbl_school_id."' ORDER BY added_date DESC ";
			if($page<>"")
				 $qry .= " limit 0, $page ";
			$rs = $this->cf->selectMultiRecords($qry);
			return $rs;
    }

	function getParentMessagesFromSchool($tbl_parent_id_to,$tbl_school_id,$page='',$tbl_class_id=""){
		    $qry = "SELECT * FROM ".TBL_MESSAGE_PARENT. " WHERE is_active='Y' AND message_to='".$tbl_parent_id_to."' AND tbl_school_id='".$tbl_school_id."' ";
			if($tbl_class_id<>"")
				$qry .= " AND tbl_class_id like '%".$tbl_class_id."%'" ;
			$qry .= " ORDER BY added_date ASC ";
			if($page<>"")
				 $qry .= " limit 0, $page ";
			$rs = $this->cf->selectMultiRecords($qry);
			//echo $qry;
			return $rs;
    }

	function saveMessageToGovt($email_id,$user_type,$subject,$feedback,$tbl_school_id)
	{
		$feedback_id = substr(md5(uniqid(rand())),0,10);
		$qry = "INSERT INTO ".TBL_GOVT_FEEDBACK." (
					`feedback_id` ,
					`email_id` ,
					`user_type` ,
					`subject` ,
					`feedback` ,
					`is_active` ,
					`added_date`,
					`school_id`
					)
					VALUES (
						'$feedback_id', '$email_id', '$user_type', '$subject', '$feedback', 'Y', NOW(),'$tbl_school_id'
					)";
				//echo $qry;
		$this->cf->insertInto($qry);	
	}


	function saveMessageToTimeline($email_id,$user_type,$subject,$feedback,$tbl_school_id)
	{
		$feedback_id = substr(md5(uniqid(rand())),0,10);
		$qry = "INSERT INTO ".TBL_WEBSITE_FEEDBACK." (
					`feedback_id` ,
					`email_id` ,
					`user_type` ,
					`subject` ,
					`feedback` ,
					`is_active` ,
					`added_date`,
					`school_id`
					)
					VALUES (
						'$feedback_id', '$email_id', '$user_type', '$subject', '$feedback', 'Y', NOW(),'$tbl_school_id'
					)";
		$this->cf->insertInto($qry);	
	}

	/*
		This will return all messages against groups. Sometimes tbl_teacher_group_id is blank [message directly send to teacher and not group] so we should alos process those records and assign them to default group of teachers
	*/
	function get_message_groups_against_teacher($tbl_teacher_id, $tbl_school_id) {
		$qry = "SELECT DISTINCT tbl_teacher_group_id FROM ".TBL_MESSAGE_TEACHER." WHERE message_to='".$tbl_teacher_id."' AND tbl_school_id='$tbl_school_id' ";
		//echo $qry."<br />";
		$rs = $this->cf->selectMultiRecords($qry);	
	return $rs;	
	}

	function get_total_unread_messages_against_group($tbl_teacher_group_id, $tbl_teacher_id) {
		$qry = "SELECT COUNT(*) as total_messages FROM ".TBL_MESSAGE_TEACHER." WHERE message_to='".$tbl_teacher_id."' AND is_read='N' AND is_active='Y' ";
		if (trim($tbl_teacher_group_id) != "") {
			$qry .= " AND tbl_teacher_group_id='$tbl_teacher_group_id' ";
		} else {
			$qry .= " AND tbl_teacher_group_id='' ";		}	
		//echo $qry."<br />";
		$rs = $this->cf->selectMultiRecords($qry);	
	return $rs[0]["total_messages"];	
	}


	function get_upload_files($item_id)
	{
		$dataQuery 		= "SELECT * FROM ".TBL_UPLOADS." WHERE tbl_item_id='".$item_id."'";
		$dataRecords 	= $this->cf->selectMultiRecords($dataQuery);
		if(empty($dataRecords))
		{
			$itemId = explode(":",$item_id);
			$dataQuery 		= "SELECT * FROM ".TBL_UPLOADS." WHERE tbl_item_id='".$itemId[0]."'";
		    $dataRecords 	= $this->cf->selectMultiRecords($dataQuery);
		}
		return $dataRecords;
	}


	function save_private_message($message_from, $message_to, $tbl_student_id, $message_type, $message, $tbl_school_id, $tbl_item_id, $message_dir) {
		$message_from = $this->cf->get_data(trim($message_from));
		$message_to = $this->cf->get_data(trim($message_to));
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		$message_type = $this->cf->get_data(trim($message_type));
		$tbl_message_id = substr(md5(uniqid(rand())),0,10);
		$qry = "INSERT INTO ".TBL_PRIVATE_MESSAGE." (`tbl_message_id`, `message_from`, `message_to`,`message_dir`, `tbl_student_id`, `message_type`, `message`, `tbl_item_id`, `added_date`, `tbl_school_id`)
					VALUES ('$tbl_message_id', '$message_from', '$message_to', '$message_dir', '$tbl_student_id', '$message_type', '$message', '$tbl_item_id', NOW(), '$tbl_school_id') ";
		//echo $qry; //exit;
		$this->cf->insertInto($qry);
		
		if (trim($_POST["is_admin"]) == "Y" && ($message_type == "image" || $message_type == "audio")) {
			$tbl_uploads_id = substr(md5(uniqid(rand())),0,10);
			$file_name_original = $_POST["file_name_original"];
			$file_name_updated = $_POST["file_name_updated"];
			$file_name_updated_thumb = $_POST["file_name_updated_thumb"];
			$file_type = $_POST["file_type"];
			$file_size = $_POST["file_size"];
			
			
			$qry = "INSERT INTO ".TBL_UPLOADS." (
					`tbl_uploads_id` ,
					`module_name` ,
					`tbl_item_id` ,
					`file_name_original` ,
					`file_name_updated` ,
					`file_name_updated_thumb` ,
					`file_type` ,
					`file_size` ,
					`is_active` ,
					`added_date`
					)
					VALUES (
						'$tbl_uploads_id', '$module_name', '$tbl_item_id', '$file_name_original', '$file_name_updated','$file_name_updated_thumb', '$file_type', '$file_size', 'Y', NOW()
					)";
			$this->cf->insertInto($qry);
			echo $qry;
		}
	}


	function get_private_message_list($tbl_teacher_id, $tbl_student_id, $tbl_school_id) {
			$qry = "SELECT * FROM ".TBL_PRIVATE_MESSAGE. " WHERE is_active='Y' AND tbl_school_id='".$tbl_school_id."'";
			$qry .= " AND (( message_from='".$tbl_teacher_id."' AND  message_to='".$tbl_student_id."' ) ";
			$qry .= " OR ( message_from='".$tbl_student_id."' AND message_to='".$tbl_teacher_id."' ) ) ";
			$qry .= " ORDER BY id DESC ";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}

	function get_cards_detail_semester($tbl_student_id, $tbl_semester_id) {
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		$qry = "SELECT * FROM ".TBL_CARDS." WHERE tbl_student_id='$tbl_student_id' AND is_active='Y' ";
		if($tbl_semester_id<>""){
			$qry .= "  AND tbl_semester_id='$tbl_semester_id' ";
		}
		$qry .= " ORDER BY added_date DESC LIMIT 0, ".PAGING_CARDS;
		//$qry = "SELECT * FROM ".TBL_CARDS." WHERE tbl_student_id='$tbl_student_id' AND is_active='Y' ORDER BY added_date ";
		//echo "<br>".$qry;
		$rs = $this->cf->selectMultiRecords($qry);
	return $rs;
	}
	
	
	//BACKEND FUNCTIONALITY
	/**
	* @desc		Get all teacher messages_from_school
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function list_teacher_messages_from_school($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_teacher_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry = "SELECT MT.tbl_message_teacher_id, MT.message, MT.added_date, MT.id, MT.is_active, MT.tbl_message_group_id FROM ".TBL_MESSAGE_TEACHER." AS MT LEFT JOIN  ".TBL_TEACHER." AS T ON T.tbl_teacher_id=MT.message_to  WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND MT.tbl_school_id= '".$tbl_school_id."' ";
		}
		
		if($tbl_teacher_id<>"")
		{
			$qry .= " AND MT.message_to= '".$tbl_teacher_id."' ";
		}
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND MT.is_active='$is_active' ";
		}
		$qry .= " AND MT.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " AND ( MT.message LIKE '%$q%' ";
			$qry    .= " OR CONCAT(T.first_name,' ',T.last_name) LIKE '%$q%'
					   	 OR CONCAT(TRIM(T.first_name_ar),' ',TRIM(T.last_name_ar)) LIKE '%$q%' ) ";
		}
		
        
		$qry .= "  GROUP BY MT.message ";
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY MT.$sort_name $sort_by";
		} else {
			$qry .= " ORDER BY MT.id DESC";
		}
		$qry .=" LIMIT $offset, ".TBL_MESSAGE_TEACHER_PAGING;
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	/**
	* @desc		Get total teacher messages_from_school
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_teacher_messages_from_school($q, $is_active, $tbl_school_id,$tbl_teacher_id) {
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT MT.message, MT.added_date, MT.id FROM ".TBL_MESSAGE_TEACHER." AS MT LEFT JOIN  ".TBL_TEACHER." AS T ON T.tbl_teacher_id=MT.message_to  WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND MT.tbl_school_id= '".$tbl_school_id."' ";
		}
		
		if($tbl_teacher_id<>"")
		{
			$qry .= " AND MT.message_to= '".$tbl_teacher_id."' ";
		}
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND MT.is_active='$is_active' ";
		}
		$qry .= " AND MT.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " AND  ( MT.message LIKE '%$q%' ";
			$qry    .= " OR CONCAT(T.first_name,' ',T.last_name) LIKE '%$q%'
					   	 OR CONCAT(TRIM(T.first_name_ar),' ',TRIM(T.last_name_ar)) LIKE '%$q%' )  ";
		}
        
		$qry .= "  GROUP BY MT.message ";
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
	}
	
	function get_teacher_cnt_msg_from_school($msg,$tbl_school_id) {
	    $qry = " SELECT count(*) AS cnt FROM ".TBL_MESSAGE_TEACHER." WHERE message='".$msg."' AND tbl_school_id='$tbl_school_id' " ;
		$results = $this->cf->selectMultiRecords($qry);
		return $results[0]['cnt']; 
	}
	
	function get_sent_notification_cnt_msg_from_school($msg,$tbl_school_id) {
	    $qry = " SELECT count(*) AS cnt FROM ".TBL_MESSAGE_TEACHER." WHERE message='".$msg."' AND tbl_school_id='$tbl_school_id' AND is_notification_sent='Y'  " ;
		$results = $this->cf->selectMultiRecords($qry);
		return $results[0]['cnt']; 
	}
	
	function get_teachers_list_msg_from_school($msg,$tbl_school_id) {
		$qry = " SELECT first_name, first_name_ar, last_name, last_name_ar FROM ".TBL_TEACHER." WHERE is_active='Y' AND tbl_school_id='$tbl_school_id' 
		         AND tbl_teacher_id IN (SELECT message_to FROM ".TBL_MESSAGE_TEACHER." WHERE message='".$msg."') ORDER BY first_name ASC ";
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
	}
	
	//*************************************school message to teachers
	function send_group_message_teachers($tbl_teacher_group_id,$message_from,$message_from_type,$school_message,$tbl_school_id)
	{
		$qry_group_teachers = "SELECT * FROM ".TBL_TEACHER_GROUP_TEACHERS." WHERE tbl_teacher_group_id='$tbl_teacher_group_id' ";	
		$rs_group_teachers  = $this->cf->selectMultiRecords($qry_group_teachers);
		$tbl_message_group_id = substr(md5(uniqid(rand())),0,10);
			for ($t=0; $t<count($rs_group_teachers); $t++) {
				$tbl_teacher_id_t = $rs_group_teachers[$t]['tbl_teacher_id'];
				$tbl_message_teacher_id = substr(md5(uniqid(rand())),0,10);
				$qryIns = "INSERT INTO ".TBL_MESSAGE_TEACHER." (`tbl_message_teacher_id` ,`message_from` ,`message_to` ,`message` ,`message_from_type` ,`tbl_school_roles_id` ,`added_date` ,`is_active` ,`tbl_school_id` ,`tbl_teacher_group_id`,`tbl_message_group_id`)
						VALUES ('$tbl_message_teacher_id', '$message_from', '$tbl_teacher_id_t', '$school_message','$message_from_type','', NOW(), 'Y', '$tbl_school_id', '$tbl_teacher_group_id','$tbl_message_group_id')";
			    $this->cf->insertInto($qryIns);
			}
	}
	
	//group message notification
	function send_grp_msg_notification($tbl_teacher_group_id,$message_from,$message_from_type,$school_message,$tbl_school_id)
	{
		$qry_group_teachers = "SELECT * FROM ".TBL_TEACHER_GROUP_TEACHERS." WHERE tbl_teacher_group_id='$tbl_teacher_group_id' ";	
		$rs_group_teachers  = $this->cf->selectMultiRecords($qry_group_teachers);
			for ($t=0; $t<count($rs_group_teachers); $t++) {
				$tbl_teacher_id_t = $rs_group_teachers[$t]['tbl_teacher_id'];
				 /*************** notification to teachers **************/
				 $qry       = "SELECT token,device FROM ".TBL_USER_NOTIFY_TOKEN." WHERE user_id='$tbl_teacher_id_t' AND is_active='Y' ";
				 $data_tkns = $this->cf->selectMultiRecords($qry);
		     
					for ($b=0; $b<count($data_tkns); $b++) {
						//Send push message
						if (trim(ENABLE_PUSH) == "Y") {
							$message = 'Dear Teacher, You have a message from '.$_SESSION['aqdar_smartcare']['school_name'].'. ';
							$message = urlencode($message);
							$token  = $data_tkns[$b]["token"];
							$device = $data_tkns[$b]["device"];
							$this->cf->send_notification($deviceToken, $message, $device);
							if ($deviceToken == "null" || $deviceToken == "(null)") {return;}
							$message = urldecode($message);
							$tbl_notification_log_id = substr(md5(uniqid(rand())),0,10);
							$qry = "INSERT INTO ".TBL_NOTIFICATION_LOG." (`tbl_notification_log_id`, `token`, `message`, `is_sent`) VALUES ('$tbl_notification_log_id', '$deviceToken', '$message', 'N')";
							$this->cf->insertInto($qry);
						}//if (trim(ENABLE_PUSH) == "Y")
				   }
			    /********************** end notification to teachers ***********/	
			}
	}
	
	
	function send_to_teachers_message($str,$message_from,$message_from_type,$school_message,$tbl_school_id)
	{
		    $tbl_message_group_id = substr(md5(uniqid(rand())),0,10);
			$str               = explode("&", $str);
			for($m=0; $m<count($str); $m++) {
			  if($str[$m]<>""){
				$tbl_teacher_id_t = $str[$m];
				$tbl_message_teacher_id = substr(md5(uniqid(rand())),0,10);
				$qry = "INSERT INTO ".TBL_MESSAGE_TEACHER." (`tbl_message_teacher_id` ,`message_from` ,`message_to` ,`message` ,`message_from_type` ,`tbl_school_roles_id` ,`added_date` ,`is_active` ,`tbl_school_id` ,`tbl_teacher_group_id`,`tbl_message_group_id`)
						VALUES ('$tbl_message_teacher_id', '$message_from', '$tbl_teacher_id_t', '$school_message','$message_from_type','', NOW(), 'Y', '$tbl_school_id', '$tbl_teacher_group_id','$tbl_message_group_id')";
					//echo $qry;
                $this->cf->insertInto($qry);
				
				
			} //end if loop
		} //end for loop
	}
	
	function send_msg_notification($str,$message_from,$message_from_type,$school_message,$tbl_school_id)
	{
		    $str               = explode("&", $str);
			for($m=0; $m<count($str); $m++) {
			  if($str[$m]<>""){
				$tbl_teacher_id_t = $str[$m];
				
				 /*************** notification to teachers **************/
				 $qry       = "SELECT token,device FROM ".TBL_USER_NOTIFY_TOKEN." WHERE user_id='$tbl_teacher_id_t' AND is_active='Y' ";
				 $data_tkns = $this->cf->selectMultiRecords($qry);
		
					for ($b=0; $b<count($data_tkns); $b++) {
						//Send push message
						if (trim(ENABLE_PUSH) == "Y") {
							$message = 'Dear Teacher, You have a message from '.$_SESSION['aqdar_smartcare']['school_name'].'. ';
							$message = urlencode($message);
							$token  = $data_tkns[$b]["token"];
							$device = $data_tkns[$b]["device"];
							$this->cf->send_notification($deviceToken, $message, $device);
							if ($deviceToken == "null" || $deviceToken == "(null)") {return;}
							$message = urldecode($message);
							$tbl_notification_log_id = substr(md5(uniqid(rand())),0,10);
							$qry = "INSERT INTO ".TBL_NOTIFICATION_LOG." (`tbl_notification_log_id`, `token`, `message`, `is_sent`) VALUES ('$tbl_notification_log_id', '$deviceToken', '$message', 'N')";
							$this->cf->insertInto($qry);
						}//if (trim(ENABLE_PUSH) == "Y")
				   }
			    /********************** end notification to teachers ***********/	
			} //end if loop
		} //end for loop
	}
	
	
	
	/**
	* @desc		Activate Teacher Message
	* @access	default
	*/
	function activate_teacher_message($tbl_message_group_id,$tbl_school_id) {
		$tbl_message_group_id = $this->cf->get_data(trim($tbl_message_group_id));
		$qry = "UPDATE ".TBL_MESSAGE_TEACHER." SET is_active='Y' WHERE tbl_message_group_id='$tbl_message_group_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate Teacher Message
	* @param	string $tbl_message_group_id
	* @access	default
	*/
	function deactivate_teacher_message($tbl_message_group_id,$tbl_school_id) {
		$tbl_message_group_id = $this->cf->get_data(trim($tbl_message_group_id));
		$qry = "UPDATE ".TBL_MESSAGE_TEACHER." SET is_active='N' WHERE tbl_message_group_id='$tbl_message_group_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete Teacher Message
	* @param	string $tbl_message_group_id
	* @access	default
	*/
	function delete_teacher_message($tbl_message_group_id,$tbl_school_id) {
		$tbl_message_group_id = $this->cf->get_data(trim($tbl_message_group_id));
       // $qry = "UPDATE ".TBL_MESSAGE_TEACHER." SET is_active='D' WHERE tbl_message_group_id='$tbl_message_group_id' AND  tbl_school_id='$tbl_school_id'";
		//$this->cf->update($qry);
		$qry = "DELETE FROM ".TBL_MESSAGE_TEACHER." WHERE tbl_message_group_id='$tbl_message_group_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->deleteFrom($qry);
	}
	
	
	function get_school_message_to_teacher($tbl_message_group_id,$tbl_school_id)
	{
		$qry_msg = "SELECT * FROM ".TBL_MESSAGE_TEACHER." WHERE tbl_message_group_id='$tbl_message_group_id' AND  tbl_school_id='$tbl_school_id'"; 
		$rs_msg  = $this->cf->selectMultiRecords($qry_msg);
		return  $rs_msg;
	}
	
	function update_message_to_teacher($tbl_message_group_id,$message,$tbl_school_id)
	{
		$qry = "UPDATE ".TBL_MESSAGE_TEACHER." SET message='$message' WHERE tbl_message_group_id='$tbl_message_group_id' AND  tbl_school_id='$tbl_school_id' ";
		$this->cf->update($qry);
	}
	
  /*************************** SCHOOL MESSAGE TO TEACHERS **************************************/	
	
  /*** SCHOOL MESSAGE TO PARENTS  **/
  /**
	* @desc		Get all parents messages_from_school
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function list_parents_messages_from_school($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parent_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry = "SELECT message, added_date, id, is_active, tbl_message_group_id FROM ".TBL_MESSAGE_PARENT." AS MT WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND MT.tbl_school_id= '".$tbl_school_id."' ";
		}
		
		if($tbl_parent_id<>"")
		{
			$qry .= " AND MT.message_to= '".$tbl_parent_id."' ";
		}
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND MT.is_active='$is_active' ";
		}
		$qry .= " AND MT.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " AND  MT.message LIKE '%$q%' ";
		}
		
        
		$qry .= "  GROUP BY MT.message ";
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY MT.id DESC";
		}
		$qry .=" LIMIT $offset, ".TBL_MESSAGE_PARENT_PAGING;
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	/**
	* @desc		Get total parents messages_from_school
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_parents_messages_from_school($q, $is_active, $tbl_school_id,$tbl_parent_id) {
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT message, added_date, id FROM ".TBL_MESSAGE_PARENT." AS MT WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND MT.tbl_school_id= '".$tbl_school_id."' ";
		}
		
		if($tbl_parent_id<>"")
		{
			$qry .= " AND MT.message_to= '".$tbl_parent_id."' ";
		}
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND MT.is_active='$is_active' ";
		}
		$qry .= " AND MT.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " AND  MT.message LIKE '%$q%' ";
		}
        
		$qry .= "  GROUP BY MT.message ";
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
	}
	


   function get_parent_cnt_msg_from_school($tbl_message_group_id,$tbl_school_id) {
	    $qry = " SELECT count(*) AS cnt FROM ".TBL_MESSAGE_PARENT." WHERE tbl_message_group_id='".$tbl_message_group_id."' AND tbl_school_id='$tbl_school_id' " ;
		$results = $this->cf->selectMultiRecords($qry);
		return $results[0]['cnt']; 
	}
	

	function send_to_parents_message($parents_list,$message_from,$message_from_type,$school_message,$tbl_school_id,$tbl_class_id,$tbl_message_group_id)
	{
           if(!empty($parents_list) && count($parents_list)>0)
		   {
				for ($t=0; $t<count($parents_list); $t++) {
					$tbl_parent_id_t       = $parents_list[$t]['tbl_parent_id'];
					$tbl_message_parent_id = substr(md5(uniqid(rand())),0,10);
					$qryIns = "INSERT INTO ".TBL_MESSAGE_PARENT." (`tbl_message_parent_id` ,`message_from` ,`message_to` ,`message` ,`message_from_type` ,`tbl_school_roles_id` ,`added_date` ,`is_active` ,`tbl_school_id` ,`tbl_class_id`,`tbl_message_group_id`)
										VALUES ('$tbl_message_parent_id', '$message_from', '$tbl_parent_id_t', '$school_message','$message_from_type','', NOW(), 'Y', '$tbl_school_id', '$tbl_class_id','$tbl_message_group_id')";
					$this->cf->insertInto($qryIns);
				}
		   }
	}
	
	//parents message notification
	function send_parent_msg_notification($parents_list,$message_from,$message_from_type,$school_message,$tbl_school_id)
	{
		for ($t=0; $t<count($parents_list); $t++) {
				$tbl_parent_id_t = $parents_list[$t]['tbl_parent_id'];
				 /*************** notification to teachers **************/
				 $qry       = "SELECT token,device FROM ".TBL_USER_NOTIFY_TOKEN." WHERE user_id='$tbl_parent_id_t' AND is_active='Y' ";
				 $data_tkns = $this->cf->selectMultiRecords($qry);
		     
					for ($b=0; $b<count($data_tkns); $b++) {
						//Send push message
						if (trim(ENABLE_PUSH) == "Y") {
							$message = 'Dear Parent, You have a message from '.$_SESSION['aqdar_smartcare']['school_name'].'. ';
							$message = urlencode($message);
							$token  = $data_tkns[$b]["token"];
							$device = $data_tkns[$b]["device"];
							$this->cf->send_notification($deviceToken, $message, $device);
							if ($deviceToken == "null" || $deviceToken == "(null)") {return;}
							$message = urldecode($message);
							$tbl_notification_log_id = substr(md5(uniqid(rand())),0,10);
							$qry = "INSERT INTO ".TBL_NOTIFICATION_LOG." (`tbl_notification_log_id`, `token`, `message`, `is_sent`) VALUES ('$tbl_notification_log_id', '$deviceToken', '$school_message', 'N')";
							$this->cf->insertInto($qry);
						}//if (trim(ENABLE_PUSH) == "Y")
				   }
			    /********************** end notification to teachers ***********/	
			}
	}
	
	
	/**
	* @desc		Activate Parent Message
	* @access	default
	*/
	function activate_parent_message($tbl_message_group_id,$tbl_school_id) {
		$tbl_message_group_id = $this->cf->get_data(trim($tbl_message_group_id));
		$qry = "UPDATE ".TBL_MESSAGE_PARENT." SET is_active='Y' WHERE tbl_message_group_id='$tbl_message_group_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate Teacher Message
	* @param	string $tbl_message_group_id
	* @access	default
	*/
	function deactivate_parent_message($tbl_message_group_id,$tbl_school_id) {
		$tbl_message_group_id = $this->cf->get_data(trim($tbl_message_group_id));
		$qry = "UPDATE ".TBL_MESSAGE_PARENT." SET is_active='N' WHERE tbl_message_group_id='$tbl_message_group_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete Parent Message
	* @param	string $tbl_message_group_id
	* @access	default
	*/
	function delete_parent_message($tbl_message_group_id,$tbl_school_id) {
		$tbl_message_group_id = $this->cf->get_data(trim($tbl_message_group_id));
       // $qry = "UPDATE ".TBL_MESSAGE_TEACHER." SET is_active='D' WHERE tbl_message_group_id='$tbl_message_group_id' AND  tbl_school_id='$tbl_school_id'";
		//$this->cf->update($qry);
		$qry = "DELETE FROM ".TBL_MESSAGE_PARENT." WHERE tbl_message_group_id='$tbl_message_group_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->deleteFrom($qry);
	}
	
	
	function get_school_message_to_parent($tbl_message_group_id,$tbl_school_id)
	{
		$qry_msg = "SELECT * FROM ".TBL_MESSAGE_PARENT." WHERE tbl_message_group_id='$tbl_message_group_id' AND  tbl_school_id='$tbl_school_id'"; 
		$rs_msg  = $this->cf->selectMultiRecords($qry_msg);
		return  $rs_msg;
	}
	
	function update_message_to_parent($tbl_message_group_id,$message,$tbl_school_id)
	{
		$qry = "UPDATE ".TBL_MESSAGE_PARENT." SET message='$message' WHERE tbl_message_group_id='$tbl_message_group_id' AND  tbl_school_id='$tbl_school_id' ";
		$this->cf->update($qry);
	}
	
	function get_class_list_parent_msg_from_school($tbl_message_group_id,$tbl_school_id) {
		$qry = "SELECT  C.class_name, C.class_name_ar, SEC.section_name, SEC.section_name_ar  FROM ".TBL_CLASS." AS C  
				LEFT JOIN  ".TBL_SECTION." AS SEC  ON  SEC.tbl_section_id= C.tbl_section_id  
				WHERE 1 AND  C.is_active='Y' AND C.tbl_school_id='$tbl_school_id' 
		         AND C.tbl_class_id IN (SELECT distinct(tbl_class_id) FROM ".TBL_MESSAGE_PARENT." AS MP WHERE tbl_message_group_id='".$tbl_message_group_id."') ORDER BY C.id ASC ";
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
	}
	
	/*******************************************************/
	
	/*************************** TEACHER MESSAGE GROUPS **************************************/	

  /**
	* @desc		Get all teachers message groups
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function list_teachers_group($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parent_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry = "SELECT * FROM ".TBL_TEACHER_GROUP." AS TG WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND TG.tbl_school_id= '".$tbl_school_id."' ";
		}
	
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND TG.is_active='$is_active' ";
		}
		$qry .= " AND TG.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " AND ( TG.group_name_en LIKE '%$q%' OR TG.group_name_ar LIKE '%$q%' ) ";
		}
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY TG.id DESC";
		}
		$qry .=" LIMIT $offset, ".TBL_TEACHER_GROUP_PAGING;
		
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	/**
	* @desc		Get total teachers group
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_teachers_group($q, $is_active, $tbl_school_id,$tbl_parent_id) {
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT * FROM ".TBL_TEACHER_GROUP." AS TG WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND TG.tbl_school_id= '".$tbl_school_id."' ";
		}
	
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND TG.is_active='$is_active' ";
		}
		$qry .= " AND TG.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " AND (  TG.group_name_en LIKE '%$q%' OR TG.group_name_ar LIKE '%$q%' )";
		}
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
	}
	
	
	function get_teachers_list_in_group($tbl_teacher_group_id,$tbl_school_id) {
	    $qry = " SELECT tbl_teacher_id, first_name, first_name_ar, last_name, last_name_ar,file_name_updated FROM ".TBL_TEACHER." WHERE is_active='Y' AND tbl_school_id='$tbl_school_id' 
		         AND tbl_teacher_id IN (SELECT tbl_teacher_id FROM ".TBL_TEACHER_GROUP_TEACHERS." WHERE tbl_teacher_group_id='".$tbl_teacher_group_id."') ORDER BY first_name ASC ";
		
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
	
	}
	
		/**
	* @desc		Activate Teacher Group
	* @access	default
	*/
	function activate_teacher_group($tbl_teacher_group_id,$tbl_school_id) {
		$tbl_teacher_group_id = $this->cf->get_data(trim($tbl_teacher_group_id));
		$qry = "UPDATE ".TBL_TEACHER_GROUP." SET is_active='Y' WHERE tbl_teacher_group_id='$tbl_teacher_group_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate Teacher Message
	* @param	string $tbl_message_group_id
	* @access	default
	*/
	function deactivate_teacher_group($tbl_teacher_group_id,$tbl_school_id) {
		$tbl_teacher_group_id = $this->cf->get_data(trim($tbl_teacher_group_id));
		$qry = "UPDATE ".TBL_TEACHER_GROUP." SET is_active='N' WHERE tbl_teacher_group_id='$tbl_teacher_group_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete Teacher Group
	* @param	string $tbl_message_group_id
	* @access	default
	*/
	function delete_teacher_group($tbl_teacher_group_id,$tbl_school_id) {
		$tbl_teacher_group_id = $this->cf->get_data(trim($tbl_teacher_group_id));
        $qry = "UPDATE ".TBL_TEACHER_GROUP." SET is_active='D' WHERE tbl_teacher_group_id='$tbl_teacher_group_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_TEACHER_GROUP." WHERE tbl_teacher_group_id='$tbl_teacher_group_id' AND  tbl_school_id='$tbl_school_id'";
		//$this->cf->deleteFrom($qry);
	}


    // add teacher group
	function save_teacher_group($tbl_teacher_group_id,$group_name_en,$group_name_ar,$str,$tbl_school_id)
	{
           
		   $qryIns = "INSERT INTO ".TBL_TEACHER_GROUP." (tbl_teacher_group_id ,`group_name_en` ,`group_name_ar` ,`added_date` ,`is_active` ,`tbl_school_id`)
										VALUES ('$tbl_teacher_group_id', '$group_name_en', '$group_name_ar', NOW(), 'Y', '$tbl_school_id')";
		   $this->cf->insertInto($qryIns);
		   $str               = explode("&", $str);
			for($m=0; $m<count($str); $m++) {
			  if($str[$m]<>""){
				    $tbl_teacher_id_t = $str[$m];
		   
					$tbl_teacher_group_teachers_id = substr(md5(uniqid(rand())),0,10);
					$qryIns = "INSERT INTO ".TBL_TEACHER_GROUP_TEACHERS." (`tbl_teacher_group_teachers_id`,`tbl_teacher_group_id`,`tbl_teacher_id`, `is_active`,`added_date`,`tbl_school_id`) 
											 VALUES ('$tbl_teacher_group_teachers_id','$tbl_teacher_group_id','$tbl_teacher_id_t','$is_active', NOW(), '$tbl_school_id' ) ";
					$this->cf->insertInto($qryIns);
				}
		   }
	}

   function is_exist_teacher_group($tbl_teacher_group_id,$group_name_en,$tbl_school_id)
   {
	    $qry_exist = "SELECT * FROM ".TBL_TEACHER_GROUP." WHERE group_name_en='".$group_name_en."' AND tbl_school_id='".$tbl_school_id."' ";
		if($tbl_teacher_group_id<>"")
		{
			$qry_exist .= " AND tbl_teacher_group_id <> '".$tbl_teacher_group_id."' ";
		}
        $rs_exist  = $this->cf->selectMultiRecords($qry_exist);
		return  $rs_exist;
   }
	
	function get_teacher_group_info($tbl_teacher_group_id,$tbl_school_id)
	{
		$qry_msg = "SELECT * FROM ".TBL_TEACHER_GROUP." WHERE tbl_teacher_group_id='$tbl_teacher_group_id' AND  tbl_school_id='$tbl_school_id'"; 
		$rs_msg  = $this->cf->selectMultiRecords($qry_msg);
		return  $rs_msg;
	}

	
    function update_teacher_group($tbl_teacher_group_id,$group_name_en,$group_name_ar,$str,$tbl_school_id)
	{
           
		   $qry = "UPDATE ".TBL_TEACHER_GROUP." SET group_name_en='$group_name_en', group_name_ar='$group_name_ar' WHERE tbl_teacher_group_id='$tbl_teacher_group_id' AND  tbl_school_id='$tbl_school_id' ";
		   echo $qry ;
		   $this->cf->update($qry);
		   
		   $qryDel = "DELETE FROM ".TBL_TEACHER_GROUP_TEACHERS." WHERE tbl_teacher_group_id='$tbl_teacher_group_id' AND  tbl_school_id='$tbl_school_id'";
		   $this->cf->deleteFrom($qryDel);
		   
		    $str               = explode("&", $str);
			for($m=0; $m<count($str); $m++) {
			  if($str[$m]<>""){
				    $tbl_teacher_id_t = $str[$m];
					$tbl_teacher_group_teachers_id = substr(md5(uniqid(rand())),0,10);
					$qryIns = "INSERT INTO ".TBL_TEACHER_GROUP_TEACHERS." (`tbl_teacher_group_teachers_id`,`tbl_teacher_group_id`,`tbl_teacher_id`, `is_active`,`added_date`,`tbl_school_id`) 
											 VALUES ('$tbl_teacher_group_teachers_id','$tbl_teacher_group_id','$tbl_teacher_id_t','$is_active', NOW(), '$tbl_school_id' ) ";
					$this->cf->insertInto($qryIns);
				}
		   }
	}
	
   /*******************************************************/
    
   /*************************** PARENT FORUM GROUPS **************************************/	

  /**
	* @desc		Get all parent forum groups
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function list_parents_group($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parent_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry = "SELECT * FROM ".TBL_PARENT_GROUP." AS PG WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND PG.tbl_school_id= '".$tbl_school_id."' ";
		}
	
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PG.is_active='$is_active' ";
		}
		$qry .= " AND PG.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " AND ( PG.group_name_en LIKE '%$q%' OR PG.group_name_ar LIKE '%$q%' ) ";
		}
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY PG.id DESC";
		}
		$qry .=" LIMIT $offset, ".TBL_PARENT_GROUP_PAGING;
		
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	/**
	* @desc		Get total parents group
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_parents_group($q, $is_active, $tbl_school_id,$tbl_parent_id) {
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT * FROM ".TBL_PARENT_GROUP." AS PG WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND PG.tbl_school_id= '".$tbl_school_id."' ";
		}
	
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PG.is_active='$is_active' ";
		}
		$qry .= " AND PG.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " AND (  PG.group_name_en LIKE '%$q%' OR PG.group_name_ar LIKE '%$q%' )";
		}
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
	}
	
	function get_selected_class_in_group($tbl_parent_group_id,$tbl_school_id) {
	    $qry = " SELECT tbl_class_id FROM  ".TBL_PARENT_GROUP_PARENTS." WHERE tbl_parent_group_id='".$tbl_parent_group_id."' and tbl_school_id= '".$tbl_school_id."' 
		         GROUP BY tbl_class_id ORDER BY id ASC ";
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
	}
	
	function getParentsBasedOnClass($selected_class_list,$tbl_school_id)
	{
	   $qry = " SELECT P.tbl_parent_id, P.first_name, P.first_name_ar, P.last_name, P.last_name_ar,C.tbl_class_id,C.class_name,C.class_name_ar,SEC.section_name,SEC.section_name_ar 
	            FROM ".TBL_PARENT." AS P 
	            LEFT JOIN ".TBL_PARENT_STUDENT." AS PS ON PS.tbl_parent_id=P.tbl_parent_id 
	            LEFT JOIN ".TBL_STUDENT." AS S ON S.tbl_student_id=PS.tbl_student_id 
	            LEFT JOIN ".TBL_CLASS." AS C ON C.tbl_class_id=S.tbl_class_id 
				LEFT JOIN  ".TBL_SECTION." AS SEC  ON  C.tbl_section_id = SEC.tbl_section_id  
				WHERE P.is_active='Y' AND P.tbl_school_id='$tbl_school_id'  ";
	    if($selected_class_list<>"")
		{
			$qry .= " AND C.tbl_class_id IN ( ".$selected_class_list.")  ";
		}
		$qry .= " ORDER BY C.class_name ASC, SEC.section_name ASC, S.first_name ASC ";
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;	
	}
	
	
	
	function get_parents_list_in_group($tbl_parent_group_id,$tbl_school_id) {
	    $qry = " SELECT tbl_parent_id, first_name, first_name_ar, last_name, last_name_ar FROM ".TBL_PARENT." WHERE is_active='Y' AND tbl_school_id='$tbl_school_id' 
		         AND tbl_parent_id IN (SELECT tbl_parent_id FROM ".TBL_PARENT_GROUP_PARENTS." WHERE tbl_parent_group_id='".$tbl_parent_group_id."') ORDER BY first_name ASC ";
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
	
	}
	
		/**
	* @desc		Activate Parent Group
	* @access	default
	*/
	function activate_parent_group($tbl_parent_group_id,$tbl_school_id) {
		$tbl_parent_group_id = $this->cf->get_data(trim($tbl_parent_group_id));
		$qry = "UPDATE ".TBL_PARENT_GROUP." SET is_active='Y' WHERE tbl_parent_group_id='$tbl_parent_group_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate Parent Group
	* @param	string $tbl_message_group_id
	* @access	default
	*/
	function deactivate_parent_group($tbl_parent_group_id,$tbl_school_id) {
		$tbl_parent_group_id = $this->cf->get_data(trim($tbl_parent_group_id));
		$qry = "UPDATE ".TBL_PARENT_GROUP." SET is_active='N' WHERE tbl_parent_group_id='$tbl_parent_group_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete Parent Group
	* @param	string $tbl_message_group_id
	* @access	default
	*/
	function delete_parent_group($tbl_parent_group_id,$tbl_school_id) {
		$tbl_parent_group_id = $this->cf->get_data(trim($tbl_parent_group_id));
        $qry = "UPDATE ".TBL_PARENT_GROUP." SET is_active='D' WHERE tbl_parent_group_id='$tbl_parent_group_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_TEACHER_GROUP." WHERE tbl_teacher_group_id='$tbl_teacher_group_id' AND  tbl_school_id='$tbl_school_id'";
		//$this->cf->deleteFrom($qry);
	}


    // add parent group
	function save_parent_group($tbl_parent_group_id,$group_name_en,$group_name_ar,$str,$tbl_school_id)
	{
           
		   $qryIns = "INSERT INTO ".TBL_PARENT_GROUP." (`tbl_parent_group_id` ,`group_name_en` ,`group_name_ar` ,`added_date` ,`is_active` ,`tbl_school_id`)
										VALUES ('$tbl_parent_group_id', '$group_name_en', '$group_name_ar', NOW(), 'Y', '$tbl_school_id')";
		   $this->cf->insertInto($qryIns);
		   $str               = explode("&", $str);
			for($m=0; $m<count($str); $m++) {
			  if($str[$m]<>""){
				    $tbl_parent_id_t = $str[$m];
					$parentIds =  array();
					$parentIds = explode("*",$tbl_parent_id_t);
		            $tbl_parent_id = $parentIds[0];
					$tbl_class_id  = $parentIds[1];
					$tbl_parent_group_parents_id = substr(md5(uniqid(rand())),0,10);
					$qryIns = "INSERT INTO ".TBL_PARENT_GROUP_PARENTS." (`tbl_parent_group_parents_id`,`tbl_parent_group_id`,`tbl_parent_id`,`tbl_class_id`,`is_active`,`added_date`,`tbl_school_id`) 
											 VALUES ('$tbl_parent_group_parents_id','$tbl_parent_group_id','$tbl_parent_id','$tbl_class_id','Y', NOW(), '$tbl_school_id' ) ";
					$this->cf->insertInto($qryIns);
				}
		   }
	}

   function is_exist_parent_group($tbl_parent_group_id,$group_name_en,$tbl_school_id)
   {
	    $qry_exist = "SELECT * FROM ".TBL_PARENT_GROUP." WHERE group_name_en='".$group_name_en."' AND tbl_school_id='".$tbl_school_id."' ";
		if($tbl_parent_group_id<>"")
		{
			$qry_exist .= " AND tbl_parent_group_id <> '".$tbl_parent_group_id."' ";
		}
        $rs_exist  = $this->cf->selectMultiRecords($qry_exist);
		return  $rs_exist;
   }
	
	function get_parent_group_info($tbl_parent_group_id,$tbl_school_id)
	{
		$qry_msg = "SELECT * FROM ".TBL_PARENT_GROUP." WHERE tbl_parent_group_id='$tbl_parent_group_id' AND  tbl_school_id='$tbl_school_id'"; 
		$rs_msg  = $this->cf->selectMultiRecords($qry_msg);
		return  $rs_msg;
	}

	
    function update_parent_group($tbl_parent_group_id,$group_name_en,$group_name_ar,$str,$tbl_school_id)
	{
           
		 $qry = "UPDATE ".TBL_PARENT_GROUP." SET group_name_en='$group_name_en', group_name_ar='$group_name_ar' WHERE tbl_parent_group_id='$tbl_parent_group_id' AND  tbl_school_id='$tbl_school_id' ";
		 $this->cf->update($qry);
		   
		   $qryDel = "DELETE FROM ".TBL_PARENT_GROUP_PARENTS." WHERE tbl_parent_group_id='$tbl_parent_group_id' AND  tbl_school_id='$tbl_school_id'";
		   $this->cf->deleteFrom($qryDel);
		   
		    $str               = explode("&", $str);
			for($m=0; $m<count($str); $m++) {
			  if($str[$m]<>""){
				    $tbl_parent_id_t = $str[$m];
					$tbl_parent_group_parents_id = substr(md5(uniqid(rand())),0,10);
					$parentIds =  array();
					$parentIds = explode("*",$tbl_parent_id_t);
		            $tbl_parent_id = $parentIds[0];
					$tbl_class_id  = $parentIds[1];
					$qryIns = "INSERT INTO ".TBL_PARENT_GROUP_PARENTS." (`tbl_parent_group_parents_id`,`tbl_parent_group_id`,`tbl_parent_id`,`tbl_class_id`, `is_active`,`added_date`,`tbl_school_id`) 
											 VALUES ('$tbl_parent_group_parents_id','$tbl_parent_group_id','$tbl_parent_id','$tbl_class_id','$is_active', NOW(), '$tbl_school_id' ) ";
					$this->cf->insertInto($qryIns);
				}
		   }
	}
	
	/*******************************************************/
	//
	
	
	  /*** PARENTS MESSAGE TO SCHOOLS   **/
  /**
	* @desc		Get all parents messages_from_school
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function list_parents_messages_to_school($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parent_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry = "SELECT C.tbl_contact_us_id, C.contact_us_comments, P.first_name,P.last_name,P.first_name_ar,P.last_name_ar,P.email,C.added_date FROM ".TBL_CONTACT_US." AS C LEFT JOIN ".TBL_PARENT." AS P ON P.tbl_parent_id=C.tbl_parent_id WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND C.tbl_school_id= '".$tbl_school_id."' ";
		}
		
		if($tbl_parent_id<>"")
		{
			$qry .= " AND C.tbl_parent_id= '".$tbl_parent_id."' ";
		}
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND C.is_active='$is_active' ";
		}
		$qry .= " AND C.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " AND  ( C.contact_us_comments LIKE '%$q%'
				        OR CONCAT(P.first_name,' ',P.last_name) LIKE '%$q%'
					   	OR CONCAT(TRIM(P.first_name_ar),' ',TRIM(P.last_name_ar)) LIKE '%$q%' ) ";
		}
        
		$qry .= "  GROUP BY C.added_date ";
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY C.$sort_name $sort_by";
		} else {
			$qry .= " ORDER BY C.id DESC";
		}
		$qry .=" LIMIT $offset, ".TBL_SCHOOL_CONTACTS_PAGING;
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	/**
	* @desc		Get total parents messages_to_school
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_parents_messages_to_school($q, $is_active, $tbl_school_id,$tbl_parent_id) {
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT C.tbl_contact_us_id FROM ".TBL_CONTACT_US." AS C LEFT JOIN ".TBL_PARENT." AS P ON P.tbl_parent_id=C.tbl_parent_id WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND P.tbl_school_id= '".$tbl_school_id."' ";
		}
		
		if($tbl_parent_id<>"")
		{
			$qry .= " AND C.tbl_parent_id= '".$tbl_parent_id."' ";
		}
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND C.is_active='$is_active' ";
		}
		$qry .= " AND C.is_active<>'D' "; 
		
		if (trim($q) != "") {
			$qry    .= " AND  ( C.contact_us_comments LIKE '%$q%'
				        OR CONCAT(P.first_name,' ',P.last_name) LIKE '%$q%'
					   	OR CONCAT(TRIM(P.first_name_ar),' ',TRIM(P.last_name_ar)) LIKE '%$q%' ) ";
		}
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
	}
	

	
	// List Website Feedback
	function list_website_feedback($sort_name, $sort_by, $offset, $q, $is_active) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry = "SELECT * from ".TBL_WEBSITE_FEEDBACK." AS F WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND F.tbl_school_id= '".$tbl_school_id."' ";
		}
		
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND F.is_active='$is_active' ";
		}
		$qry .= " AND F.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " AND F.feedback LIKE '%$q%' ";
		}
        
		
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY F.$sort_name $sort_by";
		} else {
			$qry .= " ORDER BY F.id DESC";
		}
		$qry .=" LIMIT $offset, ".TBL_SCHOOL_CONTACTS_PAGING;
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	/**
	* @desc		Get total Website feedback
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_website_feedback($q, $is_active) {
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT F.feedback_id FROM ".TBL_WEBSITE_FEEDBACK." AS F  WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND F.tbl_school_id= '".$tbl_school_id."' ";
		}
		
	
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND F.is_active='$is_active' ";
		}
		$qry .= " AND F.is_active<>'D' "; 
		
		if (trim($q) != "") {
			$qry    .= " AND F.feedback LIKE '%$q%' ";
		}
        
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
	} 
	
    function get_feedbackObj($tbl_feedback_id) {
		$tbl_feedback_id = $this->cf->get_data(trim($tbl_feedback_id));
		$qry = "SELECT * FROM ".TBL_WEBSITE_FEEDBACK." WHERE feedback_id='$tbl_feedback_id'";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
	
	 function update_feedbackStatus($tbl_feedback_id) {
		$tbl_feedback_id = $this->cf->get_data(trim($tbl_feedback_id));
		$qry = "UPDATE ". TBL_WEBSITE_FEEDBACK ." SET view_status='Y' WHERE feedback_id= '".$tbl_feedback_id."' ";			
		$this->cf->update($qry);
	 }
	 
	 
	 // PARENT BACKEND SECTION 
	 function get_my_leave_applications($tbl_teacher_id, $today_date, $tbl_student_id, $tbl_school_id, $offset, $is_active) { 
		
		$offset         = $this->cf->get_data($offset);
		$today_date     = $this->cf->get_data($today_date);
		$tbl_student_id = $this->cf->get_data($tbl_student_id);
		$tbl_school_id  = $this->cf->get_data($tbl_school_id);
		$offset         = $this->cf->get_data($offset);
		$is_active      = $this->cf->get_data($is_active);
		
		$qry = "SELECT * FROM ".TBL_LEAVE_APPLICATION." 
		       WHERE 1 "; 
	    if($tbl_teacher_id<>"")
		{
			$qry .= " AND tbl_teacher_id='".$tbl_teacher_id."' ";
		}
		 if($today_date<>"")
		{
			$qry .= " AND  (start_date>= '".$today_date."' OR end_date>='".$today_date."') ";
		}
		 if($tbl_student_id<>"")
		{
			$qry .= " AND tbl_student_id='".$tbl_student_id."' ";
		}
		
		 if($tbl_school_id<>"")
		{
			$qry .= " AND tbl_school_id='".$tbl_school_id."' ";
		}
		
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND is_active='$is_active' ";
		}
		$qry .= " AND is_active<>'D' "; 
			
		$qry .= " ORDER BY added_date DESC ";
		

		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_SCHOOL_TYPE_PAGING;
		}
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
	
	function get_total_my_leave_applications($tbl_teacher_id, $today_date, $tbl_student_id, $tbl_school_id, $offset, $is_active ) { 
		
		$offset         = $this->cf->get_data($offset);
		$today_date     = $this->cf->get_data($today_date);
		$tbl_student_id = $this->cf->get_data($tbl_student_id);
		$tbl_school_id  = $this->cf->get_data($tbl_school_id);
		$offset         = $this->cf->get_data($offset);
		$is_active      = $this->cf->get_data($is_active);
		
		$qry = "SELECT tbl_leave_application_id FROM ".TBL_LEAVE_APPLICATION." 
		       WHERE 1 "; 
	    if($tbl_teacher_id<>"")
		{
			$qry .= " tbl_teacher_id='".$tbl_teacher_id."' ";
		}
		 if($today_date<>"")
		{
			$qry .= " AND  (start_date>= '".$today_date."' OR end_date>='".$today_date."') ";
		}
		 if($tbl_student_id<>"")
		{
			$qry .= " AND tbl_student_id='".$tbl_student_id."' ";
		}
		
		 if($tbl_school_id<>"")
		{
			$qry .= " AND tbl_school_id='".$tbl_school_id."' ";
		}
		
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND is_active='$is_active' ";
		}
		$qry .= " AND is_active<>'D' "; 
			
		$qry .= " ORDER BY added_date DESC ";
		$rs = $this->cf->selectMultiRecords($qry);
		return count($rs);
	}
	
	
	function is_exist_leave_aplication($tbl_leave_application_id, $start_date, $tbl_student_id, $tbl_school_id) {
		$tbl_leave_application_id  = $this->cf->get_data($tbl_leave_application_id);
		$start_date 		       = date("Y-m-d", strtotime($this->cf->get_data($start_date)));
		$tbl_student_id            = $this->cf->get_data($tbl_student_id);
		$tbl_school_id             = $this->cf->get_data($tbl_school_id);

		$qry = "SELECT * FROM ".TBL_LEAVE_APPLICATION." WHERE 1 ";
		if (trim($start_date) != "") {
			 $qry .= " AND start_date='$start_date' ";
		}
		if (trim($tbl_leave_application_id) != "") {
			 $qry .= " AND tbl_leave_application_id<>'$tbl_leave_application_id' ";
		}
		if (trim($tbl_student_id) != "") {
			 $qry .= " AND tbl_student_id='$tbl_student_id' ";
		}
		if (trim($tbl_school_id) != "") {
			 $qry .= " AND tbl_school_id='$tbl_school_id' ";
		}
	    $qry .= " AND is_active<>'D' "; 
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}
	
	
    function add_leave_application($tbl_leave_application_id, $comments_parent, $tbl_student_id, $tbl_school_id, $start_date, $end_date)
    {
		$tbl_leave_application_id      = $this->cf->get_data($tbl_leave_application_id);
		$comments_parent        	   = $this->cf->get_data($comments_parent);
		$tbl_student_id                = $this->cf->get_data($tbl_student_id);
		$start_date        			   = $this->cf->get_data($start_date);
		$end_date                      = $this->cf->get_data($end_date);
		$tbl_school_id                 = $this->cf->get_data($tbl_school_id);
		
		if($tbl_school_id<>"")
		{
			$qry_ins = "INSERT INTO ".TBL_EVENTS." (`tbl_events_id`, `title`, `title_ar`, `description`, `description_ar`, `start_date`, 
			`end_date`, `start_time`, `added_date`, `is_active`, `tbl_school_id`)
						VALUES ('$tbl_event_id', '$title', '$title_ar', '$description', '$description_ar', '$start_date',
			'$end_date', '$start_time', NOW(), 'Y', '$tbl_school_id')";
			
			//echo $qry_ins."<br />";
			
			$this->cf->insertInto($qry_ins);
			return "Y";
		}else{
		   return "N";	
			
		}
	}
	
	// LIST TEACHER PRIVATE MESSAGES ( TWO WAY COMMUNICATION PARENT TO TEACHER )
	
	function get_private_message_to_teacher($tbl_teacher_id, $tbl_student_id, $tbl_school_id, $offset) {
			$qry = "SELECT * FROM ".TBL_PRIVATE_MESSAGE. " WHERE is_active='Y' AND tbl_school_id='".$tbl_school_id."'";
			$qry .= " AND (( message_from='".$tbl_teacher_id."' AND  message_to='".$tbl_student_id."' ) ";
			$qry .= " OR ( message_from='".$tbl_student_id."' AND message_to='".$tbl_teacher_id."' ) ) ";
			$qry .= " ORDER BY id DESC ";
			$qry .= " LIMIT $offset, ".TBL_PARENTING_SCHOOL_PAGING;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
	
	function get_private_total_message_to_teacher($tbl_teacher_id, $tbl_student_id, $tbl_school_id)
	{
		$qry = "SELECT tbl_message_id FROM ".TBL_PRIVATE_MESSAGE. " WHERE is_active='Y' AND tbl_school_id='".$tbl_school_id."'";
			$qry .= " AND (( message_from='".$tbl_teacher_id."' AND  message_to='".$tbl_student_id."' ) ";
			$qry .= " OR ( message_from='".$tbl_student_id."' AND message_to='".$tbl_teacher_id."' ) ) ";
			$qry .= " ORDER BY id DESC ";
		$rs = $this->cf->selectMultiRecords($qry);
		return count($rs);
	}
	
	// END TEACHER PRIVATE MESSAGES
	
    // Parents Contact To
	
	//To School
	function sendParentMessageToSchool($tbl_parent_id, $contact_us_name, $contact_us_email, $contact_us_comments, $tbl_school_id) {

		$contact_us_name = $this->cf->get_data(trim($contact_us_name));
		$contact_us_email = $this->cf->get_data(trim($contact_us_email));
		$contact_us_comments = $this->cf->get_data(trim($contact_us_comments));
		$tbl_contact_us_id = substr(md5(uniqid(rand())),0,10);

		$qry = "INSERT INTO ".TBL_CONTACT_US." (`tbl_contact_us_id` ,`lan` ,`device` ,`tbl_parent_id` ,`contact_us_name` ,`contact_us_email` ,`contact_us_comments` ,`is_active` ,
			`added_date` ,`tbl_school_id`) VALUES ('$tbl_contact_us_id', '$lan', '$device', '$tbl_parent_id', '$contact_us_name', '$contact_us_email', '$contact_us_comments', 'Y', NOW(), '$tbl_school_id')";
		//echo $qry;
		$this->cf->insertInto($qry);
	}
	 // END PARENT BACKEND SECTION  
	 
	 
	 // SCHOOL BACKEND STUDENTS GROUP MANAGEMENT
	   /*************************** PARENT FORUM GROUPS **************************************/	

  /**
	* @desc		Get all student groups
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function list_students_group($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id, $tbl_student_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry = "SELECT * FROM ".TBL_STUDENT_GROUP." AS SG WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND SG.tbl_school_id= '".$tbl_school_id."' ";
		}
	
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND SG.is_active='$is_active' ";
		}
		$qry .= " AND SG.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " AND ( SG.group_name_en LIKE '%$q%' OR SG.group_name_ar LIKE '%$q%' ) ";
		}
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY SG.id DESC";
		}
		$qry .=" LIMIT $offset, ".TBL_STUDENT_GROUP_PAGING;
		
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	/**
	* @desc		Get total parents group
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_students_group($q, $is_active, $tbl_school_id,$tbl_parent_id) {
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT * FROM ".TBL_STUDENT_GROUP." AS SG WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND SG.tbl_school_id= '".$tbl_school_id."' ";
		}
	
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND SG.is_active='$is_active' ";
		}
		$qry .= " AND SG.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " AND (  SG.group_name_en LIKE '%$q%' OR SG.group_name_ar LIKE '%$q%' )";
		}
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
	}
	
	function get_selected_student_class_in_group($tbl_student_group_id,$tbl_school_id) {
	    $qry = " SELECT tbl_class_id FROM  ".TBL_STUDENT_GROUP_STUDENTS." WHERE tbl_student_group_id='".$tbl_student_group_id."' and tbl_school_id= '".$tbl_school_id."' 
		         GROUP BY tbl_class_id ORDER BY id ASC ";
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
	}
	
	function getStudentsBasedOnClass($selected_class_list,$tbl_school_id)
	{
	   $qry = " SELECT S.tbl_student_id, S.first_name, S.first_name_ar, S.last_name, S.last_name_ar,C.tbl_class_id,C.class_name,C.class_name_ar,SEC.section_name,SEC.section_name_ar 
	            FROM ".TBL_STUDENT." AS S
	            LEFT JOIN ".TBL_CLASS." AS C ON C.tbl_class_id=S.tbl_class_id 
				LEFT JOIN  ".TBL_SECTION." AS SEC  ON  C.tbl_section_id = SEC.tbl_section_id  
				WHERE S.is_active='Y' AND S.tbl_school_id='$tbl_school_id'  ";
	    if($selected_class_list<>"")
		{
			$qry .= " AND C.tbl_class_id IN ( ".$selected_class_list.")  ";
		}
		$qry .= " ORDER BY C.class_name ASC, SEC.section_name ASC, S.first_name ASC ";
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;	
	}
	
	
	
	function get_students_list_in_group($tbl_student_group_id,$tbl_school_id) {
	    $qry = " SELECT tbl_student_id, first_name, first_name_ar, last_name, last_name_ar FROM ".TBL_STUDENT." WHERE is_active='Y' AND tbl_school_id='$tbl_school_id' 
		         AND tbl_student_id IN (SELECT tbl_student_id FROM ".TBL_STUDENT_GROUP_STUDENTS." WHERE tbl_student_group_id='".$tbl_student_group_id."') ORDER BY first_name ASC ";
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
	
	}
	
		/**
	* @desc		Activate Student Group
	* @access	default
	*/
	function activate_student_group($tbl_student_group_id,$tbl_school_id) {
		$tbl_student_group_id = $this->cf->get_data(trim($tbl_student_group_id));
		$qry = "UPDATE ".TBL_STUDENT_GROUP." SET is_active='Y' WHERE tbl_student_group_id='$tbl_student_group_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate Student Group
	* @param	string $tbl_message_group_id
	* @access	default
	*/
	function deactivate_student_group($tbl_student_group_id,$tbl_school_id) {
		$tbl_student_group_id = $this->cf->get_data(trim($tbl_student_group_id));
		$qry = "UPDATE ".TBL_STUDENT_GROUP." SET is_active='N' WHERE tbl_student_group_id='$tbl_student_group_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete Student Group
	* @param	string $tbl_message_group_id
	* @access	default
	*/
	function delete_student_group($tbl_student_group_id,$tbl_school_id) {
		$tbl_student_group_id = $this->cf->get_data(trim($tbl_student_group_id));
        $qry = "UPDATE ".TBL_STUDENT_GROUP." SET is_active='D' WHERE tbl_student_group_id='$tbl_student_group_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_TEACHER_GROUP." WHERE tbl_teacher_group_id='$tbl_teacher_group_id' AND  tbl_school_id='$tbl_school_id'";
		//$this->cf->deleteFrom($qry);
	}


    // add student group
	function save_student_group($tbl_student_group_id, $group_name_en, $group_name_ar, $str, $tbl_school_id, $group_for, $added_by, $added_type)
	{
           
		   $qryIns = "INSERT INTO ".TBL_STUDENT_GROUP." (`tbl_student_group_id` ,`group_name_en` ,`group_name_ar` ,`added_date` ,`is_active` ,`tbl_school_id`, `group_for`, `added_by`, `added_type`)
										VALUES ('$tbl_student_group_id', '$group_name_en', '$group_name_ar', NOW(), 'Y', '$tbl_school_id' , '$group_for', '$added_by', '$added_type')";
		   $this->cf->insertInto($qryIns);
		   
		   if($added_type=="T")
		   		$str  = explode(":", $str);
		   else
		   		$str  = explode("&", $str);
				
			for($m=0; $m<count($str); $m++) {
			  if($str[$m]<>""){
				    $tbl_student_id_t = $str[$m];
					$studentIds =  array();
					$studentIds = explode("*",$tbl_student_id_t);
		            $tbl_student_id = $studentIds[0];
					$tbl_class_id   = $studentIds[1];
					$tbl_student_group_students_id = substr(md5(uniqid(rand())),0,10);
					$qryIns = "INSERT INTO ".TBL_STUDENT_GROUP_STUDENTS." (`tbl_student_group_students_id`,`tbl_student_group_id`,`tbl_student_id`,`tbl_class_id`,`is_active`,`added_date`,`tbl_school_id`) 
											 VALUES ('$tbl_student_group_students_id','$tbl_student_group_id','$tbl_student_id','$tbl_class_id','Y', NOW(), '$tbl_school_id' ) ";
					$this->cf->insertInto($qryIns);
				}
		   }
	}

   function is_exist_student_group($tbl_student_group_id,$group_name_en,$tbl_school_id)
   {
	    $qry_exist = "SELECT * FROM ".TBL_STUDENT_GROUP." WHERE group_name_en='".$group_name_en."' AND tbl_school_id='".$tbl_school_id."' ";
		if($tbl_student_group_id<>"")
		{
			$qry_exist .= " AND tbl_student_group_id <> '".$tbl_student_group_id."' ";
		}
		$qry_exist .= " AND is_active <> 'D' ";
        $rs_exist  = $this->cf->selectMultiRecords($qry_exist);
		return  $rs_exist;
   }
	
	function get_student_group_info($tbl_student_group_id,$tbl_school_id)
	{
		$qry_msg = "SELECT * FROM ".TBL_STUDENT_GROUP." WHERE tbl_student_group_id='$tbl_student_group_id' AND  tbl_school_id='$tbl_school_id'"; 
		$rs_msg  = $this->cf->selectMultiRecords($qry_msg);
		return  $rs_msg;
	}

	
    function update_student_group($tbl_student_group_id,$group_name_en,$group_name_ar,$str,$tbl_school_id,$group_for,$added_type)
	{
           
		 $qry = "UPDATE ".TBL_STUDENT_GROUP." SET group_name_en='$group_name_en', group_name_ar='$group_name_ar', group_for = '$group_for'  WHERE tbl_student_group_id='$tbl_student_group_id' AND  tbl_school_id='$tbl_school_id' ";
		 $this->cf->update($qry);
		   
		   if($added_type=="T")
		    	$str               = explode(":", $str);
		   else	
		   		$str               = explode("&", $str);
		  
		   if(!empty($str))
		   { 
			   $qryDel = "DELETE FROM ".TBL_STUDENT_GROUP_STUDENTS." WHERE tbl_student_group_id='$tbl_student_group_id' AND  tbl_school_id='$tbl_school_id'";
			   $this->cf->deleteFrom($qryDel);
			 
				for($m=0; $m<count($str); $m++) {
				  if($str[$m]<>""){
						$tbl_student_id_t = $str[$m];
						$tbl_student_group_students_id = substr(md5(uniqid(rand())),0,10);
						$studentIds =  array();
						$studentIds = explode("*",$tbl_student_id_t);
						$tbl_student_id = $studentIds[0];
						$tbl_class_id   = $studentIds[1];
						$qryIns = "INSERT INTO ".TBL_STUDENT_GROUP_STUDENTS." (`tbl_student_group_students_id`,`tbl_student_group_id`,`tbl_student_id`,`tbl_class_id`, `is_active`,`added_date`,`tbl_school_id`) 
												 VALUES ('$tbl_student_group_students_id','$tbl_student_group_id','$tbl_student_id','$tbl_class_id','$is_active', NOW(), '$tbl_school_id' ) ";
						$this->cf->insertInto($qryIns);
					}
			   }
		   }
	}
	 
	 // END SCHOOL BACKEND STUDENTS GROUP MANAGEMENT
	
	
	
	function get_total_unread_message_teacher_community($tbl_teacher_id, $tbl_school_id, $message_mode, $tbl_community_id) {

		$qry = "SELECT count(tbl_message_id) as cntMessage,TG.group_name_en AS name_en,TG.group_name_ar AS name_ar,TG.tbl_teacher_group_id, TG.file_name_updated as picture FROM ".TBL_TEACHER_COMMUNITY_MESSAGE." AS TCM 
				LEFT JOIN ".TBL_TEACHER_GROUP."  AS TG ON TG.tbl_teacher_group_id=TCM.tbl_teacher_group_id
				WHERE TCM.is_active='Y' ";
		
		if($tbl_teacher_id<>"")
		  $qry .= " AND TCM.message_to='".$tbl_teacher_id."' ";
		  
		if ($message_mode != "") {
			$qry .= " AND TCM.message_mode='".$message_mode."'";
		}
		if ($tbl_school_id != "") {
			$qry .= " AND TCM.tbl_school_id='".$tbl_school_id."'";
		}
		$qry .= " AND TCM.read_status='N' ";
		
		$qry .= " GROUP BY TCM.tbl_teacher_group_id ";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
	
	function get_total_unread_message_teacher_private($tbl_teacher_id, $tbl_school_id, $message_mode, $message_from) {

		$qry = "SELECT count(tbl_message_id) as cntMessage,CONCAT(T.first_name, ' ', T.last_name) AS name_en, CONCAT(T.first_name_ar, ' ', T.last_name_ar) AS name_ar,TCM.message_from as tbl_teacher_group_id, T.file_name_updated as picture  FROM ".TBL_TEACHER_COMMUNITY_MESSAGE." AS TCM 
				LEFT JOIN ".TBL_TEACHER."  AS T ON T.tbl_teacher_id=TCM.message_from
				WHERE TCM.is_active='Y' ";
		
		if($tbl_teacher_id<>""){
		  $qry .= " AND TCM.message_to='".$tbl_teacher_id."' ";
		  $qry .= " AND TCM.message_from<>'".$tbl_teacher_id."' ";
		  
		}
		  
		if ($message_mode != "") {
			$qry .= " AND TCM.message_mode='".$message_mode."'";
		}
		if ($tbl_school_id != "") {
			$qry .= " AND TCM.tbl_school_id='".$tbl_school_id."'";
		}
		$qry .= " AND TCM.read_status='N' ";
		
		$qry .= " GROUP BY TCM.message_from ";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
	
	
	function saveCustomerEnquiry($name, $phone, $email_id, $company, $subject, $enquiry)
	{
		$enquiry_id = substr(md5(uniqid(rand())),0,10);
		$qry = "INSERT INTO ".TBL_CUSTOMER_ENQUIRY." (
					`enquiry_id` ,
					`name` ,
					`phone` ,
					`email_id` ,
					`company` ,
					`subject` ,
					`enquiry`,
					`added_date`
					)
					VALUES (
						'$enquiry_id', '$name', '$phone', '$email_id', '$company', '$subject', '$enquiry', NOW()
					)";
		$this->cf->insertInto($qry);	
	}

	
	
	 
	 
	 
}

?>

