<?php
include_once('include/common_functions.php');
/**
 * @desc   	  	School Model
 * @category   	Model
 * @author      Shanavas.PK
 * @version    	0.1
 */
class Model_school extends CI_Model {
	var $cf;

    function model_school() {
		$this->cf = new Common_functions();
    }

	function get_school_name($tbl_school_id,$lan="") {
		$tbl_school_id = $this->cf->get_data(trim($tbl_school_id));
		$qry = "SELECT school_name,school_name_ar FROM ".TBL_SCHOOL." WHERE tbl_school_id='$tbl_school_id'";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		if($lan=="ar")
		{
			return $rs[0]['school_name_ar'];
		}else{
			return $rs[0]['school_name'];
		}
	}


	function get_all_schools() {
		$qry = "SELECT * FROM ".TBL_SCHOOL." WHERE is_active='Y' ORDER BY school_name ASC";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}

	function get_behaviour_points($tbl_school_id) {
		$qry = "SELECT * FROM ".TBL_BEHAVIOUR_POINTS." WHERE is_active='Y' ";
		$qry .=  " AND (tbl_school_id='".$tbl_school_id."' OR  tbl_school_id='') ";
		$qry .=  " ORDER BY  behaviour_point ASC";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}

	function getPointById($tbl_point_category_id) {
		$qry = "SELECT * FROM ".TBL_BEHAVIOUR_POINTS." WHERE is_active='Y' and 	tbl_point_category_id='".$tbl_point_category_id."'";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}

    //backend functionality for school
	function get_school_details($tbl_school_id,$lan="") {
		$tbl_school_id = $this->cf->get_data(trim($tbl_school_id));
		$qry = "SELECT school_name,school_name_ar,logo,school_type FROM ".TBL_SCHOOL." WHERE tbl_school_id='$tbl_school_id' and is_active ='Y' ";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
	
	
	// Backend List Schools
	function list_all_schools($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		//if (trim($offset) == "") {$offset = 0;}
		
		$qry = "SELECT S.* FROM ".TBL_SCHOOL." AS S WHERE 1 ";
		if($tbl_school_id<>"")
		{
			$qry .= " AND S.tbl_school_id= '".$tbl_school_id."' ";
		}
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND S.is_active='$is_active' ";
		}
		$qry .= " AND S.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " AND ( S.school_name LIKE '%$q%' 
					   	OR S.school_name_ar LIKE '%$q%'
						OR S.school_type LIKE '%$q%'
						) ";
		}
	
	
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY S.school_name ASC ";
		}
	
	    if($offset<>"")
			$qry .=" LIMIT $offset, ".TBL_SCHOOL_PAGING;
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	/**
	* @desc		Get total teachers
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_schools($q, $is_active, $tbl_school_id) {
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT distinct(S.tbl_school_id) FROM ".TBL_SCHOOL." AS S WHERE 1 ";
			
		if($tbl_school_id<>"")
		{		
			$qry .= " AND S.tbl_school_id= '".$tbl_school_id."' ";	
		}
		//Active/Deactive
		if(trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND S.is_active='$is_active' ";
		}
		$qry .= " AND S.is_active<>'D' "; 
	//Search
		if (trim($q) != "") {
			$qry    .= " AND ( S.school_name LIKE '%$q%' 
					   	OR S.school_name_ar LIKE '%$q%'
						OR S.school_type LIKE '%$q%'
						) ";
		}
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
	}
	
	/**
	* @desc		Activate
	* @access	default
	*/
	function activate_school($tbl_school_id) {
		$qry = "UPDATE ".TBL_SCHOOL." SET is_active='Y' WHERE tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate
	* @access	default
	*/
	function deactivate_school($tbl_school_id) {
		$qry = "UPDATE ".TBL_SCHOOL." SET is_active='N' WHERE  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete
	* @access	default
	*/
	function delete_school($tbl_school_id) {
        $qry = "UPDATE ".TBL_SCHOOL." SET is_active='D' WHERE tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_CATEGORY." WHERE tbl_category_id='$tbl_category_id' ";
		//$this->cf->deleteFrom($qry);
	}
	
	
	function is_exist_school($tbl_school_id, $school_name) {
		$school_name  = $this->cf->get_data($school_name);
		$tbl_school_id   = $this->cf->get_data($tbl_school_id);
		$qry = "SELECT * FROM ".TBL_SCHOOL." WHERE 1 ";
	
		if (trim($school_name) != "") {
			$qry .= " AND school_name = '".$school_name."' ";
		}
		
		if (trim($tbl_school_id) != "") {
			$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		}
	    $qry .= " AND is_active<>'D' "; 
		$results = $this->cf->selectMultiRecords($qry);
		//print_r($results);
	return $results;	
	}
	
	
	function is_exist_school_user_id($tbl_school_id, $user_id) {
		$user_id         = $this->cf->get_data($user_id);
		$tbl_school_id   = $this->cf->get_data($tbl_school_id);
		$qry = "SELECT * FROM ".TBL_ADMIN_USERS." WHERE 1 ";
	
		if (trim($user_id) != "") {
			$qry .= " AND user_id='".$user_id."'" ;
		}
	
	    $qry .= " AND is_active<>'D' "; 
		$results = $this->cf->selectMultiRecords($qry);
		//print_r($results);
	return $results;	
	}
	
	
	
	
	function is_exist_edit_school($tbl_school_id, $school_name) {
		$school_name  = $this->cf->get_data($school_name);
		$tbl_school_id   = $this->cf->get_data($tbl_school_id);
		$qry = "SELECT * FROM ".TBL_SCHOOL." WHERE 1 ";
	
		if (trim($school_name) != "") {
			$qry .= " AND school_name = '".$school_name."' ";
		}
		
		if (trim($tbl_school_id) != "") {
			$qry .= " AND tbl_school_id <> '".$tbl_school_id."' ";
		}
	    $qry .= " AND is_active<>'D' "; 
		$results = $this->cf->selectMultiRecords($qry);
		//print_r($results);
	return $results;	
	}
	
	
	function is_exist_edit_school_user_id($tbl_school_id, $user_id) {
		$user_id         = $this->cf->get_data($user_id);
		$tbl_school_id   = $this->cf->get_data($tbl_school_id);
		$qry = "SELECT * FROM ".TBL_ADMIN_USERS." WHERE 1 ";
	
		if (trim($user_id) != "") {
			$qry .= " AND user_id='".$user_id."'" ;
		}
	    if (trim($tbl_school_id) != "") {
			$qry .= " AND tbl_school_id <> '".$tbl_school_id."' ";
		}
	    $qry .= " AND is_active<>'D' "; 
		$results = $this->cf->selectMultiRecords($qry);
		//print_r($results);
	return $results;	
	}
	
	
	function add_school($tbl_school_id, $school_name, $school_name_ar, $school_type, $email, $contact_person, $phone, $mobile, $fax, $address, $color_code, $is_active)
	{
		$tbl_school_id    		= $this->cf->get_data($tbl_school_id);
		$school_name        	= $this->cf->get_data($school_name);
		$school_name_ar         = $this->cf->get_data($school_name_ar);
		$school_type     		= $this->cf->get_data($school_type);
		$email             		= $this->cf->get_data($email);
		$contact_person         = $this->cf->get_data($contact_person);
		$phone            		= $this->cf->get_data($phone);
		$mobile            		= $this->cf->get_data($mobile);
		$fax             		= $this->cf->get_data($fax);
		$address             	= $this->cf->get_data($address);
		$color_code             = $this->cf->get_data($color_code);
		$is_active              = $this->cf->get_data($is_active);
		
		if($tbl_school_id<>"")
		{
			$qry_ins = "INSERT INTO ".TBL_SCHOOL." (`tbl_school_id`, `school_name`, `school_name_ar`, `school_type`, `email`, `contact_person`, `phone`, `mobile`,`fax`, `address`, `color_code`,`added_date`, `is_active`)
									VALUES ('$tbl_school_id', '$school_name', '$school_name_ar', '$school_type', '$email', '$contact_person', '$phone', '$mobile',  '$fax', '$address', '$color_code',  NOW(), '$is_active')";
			$this->cf->insertInto($qry_ins);
			
			$qry = "SELECT file_name_updated,file_name_original,file_type,file_size FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_school_id' AND is_active='Y' ";
			$result = $this->cf->selectMultiRecords($qry);
			if (count($result)>0) {
				$file_name_updated  = isset($result[0]['file_name_updated'])? $result[0]['file_name_updated']:'';
				$file_type          = isset($result[0]['file_type'])? $result[0]['file_type']:'';
				$file_size          = isset($result[0]['file_size'])? $result[0]['file_size']:'';
				$qry_update  = "UPDATE ".TBL_SCHOOL." SET logo='$file_name_updated' WHERE tbl_school_id='$tbl_school_id' ";
				$this->cf->update($qry_update);
			}
			return "Y";
		}else{
		   return "N";	
			
		}
	}
	
	
	function get_schoolObj($tbl_school_id) {
		$tbl_school_id = $this->cf->get_data(trim($tbl_school_id));
		$qry = "SELECT * FROM ".TBL_SCHOOL." WHERE tbl_school_id='$tbl_school_id'";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
	
	
	function update_school($tbl_school_id, $school_name, $school_name_ar, $school_type, $email, $contact_person, $phone, $mobile, $fax, $address, $color_code, $is_active)
	{
		$tbl_school_id    		= $this->cf->get_data($tbl_school_id);
		$school_name        	= $this->cf->get_data($school_name);
		$school_name_ar         = $this->cf->get_data($school_name_ar);
		$school_type     		= $this->cf->get_data($school_type);
		$email             		= $this->cf->get_data($email);
		$contact_person         = $this->cf->get_data($contact_person);
		$phone            		= $this->cf->get_data($phone);
		$mobile            		= $this->cf->get_data($mobile);
		$fax             		= $this->cf->get_data($fax);
		$address             	= $this->cf->get_data($address);
		$color_code             = $this->cf->get_data($color_code);
		$is_active              = $this->cf->get_data($is_active);
		
		
		if($tbl_school_id<>"")
		{
			$qry_update = "UPDATE ".TBL_SCHOOL." SET school_name='$school_name', school_name_ar='$school_name_ar', school_type='$school_type', email='$email', contact_person='$contact_person',
			               phone='$phone', mobile='$mobile', fax='$fax', address='$address', color_code='$color_code', is_active='$is_active'  WHERE tbl_school_id = '$tbl_school_id' ";
			//echo $qry_update;
			$this->cf->update($qry_update);
			
		    $qry = "SELECT file_name_updated,file_name_original,file_type,file_size FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_school_id' AND is_active='Y' ";
			$result = $this->cf->selectMultiRecords($qry);
			if (count($result)>0) {
				$file_name_updated  = isset($result[0]['file_name_updated'])? $result[0]['file_name_updated']:'';
				$file_type          = isset($result[0]['file_type'])? $result[0]['file_type']:'';
				$file_size          = isset($result[0]['file_size'])? $result[0]['file_size']:'';
				$qry_update  = "UPDATE ".TBL_SCHOOL." SET logo='$file_name_updated' WHERE tbl_school_id='$tbl_school_id' ";
				$this->cf->update($qry_update);
			}
			return "Y";
		}else{
		   return "N";	
			
		}
	}
	



    }
?>





