<?php

include_once('include/common_functions.php');
/** @desc -- Student Attendance Report Model */

class Model_report extends CI_Model {
	var $cf;
	/**
	* @desc Default constructor for the Controller
	* @access default
	*/

	function model_report() {
		$this->cf = new Common_functions();
	}

	
	function get_student_cards_report($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id, $tbl_academic_year, $tbl_semester_id, $tbl_teacher_id, $tbl_student_id,$tbl_class_id )
	{
		
		    $qryAcademic = " SELECT MIN(start_date) AS start_date, MAX(end_date) AS end_date FROM ".TBL_SEMESTER." WHERE  tbl_academic_year_id='$tbl_academic_year'"; 
		    $rsAcademic  = $this->cf->selectMultiRecords($qryAcademic);
			$start_date  = $rsAcademic[0]['start_date'];
			$end_date    = $rsAcademic[0]['end_date'];
		   
		    $qry_sub  = "";
            $qry_main = "";

            if ($tbl_teacher_id != "") {
				$qry_main .= " AND A.tbl_teacher_id ='".$tbl_teacher_id."'";
			}

			if ($tbl_class_id != "") {
				$qry_main .= " AND A.tbl_class_id ='".$tbl_class_id."'";
			}

			if ($tbl_student_id != "" && $tbl_student_id != "0") {
				$qry_main .= " AND A.tbl_student_id ='".$tbl_student_id."'";
			}	
				
            if ($tbl_semester_id != "") {
				$qry_main .= " AND A.tbl_semester_id ='".$tbl_semester_id."'";
			}
			
			$qry_sub = '';
			if ($tbl_student_id != "" && $tbl_student_id != "0") {
				$qry_sub .= " AND tbl_student_id ='".$tbl_student_id."'";
			}
			if ($tbl_class_id != "") {
				$qry_sub .= " AND tbl_class_id ='".$tbl_class_id."'";
			}	
			$qry_sub .= " AND  tbl_school_id= '".$tbl_school_id."'";

            $Query = "SELECT SUM(A.card_point) AS total, A.card_type AS card_type, A.tbl_card_id, B.category_name_en, B.category_name_ar FROM ".TBL_CARDS." A 
			          LEFT JOIN ".TBL_CARD_POINTS." B ON A.card_type = B.tbl_card_category_id 
					  WHERE A.tbl_school_id='".$tbl_school_id."' ".$qry_main." ";
			if($tbl_academic_year<>"")
			{
				$Query .=" AND A.added_date BETWEEN '".$start_date."' AND '".$end_date."' ";
			}
			$Query .="  AND tbl_student_id IN (SELECT tbl_student_id FROM ".TBL_STUDENT." WHERE 1 ".$qry_sub.") GROUP BY A.card_type ORDER BY SUM(A.card_point) ASC";
			
			if($offset<>"")
				 $Query .=" LIMIT $offset, ".TBL_CARD_CATEGORY_PAGING;
			
		//echo $qry; exit;
		$rs = $this->cf->selectMultiRecords($Query);
		return $rs;	
	}
	
	function get_total_student_cards_report($q, $is_active, $tbl_school_id, $tbl_academic_year, $tbl_semester_id, $tbl_teacher_id, $tbl_student_id, $tbl_class_id)
	{
		
		    $qryAcademic = " SELECT MIN(start_date) AS start_date, MAX(end_date) AS end_date FROM ".TBL_SEMESTER." WHERE  tbl_academic_year_id='$tbl_academic_year'"; 
		    $rsAcademic  = $this->cf->selectMultiRecords($qryAcademic);
			$start_date  = $rsAcademic[0]['start_date'];
			$end_date    = $rsAcademic[0]['end_date']; 
			
			$qry_sub  = "";
            $qry_main = "";

            if ($tbl_teacher_id != "") {
				$qry_main .= " AND A.tbl_teacher_id ='".$tbl_teacher_id."'";
			}

			if ($tbl_class_id != "") {
				$qry_main .= " AND A.tbl_class_id ='".$tbl_class_id."'";
			}

			if ($tbl_student_id != "" && $tbl_student_id != "0") {
				$qry_main .= " AND A.tbl_student_id ='".$tbl_student_id."'";
			}	
				
            if ($tbl_semester_id != "") {
				$qry_main .= " AND A.tbl_semester_id ='".$tbl_semester_id."'";
			}
			
			$qry_sub = '';
			if ($tbl_student_id != "" && $tbl_student_id != "0") {
				$qry_sub .= " AND tbl_student_id ='".$tbl_student_id."'";
			}	
		    if ($tbl_class_id != "") {
				$qry_sub .= " AND tbl_class_id ='".$tbl_class_id."'";
			}
			$qry_sub .= " AND  tbl_school_id= '".$tbl_school_id."'";

			$Query = "SELECT A.card_type AS card_type FROM ".TBL_CARDS." A 
			          LEFT JOIN ".TBL_CARD_POINTS." B ON A.card_type = B.tbl_card_category_id 
					  WHERE A.tbl_school_id='".$tbl_school_id."' ".$qry_main." ";
			if($tbl_academic_year<>"")
			{
				$Query .=" AND A.added_date BETWEEN '".$start_date."' AND '".$end_date."' ";
			}
			$Query .="  AND tbl_student_id IN (SELECT tbl_student_id FROM ".TBL_STUDENT." WHERE 1 ".$qry_sub.") GROUP BY A.card_type";
			
		
		//echo $qry; exit;
		$rs = $this->cf->selectMultiRecords($Query);
		return count($rs);	
	}
	
    function get_student_cards_report_detailed($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id, $tbl_academic_year, $tbl_semester_id, $tbl_teacher_id, $tbl_student_id,$tbl_class_id, $card_type_id)
	{
		
		    $qryAcademic = " SELECT MIN(start_date) AS start_date, MAX(end_date) AS end_date FROM ".TBL_SEMESTER." WHERE  tbl_academic_year_id='$tbl_academic_year'"; 
		    $rsAcademic  = $this->cf->selectMultiRecords($qryAcademic);
			$start_date  = $rsAcademic[0]['start_date'];
			$end_date    = $rsAcademic[0]['end_date'];
		   
		    $qry_sub  = "";
            $qry_main = "";

            if ($tbl_teacher_id != "") {
				$qry_main .= " AND A.tbl_teacher_id ='".$tbl_teacher_id."'";
			}

			if ($tbl_class_id != "") {
				$qry_main .= " AND A.tbl_class_id ='".$tbl_class_id."'";
			}

			if ($tbl_student_id != "" && $tbl_student_id != "0") {
				$qry_main .= " AND A.tbl_student_id ='".$tbl_student_id."'";
			}	
				
            if ($tbl_semester_id != "") {
				$qry_main .= " AND A.tbl_semester_id ='".$tbl_semester_id."'";
			}
			
			$qry_sub = '';
			if ($tbl_student_id != "" && $tbl_student_id != "0") {
				$qry_sub .= " AND tbl_student_id ='".$tbl_student_id."'";
			}
			if ($tbl_class_id != "") {
				$qry_sub .= " AND tbl_class_id ='".$tbl_class_id."'";
			}	
			$qry_sub .= " AND  tbl_school_id= '".$tbl_school_id."'";

            $Query = "SELECT A.card_point, A.card_type AS card_type, A.tbl_card_id, A.card_issue_type, A.added_date,  B.category_name_en, B.category_name_ar, 
			          STU.first_name, STU.last_name, STU.first_name_ar, STU.last_name_ar, 
					  T.first_name AS teacher_first_name, T.last_name AS teacher_last_name, T.first_name_ar AS teacher_first_name_ar, T.last_name_ar AS teacher_last_name_ar, 
					  C.class_name, C.class_name_ar, S.section_name, S.section_name_ar, TYPE.school_type, TYPE.school_type_ar 
					  FROM ".TBL_CARDS." A 
			          LEFT JOIN ".TBL_CARD_POINTS." B ON A.card_type = B.tbl_card_category_id 
					  LEFT JOIN ".TBL_STUDENT." STU ON STU.tbl_student_id = A.tbl_student_id
					  LEFT JOIN ".TBL_TEACHER." T ON T.tbl_teacher_id = A.tbl_teacher_id
					  LEFT JOIN ".TBL_CLASS."  AS C ON  C.tbl_class_id = STU.tbl_class_id
					  LEFT JOIN  ".TBL_SECTION." AS S  ON  C.tbl_section_id = S.tbl_section_id  
					  LEFT JOIN  ".TBL_SCHOOL_TYPE." AS TYPE  ON TYPE.tbl_school_type_id= C.tbl_school_type_id  
					  
					  WHERE A.tbl_school_id='".$tbl_school_id."' ".$qry_main." ";
			if($tbl_academic_year<>"")
			{
				$Query .=" AND A.added_date BETWEEN '".$start_date."' AND '".$end_date."' ";
			}
			if($card_type_id<>"")
			{
				$Query .=" AND A.card_type='".$card_type_id."' ";
				
			}
			$Query .=" ORDER BY A.added_date DESC";
			
			if($offset<>"")
				 $Query .=" LIMIT $offset, ".TBL_CARD_CATEGORY_PAGING;
		$rs = $this->cf->selectMultiRecords($Query);
		return $rs;	
	}
	
	function get_total_student_cards_report_detailed($q, $is_active, $tbl_school_id, $tbl_academic_year, $tbl_semester_id, $tbl_teacher_id, $tbl_student_id, $tbl_class_id, $card_type_id )
	{
		
		    $qryAcademic = " SELECT MIN(start_date) AS start_date, MAX(end_date) AS end_date FROM ".TBL_SEMESTER." WHERE  tbl_academic_year_id='$tbl_academic_year'"; 
		    $rsAcademic  = $this->cf->selectMultiRecords($qryAcademic);
			$start_date  = $rsAcademic[0]['start_date'];
			$end_date    = $rsAcademic[0]['end_date']; 
			
			$qry_sub  = "";
            $qry_main = "";

            if ($tbl_teacher_id != "") {
				$qry_main .= " AND A.tbl_teacher_id ='".$tbl_teacher_id."'";
			}

			if ($tbl_class_id != "") {
				$qry_main .= " AND A.tbl_class_id ='".$tbl_class_id."'";
			}

			if ($tbl_student_id != "" && $tbl_student_id != "0") {
				$qry_main .= " AND A.tbl_student_id ='".$tbl_student_id."'";
			}	
				
            if ($tbl_semester_id != "") {
				$qry_main .= " AND A.tbl_semester_id ='".$tbl_semester_id."'";
			}
			
			$qry_sub = '';
			if ($tbl_student_id != "" && $tbl_student_id != "0") {
				$qry_sub .= " AND tbl_student_id ='".$tbl_student_id."'";
			}	
		    if ($tbl_class_id != "") {
				$qry_sub .= " AND tbl_class_id ='".$tbl_class_id."'";
			}
			$qry_sub .= " AND  tbl_school_id= '".$tbl_school_id."'";
			
			$Query = "SELECT A.card_point FROM ".TBL_CARDS." A 
			          LEFT JOIN ".TBL_CARD_POINTS." B ON A.card_type = B.tbl_card_category_id 
					  LEFT JOIN ".TBL_STUDENT." STU ON STU.tbl_student_id = A.tbl_student_id
					  LEFT JOIN ".TBL_TEACHER." T ON T.tbl_teacher_id = A.tbl_teacher_id
					  WHERE A.tbl_school_id='".$tbl_school_id."' ".$qry_main." ";
			if($tbl_academic_year<>"")
			{
				$Query .=" AND A.added_date BETWEEN '".$start_date."' AND '".$end_date."' ";
			}
			if($card_type_id<>"")
			{
				$Query .=" AND A.card_type='".$card_type_id."' ";
				
			}
			$Query .=" ORDER BY A.added_date DESC";
			
			
		
		//echo $qry; exit;
		$rs = $this->cf->selectMultiRecords($Query);
		return count($rs);	
	}
	
	
	
	
}

?>



