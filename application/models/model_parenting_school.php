<?php
include_once('include/common_functions.php');
/**
 * @desc   	  	Parenting Model
 * @category   	Model
 * @author     	Shanavas .PK
 * @version    	0.1
 */

class Model_parenting_school extends CI_Model {
	var $cf;
	/**
	* @desc Default constructor for the Controller
	* @access default
	*/
    function model_parenting_school() {
		$this->cf = new Common_functions();
    }

	/**
	* @desc		Get parenting_school categories
	* @param	
	* @access	default
	* @return	$tbl_parent_id
	*/
	function get_parenting_school_categories($tbl_school_id) {
		$qry = "SELECT * FROM ".TBL_PARENTING_SCHOOL_CAT." WHERE is_active='Y' AND tbl_school_id='".$tbl_school_id."' ORDER BY parenting_school_cat_order ASC";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}

   /**
	* @desc		Get parenting_school data
	* @param	string $tbl_parenting_school_cat_id  
	* @access	default
	* @return	$tbl_parent_id
	*/
	function get_parenting_school_data($tbl_parenting_school_cat_id, $tbl_school_id, $student_id='')  {
		$qry = "SELECT * FROM ".TBL_PARENTING_SCHOOL." WHERE tbl_parenting_school_cat_id='".$tbl_parenting_school_cat_id."' AND tbl_school_id='".$tbl_school_id."'";
		if($student_id<>"")
		{
			$qry .= " AND tbl_parenting_school_id IN  (SELECT tbl_parenting_school_id FROM ".TBL_ASSIGN_RECORDS." WHERE tbl_student_id='$student_id' AND tbl_school_id='".$tbl_school_id."' )";
		}
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}

	/**
	* @desc		Get parenting_school videos
	* @param	 
	* @access	default
	* @return	record set
	*/

	function get_parenting_school_video($tbl_parenting_school_id) {
		$qry = "SELECT * FROM ".TBL_UPLOADS." WHERE tbl_item_id='".$tbl_parenting_school_id."' ";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs[0]["file_name_updated"];
	}

	/**
	* @desc		Get parenting_school text
	* @param	 
	* @access	default
	* @return	record set
	*/
	function get_parenting_school_text($tbl_parenting_school_id) {
		$qry = "SELECT * FROM ".TBL_PARENTING_SCHOOL." WHERE tbl_parenting_school_id='".$tbl_parenting_school_id."'";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}

   
   
    //BACKEND FUNCTIONALITY
		
	function get_parenting_datas($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parenting_school_cat_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry  = " SELECT PS.*,PSC.title_en,PSC.title_ar FROM ".TBL_PARENTING_SCHOOL." AS PS LEFT JOIN ".TBL_PARENTING_SCHOOL_CAT." AS PSC ON 
		          PSC.tbl_parenting_school_cat_id= PS.tbl_parenting_school_cat_id  WHERE 1  ";
		if($tbl_school_id<>""){
			$qry .= " AND PS.tbl_school_id='".$tbl_school_id."'";
		}
		if($tbl_parenting_school_cat_id<>"")
			$qry .= " AND PS.tbl_parenting_school_cat_id='$tbl_parenting_school_cat_id' "; 
		
		if($student_id<>"")
		{
			$qry .= " AND PS.tbl_parenting_school_id IN  (SELECT tbl_parenting_school_id FROM ".TBL_ASSIGN_RECORDS." WHERE tbl_student_id='$student_id' AND tbl_school_id='".$tbl_school_id."' )";
		}
		
		//Active/Deactive
		if(trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PS.is_active='$is_active' ";
		}
		$qry .= " AND PS.is_active<>'D' "; 
			
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( PS.parenting_title_en LIKE '%$q%' 
					   	OR PS.parenting_title_ar LIKE '%$q%' 
					   	OR PSC.title_en LIKE '%$q%' 
					   	OR PSC.title_ar LIKE '%$q%' ) ";
		}
		
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY PS.$sort_name $sort_by";
		} else {
			$qry .= " ORDER BY PS.id DESC";
		}
		$qry .=" LIMIT $offset, ".TBL_PARENTING_SCHOOL_PAGING;
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
		
      }

      function get_total_parenting_datas($q, $is_active, $tbl_school_id, $tbl_parenting_school_cat_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
		
		$qry  = "SELECT PS.tbl_parenting_school_id FROM ".TBL_PARENTING_SCHOOL." AS PS LEFT JOIN ".TBL_PARENTING_SCHOOL_CAT." AS PSC ON 
		         PSC.tbl_parenting_school_cat_id= PS.tbl_parenting_school_cat_id  WHERE 1  ";
		
	    if($tbl_school_id<>"")
			$qry .= " AND PS.tbl_school_id='".$tbl_school_id."'";
			
		if($tbl_parenting_school_cat_id<>""){
			$qry .= " AND PS.tbl_parenting_school_cat_id='$tbl_parenting_school_cat_id' "; 
		}
		if($student_id<>"")
		{
			$qry .= " AND PS.tbl_parenting_school_id IN  (SELECT tbl_parenting_school_id FROM ".TBL_ASSIGN_RECORDS." WHERE tbl_student_id='$student_id' AND tbl_school_id='".$tbl_school_id."' )";
		}
		
		//Active/Deactive
		if(trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PS.is_active='$is_active' ";
		}
		$qry .= " AND PS.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( PS.parenting_title_en LIKE '%$q%' 
					   	OR PS.parenting_title_ar LIKE '%$q%' 
					   	OR PSC.title_en LIKE '%$q%' 
					   	OR PSC.title_ar LIKE '%$q%' ) ";
		}
		
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
		
      }
	  
       /**
	* @desc		Add Records
	* @param	String $tbl_parenting_school_id,$tbl_parenting_school_cat_id,$title,$title_ar,$parenting_type,$parenting_text_en,$parenting_text_ar,
		   $parenting_url,$is_active,$tbl_school_id
	* @access	default
	* @return	none
	*/
	function add_record($tbl_parenting_school_id,$tbl_parenting_school_cat_id,$parenting_title_en,$parenting_title_ar,$parenting_type,$parenting_text_en,$parenting_text_ar,
		   $parenting_url,$is_active,$tbl_school_id)
	{
			$tbl_parenting_school_id          = $this->cf->get_data(trim($tbl_parenting_school_id));
			$tbl_parenting_school_cat_id      = $this->cf->get_data(trim($tbl_parenting_school_cat_id));
			$parenting_title_en               = $this->cf->get_data(trim($parenting_title_en));
			$parenting_title_ar               = $this->cf->get_data(trim($parenting_title_ar));
			$parenting_type                   = $this->cf->get_data(trim($parenting_type));
			$parenting_text_en                = $this->cf->get_data(trim($parenting_text_en));
			$parenting_text_ar                = $this->cf->get_data(trim($parenting_text_ar));
			$parenting_url                    = $this->cf->get_data(trim($parenting_url));
			$is_active                        = $this->cf->get_data(trim($is_active));
	
			$qry = "INSERT INTO ".TBL_PARENTING_SCHOOL." (`tbl_parenting_school_id`, `tbl_parenting_school_cat_id`, `parenting_title_en`, `parenting_title_ar`, `parenting_type`, `parenting_text_en`, `parenting_text_ar`, `parenting_url`, `is_active`, `added_date`, `tbl_school_id`)
					VALUES ('$tbl_parenting_school_id', '$tbl_parenting_school_cat_id', '$parenting_title_en', '$parenting_title_ar', '$parenting_type', '$parenting_text_en', '$parenting_text_ar', '$parenting_url', '$is_active', NOW(), '$tbl_school_id') ";
			//echo $qry;
			$this->cf->insertInto($qry);
			
			$qry = "SELECT file_name_updated FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_parenting_school_id' AND module_name='parenting_logo' AND is_active='Y' ";
			$result = $this->cf->selectMultiRecords($qry);
			if (count($result)>0) {
				$file_name_updated  = isset($result[0]['file_name_updated'])? $result[0]['file_name_updated']:'';
				$qry_update  = "UPDATE ".TBL_PARENTING_SCHOOL." SET parenting_logo='$file_name_updated'  WHERE tbl_parenting_school_id='$tbl_parenting_school_id' ";
				$this->cf->update($qry_update);
				
				$qry_del = "DELETE FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_parenting_school_id' AND module_name='parenting_logo' AND is_active='Y' ";
			    $this->cf->deleteFrom($qry_del);
				
			}
			
	}
	
	
    function get_school_record($tbl_parenting_school_id,$tbl_school_id)
	{
		$qry_msg = "SELECT * FROM ".TBL_PARENTING_SCHOOL." WHERE tbl_parenting_school_id='$tbl_parenting_school_id' AND  tbl_school_id='$tbl_school_id'"; 
		$rs_msg  = $this->cf->selectMultiRecords($qry_msg);
		$tbl_parenting_school_id = $rs_msg[0]['tbl_parenting_school_id'];
		
		$video_info = $this->parenting_school_video_info($tbl_parenting_school_id);
		$rs_msg[0]['file_name_updated'] = $video_info[0]['file_name_updated'];
		$rs_msg[0]['tbl_uploads_id']    = $video_info[0]['tbl_uploads_id'];
		$rs_msg[0]['file_name_original']= $video_info[0]['file_name_original'];
		return  $rs_msg;
	}
	
	function parenting_school_video_info($tbl_parenting_school_id) {
		$qry = "SELECT * FROM ".TBL_UPLOADS." WHERE tbl_item_id='".$tbl_parenting_school_id."' AND module_name='parenting_file'";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
	
	
	function update_school_record($tbl_parenting_school_id,$tbl_parenting_school_cat_id,$parenting_title_en,$parenting_title_ar,$parenting_type,$parenting_text_en,$parenting_text_ar,$parenting_url,$is_active,$tbl_school_id)
	{
			$tbl_parenting_school_id          = $this->cf->get_data(trim($tbl_parenting_school_id));
			$tbl_parenting_school_cat_id      = $this->cf->get_data(trim($tbl_parenting_school_cat_id));
			$parenting_title_en               = $this->cf->get_data(trim($parenting_title_en));
			$parenting_title_ar               = $this->cf->get_data(trim($parenting_title_ar));
			$parenting_type                   = $this->cf->get_data(trim($parenting_type));
			$parenting_text_en                = $this->cf->get_data(trim($parenting_text_en));
			$parenting_text_ar                = $this->cf->get_data(trim($parenting_text_ar));
			$parenting_url                    = $this->cf->get_data(trim($parenting_url));
			$is_active                        = $this->cf->get_data(trim($is_active));
			
			$qry_update  = "UPDATE ".TBL_PARENTING_SCHOOL." SET tbl_parenting_school_cat_id='$tbl_parenting_school_cat_id', parenting_title_en='$parenting_title_en',        
			                parenting_title_ar='$parenting_title_ar',parenting_type='$parenting_type', parenting_text_en='$parenting_text_en',
							parenting_text_ar='$parenting_text_ar',parenting_url='$parenting_url',is_active='$is_active' 
							WHERE tbl_parenting_school_id='$tbl_parenting_school_id' AND tbl_school_id= '$tbl_school_id' ";
			$this->cf->update($qry_update);
			
			$qry = "SELECT file_name_updated FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_parenting_school_id' AND module_name='parenting_logo' AND is_active='Y' ";
			$result = $this->cf->selectMultiRecords($qry);
			if (count($result)>0) {
				$file_name_updated  = isset($result[0]['file_name_updated'])? $result[0]['file_name_updated']:'';
				$qry_update  = "UPDATE ".TBL_PARENTING_SCHOOL." SET parenting_logo='$file_name_updated'  WHERE tbl_parenting_school_id='$tbl_parenting_school_id' ";
				$this->cf->update($qry_update);
				
				$qry_del = "DELETE FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_parenting_school_id' AND module_name='parenting_logo' AND is_active='Y' ";
			    $this->cf->deleteFrom($qry_del);
				
			}
			
	}
	
	
	/**
	* @desc		Activate School Records
	* @access	default
	*/
	function activate_school_record($tbl_parenting_school_id,$tbl_school_id) {
		$tbl_parenting_school_id = $this->cf->get_data(trim($tbl_parenting_school_id));
		$qry = "UPDATE ".TBL_PARENTING_SCHOOL." SET is_active='Y' WHERE tbl_parenting_school_id='$tbl_parenting_school_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate School Records
	* @param	string $tbl_message_group_id
	* @access	default
	*/
	function deactivate_school_record($tbl_parenting_school_id,$tbl_school_id) {
		$tbl_parenting_school_id = $this->cf->get_data(trim($tbl_parenting_school_id));
		$qry = "UPDATE ".TBL_PARENTING_SCHOOL." SET is_active='N' WHERE tbl_parenting_school_id='$tbl_parenting_school_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete School Records
	* @param	string $tbl_message_group_id
	* @access	default
	*/
	function delete_school_record($tbl_parenting_school_id,$tbl_school_id) {
		$tbl_parenting_school_id = $this->cf->get_data(trim($tbl_parenting_school_id));
        $qry = "UPDATE ".TBL_PARENTING_SCHOOL." SET is_active='D' WHERE tbl_parenting_school_id='$tbl_parenting_school_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_PARENTING_SCHOOL." WHERE tbl_parenting_school_id='$tbl_parenting_school_id' AND  tbl_school_id='$tbl_school_id'";
		//$this->cf->deleteFrom($qry);
	}


    // END SCHOOL RECORDS
	
	//START SCHOOL RECORDS CATEGORIES

   	function get_parenting_categories_list($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parenting_school_cat_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = $this->cf->get_data($q);
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry  = " SELECT PSC.* FROM ".TBL_PARENTING_SCHOOL_CAT." AS PSC  WHERE 1  ";
		if($tbl_school_id<>""){
			$qry .= " AND PSC.tbl_school_id='".$tbl_school_id."'";
		}
		if($tbl_parenting_school_cat_id<>"")
			$qry .= " AND PSC.tbl_parenting_school_cat_id='$tbl_parenting_school_cat_id' "; 
		
		//Active/Deactive
		if(trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PSC.is_active='$is_active' ";
		}
		$qry .= " AND PSC.is_active<>'D' "; 
		
		
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY PSC.$sort_name $sort_by";
		} else {
			$qry .= " ORDER BY PSC.id DESC";
		}
		$qry .=" LIMIT $offset, ".TBL_PARENTING_SCHOOL_CAT_PAGING;
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
		
      }

      function get_total_parenting_categories_list($q, $is_active, $tbl_school_id, $tbl_parenting_school_cat_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = $this->cf->get_data($q);
		$is_active  = $this->cf->get_data($is_active);
		
		$qry  = "SELECT PSC.tbl_parenting_school_cat_id FROM ".TBL_PARENTING_SCHOOL_CAT." AS PSC  WHERE 1  ";
		
	    if($tbl_school_id<>"")
			$qry .= " AND PSC.tbl_school_id='".$tbl_school_id."'";
			
		if($tbl_parenting_school_cat_id<>""){
			$qry .= " AND PSC.tbl_parenting_school_cat_id='$tbl_parenting_school_cat_id' "; 
		}
	
		//Active/Deactive
		if(trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PSC.is_active='$is_active' ";
		}
		$qry .= " AND PSC.is_active<>'D' "; 
		
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
		
      }
	  
	  
	  function is_exist_records_category($tbl_parenting_school_cat_id, $title_en, $title_ar, $tbl_school_id) {
		$tbl_parenting_school_cat_id  	= $this->cf->get_data($tbl_parenting_school_cat_id);
		$title_en 		               = $this->cf->get_data($title_en);
		$title_ar                       = $this->cf->get_data($title_ar);

		$qry = "SELECT * FROM ".TBL_PARENTING_SCHOOL_CAT." WHERE 1 ";
		if (trim($title_en) != "") {
			 $qry .= " AND title_en='$title_en' ";
		}
		
		if (trim($tbl_parenting_school_cat_id) != "") {
			$qry .= " AND tbl_parenting_school_cat_id <> '$tbl_parenting_school_cat_id'";
		}
	    $qry .= " AND is_active<>'D' "; 
		$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}
	  
       /**
	* @desc		Add Records Category
	* @param	String $tbl_parenting_school_id,$tbl_parenting_school_cat_id,$title,$title_ar,$parenting_type,$parenting_text_en,$parenting_text_ar,
		   $parenting_url,$is_active,$tbl_school_id
	* @access	default
	* @return	none
	*/
	function add_records_category($tbl_parenting_school_cat_id,$title_en,$title_ar,$tbl_school_id)
	{
			$tbl_parenting_school_cat_id      = $this->cf->get_data(trim($tbl_parenting_school_cat_id));
			$title_en                         = $this->cf->get_data(trim($title_en));
			$title_ar                         = $this->cf->get_data(trim($title_ar));
			$tbl_school_id                    = $this->cf->get_data(trim($tbl_school_id));
	
			$qry = "INSERT INTO ".TBL_PARENTING_SCHOOL_CAT." ( `tbl_parenting_school_cat_id`, `title_en`, `title_ar`, `added_date`, `tbl_school_id`)
					VALUES ('$tbl_parenting_school_cat_id', '$title_en', '$title_ar', NOW(), '$tbl_school_id') ";
			//echo $qry;
			$this->cf->insertInto($qry);
			
	}
	
    function get_school_record_category($tbl_parenting_school_cat_id,$tbl_school_id)
	{
		$qry_msg = "SELECT * FROM ".TBL_PARENTING_SCHOOL_CAT." WHERE tbl_parenting_school_cat_id='$tbl_parenting_school_cat_id' AND  tbl_school_id='$tbl_school_id'"; 
		$rs_msg  = $this->cf->selectMultiRecords($qry_msg);
		return  $rs_msg;
	}

	
	
	function update_school_record_category($tbl_parenting_school_cat_id,$title_en,$title_ar,$tbl_school_id)
	{
			$tbl_parenting_school_cat_id      = $this->cf->get_data(trim($tbl_parenting_school_cat_id));
			$title_en                         = $this->cf->get_data(trim($title_en));
			$title_ar                         = $this->cf->get_data(trim($title_ar));
			$tbl_school_id                    = $this->cf->get_data(trim($tbl_school_id));
			
			$qry_update  = "UPDATE ".TBL_PARENTING_SCHOOL_CAT." SET title_en='$title_en', title_ar='$title_ar'
							WHERE tbl_parenting_school_cat_id='$tbl_parenting_school_cat_id' AND tbl_school_id= '$tbl_school_id' ";
			$this->cf->update($qry_update);
			
	}
	
	
	/**
	* @desc		Activate School Records Category
	* @access	default
	*/
	function activate_school_record_category($tbl_parenting_school_cat_id,$tbl_school_id) {
		$tbl_parenting_school_cat_id = $this->cf->get_data(trim($tbl_parenting_school_cat_id));
		$qry = "UPDATE ".TBL_PARENTING_SCHOOL_CAT." SET is_active='Y' WHERE tbl_parenting_school_cat_id='$tbl_parenting_school_cat_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate School Records
	* @param	string $tbl_message_group_id
	* @access	default
	*/
	function deactivate_school_record_category($tbl_parenting_school_cat_id,$tbl_school_id) {
		$tbl_parenting_school_cat_id = $this->cf->get_data(trim($tbl_parenting_school_cat_id));
		$qry = "UPDATE ".TBL_PARENTING_SCHOOL_CAT." SET is_active='N' WHERE tbl_parenting_school_cat_id='$tbl_parenting_school_cat_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete School Records
	* @param	string $tbl_message_group_id
	* @access	default
	*/
	function delete_school_record_category($tbl_parenting_school_cat_id,$tbl_school_id) {
		$tbl_parenting_school_cat_id = $this->cf->get_data(trim($tbl_parenting_school_cat_id));
        $qry = "UPDATE ".TBL_PARENTING_SCHOOL_CAT." SET is_active='D' WHERE tbl_parenting_school_cat_id='$tbl_parenting_school_cat_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_PARENTING_SCHOOL." WHERE tbl_parenting_school_id='$tbl_parenting_school_id' AND  tbl_school_id='$tbl_school_id'";
		//$this->cf->deleteFrom($qry);
	}
	
	//START SCHOOL RECORDS CATEGORIES
	
	
	//START ASSIGN SCHOOL RECORDS 

   	function get_list_assign_records($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id, $tbl_parenting_school_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = $this->cf->get_data($q);
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry  = " SELECT AR.tbl_parenting_school_assign_id,AR.tbl_parenting_school_id,AR.assigned_date,AR.is_active, PS.parenting_title_en,
		          PS.parenting_title_ar, PS.parenting_type, S.first_name, S.first_name_ar,
		          S.last_name, S.last_name_ar, C.class_name, C.class_name_ar, SEC.section_name, SEC.section_name_ar  
				  FROM ".TBL_ASSIGN_RECORDS." AS AR 
		          LEFT JOIN ".TBL_PARENTING_SCHOOL." AS PS ON PS.tbl_parenting_school_id =  AR.tbl_parenting_school_id
				  LEFT JOIN ".TBL_STUDENT." AS S ON S.tbl_student_id =  AR.tbl_student_id
				  LEFT JOIN ".TBL_CLASS."  AS C ON S.tbl_class_id =  C.tbl_class_id
		          LEFT JOIN  ".TBL_SECTION." AS SEC  ON  SEC.tbl_section_id = C.tbl_section_id  
		          WHERE 1  ";
		if($tbl_school_id<>""){
			$qry .= " AND AR.tbl_school_id='".$tbl_school_id."'";
		}
		if($tbl_parenting_school_id<>"")
			$qry .= " AND AR.tbl_parenting_school_id='$tbl_parenting_school_id' "; 
		
		//Active/Deactive
		if(trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND AR.is_active='$is_active' ";
		}
		$qry .= " AND AR.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( S.first_name LIKE '%$q%' OR S.last_name LIKE '%$q%' 
			            OR S.first_name_ar LIKE '%$q%' OR S.last_name_ar LIKE '%$q%' 
			            OR ( CONCAT(S.first_name,' ',S.last_name) LIKE '%$q%' 
					   	OR CONCAT(TRIM(S.first_name_ar),' ',TRIM(S.last_name_ar)) LIKE '%$q%' 
					   	OR CONCAT(TRIM(PS.parenting_title_en) LIKE '%$q%' 
					   	OR CONCAT(TRIM(PS.parenting_title_ar) LIKE '%$q%'
						
						) ";

		}
		
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY AR.$sort_name $sort_by";
		} else {
			$qry .= " ORDER BY AR.id DESC";
		}
		$qry .=" LIMIT $offset, ".TBL_PARENTING_SCHOOL_PAGING;
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
		
      }

      function get_total_assign_records($q, $is_active, $tbl_school_id, $tbl_parenting_school_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = $this->cf->get_data($q);
		$is_active  = $this->cf->get_data($is_active);
		
		$qry  = "SELECT AR.tbl_parenting_school_id FROM ".TBL_ASSIGN_RECORDS."  AS AR 
		          LEFT JOIN ".TBL_PARENTING_SCHOOL." AS PS ON PS.tbl_parenting_school_id =  AR.tbl_parenting_school_id
				  LEFT JOIN ".TBL_STUDENT." AS S ON S.tbl_student_id =  AR.tbl_student_id
				  LEFT JOIN ".TBL_CLASS."  AS C ON S.tbl_class_id =  C.tbl_class_id
		          LEFT JOIN  ".TBL_SECTION." AS SEC  ON  SEC.tbl_section_id = C.tbl_section_id  
		          WHERE 1  ";
		if($tbl_school_id<>""){
			$qry .= " AND AR.tbl_school_id='".$tbl_school_id."'";
		}
		if($tbl_parenting_school_id<>"")
			$qry .= " AND AR.tbl_parenting_school_id='$tbl_parenting_school_id' "; 
		
		//Active/Deactive
		if(trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND AR.is_active='$is_active' ";
		}
		$qry .= " AND AR.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " AND ( S.first_name LIKE '%$q%' OR S.last_name LIKE '%$q%' 
			            OR S.first_name_ar LIKE '%$q%' OR S.last_name_ar LIKE '%$q%' 
						OR( CONCAT(S.first_name,' ',S.last_name) LIKE '%$q%' 
					   	OR CONCAT(TRIM(S.first_name_ar),' ',TRIM(S.last_name_ar)) LIKE '%$q%' 
					   	OR CONCAT(TRIM(PS.parenting_title_en) LIKE '%$q%' 
					   	OR CONCAT(TRIM(PS.parenting_title_ar) LIKE '%$q%'
						) ";
		}
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
		
      }
	  
	  
	  
	 function getcntAssignRecords($tbl_parenting_school_id,$tbl_school_id)
	 {
		$qry  = "SELECT AR.tbl_student_id FROM ".TBL_ASSIGN_RECORDS."  AS AR WHERE 1  ";
		if($tbl_school_id<>""){
			$qry .= " AND AR.tbl_school_id='".$tbl_school_id."'";
		}
		if($tbl_parenting_school_id<>"")
			$qry .= " AND AR.tbl_parenting_school_id='$tbl_parenting_school_id' "; 
		
		$qry .= " AND AR.is_active<>'D' "; 
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
	 }
	  
	  
	  
	  
       /**
	* @desc		Assign Records 
	* @param	String $tbl_parenting_school_id,$tbl_parenting_school_cat_id,$title,$title_ar,$parenting_type,$parenting_text_en,$parenting_text_ar,
		   $parenting_url,$is_active,$tbl_school_id
	* @access	default
	* @return	none
	*/
	function assign_records_to_student($tbl_parenting_school_assign_id,$tbl_parenting_school_id,$tbl_student_id_t,$tbl_class_id_t,$tbl_school_id)
	{
			$tbl_parenting_school_assign_id      = $this->cf->get_data(trim($tbl_parenting_school_assign_id));
			$tbl_parenting_school_id             = $this->cf->get_data(trim($tbl_parenting_school_id));
			$tbl_student_id_t                    = $this->cf->get_data(trim($tbl_student_id_t));
			$tbl_class_id_t                      = $this->cf->get_data(trim($tbl_class_id_t));
			$tbl_school_id                       = $this->cf->get_data(trim($tbl_school_id));
	        
			$qry_msg = "SELECT tbl_parenting_school_id FROM ".TBL_ASSIGN_RECORDS." WHERE tbl_parenting_school_id='$tbl_parenting_school_id' AND  tbl_school_id='$tbl_school_id' "; 
			$qry_msg .= " AND tbl_student_id='$tbl_student_id_t' AND tbl_class_id='$tbl_class_id_t' AND  is_active<>'D' ";   
			$rs_msg  = $this->cf->selectMultiRecords($qry_msg);
			if(count($rs_msg)==0)
			{
				$qry = "INSERT INTO ".TBL_ASSIGN_RECORDS." ( `tbl_parenting_school_assign_id`, `tbl_parenting_school_id`,`tbl_student_id`,`tbl_class_id`, `assigned_date`, `tbl_school_id`)
						VALUES ('$tbl_parenting_school_assign_id','$tbl_parenting_school_id','$tbl_student_id_t','$tbl_class_id_t', NOW(), '$tbl_school_id') ";
				//echo $qry;
				$this->cf->insertInto($qry);
			}
    }
	
	/******************/
    function get_assigned_record_info($tbl_parenting_school_assign_id,$tbl_school_id)
	{
		$qry_msg = "SELECT * FROM ".TBL_ASSIGN_RECORDS." WHERE tbl_parenting_school_cat_id='$tbl_parenting_school_cat_id' AND  tbl_school_id='$tbl_school_id'"; 
		$rs_msg  = $this->cf->selectMultiRecords($qry_msg);
		return  $rs_msg;
	}
    /****************/
	
	
	/**
	* @desc		Activate School Assign Records
	* @access	default
	*/
	function activate_assign_record($tbl_parenting_school_assign_id,$tbl_school_id) {
		$tbl_parenting_school_assign_id = $this->cf->get_data(trim($tbl_parenting_school_assign_id));
		$qry = "UPDATE ".TBL_ASSIGN_RECORDS." SET is_active='Y' WHERE tbl_parenting_school_assign_id='$tbl_parenting_school_assign_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate Assign Records
	* @param	string $tbl_message_group_id
	* @access	default
	*/
	function deactivate_assign_record($tbl_parenting_school_assign_id,$tbl_school_id) {
		$tbl_parenting_school_assign_id = $this->cf->get_data(trim($tbl_parenting_school_assign_id));
		$qry = "UPDATE ".TBL_ASSIGN_RECORDS." SET is_active='N' WHERE tbl_parenting_school_assign_id='$tbl_parenting_school_assign_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete Assign Records
	* @param	string $tbl_message_group_id
	* @access	default
	*/
	function delete_assign_record($tbl_parenting_school_assign_id,$tbl_school_id) {
		$tbl_parenting_school_assign_id = $this->cf->get_data(trim($tbl_parenting_school_assign_id));
        $qry = "UPDATE ".TBL_ASSIGN_RECORDS." SET is_active='D' WHERE tbl_parenting_school_assign_id='$tbl_parenting_school_assign_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_PARENTING_SCHOOL." WHERE tbl_parenting_school_id='$tbl_parenting_school_id' AND  tbl_school_id='$tbl_school_id'";
		//$this->cf->deleteFrom($qry);
	}
	
	
	function get_school_records_list($tbl_school_id,$is_active) {
		$is_active  = $this->cf->get_data($is_active);
		$qry  = " SELECT PS.tbl_parenting_school_id, PS.parenting_title_en,PS.parenting_title_ar
		          FROM ".TBL_PARENTING_SCHOOL." AS PS 
		          WHERE 1  ";
		if($tbl_school_id<>""){
			$qry .= " AND PS.tbl_school_id='".$tbl_school_id."'";
		}
		if($is_active<>"")
			$qry .= " AND PS.is_active='".$is_active."' ";
		else
			$qry .= " AND PS.is_active='Y' "; 
		$qry .= " ORDER BY PS.id DESC";
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
      }
	
	
	//END ASSIGN SCHOOL RECORDS TO STUDENTS
	
	
	// GET SCHOOL PARENTING ( RECORDS) based on Student
	function get_child_parenting_datas($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$tbl_parenting_school_cat_id, $tbl_student_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry = "SELECT PS.*,PSC.title_en,PSC.title_ar FROM ".TBL_PARENTING_SCHOOL." AS PS LEFT JOIN ".TBL_PARENTING_SCHOOL_CAT." AS PSC ON 
		          PSC.tbl_parenting_school_cat_id= PS.tbl_parenting_school_cat_id 
				  WHERE 1 AND PS.tbl_school_id='".$tbl_school_id."'";
		if($tbl_student_id<>"")
		{
			$qry .= " AND PS.tbl_parenting_school_id IN  (SELECT tbl_parenting_school_id FROM ".TBL_ASSIGN_RECORDS." WHERE tbl_student_id='$tbl_student_id' AND tbl_school_id='".$tbl_school_id."' )";
		}
		if($tbl_school_id<>""){
			$qry .= " AND PS.tbl_school_id='".$tbl_school_id."'";
		}
		if($tbl_parenting_school_cat_id<>""){
			$qry .= " AND PS.tbl_parenting_school_cat_id='$tbl_parenting_school_cat_id' "; 
		}
		$qry .= " ORDER BY PS.id DESC";
		$qry .=" LIMIT $offset, ".TBL_PARENTING_SCHOOL_PAGING;
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;
      }

      function get_child_total_parenting_datas($q, $is_active, $tbl_school_id, $tbl_parenting_school_cat_id,$tbl_student_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
		
		$qry = "SELECT PS.tbl_parenting_school_id FROM ".TBL_PARENTING_SCHOOL." AS PS LEFT JOIN ".TBL_PARENTING_SCHOOL_CAT." AS PSC ON 
		          PSC.tbl_parenting_school_cat_id= PS.tbl_parenting_school_cat_id 
				  WHERE 1 AND PS.tbl_school_id='".$tbl_school_id."'";
		if($tbl_student_id<>"")
		{
			$qry .= " AND PS.tbl_parenting_school_id IN  (SELECT tbl_parenting_school_id FROM ".TBL_ASSIGN_RECORDS." WHERE tbl_student_id='$tbl_student_id' AND tbl_school_id='".$tbl_school_id."' )";
		}
		if($tbl_school_id<>""){
			$qry .= " AND PS.tbl_school_id='".$tbl_school_id."'";
		}
		if($tbl_parenting_school_cat_id<>""){
			$qry .= " AND PS.tbl_parenting_school_cat_id='$tbl_parenting_school_cat_id' "; 
		}
		$results = $this->cf->selectMultiRecords($qry);
	    return count($results);
		
      }
	  
	  
	
	
	

	


 }


?>





