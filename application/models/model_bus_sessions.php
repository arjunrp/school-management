<?php
include_once('include/common_functions.php');

/**
 * @desc   	  	Bus Sessions Model
 *
 * @category   	Model
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Model_bus_sessions extends CI_Model {
	var $cf;


	/**
	* @desc Default constructor for the Controller
	*
	* @access default
	*/
    function model_bus_sessions() {
		$this->cf = new Common_functions();
    }


	/**
	* @desc		Get bus sessions
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_bus_sessions($tbl_school_id) {
		$qry = "SELECT tbl_bus_sessions_id, title, title_ar, description, description_ar, start_time, end_time FROM ".TBL_BUS_SESSIONS." WHERE tbl_school_id='$tbl_school_id' AND is_active='Y'";
		//echo $qry;
		$data_rs = $this->cf->selectMultiRecords($qry);
		return $data_rs;
	}
	
	
	/**
	* @desc		Get bus sessions
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_bus_session_details($tbl_bus_sessions_id) {
		$qry = "SELECT * FROM ".TBL_BUS_SESSIONS." WHERE tbl_bus_sessions_id='$tbl_bus_sessions_id'";
		$data_rs = $this->cf->selectMultiRecords($qry);
		return $data_rs;
	}
	
}
?>

