<?
include_once('image.class.php');
/**
* @desc         Define common helper functions
* @category     Common Functions Model
* @author       Shanavas.PK
* @version      0.1
*/

class Common_functions extends CI_Model {
	   // User logged session function
		function loggedUser_user($sid){  
			 $sql4session = "SELECT session_id, user_id from ". TBL_SESSIONS ." where session_id='$sid'";
			 if (!$this->isExist($sql4session) || !isset($sid)){
				return false;
				 }
				 $contents = selectFrom($sql4session);   
			 return $contents["user_id"];
		}
		
		// session register function for current site
		function register_session_user($userID){
				$ip = $this->getIP();
			    $SessionID = md5(uniqid(rand()));  // To Creat a unique SessionID	
				$sql4sess = "INSERT INTO " . TBL_SESSIONS . "(session_id, user_id, session_date, ip) VALUES('$SessionID','$userID',NOW(), '$ip')";
				$sql4userSession = "SELECT user_id from " . TBL_SESSIONS . " where user_id='$userID'";
				$resUser = $this->selectMultiRecords($sql4userSession);
				if(count($resUser)>0)
				{
					$sql4DelUserSession = "DELETE FROM " . TBL_SESSIONS . " where user_id='$userID'";
			        $this->db->query($sql4DelUserSession);
				}	
				$this->insertInto($sql4sess);	
				return $SessionID;
		}

    /**
    * @desc     Performs INSERT operations in the DB
    * @param    string $query
    * @access   default
    */
	function insertInto($qry) {
        $this->db->query($qry);
    }

    /**
    * @desc     Performs UPDATE operations in the DB
    * @param    string $query
    * @access   default
    */
	function update($qry) {
        $this->db->query($qry);
    }


    /**
    * @desc     Performs DELETE operations in the DB
    * @param    string $query
    * @access   default
    */
	function deleteFrom($qry) {
        $this->db->query($qry);
    }

    /**
    * @desc     Performs SELECT operations in the DB
    * @param    string $query
    * @access   default
    * @return	array
    */

    function selectMultiRecords($qry) {
        $rs = $this->db->query($qry);
        $results = $rs->result_array();
	return $results;
    }
	
	function isExist($Querry_Sql) {	
	   $results = $this->selectMultiRecords($Querry_Sql);
	   if(count($results)>0)
	   {
		return true;	
		}else{
			return false;	
		}
	}

    /**
    * @desc     Returns the value of specified attribute of a given html/xml tag
    * @param    string $sttribute, string $tag_name
    * @access   default
    * @return	boolean
    */


    function getAttribute($attrib, $tag){
		$re = '/' . preg_quote($attrib) . '=([\'"])?((?(1).+?|[^\s>]+))(?(1)\1)/is';
		if (preg_match($re, $tag, $match)) {
			return urldecode($match[2]);
		}
	return false;
	}

   /**
    * @desc     Returns IP address
    * @param    none
    * @access   default
    * @return	string
	*/


	function getIP() {
		if (isset($HTTP_SERVER_VARS['HTTP_X_FORWARDED_FOR']) && eregi("^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}$",$HTTP_SERVER_VARS['HTTP_X_FORWARDED_FOR'])) {
			$ip = $HTTP_SERVER_VARS['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = getenv("REMOTE_ADDR");
		}
	return $ip;
	}

	/**
	* @desc		A function to format price
    * @param    float $price
    * @access   default
    * @return	float
	*/

	function price_format($price){
		$price = number_format($price, 2, '.', ',');
	return $price;
	}

	/**
	* @desc		A function copy file with new name
    * @param    source file, destination folder path, new file to copy, allow overwrite, fatal
    * @access   default
    * @return	string
	*/


	function file_copy($file_origin, $destination_directory, $file_destination, $overwrite, $fatal) {
		if ($fatal){
			$error_prefix = 'FATAL: File copy of \'' . $file_origin . '\' to \'' . $destination_directory . $file_destination . '\' failed.';
			$fp = @fopen($file_origin, "r");
			if (!$fp){
			echo $error_prefix . ' Originating file cannot be read or does not exist.';
			exit();
			}

			$dir_check = @is_writeable($destination_directory);
			if (!$dir_check){
			echo $error_prefix . ' Destination directory is not writeable or does not exist.';
			exit();
			}

			$dest_file_exists = file_exists($destination_directory . $file_destination);
			if ($dest_file_exists){
				if ($overwrite){
					$fp = @is_writeable($destination_directory . $file_destination);
					if (!$fp){
					  echo  $error_prefix . ' Destination file is not writeable [OVERWRITE].';
					  exit();
					}
				$copy_file = @copy($file_origin, $destination_directory . $file_destination); 
				}           
			}else{
				$copy_file = @copy($file_origin, $destination_directory . $file_destination);
			}
		}else{
				$copy_file = @copy($file_origin, $destination_directory . $file_destination);
		}
	}


	function generate_thumb($source, $destination) {
			$save_to_file = true;
			// Quality for JPEG and PNG.
			// 0 (worst quality, smaller file) to 100 (best quality, bigger file)
			// Note: PNG quality is only supported starting PHP 5.1.2
			$image_quality = 100;
			// resulting image type (1 = GIF, 2 = JPG, 3 = PNG)
			// enter code of the image type if you want override it
			// or set it to -1 to determine automatically
			$image_type = -1;
			// maximum thumb side size
			$max_x = 150;
			$max_y = 100;
			// cut image before resizing. Set to 0 to skip this.
			$cut_x = 0;
			$cut_y = 0;
			// Folder where source images are stored (thumbnails will be generated from these images).
			// MUST end with slash.
			//$images_folder = '/www/images/';
			// Folder to save thumbnails, full path from the root folder, MUST end with slash.
			// Only needed if you save generated thumbnails on the server.
			// Sample for windows:     c:/wwwroot/thumbs/
			// Sample for unix/linux:  /home/site.com/htdocs/thumbs/
			//$thumbs_folder = '/www/thumbs/';
			$save_to_file = 1;
			/*///////////////////////////////////////////////////
			/////////////// DO NOT EDIT BELOW
			///////////////////////////////////////////////////

			$to_name = '';
			if (isset($_REQUEST['f'])) {
			$save_to_file = intval($_REQUEST['f']) == 1;
			}
			if (isset($_REQUEST['src'])) {
			$from_name = urldecode($_REQUEST['src']);
			}
			else {
			die("Source file name must be specified.");
			}
			if (isset($_REQUEST['dest'])) {
			$to_name = urldecode($_REQUEST['dest']);
			}
			else if ($save_to_file) {
			die("Thumbnail file name must be specified.");
			}
			if (isset($_REQUEST['q'])) {
			$image_quality = intval($_REQUEST['q']);
			}
			if (isset($_REQUEST['t'])) {
			$image_type = intval($_REQUEST['t']);
			}
			if (isset($_REQUEST['x'])) {
			$max_x = intval($_REQUEST['x']);
			}
			if (isset($_REQUEST['y'])) {
			$max_y = intval($_REQUEST['y']);
			}
			if (!file_exists($images_folder)) die('Images folder does not exist (update $images_folder in the script)');
			if ($save_to_file && !file_exists($thumbs_folder)) die('Thumbnails folder does not exist (update $thumbs_folder in the script)');
			*/// Allocate all necessary memory for the image.
			// Special thanks to Alecos for providing the code.
			ini_set('memory_limit', '-1');
			// include image processing code
			////include('image.class.php');
			$img = new Zubrag_image;
			// initialize
			$img->max_x        = $max_x;
			$img->max_y        = $max_y;
			$img->cut_x        = $cut_x;
			$img->cut_y        = $cut_y;
			$img->quality      = $image_quality;
			$img->save_to_file = $save_to_file;
			$img->image_type   = $image_type;
			// generate thumbnail
			$img->GenerateThumbFile($source, $destination);
	}

	/**
	* @desc		A function to get last day of current month
    * @param    none
    * @access   default
    * @return	int
	*/
	function get_last_day(){
		$last_day = date('d',strtotime('-1 second',strtotime('+1 month',strtotime(date('m').'/01/'.date('Y').' 00:00:00'))));
	return $last_day;
	}	

	/**
	* @desc		A function to generate random numbers
    * @param    none
    * @access   default
    * @return	string
	*/
	function getMD5_Id(){
		$rnd = substr(md5(uniqid()), 0,10);
	return $rnd;
	}


	/**
	* @desc		General function
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_data($str) {
		return $str;
	}

	function send_notification($deviceToken, $message, $device_type) {
		if ($device_type == "iPhone") {
			// Put your device token here (without spaces):
			//$deviceToken = '4c33a17dabf60616e8ef519c02037ad450cb492abde2ecb569b8ab6cc18b8ee8';
			//echo $deviceToken."<br>".$message;
			//return;
			// Put your private key's passphrase here:
			$message = urldecode($message);
			$passphrase = 'google';
			// Put your alert message here:
			//$message = 'Good behaviour buddy.';
			//echo  $_SERVER['DOCUMENT_ROOT'].'/aqdar/ck.pem';
			//exit();
			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert',  $_SERVER['DOCUMENT_ROOT'].'/aqdar/ck.pem');
			stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
			stream_context_set_option($ctx, 'ssl', 'cafile', 'entrust_2048_ca.cer');
			// Open a connection to the APNS server
			$fp = stream_socket_client(
			'ssl://gateway.push.apple.com:2195', $err,
			$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
			if (!$fp) {
			//exit("Failed to connect: $err $errstr" . PHP_EOL);
			}
			//echo 'Connected to APNS' . PHP_EOL;
			// Create the payload body
			$body['aps'] = array(
			'alert' => $message,
			'sound' => 'default'
			);
			// Encode the payload as JSON
			$payload = json_encode($body);
			// Build the binary notification
			$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
			// Send it to the server
			$result = fwrite($fp, $msg, strlen($msg));
			/*if (!$result)
			echo 'Message not delivered' . PHP_EOL;
			else
			echo 'Message successfully delivered' . PHP_EOL;
			*/
			// Close the connection to the server
			fclose($fp);
		}else{
			//echo $deviceToken."<br>".$message;
			//$url = 'https://android.googleapis.com/gcm/send';
			//$serverApiKey = "AIzaSyBCm4p-WoMq2L29cxUjRlmYGnfsW9YLZlc"; //api key
			//$devices = array($deviceToken); //array of tokens
			//$message = "The message to send"; //message o send
			//$gcpm = new GCMPushMessage($apiKey);
			//$gcpm->setDevices($devices);
			//$response = $gcpm->send($message, array('title' => 'Test title')); //title of the message
			//$fields = array(
			//	'registration_ids'  => $devices,
			//	'data'              => array( "message" => $message ),
			//);
			//if(is_array($data)){
			//	foreach ($data as $key => $value) {
			//		$fields['data'][$key] = $value;
				//}
		//	}
			//$headers = array(
			//	'Authorization: key=' . $serverApiKey,
			//	'Content-Type: application/json'
			//);
			//$ch = curl_init();
			//curl_setopt( $ch, CURLOPT_URL, $url );
			//curl_setopt( $ch, CURLOPT_POST, true );
			//curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
			//curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			//curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
			//curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false);
			//curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
			//$result = curl_exec($ch);
			//curl_close($ch);
			//return $result;
				        // Set POST variables
			$url = 'https://android.googleapis.com/gcm/send';
	        $devices = array($deviceToken);
			$fields = array(
				'registration_ids' => $devices,
				'data' =>  array( "message" => $message ),
			);
	
	        if(is_array($data)){
				foreach ($data as $key => $value) {
					$fields['data'][$key] = $value;
				}
			}
			
			$headers = array(
				'Authorization: key=' . "AIzaSyB7t4Mwc4ouCzk6GDWHfoedv7U7KxXaE9w",
				'Content-Type: application/json'
			);
	        //print_r($headers);
			// Open connection
			$ch = curl_init();
	
			// Set the url, number of POST vars, POST data
			curl_setopt($ch, CURLOPT_URL, $url);
	
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	
			// Disabling SSL Certificate support temporarly
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
			curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
	
			// Execute post
			$result = curl_exec($ch);
			/*if ($result === FALSE) {
				die('Curl failed: ' . curl_error($ch));
			}*/
	
			// Close connection
			curl_close($ch);
		//	echo $result;
			
		}
	}
}


?>