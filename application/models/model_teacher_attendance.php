<?php
include_once('include/common_functions.php');

/**
 * @desc   	  	Teacher Attendance Model
 *
 * @category   	Model
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Model_teacher_attendance extends CI_Model {
	var $cf;


	/**
	* @desc Default constructor for the Controller
	*
	* @access default
	*/
    function model_teacher_attendance() {
		$this->cf = new Common_functions();
    }



	/**
	* @desc		Get attendances against class
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_all_attendances_against_class_t($tbl_class_id) {
		$tbl_class_id = $this->cf->get_data(trim($tbl_class_id));
		$qry = "SELECT * FROM ".TBL_STUDENT." WHERE tbl_class_id='$tbl_class_id' AND is_active='Y' ORDER BY first_name ASC ";
		//echo $qry."<br />";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;	
	}
	
	
	/**
	* @desc		Get attendances against class
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_attendance_t($tbl_student_id, $tbl_teacher_id, $tbl_class_id, $attendance_date,  $att_option){
		$qry = "SELECT is_present FROM ".TBL_TEACHER_ATTENDANCE." WHERE tbl_student_id='$tbl_student_id' AND tbl_teacher_id='$tbl_teacher_id' AND tbl_class_id='$tbl_class_id' AND attendance_date='$attendance_date' AND is_active='Y'";
		//echo $qry."<br />";
		//exit();
		$rs = $this->cf->selectMultiRecords($qry);
		//if (trim($rs[0]["is_present"]) == "") {return "N";}
		return $rs[0]["is_present"];	
	}
	
	
	/**
	* @desc		Save attendance
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function save_attendance_t($tbl_student_id, $tbl_teacher_id, $attendance_date, $is_present, $tbl_class_id, $tbl_school_id, $tbl_class_sessions_id, $tbl_semester_id) {
		$tbl_teacher_attendance_id = substr(md5(uniqid(rand())),0,10);
		$qry = "INSERT INTO ".TBL_TEACHER_ATTENDANCE." (
			`tbl_teacher_attendance_id` ,
			`tbl_class_id` ,
			`tbl_student_id` ,
			`tbl_teacher_id` ,
			`tbl_class_sessions_id` ,
			`attendance_date` ,
			`is_present` ,
			`is_active` ,
			`added_date` ,
			`tbl_school_id`,
			`tbl_semester_id`
			)
			VALUES (
			'$tbl_teacher_attendance_id', '$tbl_class_id', '$tbl_student_id', '$tbl_teacher_id', '$tbl_class_sessions_id', '$attendance_date', '$is_present', 'Y', NOW(), '$tbl_school_id','$tbl_semester_id'
			)";
		
		//echo $qry."<br />";
		$this->cf->insertInto($qry);
	}
	
	
	/**
	* @desc		Delate old attendance data if any, for a given bus on particular date for either morning or afternoon session.
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function delete_attendance_t($tbl_teacher_id, $attendance_date, $tbl_class_id, $tbl_class_sessions_id) {
		$qry = "DELETE FROM ".TBL_TEACHER_ATTENDANCE." WHERE tbl_teacher_id='".$tbl_teacher_id."' AND attendance_date='".$attendance_date."' AND tbl_class_id='".$tbl_class_id."' AND tbl_class_sessions_id='".$tbl_class_sessions_id."'";
		
		//echo $qry."<br />";
		$this->cf->deleteFrom($qry);
	}
	
	
	/**
	* @desc		Get attendances details 
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_attendance_details_t($attendance_date, $att_option, $tbl_teacher_id_sel) {
		$qry = "SELECT tbl_student_id FROM ".TBL_STUDENT." WHERE is_active='Y' AND tbl_class_id IN (SELECT tbl_class_id FROM ".TBL_TEACHER_CLASS." WHERE tbl_teacher_id='".$tbl_teacher_id_sel."')";
		$rs = $this->cf->selectMultiRecords($qry);
		for ($i=0; $i<count($rs); $i++) {
			$str = $str."'".$rs[$i]["tbl_student_id"]."',";
		}
		if ($str != "") {
			$str = substr($str, 0, -1);
		} else {
			return $rs;
		}
		
		$qry = "SELECT * FROM ".TBL_TEACHER_ATTENDANCE." WHERE attendance_date='".$attendance_date."' AND att_option='".$att_option."' AND is_active='Y' AND tbl_student_id IN (".$str.")";
		//echo $qry."<br />";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
	
	
	/**
	* @desc		Get child attendance listing
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_child_bus_attendance_t($tbl_student_id_sel) {
		$qry = "SELECT * FROM ".TBL_TEACHER_ATTENDANCE." WHERE tbl_student_id='".$tbl_student_id_sel."' AND is_active='Y' ORDER BY attendance_date DESC LIMIT 0, ".PAGING_BUS_ATTENDANCE;
		//echo $qry."<br />";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
	
	
	/**
	* @desc		Function to check if attendance already is taken for a particular class session
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function is_attendance_exists($tbl_class_id, $tbl_class_sessions_id, $attendance_date, $tbl_school_id) {
		$qry = "SELECT COUNT(*) AS cnt FROM ".TBL_TEACHER_ATTENDANCE." WHERE tbl_class_id='".$tbl_class_id."' AND attendance_date='".$attendance_date."' AND tbl_class_sessions_id='".$tbl_class_sessions_id."' AND tbl_school_id='".$tbl_school_id."'";
		//echo $qry."<br />";
		$rs = $this->cf->selectMultiRecords($qry);
		if ($rs[0]["cnt"]<=0) {
			return "N";
		} else {
			return "Y";
		}
	}
	
	
	/**
	* @desc		Get student attendance between dates
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_attendance_between_dates($tbl_student_id, $start_date, $end_date) {
		$qry = "SELECT * FROM ".TBL_TEACHER_ATTENDANCE." WHERE tbl_student_id='".$tbl_student_id."' AND is_active='Y' AND attendance_date BETWEEN '".$start_date."' AND '".$end_date."' ORDER BY attendance_date DESC";
		//echo $qry."<br />";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
	
}
?>

