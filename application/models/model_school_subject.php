<?php
include_once('include/common_functions.php');
/**
 * @desc   	  	School Subject Model
 * @category   	Model
 * @author     	Shanavas.PK
 * @version    	0.1
 */

class Model_school_subject extends CI_Model {
	var $cf;
	
	/**
	* @desc Default constructor for the Controller
	* @access default
	*/
    function model_school_subject() {
		$this->cf = new Common_functions();
    }

	/**
	* @desc		Get subjects details
	* @param	none 	
	* @access	default
	* @return	array $rs
	*/
	function get_school_subject_obj($tbl_subject_id, $tbl_school_id) {
		$qry_sel = "SELECT * FROM ".TBL_SUBJECT." WHERE tbl_subject_id='$tbl_subject_id' AND tbl_school_id='$tbl_school_id'";
		$rs = $this->cf->selectMultiRecords($qry_sel);
	return $rs;	
	}
	
	//BACKEND FUNCTIONALITY
	
	/**
	* @desc		Get all school subjects
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function get_all_school_subjects($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = $this->cf->get_data($q);
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		$qry = "SELECT * FROM ".TBL_SUBJECT."  WHERE 1 ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND is_active='$is_active' ";
		}
		$qry .= " AND is_active<>'D' "; 
		//Search
		if (trim($q) != "") {
			$qry .= " AND (subject LIKE '%$q%' || subject_ar LIKE '%$q%')";
		} 
		
		$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY subject ASC ";
		}
		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_SUBJECT_PAGING;
		}
	
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}

	
	/**
	* @desc		Get total subjects
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_school_subjects($q, $is_active, $tbl_school_id) {
		$q         = $this->cf->get_data($q);
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT COUNT(*) as total_subjects FROM ".TBL_SUBJECT." WHERE 1 ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND is_active='$is_active' ";
		}
		$qry .= " AND is_active<>'D' "; 
		
		$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		//Search
		if (trim($q) != "") {
			$qry .= " AND (subject LIKE '%$q%' || subject_ar LIKE '%$q%')";
		} 
		//echo $qry;
		$results = $this->cf->selectMultiRecords($qry);
		
	return $results[0]['total_subjects'];
	}
	
	
	function is_exist_subject($tbl_subject_id, $subject, $subject_ar, $tbl_school_id) {
		
		$tbl_subject_id  	       = $this->cf->get_data($tbl_subject_id);
		$subject 		           = $this->cf->get_data($subject);
		$subject_ar                = $this->cf->get_data($subject_ar);

		$qry = "SELECT * FROM ".TBL_SUBJECT." WHERE 1 ";
		if (trim($subject) != "") {
			 $qry .= " AND subject='$subject' ";
		}
		
		if (trim($tbl_subject_id) != "") {
			$qry .= " AND tbl_subject_id <> '$tbl_subject_id'";
		}
	    $qry .= " AND is_active<>'D' "; 
		$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}
	
	/**
	* @desc		Create school subject
	* @param	String params
	* @access	default
	*/
	function create_school_subject($tbl_subject_id, $subject, $subject_ar, $tbl_school_id) {
		$tbl_subject_id 		= $this->cf->get_data($tbl_subject_id);
		$subject                = $this->cf->get_data($subject);
		$subject_ar             = $this->cf->get_data($subject_ar);
		if($tbl_school_id<>"")
		{
			$qry_ins = "INSERT INTO ".TBL_SUBJECT." (`tbl_subject_id`, `subject`, `subject_ar`, `added_date`, `is_active`, `tbl_school_id`)
						VALUES ('$tbl_subject_id', '$subject', '$subject_ar', NOW(), 'Y', '$tbl_school_id')";
			$this->cf->insertInto($qry_ins);
			return "Y";
		}else{
		   return "N";	
			
		}
	}
	
	/**
	* @desc		Activate
	* @param	string 
	* @access	default
	*/
	function activate_subject($school_subjects_id_enc,$tbl_school_id) {
		$tbl_subject_id = $this->cf->get_data(trim($school_subjects_id_enc));
		$qry = "UPDATE ".TBL_SUBJECT." SET is_active='Y' WHERE tbl_subject_id='$tbl_subject_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate
	* @param	string $school_subjects_id_enc
	* @access	default
	*/
	function deactivate_subject($school_subjects_id_enc,$tbl_school_id) {
		$tbl_subject_id = $this->cf->get_data(trim($school_subjects_id_enc));
		$qry = "UPDATE ".TBL_SUBJECT." SET is_active='N' WHERE tbl_subject_id='$tbl_subject_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete
	* @param	string $tbl_school_type_id
	* @access	default
	*/
	function delete_subject($school_subjects_id_enc,$tbl_school_id) {
		$tbl_subject_id = $this->cf->get_data(trim($school_subjects_id_enc));
        $qry = "UPDATE ".TBL_SUBJECT." SET is_active='D' WHERE tbl_subject_id='$tbl_subject_id' AND  tbl_school_id='$tbl_school_id'";
		echo $qry;
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_CATEGORY." WHERE tbl_category_id='$tbl_category_id' ";
		//$this->cf->deleteFrom($qry);
	}
	

	/**
	* @desc		Save changes
	* @param	String params
	* @access	default
	*/
	function save_subject_changes($tbl_subject_id, $subject, $subject_ar, $tbl_school_id) {
		$tbl_subject_id        = $this->cf->get_data($tbl_subject_id);
		$subject                       = $this->cf->get_data($subject);
		$subject_ar                    = $this->cf->get_data($subject_ar);
		$tbl_school_id              = $this->cf->get_data($tbl_school_id);

		$qry_update = "UPDATE ".TBL_SUBJECT." SET subject='$subject', subject_ar='$subject_ar' "; 
		$qry_update .= " WHERE tbl_subject_id='$tbl_subject_id' AND  tbl_school_id='$tbl_school_id'  " ;
		$this->cf->update($qry_update);
	}
	
	//end backend functionality
	
	
	

}

?>



