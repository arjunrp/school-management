<?php
include_once('include/common_functions.php');

/**
 * @desc   	  	Contacts Model
 *
 * @category   	Model
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Model_contacts extends CI_Model {
	var $cf;


	/**
	* @desc Default constructor for the Controller
	*
	* @access default
	*/
    function model_contacts() {
		$this->cf = new Common_functions();
    }


	/**
	* @desc		Get class name
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_school_contacts($lan, $tbl_school_id) {
		
		$qry_sel = "SELECT * FROM ".TBL_SCHOOL_CONTACTS." WHERE tbl_school_id='".$tbl_school_id."'";
		//echo $qry_sel."<br />";
		$rs = $this->cf->selectMultiRecords($qry_sel);
		return $rs;
	}
	

	/**
	* @desc		Function to save contact details
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function save_contact_details($lan, $device, $tbl_parent_id, $contact_us_name, $contact_us_email, $contact_us_comments, $tbl_school_id) {

		$contact_us_name = $this->cf->get_data(trim($contact_us_name));
		$contact_us_email = $this->cf->get_data(trim($contact_us_email));
		$contact_us_comments = $this->cf->get_data(trim($contact_us_comments));
		
		$tbl_contact_us_id = substr(md5(uniqid(rand())),0,10);
		
		$qry = "INSERT INTO ".TBL_CONTACT_US." (
			`tbl_contact_us_id` ,
			`lan` ,
			`device` ,
			`tbl_parent_id` ,
			`contact_us_name` ,
			`contact_us_email` ,
			`contact_us_comments` ,
			`is_active` ,
			`added_date` ,
			`tbl_school_id`
			)
			VALUES (
			'$tbl_contact_us_id', '$lan', '$device', '$tbl_parent_id', '$contact_us_name', '$contact_us_email', '$contact_us_comments', 'Y', NOW(), '$tbl_school_id')";
		//echo $qry;
		$this->cf->insertInto($qry);
	}
    }
?>





