<?php
include_once('include/common_functions.php');

/**
 * @desc   	  	Bus Sessions Model
 *
 * @category   	Model
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Model_bus_announcement extends CI_Model {
	var $cf;


	/**
	* @desc Default constructor for the Controller
	*
	* @access default
	*/
    function model_bus_announcement() {
		$this->cf = new Common_functions();
    }


	/**
	* @desc		Save bus announcement
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function save_bus_announcement($tbl_bus_id, $tbl_student_id, $tbl_school_id) {
		$tbl_bus_announcement_id = substr(md5(uniqid(rand())),0,10);
		$qry = "INSERT INTO ".TBL_BUS_ANNOUNCEMENT." (
			`tbl_bus_announcement_id` ,
			`tbl_bus_id` ,
			`tbl_student_id` ,
			`is_announced` ,
			`added_date` ,
			`tbl_school_id`
			)
			VALUES (
			'$tbl_bus_announcement_id', '$tbl_bus_id', '$tbl_student_id', 'N', NOW(), '$tbl_school_id'
			)";
		$this->cf->insertInto($qry);
	}
	
	/**
	* @desc		Save bus announcement
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_bus_announcement($tbl_school_id) {
		$qry = "SELECT * FROM ".TBL_BUS_ANNOUNCEMENT." WHERE tbl_school_id='".$tbl_school_id."' AND is_announced='N' ORDER BY added_date ASC LIMIT 0,1" ;
		$data_rs = $this->cf->selectMultiRecords($qry);
		$id = $data_rs[0]["id"];
		
		if ($id != "") {
			$qry = "UPDATE ".TBL_BUS_ANNOUNCEMENT." SET is_announced='Y' WHERE id='".$id."'" ;
			$this->cf->update($qry);
		}
		return $data_rs;
	}
}
?>

