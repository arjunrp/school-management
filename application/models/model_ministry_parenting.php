<?php
include_once('include/common_functions.php');

/**
 * @desc   	  	Ministry Parenting Model
 *
 * @category   	Model
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Model_ministry_parenting extends CI_Model {
	var $cf;

	/**
	* @desc Default constructor for the Controller
	*
	* @access default
	*/
    function model_ministry_parenting() {
		$this->cf = new Common_functions();
    }


	/**
	* @desc		Activate
	* 
	* @param	string $tbl_parenting_id
	* @access	default
	*/
	function activate($tbl_parenting_id) {
		$tbl_parenting_id = mysql_real_escape_string(trim($tbl_parenting_id));
		
		$qry = "UPDATE ".TBL_PARENTING." SET is_active='Y' WHERE tbl_parenting_id='$tbl_parenting_id' ";
		//echo $qry;
		
		$this->cf->update($qry);
	}


	/**
	* @desc		De-Activate
	* 
	* @param	string $tbl_parenting_id
	* @access	default
	*/
	function deactivate($tbl_parenting_id) {
		$tbl_parenting_id = mysql_real_escape_string(trim($tbl_parenting_id));
		
		$qry = "UPDATE ".TBL_PARENTING." SET is_active='N' WHERE tbl_parenting_id='$tbl_parenting_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}


	/**
	* @desc		Delete
	* 
	* @param	string $tbl_parenting_id
	* @access	default
	*/
	function delete($tbl_parenting_id) {
		$tbl_parenting_id = mysql_real_escape_string(trim($tbl_parenting_id));

		$qry = "DELETE FROM ".TBL_PARENTING." WHERE tbl_parenting_id='$tbl_parenting_id' ";
		//echo $qry;
		$this->cf->deleteFrom($qry);
	}
	

	/**
	* @desc		Get a country object based id
	* 
	* @param	string $tbl_parenting_id
	* @access	default
	*/
	function get_ministry_parenting_obj($tbl_parenting_id) {
		$email = mysql_real_escape_string($email);
	
		$qry = "SELECT * FROM ".TBL_PARENTING." WHERE 1 AND tbl_parenting_id='$tbl_parenting_id'";

		$results = $this->cf->selectMultiRecords($qry);
		
		$ministry_parenting_obj = array();
		$ministry_parenting_obj['tbl_parenting_id'] = $results[0]['tbl_parenting_id'];
		$ministry_parenting_obj['tbl_parenting_cat_id'] = $results[0]['tbl_parenting_cat_id'];
		$ministry_parenting_obj['parenting_title_en'] = $results[0]['parenting_title_en'];
		$ministry_parenting_obj['parenting_title_ar'] = $results[0]['parenting_title_ar'];
		$ministry_parenting_obj['parenting_type'] = $results[0]['parenting_type'];
		$ministry_parenting_obj['parenting_logo'] = $results[0]['parenting_logo'];
		$ministry_parenting_obj['parenting_text_en'] = $results[0]['parenting_text_en'];
		$ministry_parenting_obj['parenting_text_ar'] = $results[0]['parenting_text_ar'];
		$ministry_parenting_obj['added_date'] = $results[0]['added_date'];
		$ministry_parenting_obj['is_active'] = $results[0]['is_active'];
		
	return $ministry_parenting_obj;
	}

	
	/**
	* @desc		Get all ministry parenting categorys
	* 
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function get_all_ministry_parentings($sort_name, $sort_by, $offset, $q, $is_active, $parenting_cat_id_enc='') {
		$sort_name = mysql_real_escape_string($sort_name);
		$sort_by = mysql_real_escape_string($sort_by);
		$offset = mysql_real_escape_string($offset);
		$q = mysql_real_escape_string($q);
		$is_active = mysql_real_escape_string($is_active);
	
		$qry = "SELECT * FROM ".TBL_PARENTING." WHERE 1 ";
		
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND is_active='$is_active' ";
		}
		 
		//Search
		if (trim($parenting_cat_id_enc) != "") {
			$qry .= " AND tbl_parenting_cat_id = '$parenting_cat_id_enc' ";
		} 
		
			if (trim($q) != "") {
			$qry .= " AND (parenting_title_en LIKE '%$q%' || parenting_title_ar LIKE '%$q%')";
		} 

		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY first_name ASC";
		}

		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_PARENTING_PAGING;
		}
		//echo $qry;
		
		$results = $this->cf->selectMultiRecords($qry);
		
	return $results;
	}

	
	/**
	* @desc		Get total ministry parenting categorys
	* 
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_ministry_parentings($q, $is_active, $parenting_cat_id_enc='') {
		$q = mysql_real_escape_string($q);
		$is_active = mysql_real_escape_string($is_active);
	
		$qry = "SELECT COUNT(*) as total_ministry_parentings FROM ".TBL_PARENTING." WHERE 1 ";
		
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND is_active='$is_active' ";
		}
		 
		//Search
		if (trim($q) != "") {
			$qry .= " AND (parenting_title_en LIKE '%$q%' || parenting_title_ar LIKE '%$q%')";
		} 

		//echo $qry;
		
		$results = $this->cf->selectMultiRecords($qry);
		
	return $results[0]['total_ministry_parentings'];
	}

	
	/**
	* @desc		Check if parenting already exists
	* 
	* @param	String params
	* @access	default
	*/
	function is_exist($tbl_parenting_id, $parenting_title_en, $parenting_title_ar) {
		$tbl_parenting_id = mysql_real_escape_string($tbl_parenting_id);
		$parenting_title_en = mysql_real_escape_string($parenting_title_en);
		$parenting_title_ar = mysql_real_escape_string($parenting_title_ar);

		$qry = "SELECT * FROM ".TBL_PARENTING." WHERE 1 ";
		if (trim($parenting_title_en) != "") {
			$qry .= " AND parenting_title_en='$parenting_title_en'";
		}
		if (trim($tbl_parenting_id) != "") {
			$qry .= " AND tbl_parenting_id <> '$tbl_parenting_id'";
		}
		
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}

	
	/**
	* @desc		Create parenting
	* 
	* @param	String params
	* @access	default
	*/
	function create_ministry_parenting($tbl_parenting_id, $tbl_parenting_cat_id, $parenting_title_en, $parenting_title_ar, $parenting_type, $parenting_logo, $parenting_text_en, $parenting_text_ar) {
		$tbl_parenting_id = mysql_real_escape_string($tbl_parenting_id);
		$tbl_parenting_cat_id = mysql_real_escape_string($tbl_parenting_cat_id);
		$parenting_title_en = mysql_real_escape_string($parenting_title_en);
		$parenting_title_ar = mysql_real_escape_string($parenting_title_ar);
		$parenting_type = mysql_real_escape_string($parenting_type);
		$parenting_logo = mysql_real_escape_string($parenting_logo);
		$parenting_text_en = mysql_real_escape_string($parenting_text_en);
		$parenting_text_ar = mysql_real_escape_string($parenting_text_ar);

		$qry_ins = "INSERT INTO ".TBL_PARENTING." (`tbl_parenting_id`, `tbl_parenting_cat_id`, `parenting_title_en`, `parenting_title_ar`, `parenting_type`, `parenting_logo`, `parenting_text_en`, `parenting_text_ar`, `added_date`, `is_active`)
					VALUES ('$tbl_parenting_id', '$tbl_parenting_cat_id', '$parenting_title_en', '$parenting_title_ar', '$parenting_type', '$parenting_logo', '$parenting_text_en', '$parenting_text_ar', NOW(), 'Y')";
		//echo $qry_ins."<br>";
		$this->cf->insertInto($qry_ins);
	}

	
	/**
	* @desc		Save changes
	* 
	* @param	String params
	* @access	default
	*/
	function save_changes($tbl_parenting_id, $tbl_parenting_cat_id, $parenting_title_en, $parenting_title_ar, $parenting_type, $parenting_logo, $parenting_text_en, $parenting_text_ar) {
		$tbl_parenting_id = mysql_real_escape_string($tbl_parenting_id);
		$parenting_title_en = mysql_real_escape_string($parenting_title_en);
		$parenting_title_ar = mysql_real_escape_string($parenting_title_ar);

		$parenting_type = mysql_real_escape_string($parenting_type);
		$parenting_logo = mysql_real_escape_string($parenting_logo);
		$parenting_text_en = mysql_real_escape_string($parenting_text_en);
		$parenting_text_ar = mysql_real_escape_string($parenting_text_ar);

		$qry_update = "UPDATE ".TBL_PARENTING." SET parenting_title_en='$parenting_title_en', parenting_title_ar='$parenting_title_ar', parenting_type='$parenting_type', parenting_text_en='$parenting_text_en', parenting_text_ar='$parenting_text_ar' "; 
		
		if (trim($parenting_logo) != "") {
			$qry_update .=  " , parenting_logo='$parenting_logo' ";
		}


		$qry_update .= " WHERE tbl_parenting_id='$tbl_parenting_id' " ;

		//echo $qry_update."<br>";
		
		$this->cf->update($qry_update);
	}

	
	/**
	* @desc		Delete image
	* 
	* @param	String params
	* @access	default
	*/
	function delete_image($tbl_parenting_id) {
		$tbl_parenting_id = mysql_real_escape_string($tbl_parenting_id);
		$qry_update = "UPDATE ".TBL_PARENTING." SET parenting_logo='' WHERE tbl_parenting_id='$tbl_parenting_id' "; 

		echo $qry_update."<br>";
		$this->cf->update($qry_update);
	}

}
?>