<?php
include_once('include/common_functions.php');


/**
* @desc   	  	Student Model
*
* @category   	Model
* @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
* @version    	0.1
*/

class Model_student extends CI_Model {
	var $cf;


	/**
	* @desc Default constructor for the Controller
	*
	* @access default
	*/
	function model_student() {
		$this->cf = new Common_functions();
	}



	/**
	* @desc		Get students against class
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_all_students_against_class($tbl_class_id) {
		$tbl_class_id = $this->cf->get_data(trim($tbl_class_id));
		$qry_sel = "SELECT * FROM ".TBL_STUDENT." WHERE tbl_class_id='$tbl_class_id' AND is_active='Y' ORDER BY first_name ASC ";
		//echo $qry_sel."<br />";
		$rs = $this->cf->selectMultiRecords($qry_sel);
		return $rs;	
	}


	/**
	* @desc		Save points
	* 
	* @param	String $tbl_teacher_id, $tbl_class_id, $tbl_student_id, $points_student, $comments_student
	* @access	default
	* @return	none
	*/
	function save_points($tbl_teacher_id, $tbl_class_id, $tbl_student_id, $points_student, $comments_student, $tbl_school_id) {
		$tbl_teacher_id = $this->cf->get_data(trim($tbl_teacher_id));
		$tbl_class_id = $this->cf->get_data(trim($tbl_class_id));
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		$points_student = $this->cf->get_data(trim($points_student));
		$comments_student = $this->cf->get_data(trim($comments_student));
		
		$tbl_points_id = substr(md5(uniqid(rand())),0,10);
		
		$qry = "INSERT INTO ".TBL_POINTS." (`tbl_points_id`, `tbl_teacher_id`, `tbl_class_id`, `tbl_student_id`, `points_student`, `comments_student`, `added_date`, `tbl_school_id`)
				VALUES ('$tbl_points_id', '$tbl_teacher_id', '$tbl_class_id', '$tbl_student_id', '$points_student', '$comments_student', NOW(), '$tbl_school_id') ";
		//echo $qry;
		$this->cf->insertInto($qry);
	}


	/**
	* @desc		Issue card
	* 
	* @param	String $tbl_teacher_id, $tbl_class_id, $tbl_student_id, $card_type
	* @access	default
	* @return	none
	*/
	function issue_card($tbl_teacher_id, $tbl_class_id, $tbl_student_id, $card_type, $comments_student, $card_issue_type, $tbl_school_id, $tbl_semester_id) {
		$tbl_teacher_id = $this->cf->get_data(trim($tbl_teacher_id));
		$tbl_class_id = $this->cf->get_data(trim($tbl_class_id));
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		$comments_student = $this->cf->get_data(trim($comments_student));
		$card_type = $this->cf->get_data(trim($card_type));
		$tbl_card_id = substr(md5(uniqid(rand())),0,10);
		
		$qry = "INSERT INTO ".TBL_CARDS." (`tbl_card_id`, `tbl_teacher_id`, `tbl_class_id`, `tbl_student_id`, `card_type`, `card_issue_type`,`comments_student`, `added_date`, `tbl_school_id`, `tbl_semester_id`)
				VALUES ('$tbl_card_id', '$tbl_teacher_id', '$tbl_class_id', '$tbl_student_id', '$card_type', '$card_issue_type', '$comments_student', NOW(), '$tbl_school_id', '$tbl_semester_id') ";
		$this->cf->insertInto($qry);
	}
	function cancel_card($tbl_card_id) {		
		$qry = "DELETE FROM ".TBL_CARDS." WHERE tbl_CARD_id='".$tbl_card_id."'";
		$this->cf->deleteFrom($qry);
	}


	/**
	* @desc		Register absent
	* 
	* @param	String $tbl_teacher_id, $tbl_class_id, $tbl_student_id 
	* @access	default
	* @return	none
	*/
	function register_absent($tbl_teacher_id, $tbl_class_id, $tbl_student_id) {
		$tbl_teacher_id = $this->cf->get_data(trim($tbl_teacher_id));
		$tbl_class_id = $this->cf->get_data(trim($tbl_class_id));
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		
		$tbl_absent_id = substr(md5(uniqid(rand())),0,10);
		
		$qry = "INSERT INTO ".TBL_ABSENT." (`tbl_absent_id`, `tbl_teacher_id`, `tbl_class_id`, `tbl_student_id`, `added_date`)
				VALUES ('$tbl_absent_id', '$tbl_teacher_id', '$tbl_class_id', '$tbl_student_id', NOW() ) ";
		//echo $qry;
		$this->cf->insertInto($qry);
	}


	/**
	* @desc		Get total points
	* 
	* @param	String $tbl_student_id 
	* @access	default
	* @return	none
	*/
	function get_total_points($tbl_student_id) {
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		
		$total_points = 0;
			
		$qry = "SELECT SUM(points_student) AS total_points FROM ".TBL_POINTS." WHERE tbl_student_id='$tbl_student_id' AND is_active='Y' ";
		//echo $qry;
		$rs = $this->cf->SelectMultiRecords($qry);
		
		if ($rs[0]['total_points'] == "NULL" || !is_numeric($rs[0]['total_points'])) {
			$total_points = 0;
		} else {
			$total_points = $rs[0]['total_points'];
		}
		return $total_points;
	}

	
	/**
	* @desc		Get cards
	* 
	* @param	String $tbl_student_id 
	* @access	default
	* @return	none
	*/
	function get_cards($tbl_student_id) {
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		
		//$qry = "SELECT COUNT(card_type) as total_cards, card_type FROM ".TBL_CARDS." WHERE tbl_student_id='$tbl_student_id' AND is_active='Y' GROUP BY card_type ";
		
		$qry = "SELECT count( * ) AS cnt , card_type, card_issue_type
			FROM ".TBL_CARDS." WHERE tbl_student_id='$tbl_student_id'
			GROUP BY `card_type` , `card_issue_type`
			ORDER BY card_type";
		//echo $qry;
		$rs = $this->cf->SelectMultiRecords($qry);
		return $rs;	
	}


	function get_negative_cards($tbl_student_id,$tbl_class_id,$tbl_teacher_id) {
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		//$qry = "SELECT COUNT(card_type) as total_cards, card_type FROM ".TBL_CARDS." WHERE tbl_student_id='$tbl_student_id' AND is_active='Y' GROUP BY card_type ";
		$qry = "SELECT count( * ) AS cnt , CARDS.card_type, CARDS.card_issue_type, CARDS.tbl_card_id
			FROM ".TBL_CARDS." AS CARDS left join ".TBL_CARD_CATEGORY." AS POINTS ON CARDS.card_type=POINTS.tbl_card_category_id  
			WHERE CARDS.tbl_student_id='".$tbl_student_id."' 
			AND CARDS.tbl_class_id='".$tbl_class_id."' 
			AND CARDS.tbl_teacher_id='".$tbl_teacher_id."' 
			and POINTS.card_point < 0
			GROUP BY `card_type` , `card_issue_type`
			ORDER BY card_type"; 
		//echo $qry;
		$rs = $this->cf->SelectMultiRecords($qry);
		return $rs;	
	}


	/**
	* @desc		Get student object
	* 
	* @param	string $tbl_student_id  
	* @access	default
	* @return	object student
	*/
	function get_student_obj($tbl_student_id) {
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		
		$qry = "SELECT * FROM ".TBL_STUDENT." WHERE tbl_student_id='$tbl_student_id' ";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}


	/**
	* @desc		Get student object
	* 
	* @param	string $tbl_student_id  
	* @access	default
	* @return	object student
	*/
	function get_student_name($tbl_student_id) {
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		
		$qry = "SELECT first_name, first_name_ar, last_name, last_name_ar FROM ".TBL_STUDENT." WHERE tbl_student_id='$tbl_student_id' ";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
	

	/**
	* @desc		Get student picture
	* 
	* @param	string $tbl_student_id  
	* @access	default
	* @return	object student
	*/
	function get_student_picture($tbl_student_id) {
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		
		$qry = "SELECT file_name_updated FROM ".TBL_STUDENT." WHERE tbl_student_id='$tbl_student_id' ";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs[0]["file_name_updated"];
	}
	

	/**
	* @desc		Get child report and other data uploaded by school
	* 
	* @param	 
	* @access	default
	* @return	record set
	*/
	function get_child_data() {
		$email = $this->cf->get_data(trim($email));
		
		$qry = "SELECT * FROM ".TBL_PARENTING." WHERE is_active='Y' ORDER BY parenting_order ASC";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		//print_r($rs);
		return $rs;
	}


	/**
	* @desc		Get students list from given class name
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_student_list($tbl_class_id, $tbl_school_id) {
		$qry = "SELECT * FROM ".TBL_STUDENT." WHERE tbl_class_id='".$tbl_class_id."' AND tbl_school_id='".$tbl_school_id."' AND is_active='Y' ORDER BY first_name ASC";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;	
	}


	/**
	* @desc		Get students bus
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_student_bus($tbl_student_id) {
		$qry = "SELECT tbl_bus_id FROM ".TBL_BUS_STUDENT." WHERE tbl_student_id='".$tbl_student_id."' AND is_active='Y'";
		//echo "<br>".$qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs[0]["tbl_bus_id"];	
	}


	/**
	* @desc		Get all students that are registered with a particular bus
	* 
	* @param	tbl_bus_id
	* @access	default
	* @return	array $rs
	*/
	function get_all_bus_students($tbl_bus_id) {
		$qry = "SELECT * FROM ".TBL_STUDENT." WHERE is_active='Y' AND tbl_student_id IN (SELECT tbl_student_id FROM ".TBL_BUS_STUDENT." WHERE tbl_bus_id='".$tbl_bus_id."' AND is_active='Y')  ORDER BY first_name ASC ";
		//echo "<br>".$qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;	
	}


	/**
	* @desc		Get student's teacher
	* 
	* @param	tbl_bus_id
	* @access	default
	* @return	array $rs
	*/
	function get_students_teacher($tbl_student_id) {
		$qry = "SELECT * FROM ".TBL_STUDENT." WHERE is_active='Y' AND tbl_student_id IN (SELECT tbl_student_id FROM ".TBL_BUS_STUDENT." WHERE tbl_bus_id='".$tbl_bus_id."' AND is_active='Y')  ORDER BY first_name ASC ";
		//echo "<br>".$qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;	
	}
	

	/**
	* @desc		Get gallery images of a student
	* 
	* @param	string $email  
	* @access	default
	* @return	$tbl_teacher_id
	*/
	function get_gallery_student_images($tbl_student_id) {
		$tbl_gallery_category_id = $this->cf->get_data(trim($tbl_student_id));
		
		$qry = "SELECT * FROM ".TBL_IMAGE_GALLERYSTU." WHERE is_active='Y' AND tbl_student_id='$tbl_student_id' ORDER BY image_order ASC ";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;	
	}
	

	/**
	* @desc		Get gallery image
	* 
	* @param	string $tbl_image_gallery_id  
	* @access	default
	* @return	$rs
	*/
	function get_gallerystu_image($tbl_image_gallerystu_id) {
		$tbl_image_gallerystu_id = $this->cf->get_data(trim($tbl_image_gallerystu_id));
		
		$qry = "SELECT * FROM ".TBL_IMAGE_GALLERYSTU." WHERE tbl_image_gallerystu_id='$tbl_image_gallerystu_id' ";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;	
	}


	function list_all_students_against_class($tbl_class_id) {
		$tbl_class_id = $this->cf->get_data(trim($tbl_class_id));
		$qry_sel = "SELECT tbl_student_id FROM ".TBL_STUDENT." WHERE tbl_class_id='$tbl_class_id' AND is_active='Y' ORDER BY first_name ASC ";
		//echo $qry_sel."<br />";
		$rs = $this->cf->selectMultiRecords($qry_sel);
		return $rs;	
	}
	
	
	function get_semester_id($school_id,$current_date) {
		$school_id = $this->cf->get_data(trim($school_id));
		$qry_sel = "SELECT tbl_semester_id FROM ".TBL_SEMESTER." WHERE tbl_school_id='$school_id' AND is_active='Y'  AND 
					((start_date <= '$current_date') AND (end_date >= '$current_date'))";
		//echo $qry_sel."<br />";
		$rs = $this->cf->selectMultiRecords($qry_sel);
		return $rs;	
	}
	
	
}
?>