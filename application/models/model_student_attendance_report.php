<?php

include_once('include/common_functions.php');
/** @desc -- Student Attendance Report Model */

class Model_student_attendance_report extends CI_Model {
     var $cf;
     
	/** @desc Default constructor for the Controller	*/

    function model_student_attendance_report() {

		$this->cf = new Common_functions();

    }
	
	function getAttendanceInformation($tbl_student_id, $attendance_from, $attendance_to, $tbl_school_id, $tbl_teacher_id='',$tbl_class_id)
	{
			$qry = "SELECT TA.attendance_date, TA.is_present, TA.tbl_class_sessions_id, TA.tbl_teacher_id, CS.title, CS.title_ar, CONCAT(T.first_name ,' ', T.last_name) AS teacher_name                    ,CONCAT(T.first_name_ar ,' ', T.last_name_ar) AS teacher_name_ar  
			        FROM ".TBL_TEACHER_ATTENDANCE." AS TA 
			        LEFT JOIN ".TBL_CLASS_SESSIONS." AS CS ON CS.tbl_class_sessions_id=TA.tbl_class_sessions_id
					LEFT JOIN ".TBL_TEACHER." AS T ON T.tbl_teacher_id=TA.tbl_teacher_id
					WHERE 
			        TA.tbl_student_id='$tbl_student_id' AND TA.tbl_class_id='$tbl_class_id' "; 
			if($tbl_teacher_id<>""){
				$qry .= " AND TA.tbl_teacher_id='$tbl_teacher_id' ";
			}
            $qry .= "  AND TA.tbl_school_id='$tbl_school_id' ";
			if($attendance_to<>"")
			{
				$qry .=  " AND (TA.attendance_date>='$attendance_from' OR TA.attendance_date<='$attendance_to') "; 
			}else{
				$qry .=  " AND TA.attendance_date ='$attendance_from' ";
			}
			
			$qry .= "AND TA.is_active='Y' order by TA.attendance_date ASC, TA.added_date ASC ";
		//echo $qry; exit;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;	
		
	}
}

?>



