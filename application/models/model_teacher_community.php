<?php
include_once('include/common_functions.php');
/**
 * @DESC   	  	Message Model
 * @category   	Model
 * @author     	Shanavas .PK
 * @version    	0.1
 */

class Model_teacher_community extends CI_Model {
	var $cf;
	/**
	* @DESC Default constructor for the Controller
	* @access default
	*/

    function model_teacher_community() {
		$this->cf = new Common_functions();
    }
	
	
	function list_my_teachers_group($tbl_school_id, $tbl_teacher_id) {
		
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		
		$qry = "SELECT * FROM ".TBL_TEACHER_GROUP." AS TG LEFT JOIN ".TBL_TEACHER_GROUP_TEACHERS."  AS TGT ON TGT.tbl_teacher_group_id = TG.tbl_teacher_group_id WHERE 1 ";
		
		if($tbl_school_id<>"")
		{
			$qry .= " AND TG.tbl_school_id= '".$tbl_school_id."' ";
		}
		if($tbl_teacher_id<>"")
		{
			$qry .= " AND TGT.tbl_teacher_id= '".$tbl_teacher_id."' ";
		}
	
		//Active/Deactive
		$qry .= " AND TG.is_active='Y' ";
		$qry .= " GROUP BY TG.tbl_teacher_group_id";
		$qry .= " ORDER BY TG.id DESC";

        if($offset<>"")
			$qry .=" LIMIT $offset, ".TBL_TEACHER_GROUP_PAGING;
		
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	

	
	function get_teacher_community_messages($logged_teacher_id, $colleague_teacher_id, $tbl_school_id, $message_mode='', $tbl_teacher_group_id='') 
	{
		if($message_mode=="P")  // P - Private
		{
			$qry = "SELECT * FROM ".TBL_TEACHER_COMMUNITY_MESSAGE. " WHERE is_active='Y' AND tbl_school_id='".$tbl_school_id."'";
			$qry .= " AND (( message_from='".$logged_teacher_id."' AND  message_to='".$colleague_teacher_id."' ) ";
			$qry .= " OR ( message_from='".$colleague_teacher_id."' AND message_to='".$logged_teacher_id."' ) ) ";
			$qry .= "  AND  message_mode = '".$message_mode."' ";
			$qry .= " ORDER BY id DESC ";
		}else{
			 // G - Group
			$qry = "SELECT * FROM ".TBL_TEACHER_COMMUNITY_MESSAGE. " WHERE is_active='Y' AND tbl_school_id='".$tbl_school_id."'";
			$qry .= " AND ( message_from='".$logged_teacher_id."' OR  message_to='".$logged_teacher_id."' ) ";
			$qry .= "  AND  message_mode = '".$message_mode."' ";
			if($tbl_teacher_group_id<>"")
			{
				$qry .= "  AND tbl_teacher_group_id = '".$tbl_teacher_group_id."' ";
			}
			$qry .= " GROUP BY tbl_message_id";
			$qry .= " ORDER BY id DESC ";
		}
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
	
	
	
	function save_teacher_community_message($message_from, $message_to, $message_type, $tbl_message, $tbl_school_id, $tbl_item_id, $message_mode, $tbl_teacher_group_id='') {
		$message_from = $this->cf->get_data(trim($message_from));
		$message_to = $this->cf->get_data(trim($message_to));
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		$message_type = $this->cf->get_data(trim($message_type));
		$tbl_message_id = substr(md5(uniqid(rand())),0,10);
		
		if($tbl_teacher_group_id<>"")
		{
				$qryTeachers = "SELECT TGT.tbl_teacher_id FROM ".TBL_TEACHER_GROUP_TEACHERS."  AS TGT LEFT JOIN ".TBL_TEACHER." AS T ON T.tbl_teacher_id=TGT.tbl_teacher_id  WHERE 1 ";
		
				if($tbl_school_id<>"")
				{
					$qryTeachers .= " AND TGT.tbl_school_id= '".$tbl_school_id."' ";
				}
				if($tbl_teacher_group_id<>"")
				{
					$qryTeachers .= " AND TGT.tbl_teacher_group_id= '".$tbl_teacher_group_id."' ";
				}
                $qryTeachers .= " AND TGT.tbl_teacher_id <> '".$message_from."' ";
				$qryTeachers .= " AND T.is_active='Y' ";
				$qryTeachers .= " ORDER BY TGT.id DESC";
		        $rsTeachers   = $this->cf->selectMultiRecords($qryTeachers);
			for($k=0;$k<count($rsTeachers);$k++)
			{
				$message_to = $rsTeachers[$k]['tbl_teacher_id'];	
				$qry = "INSERT INTO ".TBL_TEACHER_COMMUNITY_MESSAGE." (`tbl_message_id`, `message_from`, `message_to`,`tbl_teacher_group_id`, `message_mode`, `message_type`, `message`, `tbl_item_id`, `added_date`, `tbl_school_id`)
					VALUES ('$tbl_message_id', '$message_from', '$message_to', '$tbl_teacher_group_id', '$message_mode', '$message_type', '$tbl_message', '$tbl_item_id', NOW(), '$tbl_school_id') ";
				$this->cf->insertInto($qry);
			}
			
		}else{
			$qry = "INSERT INTO ".TBL_TEACHER_COMMUNITY_MESSAGE." (`tbl_message_id`, `message_from`, `message_to`,`tbl_teacher_group_id`, `message_mode`, `message_type`, `message`, `tbl_item_id`, `added_date`, `tbl_school_id`)
					VALUES ('$tbl_message_id', '$message_from', '$message_to', '$tbl_teacher_group_id', '$message_mode', '$message_type', '$tbl_message', '$tbl_item_id', NOW(), '$tbl_school_id') ";
		    $this->cf->insertInto($qry);
		}
		//echo $qry; //exit;
	
		
		if (trim($_POST["is_admin"]) == "Y" && ($message_type == "image" || $message_type == "audio")) {
			$tbl_uploads_id = substr(md5(uniqid(rand())),0,10);
			$file_name_original = $_POST["file_name_original"];
			$file_name_updated = $_POST["file_name_updated"];
			$file_name_updated_thumb = $_POST["file_name_updated_thumb"];
			$file_type = $_POST["file_type"];
			$file_size = $_POST["file_size"];
			
			
			$qry = "INSERT INTO ".TBL_UPLOADS." (
					`tbl_uploads_id` ,
					`module_name` ,
					`tbl_item_id` ,
					`file_name_original` ,
					`file_name_updated` ,
					`file_name_updated_thumb` ,
					`file_type` ,
					`file_size` ,
					`is_active` ,
					`added_date`
					)
					VALUES (
						'$tbl_uploads_id', '$module_name', '$tbl_item_id', '$file_name_original', '$file_name_updated','$file_name_updated_thumb', '$file_type', '$file_size', 'Y', NOW()
					)";
			$this->cf->insertInto($qry);
			echo $qry;
		}
	}
	
	
	
	// Update teacher community /private  Message Update against Read Status
	function updateMessageReadStatus($tbl_teacher_group_id, $message_from='', $message_to='', $tbl_school_id, $message_mode ) {
		$tbl_teacher_group_id 	= $this->cf->get_data(trim($tbl_teacher_group_id));
		$message_from   		= $this->cf->get_data(trim($message_from));
		$message_to     		= $this->cf->get_data(trim($message_to));
		$qry = "UPDATE ".TBL_TEACHER_COMMUNITY_MESSAGE." SET read_status='Y' WHERE 1 ";
		if($tbl_teacher_group_id<>"")
		   $qry .= " AND  tbl_teacher_group_id='$tbl_teacher_group_id' ";
		if($message_from<>"")
		   $qry .= " AND  message_from ='$message_from' ";
		if($message_to<>"")
		   $qry .= " AND  message_to   ='$message_to' ";
        if($message_mode<>"")
		   $qry .= " AND  message_mode   ='$message_mode' ";
		$qry .= " AND tbl_school_id='$tbl_school_id' ";
		$this->cf->update($qry);
		return "Y";
	}
	
	
	

		
	/*function save_private_message($message_from, $message_to, $tbl_student_id, $message_type, $message, $tbl_school_id, $tbl_item_id, $message_dir) {
		$message_from = $this->cf->get_data(trim($message_from));
		$message_to = $this->cf->get_data(trim($message_to));
		$tbl_student_id = $this->cf->get_data(trim($tbl_student_id));
		$message_type = $this->cf->get_data(trim($message_type));
		$tbl_message_id = substr(md5(uniqid(rand())),0,10);
		$qry = "INSERT INTO ".TBL_PRIVATE_MESSAGE." (`tbl_message_id`, `message_from`, `message_to`,`message_dir`, `tbl_student_id`, `message_type`, `message`, `tbl_item_id`, `added_date`, `tbl_school_id`)
					VALUES ('$tbl_message_id', '$message_from', '$message_to', '$message_dir', '$tbl_student_id', '$message_type', '$message', '$tbl_item_id', NOW(), '$tbl_school_id') ";
		//echo $qry; //exit;
		$this->cf->insertInto($qry);
		
		if (trim($_POST["is_admin"]) == "Y" && ($message_type == "image" || $message_type == "audio")) {
			$tbl_uploads_id = substr(md5(uniqid(rand())),0,10);
			$file_name_original = $_POST["file_name_original"];
			$file_name_updated = $_POST["file_name_updated"];
			$file_name_updated_thumb = $_POST["file_name_updated_thumb"];
			$file_type = $_POST["file_type"];
			$file_size = $_POST["file_size"];
			
			
			$qry = "INSERT INTO ".TBL_UPLOADS." (
					`tbl_uploads_id` ,
					`module_name` ,
					`tbl_item_id` ,
					`file_name_original` ,
					`file_name_updated` ,
					`file_name_updated_thumb` ,
					`file_type` ,
					`file_size` ,
					`is_active` ,
					`added_date`
					)
					VALUES (
						'$tbl_uploads_id', '$module_name', '$tbl_item_id', '$file_name_original', '$file_name_updated','$file_name_updated_thumb', '$file_type', '$file_size', 'Y', NOW()
					)";
			$this->cf->insertInto($qry);
			echo $qry;
		}
	} */
	 
	 
}

?>

