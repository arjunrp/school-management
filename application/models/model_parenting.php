<?php
include_once('include/common_functions.php');

/**
 * @desc   	  	Parenting Model
 *
 * @category   	Model
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Model_parenting extends CI_Model {
	var $cf;


	/**
	* @desc Default constructor for the Controller
	*
	* @access default
	*/
    function model_parenting() {
		$this->cf = new Common_functions();
    }


	/**
	* @desc		Get parenting categories
	* 
	* @param	
	* @access	default
	* @return	$tbl_parent_id
	*/
	function get_parenting_categories($tbl_school_id) {
		// Now parenting is from Ministry so we exclude tbl_school_id from query
		$qry = "SELECT * FROM ".TBL_PARENTING_CAT." WHERE is_active='Y' ORDER BY parenting_cat_order ASC";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}


	/**
	* @desc		Get parenting data
	* 
	* @param	string $tbl_parenting_cat_id  
	* @access	default
	* @return	$tbl_parent_id
	*/
	function get_parenting_data($tbl_parenting_cat_id, $tbl_school_id) {
		// Now parenting is from Ministry so we exclude tbl_school_id from query
		$qry = "SELECT * FROM ".TBL_PARENTING." WHERE tbl_parenting_cat_id='$tbl_parenting_cat_id'";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}
	
		
	/**
	* @desc		Get parenting videos
	* 
	* @param	 
	* @access	default
	* @return	record set
	*/
	function get_parenting_video($tbl_parenting_id) {
		$qry = "SELECT * FROM ".TBL_UPLOADS." WHERE tbl_item_id='".$tbl_parenting_id."'";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs[0]["file_name_updated"];
	}
	
	
	/**
	* @desc		Get parenting text
	* 
	* @param	 
	* @access	default
	* @return	record set
	*/
	function get_parenting_text($tbl_parenting_id) {
		$qry = "SELECT * FROM ".TBL_PARENTING." WHERE tbl_parenting_id='".$tbl_parenting_id."'";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;
	}


    }
?>

