<?php
include_once('include/common_functions.php');

/**
 * @desc   	  	Ministry Parenting Category Model
 *
 * @category   	Model
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Model_ministry_parenting_category extends CI_Model {
	var $cf;

	/**
	* @desc Default constructor for the Controller
	*
	* @access default
	*/
    function model_ministry_parenting_category() {
		$this->cf = new Common_functions();
    }


	/**
	* @desc		Activate
	* 
	* @param	string $tbl_parenting_cat_id
	* @access	default
	*/
	function activate($tbl_parenting_cat_id) {
		$tbl_parenting_cat_id = mysql_real_escape_string(trim($tbl_parenting_cat_id));
		
		$qry = "UPDATE ".TBL_PARENTING_CAT." SET is_active='Y' WHERE tbl_parenting_cat_id='$tbl_parenting_cat_id' ";
		//echo $qry;
		
		$this->cf->update($qry);
	}


	/**
	* @desc		De-Activate
	* 
	* @param	string $tbl_parenting_cat_id
	* @access	default
	*/
	function deactivate($tbl_parenting_cat_id) {
		$tbl_parenting_cat_id = mysql_real_escape_string(trim($tbl_parenting_cat_id));
		
		$qry = "UPDATE ".TBL_PARENTING_CAT." SET is_active='N' WHERE tbl_parenting_cat_id='$tbl_parenting_cat_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}


	/**
	* @desc		Delete
	* 
	* @param	string $tbl_parenting_cat_id
	* @access	default
	*/
	function delete($tbl_parenting_cat_id) {
		$tbl_parenting_cat_id = mysql_real_escape_string(trim($tbl_parenting_cat_id));

		$qry = "DELETE FROM ".TBL_PARENTING_CAT." WHERE tbl_parenting_cat_id='$tbl_parenting_cat_id' ";
		//echo $qry;
		$this->cf->deleteFrom($qry);
	}
	

	/**
	* @desc		Get a country object based id
	* 
	* @param	string $tbl_parenting_cat_id
	* @access	default
	*/
	function get_ministry_parenting_category_obj($tbl_parenting_cat_id) {
		$email = mysql_real_escape_string($email);
	
		$qry = "SELECT * FROM ".TBL_PARENTING_CAT." WHERE 1 AND tbl_parenting_cat_id='$tbl_parenting_cat_id'";

		$results = $this->cf->selectMultiRecords($qry);
		
		$country_obj = array();
		$country_obj['tbl_parenting_cat_id'] = $results[0]['tbl_parenting_cat_id'];
		$country_obj['title_en'] = $results[0]['title_en'];
		$country_obj['title_ar'] = $results[0]['title_ar'];
		$country_obj['added_date'] = $results[0]['added_date'];
		$country_obj['is_active'] = $results[0]['is_active'];
		
	return $country_obj;
	}

	
	/**
	* @desc		Get all ministry parenting categorys
	* 
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function get_all_ministry_parenting_categorys($sort_name, $sort_by, $offset, $q, $is_active) {
		$sort_name = mysql_real_escape_string($sort_name);
		$sort_by = mysql_real_escape_string($sort_by);
		$offset = mysql_real_escape_string($offset);
		$q = mysql_real_escape_string($q);
		$is_active = mysql_real_escape_string($is_active);
	
		$qry = "SELECT * FROM ".TBL_PARENTING_CAT." WHERE 1 ";
		
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND is_active='$is_active' ";
		}
		 
		//Search
		if (trim($q) != "") {
			$qry .= " AND (title_en LIKE '%$q%' || title_ar LIKE '%$q%')";
		} 

		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY first_name ASC";
		}

		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_PARENTING_CAT_PAGING;
		}
		//echo $qry;
		
		$results = $this->cf->selectMultiRecords($qry);
		
	return $results;
	}

	
	/**
	* @desc		Get total ministry parenting categorys
	* 
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_ministry_parenting_categorys($q, $is_active) {
		$q = mysql_real_escape_string($q);
		$is_active = mysql_real_escape_string($is_active);
	
		$qry = "SELECT COUNT(*) as total_ministry_parenting_categorys FROM ".TBL_PARENTING_CAT." WHERE 1 ";
		
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND is_active='$is_active' ";
		}
		 
		//Search
		if (trim($q) != "") {
			$qry .= " AND (title_en LIKE '%$q%' || title_ar LIKE '%$q%')";
		} 

		//echo $qry;
		
		$results = $this->cf->selectMultiRecords($qry);
		
	return $results[0]['total_ministry_parenting_categorys'];
	}

	
	/**
	* @desc		Check if job category already exists
	* 
	* @param	String params
	* @access	default
	*/
	function is_exist($tbl_parenting_cat_id, $title_en, $title_ar) {
		$tbl_parenting_cat_id = mysql_real_escape_string($tbl_parenting_cat_id);
		$title_en = mysql_real_escape_string($title_en);
		$title_ar = mysql_real_escape_string($title_ar);

		$qry = "SELECT * FROM ".TBL_PARENTING_CAT." WHERE 1 ";
		if (trim($title_en) != "") {
			$qry .= " AND title_en='$title_en'";
		}
		if (trim($tbl_parenting_cat_id) != "") {
			$qry .= " AND tbl_parenting_cat_id <> '$tbl_parenting_cat_id'";
		}
		
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}

	
	/**
	* @desc		Create job category
	* 
	* @param	String params
	* @access	default
	*/
	function create_ministry_parenting_category($tbl_parenting_cat_id, $title_en, $title_ar) {
		$tbl_parenting_cat_id = mysql_real_escape_string($tbl_parenting_cat_id);
		$title_en = mysql_real_escape_string($title_en);
		$title_ar = mysql_real_escape_string($title_ar);

		$qry_ins = "INSERT INTO ".TBL_PARENTING_CAT." (`tbl_parenting_cat_id`, `title_en`, `title_ar`, `added_date`, `is_active`)
					VALUES ('$tbl_parenting_cat_id', '$title_en', '$title_ar', NOW(), 'Y')";
		//echo $qry_ins."<br>";
		$this->cf->insertInto($qry_ins);
	}

	
	/**
	* @desc		Save changes
	* 
	* @param	String params
	* @access	default
	*/
	function save_changes($tbl_parenting_cat_id, $title_en, $title_ar) {
		$tbl_parenting_cat_id = mysql_real_escape_string($tbl_parenting_cat_id);
		$title_en = mysql_real_escape_string($title_en);
		$title_ar = mysql_real_escape_string($title_ar);

		$qry_update = "UPDATE ".TBL_PARENTING_CAT." SET title_en='$title_en', title_ar='$title_ar' "; 
		$qry_update .= " WHERE tbl_parenting_cat_id='$tbl_parenting_cat_id' " ;

		//echo $qry_update."<br>";
		
		$this->cf->update($qry_update);
	}

}
?>