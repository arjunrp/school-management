<?php
include_once('include/common_functions.php');

/**
 * @desc   	  	Daily Report Model
 *
 * @category   	Model
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Model_daily_report extends CI_Model {
	var $cf;


	/**
	* @desc Default constructor for the Controller
	*
	* @access default
	*/
    function model_daily_report() {
		$this->cf = new Common_functions();
    }


	/**
	* @desc		Get daily_report
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function save_daily_report($tbl_teacher_id, $tbl_student_id, $tbl_parent_id, $option1, $option2, $option3, $option4, $message, $tbl_school_id) {
		
		date_default_timezone_set('Asia/Dubai');
		$today_date = date("Y-m-d");
		
		$tbl_daily_report_id = substr(md5(uniqid(rand())),0,10);
		$qry = "INSERT INTO ".TBL_DAILY_REPORT." (
				`tbl_daily_report_id` ,
				`tbl_teacher_id` ,
				`tbl_student_id` ,
				`tbl_parent_id` ,
				`option1` ,
				`option2` ,
				`option3` ,
				`option4` ,
				`message` ,
				`is_active` ,
				`added_date` ,
				`tbl_school_id`
				)
				VALUES (
					'$tbl_daily_report_id', '$tbl_teacher_id', '$tbl_student_id', '$tbl_parent_id', '$option1', '$option2', '$option3', '$option4', '$message', 'Y', '$today_date', '$tbl_school_id'
				)";
				
		//echo $qry."<br />";
		$this->cf->insertInto($qry);
		return;	
	}
	
	
	/**
	* @desc		Get daily_report
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_daily_report($tbl_student_id) {
		
		$qry = "SELECT * FROM ".TBL_DAILY_REPORT." WHERE tbl_student_id='".$tbl_student_id."' AND is_active='Y' ORDER BY added_date DESC LIMIT 0, ".PAGING_DAILY_REPORT;
		//echo $qry."<br />";
		$data_rs = $this->cf->SelectMultiRecords($qry);
		return $data_rs;	
	}
	
	
	/**
	* @desc		Check if report for the given date exists or not
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function is_daily_report_exists($report_date, $tbl_class_id) {
		
		$qry = "SELECT id FROM ".TBL_DAILY_REPORT." WHERE added_date LIKE '%".$report_date."%' AND is_active='Y' AND tbl_student_id IN (SELECT tbl_student_id FROM ".TBL_STUDENT." WHERE tbl_class_id='".$tbl_class_id."')";
		//echo "<br>".$qry."<br />";
		$data_rs = $this->cf->SelectMultiRecords($qry);
		if ($data_rs[0]["id"] != "") {
			return "Y";
		} else {
			return "N";
		}
	}
}
?>

