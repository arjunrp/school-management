<?php
include_once('include/common_functions.php');
/**
 * @desc   	  	Classes Model
 * @category   	Model
 * @author      Shanavas.PK
 * @version    	0.1
 */

class Model_classes extends CI_Model {

	var $cf;
	/**
	* @desc Default constructor for the Controller
	* @access default
	*/

    function model_classes() {
		$this->cf = new Common_functions();
    }

	/**
	* @desc		Get class name
	* @param	
	* @access	default
	* @return	array $rs
	*/

	function get_class_name($tbl_class_id) {
		$class_name = "";
 		$tbl_class_id = $this->cf->get_data(trim($tbl_class_id));
 		$qry_sel = "SELECT class_name FROM ".TBL_CLASS." WHERE tbl_class_id='$tbl_class_id' AND is_active='Y' ";
		//echo $qry_sel."<br />";
		$rs = $this->cf->selectMultiRecords($qry_sel);
		if (count($rs)>0) {
			$class_name = $rs[0]['class_name'];
		}
	return $class_name;	
	}

	/**
	* @desc		Get class name in Arabic
	* @param	
	* @access	default
	* @return	array $rs
	*/

	function get_class_name_ar($tbl_class_id) {
		$class_name = "";
		$tbl_class_id = $this->cf->get_data(trim($tbl_class_id));
		$qry_sel = "SELECT class_name_ar FROM ".TBL_CLASS." WHERE tbl_class_id='$tbl_class_id' AND is_active='Y' ";
		$rs = $this->cf->selectMultiRecords($qry_sel);
		if (count($rs)>0) {
			$class_name_ar = $rs[0]['class_name_ar'];
		}
	return $class_name_ar;	
	}


	/**
	* @desc		Get all teachers in class
	* @param	
	* @access	default
	* @return	array $rs
	*/

	function get_all_teachers_in_class($tbl_class_id) {
		$tbl_class_id = $this->cf->get_data(trim($tbl_class_id));
		$qry_sel = "SELECT * FROM ".TBL_TEACHER_CLASS." WHERE tbl_class_id='$tbl_class_id' AND is_active='Y'  ";
		//echo $qry_sel."<br />";
		$rs = $this->cf->selectMultiRecords($qry_sel);
	return $rs;	
	}


	function get_all_teachers_in_class_bysort($tbl_class_id,$lan='') {
		$tbl_class_id = $this->cf->get_data(trim($tbl_class_id));
		$qry_sel = "SELECT T.* FROM ".TBL_TEACHER." AS T 
					INNER JOIN  ".TBL_TEACHER_CLASS." AS TC ON TC.tbl_teacher_id=T.tbl_teacher_id 
					WHERE TC.tbl_class_id='$tbl_class_id' AND TC.is_active='Y' AND T.is_active='Y'  ";
					if($lan=="ar")
					{
						$qry_sel .=	" ORDER BY first_name_ar ASC ";
					}else{
						$qry_sel .=	" ORDER BY first_name ASC  ";
					}
		//echo $qry_sel."<br />";
		$rs = $this->cf->selectMultiRecords($qry_sel);
	return $rs;	
	}

	/**
	* @desc		Get class name list
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_class_list($tbl_school_id) {
		$qry = "SELECT * FROM ".TBL_CLASS." WHERE is_active='Y' AND tbl_school_id='$tbl_school_id' ORDER BY priority ASC";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;	
	}

	/**
	* @desc		Get classes against teacher
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_all_classes_against_teacher($tbl_teacher_id) {
		$qry = "SELECT * FROM ".TBL_CLASS." WHERE is_active='Y' AND tbl_class_id IN (SELECT tbl_class_id FROM ".TBL_TEACHER_CLASS." WHERE tbl_teacher_id='$tbl_teacher_id' AND is_active='Y') ORDER BY priority ASC";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;	
	}

	/**
	* @desc		Get class teacher
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_class_teacher($tbl_class_id) {
		$qry = "SELECT * FROM ".TBL_TEACHER." WHERE tbl_class_id='$tbl_class_id'";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;	
	}

	/**
	* @desc		Get class section
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_class_section($tbl_class_id) {
		$qry = "SELECT tbl_section_id FROM ".TBL_CLASS." WHERE tbl_class_id='$tbl_class_id'";
		//echo $qry;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs[0]["tbl_section_id"];	
	}

	
	function getClassInfo($tbl_class_id)
	{
		$qry = "SELECT * FROM ".TBL_CLASS." WHERE tbl_class_id='".$tbl_class_id."'";
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;	 
	}


	function getClassSectionInfo($tbl_section_id)
	{
		$qry = "SELECT * FROM ".TBL_SECTION." WHERE tbl_section_id='$tbl_section_id'";
		$rs = $this->cf->SelectMultiRecords($qry);
		return $rs;	
	}
	
	
  /************************************************************************************************/
  /************************************************************************************************/
  /************************************************************************************************/
	
	//BACKEND FUNCTIONALITY
	
	/**
	* @desc		Get all school types
	* 
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function get_all_school_types($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		$qry = "SELECT * FROM ".TBL_SCHOOL_TYPE."  WHERE 1 ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND is_active='$is_active' ";
		}
		$qry .= " AND is_active<>'D' "; 
		//Search
		if (trim($q) != "") {
			$qry .= " AND (school_type LIKE '%$q%' || school_type_ar LIKE '%$q%')";
		} 
		
		$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY school_type ASC ";
		}
		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_SCHOOL_TYPE_PAGING;
		}
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}

	
	/**
	* @desc		Get total categories
	* 
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_school_types($q, $is_active, $tbl_school_id) {
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT COUNT(*) as total_types FROM ".TBL_SCHOOL_TYPE." WHERE 1 ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND is_active='$is_active' ";
		}
		$qry .= " AND is_active<>'D' "; 
		
		$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		//Search
		if (trim($q) != "") {
			$qry .= " AND (school_type LIKE '%$q%' || school_type_ar LIKE '%$q%')";
		} 
		//echo $qry;
		$results = $this->cf->selectMultiRecords($qry);
		
	return $results[0]['total_types'];
	}

	
	
	function is_exist_type($tbl_school_type_id, $school_type, $school_type_ar, $tbl_school_id) {
		
		$tbl_school_type_id  	= $this->cf->get_data($tbl_school_type_id);
		$school_type 		    = $this->cf->get_data($school_type);
		$school_type_ar         = $this->cf->get_data($school_type_ar);

		$qry = "SELECT * FROM ".TBL_SCHOOL_TYPE." WHERE 1 ";
		
		if (trim($school_type) != "") {
			 $qry .= " AND school_type='$school_type' ";
		}
		
		if (trim($tbl_school_type_id) != "") {
			$qry .= " AND tbl_school_type_id <> '$tbl_school_type_id'";
		}
	    $qry .= " AND is_active<>'D' "; 
		$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}
	
	
	function school_type_info($school_type, $tbl_school_id) {
		
		$tbl_school_type_id  	= $this->cf->get_data($tbl_school_type_id);
		$school_type 		    = $this->cf->get_data($school_type);
		$school_type_ar         = $this->cf->get_data($school_type_ar);

		$qry = "SELECT * FROM ".TBL_SCHOOL_TYPE." WHERE 1 ";
		if (trim($school_type) != "") {
			 $qry .= " AND school_type='$school_type' ";
		}
	    $qry .= " AND is_active<>'D' "; 
		$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}
	
	/**
	* @desc		Create school type
	* 
	* @param	String params
	* @access	default
	*/
	function create_school_type($tbl_school_type_id, $school_type, $school_type_ar, $tbl_school_id) {
		$tbl_school_type_id = $this->cf->get_data($tbl_school_type_id);
		$school_type        = $this->cf->get_data($school_type);
		$school_type_ar     = $this->cf->get_data($school_type_ar);
		if($tbl_school_id<>"")
		{
			$qry_ins = "INSERT INTO ".TBL_SCHOOL_TYPE." (`tbl_school_type_id`, `school_type`, `school_type_ar`, `added_date`, `is_active`, `tbl_school_id`)
						VALUES ('$tbl_school_type_id', '$school_type', '$school_type_ar', NOW(), 'Y', '$tbl_school_id')";
			$this->cf->insertInto($qry_ins);
			return "Y";
		}else{
		   return "N";	
			
		}
	}
	
	/**
	* @desc		Activate
	* @param	string $tbl_school_type_id
	* @access	default
	*/
	function activate_type($school_type_id_enc,$tbl_school_id) {
		$tbl_school_type_id = $this->cf->get_data(trim($school_type_id_enc));
		
		$qry = "UPDATE ".TBL_SCHOOL_TYPE." SET is_active='Y' WHERE tbl_school_type_id='$tbl_school_type_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate
	* @param	string $tbl_school_type_id
	* @access	default
	*/
	function deactivate_type($school_type_id_enc,$tbl_school_id) {
		$tbl_school_type_id = $this->cf->get_data(trim($school_type_id_enc));
		
		$qry = "UPDATE ".TBL_SCHOOL_TYPE." SET is_active='N' WHERE tbl_school_type_id='$tbl_school_type_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete
	* @param	string $tbl_school_type_id
	* @access	default
	*/
	function delete_type($school_type_id_enc,$tbl_school_id) {
		$tbl_school_type_id = $this->cf->get_data(trim($school_type_id_enc));
        $qry = "UPDATE ".TBL_SCHOOL_TYPE." SET is_active='D' WHERE tbl_school_type_id='$tbl_school_type_id' AND  tbl_school_id='$tbl_school_id'";
		echo $qry;
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_CATEGORY." WHERE tbl_category_id='$tbl_category_id' ";
		//$this->cf->deleteFrom($qry);
	}
	
	/**
	* @desc		Get a category object based id
	* @param	string $tbl_school_type_id
	* @access	default
	*/
	function get_school_type_obj($tbl_school_type_id) {
		$qry = "SELECT * FROM ".TBL_SCHOOL_TYPE." WHERE 1  AND tbl_school_type_id='$tbl_school_type_id'";
		$results = $this->cf->selectMultiRecords($qry);
		$school_type_obj = array();
		$school_type_obj['tbl_school_type_id'] = $results[0]['tbl_school_type_id'];
		$school_type_obj['school_type']        = $results[0]['school_type'];
		$school_type_obj['school_type_ar']     = $results[0]['school_type_ar'];
		$school_type_obj['added_date']         = $results[0]['added_date'];
		$school_type_obj['is_active']          = $results[0]['is_active'];
		
	return $school_type_obj;
	}

	/**
	* @desc		Save changes
	* @param	String params
	* @access	default
	*/
	function save_type_changes($tbl_school_type_id, $school_type, $school_type_ar, $tbl_school_id) {
		$tbl_school_type_id         = $this->cf->get_data($tbl_school_type_id);
		$school_type                = $this->cf->get_data($school_type);
		$school_type_ar             = $this->cf->get_data($school_type_ar);
		$tbl_school_id              = $this->cf->get_data($tbl_school_id);

		$qry_update = "UPDATE ".TBL_SCHOOL_TYPE." SET school_type='$school_type', school_type_ar='$school_type_ar' "; 
		$qry_update .= " WHERE tbl_school_type_id='$tbl_school_type_id' AND  tbl_school_id='$tbl_school_id'  " ;
		$this->cf->update($qry_update);
	}

    /**
	* @desc		Get all class sections
	* 
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function get_all_class_sections($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		$qry = "SELECT * FROM ".TBL_SECTION."  WHERE 1 ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND is_active='$is_active' ";
		}
		$qry .= " AND is_active<>'D' "; 
		//Search
		if (trim($q) != "") {
			$qry .= " AND (section_name LIKE '%$q%' || section_name_ar LIKE '%$q%')";
		} 
		
		$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY school_type ASC ";
		}
		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_SECTION_PAGING;
		}
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	/**
	* @desc		Get total categories
	* 
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_class_sections($q, $is_active, $tbl_school_id) {
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT COUNT(*) as total_section FROM ".TBL_SECTION." WHERE 1 ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND is_active='$is_active' ";
		}
		$qry .= " AND is_active<>'D' "; 
		
		$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		//Search
		if (trim($q) != "") {
			$qry .= " AND (section_name LIKE '%$q%' || section_name_ar LIKE '%$q%')";
		} 
		//echo $qry;
		$results = $this->cf->selectMultiRecords($qry);
		
	return $results[0]['total_section'];
	}
	
	function is_exist_section($tbl_section_id, $section_name, $section_name_ar, $tbl_school_id) {
		
		$tbl_section_id  	    = $this->cf->get_data($tbl_section_id);
		$section_name 		  = $this->cf->get_data($section_name);
		$section_name_ar        = $this->cf->get_data($section_name_ar);

		$qry = "SELECT * FROM ".TBL_SECTION." WHERE 1 ";
		if (trim($section_name) != "") {
			 $qry .= " AND section_name='$section_name' ";
		}
		
		if (trim($tbl_section_id) != "") {
			$qry .= " AND tbl_section_id <> '$tbl_section_id'";
		}
	    $qry .= " AND is_active<>'D' "; 
		$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}
	
	/**
	* @desc		create_class_section
	* 
	* @param	String params
	* @access	default
	*/
	function create_class_section($tbl_section_id, $section_name, $section_name_ar, $tbl_school_id) {
		$tbl_section_id     = $this->cf->get_data($tbl_section_id);
		$section_name       = $this->cf->get_data($section_name);
		$section_name_ar     = $this->cf->get_data($section_name_ar);
		if($tbl_school_id<>"")
		{
			$qry_ins = "INSERT INTO ".TBL_SECTION." (`tbl_section_id`, `section_name`, `section_name_ar`, `added_date`, `is_active`, `tbl_school_id`)
						VALUES ('$tbl_section_id', '$section_name', '$section_name_ar', NOW(), 'Y', '$tbl_school_id')";
			$this->cf->insertInto($qry_ins);
			return "Y";
		}else{
		   return "N";	
			
		}
	}
	
	/**
	* @desc		Activate
	* @param	string $section_id_enc
	* @access	default
	*/
	function activate_section($section_id_enc,$tbl_school_id) {
		$tbl_section_id = $this->cf->get_data(trim($section_id_enc));
		
		$qry = "UPDATE ".TBL_SECTION." SET is_active='Y' WHERE tbl_section_id='$tbl_section_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate
	* @param	string $section_id_enc
	* @access	default
	*/
	function deactivate_section($section_id_enc,$tbl_school_id) {
		$tbl_section_id = $this->cf->get_data(trim($section_id_enc));
		
		$qry = "UPDATE ".TBL_SECTION." SET is_active='N' WHERE tbl_section_id='$tbl_section_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete
	* @param	string $section_id_enc
	* @access	default
	*/
	function delete_section($section_id_enc,$tbl_school_id) {
		$tbl_section_id = $this->cf->get_data(trim($section_id_enc));
        $qry = "UPDATE ".TBL_SECTION." SET is_active='D' WHERE tbl_section_id='$tbl_section_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_CATEGORY." WHERE tbl_category_id='$tbl_category_id' ";
		//$this->cf->deleteFrom($qry);
	}
	
	/**
	* @desc		Get a category object based id
	* @param	string $tbl_section_id
	* @access	default
	*/
	function get_class_section_obj($tbl_section_id) {
		$qry = "SELECT * FROM ".TBL_SECTION." WHERE 1  AND tbl_section_id='$tbl_section_id'";
		$results = $this->cf->selectMultiRecords($qry);
		$class_section_obj = array();
		$class_section_obj['tbl_section_id']     = $results[0]['tbl_section_id'];
		$class_section_obj['section_name']       = $results[0]['section_name'];
		$class_section_obj['section_name_ar']    = $results[0]['section_name_ar'];
		$class_section_obj['added_date']         = $results[0]['added_date'];
		$class_section_obj['is_active']          = $results[0]['is_active'];
		
	return $class_section_obj;
	}

	/**
	* @desc		Save changes
	* @param	String params
	* @access	default
	*/
	function save_section_changes($tbl_section_id, $section_name, $section_name_ar, $tbl_school_id) {
		$tbl_section_id         = $this->cf->get_data($tbl_section_id);
		$section_name           = $this->cf->get_data($section_name);
		$section_name_ar        = $this->cf->get_data($section_name_ar);
		$tbl_school_id          = $this->cf->get_data($tbl_school_id);

		$qry_update = "UPDATE ".TBL_SECTION." SET section_name='$section_name', section_name_ar='$section_name_ar' "; 
		$qry_update .= " WHERE tbl_section_id='$tbl_section_id' AND  tbl_school_id='$tbl_school_id'  " ;
		$this->cf->update($qry_update);
	}


   // CLASS GRADES / CLASS FUNCTION 
   
     /**
	* @desc		Get all class 
	* 
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function get_all_class_grades($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		$qry = "SELECT C.*, S.section_name, S.section_name_ar, T.school_type, T.school_type_ar  FROM ".TBL_CLASS."  AS C 
		        LEFT JOIN  ".TBL_SECTION." AS S  ON  C.tbl_section_id = S.tbl_section_id  
				LEFT JOIN  ".TBL_SCHOOL_TYPE." AS T  ON  T.tbl_school_type_id= C.tbl_school_type_id  
				WHERE 1 ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND C.is_active='$is_active' ";
		}
		$qry .= " AND C.is_active<>'D' "; 
		//Search
		if (trim($q) != "") {
			$qry .= " AND ((C.class_name LIKE '%$q%' || C.class_name_ar LIKE '%$q%') 
			          OR  (S.section_name LIKE '%$q%' || S.section_name_ar LIKE '%$q%') 
					  OR  (T.school_type  LIKE '%$q%' || T.school_type_ar LIKE '%$q%'))";
		} 
		
		$qry .= " AND C.tbl_school_id = '".$tbl_school_id."' ";
		
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY C.$sort_name $sort_by , S.section_name ASC  ";
		} else {
			/*$qry .= " ORDER BY C.class_name ASC, S.section_name ASC  ";*/
			
			$qry .= " ORDER BY C.priority ASC, C.id DESC, C.class_name ASC, S.section_name ASC  ";
		}
		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_CLASS_PAGING;
		}
		$results = $this->cf->selectMultiRecords($qry);
		
		for($m=0;$m<count($results);$m++)
		{
			$tbl_class_id = $results[$m]['tbl_class_id'];
			$cntPeriods = $this->getCntPeriods($tbl_class_id, $tbl_school_id);
			$results[$m]['cntPeriods']	=	$cntPeriods;
		}
	return $results;
	}
	
	
	function getCntPeriods($tbl_class_id, $tbl_school_id)
	{
		$qry_sel = "SELECT * FROM ".TBL_CLASS_SESSIONS." WHERE is_active<>'D' AND tbl_school_id='".$tbl_school_id."' ";
		if($tbl_class_id<>""){
			$qry_sel .= " AND tbl_class_id = '".$tbl_class_id."' ";
		}
		$qry_sel .= " ORDER BY sessions_order ASC";
		//echo $qry_sel."<br />";
		$rs = $this->cf->selectMultiRecords($qry_sel);
		return count($rs);
		
	}
	
	
	/**
	* @desc		Get total categories
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_class_grades($q, $is_active, $tbl_school_id) {
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT COUNT(*) as total_class FROM ".TBL_CLASS." AS C 
		        LEFT JOIN  ".TBL_SECTION." AS S  ON  C.tbl_section_id = S.tbl_section_id  
				LEFT JOIN  ".TBL_SCHOOL_TYPE." AS T  ON  T.tbl_school_type_id= C.tbl_school_type_id  
				WHERE 1 ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND C.is_active='$is_active' ";
		}
		$qry .= " AND C.is_active<>'D' "; 
		//Search
		if (trim($q) != "") {
			$qry .= " AND ((C.class_name LIKE '%$q%' || C.class_name_ar LIKE '%$q%') 
			          OR  (S.section_name LIKE '%$q%' || S.section_name_ar LIKE '%$q%') 
					  OR  (T.school_type  LIKE '%$q%' || T.school_type_ar LIKE '%$q%'))";
		} 
		
		$qry .= " AND C.tbl_school_id = '".$tbl_school_id."' ";
		//Search
		if (trim($q) != "") {
			$qry .= " AND (class_name LIKE '%$q%' || class_name_ar LIKE '%$q%')";
		} 
		$results = $this->cf->selectMultiRecords($qry);
		
	return $results[0]['total_class'];
	}
	
	function is_exist_grade($tbl_class_id, $tbl_school_type_id, $class_name, $class_name_ar, $tbl_section_id, $tbl_school_id) {
		$tbl_class_id  	      = $this->cf->get_data($tbl_class_id);
		$tbl_school_type_id  	= $this->cf->get_data($tbl_school_type_id);
		$class_name 		    = $this->cf->get_data($class_name);
		$class_name_ar         = $this->cf->get_data($class_name_ar);
		$tbl_section_id  	      = $this->cf->get_data($tbl_section_id);

		$qry = "SELECT * FROM ".TBL_CLASS." WHERE 1 ";
		
		if (trim($class_name) != "") {
			 $qry .= " AND class_name='$class_name' ";
		}
		
		if (trim($tbl_class_id) != "") {
			$qry .= " AND tbl_class_id <> '$tbl_class_id'";
		}
		
		if (trim($tbl_section_id) != "") {
			$qry .= " AND tbl_section_id = '".$tbl_section_id."'" ;
		}
	    $qry .= " AND is_active<>'D' "; 
		$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}
	
	/**
	* @desc		create_class_grade
	* 
	* @param	String params
	* @access	default
	*/
	function create_class_grade($tbl_class_id, $tbl_school_type_id, $school_type, $school_type_ar, $class_name, $class_name_ar, $tbl_section_id, $section_name, $section_name_ar,  $tbl_school_id)
	{
		$tbl_class_id  	      	= $this->cf->get_data($tbl_class_id);
		$tbl_school_type_id  	= $this->cf->get_data($tbl_school_type_id);
		$school_type 		    = $this->cf->get_data($school_type);
		$school_type_ar         = $this->cf->get_data($school_type_ar);
		$class_name 		    = $this->cf->get_data($class_name);
		$class_name_ar         	= $this->cf->get_data($class_name_ar);
		$tbl_section_id  	    = $this->cf->get_data($tbl_section_id);
		$section_name 		    = $this->cf->get_data($section_name);
		$section_name_ar        = $this->cf->get_data($section_name_ar);
		
		if($tbl_school_id<>"")
		{
			if($school_type<>"")
			{
				$tbl_school_type_id     = md5(uniqid(rand()));
			    $res_exist_type         = $this->is_exist_type($tbl_school_type_id, $school_type, $school_type_ar, $tbl_school_id);
				
				if(empty($res_exist_type))
				{
			    	$this->create_school_type($tbl_school_type_id, $school_type, $school_type_ar, $tbl_school_id);
				}else{
					$res_type			=	$this->school_type_info($school_type, $tbl_school_id);
					$tbl_school_type_id	=	$res_type[0]['tbl_school_type_id'];
				}
			}
			
			if($section_name<>"")
			{
				$tbl_section_id     = md5(uniqid(rand()));
			    $res_exist_section  = $this->is_exist_section($tbl_section_id, $section_name, $section_name_ar, $tbl_school_id);
				if(empty($res_exist_section))
				{
			    	$this->create_class_section($tbl_section_id, $section_name, $section_name_ar, $tbl_school_id);
				}
				
			}
		
			$resultsExist = $this->is_exist_grade($tbl_class_id, $tbl_school_type_id, $class_name, $class_name_ar, $tbl_section_id, $tbl_school_id);
			if(count($resultsExist)==0)
			{
				$qry_ins = "INSERT INTO ".TBL_CLASS." (`tbl_class_id`, `tbl_school_type_id`, `tbl_section_id`, `class_name`, `class_name_ar`, `added_date`, `is_active`, `tbl_school_id`)
							VALUES ('$tbl_class_id', '$tbl_school_type_id', '$tbl_section_id', '$class_name', '$class_name_ar', NOW(), 'Y', '$tbl_school_id')";
				$this->cf->insertInto($qry_ins);
				return "Y";
			}else{
				 return "X";		
			}
		}else{
		   return "N";	
			
		}
	}
	
	/**
	* @desc		Activate
	* @param	string $class_id_enc
	* @access	default
	*/
	function activate_grade($class_id_enc,$tbl_school_id) {
		$tbl_class_id = $this->cf->get_data(trim($class_id_enc));
		
		$qry = "UPDATE ".TBL_CLASS." SET is_active='Y' WHERE tbl_class_id='$tbl_class_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate
	* @param	string $class_id_enc
	* @access	default
	*/
	function deactivate_grade($class_id_enc,$tbl_school_id) {
		$tbl_class_id = $this->cf->get_data(trim($class_id_enc));
		
		$qry = "UPDATE ".TBL_CLASS." SET is_active='N' WHERE tbl_class_id='$tbl_class_id' AND  tbl_school_id='$tbl_school_id' ";
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete
	* @param	string $class_id_enc
	* @access	default
	*/
	function delete_grade($class_id_enc,$tbl_school_id) {
		$tbl_class_id = $this->cf->get_data(trim($class_id_enc));
        $qry = "UPDATE ".TBL_CLASS." SET is_active='D' WHERE tbl_class_id='$tbl_class_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
	}
	
	/**
	* @desc		Get a category object based id
	* @param	string $tbl_class_id
	* @access	default
	*/
	function get_class_grade_obj($tbl_class_id) {
		$qry = "SELECT * FROM ".TBL_CLASS." WHERE 1  AND tbl_class_id='$tbl_class_id'";
		$results = $this->cf->selectMultiRecords($qry);
		$class_section_obj = array();
		$class_section_obj['tbl_class_id']     	= $results[0]['tbl_class_id'];
		$class_section_obj['tbl_school_type_id']  = $results[0]['tbl_school_type_id'];
		$class_section_obj['tbl_section_id']      = $results[0]['tbl_section_id'];
		$class_section_obj['class_name']          = $results[0]['class_name'];
		$class_section_obj['class_name_ar']       = $results[0]['class_name_ar'];
		$class_section_obj['added_date']          = $results[0]['added_date'];
		$class_section_obj['is_active']           = $results[0]['is_active'];
		
	return $class_section_obj;
	}

	/**
	* @desc		Save changes
	* @param	String params
	* @access	default
	*/
	function save_grade_changes($tbl_class_id, $tbl_school_type_id, $class_name, $class_name_ar, $tbl_section_id, $tbl_school_id) {
		$tbl_class_id  	      = $this->cf->get_data($tbl_class_id);
		$tbl_school_type_id  	= $this->cf->get_data($tbl_school_type_id);
		$class_name 		    = $this->cf->get_data($class_name);
		$class_name_ar         = $this->cf->get_data($class_name_ar);
		$tbl_section_id  	      = $this->cf->get_data($tbl_section_id);

		$qry_update = "UPDATE ".TBL_CLASS." SET class_name='$class_name', class_name_ar='$class_name_ar', tbl_school_type_id='$tbl_school_type_id', tbl_section_id= '$tbl_section_id' "; 
		$qry_update .= " WHERE tbl_class_id='$tbl_class_id' AND  tbl_school_id='$tbl_school_id'  " ;
		$this->cf->update($qry_update);
	}


   function get_school_types($tbl_school_id, $active)
   {
	    $qry = "SELECT * FROM ".TBL_SCHOOL_TYPE." WHERE 1 ";
	    $qry .= " AND is_active<>'D' "; 
		$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		if($active<>"")
			$qry .= "  AND is_active = '$active' ";
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;	
   }

   function get_class_sections($tbl_school_id, $active)
   {
	    $qry = "SELECT * FROM ".TBL_SECTION." WHERE 1 ";
	    $qry .= " AND is_active<>'D' "; 
		$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		if($active<>"")
			$qry .= "  AND is_active = '$active' ";
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;	
   }
   
   
   // SEMESTER FUNCTION 
   
     /**
	* @desc		Get all SEMESTERS
	* 
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function get_all_semesters($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id,$academic_year_id='') {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		$qry = "SELECT SEM.*, AY.academic_start, AY.academic_end  FROM ".TBL_SEMESTER." AS SEM 
		        LEFT JOIN ".TBL_ACADEMIC_YEAR." AS AY ON SEM.tbl_academic_year_id = AY.tbl_academic_year_id
				WHERE 1 ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND SEM.is_active='$is_active' ";
		}
		$qry .= " AND SEM.is_active<>'D' "; 
		//Search
		if (trim($q) != "") {
			$qry .= " AND ( SEM.title LIKE '%$q%' OR SEM.title_ar LIKE '%$q%'  OR AY.academic_start ='$q' OR AY.academic_end ='$q' )";
		} 
		
		if($academic_year_id<>"")
		{
			$qry    .= " AND SEM.tbl_academic_year_id = '".trim($academic_year_id)."' ";
			
		}

		if($field && !empty($q)){	    	

			if($field == 'academic_year')

			{
				$qry    .= " AND SEM.tbl_academic_year_id = '".trim($academic_year_id)."' ";

			}else{
				$qry .= " AND SEM.`$field` $LIKE '%$q%' ";
			}
		}	
		$qry .= " AND SEM.tbl_school_id = '".$tbl_school_id."' ";
		
		
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY SEM.$sort_name $sort_by  ";
		} else {
			$qry .= " ORDER BY SEM.added_date ASC ";
		}
		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_SEMESTER_PAGING;
		}
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	
	function get_current_semester($tbl_school_id)
	{
		$qry = "SELECT *  FROM ".TBL_SEMESTER." AS SEM WHERE 1 ";
		$qry .= " AND SEM.is_active='Y' ";
		$qry .= " AND CURDATE() between start_date and end_date ";
		$qry .= " AND SEM.tbl_school_id = '".$tbl_school_id."' ";
		$results = $this->cf->selectMultiRecords($qry);
	  return $results;
	}
	
	function get_academic_semesters($tbl_school_id,$academic_year_id)
	{
		$qry = "SELECT *  FROM ".TBL_SEMESTER." AS SEM WHERE 1 ";
		$qry .= " AND SEM.is_active='Y' ";
		if($academic_year_id<>"")
		{
			$qry .= " AND tbl_academic_year_id= '".$academic_year_id."' ";
		}
		$qry .= " AND SEM.tbl_school_id = '".$tbl_school_id."' ";
		$results = $this->cf->selectMultiRecords($qry);
	  return $results;
	}
	
	
	/**
	* @desc		Get total SEMESTERS
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_semesters($q, $is_active, $tbl_school_id) { 
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT COUNT(*) as total_semester  FROM ".TBL_SEMESTER." AS SEM LEFT JOIN ".TBL_ACADEMIC_YEAR." AS AY ON  SEM.tbl_academic_year_id = AY.tbl_academic_year_id 
		        WHERE 1 ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND SEM.is_active='$is_active' ";
		}
		$qry .= " AND SEM.is_active<>'D' "; 
		//Search
		if (trim($q) != "") {
			$qry .= " AND ( SEM.title LIKE '%$q%' OR SEM.title_ar LIKE '%$q%'  OR AY.academic_start ='$q' OR AY.academic_end ='$q' )";
		} 
		
		if($academic_year_id<>"")
		{
			$qry    .= " AND SEM.tbl_academic_year_id = '".trim($academic_year_id)."' ";
			
		}

		if($field && !empty($q)){	    	

			if($field == 'academic_year')
			{
				$qry    .= " AND SEM.tbl_academic_year_id = '".trim($academic_year_id)."' ";

			}else{
				$qry .= " AND SEM.`$field` $LIKE '%$q%' ";
			}
		}	
		$qry .= " AND SEM.tbl_school_id = '".$tbl_school_id."' ";
		//echo $qry;
		$results = $this->cf->selectMultiRecords($qry);
		
	return $results[0]['total_semester'];
	}
	
	function get_academic_year($tbl_school_id)
	{
	    $qry = "SELECT * FROM ".TBL_ACADEMIC_YEAR." WHERE tbl_school_id='$tbl_school_id' AND is_active='Y' ORDER BY academic_start ASC";
		$results = $this->cf->selectMultiRecords($qry);
		return $results;
	}
	
	
	function is_exist_semester($tbl_semester_id, $title, $start_date, $end_date, $tbl_school_id) {
		$tbl_semester_id 	      = $this->cf->get_data($tbl_semester_id);
		$title 		            = $this->cf->get_data($title);
		$start_date  		       = $this->cf->get_data($start_date);
		$end_date  	             = $this->cf->get_data($end_date);

		$qry = " SELECT * FROM ".TBL_SEMESTER." WHERE tbl_school_id='$tbl_school_id' AND 
							(('$start_date' BETWEEN `start_date` AND `end_date`) OR ( '$end_date' between `start_date` AND `end_date`)) AND is_active<>'D'";
		$results = $this->cf->selectMultiRecords($qry);
		if(count($results)>0)
		{
			return "Y";
		}else{
			return "N";
		}
		
	}
	
	/**
	* @desc		create_class_semester
	* @param	String params
	* @access	default
	*/
	function create_semester($tbl_semester_id, $title, $title_ar, $tbl_academic_year_id, $start_date, $end_date, $tbl_school_id) {
		$tbl_semester_id 	    = $this->cf->get_data($tbl_semester_id);
		$title  			      = $this->cf->get_data($title);
		$title_ar 		       = $this->cf->get_data($title_ar);
		$tbl_academic_year_id   = $this->cf->get_data($tbl_academic_year_id);
		$start_date  	         = $this->cf->get_data($start_date);
		$end_date  	           = $this->cf->get_data($end_date);

		if($tbl_school_id<>"")
		{
			$qry_ins = "INSERT INTO ".TBL_SEMESTER." (`tbl_semester_id`, `title`, `title_ar`, `tbl_academic_year_id`, `start_date`, `end_date`, `added_date`, `is_active`, `tbl_school_id`)
								VALUES ('$tbl_semester_id', '$title', '$title_ar', '$tbl_academic_year_id', '$start_date', '$end_date', NOW(), 'Y', '$tbl_school_id')";
			$this->cf->insertInto($qry_ins);
			 return "Y";		
		}else{
			return "N";
		}
	
	}
	
	/**
	* @desc		Activate
	* @param	string $class_id_enc
	* @access	default
	*/
	function activate_semester($tbl_semester_id,$tbl_school_id) {
		$tbl_semester_id = $this->cf->get_data(trim($tbl_semester_id));
		$qry = "UPDATE ".TBL_SEMESTER." SET is_active='Y' WHERE tbl_semester_id='$tbl_semester_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate
	* @param	string $class_id_enc
	* @access	default
	*/
	function deactivate_semester($tbl_semester_id,$tbl_school_id) {
		$tbl_semester_id = $this->cf->get_data(trim($tbl_semester_id));
		$qry = "UPDATE ".TBL_SEMESTER." SET is_active='N' WHERE tbl_semester_id='$tbl_semester_id' AND  tbl_school_id='$tbl_school_id' ";
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete
	* @param	string $class_id_enc
	* @access	default
	*/
	function delete_semester($tbl_semester_id,$tbl_school_id) {
		$tbl_semester_id = $this->cf->get_data(trim($tbl_semester_id));
        $qry = "UPDATE ".TBL_SEMESTER." SET is_active='D' WHERE tbl_semester_id='$tbl_semester_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
	}
	
	/**
	* @desc		Get a category object based id
	* @param	string $tbl_class_id
	* @access	default
	*/
	function get_semester_obj($tbl_semester_id) {
		$qry = "SELECT *  FROM ".TBL_SEMESTER." AS SEM LEFT JOIN ".TBL_ACADEMIC_YEAR." AS AY ON  SEM.tbl_academic_year_id = AY.tbl_academic_year_id WHERE 1 ";
		if($tbl_semester_id<>"")
		{
			$qry .= " AND tbl_semester_id= '".$tbl_semester_id."' ";
		}
		$results = $this->cf->selectMultiRecords($qry);
		$class_semester_obj = array();
		$class_semester_obj['tbl_semester_id']     	= $results[0]['tbl_semester_id'];
		$class_semester_obj['title']  		          = $results[0]['title'];
		$class_semester_obj['title_ar']               = $results[0]['title_ar'];
		$class_semester_obj['tbl_academic_year_id']   = $results[0]['tbl_academic_year_id'];
		$class_semester_obj['start_date']             = $results[0]['start_date'];
		$class_semester_obj['end_date']               = $results[0]['end_date'];
	return $class_semester_obj;
	}

	/**
	* @desc		Save changes
	* @param	String params
	* @access	default
	*/
	function save_semester_changes($tbl_semester_id, $title, $title_ar, $tbl_academic_year_id, $start_date, $end_date, $tbl_school_id) {
		$tbl_semester_id 	    = $this->cf->get_data($tbl_semester_id);
		$title  			      = $this->cf->get_data($title);
		$title_ar 		       = $this->cf->get_data($title_ar);
		$tbl_academic_year_id   = $this->cf->get_data($tbl_academic_year_id);
		$start_date  	         = $this->cf->get_data($start_date);
		$end_date  	           = $this->cf->get_data($end_date);

		$qry_update = "UPDATE ".TBL_SEMESTER." SET title='$title', title_ar='$title_ar', start_date='$start_date', end_date= '$end_date' , tbl_academic_year_id= '$tbl_academic_year_id' "; 
		$qry_update .= " WHERE tbl_semester_id='$tbl_semester_id' AND tbl_school_id='$tbl_school_id' " ;
		$this->cf->update($qry_update);
	}

   function get_all_classes($tbl_school_id, $active, $tbl_class_id='')
   {
	   $qry = "SELECT C.*, S.section_name, S.section_name_ar, T.school_type, T.school_type_ar  FROM ".TBL_CLASS."  AS C 
		        LEFT JOIN  ".TBL_SECTION." AS S  ON  C.tbl_section_id = S.tbl_section_id  
				LEFT JOIN  ".TBL_SCHOOL_TYPE." AS T  ON  T.tbl_school_type_id= C.tbl_school_type_id  
				WHERE 1 ";
		$qry .= " AND C.is_active='Y' ";
		$qry .= " AND C.tbl_school_id = '".$tbl_school_id."' ";
		if($tbl_class_id<>"")
		{
			$qry .= " AND C.tbl_class_id = '".$tbl_class_id."' ";
		}
		$qry .= " ORDER BY class_name ASC, S.section_name ASC  ";
		$results = $this->cf->selectMultiRecords($qry);
	    return $results;	
   }
   // END SEMESTER CONFIGURATION SECTION
   
   //START ACADEMIC YEAR
   	function get_all_academic_year($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = $this->cf->get_data($q);
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		$qry = "SELECT * FROM ".TBL_ACADEMIC_YEAR." AS AY WHERE 1 ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND AY.is_active='$is_active' ";
		}
		$qry .= " AND AY.is_active<>'D' "; 
		$qry .= " AND AY.tbl_school_id = '".$tbl_school_id."' ";
	
		$qry .= " ORDER BY AY.added_date ASC ";
		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_SEMESTER_PAGING;
		}
		//echo $qry;
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
   /**
	* @desc		Get Academic Year
	* 
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_academic_year($q, $is_active, $tbl_school_id) {
		$q         = $this->cf->get_data($q);
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT COUNT(*) as total_year  FROM ".TBL_ACADEMIC_YEAR." AS AY  WHERE 1 ";
		
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND AY.is_active='$is_active' ";
		}
		$qry .= " AND AY.is_active<>'D' "; 
		$qry .= " AND AY.tbl_school_id = '".$tbl_school_id."' ";
		$results = $this->cf->selectMultiRecords($qry);
		
	    return $results[0]['total_year'];
	}
	

	
	function is_exist_academic_year($tbl_academic_year_id, $academic_start, $academic_end, $tbl_school_id) {
		$tbl_academic_year_id 	 = $this->cf->get_data($tbl_academic_year_id);
		$academic_start 		   = $this->cf->get_data($academic_start);
		$academic_end  		     = $this->cf->get_data($academic_end);

		$qry = " SELECT * FROM ".TBL_ACADEMIC_YEAR." WHERE tbl_school_id='$tbl_school_id' AND academic_start='$academic_start' AND is_active<>'D'";
		if($tbl_academic_year_id<>"")
		{
			$qry .= " AND tbl_academic_year_id <>'".$tbl_academic_year_id."' ";
		}
		$results = $this->cf->selectMultiRecords($qry);
		if(count($results)>0)
		{
			return "Y";
		}else{
			return "N";
		}
		
	}
	
	/**
	* @desc		Academic Year
	* @param	String params
	* @access	default
	*/
	function create_academic_year($tbl_academic_year_id, $academic_start, $academic_end, $tbl_school_id) {
		$tbl_academic_year_id 	 = $this->cf->get_data($tbl_academic_year_id);
		$academic_start 		   = $this->cf->get_data($academic_start);
		$academic_end  		     = $this->cf->get_data($academic_end);

		if($tbl_school_id<>"")
		{
			$qry_ins = "INSERT INTO ".TBL_ACADEMIC_YEAR." (`tbl_academic_year_id`, `academic_start`, `academic_end`, `added_date`, `is_active`, `tbl_school_id`)
								VALUES ('$tbl_academic_year_id', '$academic_start', '$academic_end', NOW(), 'Y', '$tbl_school_id')";
			$this->cf->insertInto($qry_ins);
			return "Y";		
		}else{
			return "N";
		}
	
	}
	
	/**
	* @desc		Activate Academic Year
	* @param	string $class_id_enc
	* @access	default
	*/
	function activate_academic_year($academic_year_id_enc,$tbl_school_id) {
		$tbl_academic_year_id = $this->cf->get_data(trim($academic_year_id_enc));
		$qry = "UPDATE ".TBL_ACADEMIC_YEAR." SET is_active='Y' WHERE tbl_academic_year_id='$tbl_academic_year_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate Academic Year
	* @param	string $class_id_enc
	* @access	default
	*/
	function deactivate_academic_year($academic_year_id_enc,$tbl_school_id) {
		$tbl_academic_year_id = $this->cf->get_data(trim($academic_year_id_enc));
		$qry = "UPDATE ".TBL_ACADEMIC_YEAR." SET is_active='N' WHERE tbl_academic_year_id='$tbl_academic_year_id' AND  tbl_school_id='$tbl_school_id' ";
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete Academic Year
	* @param	string $class_id_enc
	* @access	default
	*/
	function delete_academic_year($academic_year_id_enc,$tbl_school_id) {
		$tbl_academic_year_id = $this->cf->get_data(trim($academic_year_id_enc));
        $qry = "UPDATE ".TBL_ACADEMIC_YEAR." SET is_active='D' WHERE tbl_academic_year_id='$tbl_academic_year_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
	}
	
	/**
	* @desc		Get a category object based id
	* @param	string $tbl_class_id
	* @access	default
	*/
	function get_academic_year_obj($academic_year_id_enc) {
		$qry = "SELECT *  FROM  ".TBL_ACADEMIC_YEAR." AS AY  WHERE 1 ";
		if($academic_year_id_enc<>"")
		{
			$qry .= " AND tbl_academic_year_id = '".$academic_year_id_enc."' ";
		}
		$results = $this->cf->selectMultiRecords($qry);
		$academic_year_obj = array();
		$academic_year_obj['tbl_academic_year_id']   = $results[0]['tbl_academic_year_id'];
		$academic_year_obj['academic_start']         = $results[0]['academic_start'];
		$academic_year_obj['academic_end']           = $results[0]['academic_end'];
	return $academic_year_obj;
	}

	/**
	* @desc		Save Academic Year changes
	* @param	String params
	* @access	default
	*/
	function save_academic_year_changes($tbl_academic_year_id, $academic_start, $academic_end, $tbl_school_id) {
		$tbl_academic_year_id 	 = $this->cf->get_data($tbl_academic_year_id);
		$academic_start 		   = $this->cf->get_data($academic_start);
		$academic_end  		     = $this->cf->get_data($academic_end);

		$qry_update = "UPDATE ".TBL_ACADEMIC_YEAR." SET academic_start='$academic_start', academic_end='$academic_end' "; 
		$qry_update .= " WHERE tbl_academic_year_id='$tbl_academic_year_id' AND tbl_school_id='$tbl_school_id' " ;
		$this->cf->update($qry_update);
	}
   //End Academic Year 
   
  // Classes against teacher
   function list_classes_against_teacher($tbl_teacher_id,$tbl_school_id)
   {  
        $qry = "SELECT C.*, S.section_name, S.section_name_ar, T.school_type, T.school_type_ar  FROM ".TBL_CLASS."  AS C 
		        LEFT JOIN  ".TBL_SECTION." AS S  ON  C.tbl_section_id = S.tbl_section_id  
				LEFT JOIN  ".TBL_SCHOOL_TYPE." AS T  ON  T.tbl_school_type_id= C.tbl_school_type_id  
				WHERE 1  AND C.tbl_class_id IN (SELECT tbl_class_id FROM ".TBL_TEACHER_CLASS." WHERE tbl_teacher_id='$tbl_teacher_id' AND is_active='Y')";
		//Active/Deactive
		$qry .= " AND C.is_active='Y' ";
		//Search
		if (trim($q) != "") {
			$qry .= " AND ((C.class_name LIKE '%$q%' || C.class_name_ar LIKE '%$q%') 
			          OR  (S.section_name LIKE '%$q%' || S.section_name_ar LIKE '%$q%') 
					  OR  (T.school_type  LIKE '%$q%' || T.school_type_ar LIKE '%$q%'))";
		} 
		
		$qry .= " AND C.tbl_school_id = '".$tbl_school_id."' ";
		
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY C.$sort_name $sort_by , S.section_name ASC  ";
		} else {
			$qry .= " ORDER BY C.class_name ASC, S.section_name ASC  ";
		}
		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_CLASS_PAGING;
		}
		$results = $this->cf->selectMultiRecords($qry);
	return $results; 

   }
   
   
   function get_semesters_against_academicYear($academic_year_id,$tbl_school_id)
   {
	   $qry = "SELECT SEM.*  FROM ".TBL_SEMESTER." AS SEM WHERE 1 ";
	   $qry .= " AND SEM.is_active<>'D' "; 
		
	   if($academic_year_id<>"")
	   {
		  $qry    .= " AND SEM.tbl_academic_year_id = '".trim($academic_year_id)."' ";
	   }
	
	   $qry .= " AND SEM.tbl_school_id = '".$tbl_school_id."' ";
	   $results = $this->cf->selectMultiRecords($qry);
	return $results; 
   }
   
   
   
   function update_sort_order($sortVal, $class_id_enc,$tbl_school_id) {
		
		$qry = "UPDATE ".TBL_CLASS." SET priority='".$sortVal."' WHERE tbl_class_id='$class_id_enc' AND  tbl_school_id='$tbl_school_id' ";
		$this->cf->update($qry);
	}
 
  /************************************************************************************************/
  /************************************************************************************************/
  /************************************************************************************************/
 


}

?>



