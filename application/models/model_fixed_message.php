<?php
include_once('include/common_functions.php');

/**
 * @desc   	  	Fixed Message Model
 *
 * @category   	Model
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Model_fixed_message extends CI_Model {
	var $cf;


	/**
	* @desc Default constructor for the Controller
	*
	* @access default
	*/
    function model_fixed_message() {
		$this->cf = new Common_functions();
    }


	/**
	* @desc		Get fixe message details
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_fixed_message($tbl_fixed_message_id) { 
		$qry = "SELECT * FROM ".TBL_FIXED_MESSAGE." WHERE tbl_fixed_message_id='".$tbl_fixed_message_id."'";
		//echo $qry;
		$data_rs = $this->cf->selectMultiRecords($qry);
		return $data_rs;
	}
	
	
	/**
	* @desc		Get fixe message details
	* 
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_all_fixed_messages($message_type) { 
		$qry = "SELECT tbl_fixed_message_id, message_en, message_ar	 FROM ".TBL_FIXED_MESSAGE." WHERE message_type='".$message_type."' AND is_active='Y' ORDER BY message_en ASC";
		//echo $qry;
		$data_rs = $this->cf->selectMultiRecords($qry);
		return $data_rs;
	}
	
}
?>

