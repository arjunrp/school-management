<?php
include_once('include/common_functions.php');

/**
 * @desc   	  	Home Model
 *
 * @category   	Model
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Model_home extends CI_Model {
	var $cf;


	/**
	* @desc Default constructor for the Controller
	*
	* @access default
	*/
    function model_home() {
		$this->cf = new Common_functions();
    }


	/**
	* @desc		Get principal message for home page
	* 
	* @param	none
	* @access	default
	* @return	array $rs
	*/
	function home_page_message() {
		$qry_sel = "SELECT * FROM ".TBL_HOME_PAGE_MESSAGE." WHERE is_active='Y' ";
		//echo $qry_sel."<br />";
		$rs = $this->cf->selectMultiRecords($qry_sel);
	return $rs;	
	}


	/**
	* @desc		Get home page images
	* 
	* @param	none
	* @access	default
	* @return	array $rs
	*/
	function home_page_images() {
		$qry_sel = "SELECT * FROM ".TBL_HOME_PAGE_IMAGES." WHERE is_active='Y' ORDER BY RAND() LIMIT 4 ";
		//echo $qry_sel."<br />";
		$rs = $this->cf->selectMultiRecords($qry_sel);
	return $rs;	
	}
    }
?>

