<?php
include_once('include/common_functions.php');
/**
 * @desc   	  	Events Model
 * @category   	Model
 * @author     	Shanavas PK
 * @version    	0.1
 */

class Model_events extends CI_Model {

	var $cf;
	/**
	* @desc Default constructor for the Controller
	* @access default
	*/

    function model_events() {
		$this->cf = new Common_functions();
    }

	/**
	* @desc		Get event details
	* @param	none 	
	* @access	default
	* @return	array $rs
	*/

	function get_event_details($id) {
		$qry_sel = "SELECT * FROM ".TBL_EVENTS." WHERE id='$id' ";
		//echo $qry_sel;
		$rs = $this->cf->selectMultiRecords($qry_sel);
	return $rs;	
	}

	
	function get_event_list($tbl_school_id) {
		$offset = 0;
		$CountRec .= "SELECT * FROM ".TBL_EVENTS." WHERE tbl_school_id='$tbl_school_id' ";	
		$Query .= "SELECT * FROM ".TBL_EVENTS." WHERE tbl_school_id='$tbl_school_id' ";	
		
		if(!$by) {
			$by="DESC";
		}
		
		$currDate =  date("Y-m-d");
		$Query .= "  AND end_date>= '".$currDate."' ";  
		$CountRec .= " AND end_date>= '".$currDate."' ";
		$CountRec .= " AND is_active= 'Y' "; 
		$Query .= " AND is_active= 'Y' "; 
		$Query .= " ORDER BY start_date ASC ";
		$Query .=" LIMIT $offset, ".TBL_EVENTS_PAGING;
		$data = $rs = $this->cf->selectMultiRecords($Query);
	return $data;
	}
	
	
	// BACKEND FUNCTIONALITY
		/**
	* @desc		Get all events
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function get_all_events($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id) {
		$sort_name  = $this->cf->get_data($sort_name);
		$sort_by 	= $this->cf->get_data($sort_by);
		$offset     = $this->cf->get_data($offset);
		$q          = urldecode($this->cf->get_data($q));
		$is_active  = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
		
		$qry = "SELECT * FROM ".TBL_EVENTS." AS E WHERE 1 ";
		$qry .= " AND E.tbl_school_id= '".$tbl_school_id."' ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND E.is_active='$is_active' ";
		}
		$qry .= " AND E.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( E.title LIKE '%$q%' OR E.title_ar LIKE '%$q%' ) ";
		}
		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY E.id DESC ";
		}
		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_EVENTS_PAGING;
		}
		
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	/**
	* @desc		Get total events
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_events($q, $is_active, $tbl_school_id) {
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry =  "SELECT COUNT(distinct(E.tbl_events_id)) as total_events FROM ".TBL_EVENTS." AS E WHERE 1 ";
		$qry .= " AND E.tbl_school_id= '".$tbl_school_id."' ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND E.is_active='$is_active' ";
		}
		$qry .= " AND E.is_active<>'D' "; 
		
		//Search
		if (trim($q) != "") {
			$qry    .= " 	AND ( E.title LIKE '%$q%' OR E.title_ar LIKE '%$q%' ) ";
		}
		$results = $this->cf->selectMultiRecords($qry);
	    return $results[0]['total_events'];
	}
	

	function is_exist_event($tbl_event_id, $title, $title_ar, $tbl_school_id) {
		$tbl_event_id  	    = $this->cf->get_data($tbl_event_id);
		$title 		       = $this->cf->get_data($title);
		$title_ar            = $this->cf->get_data($title_ar);

		$qry = "SELECT * FROM ".TBL_EVENTS." WHERE 1 ";
		if (trim($title) != "") {
			 $qry .= " AND title='$title' ";
		}
		if (trim($tbl_event_id) != "") {
			 $qry .= " AND tbl_events_id<>'$tbl_event_id' ";
		}
	    $qry .= " AND is_active<>'D' "; 
		$qry .= " AND tbl_school_id = '".$tbl_school_id."' ";
		$results = $this->cf->selectMultiRecords($qry);
	return $results;	
	}
	
	
	/**
	* @desc		Add events
	* @param	String params
	* @access	default
	*/
	function add_event($tbl_event_id, $title, $title_ar, $description, $description_ar, $start_date, $end_date, $start_time, $tbl_school_id)
    {
		$tbl_event_id      = $this->cf->get_data($tbl_event_id);
		$title        	 = $this->cf->get_data($title);
		$title_ar          = $this->cf->get_data($title_ar);
		$description       = $this->cf->get_data($description);
		$description_ar    = $this->cf->get_data($description_ar);
		$start_date        = $this->cf->get_data($start_date);
		$end_date          = $this->cf->get_data($end_date);
		$start_time        = $this->cf->get_data($start_time);
		$tbl_school_id     = $this->cf->get_data($tbl_school_id);
		
		if($tbl_school_id<>"")
		{
			$qry_ins = "INSERT INTO ".TBL_EVENTS." (`tbl_events_id`, `title`, `title_ar`, `description`, `description_ar`, `start_date`, 
			`end_date`, `start_time`, `added_date`, `is_active`, `tbl_school_id`)
						VALUES ('$tbl_event_id', '$title', '$title_ar', '$description', '$description_ar', '$start_date',
			'$end_date', '$start_time', NOW(), 'Y', '$tbl_school_id')";
			
			//echo $qry_ins."<br />";
			
			$this->cf->insertInto($qry_ins);
			return "Y";
		}else{
		   return "N";	
			
		}
	}
	
	/**
	* @desc		Activate event
	* @param	string $tbl_event_id
	* @access	default
	*/
	function activate_event($tbl_event_id,$tbl_school_id) {
		$tbl_event_id = $this->cf->get_data(trim($tbl_event_id));
		$qry = "UPDATE ".TBL_EVENTS." SET is_active='Y' WHERE tbl_events_id='$tbl_event_id' AND  tbl_school_id='$tbl_school_id' ";
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate event
	* @param	string $tbl_student_id
	* @access	default
	*/
	function deactivate_event($tbl_event_id,$tbl_school_id) {
		$tbl_event_id = $this->cf->get_data(trim($tbl_event_id));
		
		$qry = "UPDATE ".TBL_EVENTS." SET is_active='N' WHERE tbl_events_id='$tbl_event_id' AND  tbl_school_id='$tbl_school_id' ";
		//echo $qry;
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete event
	* @param	string $tbl_student_id
	* @access	default
	*/
	function delete_event($tbl_event_id,$tbl_school_id) {
		$tbl_event_id = $this->cf->get_data(trim($tbl_event_id));
        $qry = "UPDATE ".TBL_EVENTS." SET is_active='D' WHERE tbl_events_id='$tbl_event_id' AND  tbl_school_id='$tbl_school_id'";
		$this->cf->update($qry);
		//$qry = "DELETE FROM ".TBL_CATEGORY." WHERE tbl_category_id='$tbl_category_id' ";
		//$this->cf->deleteFrom($qry);
	}
	
	
	
	function get_event_obj($tbl_event_id,$tbl_school_id) {
		$qry = "SELECT * FROM ".TBL_EVENTS." WHERE 1  AND tbl_events_id='$tbl_event_id' AND  tbl_school_id='$tbl_school_id'";
		$results = $this->cf->selectMultiRecords($qry);
		$event_obj = array();
		$event_obj['tbl_events_id']      = $results[0]['tbl_events_id'];
		$event_obj['title']              = $results[0]['title'];
		$event_obj['title_ar']           = $results[0]['title_ar'];
		$event_obj['description']        = $results[0]['description'];
		$event_obj['description_ar']     = $results[0]['description_ar'];
		$event_obj['start_date']         = $results[0]['start_date'];
		$event_obj['end_date']           = $results[0]['end_date'];
		$event_obj['start_time']         = $results[0]['start_time'];
		$event_obj['added_date']         = $results[0]['added_date'];
		$event_obj['is_active']          = $results[0]['is_active'];
		
	   return $event_obj;
	}

	function save_event_changes($tbl_event_id, $title, $title_ar, $description, $description_ar, $start_date, $end_date, $start_time, $tbl_school_id)
	{
	    $tbl_event_id      = $this->cf->get_data($tbl_event_id);
		$title        	 = $this->cf->get_data($title);
		$title_ar          = $this->cf->get_data($title_ar);
		$description       = $this->cf->get_data($description);
		$description_ar    = $this->cf->get_data($description_ar);
		$start_date        = $this->cf->get_data($start_date);
		$end_date          = $this->cf->get_data($end_date);
		$start_time        = $this->cf->get_data($start_time);
		$tbl_school_id     = $this->cf->get_data($tbl_school_id);
		
		if($tbl_school_id<>"")
		{
			$qry  = "UPDATE ".TBL_EVENTS." SET title='$title', title_ar='$title_ar', description='$description', start_date='$start_date', end_date='$end_date', 
			         start_time='$start_time' WHERE tbl_events_id = '$tbl_event_id' and tbl_school_id = '$tbl_school_id' ";
			$this->cf->update($qry);
		    return "Y";
		}else{
		   return "N";	
			
		}
	}
	
	// END BACKEND FUNCTIONALITY
	
	
	
	
	

    }

?>



