<?php
include_once('include/common_functions.php');

/**
 * @desc   	  	User Model
 *
 * @category   	Model
 * @author     	Shanavas.PK
 * @version    	0.1
 */
class Model_book extends CI_Model {
	var $cf;

	/**
	* @desc Default constructor for the Controller
	*
	* @access default
	*/
    function model_book() {
		$this->cf = new Common_functions();
    }
	
	function get_all_books($sort_name, $sort_by, $offset, $q="", $tbl_category_id="", $is_active='') {
		$qry = "SELECT B.*, BC.category_name_en, BC.category_name_ar  FROM ".TBL_BOOKS." AS B LEFT JOIN ".TBL_BOOK_CATEGORY." AS BC ON B.tbl_book_category_id=BC.tbl_book_category_id WHERE 1= 1  ";
		if($is_active<>"")
		{
			$qry .= " AND B.is_active='Y' ";
		}
		
		$qry .= " AND B.is_active <>'D' ";
		
		if($tbl_category_id<>"")
		{
			$qry .= " AND BC.tbl_book_category_id= '".$tbl_category_id."' ";
		}
		if($q<>"")
		{
			$qry .= " AND (  B.title_en like  '%".$q."%' OR B.title_ar like ' %".$q."%'  OR  B.author_name_en like  '%".$q."%' OR  B.author_name_ar like  '%".$q."%' OR  B.description_en like  '%".$q."%' OR  B.description_ar like  '%".$q."%' OR   BC.category_name_en like  '%".$q."%' OR  BC.category_name_ar like '%".$q."%' OR  B.book_language like  '%".$q."%'  ) " ;
		}
		if($sort_name<>"")
		{
			 $qry .= " ORDER BY B.$sort_name $sort_by ";
		}else{
			$qry .= " ORDER BY B.title_en ASC, B.added_date ASC ";
		}
		
		$qry .= "LIMIT $offset , ".PAGING_TBL_BOOKS;
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	
	
	function get_total_books($q,$tbl_category_id='',$is_active='') {
		$qry = "SELECT B.tbl_book_id FROM ".TBL_BOOKS." AS B LEFT JOIN ".TBL_BOOK_CATEGORY." AS BC ON B.tbl_book_category_id=BC.tbl_book_category_id WHERE 1 = 1  ";
		if($is_active<>"")
		{
			$qry .= " AND B.is_active='Y' ";
		}
		
		$qry .= " AND B.is_active <>'D' ";
		
		if($q<>"")
		{
			$qry .= " AND (  B.title_en like  '%".$q."%' OR B.title_ar like ' %".$q."%'  OR  B.author_name_en like  '%".$q."%' OR  B.author_name_ar like  '%".$q."%' OR  B.description_en like  '%".$q."%' OR  B.description_ar like  '%".$q."%' OR   BC.category_name_en like  '%".$q."%' OR  BC.category_name_ar like '%".$q."%' OR  B.book_language like  '%".$q."%'  ) " ;
		}
		
		if($tbl_category_id<>"")
		{
			$qry .= " AND BC.tbl_book_category_id= '".$tbl_category_id."' " ;
		}
		
		$qry .= " ORDER BY B.title_en ASC, B.added_date ASC ";
		
		//echo $qry;
		$results = $this->cf->selectMultiRecords($qry);
	return count($results);
	}
	
	
	function save_book($tbl_book_id,$tbl_book_category_id, $tbl_language_id, $title_en, $title_ar, $author_name_en, $author_name_ar, $isbn, $description_en, $description_ar,$tbl_class_id){
				
				$title_en =  addslashes($title_en);
				$title_ar =  addslashes($title_ar);
				$author_name_en = addslashes($author_name_en);
				$author_name_ar = addslashes($author_name_ar);
				$description_en = addslashes($description_en);
				$description_ar = addslashes($description_ar);
				
				$sql4sess = "INSERT INTO " . TBL_BOOKS . "(tbl_book_id, tbl_book_category_id, book_language, title_en, title_ar,author_name_en,author_name_ar,isbn,description_en,description_ar,added_date) VALUES('$tbl_book_id','$tbl_book_category_id', '$tbl_language_id','$title_en','$title_ar','$author_name_en','$author_name_ar','$isbn','$description_en','$description_ar',NOW())";
				$sql4userSession = "SELECT tbl_book_id from " . TBL_BOOKS . " where (title_en='$title_en' or title_ar= '$title_ar') and tbl_book_category_id='$tbl_book_category_id'  ";
			    if ($this->cf->isExist($sql4userSession)){
					return "X";
					  // $this->deleteFrom($sql4DelUserSession);
				}else{	
					$this->cf->insertInto($sql4sess);
					return "Y";
				}
	}
	
	
	function assign_book_to_class($tbl_book_id,$tbl_class_id,$tbl_school_id)
	{
		
		if($tbl_class_id<>"")
		{
			 $qryDel = "DELETE FROM ".TBL_BOOK_ASSIGN_CLASS." WHERE tbl_book_id='$tbl_book_id' and  tbl_school_id='$tbl_school_id' ";
		     $this->cf->deleteFrom($qryDel);
			
			$str  = $tbl_class_id; //e.g." tbl_class_id=f31981d6285a6a958f754&tbl_class_id=d99f9fee0&tbl_class_id=a343d70666c3
			$str  = explode("&", $str); 
			for($m=0; $m<count($str); $m++) {
				  if($str[$m]<>""){
					$tbl_class_id       =  $str[$m];
					$tbl_book_assign_id = substr(md5(uniqid(rand())),0,10);
					$qryIns = "INSERT INTO ".TBL_BOOK_ASSIGN_CLASS." (`tbl_book_assign_id`,`tbl_book_id`,`tbl_class_id`, `tbl_school_id`, `is_active`) 
									 VALUES ('$tbl_book_assign_id','$tbl_book_id','$tbl_class_id','$tbl_school_id','Y') ";
					$this->cf->insertInto($qryIns);
				  }
			}
		}
	}
	
	
	function get_assigned_classes($tbl_book_id,$tbl_school_id, $is_active)
   {
	   $qry = " SELECT tbl_class_id FROM ".TBL_BOOK_ASSIGN_CLASS." AS AC WHERE 1  AND AC.tbl_book_id= '".$tbl_book_id."' ";  
	   $qry .= " AND AC.is_active='".$is_active."' ";
	   $qry .= " AND AC.tbl_school_id = '".$tbl_school_id."' ORDER BY AC.id ASC ";
	   $results = $this->cf->selectMultiRecords($qry);
	   return $results;	
   }
	
	
	
	
	
	function add_file_to_book($tbl_book_id)
	{
		$qry = "SELECT * FROM ".TBL_BOOK_FILE_TEMP." WHERE tbl_book_id='$tbl_book_id'";
		$results = $this->cf->selectMultiRecords($qry);
        if(count($results)>0)
		{
			
			$book_file_name =  $results[0]['book_file_name'];
			$cover_pic      =  $results[0]['cover_pic'];
			$sql4Update = "UPDATE " . TBL_BOOKS . " SET book_file_name='".$book_file_name."',  cover_pic='".$cover_pic."' WHERE tbl_book_id = '".$tbl_book_id."'" ;
			$this->cf->update($sql4Update);
			
		}
		/*$qry = "DELETE FROM ".TBL_BOOK_FILE_TEMP." WHERE tbl_book_id='$tbl_book_id' ";
		$this->cf->deleteFrom($qry);*/
		
	}
	
	
	function  delete_book_file($tbl_book_id)
	{
		$qry = "SELECT * FROM ".TBL_BOOK_FILE_TEMP." WHERE tbl_book_id='$tbl_book_id'";
		$results = $this->cf->selectMultiRecords($qry);
        if(count($results)>0)
		{
			
			$book_file_name =  $results[0]['book_file_name'];
			$book_file_path =  HOST_URL."/uploads/books/".$book_file_name;
			$cover_pic      =  $results[0]['cover_pic'];
			$cover_pic_path =  HOST_URL."/uploads/books/".$cover_pic;
			unlink($book_file_path);
			unlink($cover_pic_path);
			
		}
		$qry = "DELETE FROM ".TBL_BOOK_FILE_TEMP." WHERE tbl_book_id='$tbl_book_id' ";
		$this->cf->deleteFrom($qry);
		
	}
	
	
	
	function getBookInfo($tbl_book_id) {
		$qry = "SELECT * FROM ".TBL_BOOKS." WHERE tbl_book_id='$tbl_book_id'";
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	function upload_book_cover($file_name_updated,$tbl_book_id){
		$data['cover_pic'] = $file_name_updated;
		$qry = "SELECT * FROM ".TBL_BOOK_FILE_TEMP." WHERE tbl_book_id='$tbl_book_id'";
		$results = $this->cf->selectMultiRecords($qry);
        if(count($results)>0)
		{
			$sql4Update = "UPDATE " . TBL_BOOK_FILE_TEMP . " SET cover_pic='".$file_name_updated."' WHERE tbl_book_id = '".$tbl_book_id."'" ;
			$this->cf->update($sql4Update);
			
		}else{
			$qry = "INSERT INTO ".TBL_BOOK_FILE_TEMP." (tbl_book_id,cover_pic,added_date)  VALUES ('$tbl_book_id','$file_name_updated',NOW()) ";
			//echo $qry;
			$this->cf->insertInto($qry);
		}
	  return $data;	
	}
	
	
	function upload_book_file($file_name_updated,$tbl_book_id) {
		
		$data['book_file_name'] = $file_name_updated;
		
		$qry = "SELECT * FROM ".TBL_BOOK_FILE_TEMP." WHERE tbl_book_id='".$tbl_book_id."'";
		$results = $this->cf->selectMultiRecords($qry);
        if(count($results)>0)
		{
			$sql4Update = "UPDATE " . TBL_BOOK_FILE_TEMP . " SET book_file_name='".$file_name_updated."' WHERE tbl_book_id = '".$tbl_book_id."'" ;
			$this->cf->update($sql4Update);
			
		}else{
			$qry = "INSERT INTO ".TBL_BOOK_FILE_TEMP." (tbl_book_id,book_file_name,added_date)  VALUES ('$tbl_book_id','$file_name_updated',NOW()) ";
			//echo $qry;
			$this->cf->insertInto($qry);
		}
	return $data;	
	}
	
	
	
	function is_exist_book($tbl_book_id, $tbl_language_id, $title_en, $title_ar){
			
				$sql4existBook = "SELECT tbl_book_id from " . TBL_BOOKS . " where (title_en='$title_en' OR title_ar = '$title_ar') 
				and tbl_book_id<>'$tbl_book_id'";
				echo $sql4existBook;
				if ($this->cf->isExist($sql4existBook)){
					return "Y";
					  // $this->deleteFrom($sql4DelUserSession);
				}else{	
					return "N";
				}
	}
	
	
	function update_book($tbl_book_id,$tbl_book_category_id, $tbl_language_id, $title_en, $title_ar, $author_name_en, $author_name_ar, $isbn, $description_en, $description_ar,$age_group,$book_point
	,$total_page,$book_price){
				$sql4sess = "UPDATE " . TBL_BOOKS . " SET tbl_book_category_id='$tbl_book_category_id', 
				book_language='$tbl_language_id',title_en='$title_en',title_ar='$title_ar',author_name_en='$author_name_en',author_name_ar='$author_name_ar',
				isbn='$isbn',description_en='$description_en',description_ar='$description_ar',
				age_group='$age_group',book_point='$book_point', total_page='$total_page', price ='$book_price' where tbl_book_id='$tbl_book_id'";
				//echo $sql4sess; exit;
				$this->cf->update($sql4sess);
	}
	
	function  update_file_to_book($tbl_book_id)
	{
		$qry = "SELECT * FROM ".TBL_BOOK_FILE_TEMP." WHERE tbl_book_id='$tbl_book_id'";
		$results = $this->cf->selectMultiRecords($qry);
		
		$sqlBooks = "SELECT cover_pic, book_file_name from " . TBL_BOOKS . " WHERE tbl_book_id='$tbl_book_id' "; 
		$resBooks = $this->cf->selectMultiRecords($sqlBooks);
        if(count($results)>0)
		{
			if(count($resBooks)){
				$existCoverPic 		= $resBooks[0]['cover_pic']; 
				$existBook_file_name  = $resBooks[0]['book_file_name'];
			}else{
				$existCoverPic 		= ""; 
				$existBook_file_name  = "";
			}
			
			$book_file_name =  $results[0]['book_file_name'];
			$cover_pic      =  $results[0]['cover_pic'];
			if($cover_pic<>"" && $existCoverPic<>$cover_pic)
			{
				unlink(FILE_UPLOAD_PATH."/books/".$existCoverPic);
				$sql4Update = "UPDATE " . TBL_BOOKS . " SET cover_pic='".$cover_pic."' WHERE tbl_book_id = '".$tbl_book_id."'" ;
			    $this->cf->update($sql4Update);
			}
			
			if($book_file_name<>"" && $existBook_file_name<>$book_file_name)
			{
				unlink(FILE_UPLOAD_PATH."/books/".$existBook_file_name);
				$sql4Update = "UPDATE " . TBL_BOOKS . " SET book_file_name='".$book_file_name."' WHERE tbl_book_id = '".$tbl_book_id."'" ;
			    $this->cf->update($sql4Update);
			}
			
		}
		$qry = "DELETE FROM ".TBL_BOOK_FILE_TEMP." WHERE tbl_book_id='$tbl_book_id' ";
		$this->cf->deleteFrom($qry);
		
	}
	
	
	
	/** Activate book ***/
	function activate_book($tbl_book_id) {
		$qry = "UPDATE ".TBL_BOOKS." SET is_active='Y' WHERE tbl_book_id='$tbl_book_id' ";
		$this->cf->update($qry);
	}

    /** Deactivate book ***/
	function deactivate_book($tbl_book_id) {
		$qry = "UPDATE ".TBL_BOOKS." SET is_active='N' WHERE tbl_book_id='$tbl_book_id' ";
		$this->cf->update($qry);
	}
	
	 /** Delete book ***/
	function delete_book($tbl_book_id) {
	
		/*$sqlBooks = "SELECT cover_pic, book_file_name from " . TBL_BOOKS . " WHERE tbl_book_id='$tbl_book_id' "; 
		$resBooks = $this->cf->selectMultiRecords($sqlBooks);
		if(count($resBooks)>0){
				$existCoverPic 		= $resBooks[0]['cover_pic']; 
				$existBook_file_name  = $resBooks[0]['book_file_name'];
				unlink(FILE_UPLOAD_PATH."/books/".$existCoverPic);
				unlink(FILE_UPLOAD_PATH."/books/".$existBook_file_name);
		}*/
		/*$qry = "DELETE FROM ".TBL_BOOKS." WHERE tbl_book_id='$tbl_book_id' ";
		$this->cf->deleteFrom($qry);*/
		$qry = "UPDATE ".TBL_BOOKS." SET is_active='D' WHERE tbl_book_id='$tbl_book_id' ";
		$this->cf->update($qry);
		
	}
	
	
	
	/*************** mobileapp functionality **********************/
	
	function get_recent_books($sort_name, $sort_by, $offset, $q="", $tbl_category_id,$tbl_class_id='',$lan='') {
	  
		//recently_added
		$qry = "SELECT B.*, BC.category_name_en, BC.category_name_ar   FROM ".TBL_BOOKS." AS B 
				LEFT JOIN ".TBL_BOOK_ASSIGN_CLASS." AS AC ON AC.tbl_book_id=B.tbl_book_id
				LEFT JOIN ".TBL_BOOK_CATEGORY." AS BC ON B.tbl_book_category_id=BC.tbl_book_category_id WHERE B.is_active='Y' ";
		if($tbl_category_id<>"")
		{
			$qry .= " AND BC.tbl_book_category_id= ".$tbl_category_id ;
		}
		if($tbl_class_id<>"")
		{
			$qry .= " AND AC.tbl_class_id= '".$tbl_class_id."' ";
		}
		if($lan<>"")
		{
			$qry .= " AND B.book_language = '$lan' ";
		}
		else{
			$qry .= " AND B.book_language = 'ar' ";
		}
		$qry .= " ORDER BY B.added_date DESC LIMIT 0, 10";
		$recentRes = $this->cf->selectMultiRecords($qry);
		$data[0]['books'] = $recentRes;
		for($m=0;$m<count($recentRes);$m++)
		{
			$data[0]['books'][$m]['file_path'] 			= IMG_PATH_BOOK."/".$recentRes[$m]['book_file_name'];
			$data[0]['books'][$m]['cover_pic_path']   	= IMG_PATH_BOOK."/".$recentRes[$m]['cover_pic'];
		}
	return $data;
	}
	
	
	
	function get_categorized_books($sort_name, $sort_by, $offset, $q="", $tbl_class_id='', $lan='') {
		
		$data =array();
		$qry = "SELECT * FROM ".TBL_BOOK_CATEGORY." WHERE is_active='Y' ";
		$qry .= " ORDER BY category_name_en ASC ";
		//echo $qry;
		$results = $this->cf->selectMultiRecords($qry);
		
		$data = array();
		for($y=0;$y<count($results);$y++)
		{
		    $tbl_category_id = "";
			$data[$y]['category']['category_name_en'] 		= $results[$y]['category_name_en'];
			$data[$y]['category']['category_name_ar'] 		= $results[$y]['category_name_ar'];
			$picture                                         = isset($results[$y]['picture'])? $results[$y]['picture'] : '';
			if($picture<>"")
			{
				$data[$y]['category']['category_pic'] 		= IMG_PATH_BOOK."/".$results[$y]['picture'];
			}else{
				$data[$y]['category']['category_pic']        = "";
			}
			
			$data[$y]['category']['tbl_book_category_id'] 	= $results[$y]['tbl_book_category_id'];
			
			$tbl_category_id = $results[$y]['tbl_book_category_id'];
			//recently_added
			$qry = "SELECT B.*,AC.tbl_class_id FROM ".TBL_BOOKS." AS B
					LEFT JOIN ".TBL_BOOK_ASSIGN_CLASS." AS AC ON AC.tbl_book_id=B.tbl_book_id
			 		LEFT JOIN ".TBL_BOOK_CATEGORY." AS BC ON B.tbl_book_category_id=BC.tbl_book_category_id WHERE B.is_active='Y' ";
			if($tbl_category_id<>"")
			{
				$qry .= " AND BC.tbl_book_category_id= '".$tbl_category_id."'" ;
			}
			if($tbl_class_id<>"")
			{
			$qry .= " AND AC.tbl_class_id= '".$tbl_class_id."' ";
			}
			if($lan<>"")
			{
				$qry .= " AND B.book_language = '$lan' ";
			}
			else{
				$qry .= " AND B.book_language = 'ar' ";
			}
			
			$qry .= " ORDER BY B.added_date DESC ";
			
			if($offset<>"")
			{
				$qry .= "LIMIT $offset , ".PAGING_TBL_BOOKS;
			}else{
			    $qry .= " LIMIT 0, 10 ";
			}
			
			$recentRes = $this->cf->selectMultiRecords($qry);
			for($m=0;$m<count($recentRes);$m++)
			{
				$recentRes[$m]['file_path']        = IMG_PATH_BOOK."/".$recentRes[$m]['book_file_name'];
				$recentRes[$m]['cover_pic_path']   = IMG_PATH_BOOK."/".$recentRes[$m]['cover_pic'];
			}
			
			$data[$y]['books'] =  $recentRes;
		}
		//echo $qry;
		
	return $data;
	}
	
	// Below are not used
	
	
	
	
	
	
   function get_all_active_books($sort_name, $sort_by, $offset, $q="", $tbl_category_id="",$tbl_user_id='', $lan='') {
	    $tbl_age_group_id = $this->getAgeGroup($tbl_user_id,'Y');
		$tbl_age_group_id = "";
		$qry = "SELECT B.*, BC.category_name_en, BC.category_name_ar   FROM ".TBL_BOOKS." AS B LEFT JOIN ".TBL_BOOK_CATEGORY." AS BC ON B.tbl_book_category_id=BC.tbl_book_category_id 
		        WHERE B.is_active='Y' ";
		if($tbl_category_id<>"")
		{
			$qry .= " AND BC.tbl_book_category_id= '".$tbl_category_id."' ";
		}
		if($q<>"")
		{
			$qry .= " AND (  B.title_en like  '%".$q."%' OR B.title_ar like ' %".$q."%'  OR  B.author_name_en like  '%".$q."%' OR  B.author_name_ar like  '%".$q."%' OR  B.description_en like  '%".$q."%' OR  B.description_ar like  '%".$q."%' OR   BC.category_name_en like  '%".$q."%' OR  BC.category_name_ar like '%".$q."%' ) " ;
		}
		
		if($tbl_age_group_id<>"")
		{
			$qry .= " AND ((B.age_group='$tbl_age_group_id') OR (B.age_group LIKE '$tbl_age_group_id|%') OR (B.age_group LIKE '%|$tbl_age_group_id') OR (B.age_group LIKE '%|$tbl_age_group_id|%')) ";
		}
		if($lan<>"")
		{
			$qry .= " AND B.book_language = '$lan' ";
		}
		else{
			$qry .= " AND B.book_language = 'ar' ";
		}
		
		$qry .= " ORDER BY B.added_date DESC ";
		
		$qry .= "LIMIT $offset , ".PAGING_TBL_BOOKS;
		//echo $qry;
		$results = $this->cf->selectMultiRecords($qry);
		
		for($m=0;$m<count($results);$m++)
		{
			$results[$m]['file_path'] = HOST_URL."/uploads/books/".$results[$m]['book_file_name'];
			$results[$m]['cover_pic_path']   = HOST_URL."/uploads/books/".$results[$m]['cover_pic'];
			
			$tbl_book_id = $results[$m]['tbl_book_id'];
			$results[$m]['is_wish_list'] 		= $this->isWishList($tbl_book_id,$tbl_user_id);
			$results[$m]['is_favourite'] 		= $this->isFavourite($tbl_book_id,$tbl_user_id);
			$results[$m]['is_read']      		 = $this->isRead($tbl_book_id,$tbl_user_id);
			//$results[0]['books'][$m]['cnt_quiz_attended']   = $this->cntQuizAttended($tbl_book_id,$tbl_user_id);
			$cntQuizAttended 								=  $this->quizHistory($tbl_book_id,$tbl_user_id);
			$results[$m]['cnt_quiz_attended']   = isset($cntQuizAttended[0]['cntAttended'])? $cntQuizAttended[0]['cntAttended']:'0';
			$results[$m]['cnt_quiz_success']    = isset($cntQuizAttended[0]['cntQuizSuccess'])? $cntQuizAttended[0]['cntQuizSuccess']:'0';
			$results[$m]['cnt_quiz_failed']     = isset($cntQuizAttended[0]['cntQuizFailed'])? $cntQuizAttended[0]['cntQuizFailed']:'0' ;
			
			$dataRatings = $this->getUserRatings($tbl_book_id,'');
			$earnedRatings  = 0;
			$totalViews     = 0;
			$totalRatings   = 0;
			if($dataRatings[0]['cnt_view']>0)
			{	   
				$earnedRatings    = $dataRatings[0]['total_rating'];
				$totalRatings     = $dataRatings[0]['cnt_view'] * 5;
				$ratePercentage   = ($earnedRatings / $totalRatings) * 100 ;
			}else{
				$ratePercentage   = 0;
			}
			$results[$m]['ratePercentage'] = $ratePercentage;
			
				
		}
	return $results;
	}
	
	
	
	function get_total_active_books($q,$tbl_category_id='',$tbl_user_id='',$lan='') {
		 $tbl_age_group_id = $this->getAgeGroup($tbl_user_id,'Y');
		$qry = "SELECT B.tbl_book_id FROM ".TBL_BOOKS." AS B LEFT JOIN ".TBL_BOOK_CATEGORY." AS BC ON B.tbl_book_category_id=BC.tbl_book_category_id 
		        WHERE B.is_active='Y' ";
		if($tbl_category_id<>"")
		{
			$qry .= " AND BC.tbl_book_category_id= '".$tbl_category_id."' ";
		}
		if($q<>"")
		{
			$qry .= " AND (  B.title_en like  '%".$q."%' OR B.title_ar like ' %".$q."%'  OR  B.author_name_en like  '%".$q."%' OR  B.author_name_ar like  '%".$q."%' OR  B.description_en like  '%".$q."%' OR  B.description_ar like  '%".$q."%' OR   BC.category_name_en like  '%".$q."%' OR  BC.category_name_ar like '%".$q."%' ) " ;
		}
		if($tbl_age_group_id<>"")
		{
			$qry .= " AND ((B.age_group='$tbl_age_group_id') OR (B.age_group LIKE '$tbl_age_group_id|%') OR (B.age_group LIKE '%|$tbl_age_group_id') OR (B.age_group LIKE '%|$tbl_age_group_id|%')) ";
		}
		if($lan<>"")
		{
			$qry .= " AND B.book_language = '$lan' ";
		}
		else{
			$qry .= " AND B.book_language = 'ar' ";
		}
		
		//echo $qry;
		$results = $this->cf->selectMultiRecords($qry);
	return count($results);
	}
	
	
		//mobileapp
	
	
	function quizHistory($tbl_book_id,$tbl_user_id)
	{
		$qrySel = "SELECT count(tbl_book_id) as cntAttended, SUM(point>'0') as cntQuizSuccess, SUM(point = 0 or point='' ) as cntQuizFailed  from " . TBL_BOOK_USER_VIEWS . "  where tbl_book_id='$tbl_book_id' AND tbl_user_id='$tbl_user_id'  AND quiz_attended='Y' ";
		$recentRes = $this->cf->selectMultiRecords($qrySel);
	    return $recentRes;
	}
	
	
	function getAgeGroup($tbl_user_id='',$is_active='')
	{
		$tbl_age_group_id ="";
		$qry = "SELECT dob_month, dob_day, dob_year FROM ".TBL_USER." AS U WHERE 1=1 ";
		if($is_active<>""){
			$qry .= " AND U.is_active='Y' ";
		}
		if($tbl_user_id<>""){
			$qry .= " AND U.tbl_user_id='".$tbl_user_id."' ";
		}
		$results = $this->cf->selectMultiRecords($qry);
		$dob      = $results[0]['dob_year']."-".$results[0]['dob_month']."-".$results[0]['dob_day'];
		$currDate = date("Y-m-d");
		$age = $this->dateDifference($dob,$currDate);
		if($age<>"")
		{
			$results = $this->getGroupId($age);
			$tbl_age_group_id	=	$results[0]['tbl_age_group_id'];
		}
		return $tbl_age_group_id;
	}
	
	
	
	function getGroupId($age)
	{
		$qry = "SELECT tbl_age_group_id FROM ".TBL_AGE_GROUP." WHERE is_active='Y' ";
		if($age<>""){
			$qry .= " AND $age BETWEEN `age_from` AND `age_to` ";
		}
		$results = $this->cf->selectMultiRecords($qry);
		return $results;
	}
	
	function dateDifference($date_1 , $date_2 , $differenceFormat = '%y' )
	{
		$datetime1 = date_create($date_1);
		$datetime2 = date_create($date_2);
		$interval = date_diff($datetime1, $datetime2);
		return $interval->format($differenceFormat);
	}
	
	
	
	//click on read book button action
	function user_book_read($tbl_user_view_id, $tbl_user_id, $tbl_book_id, $view_date_time, $total_chapter, $read_chapter)
	{
	 		$qrySel = "SELECT * FROM ".TBL_BOOK_USER_VIEWS."  WHERE tbl_book_id='$tbl_book_id' and tbl_user_id='$tbl_user_id' ";
			$recentRes = $this->cf->selectMultiRecords($qrySel);
			$cntView = count($recentRes); 
			if($cntView==0){
				// First Time View Of a Book - Point is there
				$qry = "SELECT * FROM ".TBL_COMMON_SETTINGS." ";
				$results = $this->cf->selectMultiRecords($qry);
				if(count($results)>0){
					$point   = isset($results[0]['read_point_inside_app'])? $results[0]['read_point_inside_app']:'0' ; 
				}else{
					$point   = 0;
				}
				$sql4sess = "INSERT INTO " . TBL_BOOK_USER_VIEWS . "(tbl_user_view_id, tbl_user_id, tbl_book_id, view_date_time, point, total_chapter, read_chapter)  
							 VALUES('$tbl_user_view_id','$tbl_user_id','$tbl_book_id','$view_date_time','$point','$total_chapter','$read_chapter')";
				$this->cf->insertInto($sql4sess);
				
				return "Y";
				
			}else{
				//Second Time View of a Book - No Point
			
				$sql4sess = "INSERT INTO " . TBL_BOOK_USER_VIEWS . "(tbl_user_view_id, tbl_user_id, tbl_book_id, view_date_time, total_chapter, read_chapter)  
							 VALUES('$tbl_user_view_id','$tbl_user_id','$tbl_book_id','$view_date_time','$total_chapter','$read_chapter')";
				$this->cf->insertInto($sql4sess);
				
				return "N";
			}
			
	}
	
	
	function update_my_package_point($tbl_user_view_id, $tbl_user_id,$tbl_age_group_id)
	{
	
			$qry = "SELECT BP.tbl_book_package_id,BP.point,BP.package_name_en,BP.package_name_ar FROM ".TBL_BOOK_PACKAGE." AS BP 
			        WHERE BP.is_active='Y' ";
			if($tbl_age_group_id<>"")
			{
			 $qry .= " AND ((BP.age_group='$tbl_age_group_id') OR (BP.age_group LIKE '$tbl_age_group_id|%') OR (BP.age_group LIKE '%|$tbl_age_group_id') OR (BP.age_group LIKE '%|$tbl_age_group_id|%'))";
			}
			$results = $this->cf->selectMultiRecords($qry);
			if(count($results)>0){
				
				for($d=0;$d<count($results);$d++)
				{
					$tbl_book_package_id = $results[$d]['tbl_book_package_id'];
					$book_point          = $results[$d]['point'];
					$package_name_en     = $results[$d]['package_name_en'];
					$package_name_ar     = $results[$d]['package_name_ar'];
					
					$existPackagePoint   = $this->existPackagePoint($tbl_book_package_id,$tbl_user_id);
						
						if($existPackagePoint == 0 )
						{
							$qryBook = "SELECT B.tbl_book_id FROM ".TBL_BOOKS." AS B LEFT JOIN ".TBL_PACKAGE_ASSIGNED_BOOKS." AS PAB ON PAB.tbl_book_id=B.tbl_book_id
							WHERE B.is_active='Y' ";
								if($tbl_book_package_id<>"")
								{
									$qryBook .= " AND PAB.tbl_book_package_id= '".$tbl_book_package_id."'" ;
								}
						         $resultsBook = $this->cf->selectMultiRecords($qryBook);
								
								$book_missing = 0;
								for($c=0;$c<count($resultsBook);$c++)
								{
									
									$tbl_book_id =  $resultsBook[$c]['tbl_book_id'];
									$qrySelBook  = "SELECT tbl_user_view_id FROM ".TBL_BOOK_USER_VIEWS."  WHERE tbl_book_id='$tbl_book_id' and tbl_user_id='$tbl_user_id' ";
									$recentBook = $this->cf->selectMultiRecords($qrySelBook);
									if(count($recentBook)==0)
									{
										$book_missing = 1;
									}
								}
						
								
								if($book_missing<>'1')
								{
									$qryUpdate = "UPDATE ".TBL_BOOK_USER_VIEWS." SET package_point ='$book_point', tbl_book_package_id = '$tbl_book_package_id'  
												  WHERE tbl_user_view_id='$tbl_user_view_id' and tbl_user_id='$tbl_user_id' ";
									$this->cf->update($qryUpdate);
									
									$this->create_package_certificate($tbl_user_id,$package_name_en,$package_name_ar,$tbl_book_package_id);
									
									
									
									
								}
					   } //if condition
					
				} //for condition
				
				
			}//if loop
	}
	
	function existPackagePoint($tbl_book_package_id,$tbl_user_id)
	{
		$qrySel = "SELECT tbl_user_view_id FROM ".TBL_BOOK_USER_VIEWS."  WHERE tbl_book_package_id='$tbl_book_package_id' and tbl_user_id='$tbl_user_id' ";
		$recentRes = $this->cf->selectMultiRecords($qrySel);
		return count($recentRes); 
	}
	
	
	/***************** CREATE PACKAGE CERTIFICATE ********************/
    function create_package_certificate($tbl_user_id,$package_name_en,$package_name_ar,$tbl_book_package_id){
		
		$tbl_user_rewards_id    = substr(md5(rand()),0,15);
		$tbl_reward_settings_id = "";
		$add_user_rewards       = $this->add_user_rewards_package($tbl_user_rewards_id, $tbl_user_id, $tbl_book_package_id);
				
		$qrySel = "SELECT U.first_name, U.last_name, U.picture,U.id,U.emirates  FROM ".TBL_USER." AS U WHERE tbl_user_id='$tbl_user_id' ";
		$recentRes = $this->cf->selectMultiRecords($qrySel);
		$name        =  $recentRes[0]['first_name']." ".$recentRes[0]['last_name'];
		$roll_number =  $recentRes[0]['id'];
						  
		$message    = "Congratulations for the completion of package '".$package_name_en."' in the BOOK & BOOK .";
		$message .= " Regards,";
		$message .= " BOOK & BOOK";
	    // SAVE AS MESSAGE
		$message_from   = "BOOK & BOOK"; //super admin
		$message_to     = $tbl_user_id;
		$tbl_user_id    = $message_to;
		$message_type   = "message";
		$this->save_message($message_from, $message_to, $tbl_user_id, $message_type, $message);
		
		
		if($recentRes[0]["picture"]<>""){
			$picture     =  UPLOAD_URL."/users/".$recentRes[0]["picture"];
	     }
		 $emirates  =   $recentRes[0]['emirates'];
		 
		 $color = "#bda077";
		 $layer = "Layer1"; 
		 
		 if($emirates=="Abudhabi")
		 {
			$signature ="signature-abudhabi.png"; 
		 }else if($emirates=="Dubai")
		 {
			 $signature ="signature-dubai.png"; 
		 } else if($emirates=="Sharja")
		 {
			 $signature ="signature-sharjah.png"; 
			 
		 } else if($emirates=="Ajman")
		 {
			$signature ="signature-ajman.png";  
			 
		 }else if($emirates=="Fujairah")
		 {
			 $signature ="signature-fujairah.png"; 
			 
		 }else if($emirates=="Rasalkhaimah")
		 {
			$signature ="signature-rasalkhaima.png";  
			 
		 }else if($emirates=="Ummalquwain")
		 {
			$signature ="signature-ummalquwain.png";  
			 
		 }else{
			$signature ="signature-uae.png";  
		 }
		 
		 $signature = UPLOAD_URL.'/signatures/'.$signature;
		 
		 $certificate_date = date("j F, Y");
		 
		 $content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
					<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr">
						<head>
							<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
							<title>BOOK & BOOK certificate</title>
							<link href="'.CSS_PATH.'/certificate.css" rel="stylesheet" type="text/css">
						</head>
						<body dir="ltr">
						<table align="left">
						<tr><td>
							<div id="background">
								<div id="Layer8copy9"><img src="'.IMG_PATH.'/certificate/Layer8copy9.png"></div>
								<div id="LayerLogo" ><span><img src="'.IMG_PATH.'/certificate/ministry-logo-abudhabi.png" width="150" height="150" /></span></div>
								<div id="LayerMain"><span><strong>شهادة شكر و تقدير</strong></span></div>
								<div id="Layer33"><span><strong>CERTIFICATE OF APPRECIATION</strong></span></div>
								<div id="LayerContentLeft" ><span style="color:#9d9d9d; font-weight:bold;">Reg. No. : '.$roll_number.'</span><img src="'.$picture.'" width="220" height="250" style="border:1px solid '.$color.'; border-radius:5px; margin-top:5px;" /></div>
								<div id="LayerContentRight">
									<div id="LayerContent1" ><span><strong>This certificate is awarded to</strong></span></div>
									<div id="LayerContentName"  style="color:'.$color.';"><span><strong>'.$name.'</strong></span></div>
									<div id="LayerContent2" ><span><strong>for the completion of package </strong></br>
									<strong style="color:#bda077;">'.$package_name_en.'</strong></br></span></div>
									<div id="LayerContent3" ><span><strong>Certified UAE Ministry of Interior</strong></span></div>
									<div id="LayerContent4" ><span><strong>Presented '.$certificate_date.'</strong></span></div>
								</div>
								<div style="clear:both;">&nbsp;</div>
								<div id="LayerBottom">
								    <div id="LayerBottomLeft1" ><span><strong>MOI</strong></span></div>
								    <div id="LayerBottomLeft2" ><span><img src="'.$signature.'" /></span></div>
								</div>
								
								<div style="clear:both;"></div><div id="'.$layer.'"><img src="'.IMG_PATH.'/certificate/'.$layer.'.png" ></div>
							</div>
						</td></tr>
					</table>
					 </body>
					 </html>';
				$fp = fopen(FILE_UPLOAD_PATH."/certificates/".$tbl_user_rewards_id.".html","w+");
				fwrite($fp,$content);
				fclose($fp);
				
				
			$output = ob_get_clean( );
			$html = $output;
			$mpdf = new mPDF('utf-8'); 
			$pdf_path =FILE_UPLOAD_PATH."/certificates/".$tbl_user_rewards_id.".pdf";
			//$mpdf = new mPDF('ar','A4-L','','',32,25,27,25,16,13); 
			$mpdf = new mPDF('ar','A4-L','','',0,10,0,0,16,13); 
			$mpdf->SetDirectionality('ltr');
			$mpdf->mirrorMargins = true;
			$mpdf->WriteHTML($content);
			$mpdf->Output($pdf_path);
			return;
			
			
	}
	
	//add user rewards
    function add_user_rewards_package($tbl_user_rewards_id, $tbl_user_id, $tbl_book_package_id)
	{
			$sql4sess = "INSERT INTO " . TBL_USER_REWARDS . "(tbl_user_rewards_id,tbl_user_id,tbl_book_package_id,reward_date)  
			             VALUES('$tbl_user_rewards_id','$tbl_user_id','$tbl_book_package_id',NOW())";
			$sql4rewardExist = "SELECT tbl_user_rewards_id from " . TBL_USER_REWARDS . " where tbl_user_id='$tbl_user_id' and tbl_reward_settings_id='$tbl_reward_settings_id' ";
			if ($this->cf->isExist($sql4rewardExist)){		 
				return "N";
			}else{ 
				$this->cf->insertInto($sql4sess);
				return "Y";
			}
	}
	function save_message($message_from, $message_to, $tbl_user_id, $message_type, $message) {
		    $tbl_message_id = substr(md5(uniqid(rand())),0,10);
			$message = addslashes($message);
			$qry = "INSERT INTO ".TBL_MESSAGE." (`tbl_message_id`, `message_from`, `message_to`, `tbl_user_id`, `message_type`, `message`, `added_date`)
					VALUES ('$tbl_message_id', '$message_from', '$message_to', '$tbl_user_id', '$message_type', '$message', NOW()) ";
			//echo $qry;
			$this->cf->insertInto($qry);
			return "Y";
			
		//}
	}
	/****************  END PACKAGE CERTIFICATE    *******************/
	
	
    function user_book_read_outside_app($tbl_user_view_id, $tbl_user_id, $tbl_book_id)
	{
	 		$qrySel = "SELECT * FROM ".TBL_BOOK_USER_VIEWS."  WHERE tbl_book_id='$tbl_book_id' and tbl_user_id='$tbl_user_id' ";
			$recentRes = $this->cf->selectMultiRecords($qrySel);
			$cntView = count($recentRes); 
			if($cntView==0){
				
				$qry = "SELECT * FROM ".TBL_COMMON_SETTINGS." ";
				$results = $this->cf->selectMultiRecords($qry);
				if(count($results)>0){
					$point   = isset($results[0]['read_point_out_of_app'])? $results[0]['read_point_out_of_app']:'0' ; 
				}else{
					$point   = 0;
				}
				$sql4sess = "INSERT INTO " . TBL_BOOK_USER_VIEWS . "(tbl_user_view_id, tbl_user_id, tbl_book_id, point)  
							 VALUES('$tbl_user_view_id','$tbl_user_id','$tbl_book_id','$point')";
				$this->cf->insertInto($sql4sess);
				return "Y";
			}else{
				return "N";
				
			}
			
	}
	
	
	
	
	
	
	//user book rating
	function user_book_rating($tbl_book_rating_id, $tbl_user_id, $tbl_book_id, $tbl_book_rating, $rating_date_time)
	{
	 		
			
			$sql4sess = "INSERT INTO " . TBL_BOOK_USER_RATINGS . "(tbl_book_rating_id, tbl_user_id, tbl_book_id, book_rating, rating_date_time)  
			             VALUES('$tbl_book_rating_id','$tbl_user_id','$tbl_book_id','$tbl_book_rating','$rating_date_time')";
			$this->cf->insertInto($sql4sess);
			return "Y";
	}	
	
	//add user book reviews
    function add_user_book_reviews($tbl_book_review_id, $tbl_book_id, $tbl_user_id, $review_comments)
	{
	 		$sql4sess = "INSERT INTO " . TBL_BOOK_REVIEWS . "(tbl_book_review_id,tbl_book_id,tbl_user_id,review_comments,is_active,added_date)  
			             VALUES('$tbl_book_review_id','$tbl_book_id','$tbl_user_id','$review_comments','Y',NOW())";
			$this->cf->insertInto($sql4sess);
			return "Y";
	}
	
	//user book selected as favourite after read, is_read= "Y" ***** user book selected as read later - not read, is_read= "N"  
	function add_book_favourite($tbl_user_favourite_id, $tbl_user_id, $tbl_book_id, $is_read)
	{
	 		$sql4sess = "INSERT INTO " . TBL_BOOK_FAVOURITE . "(tbl_user_favourite_id, tbl_user_id, tbl_book_id, is_read)  
			             VALUES('$tbl_user_favourite_id','$tbl_user_id','$tbl_book_id', '$is_read')";
						 
		    $sql4userSession = "SELECT tbl_user_favourite_id from " . TBL_BOOK_FAVOURITE . " where tbl_book_id='$tbl_book_id' and tbl_user_id='$tbl_user_id' and is_read='$is_read' ";
			if ($this->cf->isExist($sql4userSession)){
					return "N";
			}else{ 
               if($is_read=="Y")
			   {
				  $sql4userLater = "SELECT tbl_user_favourite_id from " . TBL_BOOK_FAVOURITE . " where tbl_book_id='$tbl_book_id' and tbl_user_id='$tbl_user_id' and is_read='N' ";
				  if ($this->cf->isExist($sql4userLater)){
				 	 $qry = "UPDATE ".TBL_BOOK_FAVOURITE." SET is_read='$is_read' WHERE tbl_book_id='$tbl_book_id' and tbl_user_id='$tbl_user_id' ";
					 $this->cf->update($qry);
				  }else{
					 $this->cf->insertInto($sql4sess); 
				  }
				   
			   } else if($is_read=="N"){
			        $sql4userLater = "SELECT tbl_user_favourite_id from " . TBL_BOOK_FAVOURITE . " where tbl_book_id='$tbl_book_id' and tbl_user_id='$tbl_user_id' and is_read='Y' ";
				    if ($this->cf->isExist($sql4userLater)){
						return "N";
					}else{
			     		$this->cf->insertInto($sql4sess);
					}
			   }
			return "Y";
			}
	}
	
	// update user points, quiz attended, spending time after reading	
	function update_user_book_read($tbl_user_view_id, $tbl_user_id, $tbl_book_id, $spending_time,$quiz_attended,$point,$is_active,$total_chapter,$read_chapter)
	{
		   $cntViews = 0;
		   if($tbl_user_view_id<>"")
			{
				$qrySel = "SELECT SUM(point) as prevPoint, COUNT(tbl_book_id) as cntViews FROM ".TBL_BOOK_USER_VIEWS."  WHERE tbl_book_id='$tbl_book_id' and tbl_user_id='$tbl_user_id' AND tbl_user_view_id='$tbl_user_view_id' ";
			    $recentRes = $this->cf->selectMultiRecords($qrySel);
				$cntViews  = $recentRes[0]['cntViews'];
				$prevPoint  = $recentRes[0]['prevPoint'];
			}
			if($cntViews>0){
				$point = $point + $prevPoint;
				$qry = "UPDATE ".TBL_BOOK_USER_VIEWS." SET spending_time='$spending_time',quiz_attended='$quiz_attended',point='$point',is_active='Y',read_chapter='$read_chapter' WHERE 
				tbl_book_id='$tbl_book_id' and tbl_user_id='$tbl_user_id' ";
				if($tbl_user_view_id<>"")
				{
					$qry .= " AND tbl_user_view_id='$tbl_user_view_id' ";
				}
				$this->cf->update($qry);
				return "Y";
			}else{
				$tbl_user_view_id     = substr(md5(rand()),0,15);
				/*$sql4sess = "INSERT INTO " . TBL_BOOK_USER_VIEWS . "(tbl_user_view_id, tbl_user_id, tbl_book_id, view_date_time,spending_time,quiz_attended,point,is_active)  
			             VALUES('$tbl_user_view_id','$tbl_user_id','$tbl_book_id',NOW(),'$spending_time','$quiz_attended','$point','Y')";
				$this->cf->insertInto($sql4sess);*/
				$sql4sess = "INSERT INTO " . TBL_BOOK_USER_VIEWS . "(tbl_user_view_id, tbl_user_id, tbl_book_id, spending_time,quiz_attended,point,is_active)  
			             VALUES('$tbl_user_view_id','$tbl_user_id','$tbl_book_id','$spending_time','$quiz_attended','$point','Y')";
				$this->cf->insertInto($sql4sess);
			    return "Y";
			}
	}
	
	
	function list_favourite_books($tbl_user_id, $is_read,$lan='')
	{
	 	$qrySel = "SELECT B.* from " . TBL_BOOK_FAVOURITE . " AS BV left join " . TBL_BOOKS. "  AS B ON B.tbl_book_id=BV.tbl_book_id where BV.tbl_user_id='$tbl_user_id' and BV.is_read='$is_read' ";
		if($lan<>"")
		{
			$qrySel .= " AND B.book_language= '$lan' ";
		}
		$recentRes = $this->cf->selectMultiRecords($qrySel);
		
		for($m=0;$m<count($recentRes);$m++)
		{
			$recentRes[$m]['file_path']        = HOST_URL."/uploads/books/".$recentRes[$m]['book_file_name'];
			$recentRes[$m]['cover_pic_path']   = HOST_URL."/uploads/books/".$recentRes[$m]['cover_pic'];
		}
		return $recentRes;
	}
	
	function isWishList($tbl_book_id,$tbl_user_id)
	{
		$qrySel = "SELECT tbl_book_id from " . TBL_BOOK_FAVOURITE . " where is_read='N' AND tbl_book_id='$tbl_book_id' AND tbl_user_id='$tbl_user_id' ";
		$recentRes = $this->cf->selectMultiRecords($qrySel);
	    if(count($recentRes))
		{
			return "Y";
		}else{
			return "N";
		}
	}
	
	function isFavourite($tbl_book_id,$tbl_user_id)
	{
		 $qrySel = "SELECT tbl_book_id from " . TBL_BOOK_FAVOURITE . " where is_read='Y' AND tbl_book_id='$tbl_book_id' AND tbl_user_id='$tbl_user_id' ";
		$recentRes = $this->cf->selectMultiRecords($qrySel);
	    if(count($recentRes))
		{
			return "Y";
		}else{
			return "N";
		}
	}
	
	function isRead($tbl_book_id,$tbl_user_id)
	{
		$qrySel = "SELECT tbl_book_id from " . TBL_BOOK_USER_VIEWS . "  where tbl_book_id='$tbl_book_id' AND tbl_user_id='$tbl_user_id' ";
		$recentRes = $this->cf->selectMultiRecords($qrySel);
	    if(count($recentRes))
		{
			return "Y";
		}else{
			return "N";
		}
	}
	function cntQuizAttended($tbl_book_id,$tbl_user_id)
	{
		$qrySel = "SELECT count(tbl_book_id) as cntAttended from " . TBL_BOOK_USER_VIEWS . "  where tbl_book_id='$tbl_book_id' AND tbl_user_id='$tbl_user_id'  AND quiz_attended='Y' ";
		$recentRes = $this->cf->selectMultiRecords($qrySel);
	    return $recentRes[0]['cntAttended'];
	}
	
	
	function getForumGroup($tbl_user_id='',$is_active='')
	{
		$tbl_age_group_id ="";
		$qry = "SELECT tbl_age_group_id FROM ".TBL_USER_FORUM_GROUP." AS FG WHERE 1=1 ";
		if($is_active<>""){
			$qry .= " AND FG.is_active='Y' ";
		}
		if($tbl_user_id<>""){
			$qry .= " AND FG.tbl_user_id='".$tbl_user_id."' ";
		}
		$results = $this->cf->selectMultiRecords($qry);
		if(count($results)>0){
			$tbl_age_group_id	=	$results[0]['tbl_age_group_id'];
		}else{
			$tbl_age_group_id	=    "";
		}
		return $tbl_age_group_id;
	}
		
	function readBooks($tbl_user_id,$lan='')
	{
		$qrySel = "SELECT BUV.tbl_book_id,SUM(BUV.point) as totalPoint,SUM(BUV.spending_time) as totalSpent,SUM(BUV.quiz_attended = 'Y') AS attended_count
		,SUM(BUV.quiz_attended = 'N') AS not_attended_count,SUM(BUV.quiz_attended = 'Y' AND BUV.point='0' ) AS attended_failed_count,SUM(BUV.quiz_attended = 'Y' AND BUV.point>0 ) AS attended_success_count, BUV.view_date_time as view_date_time , B.*  from " . TBL_BOOK_USER_VIEWS . " AS BUV LEFT JOIN ".TBL_BOOKS." AS B ON B.tbl_book_id=BUV.tbl_book_id where tbl_user_id='$tbl_user_id' ";
		if($lan<>"")
		{
			$qrySel .= " AND B.book_language='$lan' ";
		}
		
		$qrySel .= "  GROUP BY BUV.tbl_book_id ORDER BY id desc";
		$recentRes = $this->cf->selectMultiRecords($qrySel);
	    return $recentRes;
	}	
	
	
	function finishedPackageList($tbl_user_id)
	{
		$qrySel = "SELECT BUV.tbl_book_package_id,BUV.package_point as totalPoint, BUV.view_date_time as view_date_time , BP.*  from " . TBL_BOOK_USER_VIEWS . " AS BUV LEFT JOIN ".TBL_BOOK_PACKAGE." AS BP ON BP.tbl_book_package_id=BUV.tbl_book_package_id where BUV.tbl_user_id='$tbl_user_id' AND BUV.package_point <> '' ";
		$qrySel .= " ORDER BY BUV.id desc";
		$recentRes = $this->cf->selectMultiRecords($qrySel);
	    return $recentRes;
	}	
	
	function get_book_assigned_age_group($tbl_book_id)
	{
		$qry = "SELECT age_group FROM ".TBL_BOOKS." WHERE tbl_book_id='$tbl_book_id' and is_active='Y' ";
		$recentRes = $this->cf->selectMultiRecords($qry);
	    return $recentRes[0]['age_group'];
	}
	
	
	
	function get_book_details($q="", $tbl_book_id,$tbl_user_id='',$lan='') {
	    $tbl_age_group_id = $this->getAgeGroup($tbl_user_id,'Y');
		//recently_added
		$qry = "SELECT B.*, BC.category_name_en, BC.category_name_ar   FROM ".TBL_BOOKS." AS B LEFT JOIN ".TBL_BOOK_CATEGORY." AS BC ON B.tbl_book_category_id=BC.tbl_book_category_id WHERE B.is_active='Y' ";
		if($tbl_book_id<>"")
		{
			$qry .= " AND B.tbl_book_id= '".$tbl_book_id."'" ;
		}
		if($lan<>"")
		{
			$qry .= " AND B.book_language = '$lan' ";
		}
		else{
			$qry .= " AND B.book_language = 'ar' ";
		}
		$recentRes = $this->cf->selectMultiRecords($qry);
		$data = $recentRes[0];
		for($m=0;$m<count($recentRes);$m++)
		{
			$data['file_path'] = HOST_URL."/uploads/books/".$recentRes[$m]['book_file_name'];
			$data['cover_pic_path']   = HOST_URL."/uploads/books/".$recentRes[$m]['cover_pic'];
			
			$tbl_book_id = $recentRes[$m]['tbl_book_id'];
			$data['is_wish_list'] 		= $this->isWishList($tbl_book_id,$tbl_user_id);
			$data['is_favourite'] 		= $this->isFavourite($tbl_book_id,$tbl_user_id);
			$data['is_read']      		 = $this->isRead($tbl_book_id,$tbl_user_id);
			$cntQuizAttended =  $this->quizHistory($tbl_book_id,$tbl_user_id);
			$data['cnt_quiz_attended']   = isset($cntQuizAttended[0]['cntAttended'])? $cntQuizAttended[0]['cntAttended']:'0' ;
			$data['cnt_quiz_success']    = isset($cntQuizAttended[0]['cntQuizSuccess'])? $cntQuizAttended[0]['cntQuizSuccess']:'0' ;
			$data['cnt_quiz_failed']     = isset($cntQuizAttended[0]['cntQuizFailed'])? $cntQuizAttended[0]['cntQuizFailed']:'0' ;
			
			$dataRatings = $this->getUserRatings($tbl_book_id,'');
			$earnedRatings  = 0;
			$totalViews     = 0;
			$totalRatings   = 0;
			if($dataRatings[0]['cnt_view']>0)
			{	   
				$earnedRatings    = $dataRatings[0]['total_rating'];
				$totalRatings     = $dataRatings[0]['cnt_view'] * 5;
				$ratePercentage   = ($earnedRatings / $totalRatings) * 100 ;
			}else{
				$ratePercentage   = 0;
			}
			$data['ratePercentage'] = $ratePercentage;
			
		}
	return $data;
	}
	
	
	function get_book_by_isbn($isbn, $tbl_book_id) {
		//recently_added
		$qry = "SELECT B.tbl_book_id  FROM ".TBL_BOOKS." AS B WHERE B.is_active='Y' ";
		if($tbl_book_id<>"")
		{
			$qry .= " AND B.tbl_book_id= '".$tbl_book_id."'" ;
		}
		if($isbn<>"")
		{
			$qry .= " AND B.isbn = '".$isbn."' ";
		}
	
		$recentRes = $this->cf->selectMultiRecords($qry);
	    return $recentRes;
	}
	
	
	
   /************************ Child Books ***************************/
   
   function get_all_my_books($sort_name, $sort_by, $offset, $q="", $tbl_category_id="", $is_active='',$tbl_class_id,$tbl_school_id) {
	   
		$qry = "SELECT B.*, BC.category_name_en, BC.category_name_ar  FROM ".TBL_BOOKS." AS B 
		        INNER JOIN ".TBL_BOOK_ASSIGN_CLASS." AS AC ON AC.tbl_book_id=B.tbl_book_id
		        LEFT JOIN ".TBL_BOOK_CATEGORY." AS BC ON B.tbl_book_category_id=BC.tbl_book_category_id
				WHERE 1= 1  ";
		if($is_active<>"")
		{
			$qry .= " AND B.is_active='Y' ";
		}
		
		$qry .= " AND B.is_active <>'D' ";
		
		if($tbl_category_id<>"")
		{
			$qry .= " AND BC.tbl_book_category_id= '".$tbl_category_id."' ";
		}
		if($q<>"")
		{
			$qry .= " AND (  B.title_en like  '%".$q."%' OR B.title_ar like ' %".$q."%'  OR  B.author_name_en like  '%".$q."%' OR  B.author_name_ar like  '%".$q."%' OR  B.description_en like  '%".$q."%' OR  B.description_ar like  '%".$q."%' OR   BC.category_name_en like  '%".$q."%' OR  BC.category_name_ar like '%".$q."%' OR  B.book_language like  '%".$q."%'  ) " ;
		}
		
		if($tbl_class_id<>"")
		{
			$qry .= " AND AC.tbl_class_id= '".$tbl_class_id."' ";
		}
		
		if($tbl_category_id<>"")
		{
			$qry .= " AND AC.tbl_school_id= '".$tbl_school_id."' ";
		}
		
		
		$qry .= " GROUP BY B.tbl_book_id ";
		if($sort_name<>"")
		{
			 $qry .= " ORDER BY B.$sort_name $sort_by ";
		}else{
			$qry .= " ORDER BY B.title_en ASC, B.added_date ASC ";
		}
		
		$qry .= "LIMIT $offset , ".PAGING_TBL_BOOKS;
		$results = $this->cf->selectMultiRecords($qry);
	return $results;
	}
	
	
	function get_all_my_total_books($q,$tbl_category_id='',$is_active='',$tbl_class_id,$tbl_school_id) {
		$qry = "SELECT B.tbl_book_id FROM ".TBL_BOOKS." AS B 
		INNER JOIN ".TBL_BOOK_ASSIGN_CLASS." AS AC ON AC.tbl_book_id=B.tbl_book_id
		LEFT JOIN ".TBL_BOOK_CATEGORY." AS BC ON B.tbl_book_category_id=BC.tbl_book_category_id
		
		WHERE 1 = 1  ";
		if($is_active<>"")
		{
			$qry .= " AND B.is_active='Y' ";
		}
		
		$qry .= " AND B.is_active <>'D' ";
		
		if($q<>"")
		{
			$qry .= " AND (  B.title_en like  '%".$q."%' OR B.title_ar like ' %".$q."%'  OR  B.author_name_en like  '%".$q."%' OR  B.author_name_ar like  '%".$q."%' OR  B.description_en like  '%".$q."%' OR  B.description_ar like  '%".$q."%' OR   BC.category_name_en like  '%".$q."%' OR  BC.category_name_ar like '%".$q."%' OR  B.book_language like  '%".$q."%'  ) " ;
		}
		
		if($tbl_category_id<>"")
		{
			$qry .= " AND BC.tbl_book_category_id= '".$tbl_category_id."' " ;
		}
		if($tbl_class_id<>"")
		{
			$qry .= " AND AC.tbl_class_id= '".$tbl_class_id."' ";
		}
		
		if($tbl_category_id<>"")
		{
			$qry .= " AND AC.tbl_school_id= '".$tbl_school_id."' ";
		}
		
		
		
		$qry .= " GROUP BY B.tbl_book_id ";
		$qry .= " ORDER BY B.title_en ASC, B.added_date ASC ";
		
		//echo $qry;
		$results = $this->cf->selectMultiRecords($qry);
	return count($results);
	}	

	
	
	
}
?>