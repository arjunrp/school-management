<?php
include_once('include/common_functions.php');
/**
 * @desc   	  	Config Model
 * @category   	Model
 * @author     	Shanavas PK
 * @version    	0.1
 */

class Model_config extends CI_Model {

	var $cf;
	/**
	* @desc Default constructor for the Controller
	* @access default
	*/

    function model_config() {
		$this->cf = new Common_functions();
    }

	/**
	* @desc		Get Config
	* @param	
	* @access	default
	* @return	array $rs
	*/
	function get_config($tbl_school_id) {
		$qry_sel = "SELECT * FROM ".TBL_CONFIG." WHERE tbl_school_id='".$tbl_school_id."'";
		//echo $qry_sel."<br />";
		$rs = $this->cf->selectMultiRecords($qry_sel);
		return $rs;	
	}

	function get_cards_categories($tbl_school_id,$tbl_card_category_id="") {
		//cards is common for all schools. It is managing from govt. side
		//tbl_school_id='".$tbl_school_id."' and 
		$qry_sel = "SELECT * FROM ".TBL_CARD_CATEGORY." WHERE is_active='Y' ";
		if($tbl_card_category_id<>"")
			$qry_sel .= " AND tbl_card_category_id='".$tbl_card_category_id."' ";
		$rs = $this->cf->selectMultiRecords($qry_sel);
		return $rs;	
	}

	function get_cards_categories_new($tbl_school_id,$category="",$lan="") {
		//cards is common for all schools. It is managing from govt. side
		//tbl_school_id='".$tbl_school_id."' and 
		$Query = "SELECT * FROM ".TBL_CARD_CATEGORY." WHERE tbl_school_id = '$tbl_school_id' AND is_active='Y' "; 
		if($category<>""){
			$Query .= " AND  (tbl_school_type_id LIKE '%$category%' OR  tbl_school_type_id LIKE '%general%' )";
		}
		
		if($lan=="en")
			$Query .= " ORDER BY category_name_en ASC  ";
		else
			$Query .= " ORDER BY category_name_ar ASC ";

		$rs = $this->cf->selectMultiRecords($Query);
		return $rs;	
	}

	function get_all_country()
	{
		$qry_sel = "SELECT * FROM ".TBL_COUNTRY." WHERE is_active='Y' ";
		$rs = $this->cf->selectMultiRecords($qry_sel);
		return $rs;	
	}
	
	//school contact information
	function get_school_contact($tbl_school_id)
	{
	   $qry = "SELECT * FROM " . TBL_SCHOOL_CONTACTS . " WHERE tbl_school_id='$tbl_school_id' ";
	   $rs = $this->cf->selectMultiRecords($qry);
	   return $rs;	
	}
	
	function save_school_contact($tbl_school_contacts_id, $latitude, $longitude, $email, $phone, $mobile, $fax, $tbl_school_id,
	                             $website, $payment_link, $facebook, $google, $youtube, $twitter, $linkedin)
	{
		$qry = "DELETE FROM ".TBL_SCHOOL_CONTACTS." WHERE tbl_school_id='$tbl_school_id' ";
        $this->cf->deleteFrom($qry);

		$phone_str 	     = isset($phone)? "+971".$phone:'';
		$mobile_str 	 = isset($mobile)? "+971".$mobile:'';
		$fax_str 		 = isset($fax)? "+971".$fax:'';

		if($tbl_school_id<>""){
			$qry = "INSERT INTO ".TBL_SCHOOL_CONTACTS." (`tbl_school_contacts_id` ,`school_contacts_en` ,`school_contacts_ar` ,`latitude` ,`longitude` ,`email` ,`phone` ,`mobile` ,`fax` ,`website`, `payment_link`, `facebook`, `google`, `youtube`, `twitter`, `linkedin`,
					`is_active` ,`added_date` ,`tbl_school_id`)
			   VALUES ('$tbl_school_contacts_id', '$school_contacts_en', '$school_contacts_ar', '$latitude', '$longitude','$email','$phone_str','$mobile_str', '$fax_str',
			  '$website', '$payment_link', '$facebook', '$google', '$youtube', '$twitter', '$linkedin', 'Y', NOW(), '$tbl_school_id');";	
			$this->cf->insertInto($qry);
		   return "Y";
		}else{
		   return "N";	
			
		}
	}
	
	
	// performance topics
	function get_performance_topics_new($tbl_student_id,$tbl_class_id,$tbl_school_id,$is_active='') {
		
	    $qry = "SELECT *  FROM ".TBL_STUDENT_PERFORMANCE_MASTER." AS PM 
		        LEFT JOIN ".TBL_STUDENT_PERFORMANCE_CATEGORY." AS PC ON PC.tbl_performance_category_id=PM.tbl_performance_category_id 
				LEFT JOIN ".TBL_PERFORMANCE_CLASS." AS PCL ON PCL.tbl_performance_id=PM.tbl_performance_id 
				WHERE 1 ";
		$qry .= " AND PM.tbl_school_id= '".$tbl_school_id."' ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PM.is_active='$is_active' ";
		}
		$qry .= " AND PM.is_active<>'D' "; 
		if($tbl_performance_id<>"")
		{
			$qry .= " AND PM.tbl_performance_id = '".$tbl_performance_id."' "; 
		}
		if($tbl_class_id<>"")
		{
			$qry .= " AND PCL.tbl_class_id = '".$tbl_class_id."' "; 
		}
		
		$qry .= " ORDER BY PC.performance_category_en ASC  ";
		$rs = $this->cf->selectMultiRecords($qry);
		for($d=0;$d<count($rs);$d++)
		{
			   $tbl_performance_id     = $rs[$d]['tbl_performance_id'];
			   $qryPerformance = "SELECT *  FROM ".TBL_PERFORMANCE_STUDENT." AS PS WHERE 1 ";
			   	if($tbl_student_id<>"")
				{
					$qryPerformance .= " AND PS.tbl_student_id = '".$tbl_student_id."' "; 
				}
				if($tbl_class_id<>"")
				{
					$qryPerformance .= " AND PS.tbl_class_id = '".$tbl_class_id."' "; 
				}
					if($tbl_school_id<>"")
				{
					$qryPerformance .= " AND PS.tbl_school_id = '".$tbl_school_id."' "; 
				}
				if($tbl_performance_id<>"")
				{
					$qryPerformance .= " AND PS.tbl_performance_id = '".$tbl_performance_id."' "; 
				}
				
			   $qryPerformance .= " AND PS.is_active='Y' ORDER BY PS.id DESC limit 1 ";
			   $results         = $this->cf->selectMultiRecords($qryPerformance);
			   $rs[$d]['performance_value']  = $results[0]['performance_value'];
			   $rs[$d]['reported_date']      = $results[0]['reported_date'];
			
		}
		return $rs;	
	}
	
	/****************************************** performance graph *********************************/
	function get_performance_graphs($tbl_student_id,$tbl_class_id,$tbl_school_id,$is_active='',$tbl_performance_id='',$from_date='',$to_date='',$range='') {
		
	    $qry = "SELECT *  FROM ".TBL_STUDENT_PERFORMANCE_MASTER." AS PM 
		        LEFT JOIN ".TBL_STUDENT_PERFORMANCE_CATEGORY." AS PC ON PC.tbl_performance_category_id=PM.tbl_performance_category_id 
				LEFT JOIN ".TBL_PERFORMANCE_CLASS." AS PCL ON PCL.tbl_performance_id=PM.tbl_performance_id 
				WHERE 1 ";
		$qry .= " AND PM.tbl_school_id= '".$tbl_school_id."' ";
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND PM.is_active='$is_active' ";
		}
		$qry .= " AND PM.is_active<>'D' "; 
		if($tbl_performance_id<>"")
		{
			$qry .= " AND PM.tbl_performance_id = '".$tbl_performance_id."' "; 
		}
		if($tbl_class_id<>"")
		{
			$qry .= " AND PCL.tbl_class_id = '".$tbl_class_id."' "; 
		}
		
		$qry .= " ORDER BY PC.performance_category_en ASC  ";
		$rs = $this->cf->selectMultiRecords($qry);
	
		$m = 0;
		for($d=0;$d<count($rs);$d++)
		{
			 
			   $tbl_performance_id     = $rs[$d]['tbl_performance_id'];
			  /* $qryPerformance = "SELECT tbl_performance_student_id,tbl_performance_id,tbl_student_id,tbl_class_id,tbl_teacher_id,performance_value, DATE(MAX(reported_date))  AS reported_date, MONTHNAME(MAX(reported_date)) AS reported_month FROM ".TBL_PERFORMANCE_STUDENT." AS PS WHERE 1 ";
			   	if($tbl_student_id<>"")                                                 
				{
					$qryPerformance .= " AND PS.tbl_student_id = '".$tbl_student_id."' "; 
				}
				if($tbl_class_id<>"")
				{
					$qryPerformance .= " AND PS.tbl_class_id = '".$tbl_class_id."' "; 
				}
					if($tbl_school_id<>"")
				{
					$qryPerformance .= " AND PS.tbl_school_id = '".$tbl_school_id."' "; 
				}
				if($tbl_performance_id<>"")
				{
					$qryPerformance .= " AND PS.tbl_performance_id = '".$tbl_performance_id."' "; 
				}
			
				if($from_date<>"" && $from_date<>'""' )
				{
					$from_date       = $from_date." 00:00:00";
					$to_date         = $to_date." 23:59:60";
					$qryPerformance .= " AND ( PS.reported_date >=  '".$from_date."' AND  PS.reported_date <='".$to_date."' ) ";
				}
				
				 $qryPerformance .= " AND PS.is_active='Y' ";
				
				if($range=="year")
			   {
				 $qryPerformance .= " GROUP BY MONTH(PS.reported_date) ";   
				   
			   }
			   else if($range=="month")
			   {
				 $qryPerformance .= " GROUP BY DAY(PS.reported_date) ";   
				   
			   } else if($range=="week")
			   {
				 $qryPerformance .= " GROUP BY DAY(PS.reported_date) ";   
				   
			   }
			   $qryPerformance .= " ORDER BY PS.id ASC  "; */
			 
			  // echo $qryPerformance;
			  
			  
			  $qryPerformance = "SELECT tbl_performance_student_id,tbl_performance_id,tbl_student_id,tbl_class_id,tbl_teacher_id,performance_value, reported_date, DATE_FORMAT(reported_date, '%b') AS reported_month FROM ".TBL_PERFORMANCE_STUDENT." AS PS WHERE 1 ";
			   	if($tbl_student_id<>"")                                                 
				{
					$qryPerformance .= " AND PS.tbl_student_id = '".$tbl_student_id."' "; 
				}
				if($tbl_class_id<>"")
				{
					$qryPerformance .= " AND PS.tbl_class_id = '".$tbl_class_id."' "; 
				}
					if($tbl_school_id<>"")
				{
					$qryPerformance .= " AND PS.tbl_school_id = '".$tbl_school_id."' "; 
				}
				if($tbl_performance_id<>"")
				{
					$qryPerformance .= " AND PS.tbl_performance_id = '".$tbl_performance_id."' "; 
				}
			
				if($from_date<>"" && $from_date<>'""' )
				{
					$from_date       = $from_date." 00:00:00";
					$to_date         = $to_date." 23:59:60";
					$qryPerformance .= " AND ( PS.reported_date >=  '".$from_date."' AND  PS.reported_date <='".$to_date."' ) ";
				}
				
				 $qryPerformance .= " AND PS.is_active='Y' "; 
				 $qryPerformance .= " ORDER BY PS.id DESC  ";
				 $results         = $this->cf->selectMultiRecords($qryPerformance);
				
			/* if($range=="year")
			   {
				 $qryPerformance .= " GROUP BY MONTH(PS.reported_date) ";   
				   
			   }
			   else if($range=="month")
			   {
				 $qryPerformance .= " GROUP BY DAY(PS.reported_date) ";   
				   
			   } else if($range=="week")
			   {
				 $qryPerformance .= " GROUP BY DAY(PS.reported_date) ";   
				   
			   }*/
			   
			   $newResultsArray = array(); 
			
			  
			   if($range=="year")
			   {
						   $sel_month = date("M");
						   $prevMonth = "";
						   $x = 0;
						   $y= 1;
						   for($c=0;$c<12;$c++)
						   {
								for($t=0;$t<count($results);$t++)
								{   
									$b=0;
									if($sel_month==$results[$t]['reported_month'])
									{
										if($prevMonth <> $sel_month)
										{
											
											$reported_date_array = explode(" ",$results[$t]['reported_date']);
											
											$newResultsArray[$x]['reported_month']      = $results[$t]['reported_month'];
											$newResultsArray[$x]['tbl_performance_id']  = $results[$t]['tbl_performance_id'];
											$newResultsArray[$x]['tbl_student_id']      = $results[$t]['tbl_student_id'];
											$newResultsArray[$x]['tbl_class_id']        = $results[$t]['tbl_class_id'];
											$newResultsArray[$x]['tbl_teacher_id']      = $results[$t]['tbl_teacher_id'];
											$newResultsArray[$x]['performance_value']   = $results[$t]['performance_value'];
											$newResultsArray[$x]['reported_date']       = $reported_date_array[0];
											$x =$x+1;
											$t = count($results);
											$prevMonth  =  $results[$t]['reported_month'];
											$b = 1;
										}
									}
								}
								
								if($b==0)
								{
									$newResultsArray[$x]['reported_month']      = $sel_month;
									$newResultsArray[$x]['tbl_performance_id']  = '';
									$newResultsArray[$x]['tbl_student_id']      = '';
									$newResultsArray[$x]['tbl_class_id']        = '';
									$newResultsArray[$x]['tbl_teacher_id']      = '';
									$newResultsArray[$x]['performance_value']   = '1000';
									$newResultsArray[$x]['reported_date']       = '';
									$x =$x+1;
									$prevMonth  =  $sel_month;
								}
								
								/*$startdateStr     = strtotime("-1 months", strtotime($sel_month));
								$sel_month        = date("F",$startdateStr); */
	
				                 $sel_month = date("M", strtotime( date( 'Y-m-01' )." -$y months"));
								 $y = $y + 1 ;
								
		
								
						   }
			   } 
			   else if($range=="month")
			   {
						   $sel_date = date("Y-m");
						   $sel_date = $sel_date."-31";
						   $prevDate = "";
						   $x = 0;
						  
						   for($c=0;$c<31;$c++)
						   {
								for($t=0;$t<count($results);$t++)
								{   
								    $dateArray =  explode(" ",$results[$t]['reported_date']);
									
									$b=0;
									if($sel_date==$dateArray[0])
									{
										if($prevDate <> $sel_date)
										{
											$reported_date_array = explode(" ",$results[$t]['reported_date']);
											
											$newResultsArray[$x]['reported_month']      = $results[$t]['reported_month'];
											$newResultsArray[$x]['tbl_performance_id']  = $results[$t]['tbl_performance_id'];
											$newResultsArray[$x]['tbl_student_id']      = $results[$t]['tbl_student_id'];
											$newResultsArray[$x]['tbl_class_id']        = $results[$t]['tbl_class_id'];
											$newResultsArray[$x]['tbl_teacher_id']      = $results[$t]['tbl_teacher_id'];
											$newResultsArray[$x]['performance_value']   = $results[$t]['performance_value'];
											$newResultsArray[$x]['reported_date']       = date("d M ",strtotime($reported_date_array[0]));
											$x =$x+1;
											$t = count($results);
											$prevDate  =  $results[$t]['reported_date'];
											$b = 1;
										}
									}
								}
								
								if($b==0)
								{
									$newResultsArray[$x]['reported_month']      = '';
									$newResultsArray[$x]['reported_date']       = date("d M ",strtotime($sel_date));
									$newResultsArray[$x]['tbl_performance_id']  = '';
									$newResultsArray[$x]['tbl_student_id']      = '';
									$newResultsArray[$x]['tbl_class_id']        = '';
									$newResultsArray[$x]['tbl_teacher_id']      = '';
									$newResultsArray[$x]['performance_value']   = '1000';
									$x =$x+1;
									$prevDate     =  $sel_date;
								}
								
								$startdateStr     = strtotime("-1 day", strtotime($sel_date));
								$sel_date         = date("Y-m-d",$startdateStr); 
								
						   }  
				   
			   }
			     else if($range=="week")
			   {
						   $sel_date = date("Y-m-d");
						   $prevDate = "";
						   $x = 0;
						  
						   for($c=0;$c<7;$c++)
						   {
								for($t=0;$t<count($results);$t++)
								{   
								    $dateArray =  explode(" ",$results[$t]['reported_date']);
									
									$b=0;
									if($sel_date==$dateArray[0])
									{
										if($prevDate <> $sel_date)
										{
											$reported_date_array = explode(" ",$results[$t]['reported_date']);
											
											$newResultsArray[$x]['reported_month']      = $results[$t]['reported_month'];
											$newResultsArray[$x]['tbl_performance_id']  = $results[$t]['tbl_performance_id'];
											$newResultsArray[$x]['tbl_student_id']      = $results[$t]['tbl_student_id'];
											$newResultsArray[$x]['tbl_class_id']        = $results[$t]['tbl_class_id'];
											$newResultsArray[$x]['tbl_teacher_id']      = $results[$t]['tbl_teacher_id'];
											$newResultsArray[$x]['performance_value']   = $results[$t]['performance_value'];
											$newResultsArray[$x]['reported_date']       = date("d M ",strtotime($reported_date_array[0]));
											$x =$x+1;
											$t = count($results);
											$prevDate  =  $results[$t]['reported_date'];
											$b = 1;
										}
									}
								}
								
								if($b==0)
								{
									$newResultsArray[$x]['reported_month']      = '';
									$newResultsArray[$x]['reported_date']       = date("d M ",strtotime($sel_date)); 
									$newResultsArray[$x]['tbl_performance_id']  = '';
									$newResultsArray[$x]['tbl_student_id']      = '';
									$newResultsArray[$x]['tbl_class_id']        = '';
									$newResultsArray[$x]['tbl_teacher_id']      = '';
									$newResultsArray[$x]['performance_value']   = '1000';
									$x =$x+1;
									$prevDate     =  $sel_date;
								}
								
								$startdateStr     = strtotime("-1 day", strtotime($sel_date));
								$sel_date         = date("Y-m-d",$startdateStr); 
						   }  
			   }
			   
			   $rs[$d]['performance_data']  = $newResultsArray;
			  
		}
		return $rs;	
	}
	
	/****************************************** end performance graph ****************************/
	
	

  }

?>



