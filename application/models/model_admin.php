<?php
include_once('include/common_functions.php');
/**
* @desc   	  	Admin Model
* @category   	Model
* @author     	Shanavas
* @version    	0.1
*/

class Model_admin extends CI_Model {
	var $cf;

	function model_admin() {
		$this->cf = new Common_functions();
	}

	/**
	* @desc		Get event details
	* @param	none 	
	* @access	default
	* @return	array $rs
	*/
	function get_admin_user_obj($tbl_admin_id) {
		$qry_sel = "SELECT * FROM ".TBL_ADMIN_USERS." WHERE tbl_admin_id='$tbl_admin_id' ";
		$rs = $this->cf->selectMultiRecords($qry_sel);
		return $rs;	
	}

	/**
	* @desc		Get admin users having roles
	* @param	none 	
	* @access	default
	* @return	array $rs
	*/
	function get_admin_users_with_roles($tbl_school_id) {
		$qry_sel = "SELECT * FROM ".TBL_ADMIN_USERS." WHERE tbl_school_roles_id<>'' AND tbl_school_id='".$tbl_school_id."' AND is_active='Y'";
		//echo $qry_sel;
		$rs = $this->cf->selectMultiRecords($qry_sel);
		return $rs;	
	}

	function authenticate_g($username, $password) {
	
		$username    =  $this->cf->get_data(trim($username));
		$password   =  $this->cf->get_data(trim($password));
		$module_access = "G";
	    $qry = "SELECT password, salt FROM ".TBL_ADMIN_USERS." WHERE user_id='$username' AND is_active='Y' AND module_access='$module_access' ";
		$result = $this->cf->selectMultiRecords($qry);
		if (count($result)>0) {
			$salt = $result[0]['salt'];
			$db_password = $result[0]['password'];
		} else {
			return "D";//User does not exists
		}
		
		$hash = sha1($salt . sha1($password));
								
		if (trim($hash) != trim($db_password)) {                
			return "N";
		} else {     
			return "Y";
		}
		
	}
	
	function get_admin_info($user_id) {
		$Query = "SELECT * FROM ". TBL_ADMIN_USERS ." WHERE user_id='$user_id' AND is_active='Y' ";
		$rs = $this->cf->selectMultiRecords($Query);
		return $rs;	
	}
	
	function update_last_login($tbl_admin_id)
	{
		$this->cf->register_session_user($tbl_admin_id);
		$qry_update = "UPDATE ".TBL_ADMIN_USERS." SET last_login=NOW() WHERE tbl_admin_id='$tbl_admin_id' ";
		$this->cf->update($qry_update);	
	}
	
	
	
    //website authentication
	function authenticate($username, $password, $module_access) {
		
		$username    =  $this->cf->get_data(trim($username));
		$password   =  $this->cf->get_data(trim($password));
	    $qry = "SELECT password, salt FROM ".TBL_ADMIN_USERS." WHERE user_id='$username' AND is_active='Y' AND module_access='$module_access' ";
		$result = $this->cf->selectMultiRecords($qry);
		if (count($result)>0) {
			$salt = $result[0]['salt'];
			$db_password = $result[0]['password'];
		} else {
			return "D";//User does not exists
		}
		
		$hash = sha1($salt . sha1($password));
								
		if (trim($hash) != trim($db_password)) {                
			return "N";
		} else {     
			return "Y";
		}
		
		/*$Query = "SELECT tbl_admin_id,user_id,password FROM ". TBL_ADMIN_USERS ." WHERE user_id='$username' AND password='$password' AND is_active='Y' ";
		$result = $this->cf->selectMultiRecords($Query);
		if($result) {
			return "Y";
		} else {     
			return "";
		}*/
	
	}
	
	
	/**
	* @desc		Get all users
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function get_all_users($sort_name, $sort_by, $offset, $q, $is_active, $tbl_school_id) {
		$sort_name = $this->cf->get_data($sort_name);
		$sort_by   = $this->cf->get_data($sort_by);
		$offset    = $this->cf->get_data($offset);
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
	
		$qry = "SELECT * FROM ".TBL_ADMIN_USERS." WHERE 1 ";
		
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND is_active='$is_active' ";
		}
		
		$qry .= " AND is_active<>'D' ";
		//Search
		if (trim($q) != "") {
			$qry .= " AND (first_name LIKE '%$q%' || last_name LIKE '%$q%' || email LIKE '%$q%')";
		} 
		
		if($tbl_school_id<>"")
		{
			$qry .= " AND tbl_school_id = '$tbl_school_id' ";
		}
		
		$qry .= " AND module_access = 'S' ";
		

		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY first_name ASC";
		}

		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_ADMIN_USERS_PAGING;
		}
		//echo $qry;
		
		$results = $this->cf->selectMultiRecords($qry);
		
	return $results;
	}
	
	/**
	* @desc		Get all users
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_users($q, $is_active, $tbl_school_id) {
		$q 		 = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT COUNT(*) as total_users FROM ".TBL_ADMIN_USERS." WHERE 1 ";
		
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND is_active='$is_active' ";
		}
		
		$qry .= " AND is_active<>'D' ";
		$qry .= " AND module_access = 'S' ";
		//Search
		if (trim($q) != "") {
			$qry .= " AND (first_name LIKE '%$q%' || last_name LIKE '%$q%' || email LIKE '%$q%')";
		} 
		if($tbl_school_id<>"")
		{
			$qry .= " AND tbl_school_id = '$tbl_school_id' ";
		}
		//echo $qry;
		$results = $this->cf->selectMultiRecords($qry);
	return $results[0]['total_users'];
	}
	
		/**
	* @desc		Activate
	* 
	* @param	string $tbl_admin_user_id
	* @access	default
	*/
	function activate($tbl_admin_user_id) {
		$qry = "UPDATE ".TBL_ADMIN_USERS." SET is_active='Y' WHERE tbl_admin_id='$tbl_admin_user_id' ";
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate
	* 
	* @param	string $tbl_admin_user_id
	* @access	default
	*/
	function deactivate($tbl_admin_user_id) {
		$qry = "UPDATE ".TBL_ADMIN_USERS." SET is_active='N' WHERE tbl_admin_id='$tbl_admin_user_id' ";
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete
	* 
	* @param	string $tbl_admin_user_id
	* @access	default
	*/
	/*function delete($tbl_admin_user_id) {
		$qry = "DELETE FROM ".TBL_ADMIN_USERS." WHERE tbl_admin_id='".$tbl_admin_user_id."' ";
		$this->cf->deleteFrom($qry);
	}*/
	
	function delete($tbl_admin_user_id) {
		$qry = "UPDATE ".TBL_ADMIN_USERS." SET is_active='D' WHERE  tbl_admin_id='".$tbl_admin_user_id."' ";
		$this->cf->update($qry);
	}
	
	
	/**
	* @desc		Create user
	* @param	String params
	* @access	default
	*/
	function create_user($tbl_admin_user_id, $first_name, $last_name, $dob, $gender, $mobile, $email, $username, $password) {
		$tbl_admin_user_id = $this->cf->get_data($tbl_admin_user_id);
		$first_name        = $this->cf->get_data($first_name);
		$last_name         = $this->cf->get_data($last_name);
		$dob               = $this->cf->get_data($dob);
		$gender            = $this->cf->get_data($gender);
		$mobile            = $this->cf->get_data($mobile);
		$email             = $this->cf->get_data($email);
		$username          = $this->cf->get_data($username);
		$password          = $this->cf->get_data($password);

		$ip = $this->cf->getIP();
		
		$password_code = base64_encode($password); 		
		$hash = sha1($password);
		$salt = substr(md5(uniqid(rand(), true)), 0, 3);
		$hash = sha1($salt . $hash);
		
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
		
		$sql4userSession = "SELECT * FROM ".TBL_ADMIN_USERS." WHERE user_id='".$username."' and is_active<>'D' ";
        if($tbl_school_id<>"")
		{
        	$sql4userSession .= " AND tbl_school_id = '$tbl_school_id' ";
		}

        $dob = date("Y-m-d", strtotime($dob));
		
		if($mobile<>"")
			$mobile 	       = "+971".$mobile;
			
		if ($this->cf->isExist($sql4userSession)){
			$MSG="The User ID you have given already exists!";
			return "E";
		}else{
	
			$qry_ins = "INSERT INTO ".TBL_ADMIN_USERS." (`tbl_admin_id`, `first_name`, `last_name`, `dob`, `gender`, `phone`, `email`, `user_id`, `password`, `salt`, `password_code`,  `reg_date`, `is_active`, `ip`, `last_login`, `tbl_school_id`)
						VALUES ('$tbl_admin_user_id', '$first_name', '$last_name', '$dob', '$gender', '$mobile', '$email', '$username', '$hash', '$salt', '$password_code', NOW(), 'Y', '$ip', NOW(),'$tbl_school_id' )";
			
			$this->cf->insertInto($qry_ins);
			
			$qry = "SELECT file_name_updated FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_admin_user_id' AND is_active='Y' ";
			$result = $this->cf->selectMultiRecords($qry);
			if (count($result)>0) {
				$picture = isset($result[0]['file_name_updated'])? $result[0]['file_name_updated']:'';
				$qry_update  = "UPDATE ".TBL_ADMIN_USERS." SET picture='$picture' WHERE tbl_admin_id='$tbl_admin_user_id' ";
				$this->cf->update($qry_update);
			}
			return "Y";
		}
		
	}

	
	/**
	* @desc		Save changes
	* @param	String params
	* @access	default
	*/
	function save_changes($tbl_admin_user_id, $first_name, $last_name, $dob, $gender, $mobile, $email, $password) {
		$tbl_admin_user_id = $this->cf->get_data($tbl_admin_user_id);
		$first_name        = $this->cf->get_data($first_name);
		$last_name         = $this->cf->get_data($last_name);
		$dob               = $this->cf->get_data($dob);
		$gender 			= $this->cf->get_data($gender);
		$mobile 			= $this->cf->get_data($mobile);
		$email             = $this->cf->get_data($email);
		$password          = $this->cf->get_data($password);
		
		if (trim($password) != "") {
			$password_code = base64_encode($password); 						
			$hash = sha1($password);
			$salt = substr(md5(uniqid(rand(), true)), 0, 3);
			$hash = sha1($salt . $hash);
		}
		
		$tbl_school_id = $_SESSION['aqdar_smartcare']['tbl_school_id_sess'];
		
		if($mobile<>"")
			$mobile 	       = "+971".$mobile;
		
		$qry = "SELECT file_name_updated FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_admin_user_id' AND is_active='Y' ";
		$result = $this->cf->selectMultiRecords($qry);
		if (count($result)>0) {
			$picture = isset($result[0]['file_name_updated'])? $result[0]['file_name_updated']:'';
		}
		
		$qry_update = "UPDATE ".TBL_ADMIN_USERS." SET first_name='$first_name', last_name='$last_name', phone='$mobile', email='$email', gender='$gender' "; 
		
		if (trim($password) != "") {
			$qry_update .= " ,password='$hash', salt='$salt', password_code='$password_code' ";
		}
		if($picture<>"")
		{
			$qry_update .= " ,picture='$picture' ";
			
		}
		
		$qry_update .= " WHERE tbl_admin_id='$tbl_admin_user_id' " ;
		if($tbl_school_id<>"")
		{
			$qry_update .= " AND tbl_school_id='$tbl_school_id' " ;
		}
		//echo $qry_update."<br>"; exit;
		$this->cf->update($qry_update);
	}
	
	// End admin user section
	
	//Start admin user rights
	function get_backend_modules_rights($tbl_admin_user_id,$moduleAccess) {
		$qry_sel = "SELECT ".TBL_MODULE_CATEGORIES.".module_cat_name, ".TBL_MODULES.". *
			        FROM ".TBL_MODULE_CATEGORIES.", ".TBL_MODULES."
			        WHERE ".TBL_MODULE_CATEGORIES.".tbl_module_cat_id = ".TBL_MODULES.".tbl_module_cat_id  
			        AND tbl_module_categories.is_active='Y' 
			        AND tbl_module_categories.module_access='$moduleAccess' 
			        AND tbl_modules.is_active='Y' 
			        order by ".TBL_MODULES.".sort_order asc";
		$rs = $this->cf->selectMultiRecords($qry_sel);
		for($m=0;$m<count($rs);$m++)
		{
			$check_status = '';
			$tbl_module_id =  $rs[$m]['tbl_module_id'];
		    $qry = "SELECT tbl_module_id FROM ".TBL_ADMIN_RIGHTS." WHERE tbl_module_id='".$tbl_module_id."' AND tbl_admin_id='".$tbl_admin_user_id."' AND is_active='Y'";
			$data_module = $this->cf->selectMultiRecords($qry);
			if (count($data_module) > 0){
				$check_status = 'checked';
			}
			$rs[$m]['check_status'] = $check_status;
		}
		
		return $rs;	
	}
	
	
	function del_admin_rights($tbl_admin_user_id)
	{
		$qry = "DELETE FROM ".TBL_ADMIN_RIGHTS." WHERE tbl_admin_id= '$tbl_admin_user_id'";
		$this->cf->deleteFrom($qry);
	}
	
	
	function save_admin_rights($str,$tbl_admin_user_id) {
			$tbl_admin_right_id = md5(uniqid(rand()));	
			$tbl_admin_right_id = substr($tbl_admin_right_id,0,10);
			$tbl_module_id      = $str;
			$user_id            = $_SESSION['aqdar_smartcare']['tbl_admin_id_sess'];

			$qry_ins = "INSERT INTO ".TBL_ADMIN_RIGHTS."(`id`, `tbl_admin_right_id`, `tbl_admin_id`, `tbl_module_id`, `added_by`, `added_date`, `approved_by`, `approved_date`, `last_modified_by`, `last_modified_date`, `is_active`,`tbl_school_id`)
			VALUES ('$id', '$tbl_admin_right_id', '$tbl_admin_user_id', '$tbl_module_id', '$user_id', NOW(), '$user_id',  NOW(), '$user_id', NOW(), 'Y', '$tbl_school_id')";
			$this->cf->insertInto($qry_ins);
	}
	
	
	//GET BACKEND MODULE ACCESS
	function getSchoolModuleAccess($tbl_admin_user_id)
	{
		 $qry = "SELECT AR.tbl_module_id, M.module_key FROM ".TBL_ADMIN_RIGHTS." AS AR LEFT JOIN ".TBL_MODULES." AS M ON  AR.tbl_module_id = M.tbl_module_id 
		         WHERE AR.tbl_admin_id='".$tbl_admin_user_id."' AND AR.is_active='Y'";
		 $data_module = $this->cf->selectMultiRecords($qry);
		 return $data_module;
		
	}
	
	//END BACKEND MODULE ACCESS
	
	
	//MINISTRY ADMIN USERS SECTIONS
		/**
	* @desc		Get all users
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function get_all_ministry_users($sort_name, $sort_by, $offset, $q, $is_active) {
		$sort_name = $this->cf->get_data($sort_name);
		$sort_by   = $this->cf->get_data($sort_by);
		$offset    = $this->cf->get_data($offset);
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
	
		$qry = "SELECT * FROM ".TBL_ADMIN_USERS." WHERE 1 ";
		
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND is_active='$is_active' ";
		}
		
		$qry .= " AND is_active<>'D' ";
		
		$qry .= " AND module_access ='G' ";
		//Search
		if (trim($q) != "") {
			$qry .= " AND (first_name LIKE '%$q%' || last_name LIKE '%$q%' || email LIKE '%$q%')";
		} 

		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY first_name ASC";
		}

		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_ADMIN_USERS_PAGING;
		}
		//echo $qry;
		
		$results = $this->cf->selectMultiRecords($qry);
		
	return $results;
	}
	
	/**
	* @desc		Get all users
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_ministry_users($q, $is_active) {
		$q 		 = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT COUNT(*) as total_users FROM ".TBL_ADMIN_USERS." WHERE 1 ";
		
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND is_active='$is_active' ";
		}
		
		$qry .= " AND is_active<>'D' ";
		$qry .= " AND module_access ='G' ";
		//Search
		if (trim($q) != "") {
			$qry .= " AND (first_name LIKE '%$q%' || last_name LIKE '%$q%' || email LIKE '%$q%')";
		} 
		//echo $qry;
		$results = $this->cf->selectMultiRecords($qry);
	return $results[0]['total_users'];
	}
	
		/**
	* @desc		Activate
	* 
	* @param	string $tbl_admin_user_id
	* @access	default
	*/
	function activate_ministry($tbl_admin_user_id) {
		$qry = "UPDATE ".TBL_ADMIN_USERS." SET is_active='Y' WHERE tbl_admin_id='$tbl_admin_user_id' ";
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate
	* 
	* @param	string $tbl_admin_user_id
	* @access	default
	*/
	function deactivate_ministry($tbl_admin_user_id) {
		$qry = "UPDATE ".TBL_ADMIN_USERS." SET is_active='N' WHERE tbl_admin_id='$tbl_admin_user_id' ";
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete
	* 
	* @param	string $tbl_admin_user_id
	* @access	default
	*/
	/*function delete($tbl_admin_user_id) {
		$qry = "DELETE FROM ".TBL_ADMIN_USERS." WHERE tbl_admin_id='".$tbl_admin_user_id."' ";
		$this->cf->deleteFrom($qry);
	}*/
	
	function delete_ministry($tbl_admin_user_id) {
		$qry = "DELETE FROM ".TBL_ADMIN_USERS." SET is_active='D' WHERE  tbl_admin_id='".$tbl_admin_user_id."' ";
		$this->cf->deleteFrom($qry);
	}
	
	
	/**
	* @desc		Create user
	* @param	String params
	* @access	default
	*/
	function create_ministry_user($tbl_admin_user_id, $first_name, $last_name, $dob, $gender, $mobile, $email, $username, $password) {
		$tbl_admin_user_id = $this->cf->get_data($tbl_admin_user_id);
		$first_name        = $this->cf->get_data($first_name);
		$last_name         = $this->cf->get_data($last_name);
		$dob               = $this->cf->get_data($dob);
		$gender            = $this->cf->get_data($gender);
		$mobile            = $this->cf->get_data($mobile);
		$email             = $this->cf->get_data($email);
		$username          = $this->cf->get_data($username);
		$password          = $this->cf->get_data($password);

		$ip = $this->cf->getIP();
		
		$password_code = base64_encode($password); 		
		$hash = sha1($password);
		$salt = substr(md5(uniqid(rand(), true)), 0, 3);
		$hash = sha1($salt . $hash);
		
		$sql4userSession = "SELECT * FROM ".TBL_ADMIN_USERS." WHERE user_id='".$username."' and is_active<>'D' ";

        $dob = date("Y-m-d", strtotime($dob));
		
		if($mobile<>"")
			$mobile 	       = "+971".$mobile;
			
		if ($this->cf->isExist($sql4userSession)){
			$MSG="The User ID you have given already exists!";
			return "E";
		}else{
	
			$qry_ins = "INSERT INTO ".TBL_ADMIN_USERS." (`tbl_admin_id`, `first_name`, `last_name`, `dob`, `gender`, `phone`, `email`, `user_id`, `password`, `salt`, `password_code`,  `reg_date`, `is_active`, `ip`, `last_login`, `module_access`)
						VALUES ('$tbl_admin_user_id', '$first_name', '$last_name', '$dob', '$gender', '$mobile', '$email', '$username', '$hash', '$salt', '$password_code', NOW(), 'Y', '$ip', NOW(), 'G')";
			
			$this->cf->insertInto($qry_ins);
			
			$qry = "SELECT file_name_updated FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_admin_user_id' AND is_active='Y' ";
			$result = $this->cf->selectMultiRecords($qry);
			if (count($result)>0) {
				$picture = isset($result[0]['file_name_updated'])? $result[0]['file_name_updated']:'';
				$qry_update  = "UPDATE ".TBL_ADMIN_USERS." SET picture='$picture' WHERE tbl_admin_id='$tbl_admin_user_id' ";
				$this->cf->update($qry_update);
			}
			return "Y";
		}
		
	}

	
	/**
	* @desc		Save changes
	* @param	String params
	* @access	default
	*/
	function save_ministry_changes($tbl_admin_user_id, $first_name, $last_name, $dob, $gender, $mobile, $email, $password) {
		$tbl_admin_user_id = $this->cf->get_data($tbl_admin_user_id);
		$first_name        = $this->cf->get_data($first_name);
		$last_name         = $this->cf->get_data($last_name);
		$dob               = $this->cf->get_data($dob);
		$gender 			= $this->cf->get_data($gender);
		$mobile 			= $this->cf->get_data($mobile);
		$email             = $this->cf->get_data($email);
		$password          = $this->cf->get_data($password);
		
		if (trim($password) != "") {
			$password_code = base64_encode($password); 						
			$hash = sha1($password);
			$salt = substr(md5(uniqid(rand(), true)), 0, 3);
			$hash = sha1($salt . $hash);
		}
		
		$qry = "SELECT file_name_updated FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_admin_user_id' AND is_active='Y' ";
		$result = $this->cf->selectMultiRecords($qry);
		if (count($result)>0) {
			$picture = isset($result[0]['file_name_updated'])? $result[0]['file_name_updated']:'';
		}
		
		$qry_update = "UPDATE ".TBL_ADMIN_USERS." SET first_name='$first_name', last_name='$last_name', phone='$mobile', email='$email', gender='$gender' "; 
		
		if (trim($password) != "") {
			$qry_update .= " ,password='$hash', salt='$salt', password_code='$password_code' ";
		}
		if($picture<>"")
		{
			$qry_update .= " ,picture='$picture' ";
			
		}
		
		$qry_update .= " WHERE tbl_admin_id='$tbl_admin_user_id' " ;
		//echo $qry_update."<br>"; exit;
		$this->cf->update($qry_update);
	}
	
	// End admin user section
	
	//Start admin user rights
	function get_ministry_backend_modules_rights($tbl_admin_user_id,$moduleAccess) {
		$qry_sel = "SELECT ".TBL_MODULE_CATEGORIES.".module_cat_name, ".TBL_MODULES.". *
			        FROM ".TBL_MODULE_CATEGORIES.", ".TBL_MODULES."
			        WHERE ".TBL_MODULE_CATEGORIES.".tbl_module_cat_id = ".TBL_MODULES.".tbl_module_cat_id  
			        AND tbl_module_categories.is_active='Y' 
			        AND tbl_module_categories.module_access='$moduleAccess' 
			        AND tbl_modules.is_active='Y' 
			        order by ".TBL_MODULES.".sort_order asc";
		$rs = $this->cf->selectMultiRecords($qry_sel);
		for($m=0;$m<count($rs);$m++)
		{
			$check_status = '';
			$tbl_module_id =  $rs[$m]['tbl_module_id'];
		    $qry = "SELECT tbl_module_id FROM ".TBL_ADMIN_RIGHTS." WHERE tbl_module_id='".$tbl_module_id."' AND tbl_admin_id='".$tbl_admin_user_id."' AND is_active='Y'";
			$data_module = $this->cf->selectMultiRecords($qry);
			if (count($data_module) > 0){
				$check_status = 'checked';
			}
			$rs[$m]['check_status'] = $check_status;
		}
		
		return $rs;	
	}
	
	
	function del_ministry_admin_rights($tbl_admin_user_id)
	{
		$qry = "DELETE FROM ".TBL_ADMIN_RIGHTS." WHERE tbl_admin_id= '$tbl_admin_user_id'";
		$this->cf->deleteFrom($qry);
	}
	
	
	function save_ministry_admin_rights($str,$tbl_admin_user_id) {
			$tbl_admin_right_id = md5(uniqid(rand()));	
			$tbl_admin_right_id = substr($tbl_admin_right_id,0,10);
			$tbl_module_id      = $str;
			$user_id            = $_SESSION['aqdar_smartcare']['tbl_admin_id_sess'];

			$qry_ins = "INSERT INTO ".TBL_ADMIN_RIGHTS."(`id`, `tbl_admin_right_id`, `tbl_admin_id`, `tbl_module_id`, `added_by`, `added_date`, `approved_by`, `approved_date`, `last_modified_by`, `last_modified_date`, `is_active`)
			VALUES ('$id', '$tbl_admin_right_id', '$tbl_admin_user_id', '$tbl_module_id', '$user_id', NOW(), '$user_id',  NOW(), '$user_id', NOW(), 'Y', '$tbl_school_id')";
			$this->cf->insertInto($qry_ins);
	}
	
	
	
	//END MINISTRY ADMIN USERS SECTION
	
	
	
	//TIMELINE ADMIN USERS SECTIONS
		/**
	* @desc		Get all users
	* @param	string $sort_name, $sort_by, $offset, $q, $is_active
	* @access	default
	*/
	function get_all_timeline_users($sort_name, $sort_by, $offset, $q, $is_active) {
		$sort_name = $this->cf->get_data($sort_name);
		$sort_by   = $this->cf->get_data($sort_by);
		$offset    = $this->cf->get_data($offset);
		$q         = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
	
		if (trim($offset) == "") {$offset = 0;}
	
		$qry = "SELECT * FROM ".TBL_ADMIN_USERS." WHERE 1 ";
		
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND is_active='$is_active' ";
		}
		
		$qry .= " AND is_active<>'D' ";
		
		$qry .= " AND module_access ='T' ";
		//Search
		if (trim($q) != "") {
			$qry .= " AND (first_name LIKE '%$q%' || last_name LIKE '%$q%' || email LIKE '%$q%')";
		} 

		//Sort Option		
		if (trim($sort_name) != "" && trim($sort_by) !="") {
			$qry .= " ORDER BY $sort_name $sort_by";
		} else {
			$qry .= " ORDER BY first_name ASC";
		}

		if (trim($offset) != "") {
			$qry .= " LIMIT $offset, ".TBL_ADMIN_USERS_PAGING;
		}
		//echo $qry;
		
		$results = $this->cf->selectMultiRecords($qry);
		
	return $results;
	}
	
	/**
	* @desc		Get all users
	* @param	string $q, $is_active
	* @access	default
	*/
	function get_total_timeline_users($q, $is_active) {
		$q 		 = urldecode($this->cf->get_data($q));
		$is_active = $this->cf->get_data($is_active);
		$qry = "SELECT COUNT(*) as total_users FROM ".TBL_ADMIN_USERS." WHERE 1 ";
		
		//Active/Deactive
		if (trim($is_active) == "Y" || trim($is_active) == "N" ) {
			$qry .= " AND is_active='$is_active' ";
		}
		
		$qry .= " AND is_active<>'D' ";
		$qry .= " AND module_access ='T' ";
		//Search
		if (trim($q) != "") {
			$qry .= " AND (first_name LIKE '%$q%' || last_name LIKE '%$q%' || email LIKE '%$q%')";
		} 
		//echo $qry;
		$results = $this->cf->selectMultiRecords($qry);
	return $results[0]['total_users'];
	}
	
		/**
	* @desc		Activate
	* 
	* @param	string $tbl_admin_user_id
	* @access	default
	*/
	function activate_timeline($tbl_admin_user_id) {
		$qry = "UPDATE ".TBL_ADMIN_USERS." SET is_active='Y' WHERE tbl_admin_id='$tbl_admin_user_id' ";
		$this->cf->update($qry);
	}

	/**
	* @desc		De-Activate
	* 
	* @param	string $tbl_admin_user_id
	* @access	default
	*/
	function deactivate_timeline($tbl_admin_user_id) {
		$qry = "UPDATE ".TBL_ADMIN_USERS." SET is_active='N' WHERE tbl_admin_id='$tbl_admin_user_id' ";
		$this->cf->update($qry);
	}

	/**
	* @desc		Delete
	* 
	* @param	string $tbl_admin_user_id
	* @access	default
	*/
	/*function delete($tbl_admin_user_id) {
		$qry = "DELETE FROM ".TBL_ADMIN_USERS." WHERE tbl_admin_id='".$tbl_admin_user_id."' ";
		$this->cf->deleteFrom($qry);
	}*/
	
	function delete_timeline($tbl_admin_user_id) {
		$qry = "DELETE FROM ".TBL_ADMIN_USERS." SET is_active='D' WHERE  tbl_admin_id='".$tbl_admin_user_id."' ";
		$this->cf->deleteFrom($qry);
	}
	
	
	/**
	* @desc		Create user
	* @param	String params
	* @access	default
	*/
	function create_timeline_user($tbl_admin_user_id, $first_name, $last_name, $dob, $gender, $mobile, $email, $username, $password) {
		$tbl_admin_user_id = $this->cf->get_data($tbl_admin_user_id);
		$first_name        = $this->cf->get_data($first_name);
		$last_name         = $this->cf->get_data($last_name);
		$dob               = $this->cf->get_data($dob);
		$gender            = $this->cf->get_data($gender);
		$mobile            = $this->cf->get_data($mobile);
		$email             = $this->cf->get_data($email);
		$username          = $this->cf->get_data($username);
		$password          = $this->cf->get_data($password);

		$ip = $this->cf->getIP();
		
		$password_code = base64_encode($password); 		
		$hash = sha1($password);
		$salt = substr(md5(uniqid(rand(), true)), 0, 3);
		$hash = sha1($salt . $hash);
		
		$sql4userSession = "SELECT * FROM ".TBL_ADMIN_USERS." WHERE user_id='".$username."' and is_active<>'D' ";

        $dob = date("Y-m-d", strtotime($dob));
		
		if($mobile<>"")
			$mobile 	       = "+971".$mobile;
			
		if ($this->cf->isExist($sql4userSession)){
			$MSG="The User ID you have given already exists!";
			return "E";
		}else{
	
			$qry_ins = "INSERT INTO ".TBL_ADMIN_USERS." (`tbl_admin_id`, `first_name`, `last_name`, `dob`, `gender`, `phone`, `email`, `user_id`, `password`, `salt`, `password_code`,  `reg_date`, `is_active`, `ip`, `last_login`, `module_access`)
						VALUES ('$tbl_admin_user_id', '$first_name', '$last_name', '$dob', '$gender', '$mobile', '$email', '$username', '$hash', '$salt', '$password_code', NOW(), 'Y', '$ip', NOW(), 'T')";
			
			$this->cf->insertInto($qry_ins);
			
			$qry = "SELECT file_name_updated FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_admin_user_id' AND is_active='Y' ";
			$result = $this->cf->selectMultiRecords($qry);
			if (count($result)>0) {
				$picture = isset($result[0]['file_name_updated'])? $result[0]['file_name_updated']:'';
				$qry_update  = "UPDATE ".TBL_ADMIN_USERS." SET picture='$picture' WHERE tbl_admin_id='$tbl_admin_user_id' ";
				$this->cf->update($qry_update);
			}
			return "Y";
		}
		
	}

	
	/**
	* @desc		Save changes
	* @param	String params
	* @access	default
	*/
	function save_timeline_changes($tbl_admin_user_id, $first_name, $last_name, $dob, $gender, $mobile, $email, $password) {
		$tbl_admin_user_id = $this->cf->get_data($tbl_admin_user_id);
		$first_name        = $this->cf->get_data($first_name);
		$last_name         = $this->cf->get_data($last_name);
		$dob               = $this->cf->get_data($dob);
		$gender 			= $this->cf->get_data($gender);
		$mobile 			= $this->cf->get_data($mobile);
		$email             = $this->cf->get_data($email);
		$password          = $this->cf->get_data($password);
		
		if (trim($password) != "") {
			$password_code = base64_encode($password); 						
			$hash = sha1($password);
			$salt = substr(md5(uniqid(rand(), true)), 0, 3);
			$hash = sha1($salt . $hash);
		}
		
		$qry = "SELECT file_name_updated FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_admin_user_id' AND is_active='Y' ";
		$result = $this->cf->selectMultiRecords($qry);
		if (count($result)>0) {
			$picture = isset($result[0]['file_name_updated'])? $result[0]['file_name_updated']:'';
		}
		
		$qry_update = "UPDATE ".TBL_ADMIN_USERS." SET first_name='$first_name', last_name='$last_name', phone='$mobile', email='$email', gender='$gender' "; 
		
		if (trim($password) != "") {
			$qry_update .= " ,password='$hash', salt='$salt', password_code='$password_code' ";
		}
		if($picture<>"")
		{
			$qry_update .= " ,picture='$picture' ";
			
		}
		
		$qry_update .= " WHERE tbl_admin_id='$tbl_admin_user_id' " ;
		//echo $qry_update."<br>"; exit;
		$this->cf->update($qry_update);
	}
	
	// End admin user section
	
	//Start admin user rights
	function get_timeline_backend_modules_rights($tbl_admin_user_id,$moduleAccess) {
		$qry_sel = "SELECT ".TBL_MODULE_CATEGORIES.".module_cat_name, ".TBL_MODULES.". *
			        FROM ".TBL_MODULE_CATEGORIES.", ".TBL_MODULES."
			        WHERE ".TBL_MODULE_CATEGORIES.".tbl_module_cat_id = ".TBL_MODULES.".tbl_module_cat_id  
			        AND tbl_module_categories.is_active='Y' 
			        AND tbl_module_categories.module_access='$moduleAccess' 
			        AND tbl_modules.is_active='Y' 
			        order by ".TBL_MODULES.".sort_order asc";
		$rs = $this->cf->selectMultiRecords($qry_sel);
		for($m=0;$m<count($rs);$m++)
		{
			$check_status = '';
			$tbl_module_id =  $rs[$m]['tbl_module_id'];
		    $qry = "SELECT tbl_module_id FROM ".TBL_ADMIN_RIGHTS." WHERE tbl_module_id='".$tbl_module_id."' AND tbl_admin_id='".$tbl_admin_user_id."' AND is_active='Y'";
			$data_module = $this->cf->selectMultiRecords($qry);
			if (count($data_module) > 0){
				$check_status = 'checked';
			}
			$rs[$m]['check_status'] = $check_status;
		}
		
		return $rs;	
	}
	
	
	function del_timeline_admin_rights($tbl_admin_user_id)
	{
		$qry = "DELETE FROM ".TBL_ADMIN_RIGHTS." WHERE tbl_admin_id= '$tbl_admin_user_id'";
		$this->cf->deleteFrom($qry);
	}
	
	
	function save_timeline_admin_rights($str,$tbl_admin_user_id) {
			$tbl_admin_right_id = md5(uniqid(rand()));	
			$tbl_admin_right_id = substr($tbl_admin_right_id,0,10);
			$tbl_module_id      = $str;
			$user_id            = $_SESSION['aqdar_smartcare']['tbl_admin_id_sess'];

			$qry_ins = "INSERT INTO ".TBL_ADMIN_RIGHTS."(`id`, `tbl_admin_right_id`, `tbl_admin_id`, `tbl_module_id`, `added_by`, `added_date`, `approved_by`, `approved_date`, `last_modified_by`, `last_modified_date`, `is_active`)
			VALUES ('$id', '$tbl_admin_right_id', '$tbl_admin_user_id', '$tbl_module_id', '$user_id', NOW(), '$user_id',  NOW(), '$user_id', NOW(), 'Y')";
			$this->cf->insertInto($qry_ins);
	}
	
	
	
	
	function create_school_admin($tbl_admin_user_id, $first_name, $last_name, $dob, $gender, $mobile, $email, $username, $password, $tbl_school_id) {
		$tbl_admin_user_id = $this->cf->get_data($tbl_admin_user_id);
		$first_name        = $this->cf->get_data($first_name);
		$last_name         = $this->cf->get_data($last_name);
		$dob               = $this->cf->get_data($dob);
		$gender            = $this->cf->get_data($gender);
		$mobile            = $this->cf->get_data($mobile);
		$email             = $this->cf->get_data($email);
		$username          = $this->cf->get_data($username);
		$password          = $this->cf->get_data($password);

		$ip = $this->cf->getIP();
		
		$password_code = base64_encode($password); 		
		$hash = sha1($password);
		$salt = substr(md5(uniqid(rand(), true)), 0, 3);
		$hash = sha1($salt . $hash);
		
		$sql4userSession = "SELECT * FROM ".TBL_ADMIN_USERS." WHERE user_id='".$username."' and is_active<>'D' ";

        $dob = date("Y-m-d", strtotime($dob));
		
		/*if($mobile<>"")
			$mobile 	       = "+971".$mobile;*/
			
		if ($this->cf->isExist($sql4userSession)){
			$MSG="The Username you have given already exists!";
			return "E";
		}else{
	
			$qry_ins = "INSERT INTO ".TBL_ADMIN_USERS." (`tbl_admin_id`, `first_name`, `last_name`, `dob`, `gender`, `phone`, `user_type`, `module_access`, `email`, `user_id`, `password`, `salt`, `password_code`,  `reg_date`, `is_active`, `ip`, `last_login`, `tbl_school_id`)
						VALUES ('$tbl_admin_user_id', '$first_name', '$last_name', '$dob', '$gender', '$mobile', 'SA', 'S', '$email', '$username', '$hash', '$salt', '$password_code', NOW(), 'Y', '$ip', NOW(),'$tbl_school_id' )";
			
			$this->cf->insertInto($qry_ins);
			
			$qry = "SELECT file_name_updated FROM ".TBL_UPLOADS." WHERE tbl_item_id='$tbl_admin_user_id' AND is_active='Y' ";
			$result = $this->cf->selectMultiRecords($qry);
			if (count($result)>0) {
				$picture = isset($result[0]['file_name_updated'])? $result[0]['file_name_updated']:'';
				$qry_update  = "UPDATE ".TBL_ADMIN_USERS." SET picture='$picture' WHERE tbl_admin_id='$tbl_admin_user_id' ";
				$this->cf->update($qry_update);
			}
			return "Y";
		}
		
	}
	
	function getSchoolAdmin($tbl_school_id,$user_type, $module_access)
	{
		$tbl_school_id = $this->cf->get_data($tbl_school_id);
		$user_type     = $this->cf->get_data($user_type);
		
		$sql4userSession = "SELECT * FROM ".TBL_ADMIN_USERS." WHERE tbl_school_id='".$tbl_school_id."'  and is_active<>'D' ";
		if($user_type<>"")
		{
			$sql4userSession .= " AND user_type ='".$user_type."'  ";
		}
		if($module_access<>"")
		{
			$sql4userSession .= " AND module_access ='".$module_access."'  ";
		}
		$result = $this->cf->selectMultiRecords($sql4userSession);
		return $result;
	}
	
	function update_school_admin($tbl_admin_user_id, $first_name, $last_name, $dob, $gender, $mobile, $email, $username, $password, $tbl_school_id)  {
		$tbl_admin_user_id = $this->cf->get_data($tbl_admin_user_id);
		$first_name        = $this->cf->get_data($first_name);
		$last_name         = $this->cf->get_data($last_name);
		$dob               = $this->cf->get_data($dob);
		$gender            = $this->cf->get_data($gender);
		$mobile            = $this->cf->get_data($mobile);
		$email             = $this->cf->get_data($email);
		$username          = $this->cf->get_data($username);
		$password          = $this->cf->get_data($password);

		$ip = $this->cf->getIP();
		
		$password_code = base64_encode($password); 		
		$hash = sha1($password);
		$salt = substr(md5(uniqid(rand(), true)), 0, 3);
		$hash = sha1($salt . $hash);
		
		$qry_update  = "UPDATE ".TBL_ADMIN_USERS." SET first_name='$first_name', last_name='$last_name', phone ='$mobile', email='$email'
		                WHERE tbl_admin_id='$tbl_admin_user_id' and tbl_school_id='$tbl_school_id'  ";
		$this->cf->update($qry_update);
		
		if($password<>"")
		{
			$qry_update  = "UPDATE ".TBL_ADMIN_USERS." SET password='$hash', salt='$salt', password_code ='$password_code' WHERE tbl_admin_id='$tbl_admin_user_id' and tbl_school_id='$tbl_school_id'  ";
			$this->cf->update($qry_update);
			
		}
	}
	
	//  School Admin / Minstry / School / Timeline
	
	function get_admin_info_by_email($email) {
		$email = $this->cf->get_data(trim($email));
		$rs    =  array();
		if($email<>""){
			$qry = "SELECT * FROM ".TBL_ADMIN_USERS." WHERE email='$email' ";
			$rs = $this->cf->selectMultiRecords($qry);
	   }
		return $rs;
	}
	
	//END TIMELINE ADMIN USERS SECTION
	
	
	
	
	
	
	
	
	
	
		

    /* function import_data_from_db()
	 {
		 $qry = "SELECT * FROM tbl_datalog_new limit 0,1000";
		 $result = $this->cf->selectMultiRecords($qry);
		 if (count($result)>0) {
		
			 for($i=0;$i<count($result);$i++)
			 {
				
				$tbl_student_id    		= substr(md5(uniqid(rand())),0,15);
				$first_name        		= $result[$i]['first_name'];
				$last_name         		= $result[$i]['last_name'];
				$first_name_ar     		= $result[$i]['first_name_ar'];
				$last_name_ar      		= $result[$i]['last_name_ar'];
				$dob_month         		= $result[$i]['dob_month'];
				$dob_day           		= $result[$i]['dob_day'];
				$dob_year          		= $result[$i]['dob_year'];
				$gender            		= $result[$i]['gender'];
				$mobile            		= $result[$i]['mobile_parent'];
				$tbl_emirates_id        = $result[$i]['emirates_id'];
				$email             		= $result[$i]['email_parent'];
				$file_name_original     = $result[$i]['picture'];
				$file_name_updated      = $result[$i]['picture'];
				$country           		= $result[$i]['nationality'];
				$emirates_id_father		= $result[$i]['emirates_id_parent'];
				$tbl_academic_year_id  	= 'd11f534073';
				$tbl_class_id      		= $result[$i]['class_name'];
				$tbl_school_id     		= '9162b77c3c344ea';
				$emirates_id_mother     = ""; 
				 
				$tbl_parent_id    		= substr(md5(uniqid(rand())),0,15);
				$first_name_parent      = $result[$i]['first_name_parent'];
				$last_name_parent       = $result[$i]['last_name_parent'];
				$first_name_parent_ar   = $result[$i]['first_name_parent_ar'];
				$last_name_parent_ar    = $result[$i]['last_name_parent_ar'];
				$mobile_parent          = $result[$i]['mobile_parent'];
				$email_parent           = $result[$i]['email_parent'];
				$emirates_id_parent		= $result[$i]['emirates_id_parent'];
				$parent_user_id		    = isset($result[$i]['emirates_id_parent'])? $result[$i]['emirates_id_parent'] : $email ;
				
				$password               = "123456";
				$hash = sha1($password);
				$salt = substr(md5(uniqid(rand(), true)), 0, 3);
				$hash = sha1($salt . $hash);
				$pass_code	    =	base64_encode($password);
				
				if($mobile<>"")
					$mobile 	       = "+971".$mobile;             
					
				if($mobile_parent<>"")
					$mobile_parent 	       = "+971".$mobile_parent;
		
				$qry_ins = "INSERT INTO tbl_student (`tbl_student_id`, `tbl_emirates_id`, `first_name`, `last_name`, `first_name_ar`, `last_name_ar`, `dob_month`, 
				`dob_day`, `dob_year`, `gender`, `mobile`, `email`, `country`, `emirates_id_father`, `emirates_id_mother`, `tbl_academic_year_id` , `tbl_class_id`, `added_date`, `is_active`, `tbl_school_id`,`file_name_original`, `file_name_updated`)
							VALUES ('$tbl_student_id', '$tbl_emirates_id', '$first_name', '$last_name', '$first_name_ar', '$last_name_ar', '$dob_month',
				'$dob_day', '$dob_year', '$gender', '$mobile ', '$email', '$country', '$emirates_id_father', '$emirates_id_mother', '$tbl_academic_year_id' , '$tbl_class_id', NOW(), 'Y', '$tbl_school_id', '$file_name_original', '$file_name_updated')";
				
				mysql_query($qry_ins);
				
				
				 $qryParent = "SELECT * FROM tbl_parent WHERE user_id = '$parent_user_id' ";
		 		 $resultParent = $this->cf->selectMultiRecords($qryParent);
		 		 if (count($resultParent)==0) {
					$qry_ins_parent = "INSERT INTO  tbl_parent (`tbl_parent_id`, `first_name`, `last_name`, `first_name_ar`, `last_name_ar`, `dob_month`, 
					`dob_day`, `dob_year`, `gender`, `mobile`, `email`, `emirates_id`, `user_id` , `password`,`salt`,`pass_code`, `added_date`, `is_active`, `tbl_school_id`)
								VALUES ('$tbl_parent_id', '$first_name_parent', '$last_name_parent', '$first_name_parent_ar', '$last_name_parent_ar', '$dob_month_parent',
					'$dob_day_parent', '$dob_year_parent', '$gender_parent', '$mobile_parent', '$email_parent', '$emirates_id_parent', '$parent_user_id', '$hash', '$salt' , '$pass_code', NOW(), 'Y', '$tbl_school_id')";
					mysql_query($qry_ins_parent);
				 }
				 
				 
			 }
		 }
		 
	 }
*/

     function import_data_from_db()
	 {
		 $qry = "SELECT * FROM tbl_student left join tbl_parent ON tbl_student.emirates_id_father=tbl_parent.emirates_id  WHERE 1 ORDER BY tbl_student.id ASC limit 98,1000 ";
		 $result = $this->cf->selectMultiRecords($qry);
		 for($i=0;$i<count($result);$i++)
			 {
				 $tbl_parent_student_id    		= substr(md5(uniqid(rand())),0,15);
				 $tbl_student_id    		    = $result[$i]['tbl_student_id'];
				 $tbl_parent_id    		        = $result[$i]['tbl_parent_id'];
				 $tbl_school_id    		        = $result[$i]['tbl_school_id'];
				 
				  $qry = "INSERT INTO ".TBL_PARENT_STUDENT." (
					`tbl_parent_student_id` ,
					`tbl_parent_id` ,
					`tbl_student_id` ,
					`added_date`,
					`is_active`,
					`tbl_school_id` 
					)
					VALUES (
						'$tbl_parent_student_id', '$tbl_parent_id', '$tbl_student_id', NOW(), 'Y', '$tbl_school_id'
					)";
				mysql_query($qry);
			 }
		 
		/* if (count($result)>0) {
		
			 for($i=0;$i<count($result);$i++)
			 {
				
				$tbl_student_id    		= substr(md5(uniqid(rand())),0,15);
				$first_name        		= $result[$i]['first_name'];
				$last_name         		= $result[$i]['last_name'];
				$first_name_ar     		= $result[$i]['first_name_ar'];
				$last_name_ar      		= $result[$i]['last_name_ar'];
				$dob_month         		= $result[$i]['dob_month'];
				$dob_day           		= $result[$i]['dob_day'];
				$dob_year          		= $result[$i]['dob_year'];
				$gender            		= $result[$i]['gender'];
				$mobile            		= $result[$i]['mobile_parent'];
				$tbl_emirates_id        = $result[$i]['emirates_id'];
				$email             		= $result[$i]['email_parent'];
				$file_name_original     = $result[$i]['picture'];
				$file_name_updated      = $result[$i]['picture'];
				$country           		= $result[$i]['nationality'];
				$emirates_id_father		= $result[$i]['emirates_id_parent'];
				$tbl_academic_year_id  	= 'd11f534073';
				$tbl_class_id      		= $result[$i]['class_name'];
				$tbl_school_id     		= '9162b77c3c344ea';
				$emirates_id_mother     = ""; 
				 
				$tbl_parent_id    		= substr(md5(uniqid(rand())),0,15);
				$first_name_parent      = $result[$i]['first_name_parent'];
				$last_name_parent       = $result[$i]['last_name_parent'];
				$first_name_parent_ar   = $result[$i]['first_name_parent_ar'];
				$last_name_parent_ar    = $result[$i]['last_name_parent_ar'];
				$mobile_parent          = $result[$i]['mobile_parent'];
				$email_parent           = $result[$i]['email_parent'];
				$emirates_id_parent		= $result[$i]['emirates_id_parent'];
				$parent_user_id		    = isset($result[$i]['emirates_id_parent'])? $result[$i]['emirates_id_parent'] : $email ;
				
				$password               = "123456";
				$hash = sha1($password);
				$salt = substr(md5(uniqid(rand(), true)), 0, 3);
				$hash = sha1($salt . $hash);
				$pass_code	    =	base64_encode($password);
				
				if($mobile<>"")
					$mobile 	       = "+971".$mobile;             
					
				if($mobile_parent<>"")
					$mobile_parent 	       = "+971".$mobile_parent;
		
				$qry_ins = "INSERT INTO tbl_student (`tbl_student_id`, `tbl_emirates_id`, `first_name`, `last_name`, `first_name_ar`, `last_name_ar`, `dob_month`, 
				`dob_day`, `dob_year`, `gender`, `mobile`, `email`, `country`, `emirates_id_father`, `emirates_id_mother`, `tbl_academic_year_id` , `tbl_class_id`, `added_date`, `is_active`, `tbl_school_id`,`file_name_original`, `file_name_updated`)
							VALUES ('$tbl_student_id', '$tbl_emirates_id', '$first_name', '$last_name', '$first_name_ar', '$last_name_ar', '$dob_month',
				'$dob_day', '$dob_year', '$gender', '$mobile ', '$email', '$country', '$emirates_id_father', '$emirates_id_mother', '$tbl_academic_year_id' , '$tbl_class_id', NOW(), 'Y', '$tbl_school_id', '$file_name_original', '$file_name_updated')";
				
				mysql_query($qry_ins);
				
				
				 $qryParent = "SELECT * FROM tbl_parent WHERE user_id = '$parent_user_id' ";
		 		 $resultParent = $this->cf->selectMultiRecords($qryParent);
		 		 if (count($resultParent)==0) {
					$qry_ins_parent = "INSERT INTO  tbl_parent (`tbl_parent_id`, `first_name`, `last_name`, `first_name_ar`, `last_name_ar`, `dob_month`, 
					`dob_day`, `dob_year`, `gender`, `mobile`, `email`, `emirates_id`, `user_id` , `password`,`salt`,`pass_code`, `added_date`, `is_active`, `tbl_school_id`)
								VALUES ('$tbl_parent_id', '$first_name_parent', '$last_name_parent', '$first_name_parent_ar', '$last_name_parent_ar', '$dob_month_parent',
					'$dob_day_parent', '$dob_year_parent', '$gender_parent', '$mobile_parent', '$email_parent', '$emirates_id_parent', '$parent_user_id', '$hash', '$salt' , '$pass_code', NOW(), 'Y', '$tbl_school_id')";
					mysql_query($qry_ins_parent);
				 }
				 
				 
			 }
		 }*/
		 
	 }
	 
	 
	 function addParentRandomPassword()
	 {
		 $qryParent = "SELECT * FROM tbl_parent";
		 $resultParent = $this->cf->selectMultiRecords($qryParent);
		 for($k=0;$k<count($resultParent);$k++)
		 {
		 	$tbl_parent_id = $resultParent[$k]['tbl_parent_id'];
		 	$password               = substr(md5(uniqid(rand(), true)), 0, 6);
			//$password   = substr($tbl_parent_id,0,6);
		 	$hash = sha1($password);
		 	$salt = substr(md5(uniqid(rand(), true)), 0, 3);
		 	$hash = sha1($salt . $hash);
		 	$pass_code	    =	base64_encode($password);
		 	$updateParent = " Update tbl_parent SET password = '$hash', salt='$salt' ,pass_code='$pass_code' Where tbl_parent_id= '".$tbl_parent_id ."'";
			$this->cf->update($updateParent);
		 }
				
		 
	 }
	 
	  function addStudentRandomPassword()
	 {
		 $qryStudent = "SELECT * FROM tbl_student";
		 $resultStudent = $this->cf->selectMultiRecords($qryStudent);
		 for($k=0;$k<count($resultStudent);$k++)
		 {
		 	$tbl_student_id          = $resultStudent[$k]['tbl_student_id'];
			$tbl_emirates_id         = $resultStudent[$k]['tbl_emirates_id'];
			$first_name              = $resultStudent[$k]['first_name'];
			if($tbl_emirates_id<>"")
			{
				$student_user_id = $tbl_emirates_id;
			}else{
				$saltUser = substr(md5(uniqid(rand(), true)), 0, 3);
				$student_user_id = strtolower($first_name).$saltUser;
			}
			
		 	$student_password   = substr(md5(uniqid(rand(), true)), 0, 6);
			$student_hash 		= sha1($student_password);
			$student_salt 		= substr(md5(uniqid(rand(), true)), 0, 3);
			$student_hash 		= sha1($student_salt . $student_hash);
			$student_pass_code	= base64_encode($student_password);	

		 	$updateStudent = " Update tbl_student SET student_user_id='$student_user_id', student_password = '$student_hash', student_salt='$student_salt', student_pass_code='$student_pass_code' Where tbl_student_id= '".$tbl_student_id ."' ";
			$this->cf->update($updateStudent);
		 }
				
		 
	 }


}



?>





