<?php
include_once('include/common_functions.php');

/**
 * @desc   	  	About Us Model
 *
 * @category   	Model
 * @author     	Jawaad Ahmed <jawaadmcs@hotmail.com>
 * @version    	0.1
 */
class Model_about_us extends CI_Model {
	var $cf;


	/**
	* @desc Default constructor for the Controller
	*
	* @access default
	*/
    function model_about_us() {
		$this->cf = new Common_functions();
    }


	/**
	* @desc		Get event details
	* 
	* @param	none 	
	* @access	default
	* @return	array $rs
	*/
	function get_about_us_page($tbl_school_id) {
		$qry_sel = "SELECT * FROM ".TBL_ABOUT_US." WHERE tbl_school_id='$tbl_school_id' ";
		//echo $qry_sel;
		$rs = $this->cf->selectMultiRecords($qry_sel);
	return $rs;	
	}
    }
?>

