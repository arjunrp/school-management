<?
define("TITLE","AQDAR - SCHOOL MANAGEMENT SYSTEM");
define("TITLE_ADMIN","AQDAR - SCHOOL MANAGEMENT SYSTEM");
define("APP_HEAD","AQDAR");
define("APP_HEAD_SMALL","Aqdar");


define("FOLDER_NAME","aqdar");
define("HOST_URL", "http://" .$_SERVER["HTTP_HOST"]."/".FOLDER_NAME);
define("CSS_PATH", "http://" .$_SERVER["HTTP_HOST"]."/".FOLDER_NAME."/css");
define("JS_PATH", "http://" .$_SERVER["HTTP_HOST"]."/".FOLDER_NAME."/js");
define("IMG_PATH", "http://" .$_SERVER["HTTP_HOST"]."/".FOLDER_NAME."/images");
define("FONTS_PATH", $_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/fonts");
define("MISC_PATH", $_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/misc");
define("ROOT_PATH", $_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME);
define("JQUERY_LIB_PATH", "http://code.jquery.com/jquery-latest.js");
define("ROOT_ADMIN_PATH", $_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/application/views");

define("VIDEO_UPLOAD_PATH", $_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/images/video");
define("VIDEO_SHOW_PATH", IMG_PATH."/video");

define("THUMBNAIL_UPLOAD_PATH", $_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/images/video/thumb");
define("THUMBNAIL_SHOW_PATH", IMG_PATH."/video/thumb");

define("STUDENT_IMG_UPLOAD_PATH", $_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/images/student");
define("STUDENT_IMG_SHOW_PATH", IMG_PATH."/student");

define("IMG_UPLOAD_PATH_LOGO", $_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/images/logo");
define("IMG_SHOW_PATH_LOGO", IMG_PATH."/logo");
define("IMG_UPLOAD_PATH_HOME", $_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/images/gallery");
define("IMG_HTTP_PATH_HOME", "http://" .$_SERVER["HTTP_HOST"]."/".FOLDER_NAME."/images/gallery");
define("IMG_GALLERY_PATH", "http://".$_SERVER["HTTP_HOST"]."/".FOLDER_NAME."/images/image_gallery");
define("UPLOAD_IMG_GALLERY_PATH", $_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/images/image_gallery");
/* PCHART LIB PATH */ 
define("PCHART_PATH", $_SERVER["DOCUMENT_ROOT"]."/".FOLDER_NAME."/pChart");
/* UPLOAD DIRECTORY PATH */ 
define("UPLOADS_PATH", $_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/admin/uploads");
define("IMG_UPLOAD_PATH", "http://".$_SERVER["HTTP_HOST"]."/".FOLDER_NAME."/admin/uploads");
define("UPLOAD_LOGO_IMG_PATH", $_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/admin/uploads");
define("IMG_LOGO_IMG_PATH", "http://".$_SERVER["HTTP_HOST"]."/".FOLDER_NAME."/admin/uploads");
define("IMG_PATH_STUDENT", "http://".$_SERVER["HTTP_HOST"]."/".FOLDER_NAME."/images/student");
define("UPLOAD_PATH_STUDENT", $_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/images/student");
define("UPLOAD_PATH_TEACHER", $_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/images/teacher");
define("IMG_PATH_TEACHER", "http://".$_SERVER["HTTP_HOST"]."/".FOLDER_NAME."/images/teacher");
define("UPLOAD_PATH_BOOK", $_SERVER['DOCUMENT_ROOT']."/".FOLDER_NAME."/uploads/books");
define("IMG_PATH_BOOK", "http://".$_SERVER["HTTP_HOST"]."/".FOLDER_NAME."/uploads/books");

//=>paths admin side

define("HOST_URL_ADMIN", "http://" .$_SERVER["HTTP_HOST"]."/".FOLDER_NAME."/admin/");
define("ADMIN_FOLDERS_PATH", "http://" .$_SERVER["HTTP_HOST"]."/".FOLDER_NAME."/admin/");
define("ADMIN_CSS_PATH", "http://" .$_SERVER["HTTP_HOST"]."/".FOLDER_NAME."/admin/css/");
define("ADMIN_IMG_PATH", "http://" .$_SERVER["HTTP_HOST"]."/".FOLDER_NAME."/admin/images/");
define("IMG_RICH_PATH", "http://" .$_SERVER["HTTP_HOST"]."/".FOLDER_NAME."/admin/images/Img");
define("ADMIN_SCRIPT_PATH", "http://" .$_SERVER["HTTP_HOST"]."/".FOLDER_NAME."/admin/scripts");
define("INC_ADMIN_PATH", $_SERVER["DOCUMENT_ROOT"]."/".FOLDER_NAME."/admin");
define("ADMIN_USERS_PATH", "http://" .$_SERVER["HTTP_HOST"]."/".FOLDER_NAME."/admin/admin_users_management/");
define("ADMIN_SITES_PATH", "http://" .$_SERVER["HTTP_HOST"]."/".FOLDER_NAME."/admin/admin_sites_management/");

define("IMG_PATH_DEFAULT", "http://".$_SERVER["HTTP_HOST"]."/".FOLDER_NAME."/assets/images/");

/* JQUERY UI */
define("JS_UI", "http://".$_SERVER["HTTP_HOST"]."/".FOLDER_NAME."/jquery-ui");
/* JS COLOR LIBRARY */
define("JS_COLOR_PATH",  "http://".$_SERVER["HTTP_HOST"]."/".FOLDER_NAME."/jscolor");

define("CSS_WEB_PATH", "http://" .$_SERVER["HTTP_HOST"]."/".FOLDER_NAME."/css/web_css");
define("JS_WEB_PATH", "http://" .$_SERVER["HTTP_HOST"]."/".FOLDER_NAME."/js/web_js");
define("IMG_WEB_PATH", "http://" .$_SERVER["HTTP_HOST"]."/".FOLDER_NAME."/images/web_images");

define("DBASE_MAIN","timeline_aqdar");
//=>tables admin 
define("TBL_ADMIN_USERS", DBASE_MAIN . ".tbl_admin_users");
define("TBL_USER_REG", DBASE_MAIN . ".tblusers_regis");
define("TBL_SESSIONS", DBASE_MAIN . ".tblsession");
define("TBL_CATEGORIES", DBASE_MAIN . ".tblcategories");
define("TBL_RIGHTS", DBASE_MAIN . ".tblrights");
define("TBL_SESSIONS_USERS", DBASE_MAIN . ".tblsession_user");
define("TBL_MODULE_CATEGORIES", DBASE_MAIN.".tbl_module_categories");
define("TBL_MODULES", DBASE_MAIN.".tbl_modules");
define("TBL_ADMIN_RIGHTS", DBASE_MAIN.".tbl_admin_rights");

/*DATABASE TABLES*/ 
define("TBL_USER", DBASE_MAIN . ".tbl_user");
define("TBL_VIDEO", DBASE_MAIN . ".tbl_video");
define("TBL_VIDEO_CATEGORY", DBASE_MAIN . ".tbl_video_category");
define("TBL_VIDEO_COMMENTS", DBASE_MAIN . ".tbl_video_comments");
define("TBL_CATEGORY_VIDEO", DBASE_MAIN . ".tbl_category_video");
define("TBL_SUBJECT", DBASE_MAIN . ".tbl_subject");
define("TBL_SCHOOL_TYPE", DBASE_MAIN . ".tbl_school_type");
define("TBL_CLASS", DBASE_MAIN . ".tbl_class");
define("TBL_TEACHER", DBASE_MAIN . ".tbl_teacher");
define("TBL_TEACHER_CLASS", DBASE_MAIN . ".tbl_teacher_class");
define("TBL_STUDENT", DBASE_MAIN . ".tbl_student");
define("TBL_PARENT", DBASE_MAIN . ".tbl_parent");
define("TBL_PARENT_STUDENT", DBASE_MAIN . ".tbl_parent_student");
define("TBL_HOME_PAGE_MESSAGE", DBASE_MAIN . ".tbl_home_page_message");
define("TBL_HOME_PAGE_IMAGES", DBASE_MAIN . ".tbl_home_page_images");
define("TBL_POINTS", DBASE_MAIN . ".tbl_points");
define("TBL_CARDS", DBASE_MAIN . ".tbl_cards");
define("TBL_ABSENT", DBASE_MAIN . ".tbl_absent");////submitted by teacher
define("TBL_LEAVE_APPLICATION", DBASE_MAIN . ".tbl_leave_application");//submitted by parent
define("TBL_SESSION_TEACHER", DBASE_MAIN . ".tbl_session_teacher");
define("TBL_SESSION_PARENT", DBASE_MAIN . ".tbl_session_parent");
define("TBL_MESSAGE", DBASE_MAIN . ".tbl_message");
define("TBL_TOKEN", DBASE_MAIN . ".tbl_token");
define("TBL_SURVEY_QUESTIONS", DBASE_MAIN . ".tbl_survey_questions");
define("TBL_SURVEY_OPTIONS", DBASE_MAIN . ".tbl_survey_options");
define("TBL_SURVEY_ANSWERS", DBASE_MAIN . ".tbl_survey_answers");
define("TBL_IMAGE_GALLERY", DBASE_MAIN.".tbl_image_gallery");
define("TBL_EVENTS", DBASE_MAIN.".tbl_events");
define("TBL_CONTACT_US", DBASE_MAIN.".tbl_contact_us");
define("TBL_SCHOOL_CONTACTS", DBASE_MAIN.".tbl_school_contacts");
define("TBL_UPLOADS", DBASE_MAIN.".tbl_uploads");
define("TBL_PARENTING_CAT", DBASE_MAIN.".tbl_parenting_cat");
define("TBL_CONFIG", DBASE_MAIN . ".tbl_config");
define("TBL_CHILD_DATA", DBASE_MAIN . ".tbl_child_data");
define("TBL_INSTANT_PICS", DBASE_MAIN . ".tbl_instant_pics");
define("TBL_MESSAGE_TEACHER", DBASE_MAIN . ".tbl_message_teacher");
define("TBL_DAILY_REPORT", DBASE_MAIN . ".tbl_daily_report");
define("TBL_BUS", DBASE_MAIN . ".tbl_bus");
define("TBL_BUS_STUDENT", DBASE_MAIN . ".tbl_bus_student");
define("TBL_BUS_TRACKING_SESSION", DBASE_MAIN . ".tbl_bus_tracking_session");
define("TBL_BUS_TRACKING", DBASE_MAIN . ".tbl_bus_tracking");
define("TBL_ATTENDANCE", DBASE_MAIN . ".tbl_attendance");
define("TBL_TEACHER_ATTENDANCE", DBASE_MAIN . ".tbl_teacher_attendance");
define("TBL_PARENTING", DBASE_MAIN . ".tbl_parenting");
define("TBL_SCHOOL", DBASE_MAIN . ".tbl_school");
define("TBL_GALLERY_CATEGORY", DBASE_MAIN . ".tbl_gallery_category");
define("TBL_IMAGE_GALLERYSTU", DBASE_MAIN.".tbl_image_gallerystu");
define("TBL_ABOUT_US", DBASE_MAIN.".tbl_about_us");
define("TBL_HELP", DBASE_MAIN.".tbl_help");
define("TBL_USER_NOTIFY_TOKEN", DBASE_MAIN.".tbl_user_notify_token");
define("TBL_NOTIFICATION_RPT", DBASE_MAIN.".tbl_notification_rpt");
define("TBL_NOTIFICATION_DATA_RPT", DBASE_MAIN.".tbl_notification_data_rpt");
define("TBL_NOTIFICATION_LOG", DBASE_MAIN.".tbl_notification_log");
define("TBL_SECTION", DBASE_MAIN . ".tbl_section");
define("TBL_SCHOOL_ROLES", DBASE_MAIN . ".tbl_school_roles");
define("TBL_USER_MESSAGE_RIGHTS", DBASE_MAIN . ".tbl_user_message_rights");
define("TBL_CLASS_SESSIONS", DBASE_MAIN . ".tbl_class_sessions");

define("TBL_PARENTING_SCHOOL_CAT", DBASE_MAIN . ".tbl_parenting_school_cat");
define("TBL_PARENTING_SCHOOL", DBASE_MAIN . ".tbl_parenting_school");

define("TBL_NOTIFICATION_RPT_TEACHERS", DBASE_MAIN.".tbl_notification_rpt_teachers");
define("TBL_NOTIFICATION_DATA_RPT_TEACHERS", DBASE_MAIN.".tbl_notification_data_rpt_teachers");
define("TBL_NOTIFICATION_LOG_TEACHERS", DBASE_MAIN.".tbl_notification_log_teachers");
define("TBL_CARD_CATEGORY", DBASE_MAIN.".tbl_card_points");
define("TBL_REPORT_ATTENDANCE", DBASE_MAIN.".tbl_report_attendance");
define("VIEW_TBL_TEACHER_ATTENDANCE", DBASE_MAIN.".view_tbl_teacher_attendance");
define("VIEW_TBL_TEACHER_CARDS", DBASE_MAIN.".view_tbl_teacher_cards");
define("TBL_MESSAGE_PARENT", DBASE_MAIN . ".tbl_message_parent");
define("TBL_WEBSITE_FEEDBACK", DBASE_MAIN.".tbl_website_feedback");
define("TBL_GOVT_FEEDBACK", DBASE_MAIN.".tbl_govt_feedback");
define("TBL_COUNTRY", DBASE_MAIN.".tbl_country");
define("TBL_BEHAVIOUR_POINTS", DBASE_MAIN.".tbl_behaviour_points");
define("TBL_TEACHER_GROUP", DBASE_MAIN.".tbl_teacher_group");
define("TBL_TEACHER_GROUP_TEACHERS", DBASE_MAIN.".tbl_teacher_group_teachers");
define("TBL_PARENT_GROUP", DBASE_MAIN.".tbl_parent_group");
define("TBL_PARENT_GROUP_PARENTS", DBASE_MAIN.".tbl_parent_group_parents");
define("TBL_PARENT_FORUM", DBASE_MAIN.".tbl_parent_forum");
define("TBL_PARENT_FORUM_COMMENTS", DBASE_MAIN.".tbl_parent_forum_comments");
define("TBL_ASSIGN_RECORDS", DBASE_MAIN.".tbl_parenting_school_assign");
define("TBL_PRIVATE_MESSAGE", DBASE_MAIN.".tbl_private_message");
define("TBL_BUS_SESSIONS", DBASE_MAIN . ".tbl_bus_sessions");

define("TBL_BUS_ABSENT_DETAILS", DBASE_MAIN . ".tbl_bus_absent_details");
define("TBL_ATTENDANCE_BUS", DBASE_MAIN . ".tbl_attendance_bus");
define("TBL_FIXED_MESSAGE", DBASE_MAIN . ".tbl_fixed_message");
define("TBL_BUS_ANNOUNCEMENT", DBASE_MAIN . ".tbl_bus_announcement");
define("TBL_CARD_POINTS", DBASE_MAIN . ".tbl_card_points");
define("TBL_SEMESTER", DBASE_MAIN . ".tbl_semester");
define("TBL_CARD_POINTS_CONFIG", DBASE_MAIN . ".tbl_card_points_config");
define("TBL_SCHOOL_COMMON_SETTINGS", DBASE_MAIN . ".tbl_school_common_settings");
define("TBL_ACADEMIC_YEAR", DBASE_MAIN . ".tbl_academic_year");

define("TBL_STUDENT_PERFORMANCE_CATEGORY", DBASE_MAIN . ".tbl_student_performance_category");
define("TBL_STUDENT_PERFORMANCE_MASTER", DBASE_MAIN . ".tbl_student_performance_master");
define("TBL_PERFORMANCE_CLASS", DBASE_MAIN . ".tbl_performance_class");
define("TBL_PERFORMANCE_STUDENT", DBASE_MAIN . ".tbl_performance_student");

define("TBL_STUDENT_GROUP", DBASE_MAIN.".tbl_student_group");
define("TBL_STUDENT_GROUP_STUDENTS", DBASE_MAIN.".tbl_student_group_students");
define("TBL_STUDENT_FORUM", DBASE_MAIN.".tbl_student_forum");
define("TBL_STUDENT_FORUM_COMMENTS", DBASE_MAIN.".tbl_student_forum_comments");

define("TBL_BOOKS", DBASE_MAIN.".tbl_books");
define("TBL_BOOK_CATEGORY", DBASE_MAIN.".tbl_book_categories");
define("TBL_BOOK_ASSIGN_CLASS", DBASE_MAIN.".tbl_book_school_assign_class");
define("TBL_BOOK_FILE_TEMP", DBASE_MAIN.".tbl_book_file_temp");

define("TBL_TEACHER_SUBJECT", DBASE_MAIN . ".tbl_teacher_subject");

define("TBL_TEACHER_COMMUNITY_MESSAGE", DBASE_MAIN . ".tbl_teacher_community_message");
define("TBL_STUDENT_COMMUNITY_MESSAGE", DBASE_MAIN . ".tbl_student_community_message");

define("TBL_CUSTOMER_ENQUIRY", DBASE_MAIN . ".tbl_customer_enquiry");

define("TBL_PROGRESS_REPORTS", DBASE_MAIN . ".tbl_progress_reports");
define("TBL_PROGRESS_REPORT_STUDENT", DBASE_MAIN . ".tbl_progress_report_student");

/*PAGING*/
//define("PAGING_OFFERS","10");
define("PAGING_LOCAL_NEWS_CATCH",5);
define("PAGING_GENERAL", "10");
define("TBL_USER_PAGING",10);
define("TBL_MODULE_CATEGORY_PAGING",10);
define("TBL_MODULE_PAGING",10);
define("TBL_ADMIN_USERS_PAGING",10);
define("TBL_CATEGORY_VIDEO_PAGING",10);
define("TBL_VIDEO_PAGING",10);
define("TBL_SUBJECT_PAGING",10);
define("TBL_SCHOOL_TYPE_PAGING",10);
define("TBL_CLASS_PAGING",10);
define("TBL_TEACHER_PAGING",12);
define("TBL_STUDENT_PAGING",12);
define("TBL_PARENT_PAGING",12);
define("TBL_SURVEY_QUESTIONS_PAGING",10);
define("TBL_HOME_PAGE_MESSAGE_PAGING",20);
define("TBL_HOME_PAGE_IMAGES_PAGING",10);
define("TBL_IMAGE_GALLERY_PAGING", "10");
define("TBL_EVENTS_PAGING",10);
define("TBL_CONTACT_US_PAGING",10);
define("TBL_SCHOOL_CONTACTS_PAGING",10);
define("TBL_PARENTING_CAT_PAGING",10);
define("TBL_CHILD_DATA_PAGING",10);
define("TBL_INSTANT_PICS_PAGING",10);
define("TBL_MESSAGE_TEACHER_PAGING",10);
define("TBL_DAILY_REPORT_PAGING",10);
define("TBL_BUS_PAGING",10);
define("TBL_BUS_STUDENT_PAGING",10);
define("TBL_TEACHER_ATTENDANCE_PAGING",20);
define("TBL_PARENTING_PAGING",10);
define("TBL_GALLERY_CATEGORY_PAGING",10);
define("TBL_IMAGE_GALLERYSTU_PAGING",10);
define("TBL_ABOUT_US_PAGING",10);
define("TBL_HELP_PAGING",10);
define("TBL_NOTIFICATION_RPT_PAGING",10);
define("TBL_SECTION_PAGING",10);
define("TBL_SCHOOL_ROLES_PAGING",10);
define("TBL_USER_MESSAGE_RIGHTS_PAGING",10);
define("TBL_NOTIFICATION_RPT_TEACHERS_PAGING",10);
define("TBL_CLASS_SESSIONS_PAGING",10);
define("TBL_MESSAGE_PARENT_PAGING",10);
define("TBL_PARENTING_SCHOOL_CAT_PAGING",10);
define("TBL_PARENTING_SCHOOL_PAGING",10);
define("TBL_SEMESTER_PAGING",10);
define("TBL_FORUM_TOPICS_PAGING",10);
define("TBL_FORUM_COMMENTS_PAGING",10);

define("TBL_SCHOOL_PAGING",10);



define("PAGING_MESSAGES",50);	// View last 50 messages
define("PAGING_CARDS",200);	// View last 200 cards
define("PAGING_POINTS",200);	// View last 200 points
define("PAGING_DAILY_REPORT",100);	// View last 100 reports
define("PAGING_BUS_ATTENDANCE",100);	// View last 100 reports
define("PAGING_TBL_SCHOOL",10);
define("TBL_CARD_CATEGORY_PAGING",10);
define("TBL_TEACHER_GROUP_PAGING",10);
define("TBL_PARENT_GROUP_PAGING",10);
define("TBL_STUDENT_PROMOTION_PAGING",40);
define("TBL_CARD_POINTS_PAGING",10);

define("TBL_STUDENT_GROUP_PAGING",10);
define("PAGING_TBL_BOOKS",10);
define("PAGING_TBL_BOOKS_CATEGORY",10);

define("PAGING_TBL_DATE_ATTENDANCE",30);

/*MISC*/
$todaydate = date("Y-m-d");
define("DOMAIN_NAME", "www.yyy.com");
define("SUPPORT_EMAIL", "info@school_app.com");
define("ENABLE_PUSH", "Y");//Y/N
define("DDM_FOR_TESTING", "0");	// To test application in browser this should be set to 1. Before app release this must be set to 0
define("MSG_NO_RECORD_FOUND", "No records found.");
define("COPYRIGHT", "Copyright © 2017. All Rights Reserved.");
define("DESIGNED_AND_DEVELOPED", "Designed and Developed By <a href='http://www.timeline.ae' target='_blank'>Timeline Technology Solutions</a>");



/*ERROR MESSAGES*/
define("NO_SEARCH_RECORD_FOUND","No Search results found");
define("NO_AUDIO_FOUND","No Record found.");
define("ERR1"," Sorry, the User-ID already exists.");
define("ERR2"," Your User-ID and Password is not correct.");
define("ERR3"," Login first to proceed.");
define("ERR5"," Sorry, you are not allowed to view this page directly.");
define("MSG3","Your Password has been changed successfully.");
define("MSG5","Your Password has been sent to your given e-mail address. Check your mail and then login again.");
define("MSG7","Sorry, incorrect User-ID.");
define("MSG18","Your old password is not correct, Please try again.");
define("MSG20","Your secret answer does not match, Please try again.");
define("NO_RECORD_FOUND","No record found."); 

define("DESIGNED_AND_DEVELOPED","Designed & Developed By Timeline Technology Solution"); 
?>