<?php
	function file_copy($file_origin, $destination_directory, $file_destination, $overwrite, $fatal) {
	
		if ($fatal){
			$error_prefix = 'FATAL: File copy of \'' . $file_origin . '\' to \'' . $destination_directory . $file_destination . '\' failed.';
			$fp = @fopen($file_origin, "r");
			if (!$fp){
			echo $error_prefix . ' Originating file cannot be read or does not exist.';
			exit();
			}
	
			$dir_check = @is_writeable($destination_directory);
			if (!$dir_check){
			echo $error_prefix . ' Destination directory is not writeable or does not exist.';
			exit();
			}
	
			$dest_file_exists = file_exists($destination_directory . $file_destination);
	
			if ($dest_file_exists){
				if ($overwrite){
					$fp = @is_writeable($destination_directory . $file_destination);
					if (!$fp){
					  echo  $error_prefix . ' Destination file is not writeable [OVERWRITE].';
					  exit();
					}
				$copy_file = @copy($file_origin, $destination_directory . $file_destination); 
				}           
			}else{
				$copy_file = @copy($file_origin, $destination_directory . $file_destination);
			}
		}else{
				$copy_file = @copy($file_origin, $destination_directory . $file_destination);
		}
	
	}
?>