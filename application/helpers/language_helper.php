<?php
	/*
		Get language
	*/
	$CI = & get_instance();
	$lan_sel = $CI->uri->segment(1);
	
	//Set default language
	if (trim($lan_sel) == "" || trim($lan_sel) == "/") {
		$lan_sel = "en";
	}
	define('LAN_SEL', $lan_sel);

	/*
		Get page URL according to language
	*/
	$request_url = $_SERVER['REQUEST_URI'];//    /en/about/our_products
	$page_url = substr($request_url,19,strlen($request_url));
	define('PAGE_URL', $page_url);
?>