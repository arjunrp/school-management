<?php

include_once('include/common_functions.php');
/** @desc -- Student Attendance Report Model */

class Model_report extends CI_Model {
     var $cf;
     
	/** @desc Default constructor for the Controller	*/

    function model_report() {
		$this->cf = new Common_functions();
    }
	
	function get_student_cards_report($tbl_student_id, $date_from, $date_to, $tbl_school_id, $tbl_teacher_id='',$tbl_class_id)
	{
		
		    $qry_sub  = "";
            $qry_main = "";

            

            if ($start_date != "" && $end_date != "" && $start_date != $end_date) {

                $qry_main .= " AND A.added_date BETWEEN '".$start_date."' AND '".$end_date."'";

            } else if ($start_date != "" && $end_date != "" && $start_date == $end_date) {

                $qry_main .= " AND A.added_date LIKE '%".trim($start_date)."%'";

            }

            if ($tbl_teacher_id != "") {
				$qry_main .= " AND A.tbl_teacher_id ='".$tbl_teacher_id."'";
			}

			if ($tbl_class_id != "") {
				$qry_main .= " AND A.tbl_class_id ='".$tbl_class_id."'";
			}

			if ($tbl_student_id != "" && $tbl_student_id != "0") {
				$qry_main .= " AND A.tbl_student_id ='".$tbl_student_id."'";
			}			

			

            $Query = "SELECT SUM(A.card_point) AS total, A.card_type AS card_type FROM ".TBL_CARDS." A, ".TBL_CARD_POINTS." B WHERE A.card_type = B.tbl_card_category_id AND A.tbl_school_id='$tbl_school_id_sess' ".$qry_main." AND tbl_student_id IN (SELECT tbl_student_id FROM ".TBL_STUDENT." WHERE 1 ".$qry_sub.") GROUP BY A.card_type";
			
			
			
			$qry = "SELECT P.tbl_class_id, SUM(P.points_student) AS points_student,P.comments_student
			        FROM ".TBL_POINTS." AS P 
					WHERE 
			        P.tbl_student_id='$tbl_student_id' AND P.tbl_class_id='$tbl_class_id' "; 
			if($tbl_teacher_id<>""){
				$qry .= " AND P.tbl_teacher_id='$tbl_teacher_id' ";
			}
            $qry .= "  AND P.tbl_school_id='$tbl_school_id' ";
			if($date_to<>"")
			{
				$qry .=  " AND (P.added_date>='$date_from' OR P.added_date<='$date_to') "; 
			}else{
				$qry .=  " AND P.added_date ='$date_from' ";
			}
			
			$qry .= "AND P.is_active='Y' GROUP BY P.comments_student order by P.comments_student ASC,  P.added_date ASC ";
		//echo $qry; exit;
		$rs = $this->cf->selectMultiRecords($qry);
		return $rs;	
	}
}

?>



