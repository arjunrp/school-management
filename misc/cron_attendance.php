<?php include ($_SERVER["DOCUMENT_ROOT"] ."/aqdar/includes/config.inc.php");?>
<?php
	/*
		Cron will populate TBL_REPORT_ATTENDANCE on each data
	*/
	
	$todaydate = date("Y-m-d");

	//Get All Schools
	$qry_schools = "SELECT * FROM ".TBL_SCHOOL." WHERE is_active='Y' ";
	$rs_schools = selectMultiRecords($qry_schools);
	
	//Loop all schools	
	for ($s=0; $s<count($rs_schools); $s++) {
		$id_s = $rs_schools[$s]['id'];
		$tbl_school_id_s = $rs_schools[$s]['tbl_school_id'];
		$school_name_s = $rs_schools[$s]['school_name'];
		$description_s = $rs_schools[$s]['description'];
		$color_code_s = $rs_schools[$s]['color_code'];
		$logo_s = $rs_schools[$s]['logo'];
		$school_type_s = $rs_schools[$s]['school_type'];
		$added_date_s = $rs_schools[$s]['added_date'];
		$is_active_s = $rs_schools[$s]['is_active'];
		
		
		/*
			Against each school find students belonging to country and gender
		*/

			/*
				In resultset a country can have 2 records. One for male and one for female with count against each colulm. 
				Once we process a country against a school we need to mark is_processed='Y'
				So at every iteration we see country processed in previous loop [var $country_processed will hold last processed country]
				If in loop current country is different from last processed country then we mark last processed country as is_processed='Y'
			*/
			$country_processed = "";

			$qry_student_gender_country = "
				SELECT COUNT(DISTINCT tbl_student_id) as total_students_present, gender, country 
				FROM ".VIEW_TBL_TEACHER_ATTENDANCE." 
				WHERE attendance_date = '".$todaydate."' AND is_present='Y' AND tbl_school_id='".$tbl_school_id_s."'
				GROUP BY gender, country
				ORDER BY country ASC, gender ASC";
			
				$rs_student_gender_country = selectMultiRecords($qry_student_gender_country);
				
					
				//Loop all countries
				for ($k=0; $k<count($rs_student_gender_country); $k++) {
					$total_students_present_k = $rs_student_gender_country[$k]['total_students_present'];
					$gender_k = $rs_student_gender_country[$k]['gender'];
					$country_k = $rs_student_gender_country[$k]['country'];
	
					if (trim($country_processed) == "") {
						$country_processed = $country_k;
					}


					$qry_processed = "SELECT * FROM ".TBL_REPORT_ATTENDANCE." WHERE attendance_date='$todaydate' AND country='$country_k' AND tbl_school_id='$tbl_school_id_s' AND is_processed='Y' ";
					$rs_processed = selectMultiRecords($qry_processed);
								
					//Country we are processing is changed or we are at last record with one school
					if (trim($country_processed) != $country_k || count($rs_student_gender_country) == $k+1) {
						$qry_update_total_students = "UPDATE ".TBL_REPORT_ATTENDANCE." SET is_processed='Y'
													  WHERE attendance_date='$todaydate' AND country='$country_processed' AND tbl_school_id='$tbl_school_id_s' ";				
						update($qry_update_total_students);	
						
						$country_processed = $country_k;						  
					}
	
					$qry_exist = "SELECT * FROM ".TBL_REPORT_ATTENDANCE." WHERE attendance_date='$todaydate' AND country='$country_k' AND tbl_school_id='$tbl_school_id_s' ";
					$rs_exist = selectMultiRecords($qry_exist);
	
	
					if ($gender_k == "male") {
						if (count($rs_exist)>0) {
								$qry_update = "UPDATE ".TBL_REPORT_ATTENDANCE." SET male_present='$total_students_present_k', country='$country_k'
											   WHERE attendance_date='$todaydate' AND country='$country_k' AND tbl_school_id='$tbl_school_id_s' ";	
								update($qry_update);			   
						} else {
								$qry = "INSERT INTO ".TBL_REPORT_ATTENDANCE." (`tbl_school_id` ,`attendance_date` ,`country` ,`male_present` ,`added_date` ,`is_active`)
										VALUES ('$tbl_school_id_s', '$todaydate', '$country_k', '$total_students_present_k', NOW(), 'Y')";
								insertInto($qry);		
						}
					}
					
					if ($gender_k == "female") {
						if (count($rs_exist)>0) {
								$qry_update = "UPDATE ".TBL_REPORT_ATTENDANCE." SET female_present='$total_students_present_k', country='$country_k'
											   WHERE attendance_date='$todaydate' AND country='$country_k' AND tbl_school_id='$tbl_school_id_s' ";	
								update($qry_update);			   
						} else {
								$qry = "INSERT INTO ".TBL_REPORT_ATTENDANCE." (`tbl_school_id` ,`attendance_date` ,`country` ,`female_present` ,`added_date` ,`is_active`)
										VALUES ('$tbl_school_id_s', '$todaydate', '$country_k', '$total_students_present_k', NOW(), 'Y')";
								insertInto($qry);		
						}
					}
					
					
					/*
						Against gender, country in a school find total students, male and female
					*/
					$qry_total_male = "SELECT COUNT(*) as total_students_male FROM ".TBL_STUDENT." WHERE gender='male' AND country='$country_k' AND tbl_school_id='$tbl_school_id_s' ";
					$rs_total_male = selectFrom($qry_total_male);
					$total_students_male = $rs_total_male['total_students_male'];
					
	
					$qry_total_female = "SELECT COUNT(*) as total_students_female FROM ".TBL_STUDENT." WHERE gender='female' AND country='$country_k' AND tbl_school_id='$tbl_school_id_s' ";
					$rs_total_female = selectFrom($qry_total_female);
					$total_students_female = $rs_total_female['total_students_female'];
					
					
					$qry_update_total_students = "UPDATE ".TBL_REPORT_ATTENDANCE." SET male_total='$total_students_male', female_total='$total_students_female'
												  WHERE attendance_date='$todaydate' AND country='$country_k' AND tbl_school_id='$tbl_school_id_s' ";				
					update($qry_update_total_students);							  
				}
			
	}//for ($s=0; $s<count($rs_schools); $s++)
?>			